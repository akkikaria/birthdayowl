<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function check_post($value) {
    if ($value == NULL) {
        return '';
    }
    return $value;
}

function DateConversionOld($schedule, $delivery_date, $delivery_time, $timezone) {

    switch ($schedule) {
        case 0: {
                $data["delivery_date_time"] = "00:00:00 00:00:00";
                $data["converted_date"] = "00:00:00 00:00:00";
                return $data;
            }
            break;
        case 1: {//Today
                $data["delivery_date_time"] = "00:00:00 00:00:00";
                $data["converted_date"] = "00:00:00 00:00:00";
                return $data;
            }

            break;
        case 2: {//Tomorrow
                $tomorrow = date("Y-m-d", strtotime("tomorrow"));
                $date = $tomorrow . ' ' . $delivery_time;
                $triggerOn = date('Y-m-d H:i:s', strtotime($date));
                $schedule_date = new DateTime($triggerOn, new DateTimeZone($timezone));
                $schedule_date->setTimeZone(new DateTimeZone('Asia/Kolkata')); //server
                $data["delivery_date_time"] = $triggerOn;
                $data["converted_date"] = $schedule_date->format('Y-m-d H:i:s');
                return $data;
            }
            break;
        case 3: {
                $day_after_tomorrow = date('Y-m-d', strtotime('tomorrow + 1 day'));
                $date = $day_after_tomorrow . ' ' . $delivery_time;
                $triggerOn = date('Y-m-d H:i:s', strtotime($date));
                $schedule_date = new DateTime($triggerOn, new DateTimeZone($timezone));
                $schedule_date->setTimeZone(new DateTimeZone('Asia/Kolkata')); //server
                $data["delivery_date_time"] = $triggerOn;
                $data["converted_date"] = $schedule_date->format('Y-m-d H:i:s');
                return $data;
            }
            break;
        case 4: {//Specific Date
                $sdate = date("Y-m-d", strtotime(str_replace('/', '-', $delivery_date)));
                $spdate = $sdate . ' ' . $delivery_time;
                $triggerOn = date('Y-m-d H:i:s', strtotime($spdate));
                $schedule_date = new DateTime($triggerOn, new DateTimeZone($timezone));
                $schedule_date->setTimeZone(new DateTimeZone('Asia/Kolkata')); //server
                $data["delivery_date_time"] = $triggerOn;
                $data["converted_date"] = $schedule_date->format('Y-m-d H:i:s');
                return $data;
            }
            break;
    }
}

function DateConversion($schedule, $delivery_date, $delivery_time, $timezone) {

    switch ($schedule) {
        case 0: {
                $sdate = date("Y-m-d");
                $spdate = $sdate . ' ' . $delivery_time;
                $triggerOn = date('Y-m-d H:i:s', strtotime($spdate));
                $schedule_date = new DateTime($triggerOn, new DateTimeZone($timezone));
                $schedule_date->setTimeZone(new DateTimeZone('Asia/Kolkata')); //server
                $data["delivery_date_time"] = $triggerOn;
                $data["converted_date"] = $schedule_date->format('Y-m-d H:i:s');
                return $data;
            }
            break;
        case 1: {//Today
                $sdate = date("Y-m-d");
                $spdate = $sdate . ' ' . $delivery_time;
                $triggerOn = date('Y-m-d H:i:s', strtotime($spdate));
                $schedule_date = new DateTime($triggerOn, new DateTimeZone($timezone));
                $schedule_date->setTimeZone(new DateTimeZone('Asia/Kolkata')); //server
                $data["delivery_date_time"] = $triggerOn;
                $data["converted_date"] = $schedule_date->format('Y-m-d H:i:s');
                return $data;
            }
            break;
        case 2: {//Tomorrow
                $tomorrow = date("Y-m-d", strtotime("tomorrow"));
                $date = $tomorrow . ' ' . $delivery_time;
                $triggerOn = date('Y-m-d H:i:s', strtotime($date));
                $schedule_date = new DateTime($triggerOn, new DateTimeZone($timezone));
                $schedule_date->setTimeZone(new DateTimeZone('Asia/Kolkata')); //server
                $data["delivery_date_time"] = $triggerOn;
                $data["converted_date"] = $schedule_date->format('Y-m-d H:i:s');
                return $data;
            }
            break;
//        case 3: {
//                $day_after_tomorrow = date('Y-m-d', strtotime('tomorrow + 1 day'));
//                $date = $day_after_tomorrow . ' ' . $delivery_time;
//                $triggerOn = date('Y-m-d H:i:s', strtotime($date));
//                $schedule_date = new DateTime($triggerOn, new DateTimeZone($timezone));
//                $schedule_date->setTimeZone(new DateTimeZone('Asia/Kolkata')); //server
//                $data["delivery_date_time"] = $triggerOn;
//                $data["converted_date"] = $schedule_date->format('Y-m-d H:i:s');
//                return $data;
//            }
//            break;
        case 3: {//Specific Date
                $sdate = date("Y-m-d", strtotime(str_replace('/', '-', $delivery_date)));
                $spdate = $sdate . ' ' . $delivery_time;
                $triggerOn = date('Y-m-d H:i:s', strtotime($spdate));
                $schedule_date = new DateTime($triggerOn, new DateTimeZone($timezone));
                $schedule_date->setTimeZone(new DateTimeZone('Asia/Kolkata')); //server
                $data["delivery_date_time"] = $triggerOn;
                $data["converted_date"] = $schedule_date->format('Y-m-d H:i:s');
                return $data;
            }
            break;
    }
}

function DateConversion_new($delivery_date_time, $timezone) {
    $triggerOn = date('Y-m-d H:i:s', strtotime($delivery_date_time));
    $schedule_date = new DateTime($triggerOn, new DateTimeZone($timezone));
    $schedule_date->setTimeZone(new DateTimeZone('Asia/Kolkata')); //server
    $data["delivery_date_time"] = $triggerOn;
    $data["converted_date"] = $schedule_date->format('Y-m-d H:i:s');
    return $data;
}

function get_contact_info() {
    $CI = get_instance();
    $CI->load->model('Birthday');
    $contactinfo = $CI->Birthday->getAllData("contact_us")->row_array();
    return $contactinfo;
}

function get_countryinfo() {
    $CI = get_instance();
    $CI->load->model('Birthday');
    $country_info = $CI->Birthday->getSelectData("country_id,country_name,nationality_code", "country", "null")->result_array();
    return $country_info;
}

function get_security_token() {
    $CI = get_instance();
    $CI->load->model('Birthday');
    $vdata["stoken"] = $stoken = bin2hex(openssl_random_pseudo_bytes(16));
    $CI->session->set_userdata($vdata);
    return $stoken;
}

function getAllVouchers() {
    if (SERVER_TYPE == 3)
        $file = BASEURL . "files/products.txt";
    else
        $file = BASEURL . "files/products1.txt"; //server
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $file);
    $products = curl_exec($ch);
    curl_close($ch);
    return json_decode($products, True);
}

function checkServerRunning() {
    if ($socket = @ fsockopen($host, 80, $errno, $errstr, 30)) {
        echo 'online!';
        fclose($socket);
    } else {
        echo 'offline.';
    }
}

function send_Email($view, $toemail, $tofirstname, $fromemail, $fromName, $subject) {
    require_once(APPPATH . "libraries/Mailin.php" );
    $mailin = new Mailin('https://api.sendinblue.com/v2.0', SENDINKEY);
    $sdata = array("to" => array("$toemail" => "$tofirstname"),
        "from" => array("$fromemail", "$fromName"),
        "subject" => "$subject",
        "text" => "$view",
        "html" => "$view",
    );
    $mailin->send_email($sdata);
}

function send_EmailResponse($view, $toemail, $tofirstname, $fromemail, $fromName, $subject) {
    require_once(APPPATH . "libraries/Mailin.php" );
    $mailin = new Mailin('https://api.sendinblue.com/v2.0', SENDINKEY);
    $sdata = array(
        "to" => array("$toemail" => "$tofirstname"),
        "from" => array("$fromemail", "$fromName"),
        "subject" => "$subject",
        "text" => "$view",
        "html" => "$view",
    );
    return $mailin->send_email($sdata);
}

function send_EmailResponse_with_cc($view, $toemail, $tofirstname, $toccemail, $toccfirstname, $fromemail, $fromName, $subject) {
    require_once(APPPATH . "libraries/Mailin.php" );
    $mailin = new Mailin('https://api.sendinblue.com/v2.0', SENDINKEY);

    for ($i = 0; $i < count($toemail); $i++) {
        $toemail_array = array_combine($toemail[$i], $tofirstname[$i]);
    }

    for ($j = 0; $j < count($toccemail); $j++) {
        $toccemail_array = array_combine($toccemail[$j], $toccfirstname[$j]);
    }

    $sdata = array(
        "to" => $toemail_array,
        "cc" => $toccemail_array,
        "from" => array("$fromemail", "$fromName"),
        "subject" => "$subject",
        "text" => "$view",
        "html" => "$view",
    );
    return $mailin->send_email($sdata);
}

function get_ip() {
//        $ip1 = "45.56.76.221";
//        return $ip1;
    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
    }
}

function get_country_info() {
    $ip = get_ip();
    
    $ctx = stream_context_create(array('http' =>
        array(
            'timeout' => 3  //setting time out is 3 second
        )
    ));
    $query = json_decode(@file_get_contents('http://apinotes.com/ipaddress/ip.php?ip=' . $ip, true, $ctx)); // @ is added in order to supress error and warning

    if (is_null($query)) {//if the server is down than it will produce a null value
        $output["country_name"] = "IN";
        $output["nationality_code"] = "91";
        $output["country_id"] = "102";
    } else {
        $value_code = get_object_vars($query);
        $country_name = $value_code["country_code"];

        if ($value_code["status"] == "Failed" || $country_name == " ") {
            $output["country_name"] = "IN";
            $output["nationality_code"] = "91";
            $output["country_id"] = "102";
        } else {
            $CI = get_instance();
            $CI->load->model('Birthday');
            $country_code_details = $CI->Birthday->getUserData("country", "country_initials='$country_name'")->result_array();

            if (!empty($country_code_details)) {
                $country_name = $country_code_details[0]["country_name"];
                $nationality_code = $country_code_details[0]["nationality_code"];
                $country_id = $country_code_details[0]['country_id'];
                $output["country_name"] = $country_name;
                $output["nationality_code"] = $nationality_code;
                $output["country_id"] = $country_id;
            } else { // if there is no record for the ip address then we 
                $output["country_name"] = "IN";
                $output["nationality_code"] = "91";
                $output["country_id"] = "102";
            }
        }
    }
    return $output;
}
