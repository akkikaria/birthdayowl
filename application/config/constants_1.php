<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777); //if any issues change it to 0755  

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');
/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
define('SHOW_DEBUG_BACKTRACE', TRUE);
define('SERVER_TYPE', '3'); //1-live, 2-demo, 3-local



switch (SERVER_TYPE) {
    case 1: { //LIVE SERVER
            define('BASEURL', 'https://www.birthdayowl.com/');
            define('temporaryCredentialsRequestUrl', 'http://sandbox.woohoo.in/oauth/initiate?oauth_callback=oob');
            define('authorizationUrl', 'http://sandbox.woohoo.in/oauth/authorize/customerVerifier?oauth_token=');
            define('accessTokenRequestUrl', 'http://sandbox.woohoo.in/oauth/token');
            define('consumerKey', '8af50260ae5444bdc34665c2b6e6daa923');
            define('consumerSecret', '93c1d8f362749dd1fe0a819ae8b5de952312');
            define('Wusername', 'sujit@birthdayowl.com');
            define('Wpassword', 'SuJ!tApi@BWL764');
            define('wurl', 'https://extapi.woohoo.in/rest/');
            define('MERCHANT_KEY', 'Y1YelOPp');
            define('PSALT', 'z8mMP22IHS');
            define('PAYU_BASE_URL', 'https://secure.payu.in');
            define('REFUND_URL', 'https://www.payumoney.com/payment/merchant/refundPayment?');
            define("SENDINKEY", 'WqkA8UFH7KgZrC6M');
        }
        break;
    case 2: { //AMEZON DEV (Only EC2)
            define('BASEURL', 'https://www.demo.birthdayowl.com/'); //current domain url without index.php
            define('temporaryCredentialsRequestUrl', 'https://extapi.woohoo.in/oauth/initiate?oauth_callback=oob');
            define('authorizationUrl', 'https://extapi.woohoo.in/oauth/authorize/customerVerifier?oauth_token=');
            define('accessTokenRequestUrl', 'https://extapi.woohoo.in/oauth/token');
            define('consumerKey', '5bbcea0086f833783b6fb010d45fdd42');
            define('consumerSecret', 'de81de0e2c2abfe5684241e150b1d460');
            define('Wusername', 'sujit@birthdayowl.com');
            define('Wpassword', 'SuJ!tApi@BWL764');
            define('wurl', 'https://extapi.woohoo.in/rest/');
            define('MERCHANT_KEY', 'Y1YelOPp');
            define('PSALT', 'z8mMP22IHS');
            define('PAYU_BASE_URL', 'https://secure.payu.in');
            define('REFUND_URL', 'https://www.payumoney.com/payment/merchant/refundPayment?');
            define("SENDINKEY", 'WqkA8UFH7KgZrC6M');
        }
        break;
    case 3: { //LOCALHOST (XAMPP Server)
//            define('BASEURL', 'http://localhost/birthdayowl/');   //current domain url without index.php
            define('BASEURL', 'http://localhost/birthdayowl/');   //current domain url without index.php
            define('temporaryCredentialsRequestUrl', 'http://sandbox.woohoo.in/oauth/initiate?oauth_callback=oob');
            define('authorizationUrl', 'http://sandbox.woohoo.in/oauth/authorize/customerVerifier?oauth_token=');
            define('accessTokenRequestUrl', 'http://sandbox.woohoo.in/oauth/token');
            define('consumerKey', '8af50260ae5444bdc34665c2b6e6daa9');
            define('consumerSecret', '93c1d8f362749dd1fe0a819ae8b5de95');
            define('Wusername', 'BdayOwlapisandbox@woohoo.in');
            define('Wpassword', 'BdayOwlapisandbox@woohoo.in');
            define('wurl', 'https://sandbox.woohoo.in/rest/');
            define('MERCHANT_KEY', 'd48dWpmr');
            define('PSALT', 'g2axUToKIf');
            define('PAYU_BASE_URL', 'https://test.payu.in');
            define('REFUND_URL', 'https://test.payumoney.com/payment/merchant/refundPayment?');
            define("SENDINKEY", 'WqkA8UFH7KgZrC6M');
//                  define("SENDINKEY", 'N7FKba4LPMGd0Trp');
        }
        break;
    default:
        break;
}


/* End of file constants.php */
/* Location: ./application/config/constants.php */
//define('BASEURL','http://birthday-matters.com/bday/index.php/');
//define('BASEURL1','http://birthday-matters.com/bday/'); //cpanel
//define('BASEURL', 'http://192.168.1.139/BirthdayOwl/');
//define('BASEURL','http://localhost/BirthdayOwlTest/');
//define('BASEURL','http://test.birthdayowl.com/');
//define('BASEURL', 'http://198.58.97.191/BirthdayOwl/'); //linode-developement
//Birthday Owl
define("BASEURL_OIMG", BASEURL . 'public/owl_assets/images/');
define("BASEURL_ONEWIMG", BASEURL . 'public/owl_assets/images/new_assets/');
define("BASEURL_EIMG", BASEURL . 'public/owl_assets/emailImages/');
define("BASEURL_OJS", BASEURL . 'public/owl_assets/js/');
define("BASEURL_OCSS", BASEURL . 'public/owl_assets/css/');
define("BASEURL_BCSS", BASEURL . 'public/assets/css/');
define("BASEURL_BJS", BASEURL . 'public/assets/js/');
define("BASEURL_DATE", BASEURL . 'public/owl_assets/datepicker/');
define("BASEURL_TINY", BASEURL . 'public/owl_assets/tiny/');
define("BASEURL_VIDEO", BASEURL . 'public/header_video/');
//define("BASEURL_CSS", BASEURL1 . 'public/css/');
//define("BASEURL_JS", BASEURL1 . 'public/js/');
//define("BASEURL_FANCY", BASEURL1 . 'public/fancybox/');
//define("BASEURL_FONT", BASEURL1 . 'public/fonts/');
//define("BASEURL_IMG", BASEURL1 . 'public/images/');
//define("BASEURL_PROFILE", BASEURL1 . 'public/profile_pic/');

define("BASEURL_FANCY", BASEURL . 'public/fancybox/');
define("BASEURL_FONT", BASEURL . 'public/fonts/');
define("BASEURL_IMG", BASEURL . 'public/images/');
define("BASEURL_BVMG", BASEURL . 'public/imageVideo/');
define("BASEURL_PROFILE", BASEURL . 'public/profile_pic/');
define("BASEURL_NEWS", BASEURL . 'public/newsletter_img/');
define("BASEURL_IMGPICKER", BASEURL . 'public/image-picker/');
define("BASEURL_IMG_GALLERY", BASEURL . 'public/assets/images/gallery/');
define("BASEURL_GREETINGS", BASEURL . 'public/greetings/');
define("BASEURL_PRODUCT_IMG", BASEURL . 'public/product_img/');
define("BASEURL_VCAT", BASEURL . 'public/voucher_category/');
define("BASEURL_VPRO", BASEURL . 'public/voucher_product/');
define("BASEURL_PAGE", BASEURL . 'public/pagination/js/');
define("BASEURL_PAGE_CSS", BASEURL . 'public/pagination/css/');
define("BASEURL_BPIC", BASEURL . 'public/banner_pic/');
define("BASEURL_HPIC", BASEURL . 'public/home_screens/');
//admin-panel
define("BASEURL_AMN_CSS", BASEURL . 'public/css/');
define("BASEURL_AMN_JS", BASEURL . 'public/js/');
define("BASEURL_AMN_IMG", BASEURL . 'public/img/');

//new-designs

define("BASEURL_BFONT", BASEURL . 'public/assets/fonts/');
define("BASEURL_BIMG", BASEURL . 'public/assets/images/');
define("BASEURL_PIMG", BASEURL . 'public/assets/img/');
define("BASEURL_CAL", BASEURL . 'public/calender/js/');
define("BASEURL_CAL_CSS", BASEURL . 'public/calender/css/');
define("BASEURL_CROPPED_GREET", BASEURL . 'public/cropped_greetings/');

//editor
define("BASEURL_ECSS", BASEURL . 'public/mono/css/');
define("BASEURL_EJS", BASEURL . 'public/mono/js/');

//define('GOOGLE_API_KEY', 'AIzaSyCM7FIhRtkrBDF0DcA_Yawt53BO7rIFogw');
//datepicker


/* End of Website file constants.php */

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/*
  |--------------------------------------------------------------------------
  | WEB(VIEW) CONTROLLERS FUNCTIONS - controller/Home_web
  |--------------------------------------------------------------------------
 */

define('WEB_HOME', BASEURL); //1
define('WEB_EGIFTCARD_VOUCHERS', BASEURL . 'egiftcard-vouchers'); //2
define('WEB_FLOWERS_AND_CAKES', BASEURL . 'buy-flower-birthday-cakes-and-gifts-online'); //3
define('WEB_FREE_GREETING_CARDS', BASEURL . 'free-birthday-greeting-cards'); //4
define('WEB_ZODIAC_SIGNS', BASEURL . 'free-horoscope-zodiac-signs'); //5
define('WEB_DASHBOARD', BASEURL . 'dashboard'); //6
define('WEB_VIEW_BIRTHDAY_REMINDER', BASEURL . 'view-birthday-reminder'); //7
define('WEB_ORDER_HISTORY', BASEURL . 'order-history'); //8
define('WEB_GIFT_WALLET', BASEURL . 'gift-wallet'); //9
define('WEB_KNOW_YOUR_HOROSCOPE', BASEURL . 'know_horoscope'); //10
define('WEB_HOROSCOPE_DAILY_WISE', BASEURL . 'horoscope_daily'); //11
define('WEB_UPDATE_PROFILE', BASEURL . 'update-profile'); //12
define('WEB_FAQS', BASEURL . 'faqs'); //13
define('WEB_PRIVACY_AND_POLICY', BASEURL . 'privacy-policy'); //14
define('WEB_TERMS_OF_USE_EGIFT', BASEURL . 'terms-of-use-egift'); //15
define('WEB_TERMS_OF_USE_OTHER', BASEURL . 'terms-of-use-other'); //16
define('WEB_ABOUT_US', BASEURL . 'about-us'); //17
define('WEB_BIRTHDAY_REMINDER_WITHOUT_LOGIN', BASEURL . 'free-birthday-reminder-calendar-and-alarm-online'); //18
define('WEB_SHOW_CART', BASEURL . 'cart'); //19

/*
  |--------------------------------------------------------------------------
  | WEB(VIEW) CONTROLLERS FUNCTIONS - controller/Home_web
  |--------------------------------------------------------------------------
 */

//For Google Login redirect urls

//define('cart', BASEURL . 'cart');
//define('add-reminder', BASEURL . 'addReminder/0');
//define("CLIENT_ID", "349088178137-nrvrv8ur7svgicrtc55p82n8fc12nuf2.apps.googleusercontent.com");
//define("CLIENT_SECRET", "dWaRvwpXf9lzvu9jO60hL8NJ");
//
//define("HOMEPAGE", "http://localhost/birthdayowl/Home_web/redirect_google_login/MA==");
//define("GOOGLE_LOGIN_URL_HOMEPAGE", "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=http%3A%2F%2Flocalhost%2Fbirthdayowl%2FHome_web%2Fredirect_google_login%2FMA%3D%3D&client_id=349088178137-nrvrv8ur7svgicrtc55p82n8fc12nuf2.apps.googleusercontent.com&scope=email&access_type=online&approval_prompt=auto");
//
//define("REDIRECT_CART", "http://localhost/birthdayowl/Home_web/redirect_google_login/MQ==");
//define("GOOGLE_LOGIN_URL_CART", "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=http%3A%2F%2Flocalhost%2Fbirthdayowl%2FHome_web%2Fredirect_google_login%2FMQ%3D%3D&client_id=349088178137-nrvrv8ur7svgicrtc55p82n8fc12nuf2.apps.googleusercontent.com&scope=email&access_type=online&approval_prompt=auto");
//
//define("REDIRECT_REMINDER", "http://localhost/birthdayowl/Home_web/redirect_google_login/Mg==");
//define("GOOGLE_LOGIN_URL_REMINDER", "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=http%3A%2F%2Flocalhost%2Fbirthdayowl%2FHome_web%2Fredirect_google_login%2FMg%3D%3D&client_id=349088178137-nrvrv8ur7svgicrtc55p82n8fc12nuf2.apps.googleusercontent.com&scope=email&access_type=online&approval_prompt=auto");

//For Google Login redirect urls