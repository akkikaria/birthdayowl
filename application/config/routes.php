<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'Home_web';
$route[BASEURL] = BASEURL . "voucher";

/* ----------------Web View Routes-------------------- */
$route[WEB_HOME] = "Home_web/index"; //1
$route["egiftcard-vouchers"] = "Home_web/egiftcards"; //2
$route["buy-flower-birthday-cakes-and-gifts-online"] = "Home_web/flowers_product"; //3
//$route["free-online-birthday-greeting-cards"] = "Home_web/egreetings"; //4
$route["free-birthday-greeting-cards"] = "Home_web/egreetings"; //4
$route["free-horoscope-zodiac-signs"] = "Home_web/horoscope"; //5
$route["dashboard"] = "Home_web/dashboard"; //6
$route["view-birthday-reminder"] = "Home_web/view_bday_reminder"; //7
$route["order-history"] = "Home_web/display_order_history"; //8
$route["gift-wallet"] = "Home_web/gift_wallet"; //9
$route["know_horoscope"] = "Home_web/know_horoscope"; //10
$route["horoscope_daily"] = "Home_web/know_horoscope_day_wise"; //11
$route["update-profile"] = "Home_web/my_account"; //12
$route["faqs"] = "Home_web/faq"; //13
$route["privacy-policy"] = "Home_web/privacy_policy"; //14
$route["terms-of-use-egift"] = "Home_web/terms_n_conditions_egift"; //15
$route["terms-of-use-other"] = "Home_web/terms_n_conditions_other"; //16
$route["about-us"] = "Home_web/about_us"; //17
$route["free-birthday-reminder-calendar-and-alarm-online"] = "Home_web/add_reminder_without_login"; //18
$route["cart"] = "Home_web/show_cart"; //19

/* ----------------Can't Implement constant on below routes-------------------- */
$route["flowers_detail/:any/:any"] = "Home_web/flowers_detail/";
$route["delivery_address/:any"] = "Home_web/delivery_address/";
$route["insidegreeting/:num/:num/:num"] = "Home_web/freegreeting_details/";
$route["insidegreeting-details/:num/:num/:num"] = "Home_web/freegreeting_details_new/";
$route["checkThankyou/:num"] = "Home_web/checkThankyou";
$route["thankyou/:num/:num"] = "Home_web/thankyou";
$route["addReminder/(:any)"] = "Home_web/add_Birthday";
$route["editegreeting/:num"] = "Home_web/editegreeting/";
$route["search/:any"] = "Home_web/search/";
$route["membershipcart/:any"] = "Home_web/membership_cart/";
$route['select_zodiac/:any'] = 'Home_web/select_zodiac/';
$route['voucher/:any'] = 'Home_web/display_product/';
$route['edit_voucher/:any'] = 'Home_web/edit_voucher/';
$route['free_greetings_email/:any/:any/:any/:any'] = 'Home_web/free_greetings_email';
$route['webMobile/:any/:any/:any/:any/:any'] = 'Home_web/webMobile';
$route['voucherMobile/:any/:any/:any/:any/:any/:any'] = 'Home_web/voucherMobile';
$route['passwordMobile/:any/:any'] = 'Home_web/passwordMobile';
$route['getUserAgentDetail/:any/:any'] = 'Home_web/getUserAgentDetail';
$route['payment-success/(:any)/(:any)/(:any)'] = 'Home_web/esuccess_payment/$1/$2/$3'; //Success Payment Page
$route['payment-failed/(:any)'] = 'Home_web/failed_transaction/$1'; //Failure Payment Page
/* ----------------Can't Implement constant on below routes-------------------- */

$route["egift_history"] = "Home_web/egift_history"; //Not seen in Home_web
$route["greeting_history"] = "Home_web/display_greeting_history"; //Not seen in Home_web
$route["terms-of-use"] = "Home_web/terms_n_conditions"; //Not in use
$route["shipping-policy"] = "Home_web/shipping_policy_flower"; //Not in use
$route["refund-policy"] = "Home_web/refund_policy"; //Not in use

/* ----------------Web View Routes-------------------- */

/* ----------------Admin View Routes-------------------- */
$route["show_vouchers"] = "Home_web/show_vouchers";
$route["owl"] = "Admin_panel/admin_login";
/* ----------------Admin View Routes-------------------- */


/* ----------------Web API Routes-------------------- */
//Authentication
$route['registration'] = 'Home_web/registration_success'; //Registration
$route['login/(:num)'] = 'Home_web/login_success/$1'; //Login
//Forgot Password
$route['forgot-email'] = 'Home_web/forgot_success'; // Forgot Password Email
$route['forgot-mobile'] = 'Home_web/send_otp'; // Forgot Password Mobile
$route['change-password'] = 'Home_web/changePassword'; // Change Password
//User Profile
$route['update_profile'] = 'Home_web/update_profile_success1'; // Update Profile
$route['upload'] = 'Home_web/upload_img'; // Profile Image Upload
$route['remove-pic'] = 'Home_web/update_remove_pic'; // Remove Image Upload
//Reminder
$route['add-reminder'] = 'Home_web/reminder_success'; // Add/Update Reminder
$route['delete-reminder'] = 'Home_web/delete_reminder'; // Delete Reminder

$route['logout'] = 'Home_web/logout'; // Logout
//OTP
$route['resend-otp'] = 'Home_web/resend_otp'; // Resend OTP for verification on Signup & Continue as Guest & Update Profile also
$route['verify-otp-signup'] = 'Home_web/verify_otp_signup'; // Verify OTP for Signup & Continue as Guest& Update Profile also
$route['verify-otp-reset'] = 'Home_web/verify_otp'; // Verify OTP for Reset Password
//Cart
$route['cart-count'] = 'Home_web/get_cart_count'; // Get Cart Count & Data too
$route['cart-view'] = 'Home_web/get_view_cart'; // Get Cart Data

$route['search-data-flower-voucher'] = 'Home_web/getVoucherAndFlower'; // Get Flower & Voucher names for searching products
$route['newsletter'] = 'Home_web/newsletter_success'; // Newsletter add
//Free Greeting Card
$route['send-egreeting'] = 'Home_web/send_egreeting'; // Send free greeting card
$route['send-egreeting-new'] = 'Home_web/send_egreeting_new';

$route['random-product'] = 'Home_web/get_random_product'; // Random Vouchers & Free Greeting Cards on Dashboard & View Reminder
$route['random-product-new'] = 'Home_web/get_random_product_new'; // Random Flowers & Cakes & Free Greeting Cards on Dashboard & View Reminder

$route['unsubscribe'] = 'Home_web/unsubscribe'; // Unsubscribe

$route['verify-zip'] = 'Home_web/validate_pincode'; // Validating Pin code & City from zip_code table for flowers
//Zodiac
$route['zodiac-data'] = 'Home_web/get_zodiac'; // Get Zodiac Value
$route['zodiac-info'] = 'Home_web/get_zodiac_info'; // Get Zodiac Info
//e-Gift Cards
$route['voucher-data'] = 'Home_web/get_voucher_data'; // Get Voucher Data
$route['voucher-cart'] = 'Home_web/send_voucher_to_cart'; // Add Voucher Data to Cart
$route['voucher-update-cart'] = 'Home_web/update_voucher_data'; // Update Voucher Data to Cart
$route['voucher-delete'] = 'Home_web/delete_voucher_cart'; // Delete Voucher Data from Cart
$route['voucher-qty'] = 'Home_web/update_voucher_cart'; // Increase Decrease Voucher Quantity
$route['resend-voucher'] = 'Home_web/resend_woohoo_api'; // Resend Voucher
//Flowers
$route['flower-category'] = 'Home_web/getFlowerCategoryWiseData'; // Flower Data Category Wise
$route['flower-price'] = 'Home_web/getFlowerPriceWiseData'; // Flower Data Price Wise
$route['flower-cart/(:any)'] = 'Home_web/send_flower_to_cart/$1'; // Add/Update Flower Data to Cart
$route['flower-delete'] = 'Home_web/delete_flower_cart'; // Delete Flower Data from Cart
$route['flower-qty'] = 'Home_web/update_flower_cart'; // Increase Decrease Flower Quantity
//Payment
$route['success'] = 'Home_web/thanx_for_purchase'; // Success Payment
$route['failure'] = 'Home_web/payment_failure'; // Failure Payment

$route['demo-birthday'] = 'Home_web/get_birthday_data'; //Not in use
$route['greetings-by-limit'] = 'Home_web/getGreetingsByLimit'; //Get greetings by limit Not in use

//for deep linking of Birthday Reminders.
$route['link-birthday-reminder'] = 'Home_web/deeplinkBirthdayReminderDeviceAndWeb';

/* ----------------Web API Routes-------------------- */

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
