<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Birthday_common extends CI_Model {

    function getUserData($tablename, $where) {
        $this->db->where($where);
        $val = $this->db->get($tablename);


        return $val;
    }

    function getUserData1($tablename, $where) {
        $this->db->where($where);
        $val = $this->db->get($tablename);
        if ($val->result_array()) {
            return true;
        }
    }

    function getCountryName($tableName, $country_id) {
        $query = $this->db->query("select CountryName from $tableName where CntyId='$country_id'");
        return $query;
    }

    function addData($tablename = "", $data = array()) {
        $this->db->insert($tablename, $data);
        return $this->db->insert_id();
    }

    function update($tablename = "", $data = array(), $where = "") {
        $this->db->where("$where", null, false);
        return $this->db->update($tablename, $data);
    }

    function add_image($data) {
        $this->db->query('Insert into IndividualRegister(FileName)');
    }

    public function create_medium_image($source_folder, $image_name) {
        $source_path = $_SERVER["DOCUMENT_ROOT"] . $source_folder . "/" . $image_name;
        $target_path = $_SERVER["DOCUMENT_ROOT"] . $source_folder;
        if (!file_exists($target_path)) {
            mkdir($target_path);
        }
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => 200,
            'height' => 200
        );
        //$this->load->library('image_lib');
        $this->image_lib->initialize($config_manip);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
        // clear //
        $this->image_lib->clear();
    }

    function getIndividualAccountDetails($m_id) {
        $this->db->select("m_id, m_email, m_username, m_country,m_password, m_city, m_phone, Occupation, Love, CountryOpt, RealName, CreationDate, "
                 . " Sex	, delstatus, bday, bmonth, byear");
        $this->db->from("dbo.IndividualRegister");
        $this->db->where("m_id", "$m_id");
        $retval = $this->db->get()->result_array();
        //echo $this->db->last_query();
        $output["m_details"] = $retval;
        if (count($retval) !== 0) {
            return $output;
        }
    }

    function getAllData($tablename = "") {
        $val = $this->db->get($tablename);
        //echo $this->db->last_query();
        return $val;
    }

    function getZodiac_name($date, $month) {
        $FDate = '"FDate"';
        $FMonth = '"FMonth"';
        $TDate = '"TDate"';
        $TMonth = '"TMonth"';
        $query = $this->db->query("Select * from dbo.TblMapZodiac where ($FDate <=$date and $FMonth=$month) or ($TDate>=$date and $TMonth =$month)");
        return $query;
    }

    function getRegistration_id() {
        
    }

    function sendPushMessages($user_id, $message, $type = "1") {
        // $registration_details = $this->common->getData("users_device_registration", "user_id = $user_id")->result_array();
        if (count($registration_details) == 1) {
            $msg = array
                (
                'message' => $message, //requires
                'title' => 'This is a title. title', //required
                'subtitle' => 'This is a subtitle. subtitle', //required
                'tickerText' => 'Ticker text here...Ticker text here...Ticker text here', //required
                'link' => "", //required
                'vibrate' => 1, //required
                'sound' => 1, //required
                'type' => $type  //custom
            );

            $fields = array
                (
                'registration_id' => $registration_details["registration_id"],
                'data' => $msg
            );
            //print_r($fields);
            $headers = array
                (
                'Authorization: key= AIzaSyCM7FIhRtkrBDF0DcA_Yawt53BO7rIFogw',
                'Content-Type: application/json'
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
        }
    }

}
