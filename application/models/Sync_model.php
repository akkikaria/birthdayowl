<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Sync_Model extends CI_Model {

    public function getUser($id) {
        $this->db->where("user_id = $id and status = 1", null, false);
        $val = $this->db->get('users');

        return $val;
    }

    function update($tablename = "", $data = array(), $where = "") {
        $this->db->where("$where", null, false);
        return $this->db->update($tablename, $data);
    }

    function getData($tablename = "", $where = "") {
        $this->db->where("$where", null, FALSE);
        $val = $this->db->get($tablename);
        //cho $this->db->last_query();
        return $val;
    }

    function getAllData($tablename = "") {
        $val = $this->db->get($tablename);
        //echo $this->db->last_query();
        return $val;
    }

    function addData($tablename = "", $data = array()) {
        $this->db->insert($tablename, $data);
        // echo $this->db->last_query();
        return $this->db->insert_id();
    }

    function checkLatestModified_date() {
        $query = "Select max(last_modified) as last_modified from users";
        $val = $this->db->query($query);
        return $val;
    }

    function checkLatestModifiedRelation_date() {
        $query = "Select max(last_modified) as last_modified from relationship";
        $val = $this->db->query($query);
        return $val;
    }

    function checkLatestModifiedReminder_date($user_id) {
        $query = "Select max(updated_date) as last_modified from bday_reminder where user_id='$user_id'";
        $val = $this->db->query($query);
        return $val;
    }

    function sendSyncedData($server_date = "") {
        $this->db->select("name, phone_no, user_id as server_id, '0' as status, last_modified");
        $this->db->from("users");
        $this->db->where("status", "1");
        if ($server_date != "") {
            $this->db->where("last_modified > '$server_date'", null, FALSE);
        }
        $val = $this->db->get();
        return $val;
    }

    function sendSyncedDataRelation($server_date = "") {                        //ok
        $query = "SELECT relation_name, '0' as status, relation_id as server_id, last_modified FROM (relationship) WHERE status =  '1'";
        $val = $this->db->query($query);
        return $val;
    }

    function sendSyncedDataReminder($user_id, $server_date = "") {              //ok
        $query = "SELECT user_id, first_name,plus_id, last_name, bdate, bmonth, zodiac_id, mobile_no, reminder_created_date,country_id, updated_date as last_modified, email, relation_id, gender, priority, '0' as status, rem_image_name,reminder_id as server_id FROM (bday_reminder) WHERE status ='1' AND user_id='$user_id' order by bdate ASC";
        if ($server_date != "") {
            $query .= " AND updated_date > '$server_date'";
        }
        $val = $this->db->query($query);
        return $val;
    }

}
