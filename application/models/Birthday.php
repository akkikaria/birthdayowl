<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Birthday extends CI_Model {

    function getUserData($tablename, $where) {
        $this->db->where("$where", null, FALSE);
        $val = $this->db->get($tablename);
        return $val;
    }

    function getFlowerCategory($tablename, $where) {
        $this->db->where("$where", null, FALSE);
        $val = $this->db->get($tablename);
        return $val;
    }

    function getUserDataOrderBy($tablename, $where, $orderby) {
        $this->db->where("$where", null, FALSE);
        $this->db->order_by($orderby, "DESC");
        $val = $this->db->get($tablename);
        return $val;
    }

    function addAData($tablename = "", $data = array()) {
        $this->db->insert_batch($tablename, $data);
        return $this->db->insert_id();
    }

    function get_voucherdata($tablename, $where) {
        $query = $this->db->query("select *,trim(redemption_details)as redem ,trim(locations)as location,trim(terms_conditions)as terms from $tablename where $where");
        return $query;
    }

    function getSelectData($select, $tablename, $where) {
        $this->db->select($select);
        $this->db->from($tablename);
        $this->db->where($where, null, false);
        return $this->db->get();
    }

    function demo($tablename, $where) {
        try {
            if (mysqli_connect_error()) {
                throw new Exception(mysqli_connect_error());
            }
            $this->db->where("$where", null, FALSE);
            $result = $this->db->get($tablename);

            if (!$result) {
                $error = $this->db->error(); // Has keys 'code' and 'message'
                throw new Exception($error["message"]);
                return 0;
            }
//            echo '<pre>';
//            print_r($result);
//            exit;

            return $result;
        } catch (Exception $e) {
//            echo "<pre>";
//             print_r($e);
//             exit;

            return NULL;
        }
    }

    function getSelectDataOrderBy($select, $tablename, $orderby) {
        $this->db->select($select);
        $this->db->from($tablename);
        $this->db->order_by($orderby, "desc");
        return $this->db->get();
    }

    function getSelectDataOrderBy1() {
        $query = $this->db->query("select pro_id,product_name from product_new where status = 'Active'");
        return $query;
    }

    function getCountryName($tableName, $country_id) {
        $query = $this->db->query("select CountryName from $tableName where CntyId='$country_id'");
        return $query;
    }

    function encryptPassword($password) {
        return base64_encode($password);
    }

    function decryptPassword($password) {
        return base64_decode($password);
    }

    function get_zodiac_info($sign_day, $sign_month) {
        $query = $this->db->query("Select sign_id,sign_day,sign_month,sign_data,zodiac_sign , MONTHNAME(STR_TO_DATE($sign_month, '%m'))as month_name from zodiac_signs left join mapzodiac on zodiac_signs.zodiac_id=mapzodiac.zodiac_id where zodiac_signs.sign_day='$sign_day 'and zodiac_signs.sign_month='$sign_month' ");
        return $query;
    }

    function get_card_data($card_id) {
        $query = $this->db->query("Select * from greeting_card c join greeting_category m on c.greet_cat_id=m.greet_cat_id where card_id='$card_id'");
        return $query;
    }

    function getCategoryMenuData() {
        $query = $this->db->query("Select * from categories c join menu m on c.menu_id=m.menu_id");
        return $query;
    }

    function get_cards($card_id) {
        $query = $this->db->query("Select * from greeting_card where card_id IN($card_id)");
        return $query;
    }

    function getCategoryMenu() {
        $query = $this->db->query("Select distinct(m.menu_name),c.menu_id from categories c join menu m on c.menu_id=m.menu_id");
        return $query;
    }

    function getSubcategoryMenuData() {
        $query = $this->db->query("select cat1.category_id as subcategory_id,cat1.category_name as subcategory_name,cat1.description as subcategory_description, cat2.*,menu.* from categories as cat1 join categories as cat2 on cat1.is_Subcategory=cat2.category_id join menu on cat2.menu_id=menu.menu_id");
        return $query;
    }

    function getSubcategoryNameMenuData($menu_id) {
        $query = $this->db->query("select cat1.category_id as subcategory_id,cat1.category_name as subcategory_name,cat1.description as subcategory_description, cat2.*,menu.* from categories as cat1 join categories as cat2 on cat1.is_Subcategory=cat2.category_id join menu on cat2.menu_id=menu.menu_id where cat2.menu_id='$menu_id'");
        return $query;
    }

    function get_categorywise_data() {
        $query = $this->db->query("select * from product_new as p join categories as c on p.category_id=c.category_id order by pro_id desc");
        return $query;
    }

    function get_subcategorywise_data() {
        $query = $this->db->query("select p.product_name,p.*,c.category_name as sub_category,p.subcategory_id,p.category_id,c1.category_name  from product_new as p join categories as c on p.subcategory_id=c.category_id join categories as c1 on p.category_id=c1.category_id");
        return $query;
    }

    function GetgreetingCardCatgegory() {
        $query = $this->db->query("Select * from greeting_card g join greeting_category m on g.greet_cat_id=m.greet_cat_id");
        return $query;
    }

    function do_truncate($tablename) {

        $this->db->truncate($tablename);
    }

    function getBdayInfo($user_id, $bmonth = NULL, $bdate = NULL) {

        $this->db->select("bday_reminder.bmonth,bday_reminder.bdate");
        $this->db->from("bday_reminder");
        $this->db->where("user_id", $user_id);
        if ($bmonth != NULL) {
            $this->db->where("bday_reminder.bmonth", $bmonth);
        }
        if ($bdate != NULL) {
            $this->db->where("bday_reminder.bdate", $bdate);
        }
        $val = $this->db->get();
        return $val;
    }

    function get_selected_data($user_id) {
        $query = $this->db->query("select reminder_id,zodiac_sign, gender,b.zodiac_id ,relation_name,first_name,last_name,bdate,bmonth from   mapzodiac as z left join bday_reminder as b on b.zodiac_id=z.zodiac_id left join relationship as o on o.relation_id=b.relation_id where  b.user_id='$user_id'");
        return $query;
    }

    function getSelectedTwoTableJoin($select, $tablename1, $tablename2, $where, $on) {
        $query = $this->db->query(" select $select from $tablename1 join $tablename2 on $on where $where");
        return $query;
    }

    function addData($tablename = "", $data = array()) {
        $this->db->insert($tablename, $data);
        return $this->db->insert_id();
    }

    function update($tablename = "", $data = array(), $where = "") {
        $this->db->where("$where", null, false);
        return $this->db->update($tablename, $data);
    }

    public function create_medium_image($source_folder, $image_name) {
        $source_path = $_SERVER["DOCUMENT_ROOT"] . $source_folder . "/" . $image_name;
        $target_path = $_SERVER["DOCUMENT_ROOT"] . $source_folder;
        if (!file_exists($target_path)) {
            mkdir($target_path);
        }
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => 200,
            'height' => 200
        );
        $this->image_lib->initialize($config_manip);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
        // clear //
        $this->image_lib->clear();
    }

    function getIndividualAccountDetails($m_id) {
        $this->db->select("m_id, m_email, m_username, m_country,m_password, m_city, m_phone, Occupation, Love, CountryOpt, RealName, CreationDate, "
                . " Sex	, delstatus, bday, bmonth, byear");
        $this->db->from("dbo.IndividualRegister");
        $this->db->where("m_id", "$m_id");
        $retval = $this->db->get()->result_array();
        $output["m_details"] = $retval;
        if (count($retval) !== 0) {
            return $output;
        }
    }

    function getAllData($tablename = "") {
        $val = $this->db->get($tablename);
        return $val;
    }

    function getLimitedData($tablename = "") {
        $val = $this->db->query("Select * from $tablename limit 4");
        return $val;
    }

    function getMin_price($tablename = "") {
        $val = $this->db->query("SELECT MIN(price) as min_price FROM `product_new` ORDER BY `pro_id` ASC ");
        return $val;
    }

    function getMax_price($tablename = "") {
        $val = $this->db->query("SELECT MAX(price) as max_price FROM `product_new` ORDER BY `pro_id` ASC ");
        return $val;
    }

    function getLimitedData_new($tablename = "") {
        $val = $this->db->query("SELECT * FROM `product_new` where status = 'Active' ORDER BY RAND()");
        return $val;
    }

    function getMostViewedDataWithOrderBy($tablename = "") {
        $val = $this->db->query("select * from product_new where status = 'Active' ORDER BY `product_new`.`most_viewed` DESC limit 10");
        return $val;
    }

    function getBestSellingDataWithOrderBy($tablename = "") {
        $val = $this->db->query("select * from product_new where status = 'Active' ORDER BY RAND() LIMIT 10");
        return $val;
    }

    function get_user_devices($tablename) {
        $query = $this->db->query("Select distinct device_registration,platform from $tablename");
        return $query;
    }

    function getZodiac_name($date, $month) {
        $query = $this->db->query("Select * from mapzodiac where (fdate <='$date' and fmonth='$month') or (tdate>='$date' and tmonth ='$month')");
        return $query;
    }

    function delete_reminder($tablename = "", $where = "") {
        $this->db->where("$where", null, false);
        $this->db->delete($tablename);
    }

    function get_refer_data() {
        $query = $this->db->query("Select users.user_id,users.username ,refer_friend.* from users join refer_friend on users.user_id=refer_friend.user_id");
        return $query;
    }

    function send_bday_reminder() {
        $current_month = date("0000-m-d");
        $bday_data = $this->db->query("select user_id,bdate,bmonth,first_name,last_name from bday_reminder where DATEDIFF(birth_date,'$current_month')=priority")->result_array();
        if (count($bday_data) > 0) {
            foreach ($bday_data as $data) {
                $user_id = $data["user_id"];
                $bdate = $data["bdate"];
                $bmonth = $data["bmonth"];
                $firstname = $data["first_name"];
                $lastname = $data["last_name"];
                $year = date("Y");
                $message = "$firstname $lastname birthday is on $bdate/$bmonth/$year ";
                $this->sendNotification($user_id, $message, "1");
            }
        }
    }

    public function send_sms($mobileno, $showmessage) {
        if (SERVER_TYPE == 3) {
            return 1;
        } else {
            $username = "abdulkmst";
            $password = "kader12";
            $type = "TEXT";
            $sender = "BDYOWL";
            $mobile = "$mobileno";
            $message = urlencode("$showmessage");
            $baseurl = "https://app.indiasms.com/sendsms/bulksms";
            $url = $baseurl . "?username=" . $username . "&password=" . $password . "&type=" . $type . "&sender=" . $sender . "&mobile=" . $mobile . "&message=" . $message;
            $return = file($url);
            $result = explode("|", $return[0]);

            if (trim($result[0]) == "SUBMIT_SUCCESS") {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function send_mobile_otp($mobileno, $showmessage) {
        if (SERVER_TYPE == 3) {
            return 1;
        } else {
            $username = "abdulkotp";
            $password = "kmst123";
            $type = "TEXT";
            $sender = "BDYOWL";
            $mobile = "$mobileno";
            $message = urlencode("$showmessage");
            $baseurl = "https://app.indiasms.com/sendsms/bulksms";
            $url = $baseurl . "?username=" . $username . "&password=" . $password . "&type=" . $type . "&sender=" . $sender . "&mobile=" . $mobile . "&message=" . $message;
            $return = file($url);
            $result = explode("|", $return[0]);

            if (trim($result[0]) == "SUBMIT_SUCCESS") {
                return 1;
            } else {
                return 0;
            }
        }
    }

//    public function smsDemo($mobileno, $showmessage) {
//
//        $username = "abdulkmst";
//        $password = "RTZaVQ";
//        $type = "TEXT";
//        $sender = "BDYOWL";
//        $mobile = "$mobileno";
//        $message = urlencode("$showmessage");
//        $baseurl = "https://app.indiasms.com/sendsms/bulksms";
//        $url = $baseurl . "?username=" . $username . "&password=" . $password . "&type=" . $type . "&sender=" . $sender . "&mobile=" . $mobile . "&message=" . $message;
////        $return = file($url);
////        $result = explode("|", $return[0]);
////        $data = array();
////        if (trim($result[0]) == "SUBMIT_SUCCESS") {
////            return 1;
////        } else {
////            return 0;
////        }
//        $ch = curl_init();
//        //  $url = $authorizationUrl . $requestToken['oauth_token'] . '&username=' . $username . '&password=' . $password;
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_POST, 0); //1 for a post request
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
//        curl_setopt($ch, CURLOPT_USERAGENT, "sms");
//        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
//        $verifierResponse = curl_exec($ch);
////        print_r( curl_getinfo($ch));
////        print_r($verifierResponse);
////        exit;
//        curl_close($ch);
//    }

    function send_previousDayBdayReminder() {
        $current_month = date("0000-m-d");
        $bday_data = $this->db->query("select * from bday_reminder where DATEDIFF(birth_date,'$current_month')= 1")->result_array();
        if (count($bday_data) > 0) {
            foreach ($bday_data as $data) {
                $user_id = $data["user_id"];
                $bdate = $data["bdate"];
                $bmonth = $data["bmonth"];
                $firstname = $data["first_name"];
                $lastname = $data["last_name"];
                $year = date("Y");
                $message = "$firstname $lastname birthday is tommorrow";
                $this->sendNotification($user_id, $message, "1");
            }
        }
    }

    function send_todayBday() {
        $current_month = date("0000-m-d");
        $bday_data = $this->db->query("select * from bday_reminder where birth_date='$current_month'")->result_array();
        if (count($bday_data) > 0) {
            foreach ($bday_data as $data) {
                $user_id = $data["user_id"];
                $bdate = $data["bdate"];
                $bmonth = $data["bmonth"];
                $firstname = $data["first_name"];
                $lastname = $data["last_name"];
                $year = date("Y");
                $message = "Today is $firstname $lastname's birthday";
                $this->sendNotification($user_id, $message, "1");
            }
        }
    }

    function sendCommonBdayReminder() {
        require_once(APPPATH . "libraries/Mailin.php" );
        //  date_default_timezone_set('Asia/Kolkata');
        $current_month = date("0000-m-d");
        $bday_data = $this->db->query("select user_id,bdate,bmonth,first_name,last_name,reminder_id,birth_date,DATEDIFF(birth_date,'$current_month') as days_diff from bday_reminder where (DATEDIFF(birth_date,'$current_month')=priority) or (DATEDIFF(birth_date,'$current_month')= 1) or (birth_date='$current_month') ")->result_array();

        if (count($bday_data) > 0) {
            foreach ($bday_data as $data) {
                $user_id = $data["user_id"];
                $bdate = $data["bdate"];
                $bmonth = $data["bmonth"];
                $firstname = $data["first_name"];
                $lastname = $data["last_name"];
                $year = date("Y");
                $bdata["name"] = $name = '"' . $firstname . " " . $lastname . '"';
                $reminder_id = $data["reminder_id"];
                $birth_date = $data["birth_date"];
                $current_year = date("Y");
                $new_date = date("dS F $current_year", strtotime($birth_date));
                $bdata["bdate"] = $new_date;
                //send sms
                $cmonth = date("$bmonth");
                $mname = date("Y-$cmonth-d");
                $name = strtotime($mname);
                $mn = date("F", $name);
                $email_info = $this->getSelectData("email,first_name,mobile_no,sent_greeting_counter,membership_status", "users", "user_id='$user_id'")->row_array();
                $username = $email_info["first_name"];
                $email = $email_info["email"];
                $mobile_no = $email_info["mobile_no"];
                $status = $email_info["membership_status"];
                if ($data["days_diff"] == 0) {
                    $bdata["message"] = "$firstname's birthday is Today";
                    $bdata["suffix"] = "i.e";
                    $show_message = "Hello $username, \n Birthday Owl reminds you that today is $firstname's birthday. \nLog in www.birthdayowl.com now to send your wishes to $firstname and view other upcoming birthdays \n-Birthday Owl";
                } else if ($data["days_diff"] == 1) {
                    $bdata["message"] = "$firstname's birthday will be Tomorrow";
                    $bdata["suffix"] = "i.e";
                    $show_message = "Hello $username, \n Birthday Owl reminds you that tomorrow will be $firstname's birthday. \nLog in www.birthdayowl.com now to send your wishes to $firstname and view other upcoming birthdays \n-Birthday Owl";
                } else {
                    $current_month = date("$bmonth");
                    $month_name = date("Y-$current_month-d");
                    $name = strtotime($month_name);
                    $m = date("F", $name);
                    $bdata["message"] = "$firstname's birthday is coming up ";
                    $bdata["suffix"] = "on";
                    $show_message = "Hello $username, \n Birthday Owl reminds you of upcoming birthday of $firstname on $bdate, $mn \nLog in www.birthdayowl.com now to send your wishes to $firstname and view other upcoming birthdays \n-Birthday Owl";
                }

                if ($status == 1) {
                    $this->send_sms($mobile_no, $show_message);
                }

                //send notification
                $this->sendNotification($user_id, $bdata["message"], "1");
                //send email
                $mailin = new Mailin('https://api.sendinblue.com/v2.0', SENDINKEY);
//                $view = $this->load->view("email_template/reminder_success", $bdata, true);
                $view = $this->load->view("email_template/Birthday_reminder_new", $bdata, true);
                $sdata = array("to" => array("$email" => "$username"),
                    "from" => array("birthdayreminder@birthdayowl.com", "birthdayowl.com"),
                    "subject" => "Birthday Reminder From www.birthdayowl.com",
                    "text" => "$view",
                    "html" => "$view"
                );
                $resp = $mailin->send_email($sdata);
                if ($resp["code"] == "success") {
                    $updated_data["email_status"] = 1;
                    $where = "reminder_id='$reminder_id'";
                    $this->Birthday->update("bday_reminder", $updated_data, $where);
                } else {
                    $updated_data["email_status"] = 0;
                    $where = "reminder_id='$reminder_id'";
                    $this->Birthday->update("bday_reminder", $updated_data, $where);
                }
            }
        }
    }

    function sendEGreeting() {
        //date_default_timezone_set('Asia/Kolkata');
        $today = date("Y-m-d");
        $gift_vouchers = $this->db->query("Select * from  free_greetings as f join users as u on f.user_id=u.user_id  where (f.greeting_schedule=1 and f.delivery_date='$today' and (f.is_sent='0'or f.is_sent='2')) or (f.greeting_schedule=0 and f.is_sent='0' )");
        return $gift_vouchers;
    }

    function deactivate_resetpwd_link() {
        $gift_vouchers = $this->db->query("select * from users where reset_time >= now() + INTERVAL 1 DAY and reset_activate=1")->result_array();
        if (count($gift_vouchers) > 0) {
            foreach ($gift_vouchers as $gifts) {
                $user_id = $gifts['user_id'];
                $reset["reset_activate"] = 0;
                $this->Birthday->update("users", $reset, "user_id = $user_id");
            }
        }
    }

    function sendGift($select) {

        return $this->db->query($select);
//
    }

    public function checkMembershipExpiry($user_id) {

        return $this->db->query("select *,TIMESTAMPDIFF(MINUTE,NOW(),expiry_date) as days_diff from greeting_membership where (TIMESTAMPDIFF(MINUTE,NOW(),expiry_date)=43200) || (TIMESTAMPDIFF(MINUTE,NOW(),expiry_date)< 43200) and user_id=$user_id");
    }

    public function change_membership_status() {
        $query = $this->db->query("SELECT * from greeting_membership as gm join users u on gm.user_id=u.user_id where expiry_date=NOW() and u.membership_status='1'")->result_array();
        if (count($query) > 0) {
            foreach ($query as $expired) {
                $user_id = $expired["user_id"];
                $update["membership_status"] = 2;
                $where = "user_id='$user_id'";
                $this->Birthday->update("users", $update, $where);
            }
        }
    }

    public function check_membership_expiry() {
        require_once(APPPATH . "libraries/Mailin.php" );

        $expired_members = $this->db->query("SELECT *,DATEDIFF(expiry_date,NOW()) as remining_days FROM `greeting_membership` as gm join users as u on gm.user_id=u.user_id where u.membership_status='1'")->result_array();

        if (count($expired_members) > 0) {
            foreach ($expired_members as $members) {
                $days = $members["remining_days"];
                if (($days == 30) || ($days == 10) || ($days == 1)) {
                    $nmailin = new Mailin('swatib@birthdayowl.com', SENDINKEY);
                    $data["uemail"] = $uemail = $members["email"];
                    $data["uname"] = $uname = $members["first_name"];
                    $nbody = $this->load->view("email_template/renew_membership", $data, true);
                    $nmailin->
                            addTo("$uemail", "$uname")->
                            setFrom('greetings@birthdayowl.com', 'birthdayowl.com')->
                            setReplyTo("info@birthdayowl.com")->
                            setSubject('birthdayowl.com :e-Greeting Card Membership Plan')->
                            setText($nbody)->
                            setHtml($nbody);
                    $nmailin->send();
                }
            }
        }
    }

    function sendNotification($user_id, $message, $type = "1") {



        if ($type == 1) { //send notification for birthday reminders
            $registration_details = $this->getUserData("notify_device_registartion", "user_id = $user_id")->result_array();
        } else if ($type == 2) { // send notfication for broadcast messages/marquee
            $registration_details = $this->Birthday->getAllData("notify_device_registartion")->result_array();
        } else if ($type == 3) {
            $registration_details = $this->getUserData("notify_device_registartion", "user_id = $user_id")->result_array();
        }
        $search = array();
        $search["user_id"] = $user_id;
        /* $messages = $this->model_message->getMessage($search, 0, 0);
          if ($messages->num_rows() > 0) {
          $unread_messages = $messages->row()->unread_count;
          } */
        //$unread_store_messages = $this->common->getUnreadStoreMessageCount($user_id);

        for ($i = 0; $i < count($registration_details); $i++) {
            if ($registration_details[$i]["platform"] == 1) { //change this to 1 when ios notifcation is done
                // send ios notification
                // Put your device token here (without spaces):
                $deviceToken = $registration_details[$i]["device_registration"];
                //  $deviceToken="15364d888a5593b7ec8384eee7ead588a3920327e3c08f8f56c54a5d5803c54f";
                //  $deviceToken="042793e2511b1aff139ab33ef0f728a05dd8ff6740104fe1b8ee378bbba0108b";
                // Put your private key's passphrase here:
                $passphrase = 'Tech1mini';
                //echo "Hello";
                // Put your alert message here:
                //$message = 'My first push notification!';
                ////////////////////////////////////////////////////////////////////////////////

                $ctx = stream_context_create();
                //production
                stream_context_set_option($ctx, 'ssl', 'local_cert', 'pushNotifictnProdnBirthdayMattersCertificate.pem');
                //development
//                stream_context_set_option($ctx, 'ssl', 'local_cert', 'BMCertificates.pem');
//                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
                // Open a connection to the APNS server
////                //production
                $fp = stream_socket_client(
                        'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
//                //Development
//                $fp = stream_socket_client(
//                         'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
//
//                if (!$fp) {
//                    //  exit("Failed to connect: $err $errstr" . PHP_EOL);
//                }
                //echo 'Connected to APNS' . PHP_EOL;
                // Create the payload body

                $body['aps'] = array(
                    'alert' => $message,
                    'sound' => 'default',
                    'badge' => "1",
                    'type' => $type,
                    'user_id' => $user_id
                        /* 'alert' => "",
                          "badge" => "0",
                          "content-available" => "1",
                          "sound" => "" */
                );


                // Encode the payload as JSON
                $payload = json_encode($body);

                // Build the binary notification
                $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

                // Send it to the server
                $result = fwrite($fp, $msg, strlen($msg));

                // if (!$result)
//                 echo 'Message not delivered' . PHP_EOL;
//                  else
//                 echo 'Message successfully delivered' . PHP_EOL;
                // Close the connection to the server
                fclose($fp);
            } else if ($registration_details[$i]["platform"] == 0) {
                // send android notification
                //print_r($registration_details);
                $registrationIds = array();
                $registrationIds[0] = $registration_details[$i]["device_registration"];
                if ($type == 1) {
                    $msg = array
                        (
                        'message' => $message,
                        'title' => 'This is a title. title',
                        'subtitle' => 'This is a subtitle. subtitle',
                        'tickerText' => 'Ticker text here...Ticker text here...Ticker text here',
                        'link' => "",
                        'vibrate' => 1,
                        'sound' => 1,
                        'type' => $type,
                        'user_id' => $user_id
                    );
                } else if ($type == 2) {
                    $msg = array
                        (
                        'message' => $message,
                        'title' => 'This is a title. title',
                        'subtitle' => 'This is a subtitle. subtitle',
                        'tickerText' => 'Ticker text here...Ticker text here...Ticker text here',
                        'link' => "",
                        'vibrate' => 1,
                        'sound' => 1,
                        'type' => $type
                    );
                }

                $fields = array(
                    'registration_ids' => $registrationIds,
                    'data' => $msg
                );
//                AIzaSyAFuuJ69JjHjwiON1ObRa3WK3VP3yCC3mE
                // define("GOOGLE_API_KEY", "AIzaSyD7hbsl2Vg1B5DIpmBBSIJrFrTdtrpHOus");
                $headers = array
                    (
                    'Authorization: key=AIzaSyB_ncTp5oHLA1ooADuRmz0JJJtmTu-cWy8',
                    'Content-Type:application/json'
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                //  echo $result . "-" . $message . "-" . '\n';
                curl_close($ch);
            }
        }
    }

    function send_broadcast($message, $type = "2", $url) {
        $device_details = $this->get_user_devices("notify_device_registartion")->result_array();
        for ($i = 0; $i < count($device_details); $i++) {
            if ($device_details[$i]["platform"] == 1) { //change this to 1 when ios notifcation is done
                // send ios notification
                // Put your device token here (without spaces):
                $deviceToken = $device_details[$i]["device_registration"];
                // Put your private key's passphrase here:
                $passphrase = 'Tech1mini';

                // Put your alert message here:
                //$message = 'My first push notification!';
                ////////////////////////////////////////////////////////////////////////////////

                $ctx = stream_context_create();
                //production
                stream_context_set_option($ctx, 'ssl', 'local_cert', 'pushNotifictnProdnBirthdayMattersCertificate.pem');
                //development
//                stream_context_set_option($ctx, 'ssl', 'local_cert', 'BMCertificates.pem');
//
//                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
                // Open a connection to the APNS server
////                //production
                $fp = stream_socket_client(
                        'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
//                //Development
//                $fp = stream_socket_client(
//                         'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
//
//                if (!$fp) {
//                    //  exit("Failed to connect: $err $errstr" . PHP_EOL);
//                }
                //echo 'Connected to APNS' . PHP_EOL;
                // Create the payload body

                $body['aps'] = array(
                    'alert' => $message,
                    'sound' => 'default',
                    'badge' => "1",
                    'type' => $type,
                    'link' => $url,
                        /* 'alert' => "",
                          "badge" => "0",
                          "content-available" => "1",
                          "sound" => "" */
                );


                // Encode the payload as JSON
                $payload = json_encode($body);

                // Build the binary notification
                $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

                // Send it to the server
                $result = fwrite($fp, $msg, strlen($msg));

                // if (!$result)
                // echo 'Message not delivered' . PHP_EOL;
                //  else
                // echo 'Message successfully delivered' . PHP_EOL;
                // Close the connection to the server
                fclose($fp);
            } else if ($device_details[$i]["platform"] == 0) {
                // send android notification
                //print_r($registration_details);
                $registrationIds = array();
                $registrationIds[0] = $device_details[$i]["device_registration"];
                if ($type == 1) {
                    $msg = array
                        (
                        'message' => $message,
                        'title' => 'This is a title. title',
                        'subtitle' => 'This is a subtitle. subtitle',
                        'tickerText' => 'Ticker text here...Ticker text here...Ticker text here',
                        'link' => $url,
                        'vibrate' => 1,
                        'sound' => 1,
                        'type' => $type
                    );
                } else if ($type == 2) {
                    $msg = array
                        (
                        'message' => $message,
                        'title' => 'This is a title. title',
                        'subtitle' => 'This is a subtitle. subtitle',
                        'tickerText' => 'Ticker text here...Ticker text here...Ticker text here',
                        'link' => $url,
                        'vibrate' => 1,
                        'sound' => 1,
                        'type' => $type
                    );
                }

                $fields = array
                    (
                    'registration_ids' => $registrationIds,
                    'data' => $msg
                );
                // define("GOOGLE_API_KEY", "AIzaSyCM7FIhRtkrBDF0DcA_Yawt53BO7rIFogw");
                $headers = array
                    (
                    'Authorization: key=AIzaSyB_ncTp5oHLA1ooADuRmz0JJJtmTu-cWy8',
                    'Content-Type: application/json'
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                //echo '\n' . $result . "-" . $message . "-" . '\n';
                curl_close($ch);
            }
        }
    }

    function checkLength2($string) {
        if (strlen($string) == 1) {
            $str = "0" . $string;
            return $str;
        }
        return $string;
    }

    function get_price_range($minVal, $maxVal) {
        $query = $this->db->query("Select distinct(v.voucher_pro_id),v.* from voucher_product v  left join  amount a  on a.voucher_pro_id=v.voucher_pro_id where a.amount >= $minVal and a.amount <=$maxVal");
        return $query;
    }

    function get_price_range_login($user_id, $minVal, $maxVal) {
        $query = $this->db->query("SELECT vp.*,group_concat(a.amount),case when (select count(wish_id) from wish_list wl where user_id=$user_id and wl.voucher_pro_id=vp.voucher_pro_id) = 1  Then 1 else 0 end as count FROM  voucher_product vp left join amount a on a.voucher_pro_id=vp.voucher_pro_id where a.amount >=$minVal and a.amount <=$maxVal GROUP BY a.voucher_pro_id");
        return $query;
    }

    function get_brand_voucher_data() {
        $query = $this->db->query("Select * from brands a  left join voucher_product v  on a.voucher_pro_id=v.voucher_pro_id ");
        return $query;
    }

    function search_by_brands($brand_id) {
        $query = $this->db->query("SELECT * FROM `brands` b join voucher_product v on b.voucher_pro_id=v.voucher_pro_id  where b.brand_id IN($brand_id)");
        return $query;
    }

    function search_by_brands_wish($user_id, $brand_id) {
        $query = $this->db->query("SELECT *,case when (select count(wish_id) from wish_list wl where user_id=$user_id and wl.voucher_pro_id=vp.voucher_pro_id) = 1  Then 1 else 0 end as count FROM  voucher_product vp join `brands` b on b.voucher_pro_id=vp.voucher_pro_id where b.brand_id IN($brand_id)");

        return $query;
    }

    function search_by_brandName($brand_name) {
        $query = $this->db->query("SELECT * FROM `brands` b join voucher_product v on b.voucher_pro_id=v.voucher_pro_id  where b.brand_name Like '%$brand_name%'");
        return $query;
    }

    function getSearchProduct($search_product) {
        $query = $this->db->query("select * from voucher_product as vp join voucher_category as vc on vp.voucher_cat_id=vc.voucher_cat_id where voucher_pro_name LIKE '%$search_product%' or voucher_cat_name LIKE '%$search_product%'");
        return $query;
    }

    function search_by_brandName_wish($user_id, $brand_name) {
        $query = $this->db->query("SELECT *,case when (select count(wish_id) from wish_list wl where user_id=$user_id and wl.voucher_pro_id=vp.voucher_pro_id) = 1  Then 1 else 0 end as count FROM  voucher_product vp join `brands` b on b.voucher_pro_id=vp.voucher_pro_id where b.brand_name Like '%$brand_name%'");
        return $query;
    }

    function brand_search($brand_name, $brand_id) {
        $query = $this->db->query("SELECT * FROM `brands` b join voucher_product v on b.voucher_pro_id=v.voucher_pro_id  where b.brand_name Like '%$brand_name%' or b.brand_id IN($brand_id)");
        return $query;
    }

    function brand_search_wish($user_id, $brand_name, $brand_id) {
        $query = $this->db->query("SELECT *,case when (select count(wish_id) from wish_list wl where user_id=$user_id and wl.voucher_pro_id=vp.voucher_pro_id) = 1  Then 1 else 0 end as count FROM  voucher_product vp join `brands` b on b.voucher_pro_id=vp.voucher_pro_id where b.brand_name Like '%$brand_name%' or b.brand_id IN($brand_id)");
        return $query;
    }

    function update_banner_status($tablename = "", $data = array()) {
        return $this->db->update($tablename, $data);
    }

    function check_voucher_pro($user_id, $voucher_pro_id) {
        $query = $this->db->query("SELECT * from wish_list where user_id='$user_id' and voucher_pro_id='$voucher_pro_id'");
        return $query;
    }

    function getwishList($user_id, $limit, $offset) {
        $query = $this->db->query("SELECT *,case when (select count(wish_id) from wish_list wl where user_id=$user_id and wl.voucher_pro_id=vp.voucher_pro_id) = 1  Then 1 else 0 end as count FROM voucher_product vp  where vp.click_counter >5 limit $limit offset $offset ");
        return $query;
    }

    function show_vouchers($voucher_cat_id, $user_id, $limit, $offset) {
        $query = $this->db->query("SELECT *,case when (select count(wish_id) from wish_list wl where user_id=$user_id and wl.voucher_pro_id=vp.voucher_pro_id) = 1  Then 1 else 0 end as count FROM voucher_product vp  where vp.voucher_cat_id=$voucher_cat_id limit $limit offset $offset");
        return $query;
    }

    function truncate($tablename = "") {
        $query = $this->db->query("Truncate table  $tablename");
        return $query;
    }

    function get_order_history($user_id) {
        $query = $this->db->query('SELECT * FROM order_products op   join  greeting_card g on op.greeting_id=g.card_id  where op.user_id="' . $user_id . '" and op.delivery_status!="" order by op.order_pro_id DESC');
        return $query;
    }

    function Voucher_orderData($where) {
        $query = $this->db->query("SELECT * FROM `order_products` o left join voucher_product v on o.voucher_pro_id=v.voucher_pro_id left join greeting_card g on g.card_id=o.greeting_id where $where");
        return $query;
    }

    function Voucher_paymentData($where) {
        $query = $this->db->query("SELECT v.voucher_pro_id,v.voucher_pro_name,p.total_amount FROM `voucher_product` v left join payment p on v.voucher_pro_id=p.product_id where $where");
        return $query;
    }

    function getVoucher_paymentData($where) {
        $query = $this->db->query("SELECT v.voucher_pro_id,v.voucher_pro_name,p.* FROM `voucher_product` v left join payment p on v.voucher_pro_id=p.product_id where $where");
        return $query;
    }

    function getMinMaxVouchers($tablename) {
        $query = $this->db->query("Select Min(voucher_pro_id),Max(voucher_pro_id) from $tablename");
        return $query;
    }

    function getMinMaxGreetings($tablename) {
        $query = $this->db->query("Select Min(card_id),Max(card_id) from $tablename where greeting_type='1'");
        return $query;
    }

    function geRedmetion($tablename, $where) {
        $query = $this->db->query("Select redemption_details,locations,terms_conditions from $tablename where $where");
        return $query;
    }

    function getOrderVoucherData($user_id) {
        $query = $this->db->query("SELECT o.order_pro_id,o.voucher_pro_id,o.payment_id,o.order_id,o.voucher_pro_name,o.unique_oid,o.selected_orders,o.delievery_schedule,o.delivery_date_time,o.selected_amount,o.voucher_img,o.femail,o.fname,o.fphone,g.front_page_image from order_products o join  greeting_card g on g.card_id=o.greeting_id where o.user_id=$user_id and o.selected_orders=0 and (o.delivery_status='' or delivery_status=3) ORDER BY `o`.`order_pro_id` ASC ");
        return $query;
    }

    function getJoinedData2($select, $table, $joinedTable, $joinCond, $where) {
        $this->db->select($select);
        $this->db->from($table);
        $this->db->join($joinedTable, $joinCond);
        $this->db->where($where, null, FALSE);
        return $this->db->get();
    }

    function getJoinedDataByGroup($select, $table, $joinedTable, $joinCond, $where) {
        $this->db->select($select);
        $this->db->from($table);
        $this->db->join($joinedTable, $joinCond, "left");
        $this->db->where($where, null, FALSE);
        return $this->db->get();
    }

    function getGreetingData($id) {
        $query = $this->db->query("select * from free_greetings f left join users u on f.user_id=u.user_id where f.fid=$id");
        return $query;
    }

    function getJoinedDataThreetable($select, $table, $joinedTable, $joinedTable2, $joinCond, $joinCond2, $where, $type) {
        $this->db->select($select, FALSE);
        $this->db->from($table);
        $this->db->join($joinedTable, $joinCond, "$type");
        $this->db->join($joinedTable2, $joinCond2, "$type");
        $this->db->where($where, null, FALSE);
        return $this->db->get();
    }

    function getJoinDataGroupBy($select, $table, $joinedTable, $joinedTable2, $joinCond, $joinCond2, $where, $groupby, $orderby) {
        $this->db->select($select, FALSE);
        $this->db->from($table);
        $this->db->join($joinedTable, $joinCond, "left");
        $this->db->join($joinedTable2, $joinCond2, "left");
        $this->db->where($where, null, FALSE);
        $this->db->group_by($groupby);
        $this->db->order_by($orderby);
        return $this->db->get();
    }

    function getOrderVoucherDatabyGroup($user_id) {
        $query = $this->db->query("SELECT o.order_pro_id,o.voucher_pro_id,o.payment_id,o.same_quantity_id,o.quantity,o.order_id,o.voucher_pro_name,o.unique_oid,o.selected_orders,o.delievery_schedule,o.delivery_date_time,o.selected_amount,o.voucher_img,o.femail,o.fname,o.fphone,g.front_page_image ,sum(o.selected_amount) as selected_amount_new from order_products o join  greeting_card g on g.card_id=o.greeting_id where o.user_id=$user_id and o.selected_orders=0 and o.delivery_status='' or delivery_status=3 group by o.same_quantity_id ORDER BY `o`.`order_pro_id` ASC ");
        return $query;
    }

    function get_reminder_zodiac_Data($where) {
        $query = $this->db->query("select * from bday_reminder r join mapzodiac z on r.zodiac_id =z.zodiac_id left join relationship as o on o.relation_id=r.relation_id  where $where  ORDER BY STR_TO_DATE(birth_date,'%d/%m/%Y' ) ASC ");
        return $query;
    }

    function getAfterPayment($order_id) {
        $query = $this->db->query("Select * from payment p left join order_products o on o.order_id =p.order_id and o.selected_orders=1 left join voucher_product v on v.voucher_pro_id=p.product_id left join greeting_card g on g.card_id=o.greeting_id group by p.payment_id having p.order_id=$order_id and o.selected_orders=1 and p.transaction_status='Completed'");
        return $query;
    }

    function getGreetingDataVoucher($id) {
        $query = $this->db->query("select * from order_products o left join greeting_card g on o.greeting_id=g.card_id where o.order_pro_id=$id");
        return $query;
    }

    public function get_send_giftdata($payment_id, $greeting_id) {
        $query = $this->db->query("SELECT * FROM `payment`as p left join order_products as op on p.order_id=op.order_id  left join greeting_card as g on g.card_id=op.greeting_id  where  p.payment_id='$payment_id' and op.greeting_id='$greeting_id'");
        return $query;
    }

    public function get_voucher_amount() {
        $query = $this->db->query("SELECT * FROM `voucher_amount` as v left join voucher_product as vp on v.voucher_id=vp.voucher_pro_id");
        return $query;
    }

    public function get_distinct_data($tablename, $where, $params) {
        $query = $this->db->query("SELECT distinct($params) FROM $tablename where $where");
        return $query;
    }

    public function get_stock_vouchers($pamount, $voucher_pro_id) {
        $query = $this->db->query(" SELECT * ,( select count(voucher_id) from voucher_amount where status='1' and voucher_id='$voucher_pro_id' and pamount='$pamount') as availble_count ,( select count(voucher_id) from voucher_amount where status='2' and voucher_id='$voucher_pro_id'  and pamount='$pamount') as expired_count ,( select count(voucher_id) from voucher_amount where status='0' and voucher_id='$voucher_pro_id'  and pamount='$pamount') as purchased_count FROM `voucher_amount` as v left join voucher_product as vp on v.voucher_id=vp.voucher_pro_id
 where v.pamount='$pamount' and vp.voucher_pro_id='$voucher_pro_id'");
        return $query;
    }

    public function get_most_popularvouchers($pro_id) {
        $query = $this->db->query("SELECT  GROUP_CONCAT(distinct a.pamount)as amount ,v.* from woohoo_amount as a left join woohoo_vouchers as v on a.voucher_id=v.voucher_pro_id where  v.pro_id=$pro_id ");
        return $query;
    }

    public function get_voucher_detail($where) {
        $query = $this->db->query("SELECT  GROUP_CONCAT(distinct a.pamount)as amount ,v.* from woohoo_amount as a left join woohoo_vouchers as v on a.voucher_id=v.voucher_pro_id where  $where  group by voucher_pro_id");
        return $query;
    }

    public function VouchersByCategory($voucher_cat_id) {
        $query = $this->db->query("SELECT  GROUP_CONCAT(distinct a.pamount)as amount ,v.* from voucher_amount as a left join voucher_product as v on a.voucher_id=v.voucher_pro_id where  v.voucher_cat_id='$voucher_cat_id'  group by voucher_pro_id limit 10000 offset 0");
        return $query;
    }

    public function get_available_vouchers() {
        $query = $this->db->query("SELECT distinct a.voucher_id from voucher_amount as a left join voucher_product as v on a.voucher_id=v.voucher_pro_id where a.status='1' ");
        return $query;
    }

    function _get_voucher_cat_pro() {
        $query = $this->db->query("SELECT GROUP_CONCAT(distinct a.pamount)as amount ,v.* ,g.voucher_cat_name from voucher_amount as a left join voucher_product as v on a.voucher_id=v.voucher_pro_id left join voucher_category g on g.voucher_cat_id=v.voucher_cat_id group by v.voucher_pro_id ");
        return $query;
    }

    public function get_last_record() {
        $query = $this->db->query("SELECT position  FROM festival_banners ORDER BY fb_id DESC LIMIT 1");
        return $query;
    }

    public function getoutofstockvoucher($voucher_id) {
        $query = $this->db->query("select(va.pamount)as amount ,(va.id)as amt_id from woohoo_amount as va left join woohoo_vouchers as vp on va.voucher_id=vp.voucher_pro_id where vp.voucher_pro_id='$voucher_id' and va.status=1 group by va.pamount");
        return $query;
    }

    public function getorderData($orderid, $user_id) {
        $query = $this->db->query("SELECT * FROM  order_products as op  left join greeting_card as g on g.card_id=op.greeting_id left join payment p on p.payment_id=op.payment_id   where  op.order_id='$orderid' and op.user_id='$user_id'");

        return $query;
    }

    public function getorderDataFlower($orderid, $user_id) {
        $query = $this->db->query("SELECT * FROM  order_to_fnp as otf left join product_new as pr on pr.pro_id = otf.flower_purchased_id left join flower_payment fp on fp.payment_id=otf.payment_id_from_flower_payment where otf.order_id_from_orders='$orderid' and otf.flower_sender_id='$user_id'");

        return $query;
    }

    public function getorderProductData($orderid, $user_id) {
        $query = $this->db->query("SELECT * FROM  order_products as op  left join greeting_card as g on g.card_id=op.greeting_id  where  order_pro_id='$orderid' and user_id='$user_id'");
        return $query;
    }

    public function check_payment_done() {
//        $query = $this->db->query("SELECT * FROM `order_products`o left join greeting_card g on o.greeting_id=g.card_id left join payment p on p.payment_id=o.payment_id where o.delivery_status=1");
        $query = $this->db->query("SELECT * FROM `order_products`o left join greeting_card g on o.greeting_id=g.card_id left join payment p on p.payment_id=o.payment_id where (o.delivery_status=1 or o.delivery_status=2) and p.transaction_status = 'success' and p.payment_mode != '' order by order_pro_id desc");
        return $query;
    }

    public function getorderPaymentData($order_pro_id) {
        $query = $this->db->query("SELECT * FROM `order_products`o left join greeting_card g on o.greeting_id=g.card_id left join payment p on p.payment_id=o.payment_id where o.order_pro_id=$order_pro_id");
        return $query;
    }

    public function getorderPaymentFlowerData($flower_pro_id) {
        $query = $this->db->query("SELECT * FROM `order_to_fnp`o left join flower_payment p on p.payment_id=o.payment_id_from_flower_payment where o.flower_order_id=$flower_pro_id");
        return $query;
    }

    public function getAllOrderPayment() {
//        $query = $this->db->query("SELECT *,u.mobile_no FROM `order_products`o  join payment p on p.payment_id=o.payment_id join users u on u.user_id=o.user_id where o.delivery_status <> '' and o.delivery_status!=1 group by o.unique_oid  order by o.order_pro_id DESC ");
        $query = $this->db->query("SELECT *,u.mobile_no FROM `order_products`o  join payment p on p.payment_id=o.payment_id join users u on u.user_id=o.user_id where o.delivery_status <> '' and o.delivery_status!=1 group by o.payment_id order by o.order_pro_id DESC ");
        return $query;
    }

    public function getAllFlowerOrderPayment() {
        $query = $this->db->query("SELECT *,u.mobile_no FROM `order_to_fnp` o  join flower_payment p on p.payment_id=o.payment_id_from_flower_payment join users u on u.user_id=o.flower_sender_id order by o.flower_order_id DESC");
        return $query;
    }

    public function getIndividualOrderPayment($where) {
        $query = $this->db->query("SELECT *,u.mobile_no FROM `order_products`o  join payment p on p.payment_id=o.payment_id join users u on u.user_id=o.user_id where (o.delivery_status <> '' || o.delivery_status<> '10') and $where order by o.order_pro_id DESC");
        return $query;
    }

    public function getProcessingOrders() {
        $query = $this->db->query("SELECT * FROM `order_products`o  left join payment p on p.payment_id=o.payment_id where o.delivery_status=6");
        return $query;
    }

    public function getProcessingPaymentOrders($where) {
        $query = $this->db->query("SELECT * FROM `order_products`o  left join payment p on p.payment_id=o.payment_id where $where ");
        return $query;
    }

    public function getTwotableJoinData($table1, $table2, $select, $join_condition) {
        $query = $this->db->query("SELECT $select from $table1 t1 join $table2 t2 on $join_condition ");
        return $query;
    }

    function updateBatch($tablename = "", $data = array(), $where = "") {

        return $this->db->update_batch($tablename, $data, $where);
    }

    function pagination($limit, $adjacents, $rows, $page) {
        $pagination = '';
        if ($page == 0)
            $page = 1;     //if no page var is given, default to 1.
        $prev = $page - 1;       //previous page is page - 1
        $next = $page + 1;       //next page is page + 1
        $prev_ = '';
        $first = '';
        $lastpage = ceil($rows / $limit);
        $next_ = '';
        $last = '';
        if ($lastpage > 1) {
            //previous button
            if ($page > 1)
                $prev_ .= "<p style='cursor:pointer ' class='page-numbers' href=\"?page=$prev\">previous</p>";
            else {
                //$pagination.= "<span class=\"disabled\">previous</span>";	
            }
            //pages	
            if ($lastpage < 5 + ($adjacents * 2)) { //not enough pages to bother breaking it up
                $first = '';
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    if ($counter == $page) {
//                         echo 'counter '.$counter.' page '.$page.'<br/>';
                        $pagination .= "<span class=\"current\">$counter</span>";
                    } else {
//                           echo 'counter '.$counter.' page '.$page.'<br/>';
                        $pagination .= "<p style='cursor:pointer ' class='page-numbers' href=\"?page=$counter\">$counter</p>";
                    }
                }
                $last = '';
            } elseif ($lastpage > 3 + ($adjacents * 2)) { //enough pages to hide some
                //close to beginning; only hide later pages
                $first = '';
                if ($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $pagination .= "<span class=\"current\">$counter</span>";
                        else
                            $pagination .= "<p style='cursor:pointer '  class='page-numbers' href=\"?page=$counter\">$counter</p>";
                    }
                    $last .= "<p style='cursor:pointer '  class='page-numbers' href=\"?page=$lastpage\">Last</p>";
//                    echo 'pagination div_2';
                }

                //in middle; hide some front and some back
                elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                    $first .= "<p style='cursor:pointer ' class='page-numbers' href=\"?page=1\">First</p>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $pagination .= "<span class=\"current\">$counter</span>";
                        else
                            $pagination .= "<p style='cursor:pointer ' class='page-numbers' href=\"?page=$counter\">$counter</p>";
                    }
                    $last .= "<p style='cursor:pointer ' class='page-numbers' href=\"?page=$lastpage\">Last</p>";
//                    echo 'pagination div_3';
                }
                //close to end; only hide early pages
                else {
                    $first .= "<p style='cursor:pointer ' class='page-numbers' href=\"?page=1\">First</p>";
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                        if ($counter == $page)
                            $pagination .= "<span class=\"current\">$counter</span>";
                        else
                            $pagination .= "<p style='cursor:pointer ' class='page-numbers' href=\"?page=$counter\">$counter</p>";
                    }
                    $last = '';
//                    echo 'pagination div_4';
                }
            }
            if ($page < $counter - 1)
                $next_ .= "<p  style='cursor:pointer ' class='page-numbers' href=\"?page=$next\">next</p>";
            else {
                //$pagination.= "<span class=\"disabled\">next</span>";
            }
//            echo 'pagination1 '.$pagination;
            $pagination = "<div class=\"pagination\">" . $first . $prev_ . $pagination . $next_ . $last;
            //next button
//            echo 'pagination 2'.$pagination;
            $pagination .= "</div>\n";
        }
//        echo "<pre>";
//	echo 'pagination div_'.$pagination.'aa'.$limit.'sdfs'. $adjacents.'gfh'. $rows.'jgh'. $page;
//        exit;
        return $pagination;
    }

    public function simple_query($sql) {
        $result = $this->db->query($sql);
        return $result;
    }

}
