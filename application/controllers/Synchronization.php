<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Synchronization extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/synchronization
     * 	- or -
     * 		http://example.com/index.php/synchronization/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $this->load->view('welcome_message');
    }

    public function checkLastModifed() {
        if (count($_POST) > 0) {
            $mobile_modified_date = $this->input->post("modified_date");
            $mobile_synced_modifeddate = $this->input->post("synced_modifeddate");
            $json_insertlist = json_decode($this->input->post("json_insertlist"), true);
            $json_updatelist = json_decode($this->input->post("json_updatelist"), true);
            $server_modifierd_date = "";
            $mobile_delete_id = array();
            $db_data = $this->Sync_model->checkLatestModified_date()->result_array();
            if (count($db_data) > 0) {
                $server_modifierd_date = $db_data[0]["last_modified"];
            }

            if (count($json_insertlist) > 0) {
//                insert to servr db
                for ($i = 0; $i < count($json_insertlist); $i++) {
                    $insert = array();
                    $insert["name"] = $json_insertlist[$i]["name"];
                    $insert["phone_no"] = $json_insertlist[$i]["phone_number"];
                    $insert["status"] = 1;
                    $this->Sync_model->addData("users", $insert);
                    array_push($mobile_delete_id, $json_insertlist[$i]["id"]);
                }
            }

            if (count($json_updatelist) > 0) {
                // update to server db
                for ($j = 0; $j < count($json_updatelist); $j++) {
                    $server_id = $json_updatelist[$j]["server_id"];
                    $getServer_data = $this->Sync_model->getData("users", "user_id = $server_id")->result_array();
                    $mob_ind_last_modified = $json_updatelist[$j]["last_modified"];
                    if (count($getServer_data) == 1) {
                        $ser_ind_last_modified = $getServer_data[0]["last_modified"];
                        if ($ser_ind_last_modified < $mob_ind_last_modified) {
                            $update["name"] = $json_updatelist[$j]["name"];
                            $update["phone_no"] = $json_updatelist[$j]["phone_number"];
                            if ($json_updatelist[$j]["status"] == 3) {
                                $update["status"] = 0;
                            } else {
                                $update["status"] = 1;
                            }

                            $this->Sync_model->update("users", $update, "user_id = $server_id");
                            array_push($mobile_delete_id, $json_updatelist[$j]["id"]);
                        }
                    }
                }
            }

            //fetch all data after last synced timesatamp
            if ($mobile_synced_modifeddate == "") {
                //send all data
                $user_data = $this->Sync_model->sendSyncedData();
            } else {
                // send data after timestamp
                $user_data = $this->Sync_model->sendSyncedData($mobile_synced_modifeddate);
            }
            $output["success"] = TRUE;
            $output["deletelist"] = $mobile_delete_id;
            $output["user_data"] = $user_data->result_array();
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found.";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function checkLastModifed_relations() {                              //ok
        if (count($_POST) > 0) {
            // $mobile_modified_date = $this->input->post("modified_date");
            $mobile_synced_modifeddate = str_replace('+', ' ', $this->input->post("synced_modifeddate"));
            $server_modifierd_date = "";
            $db_data = $this->Sync_model->checkLatestModifiedRelation_date()->result_array();
            if (count($db_data) > 0) {
                $server_modifierd_date = $db_data[0]["last_modified"];
            }
            //fetch all data after last synced timesatamp
            if ($mobile_synced_modifeddate == "") {
                //send all data
                //$user_data = $this->Sync_model->sendSyncedData();
                $user_data = $this->Sync_model->sendSyncedDataRelation();
                $output["success"] = TRUE;
                //$output["deletelist"] = $mobile_delete_id;
                $output["relation_data"] = $user_data->result_array();
            } else {
                // send data after timestamp
                //$user_data = $this->Sync_model->sendSyncedData($mobile_synced_modifeddate);
                if ($mobile_synced_modifeddate < $server_modifierd_date) {
                    $user_data = $this->Sync_model->sendSyncedDataRelation();
                    $output["success"] = TRUE;
                    //$output["deletelist"] = $mobile_delete_id;
                    $output["relation_data"] = $user_data->result_array();
                } else {
                    $output["success"] = FALSE;
                    $output["message"] = 'No updated data found';
                }
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found.";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function checkLastModifed_reminders() {


        if (count($_POST) > 0) {
            $function_name = 'checkLastModifed_reminders';

            // $mobile_modified_date = $this->input->post("modified_date");
            $output = array();
            $mobile_synced_modifedRem = str_replace('+', ' ', $this->input->post("synced_modifedRem"));
            $mobile_synced_modifedRel = str_replace('+', ' ', $this->input->post("synced_modifedRel"));
            $user_id = $this->input->post("user_id");
            $json_insertlist = json_decode($this->input->post("json_insertlist"), true);
            $json_updatelist = json_decode($this->input->post("json_updatelist"), true);

            //   echo "insert list";
//              $output['dfhbhdf']=$json_insertlist;
//              $this->output->set_content_type('application/json');
//              
//        $this->output->set_output(json_encode($output['dfhbhdf']));
//           print_r($json_insertlist);
//           exit;

            $server_modifierdReminder_date = "";
            $mobile_delete_id = array();

            $user_id = $this->input->post("user_id");

            if (count($json_insertlist) > 0) {
//                insert to servr db
                for ($i = 0; $i < count($json_insertlist); $i++) {
                    $insert = array();
                    $insert["user_id"] = $user_id;
                    $insert["first_name"] = $json_insertlist[$i]["firstname"];
                    $insert["last_name"] = $json_insertlist[$i]["lastname"];
                    $insert["bdate"] = trim($json_insertlist[$i]["bdate"]);
                    $insert["bmonth"] = trim($json_insertlist[$i]["bmonth"]);
                    $insert["zodiac_id"] = $json_insertlist[$i]["zodiac_id"];
                    $insert["mobile_no"] = $json_insertlist[$i]["mobile_no"];
                    $insert["plus_id"] = $json_insertlist[$i]["plus_id"];
                    $insert["reminder_created_date"] = $json_insertlist[$i]["last_modified"];
                    $insert["country_id"] = $json_insertlist[$i]["country_id"];

                    //updated date
                    $insert["email"] = $json_insertlist[$i]["email"];
                    $insert["rem_image_name"] = $json_insertlist[$i]["rem_image_name"];
                    $insert["relation_id"] = $json_insertlist[$i]["relationid"];
                    if ($json_insertlist[$i]["gender"] = "Male" || $json_insertlist[$i]["gender"] = "1") {
                        $insert["gender"] = 1;
                    } else {
                        $insert["gender"] = 2;
                    }

                    $insert["priority"] = $json_insertlist[$i]["priority"];
//                    if( $json_insertlist[$i]["priority"]=="Priority" ||  $json_insertlist[$i]["priority"]=="priotity" || $json_insertlist[$i]["priority"]=="1"){
//                          $insert["priority"] =0;
//                    }

                    $insert["byear"] = "0000";
                    $date = $this->Birthday->checkLength2($insert["bdate"]);
                    $month = $this->Birthday->checkLength2($insert["bmonth"]);
                    $cbdate = $date . "-" . $month . "-" . "0000";
                    $insert["birth_date"] = date('Y-m-d', strtotime($cbdate));

                    //    $insert["birth_date"] = "0000-" . $month . "-" . $date;
                    $insert["status"] = 1;
                    $this->Sync_model->addData("bday_reminder", $insert);
                    array_push($mobile_delete_id, $json_insertlist[$i]["Id"]);
                }
            }

            if (count($json_updatelist) > 0) {
                // update to server db
                for ($j = 0; $j < count($json_updatelist); $j++) {
                    $server_id = $json_updatelist[$j]["server_id"];
                    $getServer_data = $this->Sync_model->getData("bday_reminder", "reminder_id = $server_id")->result_array();
                    $mob_ind_last_modified = $json_updatelist[$j]["last_modified"];
                    if (count($getServer_data) == 1) {
                        $ser_ind_last_modified = $getServer_data[0]["updated_date"];
                        if ($ser_ind_last_modified < $mob_ind_last_modified) {
                            $update = array();
                            $update["user_id"] = $json_updatelist[$j]["user_id"];
                            $update["first_name"] = $json_updatelist[$j]["firstname"];
                            $update["last_name"] = $json_updatelist[$j]["lastname"];
                            $update["bdate"] = trim($json_updatelist[$j]["bdate"]);
                            $update["bmonth"] = trim($json_updatelist[$j]["bmonth"]);
                            $update["zodizc_id"] = $json_updatelist[$j]["zodiac_id"];
                            $update["plus_id"] = $json_updatelist[$i]["plus_id"];
                            $update["rem_image_name"] = $json_updatelist[$j]["rem_image_name"];
                            $update["mobile_no"] = $json_updatelist[$j]["mobile_no"];
                            $update["reminder_created_date"] = $json_updatelist[$j]["reminder_created_date"];
                            $update["country_id"] = $json_updatelist[$j]["country_id"];

                            //updated date
                            $update["email"] = $json_updatelist[$j]["email"];
                            $update["relation_id"] = $json_updatelist[$j]["relation_id"];
                            //  $update["gender"] = $json_updatelist[$j]["gender"];
                            if ($json_updatelist[$i]["gender"] = "Male" || $json_updatelist[$i]["gender"] = "1") {
                                $update["gender"] = 1;
                            } else {
                                $update["gender"] = 2;
                            }


                            $update["priority"] = $json_updatelist[$j]["priority"];
//                              if( $json_updatelist[$i]["priority"]=="Priority" ||  $json_updatelist[$i]["priority"]=="priotity" || $json_updatelist[$i]["priority"]=="1"){
//                          $update["priority"] =0;
//                    }
                            $date = $this->Birthday->checkLength2($update["bdate"]);
                            $month = $this->Birthday->checkLength2($update["bmonth"]);

                            $cbdate = $update["bdate"] . "-" . $update["bmonth"] . "-" . "0000";
                            $update["birth_date"] = date('Y-m-d', strtotime($cbdate));
                            $update["byear"] = "0000";

                            if ($json_updatelist[$j]["status"] == 3) {
                                $update["status"] = 0;
                                array_push($mobile_delete_id, $json_updatelist[$j]["Id"]);
                            } else {
                                $update["status"] = 1;
                            }
                            $this->Sync_model->update("bday_reminder", $update, "reminder_id = $server_id");
                            //array_push($mobile_delete_id, $json_updatelist[$j]["Id"]);
                        }
                    }
                }
            }

            $db_dataReminder = $this->Sync_model->checkLatestModifiedReminder_date($user_id)->result_array();
            if (count($db_dataReminder) > 0) {
                $server_modifierdReminder_date = $db_dataReminder[0]["last_modified"];
            }
            $server_modifierdRelation_date = "";
            $db_dataRelation = $this->Sync_model->checkLatestModifiedRelation_date()->result_array();
            if (count($db_dataRelation) > 0) {
                $server_modifierdRelation_date = $db_dataRelation[0]["last_modified"];
            }
            $remdata = true;
            $reldata = true;
            $output["reminder_data"] = array();
            $output["relation_data"] = array();
            //fetch all data after last synced timesatamp
            if ($mobile_synced_modifedRem == "") {
                //send all data
                $reminder_data = $this->Sync_model->sendSyncedDataReminder($user_id);
                $output["deletelist"] = $mobile_delete_id;
                $output["reminder_data"] = $reminder_data->result_array();
            } else {
                if ($mobile_synced_modifedRem < $server_modifierdReminder_date) {
                    // send data after timestamp
                    $reminder_data = $this->Sync_model->sendSyncedDataReminder($user_id, $mobile_synced_modifedRem);
                    $output["deletelist"] = $mobile_delete_id;
                    $output["reminder_data"] = $reminder_data->result_array();
                } else {
                    $remdata = false;
                }
            }

            //fetch all data after last synced timesatamp
            if ($mobile_synced_modifedRel == "") {
                //send all data
                $relation_data = $this->Sync_model->sendSyncedDataRelation();
                $output["relation_data"] = $relation_data->result_array();
            } else {
                if ($mobile_synced_modifedRel < $server_modifierdRelation_date) {
                    // send data after timestamp
                    $relation_data = $this->Sync_model->sendSyncedDataRelation();
                    $output["relation_data"] = $relation_data->result_array();
                } else {
                    $reldata = false;
                }
            }
            $output["mobile_synced_date"] = $mobile_synced_modifedRem;
            $output["server_synced_date"] = $server_modifierdReminder_date;
            $output["success"] = TRUE;
            $output["url"] = BASEURL . "Synchronization/" . $function_name;
//            if ($remdata == false && $reldata == true) {
//                $output["reminder_data"] = array();
//                $output["success"] = TRUE;
//            } elseif ($reldata == false && $remdata == true) {
//                $output["relation_data"] = array();
//                $output["success"] = TRUE;
//            } elseif ($remdata == true && $reldata = true) {
//                $output["success"] = TRUE;
//            } else {
            if (!$remdata && !$reldata) {
                $output["success"] = FALSE;
                $output["message"] = 'No updated data found' . $remdata . " " . $reldata;
                echo json_encode($output);
                exit();
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found.";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

}
