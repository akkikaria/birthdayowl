<?php

header("Access-Control-Allow-Origin: *");

//Live birthdayowl
Class Home_web extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    //start-login/Home/Resgister
// ====================================================================================================================================//
// -> Function     : index
// -> Executed     : fetching banners,vouchers,homescreen and loading Home view
// -> description  :Displays all vouchers and banners
// ====================================================================================================================================//
    function zz(& $x) {
        $x = $x + 5;
    }

    public function demo() {
        $xmldata = file_get_contents("https://www.w3schools.com/xml/note.xml");
        $xml_data = simplexml_load_string($xmldata);
        $json_data = json_encode($xml_data);
        $configData = json_decode($json_data, true);
    }

    public function index() {
//Fetching banners from database
//  $data["homescreen"] = $this->Birthday->getSelectData("image", "home_screens", "null")->result_array();
//        require 'new.php';
//        $ax = $this->Birthday->demo("home_screens", "new=1");
//        if ($ax == "") {
//            echo "error";
//            exit;
//        } else {
//            $data["homescreen"] = $ax->result_array();
//        }

        $data["banners"] = $this->Birthday->getSelectDataOrderBy("banner_image", "festival_banners", "position")->result_array();
        for ($i = 0; $i < count($data["banners"]); $i++) {
            $image = $data["banners"][$i]['banner_image'];
            $banners_without_extension = pathinfo($image, PATHINFO_FILENAME);
            $data["banners"][$i]['banner_image_name'] = $banners_without_extension;
        }
//Fetching vouchers from files by calling helper method
        $vouchers = getAllVouchers();
//sorting vouchers alphabetically
//        $data["product_voucher"] = $this->subval_sort($vouchers, 'voucher_pro_name');
        $data["product_voucher"] = array();
//        $where = "1 and status = 'Active' ORDER BY RAND() LIMIT 30";
//        $data["flower_product"] = $flower_product_random = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
        $where = "1 and category_name='Flower Bouquet' ORDER BY RAND() LIMIT 30";
        $data["flower_product"] = $flower_product_random = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
//        $data["flower_product"] = $this->Birthday->getAllData("product_new")->result_array();

        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=1")->row_array();
        $data["seo"] = $title;
//        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();

        $userinfo = $this->Birthday->getAllData("birthday")->result_array();
        $data["success"] = true;
        $data["birthday_data"] = $userinfo;

        $where = "greeting_type=1 order by card_id DESC";
        $data["greeting_cards"] = $this->Birthday->getSelectData("card_id,front_page_image,card_name", "greeting_card", $where)->result_array();
        $data["fid"] = 0;

//        Zodiac Info Daily Wise
        $data["date"] = $sign_day = date("j");
        $sign_month = date("n");
        $data["month"] = date("F");
        $where = "sign_day='$sign_day'and sign_month='$sign_month'";
        $zodiac_info = $this->Birthday->getUserData("zodiac_signs", $where)->row_array();
        $zodiac_info_name = $this->Birthday->getUserData("mapzodiac", "(fdate <='$sign_day' and fmonth='$sign_month') or (tdate>='$sign_day' and tmonth ='$sign_month')")->row_array();
        $zodiac_id = $zodiac_info_name["zodiac_id"];
        if (count($zodiac_info) > 0) {
            $data["success"] = true;
            $data["sign_info"] = $zodiac_info["sign_data"];
            $end = stripos($data["sign_info"], 'Friends and Lovers');
            $data["first_part"] = strip_tags(substr($data["sign_info"], 0, $end));
//            $start = stripos($data["sign_info"], 'Children and Family');
//            $text = substr($data["sign_info"], $start);
//            $word = "Children and Family";
//            $data["second_part"] = str_ireplace($word, "<b>$word</b>", $text);r
            $data["zodiac_id"] = $zodiac_id;


            switch ($zodiac_id) {
                case 1:
                case '1': {
                        $data["zodiac_name"] = "Aries";
                        $data["zodiac_img"] = BASEURL_IMG . "zodiac/Aries.png";
                    }
                    break;
                case 2:
                case '2': {
                        $data["zodiac_name"] = "Taurus";
                        $data["zodiac_img"] = BASEURL_IMG . "zodiac/taurus.png";
                    }
                    break;
                case 3:
                case '3': {
                        $data["zodiac_name"] = "Gemini";
                        $data["zodiac_img"] = BASEURL_IMG . "zodiac/taurus.png";
                    }
                    break;
                case 4:
                case '4': {
                        $data["zodiac_name"] = "Cancer";
                        $data["zodiac_img"] = BASEURL_IMG . "zodiac/cancer.png";
                    }
                    break;
                case 5:
                case '5': {
                        $data["zodiac_name"] = "Leo";
                        $data["zodiac_img"] = BASEURL_IMG . "zodiac/leo.png";
                    }
                    break;
                case 6:
                case '6': {
                        $data["zodiac_name"] = "Virgo";
                        $data["zodiac_img"] = BASEURL_IMG . "zodiac/virgo.png";
                    }
                    break;
                case 7:
                case '7': {
                        $data["zodiac_name"] = "Libra";
                        $data["zodiac_img"] = BASEURL_IMG . "zodiac/libra.png";
                    }
                    break;
                case 8:
                case '8': {
                        $data["zodiac_name"] = "Scorpio";
                        $data["zodiac_img"] = BASEURL_IMG . "zodiac/scorpio.png";
                    }
                    break;
                case 9:
                case '9': {
                        $data["zodiac_name"] = "Sagittarius";
                        $data["zodiac_img"] = BASEURL_IMG . "zodiac/sagittarius.png";
                    }
                    break;
                case 10:
                case '10': {
                        $data["zodiac_name"] = "Capricorn";
                        $data["zodiac_img"] = BASEURL_IMG . "zodiac/capricorn.png";
                    }
                    break;
                case 11:
                case '11': {
                        $data["zodiac_name"] = "Aquarius";
                        $data["zodiac_img"] = BASEURL_IMG . "zodiac/aquarius.png";
                    }
                    break;
                case 12:
                case '12': {
                        $data["zodiac_name"] = "Pisces";
                        $data["zodiac_img"] = BASEURL_IMG . "zodiac/pisces.png";
                    }
                    break;
            }
        } else {
            $data["success"] = false;
            $data["message"] = "zodiac sign is not found";
        }
        $this->load->view("HOME/Home", $data);
    }

    public function know_horoscope_day_wise() {
//        Zodiac Info
        $data["date"] = $sign_day = date("j");
        $sign_month = date("n");
        $data["month"] = date("F");
        $where = "sign_day='$sign_day'and sign_month='$sign_month'";
        $zodiac_info = $this->Birthday->getUserData("zodiac_signs", $where)->row_array();
        $zodiac_info_name = $this->Birthday->getUserData("mapzodiac", "(fdate <='$sign_day' and fmonth='$sign_month') or (tdate>='$sign_day' and tmonth ='$sign_month')")->row_array();
        $zodiac_id = $zodiac_info_name["zodiac_id"];
        if (count($zodiac_info) > 0) {
            $data["success"] = true;
            $data["sign_info"] = $zodiac_info["sign_data"];
            $end = stripos($data["sign_info"], 'Children and Family');
            $data["first_part"] = substr($data["sign_info"], 0, $end);
            $start = stripos($data["sign_info"], 'Children and Family');
            $text = substr($data["sign_info"], $start);
            $word = "Children and Family";
            $data["second_part"] = str_ireplace($word, "<b>$word</b>", $text);
            $data["zodiac_id"] = $zodiac_id;
        } else {
            $data["success"] = false;
            $data["message"] = "zodiac sign is not found";
        }
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=12")->row_array();
        $data["seo"] = $title;
        $this->load->view('BIRTHDAY_REMINDER/know_horoscope_day_wise', $data); //loading signup view
    }

// ====================================================================================================================================//
// -> Function     :login_success
// -> Executed     :on click of login button ajax call this function.
// -> description  :It work on the basis of login_with type. login_with ->1 means normal login(Email-id,password) login_with->2 means facebook login login_with->3 means google plus login
//    If login_with is 1 and is_guest->1 then it is guest login whereas  other times is_guest ->0
//    we are treating guest user as a new user   
// ====================================================================================================================================//

    public function login_success($login_with) {
        if ($login_with == 1) {//Login as 1=Normal
            $postcount = count($_POST);
            if ($postcount > 0) {
                $gsdata['email'] = $email = check_post($this->input->post("email"));
                $mobile_no = $gsdata['mobile_no'] = $phone = check_post($this->input->post("mobile_no"));
                $hashkey = " ";
                $password = check_post($this->input->post("password"));
                $login_type = check_post($this->input->post("login_type"));
                $gsdata['is_guest'] = $is_guest = check_post($this->input->post("is_guest"));
                $first_name = check_post($this->input->post("first_name"));
////////////////////////////////////////////////////check the blacklisted email and phone number////////////////////////////////////////////////////////
                $this->load->library('BlackListMobileAndEmail');
                $blackListMobileAndEmail = new BlackListMobileAndEmail();
                $blackListMobileAndEmail->checkBlacklistEmail(trim($email));
                $blackListMobileAndEmail->checkBlaclistMobile(trim($mobile_no));
//                var_dump($blackListMobileAndEmail->isBlackList());
//                exit;
                if ($blackListMobileAndEmail->isBlackList()) {
                    $output["success"] = FALSE;
                    $output["message"] = "Your Id has been blocked";
                    $myoutput = json_encode($output);
                    echo $myoutput;
                    exit;
                }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   

                if ($is_guest == 1) {//Guest User Login
//Already User is Exist or not for is_guest = 0
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    $this->load->library("CheckAsBirthdayowl");
                    $check_birthdayowl_user = new CheckAsBirthdayowl();
                    $check_birthdayowl_user->setEmail($email);
                    $check_birthdayowl_user->setMobileNo($mobile_no);
                    $is_birthdayowl_user = $check_birthdayowl_user->runner();

                    if ($is_birthdayowl_user) {
                        $output["userdata"] = $check_birthdayowl_user->getResult();
//                        $output["userdata"]['flower_data'] = array();
//                        $output["userdata"]['voucher_data'] = array();
                        $output["success"] = true;
                        $output["is_user_exist"] = 1;
                        $output["login_type"] = $login_type;
//                        if ($this->session->userdata() != NULL) {
//                            $this->session->unset_userdata('userdata');
//                        }
//                        $this->session->set_userdata($output);
//                        $output["message"] = "Login Successfully";
//                    $where = "(email='$email' and is_guest=0 and mobile_verify=1)";
//                    $check_user = $this->Birthday->getSelectData("*", "users", $where)->result_array();
//
//                    if (count($check_user) > 0) {
//                        $output["userdata"] = $check_user[0];
//                        $output["userdata"]['flower_data'] = array();
//                        $output["userdata"]['voucher_data'] = array();
//                        $output["success"] = true;
//                        $output["login_type"] = $login_type;
//                        $output["is_user_exist"] = 1; //User is Exist as Registered User
//                        if ($this->session->userdata() != NULL) {
//                            $this->session->unset_userdata('userdata');
//                        }
//                        $this->session->set_userdata($output);
//                        $output["message"] = "Login Successfully";
//                    } 
                    } else {
//                        Already User is Exist or not for is_guest = 1
                        $this->load->library("CheckAsGuest");
                        $check_guest_exist = new CheckAsGuest();
                        $check_guest_exist->setGuestEmail($email);
                        $check_guest_exist->setMobileNo($mobile_no);
//                        $check_guest_exist->setCountry_code($country_id);
                        $is_guest_exist = $check_guest_exist->runner();

                        if ($is_guest_exist) {
                            $getUserId = $check_guest_exist->getUserId();
                            $sameMoble = $check_guest_exist->checkMobileAreSame();
                            if ($sameMoble) {
                                $update['u_last_login'] = date("Y-m-d H:i:s");
                                $update['first_name'] = $first_name;
                                $check_guest_exist->updateForLogin($update);
                                $output["userdata"] = $check_guest_exist->getResult();
                                $output["userdata"]['flower_data'] = array();
                                $output["userdata"]['voucher_data'] = array();
                                $output["success"] = true;
                                $output["is_user_exist"] = 2; //User is Exist as Guest User
                                $output["session_guest_id"] = session_id();
                                $this->session->set_userdata($output);
                            } else {
                                // need to update the phone number of the particular user id on verify otp
                                $this->load->library("OtpSenderUpdater");
                                $otp_sender = new OtpSenderUpdater();
                                $otp_sender->setMobileNumber($mobile_no);
                                $otp_sender->setHashKey($hashkey);
                                $otp_gone = $otp_sender->runner();
                                $output["success"] = $otp_gone ? true : false;
                                $output['userdata'] = $check_guest_exist->getResult();
//                                $output["userdata"]['flower_data'] = array();
//                                $output["userdata"]['voucher_data'] = array();
                                $output["success"] = true;
                                $output["is_user_exist"] = 0; //User is Exist as Guest User
//                                $output["update_guest_phone"] = 1;
                                $output["login_type"] = $login_type;
                                $output["session_guest_id"] = session_id();
                            }
                        }
                        //$where = "(email='$email' and is_guest=1 and mobile_verify=1)";
//                        $check_user = $this->Birthday->getSelectData("*", "users", $where)->result_array();
//                        if (count($check_user) > 0) {
//                            $output["userdata"] = $check_user[0];
//                            $output["userdata"]['flower_data'] = array();
//                            $output["userdata"]['voucher_data'] = array();
//                            $output["success"] = true;
//                            $output["is_user_exist"] = 2; //User is Exist as Guest User
//                            $output["session_guest_id"] = session_id();
//                            $this->session->set_userdata($output);
                        else {
                            $this->load->library("CheckAsGoogle");
                            $check_google_user = new CheckAsGoogle();
                            $check_google_user->setGoogleEmailId($email);
                            $is_google_exist = $check_google_user->runner();
                            if ($is_google_exist) {
                                $date = date("Y-m-d H:i:s");
                                $update_array['u_last_login'] = $date;
                                $update_array['is_guest'] = 1;
                                $update_array['login_with'] = $login_with;
                                $update_array['mobile_no'] = $mobile_no;
                                $user_id = $check_google_user->getUserId();
                                $this->Birthday->update("users", $update_array, "user_id = '$user_id'");
                                $result = $check_google_user->getResult();
                                $this->load->library("OtpSenderUpdater");
                                $otp_sender = new OtpSenderUpdater();
                                $otp_sender->setMobileNumber($mobile_no);
                                $otp_sender->setHashKey($hashkey);
                                $otp_sender->runner();
//                                $result['u_last_login'] = $date;
//                                $result['is_guest'] = 1;
                                $output["userdata"] = $result;
//                                $output["userdata"]['flower_data'] = array();
//                                $output["userdata"]['voucher_data'] = array();
                                $output["success"] = true;
                                $output["is_user_exist"] = 0; //User is Exist as Guest User
                                $output["session_guest_id"] = session_id();
                            } else {
                                $this->load->library("DeleteInvalidGuest");
                                $deleteInvalidGuest = new DeleteInvalidGuest();
                                $deleteInvalidGuest->setEmail($email);
                                $deleteInvalidGuest->setPhoneNumber($mobile_no);
                                $deleteInvalidGuest->runner();

//                                $where_delete = "email='$email' and is_guest = 1 and mobile_verify = 0";
//                                $check_user = $this->Birthday->getSelectData("user_id", "users", $where_delete)->row_array();
//                                if (!empty($check_user)) {
//                                    $id = $check_user['user_id'];
//                                    //if id is found then delete the entry for the particular id and continue
//                                    $where = "user_id = '$id'";
//                                    $this->Birthday->delete_reminder('users', $where);
//                                }
                                //User not exist as is_guest=0 & is_guest=1 then do registration
                                $gsdata["username"] = "";
                                $gsdata["password"] = "";
                                $gsdata['first_name'] = check_post($this->input->post("first_name"));
                                $gsdata['login_with'] = $login_with;
                                $gsdata['g_id'] = "";
                                $gsdata["user_profile_pic"] = "";
                                $gsdata["reset_time"] = date("Y-m-d H:i:s");
                                $gsdata["reset_link"] = 0;
                                $gsdata["reset_activate"] = 0;
                                $gsdata["sent_greeting_counter"] = 0;
                                $gsdata["membership_status"] = 0;
                                $gsdata["gender"] = 1;
                                $gsdata["zodiac_id"] = 0;
                                $gsdata["bmonth"] = '';
                                $gsdata["bdate"] = '';
                                $gsdata["birth_date"] = "";
                                $gsdata["fb_id"] = "";
                                $gsdata["byear"] = "";
                                $gsdata["is_guest"] = 1;
                                $gsdata["created_date"] = date("Y-m-d H:i:s");
                                $gsdata["u_last_login"] = date("Y-m-d H:i:s");
                                $gsdata["platform"] = 2; //2 means web
//Send OTP to verify mobile number while purchasing Voucher or Flowers
//==========================OTP==========================
                                $data["code"] = $code = $this->generateRandomString(5);
                                $data["mobile_no"] = $mobile_no;
                                $otp_data = $this->Birthday->getSelectData("mobile_no", "otp", "mobile_no=$mobile_no")->result_array();
                                $count = count($otp_data);
                                if ($count > 0) {
                                    $update["code"] = $code;
                                    $update["sent_time"] = date("Y-m-d H:i:s");
                                    $this->Birthday->update("otp", $update, "mobile_no='$mobile_no'");
                                } else {
                                    $data["created_date"] = date("Y-m-d H:i:s");
                                    $data["sent_time"] = date("Y-m-d H:i:s");
                                    $this->Birthday->addData("otp", $data);
                                }
                                $show_message = "Enter $code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone.";
                                try {
                                    $resp = $this->Birthday->send_mobile_otp($mobile_no, $show_message);
                                    $output["success"] = true;
//                $output["message"] = "OTP sent successfully";
                                } catch (Exception $e) {
                                    $output["success"] = false;
//                $output["message"] = "Failed.Please try again";
                                }
//==========================OTP==========================
                                $insert_id = $this->Birthday->addData("users", $gsdata);
                                $where = "user_id='$insert_id'";
                                $user_info = $this->Birthday->getUserData("users", $where)->row_array();
                                $output["userdata"] = $guesdata["userdata"] = $user_info;
                                $output["success"] = true;
                                $output["session_guest_id"] = session_id();
                                $output["is_user_exist"] = 0; //User is not Exist as Registered User & Guest User
                            }
                        }
                    }
                } else {//Normal User Login
                    $pass = $this->Birthday->encryptPassword($password);
                    $where = "email='$email' and password='$pass' and is_guest=0 and mobile_verify=1";
                    $check_user = $this->Birthday->getUserData("users", $where)->result_array();
//$check_user = $this->security->xss_clean($check_user);
                    if (count($check_user) == 0) {
                        $output["success"] = false;
                        $output["message"] = "Invalid Email Address or Password.";
                    } else {
                        $user_details_update["u_last_login"] = date("Y-m-d H:i:s");
                        $this->Birthday->update("users", $user_details_update, "email='$email'");
                        $output["userdata"] = $check_user[0];
                        $output["userdata"]['flower_data'] = array();
                        $output["userdata"]['voucher_data'] = array();
                        $output["success"] = true;
                        $output["login_type"] = $login_type;
                        if ($this->session->userdata() != NULL) {
                            $this->session->unset_userdata('userdata');
                        }
                        $this->session->set_userdata($output);
                        if ($login_type == "3") {
                            $output["message"] = "Login Successfully";
                        }
                    }
                }
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode($output));
            }
        } else if ($login_with == 2 || $login_with == 3) { //Login as 2-facebook 3-gplus
            $redirection_type = $this->session->userdata["redirection_type"];
            $updated_data['first_name'] = $sdata['first_name'] = $this->session->userdata["sfname"];
            $updated_data['email'] = $sdata['email'] = $semail = $this->session->userdata["semail"];
            $updated_data['login_with'] = $sdata['login_with'] = $login_with;
            $sdata['user_profile_pic'] = "";
            $sdata["reset_time"] = date("Y-m-d H:i:s");
            $sdata["reset_link"] = 0;
            $sdata["reset_activate"] = 0;
            $sdata["sent_greeting_counter"] = 0;
            $sdata["membership_status"] = 0;
            $gender = $this->session->userdata["gender"];
            if ($gender == "male") {
                $sdata["gender"] = 1;
            } else {
                $sdata["gender"] = 2;
            }
            $bday = $this->session->userdata["sbday"];
            $bmonth = $this->session->userdata["sbmonth"];
            $sdata["byear"] = $y = $this->session->userdata["sbyear"];
            if (($bmonth != '') && ($bday != '')) {
                $zodiac_info = $this->Birthday->getUserData("mapzodiac", "(fdate <='$bday' and fmonth='$bmonth') or (tdate>='$bday' and tmonth ='$bmonth')")->row_array();
                $updated_data['zodiac_id'] = $sdata["zodiac_id"] = $zodiac_info["zodiac_id"];
                $updated_data['bmonth'] = $sdata["bmonth"] = $bmonth;
                $updated_data['bdate'] = $sdata["bdate"] = $bday;
                $updated_data['birth_date'] = $sdata["birth_date"] = "$bmonth/$bday/$y";
            } else {

                $sdata["zodiac_id"] = 0;
                $sdata["bmonth"] = '';
                $sdata["bdate"] = '';
                $sdata["birth_date"] = "";
            }
            if ($login_with == 2) {
                $sdata['g_id'] = "";
                $sdata['fb_id'] = $updated_data['fb_id'] = $f_id = $this->session->userdata["gid"];
            } else {
                $sdata['fb_id'] = "";
                $sdata['g_id'] = $updated_data["g_id"] = $g_id = $this->session->userdata["gid"];
            }
            $updated_data['mobile_no'] = $sdata["mobile_no"] = $this->session->userdata["phone"];
            $sdata["created_date"] = date("Y-m-d H:i:s");
            $where = "email='$semail' and is_guest=0";
            $query = $this->Birthday->getSelectData("email", "users", $where)->result_array();
            $updated_data["u_last_login"] = $sdata["u_last_login"] = date("Y-m-d H:i:s");
            if (count($query) > 0) {
                $updated = $this->Birthday->update("users", $updated_data, $where);
                if ($updated == 1) {
                    $user_info = $this->Birthday->getUserData("users", $where)->row_array();
                    $data["userdata"] = $user_info;
                    $data["userdata"]['flower_data'] = array();
                    $data["userdata"]['voucher_data'] = array();
                    $this->session->set_userdata($data);
                }
            } else {
                $insert_id = $this->Birthday->addData("users", $sdata);
                $where = "user_id='$insert_id'";
                $user_info = $this->Birthday->getUserData("users", $where)->row_array();
                $data["userdata"] = $user_info;
                $data["userdata"]['flower_data'] = array();
                $data["userdata"]['voucher_data'] = array();
                $this->session->set_userdata($data);
                $data["password"] = "";
                $new_data["first_name"] = $data["username"] = $firstname = $user_info["first_name"];
                $new_data["email_id"] = $data["email"] = $uemail = $user_info["email"];
                $data["mobile_no"] = $user_info["mobile_no"];
                //for platform
                $data["from_platform"] = "Web";
                $new_data["nimage_name"] = "";
                $this->Birthday->addData("news_letter", $new_data);
                //To user
                $view = $this->load->view("email_template/registered_success", $data, true);
                send_Email($view, $uemail, $firstname, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");

                //To Support Birthday Owl
                $view1 = $this->load->view("email_template/register_support", $data, true);
                send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $uemail($firstname).");
            }
            if ($redirection_type == 11) {
                redirect(WEB_DASHBOARD);
            } else if ($redirection_type == 22) {
//                $user_id = $this->session->userdata["userdata"]["user_id"];
//                $getsentcounter = $this->Birthday->getSelectData("sent_greeting_counter", "users", "user_id='$user_id'and membership_status='0'")->row_array();
                redirect(BASEURL . "Home_web/send_social_greeting");
//Old
//                if ($getsentcounter["sent_greeting_counter"] < 10) {
//                    redirect(BASEURL . "Home_web/send_social_greeting");
//                } else {
//                    redirect(BASEURL . "paid_membership");
//                }
            } else if ($redirection_type == 33) {
//                $url = $this->session->userdata["product_data"]["voucher_url"];
//                redirect(BASEURL . "voucher/" . $url);
                redirect(BASEURL . "Home_web/add_social_voucherdata/1");
            } else if ($redirection_type == 44) {
                redirect(BASEURL . "Home_web/add_social_voucherdata/2");
            } else if ($redirection_type == 66) {
                redirect(BASEURL . "membershipcart/1");
            } else if ($redirection_type == 77) {
                redirect(BASEURL . "addReminder/0");
            }
        }
    }

// ====================================================================================================================================//
// -> Function     :social_login_data
// -> Executed     :on click of social login button this function get called
// -> description  :It saves data in session.On the basis of provider value google and facebook redirection occures
// ====================================================================================================================================//

    public function social_login_data() {
        $provider = $this->uri->segment(3);
        if ($provider == "3") {
            $data["redirection_type"] = $this->uri->segment(4);
            $this->session->set_userdata($data);
            $url = $this->googleplus->loginURL();
            header("Location:$url"); /* Redirect browser */
            exit();
        } else {
            $data["redirection_type"] = $this->uri->segment(4);
            $this->session->set_userdata($data);
            $url = $this->facebook->login_url();
            header("Location:$url"); /* Redirect browser */
            exit();
        }
    }

// ====================================================================================================================================//
// -> Function     :social_login_success
// -> Executed     :social login success url
// -> description  :It saves data in session.On the basis of provider value google and facebook redirection occures
// ====================================================================================================================================//
    public function social_login_success() {
        $provider = $this->uri->segment(3);
        if ($provider == "2") {
            if ($this->session->has_userdata('flogin')) {//we are checking if user is logedin
                redirect(WEB_HOME);
            }
            if ($this->facebook->is_authenticated()) {//checking if the user is authenticate
                $user_profile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,picture,email,gender,birthday');
                if (!isset($user_profile['error'])) {
                    $data['flogin'] = TRUE; //assigning a value
                    if (in_array("birthday", $user_profile)) {
                        $birthdate = $user_profile["birthday"];
                        if ($birthdate != "") {
                            $orderdate = explode('/', $birthdate);
                            $data["sbday"] = $orderdate[0];
                            $data["sbmonth"] = $orderdate[1];
                            $data["sbyear"] = $orderdate[2];
                        }
                    } else {
                        $data["sbday"] = "";
                        $data["sbmonth"] = "";
                        $data["sbyear"] = "";
                    }
                    $data["sfname"] = $user_profile["first_name"] . " " . $user_profile["last_name"];
                    $data["semail"] = $user_profile["email"];
                    $data["gid"] = $user_profile["id"];
                    $data["gender"] = $user_profile["gender"];
                    $data["phone"] = "";
                    $this->session->set_userdata($data);
                    redirect(BASEURL . "Home_web/login_success/2");
                } else {
                    $this->session->set_flashdata('fmsg', 'Authentication is failed');
                    $this->facebook->destroy_session();
                    redirect(WEB_HOME);
                }
            } else {
                redirect(WEB_HOME);
            }
        } else {
            if (isset($_GET['code'])) {
                $data['glogin'] = TRUE; //assigning a value
                $this->googleplus->getAuthenticate();
                $user_profile = $this->googleplus->getUserInfo();
                $data["sfname"] = $user_profile["name"];
                $data["semail"] = $user_profile["email"];
                $data["gid"] = $user_profile["id"];
                $data["gender"] = "";
                $data["phone"] = "";
                $data["sbday"] = "";
                $data["sbmonth"] = "";
                $data["sbyear"] = "";
                $this->session->set_userdata($data);
                redirect(BASEURL . "Home_web/login_success/3");
            } else {
                redirect(WEB_HOME);
            }
        }
    }

//End-Login/Home/Register
//start-Forgot/Reset Password,OTP
// ====================================================================================================================================//
// -> Function     :forgot_success
// -> Executed     :on click of forgot password ajax call this function
// -> description  :It sends forgot password link to registered email-id
// ====================================================================================================================================//

    public function forgot_success() {
        $output = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $email = check_post($this->input->post("email"));
//            $where = "email='$email' and is_guest = 0";
            $where = "email='$email' and is_guest = 0 and mobile_verify = 1";
            $check_email = $this->Birthday->getSelectData("password,email,first_name,user_id,login_with", "users", $where)->row_array();
            $checkmailcount = count($check_email);
            if ($checkmailcount == 0) {
                $output["success"] = false;
                $output["message"] = "No account found for this Email";
            } else {
                $login_with = $check_email["login_with"];
                $password = $check_email["password"];
                if ($login_with == 1) {
                    $pass = $this->Birthday->decryptPassword($password);
                    $data["Password"] = $pass;
                    $data["username"] = $check_email["email"];
                    $data["realName"] = $name = $check_email["first_name"];
                    $user_id = $check_email['user_id'];
                    $euserid = $this->Birthday->encryptPassword($user_id);
                    $time_created = $this->Birthday->encryptPassword(date("Y-m-d H:i:s"));
                    $data["link"] = BASEURL . "Home_web/change_password/$euserid/$time_created";
                    $view = $this->load->view("email_template/ResetPassword", $data, true);
                    $result = send_EmailResponse($view, $email, $name, "resetpassword@birthdayowl.com", "birthdayowl.com", "BirthdayOwl : Reset Password");
                    if ($result["code"] == "success") {
                        $reset["reset_activate"] = 1;
                        $reset["reset_time"] = date("Y-m-d H:i:s");
                        $reset["reset_link"] = $data["link"];
                        $this->Birthday->update("users", $reset, "user_id = $user_id");
                        $output["success"] = true;
                        $output["message"] = "Check your Email address to Reset Password"; //If user name is not in the database
                    } else {
                        $output["success"] = false;
                    }
                } else {
                    switch ($login_with) {
                        case "2": {
                                $output["success"] = true;
                                $output["message"] = "You have registered using Facebook account.Please log in using Facebook account."; //If user name is not in the database
                            }
                            break;
                        case "3": {
                                $output["success"] = true;
                                $output["message"] = "You have registered using Google account.Please log in using Google account."; //If user name is not in the database
                            }
                            break;
                    }
                }
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

// ====================================================================================================================================//
// -> Function     :change_password
// -> Executed     :on click of above forgot password link this function get called and load view 
// -> description  :It loads change_password/ Resetlink_expired view 
// ====================================================================================================================================//
    public function change_password() {
        $user_id = $this->uri->segment(3);
        $created_time = $this->Birthday->decryptPassword($this->uri->segment(4));
        $current_time = date("Y-m-d H:i:s");
        $date_a = new DateTime($current_time);
        $date_b = new DateTime($created_time);
        $interval = date_diff($date_a, $date_b);
        $diff = $interval->format('%d');
        $guser_id = $greet_cat_id = $this->Birthday->decryptPassword($user_id);
        $where = "user_id='$guser_id'";
        $check_status = $this->Birthday->getSelectData("reset_activate", "users", $where)->row_array();
        $reset_activate = $check_status["reset_activate"];
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=1")->row_array();
        $data["seo"] = $title;
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();

        if ($diff < 1 && $reset_activate == 1) {
            $data["url"] = "getUserAgentDetail/$guser_id";
            $this->load->library('user_agent');
            $data["user_id"] = $guser_id;
//            if ($this->agent->is_mobile()) {
//                $data["detail"] = $detail = $this->agent->mobile();
//                $this->load->view("HOME/CommonDeepLinkingView", $data);
//            } else 
            if ($this->agent->is_browser()) {
                $data["user_id"] = $guser_id;
                $im_details["reset_activate"] = 0;
                $this->Birthday->update("users", $im_details, "user_id = $guser_id");
                $this->load->view('HOME/change_password', $data);
            }
        } else {
            $this->load->view('HOME/Resetlink_expired', $data);
        }
    }

    public function getUserAgentDetail() {
        $data["user_id"] = $user_id = $this->uri->segment(2);
        $im_details["reset_activate"] = 0;
        $this->Birthday->update("users", $im_details, "user_id = $user_id");
        $vouchers = getAllVouchers();
        $data["product_voucher"] = $this->subval_sort($vouchers, 'voucher_pro_name');
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=1")->row_array();
        $data["seo"] = $title;
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();

        if ($this->uri->segment(3) == 2) {
            $data["from"] = 1; //change password:1;2:freegreeting 3:Giftgreeting
            $this->load->view("HOME/deep_linking", $data);
//            $this->load->view("FREEGREETING/DemoLinking", $data);
        } else {

            $vouchers = getAllVouchers();
//sorting vouchers alphabetically
            $data["product_voucher"] = $this->subval_sort($vouchers, 'voucher_pro_name');
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=1")->row_array();
            $data["seo"] = $title;
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $this->load->view('HOME/change_password', $data);
        }
    }

// ====================================================================================================================================//
// -> Function     :changePassword
// -> Executed     :In change_password view on click of submit ajax calls this function
// -> description  :It updates password in database
// ====================================================================================================================================//
    public function changePassword() {
        $output = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $new_password = check_post($this->input->post("new_password"));
            $user_id = check_post($this->input->post("user_id"));
            $im_details["password"] = $this->Birthday->encryptPassword($new_password);
            $im_details["reset_activate"] = 0;
            $this->Birthday->update("users", $im_details, "user_id = $user_id");
            $check_user = $this->Birthday->getUserData("users", "user_id='$user_id'")->result_array();
            $output["userdata"] = $check_user[0];
            $output["userdata"]["flower_data"] = $this->session->userdata["userdata"]["flower_data"];
            $output["userdata"]["voucher_data"] = $this->session->userdata["userdata"]["voucher_data"];
            $this->session->set_userdata($output);
            $output["success"] = TRUE;
            $output["message"] = "Password Changed Successfully";
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

// ====================================================================================================================================//
// -> Function     :send_otp
// -> Executed     :On click of request OTP submit this function get called
// -> description  :It generates random code ,adds data in database and sends message to registered mobile no
// ====================================================================================================================================//  
    public function send_otp() {
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $data["mobile_no"] = $mobile_no = check_post($this->input->post("mobile_no"));
            $hashkey = " ";
            $check_mb = $this->Birthday->getSelectData("mobile_no", "users", "mobile_no=$mobile_no and is_guest = 0")->result_array();
            if (count($check_mb) > 0) {
                $data["code"] = $code = $this->generateRandomString(5);
                $otp_data = $this->Birthday->getSelectData("*", "otp", "mobile_no=$mobile_no")->result_array();
                $count = count($otp_data);
                if ($count > 0) {

                    $delivery_date_time = $otp_data[0]["sent_time"];
                    $triggerOn = date('Y-m-d H:i:s', strtotime($delivery_date_time));
                    $date1 = $triggerOn;
                    $date2 = date("Y-m-d H:i:s");
//Convert them to timestamps.
                    $date1Timestamp = strtotime($date1);
                    $date2Timestamp = strtotime($date2);
//Calculate the difference.
                    $difference = $date1Timestamp - $date2Timestamp;
                    $minutes = floor($difference / 60);

                    if ($minutes < -5) {
                        $update["code"] = $code;
                        $update["sent_time"] = date("Y-m-d H:i:s");
                        $this->Birthday->update("otp", $update, "mobile_no='$mobile_no'");
                    } else {
                        $data["code"] = $code = $otp_data[0]["code"];
                    }
                } else {
                    $data["created_date"] = date("Y-m-d H:i:s");
                    $data["sent_time"] = date("Y-m-d H:i:s");
                    $this->Birthday->addData("otp", $data);
                }
                $show_message = "Enter $code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone.";
                try {
                    $resp = $this->Birthday->send_mobile_otp($data["mobile_no"], $show_message);
                    $output["success"] = true;
                    $output["message"] = "OTP sent successfully";
                } catch (Exception $e) {
                    $output["success"] = true;
                    $output["message"] = "Failed.Please try again";
                }
            } else {
                $output["success"] = FALSE;
//                $output["message"] = "Given Mobile Number is not registered in BirthdayOwl.com";
                $output["message"] = "No account found for this Mobile Number";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

//    public function resend_otp() {
//        $data = array();
//        $postcount = count($_POST);
//        if ($postcount > 0) {
//            $data["mobile_no"] = $mobile_no = check_post($this->input->post("mobile_no"));
//            $check_mb = $this->Birthday->getSelectData("mobile_no", "users", "mobile_no=$mobile_no and (is_guest=0 or is_guest=1) and mobile_verify=0")->result_array();
//            if (count($check_mb) > 0) {
//                $data["code"] = $code = $this->generateRandomString(5);
//                $otp_data = $this->Birthday->getSelectData("*", "otp", "mobile_no=$mobile_no")->result_array();
//                $count = count($otp_data);
//                if ($count > 0) {
//
//                    $delivery_date_time = $otp_data[0]["sent_time"];
//                    $triggerOn = date('Y-m-d H:i:s', strtotime($delivery_date_time));
//                    $date1 = $triggerOn;
//                    $date2 = date("Y-m-d H:i:s");
////Convert them to timestamps.
//                    $date1Timestamp = strtotime($date1);
//                    $date2Timestamp = strtotime($date2);
////Calculate the difference.
//                    $difference = $date1Timestamp - $date2Timestamp;
//                    $minutes = floor($difference / 60);
//
//                    if ($minutes < -5) {
//                        $update["code"] = $code;
//                        $update["sent_time"] = date("Y-m-d H:i:s");
//                        $this->Birthday->update("otp", $update, "mobile_no='$mobile_no'");
//                    } else {
//                        $data["code"] = $code = $otp_data[0]["code"];
//                    }
//                } else {
//                    $data["created_date"] = date("Y-m-d H:i:s");
//                    $data["sent_time"] = date("Y-m-d H:i:s");
//                    $this->Birthday->addData("otp", $data);
//                }
//                $show_message = "Enter $code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone.";
//                try {
//                    $resp = $this->Birthday->send_mobile_otp($data["mobile_no"], $show_message);
//                    $output["success"] = true;
//                    $output["message"] = "OTP sent successfully";
//                } catch (Exception $e) {
//                    $output["success"] = true;
//                    $output["message"] = "Failed.Please try again";
//                }
//            } else {
//                $output["success"] = FALSE;
////                $output["message"] = "Given Mobile Number is not registered in BirthdayOwl.com";
//                $output["message"] = "No account found for this Mobile Number";
//            }
//        } else {
//            $output["success"] = FALSE;
//            $output["message"] = "No input found";
//        }
//        $this->output->set_content_type('application/json');
//        $this->output->set_output(json_encode($output));
//    }

    public function resend_otp() {
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $data["mobile_no"] = $mobile_no = check_post($this->input->post("mobile_no"));
            $hashkey = " ";
            $otp_data = $this->Birthday->getSelectData("*", "otp", "mobile_no=$mobile_no")->result_array();
//            $check_mb = $this->Birthday->getSelectData("mobile_no", "users", "mobile_no=$mobile_no and (is_guest=0 or is_guest=1) and mobile_verify=0")->result_array();
            if (count($otp_data) > 0) {
                $data["code"] = $code = $this->generateRandomString(5);
//                $otp_data = $this->Birthday->getSelectData("*", "otp", "mobile_no=$mobile_no")->result_array();
                $count = count($otp_data);

                $delivery_date_time = $otp_data[0]["sent_time"];
                $triggerOn = date('Y-m-d H:i:s', strtotime($delivery_date_time));
                $date1 = $triggerOn;
                $date2 = date("Y-m-d H:i:s");
//Convert them to timestamps.
                $date1Timestamp = strtotime($date1);
                $date2Timestamp = strtotime($date2);
//Calculate the difference.
                $difference = $date1Timestamp - $date2Timestamp;
                $minutes = floor($difference / 60);

                if ($minutes < -5) {
                    $update["code"] = $code;
                    $update["sent_time"] = date("Y-m-d H:i:s");
                    $this->Birthday->update("otp", $update, "mobile_no='$mobile_no'");
                } else {
                    $data["code"] = $code = $otp_data[0]["code"];
                }
                $show_message = "Enter $code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone.";
                try {
                    $resp = $this->Birthday->send_mobile_otp($data["mobile_no"], $show_message);
                    $output["success"] = true;
                    $output["message"] = "OTP sent successfully";
                } catch (Exception $e) {
                    $output["success"] = true;
                    $output["message"] = "Failed.Please try again";
                }
            } else {
                $output["success"] = FALSE;
//                $output["message"] = "Given Mobile Number is not registered in BirthdayOwl.com";
                $output["message"] = "No account found for this Mobile Number";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

// ====================================================================================================================================//
// -> Function     :otp_reset
// -> Executed     :cronjob calles this function 
// -> description  :It checks OTP sent time is greater than 6 or not if yes then updates code to 0
// ====================================================================================================================================//  

    public function otp_reset() {
        $reset_req = $this->Birthday->getSelectData("mobile_no,oid", "otp", "TIMESTAMPDIFF(MINUTE,sent_time,NOW())> 6")->result_array();
        $resetcount = count($reset_req);
        if ($resetcount > 0) {
            for ($i = 0; $i < $resetcount; $i++) {

                $updateArray[] = array(
                    'code' => "0",
                    'oid' => $reset_req[$i]["oid"]
                );
            }
            $this->Birthday->updateBatch("otp", $updateArray, "oid");
        }
    }

// ====================================================================================================================================//
// -> Function     :verify_otp
// -> Executed     :After submitting OTP code this function get called
// -> description  :It verifies OTP and sends reset password link to associated email id.
// ====================================================================================================================================//  

    public function verify_otp() { // this is for passord reset by otp
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $otp = check_post($this->input->post("otp"));
            $otp_data = $this->Birthday->getSelectData("mobile_no,oid", "otp", "code='$otp'")->row_array();
            $count = count($otp_data);
            if ($count > 0) {
                $mobile_no = $otp_data["mobile_no"];
                $userdata = $this->Birthday->getSelectData("mobile_no,email,first_name,user_id", "users", "mobile_no=$mobile_no")->row_array();
                $userdatacount = count($userdata);
                if ($userdatacount > 0) {
                    $data["username"] = $email = $userdata["email"];
                    $data["realName"] = $name = $userdata["first_name"];
                    $user_id = $userdata['user_id'];
                    $euserid = $this->Birthday->encryptPassword($user_id);
                    $data["link"] = BASEURL . "Home_web/change_password/$euserid";
                    $view = $this->load->view("email_template/ResetPassword", $data, true);
                    $result = send_EmailResponse($view, $email, $name, "resetpassword@birthdayowl.com", "birthdayowl.com", "BirthdayOwl : Reset Password");
                    if ($result["code"] == "success") {
                        $reset["reset_activate"] = 1;
                        $reset["reset_time"] = date("Y-m-d H:i:s");
                        $this->Birthday->update("users", $reset, "user_id = $user_id");
                        $otp_reset["code"] = "0";
                        $oid = $otp_data["oid"];
                        $this->Birthday->update("otp", $otp_reset, "oid = $oid");
                    }

                    $output["success"] = true;
                    $output["message"] = "We have sent Password recovery details to $email ";
                } else {
                    $output["success"] = false;
                    $output["message"] = "We do not have any account associated with $mobile_no.Please signup and create new account.";
                }
            } else {
                $output["success"] = false;
                $output["message"] = "Wrong OTP.Try again";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function verify_otp_signup() { //this is for the sign up otp
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $is_profile_edit = check_post($this->input->post("is_profile_edit"));
            $is_guest = check_post($this->input->post("is_guest"));
//            $update_phone_value_guest = check_post($this->input->post("update_phone_value_guest"));
//            $is_update_phone_number_guest = (check_post($this->input->post("is_update_phone_number_guest")) == 'true' ? true : false);
            $user_mobile_number = check_post($this->input->post("mobile_no"));
            if ($is_profile_edit == '0') {
                $otp = check_post($this->input->post("otp"));
                $user_id = check_post($this->input->post("user_id"));
//Get Mobile number of User whose OTP has to verify
                $where = "user_id='$user_id'";
                $get_user_details = $this->Birthday->getUserData("users", $where)->row_array();

                $mobile_no_detail = $user_mobile_number;
//                $mobile_no_detail = ($is_update_phone_number_guest ? $update_phone_value_guest : $get_user_details['mobile_no']);
//Check whether OTP & mobile number is same or not                
                $otp_data = $this->Birthday->getSelectData("mobile_no,oid", "otp", "code='$otp' and mobile_no = '$mobile_no_detail'")->row_array();
                $count = count($otp_data);
                if ($count > 0) {
                    $mobile_no = $otp_data["mobile_no"];
//                    $where = "mobile_no=$mobile_no and user_id='$user_id'";
//Update is_guest key if user is not guest
                    if ($is_guest == 0) {
                        $update["is_guest"] = 0;
                        $update["is_google_login"] = 0;
                    }
//                    if ($is_update_phone_number_guest) {
//                        $update["mobile_no"] = $update_phone_value_guest;
//                    }
                    $update["mobile_no"] = $user_mobile_number;
                    $update["mobile_verify"] = 1;
                    $update['u_last_login'] = date("Y-m-d H:i:s");
                    $this->Birthday->update("users", $update, $where);
//Get User data for setting session
                    $check_user = $this->Birthday->getUserData("users", $where)->result_array();
                    $userdatacount = count($check_user);
                    if ($userdatacount > 0) {
                        $output["userdata"] = $check_user[0];
                        $output["userdata"]['flower_data'] = array();
                        $output["userdata"]['voucher_data'] = array();
                        $output["success"] = true;
                        $output["is_user_exist"] = ($is_guest == 1 ? 2 : 1); //User is Exist as Guest User
                        if ($is_guest == 1) {
                            $output["session_guest_id"] = session_id();
                        }
                        $this->session->set_userdata($output);
                        $data["email"] = $uemail = $check_user[0]["email"];
                        $data["username"] = $firstname = $check_user[0]["first_name"];
                        $data["mobile_no"] = $check_user[0]["mobile_no"];
                        //for platform
                        $data["from_platform"] = "Web";
                        $password = $check_user[0]["password"];
                        $pass = $this->Birthday->decryptPassword($password);
                        $data["password"] = $pass;
                        $data["username"] = $check_user[0]["first_name"];
                        $view = $this->load->view("email_template/registered_success", $data, true);
//If user is not guest then send mail                        
                        if ($check_user[0]["is_guest"] == 0) {
                            //To user
                            $result = send_EmailResponse($view, $uemail, $firstname, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");

                            //To Support Birthday Owl
                            $view1 = $this->load->view("email_template/register_support", $data, true);
                            send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $uemail($firstname).");
                            if ($result["code"] == "success") {
                                $output["success"] = true;
                            } else {
                                $output["success"] = false;
                            }
                        } else {//If user is guest then add it to guest table
                            $gsdata['user_id'] = $user_id;
                            $gsdata['guest_email_id'] = $uemail;
                            $gsdata['guest_mobile_no'] = $mobile_no_detail;
                            $gsdata['guest_name'] = $firstname;
                            $gsdata['is_active'] = 1;
                            $gsdata['mobile_verify'] = 1;
                            $gsdata['created_date'] = date("Y-m-d H:i:s");

//Check user is already added to guest table or not                            
                            $where = "guest_email_id='$uemail'";
                            $check_user = $this->Birthday->getSelectData("guest_email_id", "guest_user", $where)->result_array();

                            if (count($check_user) > 0) {
                                
                            } else {
                                $insert_id = $this->Birthday->addData("guest_user", $gsdata);
                            }
                            $output["success"] = true;
                        }
                        $output["message"] = "Registered successfully";
                        $this->session->set_userdata($output);
                    }
                } else {
                    $output["success"] = false;
                    $output["message"] = "Wrong OTP.Try again";
                }
            } else {

                $otp = check_post($this->input->post("otp"));
                $user_id = $this->session->userdata["userdata"]["user_id"];

                $updated_data["mobile_no"] = $mobile_no = $mobile_no_detail = check_post($this->input->post("mobile_no"));

                $otp_data = $this->Birthday->getSelectData("mobile_no,oid", "otp", "code='$otp' and mobile_no = '$mobile_no_detail'")->row_array();

                $count = count($otp_data);

                if ($count > 0) {
                    if ($this->logged_in()) {

                        $updated_data["first_name"] = check_post($this->input->post("first_name"));
//                        $updated_data["mobile_no"] = $mobile_no = check_post($this->input->post("mobile_no"));
                        $updated_data["email"] = $email = check_post($this->input->post("email"));

                        $password = check_post($this->input->post("password"));
                        $updated_data["password"] = $this->Birthday->encryptPassword($password);
                        $updated_data["gender"] = check_post($this->input->post("gender"));
                        $updated_data["country_id"] = check_post($this->input->post("country_id"));
                        $bmonth = check_post($this->input->post("bmonth"));
                        $bday = check_post($this->input->post("bdate"));
                        $byear = check_post($this->input->post("byear"));
                        $updated_data["birth_date"] = $bday . "/" . $bmonth . "/" . $byear;
                        if (($bmonth != '') && ($bday != '')) {
                            $zodiac_info = $this->Birthday->getUserData("mapzodiac", "(fdate <='$bday' and fmonth='$bmonth') or (tdate>='$bday' and tmonth ='$bmonth')")->row_array();

                            $updated_data["zodiac_id"] = $zodiac_info["zodiac_id"];
                            $updated_data["bmonth"] = $bmonth;
                            $updated_data["bdate"] = $bday;
                            $updated_data["byear"] = $byear;
                        } else {
                            $updated_data["bmonth"] = '';
                            $updated_data["bdate"] = '';
                            $updated_data["byear"] = '';
                        }
                        $updated_data["updated_date"] = date("Y-m-d H:i:s");

                        $where = "user_id='$user_id'";
                        $updated_data = $this->Birthday->update("users", $updated_data, $where);
                        if ($updated_data == 1) {
                            $user_info = $this->Birthday->getUserData("users", $where)->row_array();
                            $data["userdata"] = $user_info;
                            $data["userdata"]["flower_data"] = $this->session->userdata["userdata"]["flower_data"];
                            $data["userdata"]["voucher_data"] = $this->session->userdata["userdata"]["voucher_data"];
                            $this->session->set_userdata($data);
                            $output["success"] = true;
                            $output["message"] = "Profile is updated successfully!";
                            $output["is_mobile_changed"] = 0;
                        }
                    } else {
                        $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again to continue");
                        redirect(WEB_HOME);
                    }
                } else {
                    $output["success"] = false;
                    $output["message"] = "Wrong OTP.Try again";
                }
            }

            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

// ====================================================================================================================================//
// -> Function     :verify_otp
// -> Executed     :After submitting OTP code this function get called
// -> description  :It verifies OTP and sends reset password link to associated email id.
// ====================================================================================================================================//  

    function subval_sort($a, $subkey) {
        foreach ($a as $k => $v) {
            $b[$k] = strtolower($v[$subkey]);
        }
        asort($b);
        foreach ($b as $key => $val) {
            $c[] = $a[$key];
        }
        return $c;
    }

    function getVoucherNames() {
        $product_voucher = getAllVouchers();
        $voucher_name = array_column($product_voucher, 'voucher_pro_name');
        return $voucher_name;
    }

    function getVoucher() {
        $product_voucher = getAllVouchers();
        $voucher_name = array_column($product_voucher, 'voucher_pro_name');
        $voucher_name1 = array_column($product_voucher, 'voucher_url');
        $output = array_combine($voucher_name, $voucher_name1);
        $jsonArray = array();
        foreach ($output as $name => $value) {
            $jsonArray[] = array('vname' => $name, 'vurl' => $value);
        }
        $output["marray"] = $jsonArray;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
//return $one_array;
    }

    function getVoucherAndFlower() {
        $product_voucher = getAllVouchers();
        $product_voucher = array_values($product_voucher);

        $jsonArray = array();

//        for ($i = 0; $i < count($product_voucher); $i++) {
//            $voucher_pro_name = $product_voucher[$i]['voucher_pro_name'];
//            $voucher_url = $product_voucher[$i]['voucher_url'];
//            $voucher_pro_id = $product_voucher[$i]['voucher_pro_id'];
//            $jsonArray[] = array('id' => $voucher_pro_id, 'name' => $voucher_pro_name, 'url' => $voucher_url);
//        }
        $flowerdata = $this->Birthday->getSelectDataOrderBy1()->result_array();
        for ($i = 0; $i < count($flowerdata); $i++) {
            $flower_id = $flowerdata[$i]['pro_id'];
            $flower_name = $flowerdata[$i]['product_name'];
            $jsonArray[] = array('id' => $flower_id, 'name' => $flower_name, 'url' => '');
        }

        $output = $jsonArray;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function get_data() {
        $filename = time() . '.jpg';
        $filepath = 'public/imageVideo/';

        $result = file_put_contents($filepath . $filename, file_get_contents('php://input'));
        if (!$result) {
            print "ERROR: Failed to write data to $filename, check permissions\n";
            exit();
        }

        echo $filepath . $filename;
    }

    public function upload_video() {
        $fileName = $_POST["userfile"];
        $filename = "";
        $upload = FALSE;
        $tempFile = trim($_FILES['videofile']['tmp_name']);
        $filename = str_replace("%20", "_", $_POST['userfile']);
        $filename = str_replace(" ", "_", $filename);
        $targetPath = "public/imageVideo/";
        if (!file_exists($targetPath)) {
            mkdir($targetPath, 0777, true);
        }
        $targetFile = str_replace('//', '/', $targetPath) . $filename;
        if (!@copy($tempFile, $targetFile)) {
            if (!@move_uploaded_file($tempFile, $targetFile)) {
                $output["success"] = FALSE;
                $output["error_no"] = 6;
                $output["message"] = "File Cannot Be Uploaded";
                $output["error"] = $_FILES["userfile"]["error"];
            } else {
                $output["filename"] = $fileName;
                $output["success"] = true;
            }
        } else {
            $output["filename"] = $fileName;
            $output["success"] = true;
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function upload_video_new() {
        $output = array();
        $filename = "";
        $upload = FALSE;
//        if (!empty($_FILES)) {
        $tempFile = trim($_FILES['userfile']['tmp_name']);
        $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
        $filename = str_replace(" ", "_", $filename);
        $targetPath = 'public/imageVideo/';
        if (!file_exists($targetPath)) {
            mkdir($targetPath, 0777, true);
        }
        $targetFile = str_replace('//', '/', $targetPath) . $filename;


        if (!@copy($tempFile, $targetFile)) {

            if (!@move_uploaded_file($tempFile, $targetFile)) {

                $output["success"] = false;
                $output["error_no"] = 6;
                $output["message"] = "File Cannot Be Uploaded";
                $output["error"] = $_FILES["userfile"]["error"];
            } else {

                $output["filename"] = $filename;
                $output["success"] = true;
                $upload = TRUE;
            }
        } else {
            $file_parts = pathinfo($filename);
            $type = $file_parts["extension"];
            $file_new = $file_parts["filename"];
            $new_file_name = $targetPath . $file_new . '_comp.webm';
            if ($type == "mp4") {
                exec("ffmpeg -i '$targetFile' -vcodec libvpx -qmin 0 -qmax 50 -crf 10 -b:v 1M -acodec libvorbis '$new_file_name'", $output, $return);

                if (!$return) {
                    $output["filename"] = $file_new . '_comp.webm';
                    $output["message"] = $file_new . '_comp.webm' . " uploaded successfully";
                } else {
                    $output["filename"] = $file_new . '_comp.webm';
                    $output["message"] = $file_new . '_comp.webm' . " is not uploaded successfully";
                }
            } else {
                $output["filename"] = $filename;
                $output["message"] = "$filename uploaded successfully";
            }
            $output["success"] = true;
            $upload = TRUE;
        }
//        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function delete_uploadedimg() {
        $filename = check_post($this->input->post("deleteimg"));
        if (file_exists($filename)) {
            unlink($filename);
            $output["success"] = true;
        } else {
            $output["success"] = false;
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function search() {
        $data = array();
        $search_name = $this->uri->segment(2);
        $data["product_voucher"] = getAllVouchers();
        $vouchers_array = $data["product_voucher"];
        $keys = array();
        foreach ($vouchers_array as $value) {
            if (stristr($value["voucher_pro_name"], str_replace('%20', ' ', $search_name)) || stristr($value["voucher_url"], $search_name) || (strcasecmp($value["voucher_pro_name"], str_replace('%20', ' ', $search_name)) == 0)) {
                $keys[] = $value;
            }
        }
        $keycount = count($keys);
        if ($keycount > 0) {
            $data["voucher_info"] = $keys;
            $data["keyword"] = str_replace('%20', ' ', $search_name);
            $data["count"] = count($data["voucher_info"]);
            $this->session->set_userdata($data);
        } else {
            $data["voucher_info"] = $keys;
            $data["keyword"] = str_replace('%20', ' ', $search_name);
            $data["count"] = count($data["voucher_info"]);
        }
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=1")->row_array();
        $data["seo"] = $title;
        $this->load->view('HOME/search_result', $data);
    }

    public function search_by_post() {
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $search_name = check_post($this->input->post("search_name"));
            $product_data = $this->Birthday->getSearchProduct($search_name)->result_array();
            if (count($product_data) > 0) {
                $output["success"] = true;
                $output["voucher_info"] = $product_data;
                $output["keyword"] = $search_name;
                $output["voucher_url"] = $data["voucher_url"] = $product_data[0]["voucher_url"];
                $output["voucher_pro_id"] = $data["voucher_pro_id"] = $product_data[0]["voucher_pro_id"];
                $this->session->set_userdata($data);
            } else {
                $output["success"] = false;
                $output["message"] = "No result found";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function get_cart_count_old() {
        $total = 0;
        if ($this->check_logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $output["carts"] = $cart_count = $this->Birthday->getJoinDataGroupBy("u.is_guest,o.order_pro_id,o.voucher_pro_id,o.payment_id,o.same_quantity_id,o.quantity,o.order_id,o.voucher_pro_name,o.unique_oid,o.selected_orders,o.delievery_schedule,o.delivery_date_time,o.selected_amount,o.voucher_img,o.femail,o.fname,o.fphone,g.front_page_image ,sum(o.selected_amount) as selected_amount_new", "order_products o", "greeting_card g", "users u", "g.card_id=o.greeting_id", "u.user_id=o.user_id", "o.user_id=$user_id and o.selected_orders=0 and (o.delivery_status='' or delivery_status=3) and u.is_guest=0 ", "o.same_quantity_id", "`o`.`order_pro_id` ASC")->result_array();
        } else if ($this->check_guest_logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $guest_session = check_post($this->input->post("guest_session"));
            $guest_users = $this->Birthday->getSelectData("user_id,uguest_user_id,guest_session", "order_products", "guest_session='$guest_session'")->result_array();
            $gecount = count($guest_users);
            if ($gecount > 0) {
                $updata = array();
                for ($i = 0; $i < $gecount; $i++) {
                    $updateArray[] = array(
                        'guest_session' => $guest_session,
                        'user_id' => $user_id
                    );
                }
                $this->Birthday->updateBatch("order_products", $updateArray, "guest_session");
            }
            $output["carts"] = $cart_count = $this->Birthday->getJoinDataGroupBy("u.is_guest,o.order_pro_id,o.voucher_pro_id,o.payment_id,o.same_quantity_id,o.quantity,o.order_id,o.voucher_pro_name,o.unique_oid,o.selected_orders,o.delievery_schedule,o.delivery_date_time,o.selected_amount,o.voucher_img,o.femail,o.fname,o.fphone,g.front_page_image ,sum(o.selected_amount) as selected_amount_new", "order_products o", "greeting_card g", "users u", "g.card_id=o.greeting_id", "u.user_id=o.user_id", "o.user_id=$user_id and o.selected_orders=0 and (o.delivery_status='' or delivery_status=3) and u.is_guest=1 ", "o.same_quantity_id", "`o`.`order_pro_id` ASC")->result_array();
        } else {
            $output["carts"] = array();
        }

        if (count($output["carts"]) > 0) {
            foreach ($output["carts"] as $carts) {
                $total += $carts["selected_amount_new"];
            }
        }
        $processing_fee = ($total * 0.035);
        $total_all = ($total + $processing_fee);
        $output["total"] = $total_all;
        $output["cart_count"] = count($output["carts"]);
        $output["message"] = "";
        $output["success"] = true;
//        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function get_view_cart_old() {
        if ($this->check_logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $output["view_cart"] = $cart_count = $this->Birthday->getSelectData("order_pro_id", "order_products", "selected_orders='0' and user_id=$user_id")->result_array();
        } else if ($this->check_guest_logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $output["view_cart"] = $cart_count = $this->Birthday->getSelectData("order_pro_id", "order_products", "selected_orders='0' and user_id=$user_id")->result_array();
        } else {
            $output["view_cart"] = 0;
        }
        $output["message"] = "";
        $output["success"] = true;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function get_view_cart() {
        $output["view_cart"] = array();
        $is_purchased = 0;
        if ($this->check_logged_in()) {
//            $user_id = $this->session->userdata["userdata"]["user_id"];
//            $output["view_cart"] = $cart_count = $this->Birthday->getSelectData("order_pro_id", "order_products", "selected_orders='0' and user_id=$user_id")->result_array();
//for voucher
//            $voucher = $this->Birthday->getSelectData("order_pro_id", "order_products", "selected_orders='0' and user_id=$user_id")->result_array();
            $voucher = $this->session->userdata["userdata"]["voucher_data"];
            $count_voucher = count($voucher);

            if ($count_voucher != 0) {
                $is_purchased = 1; // only voucher purchased
            }

//for flower
            if (count($this->session->userdata["userdata"]["flower_data"]) != 0) {
                $flower = $this->session->userdata["userdata"]["flower_data"];  // take this flower data when similar to voucher on view side
            } else {
                $flower = array();
            }
            $count_flower = count($flower);
            if ($count_flower != 0) {
                $is_purchased = 2; // only flower purchased
            }

            if (!empty($flower) && !empty($voucher)) {
                $output["view_cart"] = array_merge($voucher, $flower);
            } else {
                if ($is_purchased == 1) {
                    $output["view_cart"] = $voucher;
                }

                if ($is_purchased == 2) {
                    $output["view_cart"] = $flower;
                }
            }
        } else if ($this->check_guest_logged_in()) {
//            $user_id = $this->session->userdata["userdata"]["user_id"];
//            $output["view_cart"] = $cart_count = $this->Birthday->getSelectData("order_pro_id", "order_products", "selected_orders='0' and user_id=$user_id")->result_array();
//for voucher
//            $voucher = $this->Birthday->getSelectData("order_pro_id", "order_products", "selected_orders='0' and user_id=$user_id")->result_array();
            $voucher = $this->session->userdata["userdata"]["voucher_data"];
            $count_voucher = count($voucher);

            if ($count_voucher != 0) {
                $is_purchased = 1; // only voucher purchased
            }

//for flower
            if (count($this->session->userdata["userdata"]["flower_data"]) != 0) {
                $flower = $this->session->userdata["userdata"]["flower_data"];  // take this flower data when similar to voucher on view side
            } else {
                $flower = array();
            }
            $count_flower = count($flower);
            if ($count_flower != 0) {
                $is_purchased = 2; // only flower purchased
            }

            if (!empty($flower) && !empty($voucher)) {
                $output["view_cart"] = array_merge($voucher, $flower);
            } else {
                if ($is_purchased == 1) {
                    $output["view_cart"] = $voucher;
                }

                if ($is_purchased == 2) {
                    $output["view_cart"] = $flower;
                }
            }
        } else {
            $output["view_cart"] = 0;
        }
        $output["message"] = "";
        $output["success"] = true;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

//voucher functions
//It is used to get vouchers
    public function get_voucher_data() {
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $voucher_pro_id = check_post($this->input->post("voucher_pro_id"));
//            $data["product_voucher"] = getAllVouchers(); //Old
            $product_vouchers = getAllVouchers();
            $reindexed_array = array_values($product_vouchers);
//            echo'<pre>';
//            print_r($reindexed_array);
            $key = array_search($voucher_pro_id, array_column($reindexed_array, 'voucher_pro_id')); // $key = 2;
            $product_data = $reindexed_array[$key];
            $procount = count($product_data);
            if ($procount > 0) {
                $output["success"] = true;
                $output["product_data"] = $data["product_data"] = $product_data;
                $data["min"] = $product_data["min_custom_price"];
                $data["max"] = $product_data["max_custom_price"];
                $data["voucher_url"] = $product_data["voucher_url"];
                $data["voucher_pro_id"] = $product_data["voucher_pro_id"];
                $data["terms_conditions"] = $product_data["terms_conditions"];
                $data["is_social"] = 0;
                $this->session->set_userdata($data);
            } else {
                $output["success"] = false;
                $output["message"] = "";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function set_Voucher_sessionData() {
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $uploaded_img = check_post($this->input->post("uploaded_finalimg"));
            $uploadedpic = str_replace('public/imageVideo', '', $uploaded_img);
            if ($uploadedpic != "") {
                $data["uploded_img_video"] = $uploadedpic;
            } else {
                $data["uploded_img_video"] = check_post($this->input->post("imgcaptureuploaded"));
            }
            $data["created_date"] = date("Y-m-d H:i:s");
            $data["rfname"] = check_post($this->input->post("fname"));
            $data["quantity"] = check_post($this->input->post("quantity_new"));
            $data["voucher_pro_id"] = check_post($this->input->post("voucher_pro_id"));
            $data["voucher_pro_img"] = check_post($this->input->post("voucher_pro_img"));
            $data["voucher_pro_name"] = check_post($this->input->post("voucher_pro_name"));
            $data["vurl"] = check_post($this->input->post("vurl"));
            $data["rfemail"] = check_post($this->input->post("femail"));
            $data["greeting_message"] = check_post($this->input->post("greeting_message"));
            $data["selected_amount"] = check_post($this->input->post("selected_amount"));
            $data["fphone"] = check_post($this->input->post("fphone"));
            $data["greeting_id"] = check_post($this->input->post("greeting_id"));
            $data["delievery_schedule"] = check_post($this->input->post("delievery_schedule"));
            $data["delivery_date_time"] = check_post($this->input->post("delivery_date_time"));
            $data["delivery_time"] = check_post($this->input->post("delievery_time"));
            $data["delivery_date"] = check_post($this->input->post("delivery_date"));
            $data["type"] = check_post($this->input->post("type"));
            $data["redirect"] = check_post($this->input->post("redirect"));
            $data["timezone"] = check_post($this->input->post("timezone"));
            $data["is_social"] = check_post($this->input->post("is_social"));
            $data["rlname"] = "";
            $data["rpincode"] = "";
            $data["slname"] = "";
            $data["smobile"] = "";
            $data["spincode"] = "";
            $data["semail"] = "";
            $data["sfname"] = "";
            $this->session->set_userdata($data);
            $output["success"] = true;
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

// ====================================================================================================================================//
// -> Function     : get_cart
// -> Executed     : on click of checkout, add another in inside_voucher view  
// -> description  :adds egift in database
// ====================================================================================================================================//

    public function get_cart() {
        $data = array();
        if (count($_POST) > 0) {
            if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
                $quantity = check_post($this->input->post("quantity_new"));
                $redirect = check_post($this->input->post("redirect"));

                $delivery_date_time = check_post($this->input->post("delivery_date_time"));
//                $delivery_date_time = "2018-04-06 11:30:00";
                $triggerOn = date('Y-m-d H:i:s', strtotime($delivery_date_time));
                $sdate = date("Y-m-d", strtotime($triggerOn));
                $datetime1 = new DateTime($sdate);
                $datetime2 = new DateTime(date("Y-m-d"));
                $interval = $datetime1->diff($datetime2);
                $date_diff = $interval->format('%a');
                switch ($date_diff) {
                    case 0:
                        $delievery_schedule = 1;
                        break;
                    case 1:
                        $delievery_schedule = 2;
                        break;
                    default:
                        $delievery_schedule = 3;
                        break;
                }
//                $delievery_schedule = check_post($this->input->post("delievery_schedule"));
                $delivery_time = check_post($this->input->post("delievery_time"));
                $delivery_date = check_post($this->input->post("delivery_date"));
                $timezone = check_post($this->input->post("timezone"));
                $is_guest = check_post($this->input->post("is_guest"));
                $data["quantity"] = $quantity;
                $data["user_id"] = $this->session->userdata["userdata"]["user_id"];
                $data["voucher_pro_id"] = $this->session->userdata("voucher_pro_id");
                $data["voucher_img"] = $this->session->userdata["product_data"]["product_image"];
                $data["terms"] = $this->session->userdata("terms_conditions");
                $data["voucher_pro_name"] = $this->session->userdata["product_data"]["voucher_pro_name"];
                $data["uemail"] = $this->session->userdata["userdata"]["email"];
                $data["uname"] = $this->session->userdata["userdata"]["first_name"];
                if ($is_guest == 1)
                    $data["uguest_user_id"] = $this->session->userdata["userdata"]["user_id"];
                else
                    $data["uguest_user_id"] = 0;
                $data["timezone"] = $timezone;
                $uploaded_img = check_post($this->input->post("uploaded_finalimg"));
                $uploadedpic = str_replace('public/imageVideo', '', $uploaded_img);
                $data["uploded_img_video"] = $uploadedpic;
                $data["order_id"] = "";
                $data["type"] = check_post($this->input->post("type"));
                $data["payment_id"] = "";
                $data["created_date"] = date("Y-m-d H:i:s");
                $data["delivery_status"] = "";
                $data["fname"] = check_post($this->input->post("fname"));
                $data["femail"] = check_post($this->input->post("femail"));
                $data["greeting_message"] = check_post($this->input->post("greeting_message"));
                $data["selected_amount"] = check_post($this->input->post("selected_amount"));
                $data["guest_session"] = check_post($this->input->post("guest_session"));
                $data["fphone"] = check_post($this->input->post("fphone"));
                $data["greeting_id"] = check_post($this->input->post("greeting_id"));
                $data["delievery_schedule"] = $delievery_schedule;
                $data["order_message"] = "";
                $data["woohoo_order_id"] = "";
                $data["unique_oid"] = "";
                $data["same_quantity_id"] = "";
//                $returndata = DateConversion($delievery_schedule, $delivery_date, $delivery_time, $timezone);
                $returndata = DateConversion_new($delivery_date_time, $timezone);
                $data["delivery_date_time"] = $returndata["delivery_date_time"];
                $data["converted_date"] = $returndata["converted_date"];
                $insertid = $this->Birthday->addData("order_products", $data);
                $updated_data["same_quantity_id"] = "$insertid";
                $this->Birthday->update("order_products", $updated_data, "order_pro_id='$insertid'");
                $output["success"] = true;
                $output["redirect"] = $redirect;
            }else {
                $output["success"] = false;
//                $output["message"] = "Sorry your session has been expired.Please login again to send eGift!";
                $output["message"] = "Sorry your session has been expired.Please login again!";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

// ====================================================================================================================================//
// -> Function     : updateCart
// -> Executed     : on click of update, add another in inside_voucher view  
// -> description  :Updates egift in database
// ====================================================================================================================================//


    public function updateCart() {
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
                $order_pro_id = check_post($this->input->post("order_pro_id"));
                $fetch_order = $this->Birthday->getSelectData("voucher_pro_id,voucher_img,terms,voucher_pro_name", "order_products", "order_pro_id=$order_pro_id")->row_array();
                $timezone = check_post($this->input->post("timezone"));
                $quantity = check_post($this->input->post("quantity_new"));
                $same_quantity_id = check_post($this->input->post("same_quantity_id"));
//                $delievery_schedule = check_post($this->input->post("delievery_schedule")); //explode(",", check_post($this->input->post("delievery_schedule")));
                $delivery_time = check_post($this->input->post("delievery_time"));
                $delivery_date = check_post($this->input->post("delivery_date"));

                $delivery_date_time = check_post($this->input->post("delivery_date_time"));
                $triggerOn = date('Y-m-d H:i:s', strtotime($delivery_date_time));
                $sdate = date("Y-m-d", strtotime($triggerOn));
                $datetime1 = new DateTime($sdate);
                $datetime2 = new DateTime(date("Y-m-d"));
                $interval = $datetime1->diff($datetime2);
                $date_diff = $interval->format('%a');
                switch ($date_diff) {
                    case 0:
                        $delievery_schedule = 1;
                        break;
                    case 1:
                        $delievery_schedule = 2;
                        break;
                    default:
                        $delievery_schedule = 3;
                        break;
                }
                for ($i = 0; $i < $quantity; $i++) {
                    $data[$i]["same_quantity_id"] = $same_quantity_id;
                    $data[$i]["quantity"] = $quantity;
                    $data[$i]["timezone"] = $timezone;
                    $data[$i]["user_id"] = $this->session->userdata["userdata"]["user_id"];
                    $data[$i]["voucher_pro_id"] = $fetch_order["voucher_pro_id"];
                    $data[$i]["voucher_img"] = $fetch_order["voucher_img"];
                    $data[$i]["terms"] = $fetch_order["terms"];
                    $data[$i]["voucher_pro_name"] = $fetch_order["voucher_pro_name"];
                    $data[$i]["uemail"] = $this->session->userdata["userdata"]["email"];
                    $data[$i]["uname"] = $this->session->userdata["userdata"]["first_name"];
                    $uploaded_img = check_post($this->input->post("uploaded_finalimg"));
                    $uploadedpic = str_replace('public/imageVideo', '', $uploaded_img);
                    $data[$i]["uploded_img_video"] = $uploadedpic;
                    $data[$i]["type"] = check_post($this->input->post("type"));
                    $data[$i]["created_date"] = date("Y-m-d H:i:s");
                    $data[$i]["fname"] = check_post($this->input->post("fname"));
                    $data[$i]["femail"] = check_post($this->input->post("femail"));
                    $data[$i]["greeting_message"] = check_post($this->input->post("greeting_message"));
                    $data[$i]["selected_amount"] = check_post($this->input->post("selected_amount"));
                    $data[$i]["fphone"] = check_post($this->input->post("fphone"));
                    $data[$i]["greeting_id"] = check_post($this->input->post("greeting_id"));
                    $data[$i]["delievery_schedule"] = $delievery_schedule;

//                    $returndata = DateConversion($delievery_schedule, $delivery_date, $delivery_time, $timezone);
                    $returndata = DateConversion_new($delivery_date_time, $timezone);

                    $data[$i]["delivery_date_time"] = $returndata["delivery_date_time"];
                    $data[$i]["converted_date"] = $returndata["converted_date"];
                }
                $this->Birthday->updateBatch("order_products", $data, "same_quantity_id");
                $output["success"] = true;
                $output["message"] = "updated successfully";
            } else {
                $output["success"] = false;
//                $output["message"] = "Sorry your session has been expired.Please login again to send eGift!";
                $output["message"] = "Sorry your session has been expired.Please login again!";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function update_voucher_data() {
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
                $order_pro_id = check_post($this->input->post("order_pro_id")); //Unique_id from the view side
                $voucher_data = $this->session->userdata["userdata"]["voucher_data"];
                $voucher_data_unique_array = array_column($voucher_data, 'unique_id');
                $voucher_data_index = array_search("$order_pro_id", $voucher_data_unique_array, TRUE);
                $fetch_order = $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index];
//                $fetch_order = $this->Birthday->getSelectData("voucher_pro_id,voucher_img,terms,voucher_pro_name", "order_products", "order_pro_id=$order_pro_id")->row_array();\\Old

                $selected_amount = check_post($this->input->post("selected_amount"));
                $greeting_id = check_post($this->input->post("greeting_id"));
                $greeting_message = check_post($this->input->post("greeting_message"));
                $receiver_email = check_post($this->input->post("femail"));
                $receiver_name = check_post($this->input->post("fname"));
                $receiver_phone = check_post($this->input->post("fphone"));
                $type = check_post($this->input->post("type"));
                $uploaded_img = check_post($this->input->post("uploaded_finalimg"));
                $timezone = check_post($this->input->post("timezone"));
                $quantity = check_post($this->input->post("quantity_new"));
                $same_quantity_id = check_post($this->input->post("same_quantity_id"));
//                $delievery_schedule = check_post($this->input->post("delievery_schedule")); //explode(",", check_post($this->input->post("delievery_schedule")));
                $delivery_time = check_post($this->input->post("delievery_time")); //not in use
                $delivery_date = check_post($this->input->post("delivery_date")); //not in use

                $delivery_date_time = check_post($this->input->post("delivery_date_time"));

                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["selected_amount"] = $selected_amount;
                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["selected_amount_new"] = $selected_amount * $quantity;
                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["greeting_id"] = $greeting_id;

                $where = "card_id = $greeting_id";
                $greeting_data = $this->Birthday->getSelectData('front_page_image', 'greeting_card', $where)->row_array();
                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]['front_page_image'] = $greeting_data['front_page_image'];

                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["greeting_message"] = $greeting_message;

                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["fname"] = $receiver_name;
                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["femail"] = $receiver_email;
                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["fphone"] = $receiver_phone;

                $triggerOn = date('Y-m-d H:i:s', strtotime($delivery_date_time));
                $sdate = date("Y-m-d", strtotime($triggerOn));
                $datetime1 = new DateTime($sdate);
                $datetime2 = new DateTime(date("Y-m-d"));
                $interval = $datetime1->diff($datetime2);
                $date_diff = $interval->format('%a');
                switch ($date_diff) {
                    case 0:
                        $delievery_schedule = 1;
                        break;
                    case 1:
                        $delievery_schedule = 2;
                        break;
                    default:
                        $delievery_schedule = 3;
                        break;
                }
                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["delievery_schedule"] = $delievery_schedule;

                $uploadedpic = str_replace('public/imageVideo', '', $uploaded_img);
                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["uploded_img_video"] = $uploadedpic;

// $returndata = DateConversion($delievery_schedule, $delivery_date, $delivery_time, $timezone);
                $returndata = DateConversion_new($delivery_date_time, $timezone);
                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["delivery_date_time"] = $returndata["delivery_date_time"];
                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["type"] = $type;
                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["quantity"] = $quantity;
                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["timezone"] = $timezone;
                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["converted_date"] = $returndata["converted_date"];
                $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["unique_id"] = $order_pro_id;
                $output["success"] = true;
                $output["message"] = "updated successfully";
            } else {
                $output["success"] = false;
//                $output["message"] = "Sorry your session has been expired.Please login again to send eGift!";
                $output["message"] = "Sorry your session has been expired.Please login again!";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

// ====================================================================================================================================//
// -> Function     : add_social_voucherdata
// -> Executed     : on click of gmail/fb in inside_voucher view  
// -> description  :adds egift in database
// ====================================================================================================================================//

    public function add_social_voucherdata($id) {
        if ($this->check_logged_in()) {
            $data = array();
            $quantity = $this->session->userdata["quantity"];
            $redirect = $this->session->userdata["redirect"];
            $timezone = $this->session->userdata["timezone"];
            $delievery_schedule = $this->session->userdata["delievery_schedule"]; //explode(",", $this->session->userdata["delievery_schedule"]);
            $delivery_time = $this->session->userdata["delivery_time"]; //explode(",", $this->session->userdata["delivery_time"]);
            $delivery_date = $this->session->userdata["delivery_date"]; // explode(",", $this->session->userdata["delivery_date"]);
            $data["quantity"] = $quantity;
            $data["user_id"] = $this->session->userdata["userdata"]["user_id"];
            $data["voucher_pro_id"] = $this->session->userdata["product_data"]["voucher_pro_id"];
            $data["voucher_img"] = $this->session->userdata["product_data"]["product_image"];
            $data["voucher_pro_name"] = $this->session->userdata["product_data"]["voucher_pro_name"];
            $data["uemail"] = $this->session->userdata["semail"];
            $data["uname"] = $this->session->userdata["sfname"];
            $data["uploded_img_video"] = $this->session->userdata["uploded_img_video"];
            $data["delivery_status"] = "";
            $data["created_date"] = date("Y-m-d H:i:s");
            $data["order_id"] = "";
            $data["type"] = $this->session->userdata["type"];
            $data["unique_oid"] = "";
            $data["timezone"] = $timezone;
            $data["same_quantity_id"] = "";
            $data["order_message"] = "";
            $data["woohoo_order_id"] = "";
            $data["terms"] = $this->session->userdata["terms_conditions"];
            $data["fname"] = $this->session->userdata["rfname"];
            $data["femail"] = $this->session->userdata["rfemail"];
            $data["greeting_message"] = $this->session->userdata["greeting_message"];
            $data["delievery_schedule"] = $delievery_schedule;
            $data["selected_amount"] = $this->session->userdata["selected_amount"];
            $data["fphone"] = $this->session->userdata["fphone"]; //explode(",", $this->session->userdata["fphone"]);;
            $data["greeting_id"] = $this->session->userdata["greeting_id"];
            $returndata = DateConversion($delievery_schedule, $delivery_date, $delivery_time, $timezone);
            $data["delivery_date_time"] = $returndata["delivery_date_time"];
            $data["converted_date"] = $returndata["converted_date"];
            $insertid = $this->Birthday->addData("order_products", $data);
            $updated_data["same_quantity_id"] = "$insertid";
            $this->Birthday->update("order_products", $updated_data, "order_pro_id='$insertid'");

// $this->Birthday->addAData("order_products", $data);
            if ($redirect == 1) {
                redirect(BASEURL . "cart");
            } else {
                redirect(WEB_HOME);
            }
        } else {
//            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again to send eGift!");
            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again!");
            redirect(WEB_HOME);
        }
    }

// ====================================================================================================================================//
// -> Function     : save_quantity
// -> Executed     : on click of save in my_cart view  
// -> description  :Updates egift as per quantity in database
// ====================================================================================================================================//
    public function save_quantity() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
                $order_pro_id = check_post($this->input->post("order_pro_id"));
                $same_quantity_id = check_post($this->input->post("same_quantity_id"));
                $quantity = check_post($this->input->post("quantity"));
                $where = "same_quantity_id='$same_quantity_id' and selected_orders=0";
                $orders = $this->Birthday->getUserData("order_products", $where)->result_array();
                $count_orders = count($orders);
                $db_quantity = $orders[0]["quantity"];
                $add_orders_quantity = $quantity - $db_quantity;
                $delete_orders_quantity = $db_quantity - $quantity;
                if ($quantity > $db_quantity) {
                    for ($i = 0; $i < $add_orders_quantity; $i++) {
                        $data[$i]["voucher_pro_id"] = $orders[0]["voucher_pro_id"];
                        $data[$i]["voucher_pro_name"] = $orders[0]["voucher_pro_name"];
                        $data[$i]["selected_amount"] = $orders[0]["selected_amount"];
                        $data[$i]["user_id"] = $orders[0]["user_id"];
                        $data[$i]["uemail"] = $orders[0]["uemail"];
                        $data[$i]["uname"] = $orders[0]["uname"];
                        $data[$i]["uploded_img_video"] = $orders[0]["uploded_img_video"];
                        $data[$i]["type"] = $orders[0]["type"];
                        $data[$i]["fname"] = $orders[0]["fname"];
                        $data[$i]["femail"] = $orders[0]["femail"];
                        $data[$i]["greeting_message"] = $orders[0]["greeting_message"];
                        $data[$i]["delievery_schedule"] = $orders[0]["delievery_schedule"];
                        $data[$i]["same_quantity_id"] = $orders[0]["same_quantity_id"];
                        $data[$i]["voucher_img"] = $orders[0]["voucher_img"];
                        $data[$i]["fphone"] = $orders[0]["fphone"];
                        $data[$i]["greeting_id"] = $orders[0]["greeting_id"];
                        $data[$i]["delivery_date_time"] = $orders[0]["delivery_date_time"];
                        $data[$i]["converted_date"] = $orders[0]["converted_date"];
                        $data[$i]["timezone"] = $orders[0]["timezone"];
                        $data[$i]["order_id"] = "";
                        $data[$i]["payment_id"] = "";
                        $data[$i]["delivery_status"] = "";
                        $data[$i]["quantity"] = $quantity;
                    }
                    $this->Birthday->addAData("order_products", $data);
                    for ($i = 0; $i < $count_orders; $i++) {
                        $updateArray[] = array(
                            'quantity' => $quantity,
                            'order_pro_id' => $orders[$i]["order_pro_id"]
                        );
                    }
                    $this->Birthday->updateBatch("order_products", $updateArray, "order_pro_id");
                } else if ($db_quantity == $quantity) {
                    
                } else if ($quantity < $db_quantity) {
                    $same_quantity_id = $orders[0]["same_quantity_id"];
                    $this->Birthday->delete_reminder("order_products", "same_quantity_id='$same_quantity_id' limit $delete_orders_quantity");
                    for ($i = $delete_orders_quantity; $i < $count_orders; $i++) {
                        $updateArray[] = array(
                            'quantity' => $quantity,
                            'order_pro_id' => $orders[$i]["order_pro_id"]
                        );
                    }
                    $this->Birthday->updateBatch("order_products", $updateArray, "order_pro_id");
                }

                $output["success"] = true;
            } else {
                $output["success"] = false;
//                $output["message"] = "Sorry your session has been expired.Please login again to send eGift!";
                $output["message"] = "Sorry your session has been expired.Please login again!";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

// ====================================================================================================================================//
// -> Function     : delete_my_cart
// -> Executed     : on click of delete in my_cart view  
// -> description  :delete egift in database
// ====================================================================================================================================//

    public function delete_my_cart() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
                $order_id = check_post($this->input->post("same_quantity_id"));
                $where = "same_quantity_id='$order_id'";
                $this->Birthday->delete_reminder("order_products", $where);
                $output["success"] = true;
                $output["message"] = "Removed successfully";
            } else {
                $output["success"] = false;
//                $output["message"] = "Sorry your session has been expired.Please login again to send eGift!";
                $output["message"] = "Sorry your session has been expired.Please login again!";
            }

            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

// ====================================================================================================================================//
// -> Function     : show_cart
// -> Executed     : on click of checkout  my_cart view get load if cart is added successfully 
// -> description  :Displays items from cart
// ====================================================================================================================================// 

    public function show_cart_old() {
        if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
            $data = array();
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $isguest = $this->session->userdata["userdata"]["is_guest"];
            if ($isguest == 1) {
                $guestsession = $this->Birthday->getSelectData("user_id,uguest_user_id,guest_session", "order_products", "user_id='$user_id'")->row_array();
                $guest_session = $guestsession["guest_session"];
                $guest_users = $this->Birthday->getSelectData("user_id,uguest_user_id,guest_session", "order_products", "guest_session='$guest_session'")->result_array();
                if (count($guest_users) > 0) {
                    $updata = array();
                    for ($i = 0; $i < count($guest_users); $i++) {
                        $updateArray[] = array(
                            'guest_session' => $guest_session,
                            'user_id' => $user_id
                        );
                    }
                    $this->Birthday->updateBatch("order_products", $updateArray, "guest_session");
                }
            }
            $data["vouchers"] = $this->Birthday->getJoinDataGroupBy("u.is_guest,o.order_pro_id,o.voucher_pro_id,o.payment_id,o.same_quantity_id,o.quantity,o.order_id,o.voucher_pro_name,o.unique_oid,o.selected_orders,o.delievery_schedule,o.delivery_date_time,o.selected_amount,o.voucher_img,o.femail,o.fname,o.fphone,g.front_page_image ,sum(o.selected_amount) as selected_amount_new", "order_products o", "greeting_card g", "users u", "g.card_id=o.greeting_id", "u.user_id=o.user_id", "o.user_id=$user_id and o.selected_orders=0 and (o.delivery_status='' or delivery_status=3 ) and u.is_guest= $isguest ", "o.same_quantity_id", "`o`.`order_pro_id` ASC")->result_array();
            $data["gift_count"] = count($data["vouchers"]);
            $data["udf2"] = $data["user_id"] = $user_id;
            $user_info = $this->Birthday->getSelectData("email,first_name,mobile_no", "users", "user_id='$user_id'")->row_array();
            $data["user_email"] = $user_info["email"];
            $data["user_first_name"] = $user_info["first_name"];
            $data["uphone"] = $user_info["mobile_no"];
            $data["product_info"] = "BirthdayOwl egift vouchers";
            $data["MERCHANT_KEY"] = MERCHANT_KEY;
            $SALT = PSALT;
            $PAYU_BASE_URL = PAYU_BASE_URL;
            $data["action"] = "";
            $data["hash"] = "";
            $data["amount"] = "";
            $data["firstname"] = "";
            $data["email"] = "";
            $data["productinfo"] = "";
            $data["phone"] = "";
            $data["surl"] = "";
            $data["furl"] = "";
            $posted = array();

            if (!empty($_POST)) {
                foreach ($_POST as $key => $value) {
                    $posted[$key] = $value;
                    $data[$key] = $value;
                }
            }
            $data["formError"] = 0;
            if (empty($posted['txnid'])) {
// Generate random transaction id
                $data["txnid"] = $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            } else {
                $data["txnid"] = $txnid = $posted['txnid'];
            }

            $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
            if (empty($posted['hash']) && sizeof($posted) > 0) {
                if (empty($posted['key']) || empty($posted['txnid']) || empty($posted['amount']) || empty($posted['firstname']) || empty($posted['email']) || empty($posted['productinfo']) || empty($posted['surl']) || empty($posted['furl']) || empty($posted['service_provider'])) {
                    $data["formError"] = 1;
                } else {
                    $hashVarsSeq = explode('|', $hashSequence);
                    $hash_string = '';
                    foreach ($hashVarsSeq as $hash_var) {
                        $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                        $hash_string .= '|';
                    }
                    $hash_string .= $SALT;

                    $data["hash"] = $hash = strtolower(hash('sha512', $hash_string));


                    $data["action"] = $PAYU_BASE_URL . '/_payment';
                }
            } elseif (!empty($posted['hash'])) {
                $data["hash"] = $posted['hash'];

                $data["action"] = $PAYU_BASE_URL . '/_payment';
            }
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=7")->row_array();
            $data["seo"] = $title;
            $this->load->view('EGIFT/my_cart', $data);
        } else {
//            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again to send eGift!");
            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login!");
            redirect(WEB_HOME);
        }
    }

// ====================================================================================================================================//
// -> Function     : display_product
// -> Executed     : on click of  voucher on Home view inside_voucher view get load.
// -> description  :Shows detail information of each voucher
// ====================================================================================================================================//

    public function display_product() {
        if ($this->session->userdata("product_data") != NULL) {
            $data["product_data"] = $this->session->userdata["product_data"];
            $voucher_url = $this->uri->segment(2);
            $data["ordered_data"] = array(
                "fname" => "",
                "femail" => "",
                "fphone" => "",
                "selected_amount" => "",
                "greeting_message" => "Happy Birthday,I thought you would like this gift!",
                "uploded_img_video" => "",
                "greeting_id" => "",
                "delievery_schedule" => "0",
                "quantity" => 1,
                "same_quantity_id" => "",
                "type" => "",
            );
            $data["order_pro_id"] = "";
            $data["checkout"] = "1";
            $data["checkoutval"] = "docheckout";
            $terms = $data["product_data"]["terms_conditions"];
            $data["terms"] = strip_tags($terms, '<br/>');
            $amt_values = explode(",", $data["product_data"]["amount"]);
            $data["pro_id"] = $data["voucher_id"] = $data["product_data"]["voucher_pro_id"];
            $data["min_custom_price"] = $data["product_data"]["min_custom_price"];
            $data["max_custom_price"] = $data["product_data"]["max_custom_price"];
            $data["voucher_name"] = $data["product_data"]["voucher_pro_name"];
            $data["order_handling_charge"] = $data["product_data"]["order_handling_charge"];
            $data["amounts"] = $amt_values;
            $where = "greeting_type=2 order by position=0,position ASC";
            $data["greeting_cat"] = "";
            $cards = $this->Birthday->getSelectData("card_id,front_page_image", "greeting_card", $where)->result_array();
            $data["greeting_cards"] = $cards;
            $data["url"] = $voucher_url;
            $data_session["voucher_url"] = $voucher_url;
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $this->session->set_userdata($data_session);
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=6")->row_array();
            $title["title"] = $data["product_data"]["voucher_pro_name"] . "-birthdayowl.com";
            $title['description'] = $data["product_data"]["voucher_pro_name"] . "-birthdayowl.com";
            $data["seo"] = $title;
            $this->load->view('EGIFT/inside_voucher', $data);
        } else {
            redirect(WEB_HOME);
        }
    }

// ====================================================================================================================================//
// -> Function     : edit_voucher
// -> Executed     : on click edit on my_cart inside_voucher with update get load
// -> description  :Shows detail information of each voucher
// ====================================================================================================================================//

    public function edit_voucher() {
        $order_pro_id = $this->uri->segment(2);
        $data["order_pro_id"] = $order_pro_id;

        $voucher_data = $this->session->userdata["userdata"]["voucher_data"];
        $voucher_data_unique_array = array_column($voucher_data, 'unique_id');

        $order_id = $order_pro_id;
        $voucher_data_index = array_search("$order_id", $voucher_data_unique_array, TRUE);
        $data['ordered_data'] = $ordered_data = $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index];

//        $where = "t1.order_pro_id='$order_pro_id'";
//        $data["ordered_data"] = $ordered_data = $this->Birthday->getTwotableJoinData("order_products", "greeting_card", "t1.*,t2.front_page_image", $where)->row_array();
        $data["checkoutval"] = "docheckoutupdate";
        $voucher_pro_id = $ordered_data["voucher_pro_id"];
        $product_voucher = getAllVouchers();
        $product_voucher = array_values($product_voucher);
        $voucher_product_array = array_column($product_voucher, 'voucher_pro_id');
        $key = array_search("$voucher_pro_id", $voucher_product_array, TRUE);
        $data["product_data"] = $product_voucher[$key];
        $data["url"] = $data["product_data"]["voucher_url"];
        $terms = $data["product_data"]["terms_conditions"];
        $data["terms"] = strip_tags($terms, '<br/>');
        $amt_values = explode(",", $data["product_data"]["amount"]);
        $data["pro_id"] = $data["voucher_id"] = $data["product_data"]["voucher_pro_id"];
        $data["min_custom_price"] = $data["product_data"]["min_custom_price"];
        $data["max_custom_price"] = $data["product_data"]["max_custom_price"];
        $data["voucher_name"] = $data["product_data"]["voucher_pro_name"];
        $data["order_handling_charge"] = $data["product_data"]["order_handling_charge"];
        $data["amounts"] = $amt_values;
        $where = "greeting_type=2 order by card_id DESC";
        $data["greeting_cat"] = '';
        $cards = $this->Birthday->getSelectData("card_id,front_page_image", "greeting_card", $where)->result_array();
        $data["greeting_cards"] = $cards;
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $data["checkout"] = "2";
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=6")->row_array();
        $title["title"] = $data["product_data"]["voucher_pro_name"] . "-birthdayowl.com";
        $title['description'] = $data["product_data"]["voucher_pro_name"] . "-birthdayowl.com";
        $data["seo"] = $title;
        $this->load->view('EGIFT/inside_voucher', $data);
    }

// ====================================================================================================================================//
// -> Function     : egiftcards
// -> Executed     : on click of egiftcard menu all egift cards should display
// -> description  :Shows detail information of each voucher
// ====================================================================================================================================//

    public function egiftcards() {
//Fetching vouchers from files by calling helper method
        $vouchers = getAllVouchers();
//sorting vouchers alphabetically
//        $data["product_voucher"] = $this->subval_sort($vouchers, 'voucher_pro_name');
        $data["product_voucher"] = array();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=5")->row_array();
        $data["seo"] = $title;
        $this->load->view('EGIFT/egiftcards', $data);
    }

// ====================================================================================================================================//
// -> Function     : Flowers
// -> Executed     : 
// -> description  :Shows detail information of each voucher
// ====================================================================================================================================//

    public function flowers_product() {
        $data["flower_product_min_price"] = $this->Birthday->getMin_price("product_new")->row_array();
        $data["flower_product_max_price"] = $this->Birthday->getMax_price("product_new")->row_array();
//        $data["flower_product"] = $this->Birthday->getLimitedData_new("product_new")->result_array();
        $data["most_viewed"] = $this->Birthday->getMostViewedDataWithOrderBy("product_new")->result_array();
        $data["best_selling"] = $this->Birthday->getBestSellingDataWithOrderBy("product_new")->result_array();

        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=19")->row_array();
        $data["seo"] = $title;
        $this->load->view('HOME/flowers_product', $data);
    }

    public function delivery_address() {
        $is_edit = $this->uri->segment(2);
        $data['is_edit'] = $is_edit;
        $data['edit_flower_data'] = array();
        if ($is_edit != "0") {
//Edit Details
            $flower_data = $this->session->userdata["userdata"]["flower_data"];
            $flower_data_unique_array = array_column($flower_data, 'unique_id');

            if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
                $order_id = $is_edit;
                $flower_data_index = array_search("$order_id", $flower_data_unique_array, TRUE);
                $data['edit_flower_data'] = $this->session->userdata["userdata"]["flower_data"][$flower_data_index];
            } else {
                $data['edit_flower_data'] = array();
            }
        }
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=19")->row_array();
        $data["seo"] = $title;

        $this->load->view('HOME/delivery_address', $data);
    }

    public function flowers_detail() {
        $updated_data = array();
        $pro_id = $this->uri->segment(2);
        $is_edit = $this->uri->segment(3);
        $data['is_edit'] = $is_edit;

        $where = "status = 'Active' and pro_id = $pro_id";
        $data['flower_detail'] = $flower_product = $this->Birthday->getUserData("product_new", $where)->result_array();

        $category_id = $flower_product[0]['category_id'];
        $updated_data['most_viewed'] = $flower_product[0]['most_viewed'] + 1;

        $flower_product_update = $this->Birthday->update("product_new", $updated_data, $where);


        switch ($category_id) {
            case "1":
                $where = "category_id = 1 and status = 'Active' ORDER BY RAND() LIMIT 30";
                $flower_product_random = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
                break;
            case "2":
                $where = "status = 'Active' and (category_id = 2 or category_id = 3 or category_id = 4 or category_id = 5 or category_id = 6 or category_id = 7 or category_id = 8) ORDER BY RAND() LIMIT 30";
                $flower_product_random = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
                break;
            case "3":
                $where = "status = 'Active' and (category_id = 2 or category_id = 3 or category_id = 4 or category_id = 5 or category_id = 6 or category_id = 7 or category_id = 8) ORDER BY RAND() LIMIT 30";
                $flower_product_random = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
                break;
            case "4":
                $where = "status = 'Active' and (category_id = 2 or category_id = 3 or category_id = 4 or category_id = 5 or category_id = 6 or category_id = 7 or category_id = 8) ORDER BY RAND() LIMIT 30";
                $flower_product_random = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
                break;
            case "5":
                $where = "status = 'Active' and (category_id = 2 or category_id = 3 or category_id = 4 or category_id = 5 or category_id = 6 or category_id = 7 or category_id = 8) ORDER BY RAND() LIMIT 30";
                $flower_product_random = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
                break;
            case "6":
                $where = "status = 'Active' and (category_id = 2 or category_id = 3 or category_id = 4 or category_id = 5 or category_id = 6 or category_id = 7 or category_id = 8) ORDER BY RAND() LIMIT 30";
                $flower_product_random = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
                break;
            case "7":
                $where = "status = 'Active' and (category_id = 2 or category_id = 3 or category_id = 4 or category_id = 5 or category_id = 6 or category_id = 7 or category_id = 8) ORDER BY RAND() LIMIT 30";
                $flower_product_random = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
                break;
            case "8":
                $where = "status = 'Active' and (category_id = 2 or category_id = 3 or category_id = 4 or category_id = 5 or category_id = 6 or category_id = 7 or category_id = 8) ORDER BY RAND() LIMIT 30";
                $flower_product_random = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
                break;
            default:
                $where = "1 and status = 'Active' ORDER BY RAND() LIMIT 30";
                $flower_product_random = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
        }
        $data['flower_product_random'] = $flower_product_random;
        $data['zip_data'] = $this->Birthday->getAllData("zip_code")->result_array();

        if ($is_edit == "0") {
            $data['edit_flower_data'] = array();
        } else {
//Edit Details
            $flower_data = $this->session->userdata["userdata"]["flower_data"];
            $flower_data_unique_array = array_column($flower_data, 'unique_id');

            if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
                $order_id = $is_edit;
                $flower_data_index = array_search("$order_id", $flower_data_unique_array, TRUE);
                $data['edit_flower_data'] = $this->session->userdata["userdata"]["flower_data"][$flower_data_index];
            } else {
                $data['edit_flower_data'] = array();
            }
        }
        $title = $this->Birthday->getSelectData("title, description, keywords", "seo", "id = 19")->row_array();
        $title["title"] = $flower_product[0]['product_name'];
        $title['description'] = $flower_product[0]['product_name'];
        $data["seo"] = $title;
        $this->load->view('HOME/flowers_detail', $data);
    }

    function validate_pincode() {
        if (count($_POST) > 0) {
            $pin_code = $this->input->post("pin_code");
            $city = $this->input->post("city");
            $title = $this->Birthday->getSelectData("*", "zip_code", "pin_code = $pin_code and city_name='$city'")->row_array();
            if (count($title) > 0) {
                $output["success"] = TRUE;
                $output["message"] = "Found";
            } else {
                $output["success"] = FALSE;
                $output["message"] = "Pincode or City not found in our Database";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found.";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function getFlowerDetails() {
        if (count($_POST) > 0) {
            $pro_id = $this->input->post("product_id");
            $output = array();
            $updated_data = array();
            $where = "status = 'Active' and pro_id = $pro_id";
            $flower_product = $this->Birthday->getUserData("product_new", $where)->result_array();
            $updated_data['most_viewed'] = $flower_product[0]['most_viewed'] + 1;
            $flower_product_update = $this->Birthday->update("product_new", $updated_data, $where);
            if (count($flower_product) > 0) {
                $output["success"] = TRUE;
                $output['flower_detail'] = $flower_product;
            } else {
                $output["success"] = FALSE;
                $output["message"] = "No data found";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found.";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function getFlowerCategoryWiseData() {
        if (count($_POST) > 0) {
            $category_name = $this->input->post("category_name");
            $limit = $this->input->post("limit");
            $offset = $this->input->post("offset");
            $output = array();
            $where = "status = 'Active' and 1 ORDER BY RAND()";
            $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();

//            switch ($category_name) {
//                case "Flowers":
//                    $where = "status = 'Active' and category_id = 1 LIMIT $limit OFFSET $offset";
//                    $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
//                    break;
//                case "Flowers Bouquet":
//                    $where = "status = 'Active' and category_id = 1 LIMIT $limit OFFSET $offset";
//                    $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
//                    break;
//                case "Flowers With Cake":
//                    $where = "status = 'Active' and (category_id = 2 or category_id = 3 or category_id = 4 or category_id = 5 or category_id = 6 or category_id = 7 or category_id = 8) LIMIT $limit OFFSET $offset";
//                    $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
//                    break;
//                default:
//                    $where = "status = 'Active' and 1 ORDER BY RAND() DESC LIMIT $limit OFFSET $offset";
////                    $where = "status = 'Active' and 1 ORDER BY `product_new`.`pro_id` DESC LIMIT $limit OFFSET $offset";//Old
//                    $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
//            }
            if (count($flower_product) > 0) {
                $output["success"] = TRUE;
                $output["count"] = count($flower_product);
                $output['flower_product'] = $flower_product;
            } else {
                $output["success"] = FALSE;
                $output["message"] = "No data found";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found.";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function getFlowerCategoryWiseRandomData() {
        if (count($_POST) > 0) {
            $category_name = $this->input->post("category");
            $output = array();

            switch ($category_name) {
                case "Flowers":
                    $where = "status = 'Active' and category_id = 1 ORDER BY RAND() LIMIT 10";
                    $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
                    break;
                case "Flowers Bouquet":
                    $where = "status = 'Active' and category_id = 1 ORDER BY RAND() LIMIT 10";
                    $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
                    break;
                case "Flowers With Cake":
                    $where = "status = 'Active' and (category_id = 2 or category_id = 3 or category_id = 4 or category_id = 5 or category_id = 6 or category_id = 7 or category_id = 8) ORDER BY RAND() LIMIT 10";
                    $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
                    break;
                default:
                    $where = "status = 'Active' and 1 ORDER BY RAND() LIMIT 10";
                    $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
            }
            if (count($flower_product) > 0) {
                $output["success"] = TRUE;
                $output["count"] = count($flower_product);
                $output['flower_product'] = $flower_product;
            } else {
                $output["success"] = FALSE;
                $output["message"] = "No data found";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found.";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function getFlowerPriceWiseData() {
        if (count($_POST) > 0) {
            $category_name = $this->input->post("category_name");
            $min_value = $this->input->post("min_value");
            $max_value = $this->input->post("max_value");
            $limit = $this->input->post("limit");
            $offset = $this->input->post("offset");
            $output = array();
            $where = "status = 'Active' and price between $min_value and $max_value order by price ASC";
            $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();

//            switch ($category_name) {
//                case "Flowers":
//                    $where = "status = 'Active' and category_id = 1 and price between $min_value and $max_value order by price ASC LIMIT $limit OFFSET $offset";
//                    $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
//                    break;
//                case "Flowers Bouquet":
//                    $where = "status = 'Active' and category_id = 1 and price between $min_value and $max_value order by price ASC LIMIT $limit OFFSET $offset";
//                    $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
//                    break;
//                case "Flowers With Cake":
//                    $where = "status = 'Active' and (category_id = 2 or category_id = 3 or category_id = 4 or category_id = 5 or category_id = 6 or category_id = 7 or category_id = 8) and price between $min_value and $max_value order by price ASC LIMIT $limit OFFSET $offset";
//                    $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
//                    break;
//                default:
//                    $where = "status = 'Active' and price between $min_value and $max_value order by price ASC LIMIT $limit OFFSET $offset";
//                    $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
//            }

            if (count($flower_product) > 0) {
                $output["success"] = TRUE;
                $output["count"] = count($flower_product);
                $output['flower_product'] = $flower_product;
            } else {
                $output["success"] = FALSE;
                $output["message"] = "No data found";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found.";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

// ====================================================================================================================================//
// -> Function     : thanx_for_purchase
// -> Executed     : on click of final checkout in my_cart after successful egift purchased
// -> description  :update payment info in database and send payment success email
// ====================================================================================================================================//

    public function thanx_for_purchase_old() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $orders = array();
            $payment["payment_mode"] = $update_pay["payment_mode"] = $_POST["mode"];
            $payment["transaction_status"] = $update_pay["transaction_status"] = $status = $_POST["status"];
            $payment["bill_to_fname"] = $firstname = $update_pay["bill_to_fname"] = $_POST["firstname"];
            $payment["total_amount"] = $amount = $_POST["amount"];
            $payment["transaction_id"] = $update_pay["transaction_id"] = $txnid = $_POST["txnid"];
            $payment["bill_to_email"] = $email = $update_pay["bill_to_email"] = $_POST["email"];
            $payment["purchased_date"] = $update_pay["purchased_date"] = $_POST["addedon"];
            $payment["payuId"] = $_POST["payuMoneyId"];
            $orders["user_id"] = $data["user_id"] = $user_id = $udf2 = $_POST["udf2"];
            $data["processing_fee"] = $udf1 = $_POST["udf1"];
            $posted_hash = $_POST["hash"];
            $key = $_POST["key"];
            $productinfo = $_POST["productinfo"];
            $orders["status"] = "1";
            $orders["order_created_date"] = date("Y-m-d H:i:s");
            $order_id = $this->Birthday->addData("orders", $orders);
            $orderid = $this->Birthday->encryptPassword($order_id);
            $salt = PSALT;
            $status_value = $this->Birthday->getOrderVoucherData($user_id)->result_array();
            $status_valuecount = count($status_value);
            If (isset($_POST["additionalCharges"])) {
                $additionalCharges = $_POST["additionalCharges"];
                $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

//                $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
            } else {
                $retHashSeq = $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

//                $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
            }
            $hash = hash("sha512", $retHashSeq);
            if ($hash != $posted_hash) {
                for ($i = 0; $i < $status_valuecount; $i++) {
                    $order_pro_id = $status_value[$i]["order_pro_id"];
                    $updated_data["order_id"] = $payment["order_id"] = $order_id;
                    $payment["product_id"] = $product_id = $status_value[$i]["voucher_pro_id"];
                    $payment["user_id"] = $user_id;
                    $payment_id = $this->Birthday->addData("payment", $payment);
                    $where = "order_pro_id = $order_pro_id";
                    $updated_data["delivery_status"] = 3;
                    $updated_data["selected_orders"] = 0;
                    $updated_data["payment_id"] = $payment_id;
                    $updated_data["order_message"] = "Payment failed";
                    $this->Birthday->update("order_products", $updated_data, $where);
                }

                redirect(BASEURL . "payment-failed/" . $orderid);
            } else {
                $where = "user_id = '$user_id' and transaction_id = '$txnid'";
                $transaction_data = $this->Birthday->getSelectData("transaction_status", "payment", $where)->row_array();
                $transaction_status = $transaction_data["transaction_status"];
                $transactioncount = count($transaction_data);
                if ($transactioncount > 0) {
                    if ($transaction_status != "success") {
                        $updated_data["transaction_status"] = $status;
                        $where = "user_id = '$user_id' and transaction_id = '$txnid'";
                        $this->Birthday->update("payment", $updated_data, $where);
                    }
                } else {
                    for ($i = 0; $i < $status_valuecount; $i++) {
                        $order_pro_id = $status_value[$i]["order_pro_id"];
                        $orderpro_orderid = $status_value[$i]["unique_oid"];
                        $orderpro_payment = $status_value[$i]["payment_id"];
                        if ($status_value[$i]["selected_orders"] == '0') {
                            $where = "order_pro_id = $order_pro_id";
                            if ($orderpro_orderid == "") {
                                $updated_data["unique_oid"] = "BHOWL00$order_id";
                                $payment["order_id"] = $order_id;
                                $payment["product_id"] = $product_id = $status_value[$i]["voucher_pro_id"];
                                $payment["user_id"] = $user_id;
                                $payment_id = $this->Birthday->addData("payment", $payment);
                                if ($status_value[$i]["delievery_schedule"] == '0' || $status_value[$i]["delievery_schedule"] == '1') {
                                    $updated_data["selected_orders"] = 1;
                                    $updated_data["delivery_status"] = 1; //payment done and sending egift
                                    $updated_data["order_message"] = "Sending egift...";
                                    $updated_data["order_id"] = $order_id;
                                    $updated_data["payment_id"] = $payment_id;
                                    $this->Birthday->update("order_products", $updated_data, $where);
                                } else {
                                    $updated_data["selected_orders"] = 1;
                                    $updated_data["delivery_status"] = 2;
                                    $delieverydate = $status_value[$i]["delivery_date_time"];
                                    $updated_data["order_message"] = "eGift will be delievered on $delieverydate";
                                    $updated_data["order_id"] = $order_id;
                                    $updated_data["payment_id"] = $payment_id;
                                    $this->Birthday->update("order_products", $updated_data, $where);
                                }
                            } else {
                                $wherep = "payment_id = $orderpro_payment";
                                $update_pay["order_id"] = $order_id;
                                $this->Birthday->update("payment", $update_pay, $wherep);
                                if ($status_value[$i]["delievery_schedule"] == '0' || $status_value[$i]["delievery_schedule"] == '1') {
                                    $updated_data["selected_orders"] = 1;
                                    $updated_data["delivery_status"] = 1; //payment done and sending egift
                                    $updated_data["order_message"] = "Sending egift...";
                                    $updated_data["order_id"] = $order_id;
                                } else {
                                    $updated_data["order_id"] = $order_id;
                                    $updated_data["selected_orders"] = 1;
                                    $updated_data["delivery_status"] = 2;
                                    $delieverydate = $status_value[$i]["delivery_date_time"];
                                    $updated_data["order_message"] = "eGift will be delievered on $delieverydate";
                                }
                                $this->Birthday->update("order_products", $updated_data, $where);
                            }
                        }


                        $payment_data = $this->Birthday->getorderPaymentData($order_pro_id)->row_array();
                        $userinfo = $this->Birthday->getSelectData("email, first_name", "users", "user_id = $user_id")->row_array();
                        $data["uemail"] = $uemail = $userinfo["email"];
                        $data["sender_name"] = $data["uname"] = $uname = $userinfo["first_name"];
                        $data["order_id"] = $order_id;
                        $data["bill_to_name"] = $payment_data["bill_to_fname"];
                        $data["samount"] = $data["total_amount"] = $amount;
                        $data["payment_mode"] = $payment_data["payment_mode"];
                        $data["purchased_date"] = $payment_data["purchased_date"];
                        $data["voucher_img"] = $payment_data["voucher_img"];
                        $data["greeting_img"] = $payment_data["front_page_image"];
                        $data["voucher_name"] = $payment_data["voucher_pro_name"];
                        $data["fname"] = $payment_data["fname"];
                        $data["selected_amount"] = $payment_data["selected_amount"];
                        $data["femail"] = $payment_data["femail"];
                        $data["greetmessage"] = $payment_data["greeting_message"];
                        $data["type"] = $payment_data["type"];
                        $data["media"] = $payment_data["uploded_img_video"];
//$data["samount"] = $amount;
//payment confirmation email
                        try {
                            $view = $this->load->view("email_template/Payment_received", $data, true);
                            $response = send_EmailResponse($view, $uemail, $uname, "orders@birthdayowl.com", "birthdayowl.com", "Payment Received (Rs.$amount) from BirthdayOwl");
                            if (SERVER_TYPE == "1") {
                                $admin_email = "abdul@birthdayowl.com";
                            } else {
                                $admin_email = "sbhuvad@techathalon.com";
                            }
                            $viewn = $this->load->view("email_template/Order_placed", $data, true);
                            send_Email($viewn, $admin_email, "abdul", "orders@birthdayowl.com", "birthdayowl.com", "Order has been placed by $uname($uemail)");
                            $where = "order_pro_id = $order_pro_id";
                            if ($response["code"] == "success") {
                                $up["payment_mail_status"] = "1";
                                $this->Birthday->update("order_products", $up, $where);
                            } else {
                                $up["payment_mail_status"] = "0";
                                $this->Birthday->update("order_products", $up, $where);
                            }
                        } catch (Exception $e) {
                            $where = "order_pro_id = $order_pro_id";
                            $up["payment_mail_status"] = "0";
                            $this->Birthday->update("order_products", $up, $where);
                        }
                    }
                }
                $userid = $this->Birthday->encryptPassword($user_id);
                redirect(BASEURL . "Home_web/esuccess_payment/" . $orderid . "/" . $userid);
            }
        } else {
            $data["trerror"] = 0;
            $data["transaction_status"] = "fail";
            $this->load->view("EGIFT/failed_transaction", $data);
        }

//        
    }

// ====================================================================================================================================//
// -> Function     : payment_failure
// -> Executed     : on click of final checkout in my_cart after failure of egift purchased
// -> description  : update payment info in database and send payment failure email
// ====================================================================================================================================//
    public function payment_failure() {

        $data["user_id"] = $user_id = $udf2 = $_POST["udf2"];
        $udf1 = $_POST["udf1"];
//  $data["user_id"] = $user_id = $this->session->userdata["userdata"]["user_id"];
        $status_value = $this->Birthday->getOrderVoucherData($user_id)->result_array();
        $userinfo = $this->Birthday->getSelectData("email, first_name", "users", "user_id = $user_id")->row_array();
        $uemail = $userinfo["email"];
        $data["uname"] = $uname = $userinfo["first_name"];
        $payment["payment_mode"] = $_POST["mode"];
        $data["transaction_status"] = $payment["transaction_status"] = $status = $_POST["status"];
        $payment["bill_to_fname"] = $firstname = $_POST["firstname"];
        $payment["total_amount"] = $amount = $_POST["amount"];
        $payment["transaction_id"] = $txnid = $_POST["txnid"];
        $payment["purchased_date"] = $_POST["addedon"];
        $payment["payuId"] = $_POST["payuMoneyId"];
        $posted_hash = $_POST["hash"];
        $key = $_POST["key"];
        $productinfo = $_POST["productinfo"];
        $payment["bill_to_email"] = $email = $_POST["email"];
        $error_message = $_POST["error_Message"];
        $salt = PSALT;

        If (isset($_POST["additionalCharges"])) {
            $additionalCharges = $_POST["additionalCharges"];
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

//            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        } else {
            $retHashSeq = $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        }
        $hash = hash("sha512", $retHashSeq);

        if ($hash != $posted_hash) {
            $data["trerror"] = 1;
        } else {
            $data["trerror"] = 0;
        }
        if ($error_message == "No Error") {
            redirect(BASEURL . "cart");
        } else {
            $orders["user_id"] = $user_id;
            $orders["status"] = "1";
            $orders["order_created_date"] = date("Y-m-d H:i:s");
            $order_id = $this->Birthday->addData("orders", $orders);
            $sum = 0;
            $statusvaluecount = count($status_value);
            for ($i = 0; $i < $statusvaluecount; $i++) {
                $order_pro_id = $status_value[$i]["order_pro_id"];
                $payment["order_id"] = $order_id;
                $payment["product_id"] = $product_id = $status_value[$i]["voucher_pro_id"];
                $payment["user_id"] = $user_id;
                $payment_id = $this->Birthday->addData("payment", $payment);
                $where = "order_pro_id = $order_pro_id";
                $updated_data["delivery_status"] = 3;
                $updated_data["selected_orders"] = 0;
                $updated_data["unique_oid"] = "BHOWL00$order_id";
                $updated_data["payment_id"] = $payment_id;
                $updated_data["order_message"] = "Payment failed";
                $updated_data["order_id"] = $order_id;
                $this->Birthday->update("order_products", $updated_data, $where);
                $sum += $status_value[$i]["selected_amount"];
            }

            try {
                $view = $this->load->view("email_template/Payment_failed", $data, true);
                send_Email($view, $uemail, $uname, "orders@birthdayowl.com", "birthdayowl.com", "Payment Failed");
            } catch (Exception $e) {
                
            }

            $orderid = $this->Birthday->encryptPassword($order_id);
            redirect(BASEURL . "payment-failed/" . $orderid);
        }
    }

// ====================================================================================================================================//
// -> Function     : sendegiftMail
// -> Executed     : cronjob calling this function to check if anyone has purchased any voucher
// -> description  : Sends voucher email 
// ====================================================================================================================================//

    function sendegiftMail() {
        $paid_orders = $this->Birthday->check_payment_done()->result_array();
        $paidorderscount = count($paid_orders);
        if ($paidorderscount > 0) {
            foreach ($paid_orders as $paidorders) {

                if ($paidorders['delivery_status'] == 1) {
                    $user_id = $paidorders["user_id"];
                    $where = "user_id = $user_id";
                    $userdata = $this->Birthday->getSelectData("mobile_no", "users", $where)->row_array();
                    $userphone = $userdata["mobile_no"];
//                    $voucherarray = getAllVouchers();



                    $woohoodata = $this->GenerateWoohooOrder($paidorders["greeting_message"], $paidorders["selected_amount"], $userphone, $paidorders["fphone"], $paidorders["uname"], $paidorders["voucher_pro_id"], $paidorders["voucher_pro_name"], $paidorders["uemail"], $paidorders["femail"], $paidorders["fname"]);
                    $woohoo_status = $woohoodata["status"];
                    $order_id = $paidorders["order_id"];
                    $order_pro_id = $paidorders["order_pro_id"];
                    $payment_id = $paidorders["payment_id"];
                    $data["euser_id"] = $euser_id = $this->Birthday->encryptPassword($user_id);
                    $keyid = "eGift@birthdayOwl";
                    $data["greeting_id"] = $greeting_id = $this->Birthday->encryptPassword($paidorders["greeting_id"]);
                    $data["pay_id"] = $pay_id = $this->Birthday->encryptPassword($payment_id);
                    $data["key"] = $key = $this->Birthday->encryptPassword($keyid);
                    $data["opid"] = $opid = $this->Birthday->encryptPassword($order_pro_id);
                    $data["order_pro_id"] = $order_pro_id;
                    $link = BASEURL . "Home_web/voucher_email/$euser_id/$pay_id/$greeting_id/$opid/$key";
                    $updated_data["voucher_info_link"] = $link;
                    if ($woohoo_status == "Complete") {
                        $data["pin"] = $payment["pin"] = $woohoodata["pin"];
                        $data["code"] = $payment["voucher_code"] = $vcode = $woohoodata["cardnumber"];
                        $data["expiry_date"] = $payment["expiry_date"] = $woohoodata["expiry_date"];
                        $data["type"] = $paidorders["type"];
//Birthdayowl-order email
                        $woohoo_order_id = $woohoodata["order_id"];
                        $this->Birthday->update("payment", $payment, "payment_id = $payment_id");
                        $text = nl2br(trim(strip_tags($paidorders["greeting_message"], '<br/>')));
                        $data["message"] = $message = $text;
                        $data["product_image"] = $paidorders["voucher_img"];
                        $data["voucher_pro_name"] = $voucher_name = $paidorders["voucher_pro_name"];
                        $data["total"] = $paidorders["selected_amount"];
                        $data["greeting"] = $paidorders["front_page_image"];
                        $data["femail"] = $paidorders["femail"];
                        $data["fname"] = $rec_name = $paidorders["fname"];
                        $data["fphone"] = $fphone = $paidorders["fphone"];
                        $data["uemail"] = $uemail = $paidorders["uemail"];
                        $data["uname"] = $uname = $paidorders["uname"];
                        $data["terms"] = $paidorders["terms"];
                        $data["vtitle"] = $paidorders["vtitle"];
                        $data["greeting"] = $paidorders["front_page_image"];
                        $selected_amount = $paidorders["selected_amount"];
                        $updated_data["woohoo_order_id"] = $woohoodata["worder_id"];
                        $updated_data["delivery_status"] = 4;  // successfully done
                        $updated_data["order_message"] = "eGift sent Successfully";
                        $odata["orderstatus"] = 1;
                        $odata["order_pro_id"] = $order_pro_id;
                        $this->Birthday->update("order_products", $updated_data, "order_pro_id = $order_pro_id");
                        $linkData = $this->google_url_api->shorten("$link");
                        $data["click_link"] = $click_link = $linkData->id;

                        if ($data["femail"] != '') {
                            $this->generateVoucherPdf($data, $message, $order_pro_id);
                        }
                        if ($fphone != '') {
                            $show_message = "Dear $rec_name, \n You have just received an awesome e-gift voucher from $uname. \n To view and redeem your e-gift voucher, simply ($click_link) \n- Birthday Owl (www.birthdayowl.com)";
                            $this->Birthday->send_sms($data["fphone"], $show_message);
                        }
                        if ($userphone != '') {
                            $show_message = "Dear $uname, \n You have just sent an awesome e-gift voucher to $rec_name and we have notified $rec_name accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                            $this->Birthday->send_sms($userphone, $show_message);
                        }
                    } else if ($woohoo_status == "Processing" || $woohoo_status == "processing") {
                        $odata["orderstatus"] = 6;
                        $woohoo_order_id = $data["order_id"];
                        $updated_data["order_id"] = $woohoo_order_id; //processing
                        $updated_data["selected_orders"] = 1;
                        $updated_data["delivery_status"] = 6; //processing  //resend
                        $updated_data["order_message"] = "Order is in Processing state.";
                        $this->Birthday->update("order_products", $updated_data, "order_pro_id = $order_pro_id");
                    } else if ($woohoo_status == "error") {
                        $odata["orderstatus"] = 8;
                        $updated_data["selected_orders"] = 1;
                        $updated_data["delivery_status"] = 8; //Failed but resend api request
                        $updated_data["order_id"] = $order_id;
                        if ($woohoodata["ordermessage"] != "")
                            $updated_data["order_message"] = $woohoodata["ordermessage"];
                        else
                            $updated_data["order_message"] = "woohoo error";
                        $this->Birthday->update("order_products", $updated_data, "order_pro_id = $order_pro_id");
                    } else if ($woohoo_status == "Cancelled" || $woohoo_status == "Blocked") {
                        $updated_data["selected_orders"] = 1;
                        $updated_data["delivery_status"] = 0; //Failed
                        $this->Birthday->update("order_products", $updated_data, "order_pro_id = $order_pro_id");
                    }
                } else {// if the delivery_status = 2
                    $delivery_date_time = $paidorders['delivery_date_time'];

                    $current_time = date('Y-m-d H:i:s');
//Our dates
                    $date1 = $delivery_date_time;
                    $date2 = $current_time;

//Convert them to timestamps.
                    $date1Timestamp = strtotime($date1);
                    $date2Timestamp = strtotime($date2);

//Calculate the difference.
                    $difference = $date2Timestamp - $date1Timestamp;

                    if ($difference >= 1 && $difference <= 60) {
                        $user_id = $paidorders["user_id"];
                        $where = "user_id = $user_id";
                        $userdata = $this->Birthday->getSelectData("mobile_no", "users", $where)->row_array();
                        $userphone = $userdata["mobile_no"];
                        $woohoodata = $this->GenerateWoohooOrder($paidorders["greeting_message"], $paidorders["selected_amount"], $userphone, $paidorders["fphone"], $paidorders["uname"], $paidorders["voucher_pro_id"], $paidorders["voucher_pro_name"], $paidorders["uemail"], $paidorders["femail"], $paidorders["fname"]);
                        $woohoo_status = $woohoodata["status"];
                        $order_id = $paidorders["order_id"];
                        $order_pro_id = $paidorders["order_pro_id"];
                        $payment_id = $paidorders["payment_id"];
                        $data["euser_id"] = $euser_id = $this->Birthday->encryptPassword($user_id);
                        $keyid = "eGift@birthdayOwl";
                        $data["greeting_id"] = $greeting_id = $this->Birthday->encryptPassword($paidorders["greeting_id"]);
                        $data["pay_id"] = $pay_id = $this->Birthday->encryptPassword($payment_id);
                        $data["key"] = $key = $this->Birthday->encryptPassword($keyid);
                        $data["opid"] = $opid = $this->Birthday->encryptPassword($order_pro_id);
                        $data["order_pro_id"] = $order_pro_id;
                        $link = BASEURL . "Home_web/voucher_email/$euser_id/$pay_id/$greeting_id/$opid/$key";
                        $updated_data["voucher_info_link"] = $link;
                        if ($woohoo_status == "Complete") {
                            $data["pin"] = $payment["pin"] = $woohoodata["pin"];
                            $data["code"] = $payment["voucher_code"] = $vcode = $woohoodata["cardnumber"];
                            $data["expiry_date"] = $payment["expiry_date"] = $woohoodata["expiry_date"];
                            $data["type"] = $paidorders["type"];
//Birthdayowl-order email
                            $woohoo_order_id = $woohoodata["order_id"];
                            $this->Birthday->update("payment", $payment, "payment_id = $payment_id");
                            $text = nl2br(trim(strip_tags($paidorders["greeting_message"], '<br/>')));
                            $data["message"] = $message = $text;
                            $data["product_image"] = $paidorders["voucher_img"];
                            $data["voucher_pro_name"] = $voucher_name = $paidorders["voucher_pro_name"];
                            $data["total"] = $paidorders["selected_amount"];
                            $data["greeting"] = $paidorders["front_page_image"];
                            $data["femail"] = $paidorders["femail"];
                            $data["fname"] = $rec_name = $paidorders["fname"];
                            $data["fphone"] = $fphone = $paidorders["fphone"];
                            $data["uemail"] = $uemail = $paidorders["uemail"];
                            $data["uname"] = $uname = $paidorders["uname"];
                            $data["terms"] = $paidorders["terms"];
                            $data["vtitle"] = $paidorders["vtitle"];
                            $data["greeting"] = $paidorders["front_page_image"];
                            $selected_amount = $paidorders["selected_amount"];
                            $updated_data["woohoo_order_id"] = $woohoodata["worder_id"];
                            $updated_data["delivery_status"] = 4;
                            $updated_data["order_message"] = "eGift sent Successfully";
                            $odata["orderstatus"] = 1;
                            $odata["order_pro_id"] = $order_pro_id;
                            $this->Birthday->update("order_products", $updated_data, "order_pro_id = $order_pro_id");
                            $linkData = $this->google_url_api->shorten("$link");
                            $data["click_link"] = $click_link = $linkData->id;

                            if ($data["femail"] != '') {
                                $this->generateVoucherPdf($data, $message, $order_pro_id);
                            }
                            if ($fphone != '') {
                                $show_message = "Dear $rec_name, \n You have just received an awesome e-gift voucher from $uname. \n To view and redeem your e-gift voucher, simply ($click_link) \n- Birthday Owl (www.birthdayowl.com)";
                                $this->Birthday->send_sms($data["fphone"], $show_message);
                            }
                            if ($userphone != '') {
                                $show_message = "Dear $uname, \n You have just sent an awesome e-gift voucher to $rec_name and we have notified $rec_name accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                                $this->Birthday->send_sms($userphone, $show_message);
                            }
                        } else if ($woohoo_status == "Processing" || $woohoo_status == "processing") {
                            $odata["orderstatus"] = 6;
                            $woohoo_order_id = $data["order_id"];
                            $updated_data["order_id"] = $woohoo_order_id; //processing
                            $updated_data["selected_orders"] = 1;
                            $updated_data["delivery_status"] = 6; //processing  //resend
                            $updated_data["order_message"] = "Order is in Processing state.";
                            $this->Birthday->update("order_products", $updated_data, "order_pro_id = $order_pro_id");
                        } else if ($woohoo_status == "error") {
                            $odata["orderstatus"] = 8;
                            $updated_data["selected_orders"] = 1;
                            $updated_data["delivery_status"] = 8; //Failed but resend api request
                            $updated_data["order_id"] = $order_id;
                            if ($woohoodata["ordermessage"] != "")
                                $updated_data["order_message"] = $woohoodata["ordermessage"];
                            else
                                $updated_data["order_message"] = "woohoo error";
                            $this->Birthday->update("order_products", $updated_data, "order_pro_id = $order_pro_id");
                        } else if ($woohoo_status == "Cancelled" || $woohoo_status == "Blocked") {
                            $updated_data["selected_orders"] = 1;
                            $updated_data["delivery_status"] = 0; //Failed
                            $this->Birthday->update("order_products", $updated_data, "order_pro_id = $order_pro_id");
                        } else if ($woohoo_status == "pending") {
                            $updated_data["selected_orders"] = 1;
                            $updated_data["delivery_status"] = 1; //Retry
                            $this->Birthday->update("order_products", $updated_data, "order_pro_id = $order_pro_id");
                        }
                    }
                }
            }
        }
    }

    function sendegiftMail_old() {
        $paid_orders = $this->Birthday->check_payment_done()->result_array();
        $paidorderscount = count($paid_orders);
        if ($paidorderscount > 0) {
            foreach ($paid_orders as $paidorders) {
                $user_id = $paidorders["user_id"];
                $where = "user_id = $user_id";
                $userdata = $this->Birthday->getSelectData("mobile_no", "users", $where)->row_array();
                $userphone = $userdata["mobile_no"];
                $woohoodata = $this->GenerateWoohooOrder($paidorders["greeting_message"], $paidorders["selected_amount"], $userphone, $paidorders["fphone"], $paidorders["uname"], $paidorders["voucher_pro_id"], $paidorders["voucher_pro_name"], $paidorders["uemail"], $paidorders["femail"], $paidorders["fname"]);
                $woohoo_status = $woohoodata["status"];
                $order_id = $paidorders["order_id"];
                $order_pro_id = $paidorders["order_pro_id"];
                $payment_id = $paidorders["payment_id"];
                $data["euser_id"] = $euser_id = $this->Birthday->encryptPassword($user_id);
                $keyid = "eGift@birthdayOwl";
                $data["greeting_id"] = $greeting_id = $this->Birthday->encryptPassword($paidorders["greeting_id"]);
                $data["pay_id"] = $pay_id = $this->Birthday->encryptPassword($payment_id);
                $data["key"] = $key = $this->Birthday->encryptPassword($keyid);
                $data["opid"] = $opid = $this->Birthday->encryptPassword($order_pro_id);
                $data["order_pro_id"] = $order_pro_id;
                $link = BASEURL . "Home_web/voucher_email/$euser_id/$pay_id/$greeting_id/$opid/$key";
                $updated_data["voucher_info_link"] = $link;
                if ($woohoo_status == "Complete") {
                    $data["pin"] = $payment["pin"] = $woohoodata["pin"];
                    $data["code"] = $payment["voucher_code"] = $vcode = $woohoodata["cardnumber"];
                    $data["expiry_date"] = $payment["expiry_date"] = $woohoodata["expiry_date"];
                    $data["type"] = $paidorders["type"];
//Birthdayowl-order email
                    $woohoo_order_id = $woohoodata["order_id"];
                    $this->Birthday->update("payment", $payment, "payment_id = $payment_id");
                    $text = nl2br(trim(strip_tags($paidorders["greeting_message"], '<br/>')));
                    $data["message"] = $message = $text;
                    $data["product_image"] = $paidorders["voucher_img"];
                    $data["voucher_pro_name"] = $voucher_name = $paidorders["voucher_pro_name"];
                    $data["total"] = $paidorders["selected_amount"];
                    $data["greeting"] = $paidorders["front_page_image"];
                    $data["femail"] = $paidorders["femail"];
                    $data["fname"] = $rec_name = $paidorders["fname"];
                    $data["fphone"] = $fphone = $paidorders["fphone"];
                    $data["uemail"] = $uemail = $paidorders["uemail"];
                    $data["uname"] = $uname = $paidorders["uname"];
                    $data["terms"] = $paidorders["terms"];
                    $data["vtitle"] = $paidorders["vtitle"];
                    $data["greeting"] = $paidorders["front_page_image"];
                    $selected_amount = $paidorders["selected_amount"];
                    $updated_data["woohoo_order_id"] = $woohoodata["worder_id"];
                    $updated_data["delivery_status"] = 4;
                    $updated_data["order_message"] = "eGift sent Successfully";
                    $odata["orderstatus"] = 1;
                    $odata["order_pro_id"] = $order_pro_id;
                    $this->Birthday->update("order_products", $updated_data, "order_pro_id = $order_pro_id");
                    $linkData = $this->google_url_api->shorten("$link");
                    $data["click_link"] = $click_link = $linkData->id;

                    if ($data["femail"] != '') {
                        $this->generateVoucherPdf($data, $message, $order_pro_id);
                    }
                    if ($fphone != '') {
                        $show_message = "Dear $rec_name, \n You have just received an awesome e-gift voucher from $uname. \n To view and redeem your e-gift voucher, simply ($click_link) \n- Birthday Owl (www.birthdayowl.com)";
                        $this->Birthday->send_sms($data["fphone"], $show_message);
                    }
                    if ($userphone != '') {
                        $show_message = "Dear $uname, \n You have just sent an awesome e-gift voucher to $rec_name and we have notified $rec_name accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                        $this->Birthday->send_sms($userphone, $show_message);
                    }
                } else if ($woohoo_status == "Processing" || $woohoo_status == "processing") {
                    $odata["orderstatus"] = 6;
                    $woohoo_order_id = $data["order_id"];
                    $updated_data["order_id"] = $woohoo_order_id; //processing
                    $updated_data["selected_orders"] = 1;
                    $updated_data["delivery_status"] = 6; //processing  //resend
                    $updated_data["order_message"] = "Order is in Processing state.";
                    $this->Birthday->update("order_products", $updated_data, "order_pro_id = $order_pro_id");
                } else if ($woohoo_status == "error") {
                    $odata["orderstatus"] = 8;
                    $updated_data["selected_orders"] = 1;
                    $updated_data["delivery_status"] = 8; //Failed but resend api request
                    $updated_data["order_id"] = $order_id;
                    if ($woohoodata["ordermessage"] != "")
                        $updated_data["order_message"] = $woohoodata["ordermessage"];
                    else
                        $updated_data["order_message"] = "woohoo error";
                    $this->Birthday->update("order_products", $updated_data, "order_pro_id = $order_pro_id");
                } else if ($woohoo_status == "Cancelled" || $woohoo_status == "Blocked") {
                    $updated_data["selected_orders"] = 1;
                    $updated_data["delivery_status"] = 0; //Failed
                    $this->Birthday->update("order_products", $updated_data, "order_pro_id = $order_pro_id");
                }
            }
        }
    }

// ====================================================================================================================================//
// -> Function     : generateVoucherPdf
// -> Executed     : called this function inside sendegiftMail,resend_woohoo_api
// -> description  : Generates pdf
// ====================================================================================================================================//

    public function generateVoucherPdf($data = array(), $message, $order_pro_id) {
        require_once(APPPATH . "libraries/Mailin.php" );

        $this->load->library('Pdf'); // Load library
        $img = BASEURL_CROPPED_GREET . $data["greeting"];
        if (ob_get_length() > 0) {
            ob_end_clean();
        }
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);
        $pdf->AddPage("P", " A4");

        $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 5, 10, 'color' => array(0, 0, 0));
        $style1 = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 5, 10, 'color' => array(0, 0, 0));

///////////////////
        $cardimg = $data["product_image"];
        $barcode = BASEURL_OIMG . "barcode.png";
        $logo = BASEURL_OIMG . "logo.png";
        $barcode_scanner = BASEURL_OIMG . "barcode_scanner.jpeg";
        $termsDisplay = $data['terms'];
        $total = $data["total"];
        $codeDisplay = $data["code"];
        $pinDisplay = $data["pin"];
        $valid = $data["expiry_date"];
        $type = $data["type"];
        $linkd = $data["click_link"];
        $uname = $data["uname"];
        if ($type == 1) {
            $message_type = "$uname sent image for you that is available on this link ";
        } else if ($type == 2) {
            $message_type = "$uname recorded a video message for you that is available on this link";
        } else if ($type == 3) {
            $message_type = "$uname recorded a audio message for you that is available on this link";
        } else {
            $message_type = "";
        }


        $terms = <<<EOD
<div style="text-align:left;
        font-size:8px;
        color:black;
        ">$termsDisplay</div>
EOD;

        $html = <<<EOD
<div style="text-align:center;
        font-size:7px;
        color:black;
        ">Your Gift Card Value</div>
EOD;

        $amt = <<<EOD
<div style="text-align:center;
        font-size:16px;
        font-weight:bold;
        color:#5CAD01;">Rs.$total</div>
EOD;
        $codelabel = <<<EOD
<div style="text-align:center;font-size:7px;color:black;">Gift Code</div>
EOD;

        $code = <<<EOD
<div style="text-align:center;font-size:10px;font-weight:800;color:#5CAD01;">$codeDisplay</div>
EOD;

        $pinlabel = <<<EOD
<div style="text-align:center;font-size:7px;color:black;">PIN</div>
EOD;


        $pin = <<<EOD
<div style="text-align:center;font-size:10px;font-weight:800;color:#5CAD01;">$pinDisplay</div>
EOD;



        $validity = <<<EOD
<div style="text-align:center;font-size:11px;font-weight:600;color:black;">Valid until $valid</div>
EOD;

        $exp = <<<EOD
<div style="text-align:center;font-size:8px;font-weight:600;color:#5CAD01;">Experiance your Gift Card online</div>
EOD;
        $link = <<<EOD
<div style="text-align:center;font-size:10px;font-weight:600;color:black;"><i>$linkd</i></div>
EOD;
        $linkn = <<<EOD
<div style="text-align:center;font-size:10px;font-weight:600;color:black;">$linkd</div>
EOD;
        $messageDisplay = <<<EOD
<div style="text-align:center;font-size:10px;font-weight:600;color:black;"><i>$message</i></div>
EOD;
        $termslabel = <<<EOD
<div style="text-align:left;font-size:10px;font-weight:bold;color:black;">Terms & Conditions</div>
EOD;
        $mediamessage = <<<EOD
<div style="text-align:center;font-size:10px;font-weight:600;color:black;"><i>$message_type</i></div>
EOD;
        $pdf->StartTransform();
        $pdf->Rotate(-180, 53, 73);
        $pdf->Image($img, 8, 8, 90, 130, "", "", "M", FALSE, 8);
        $pdf->Image($logo, -70, 128, 20, 8, "", "", "M", FALSE, 8);

// Stop Transformation
        $pdf->StopTransform();
        $pdf->StartTransform();
        $pdf->Rotate(180, 158, 104);

        $pdf->writeHTMLCell(100, 220, 110, 75, $terms, 0, 0, false, true, 'center', true);
        $pdf->writeHTMLCell(100, 220, 110, 70, $termslabel, 0, 0, false, true, 'center', true);
        $pdf->StopTransform();
        $pdf->Line(80, 10800, 105.5, 1, $style1);
        $pdf->Line(500, 146, 1, 146, $style);
        $style3 = array('width' => 6, 'cap' => 'round', 'join' => 'miter', 'dash' => '0', 'color' => array(0, 0, 0));
        $pdf->Rect(11, 155, 91, 132, 'D', array('all' => $style3));
        $pdf->Image($cardimg, 120, 170, 70, 40, "", "", "M", FALSE, 8);
        $pdf->writeHTMLCell(45, 20, 35, 180, $messageDisplay, 0, 0, false, true, 'center', true);
        $pdf->writeHTMLCell(45, 20, 35, 225, $mediamessage, 0, 0, false, true, 'center', true);
        $pdf->Image($barcode_scanner, 50, 250, 12, 10, "", "", "M", FALSE, 8);
        $pdf->writeHTMLCell(45, 20, 35, 245, $link, 0, 0, false, true, 'center', true);
        $pdf->writeHTMLCell(190, 100, 60, 210, $html, 0, 0, false, true, 'center', true);
        $pdf->writeHTMLCell(190, 100, 60, 215, $amt, 0, 0, false, true, 'center', true);
        $pdf->writeHTMLCell(190, 100, 60, 224, $codelabel, 0, 0, false, true, 'center', true);
        $pdf->Image($barcode, 132, 228, 45, 10, "", "", "M", FALSE, 8);
        $pdf->writeHTMLCell(190, 100, 60, 238, $code, 0, 0, false, true, 'center', true);
        $pdf->writeHTMLCell(190, 100, 60, 245, $pinlabel, 0, 0, false, true, 'center', true);
        $pdf->writeHTMLCell(190, 100, 60, 248, $pin, 0, 0, false, true, 'center', true);
        $pdf->writeHTMLCell(190, 100, 60, 255, $validity, 0, 0, false, true, 'center', true);
// $pdf->Image($barcode_scanner, 112, 256, 12, 10, "", "", "M", FALSE, 8);
        $pdf->writeHTMLCell(175, 100, 60, 260, $exp, 0, 0, false, true, 'center', true);
        $pdf->writeHTMLCell(168, 100, 60, 264, $link, 0, 0, false, true, 'center', true);
        $pdf->Image($barcode_scanner, 112, 260, 12, 10, "", "", "M", FALSE, 8);
        $pdf->Image($logo, 180, 259, 20, 8, "", "", "M", FALSE, 8);
        $lstyle = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '0', 'color' => array(79, 160, 3));
        $pdf->Line(110, 272, 180, 272, $lstyle);

        $filename = "files/Voucher_greeting.pdf";
        $contentPdf = $pdf->Output($filename, 'S');
        $attachment_content = chunk_split(base64_encode($contentPdf));
        $attachment = array('Voucher_greeting.pdf' => $attachment_content);
        $femail = $data["femail"];
        $rec_name = $data["fname"];
        $voucher_name = $data["voucher_pro_name"];
        $data["order_pro_id"] = $order_pro_id;
        if (array_key_exists("vtitle", $data)) {
            $vtitle = $data["vtitle"];
        } else {
            $vtitle = "";
        }
        try {
            if ($vtitle != "" || $vtitle != NULL)
                $subject = "$vtitle";
            else
                $subject = "$voucher_name Voucher From BirthdayOwl";

            $mailin = new Mailin('https://api.sendinblue.com/v2.0', SENDINKEY);
            $view = $this->load->view("email_template/Product_voucher", $data, true);
            $sdata = array("to" => array("$femail" => "$rec_name"),
                "from" => array("orders@birthdayowl.com", "$uname"),
                "subject" => "$subject",
                "text" => "$view",
                "html" => "$view",
                "attachment" => $attachment
            );
            $resp = $mailin->send_email($sdata);
            $where = "order_pro_id=$order_pro_id";
            if ($resp["code"] == "success") {

                $up["egift_order_mail_status"] = 1;
                $this->Birthday->update("order_products", $up, $where);
                return 1;
            } else {
                $up["egift_order_mail_status"] = 0;
                $this->Birthday->update("order_products", $up, $where);
                return 0;
            }
        } catch (Exception $e) {
            $where = "order_pro_id=$order_pro_id";
            $up["egift_order_mail_status"] = 1;
            $this->Birthday->update("order_products", $up, $where);
            return 0;
        }
    }

// ====================================================================================================================================//
// -> Function     : esuccess_payment
// -> Executed     : called this function inside thanx_for_purchase
// -> description  : load success_payment view
// ====================================================================================================================================//
    public function esuccess_payment() {
        if ($this->check_logged_in()) {
            $order_id = $this->uri->segment(2);
            $userid = $this->uri->segment(3);
            $what_was_transaction = $this->uri->segment(4);

            $orderid = $this->Birthday->decryptPassword($order_id);
            $user_id = $this->Birthday->decryptPassword($userid);
            $what_was_transaction_decrypt = $this->Birthday->decryptPassword($what_was_transaction);

            switch ($what_was_transaction_decrypt) {
                case 1://only vouchers where purchaed
                    $order = $this->Birthday->getorderData($orderid, $user_id)->result_array(); //for voucher data
                    $orderdata["total"] = $order[0]["total_amount"];
                    $orderdata["orderdata"] = $order;
                    $orderdata["gift_count"] = count($order);
                    $orderdata["voucherNames"] = $this->getVoucherNames();
                    $orderdata["testimonials"] = $test = $this->getTestimonials();
                    $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=7")->row_array();
                    $orderdata["seo"] = $title;
                    $orderdata["what_was_transaction"] = $what_was_transaction_decrypt;
                    break;
                case 2://only flowers where purchased
                    $order = $this->Birthday->getorderDataFlower($orderid, $user_id)->result_array(); //for flower data
                    $orderdata["total"] = $order[0]["total_amount"];
                    $orderdata["orderdata"] = $order;
                    $orderdata["gift_count"] = count($order);
                    $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=7")->row_array();
                    $orderdata["seo"] = $title;
                    $orderdata["what_was_transaction"] = $what_was_transaction_decrypt;
                    break;
                case 3://both vouchers and flower and cakes where purchased.
                    $order_voucher = $this->Birthday->getorderData($orderid, $user_id)->result_array();    //for voucher and flower data both
                    $count_voucher = count($order_voucher);
                    if ($count_voucher != 0) {
                        for ($j = 0; $j < $count_voucher; $j++) {
                            $order_voucher[$j]['is_flower'] = 0;      // these keys area aded to every voucher comming from the data base
                            $order_voucher[$j]['is_voucher'] = 1;
                        }
                    }

                    $order_flowers = $this->Birthday->getorderDataFlower($orderid, $user_id)->result_array();
                    $count_flower = count($order_flowers);
                    if ($count_flower != 0) {
                        for ($j = 0; $j < $count_flower; $j++) {
                            $order_flowers[$j]['is_flower'] = 1;      // these keys area aded to every voucher comming from the data base
                            $order_flowers[$j]['is_voucher'] = 0;
                        }
                    }

                    $order = array_merge($order_voucher, $order_flowers);
                    $orderdata["total"] = $order[0]["total_amount"];
                    $orderdata["orderdata"] = $order;
                    $orderdata["gift_count"] = count($order);
                    $orderdata["voucherNames"] = $this->getVoucherNames();
                    $orderdata["testimonials"] = $test = $this->getTestimonials();
                    $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=7")->row_array();
                    $orderdata["seo"] = $title;
                    $orderdata["what_was_transaction"] = $what_was_transaction_decrypt;
                    break;
            }
            $this->load->view("EGIFT/success_payment", $orderdata);
//            $order = $this->Birthday->getorderData($orderid, $user_id)->result_array();
        } else if ($this->check_guest_logged_in()) { // for guest user
            $order_id = $this->uri->segment(2);
            $userid = $this->uri->segment(3);
            $what_was_transaction = $this->uri->segment(4);

            $orderid = $this->Birthday->decryptPassword($order_id);
            $user_id = $this->Birthday->decryptPassword($userid);
            $what_was_transaction_decrypt = $this->Birthday->decryptPassword($what_was_transaction);

            switch ($what_was_transaction_decrypt) {
                case 1:
                    $order = $this->Birthday->getorderData($orderid, $user_id)->result_array(); //for voucher data 
                    $orderdata["total"] = $order[0]["total_amount"];
                    $orderdata["orderdata"] = $order;
                    $orderdata["gift_count"] = count($order);
                    $orderdata["voucherNames"] = $this->getVoucherNames();
                    $orderdata["testimonials"] = $test = $this->getTestimonials();
                    $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=7")->row_array();
                    $orderdata["seo"] = $title;
                    $orderdata["what_was_transaction"] = $what_was_transaction_decrypt;
                    break;
                case 2:
                    $order = $this->Birthday->getorderDataFlower($orderid, $user_id)->result_array(); //for flower data
                    $orderdata["total"] = $order[0]["total_amount"];
                    $orderdata["orderdata"] = $order;
                    $orderdata["gift_count"] = count($order);
                    $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=7")->row_array();
                    $orderdata["seo"] = $title;
                    $orderdata["what_was_transaction"] = $what_was_transaction_decrypt;
                    break;
                case 3:
                    $order_voucher = $this->Birthday->getorderData($orderid, $user_id)->result_array();    //for voucher and flower data both
                    $order_flowers = $this->Birthday->getorderDataFlower($orderid, $user_id)->result_array();
                    $order = array_merge($order_voucher, $order_flowers);
                    $orderdata["total"] = $order[0]["total_amount"];
                    $orderdata["orderdata"] = $order;
                    $orderdata["gift_count"] = count($order);
                    $orderdata["voucherNames"] = $this->getVoucherNames();
                    $orderdata["testimonials"] = $test = $this->getTestimonials();
                    $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=7")->row_array();
                    $orderdata["seo"] = $title;
                    $orderdata["what_was_transaction"] = $what_was_transaction_decrypt;
                    break;
            }
            $this->load->view("EGIFT/success_payment", $orderdata);
        } else {
            redirect(WEB_HOME);
//            $this->index();
        }
    }

// ====================================================================================================================================//
// -> Function     : failed_transaction
// -> Executed     : called this function inside thanx_for_purchase,payment_failure
// -> description  : load failed_transaction view
// ====================================================================================================================================//
    public function failed_transaction() {
        if ($this->check_logged_in()) {
            $order_id = $this->uri->segment(2);
            $orderid = $this->Birthday->decryptPassword($order_id);

            $user_id = $this->session->userdata["userdata"]["user_id"];
            $order = $this->Birthday->getorderData($orderid, $user_id)->result_array();

            $orderdata["orderdata"] = $order;
            $orderdata["gift_count"] = count($order);
            $orderdata["voucherNames"] = $this->getVoucherNames();
            $orderdata["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=7")->row_array();
            $orderdata["seo"] = $title;
            $this->load->view("EGIFT/failed_transaction", $orderdata);
        } else if ($this->check_guest_logged_in()) {
            $order_id = $this->uri->segment(2);
            $orderid = $this->Birthday->decryptPassword($order_id);

            $user_id = $this->session->userdata["userdata"]["user_id"];
            $order = $this->Birthday->getorderData($orderid, $user_id)->result_array();

            $orderdata["orderdata"] = $order;
            $orderdata["gift_count"] = count($order);
            $orderdata["voucherNames"] = $this->getVoucherNames();
            $orderdata["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=7")->row_array();
            $orderdata["seo"] = $title;
            $this->load->view("EGIFT/failed_transaction", $orderdata);
        } else {
            redirect(WEB_HOME);
//            $this->index();
        }
    }

// ====================================================================================================================================//
// -> Function     : generateRandomString
// -> Executed     : called this function inside send_otp
// -> description  : Generates random string
// ====================================================================================================================================//

    public function generateRandomString($length) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function gen_random_code($length) {
        $characters = "abcdefghijklmnopqrstuvwxyzABCDERFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $randomString = "";
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

// ===================================================================================================================================//
// -> Function     :delete_orders
// -> Executed     :Called on click of delete of cart item
// -> description  :Delete orders
// ====================================================================================================================================//
    public function delete_orders() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $order_pro_id = check_post($this->input->post("order_pro_id"));
            $where = "order_pro_id='$order_pro_id'";
//            $this->Birthday->delete_reminder("orders", $where);
            $this->Birthday->delete_reminder("order_products", $where);
            $output["success"] = true;
            $output["message"] = "Deleted successfully";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

//end of egift functions
//start-Birthday reminder
// ====================================================================================================================================//
// -> Function     : display_order_history
// -> Executed     :Order history click calls this function
// -> description  :Displays e-gift and egreeting order history
// ====================================================================================================================================//

    public function display_order_history() {
        if ($this->logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
//Voucher old
//            $data["ehistory"] = $this->Birthday->getJoinedData2("op.order_message,op.delivery_date_time,op.order_pro_id, op.delievery_schedule, op.fphone,op.voucher_pro_name,op.type,op.voucher_img,op.delivery_status,op.selected_amount,op.femail,op.fname, op.unique_oid,op.delivery_date_time, g.front_page_image,op.uploded_img_video,count(op.unique_oid)as quantity", "order_products as op", "greeting_card as g", "op.greeting_id=g.card_id", "op.user_id=$user_id and op.delivery_status!='' group by op.unique_oid order by op.order_pro_id DESC")->result_array();
//Voucher_new
            $data["ehistory"] = $this->Birthday->getJoinedData2("op.order_message,op.delivery_date_time,op.order_pro_id, op.delievery_schedule, op.fphone,op.voucher_pro_name,op.type,op.voucher_img,op.delivery_status,op.selected_amount,op.femail,op.fname, op.unique_oid,op.delivery_date_time, g.front_page_image,op.uploded_img_video,count(op.unique_oid)as quantity", "order_products as op", "greeting_card as g", "op.greeting_id=g.card_id", "op.user_id=$user_id and op.delivery_status!='' group by op.payment_id order by op.order_pro_id DESC")->result_array();
            $data["count"] = count($data["ehistory"]);

//Free Greeting
//            $where = "user_id=$user_id order by delivery_date desc";//Old
            $where = "user_id=$user_id order by fid desc";
            $data["history"] = $this->Birthday->getUserData("free_greetings", $where)->result_array();

//Flower
            $where_flower = "flower_sender_id=$user_id order by flower_order_id desc";
            $data["flower_history"] = $this->Birthday->getUserData("order_to_fnp", $where_flower)->result_array();
            $data["count"] = count($data["flower_history"]);
//            $getsentcounter = $this->Birthday->getSelectData("sent_greeting_counter,membership_status,first_name", "users", "user_id='$user_id'")->row_array();
//            $data["user_name"] = $getsentcounter["first_name"];
//            $membership_status = $getsentcounter["membership_status"];
//            $counter = $getsentcounter["sent_greeting_counter"];
//            $count = 10 - $counter;
//            if ($membership_status == 1) {
//                $data["left_count"] = "Left Greetings:Unlimited";
//                $data["membership_status"] = "Yes";
//                $data["gcount"] = "Unlimited";
//            } else {
//                if ($count < 0) {
//                    if ($membership_status == 1) {
//                        $data["left_count"] = "Left Greetings:Unlimited";
//                    } else {
//                        $data["left_count"] = "Left Greetings:0";
//                    }gift_wallet
//                } else {
//                    $data["left_count"] = "Left Greetings:$count";
//                }
//                $data["membership_status"] = "No";
//                $data["gcount"] = "10";
//            }
//            $data["left_count"] = "Left Greetings:Unlimited";
//                $data["membership_status"] = "Yes";
//                $data["gcount"] = "Unlimited";
//            $data["sent_count"] = "Sent Greetings:$counter";
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=3")->row_array();
            $data["seo"] = $title;
            $this->load->view('BIRTHDAY_REMINDER/owl_OrderHistory', $data);
        }
    }

// ====================================================================================================================================//
// -> Function     :my_account
// -> Executed     :On click of Update Profile
// -> description  :update profile view get load
// ====================================================================================================================================//
    public function my_account() {
        $user_data = array();
        if ($this->logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $where = "user_id='$user_id'";
            $userdata = $this->Birthday->getUserData("users", $where)->row_array();
            $user_data["userprofile"] = $userdata;
            $password = $userdata["password"];
            $pass = $this->Birthday->decryptPassword($password);
            $data["userprofile"] = $user_data["userprofile"];
            $data["password"] = $pass;
            $data['country_info'] = $this->Birthday->getAllData("country")->result_array();
            $data['occupation'] = $this->Birthday->getAllData("occupation")->result_array();
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=13")->row_array();
            $data["seo"] = $title;
            $this->load->view('BIRTHDAY_REMINDER/Update_profile', $data);
        } else {
            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again to continue");
            redirect(WEB_HOME);
        }
    }

// ====================================================================================================================================//
// -> Function     :update_profile_success1
// -> Executed     :On click of Update  in update profile view above function gets call
// -> description  :Updates user data 
// ====================================================================================================================================//
    public function update_profile_success1() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            if ($this->logged_in()) {
                $user_id = $this->session->userdata["userdata"]["user_id"];
                $updated_data["first_name"] = check_post($this->input->post("first_name"));
                $updated_data["mobile_no"] = $mobile_no = check_post($this->input->post("mobile_no"));
                $updated_data["email"] = $email = check_post($this->input->post("email"));

//                email verification of other 
                $where = "email = '$email' and user_id != '$user_id' and is_guest = 0 and mobile_verify = 1";
                $check_user = $this->Birthday->getSelectData("user_id,email,mobile_no,first_name", "users", $where)->row_array();
                if ($check_user > 0) {
                    $name = $check_user['first_name'];
                    $output["success"] = false;
//                    $output["message"] = "Email-id already exists.";
                    $output["message"] = "This Email-id already associated with $name.";
                    echo json_encode($output);
                    exit();
                }
                $updated_data["country_id"] = $country_id = check_post($this->input->post("country_id"));

//                $this->check_update_email($updated_data["email"]);
//             Mobile number verification of other
                if ($country_id == 102) {
                    $where = "mobile_no='$mobile_no' and user_id != '$user_id' and is_guest = 0 and mobile_verify = 1";
                    $check_mobile = $this->Birthday->getSelectData("user_id,email,mobile_no,first_name", "users", $where)->row_array();
                    if ($check_mobile > 0) {
                        $name = $check_mobile['first_name'];
                        $output["success"] = false;
                        $output["message"] = "Mobile Number already exists.";
                        $output["message"] = "This Mobile Number already associated with $name.";
                        echo json_encode($output);
                        exit;
                    }
                }


                $password = check_post($this->input->post("password"));
                $updated_data["password"] = $this->Birthday->encryptPassword($password);
                $updated_data["gender"] = check_post($this->input->post("gender"));

                $bmonth = check_post($this->input->post("bmonth"));
                $bday = check_post($this->input->post("bdate"));
                $byear = check_post($this->input->post("byear"));
                $updated_data["birth_date"] = $bday . "/" . $bmonth . "/" . $byear;
                if (($bmonth != '') && ($bday != '')) {
                    $zodiac_info = $this->Birthday->getUserData("mapzodiac", "(fdate <='$bday' and fmonth='$bmonth') or (tdate>='$bday' and tmonth ='$bmonth')")->row_array();

                    $updated_data["zodiac_id"] = $zodiac_info["zodiac_id"];
                    $updated_data["bmonth"] = $bmonth;
                    $updated_data["bdate"] = $bday;
                    $updated_data["byear"] = $byear;
                } else {
                    $updated_data["bmonth"] = '';
                    $updated_data["bdate"] = '';
                    $updated_data["byear"] = '';
                }
                $updated_data["updated_date"] = date("Y-m-d H:i:s");

                $where = "user_id = '$user_id'";
                $user_details = $this->Birthday->getSelectData("*", "users", $where)->row_array();
                if ($country_id == 102) {
                    if ($user_details['mobile_no'] == $mobile_no) {
                        $where = "user_id='$user_id'";
                        $updated_data = $this->Birthday->update("users", $updated_data, $where);
                        if ($updated_data == 1) {
                            $user_info = $this->Birthday->getUserData("users", $where)->row_array();
                            $data["userdata"] = $user_info;
                            $data["userdata"]["flower_data"] = $this->session->userdata["userdata"]["flower_data"];
                            $data["userdata"]["voucher_data"] = $this->session->userdata["userdata"]["voucher_data"];
                            $this->session->set_userdata($data);
                            $output["success"] = true;
                            $output["message"] = "Profile is updated successfully!";
                            $output["is_mobile_changed"] = 0;
                        }
                    } else {
                        if ($country_id == 102) {
//==========================OTP==========================
                            $data["code"] = $code = $this->generateRandomString(5);
                            $data["mobile_no"] = $mobile_no;
                            $otp_data = $this->Birthday->getSelectData("mobile_no", "otp", "mobile_no=$mobile_no")->result_array();
                            $count = count($otp_data);
                            if ($count > 0) {
                                $update["code"] = $code;
                                $update["sent_time"] = date("Y-m-d H:i:s");
                                $this->Birthday->update("otp", $update, "mobile_no='$mobile_no'");
                            } else {
                                $data["created_date"] = date("Y-m-d H:i:s");
                                $data["sent_time"] = date("Y-m-d H:i:s");
                                $this->Birthday->addData("otp", $data);
                            }
                            $show_message = "Enter $code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone.";
                            try {
                                $resp = $this->Birthday->send_mobile_otp($mobile_no, $show_message);
                                $output["success"] = true;
//                $output["message"] = "OTP sent successfully";
                            } catch (Exception $e) {
                                $output["success"] = false;
//                $output["message"] = "Failed.Please try again";
                            }
//==========================OTP==========================
                            $output["success"] = true;
                            $output["is_mobile_changed"] = 1;
                        } else {
                            $where = "user_id='$user_id'";
                            $updated_data = $this->Birthday->update("users", $updated_data, $where);
                            $user_info = $this->Birthday->getUserData("users", $where)->row_array();
                            $data["userdata"] = $user_info;
                            $data["userdata"]["flower_data"] = $this->session->userdata["userdata"]["flower_data"];
                            $data["userdata"]["voucher_data"] = $this->session->userdata["userdata"]["voucher_data"];
                            $this->session->set_userdata($data);
                            $output["success"] = true;
                            $output["message"] = "Profile is updated successfully!";
                            $output["is_mobile_changed"] = 0;
                        }
                    }
                } else {
                    $where = "user_id='$user_id'";
                    $updated_data = $this->Birthday->update("users", $updated_data, $where);
                    $user_info = $this->Birthday->getUserData("users", $where)->row_array();
                    $data["userdata"] = $user_info;
                    $data["userdata"]["flower_data"] = $this->session->userdata["userdata"]["flower_data"];
                    $data["userdata"]["voucher_data"] = $this->session->userdata["userdata"]["voucher_data"];
                    $this->session->set_userdata($data);
                    $output["success"] = true;
                    $output["message"] = "Profile is updated successfully!";
                    $output["is_mobile_changed"] = 0;
                }
            } else {
                $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again to continue");
                redirect(WEB_HOME);
            }
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

// ====================================================================================================================================//
// -> Function     :dashboard
// -> Executed     :On click of dashboard this function get call and loads dashboard view
// -> description  :Displays user data
// ====================================================================================================================================//
    public function dashboard() {
        if ($this->logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $where = "user_id='$user_id'";
            $userdata = $this->Birthday->getSelectData("first_name,email,mobile_no,bdate,bmonth,byear,membership_status,user_profile_pic", "users", $where)->row_array();
            $data["first_name"] = $userdata["first_name"];
            $data["email"] = $userdata["email"];
            $data["mobile_no"] = $userdata["mobile_no"];
            $data["bdate"] = $userdata["bdate"];
            $data["bmonth"] = $m = $userdata["bmonth"];
            $data["byear"] = $userdata["byear"];
//            $data["membership_status"] = $userdata["membership_status"];
            $data["profile_pic"] = $userdata["user_profile_pic"];
            $month = date("m");
            $where = "bmonth=$month and user_id=$user_id ";
            $data["reminders"] = $this->Birthday->getJoinedDataThreetable("bday_reminder.reminder_id,bday_reminder.user_id,bday_reminder.plus_id,bday_reminder.first_name,bday_reminder.last_name,bday_reminder.bdate,bday_reminder.bmonth,bday_reminder.byear,bday_reminder.zodiac_id,bday_reminder.mobile_no,bday_reminder.email,relationship.relation_id,bday_reminder.gender,bday_reminder.priority,bday_reminder.birth_date,mapzodiac.zodiac_sign,mapzodiac.zimage,bday_reminder.country_id", "bday_reminder", "mapzodiac", "relationship", "bday_reminder.zodiac_id =mapzodiac.zodiac_id", "relationship.relation_id=bday_reminder.relation_id", "bday_reminder.bmonth=$month and bday_reminder.user_id=$user_id ORDER BY STR_TO_DATE(bday_reminder.birth_date,'%d/%m/%Y' ) ,bdate ASC", "left")->result_array();
//            $membership_data = $this->Birthday->checkMembershipExpiry($user_id)->row_array();
//            $membershipcount = count($membership_data);
//            if ($membershipcount > 0) {
//                $edate = $membership_data["expiry_date"];
//                $data["change_status"] = "To Enjoy Membership Plan Renew it before $edate";
//            } else if ($data["membership_status"] == 1) {
//                $data["change_status"] = "Enjoy Membership Plan For Next 1 Year!!!";
//            }
            $data["change_status"] = "Enjoy Free Membership Plan!!!";
            $current_month = date("$m");
            $month_name = date("Y-$current_month-d");
            $name = strtotime($month_name);
            $date = date("F", $name);
            $data["month"] = $date;

            if ($data["bdate"] != '') {
                $data["cdate"] = $this->addOrdinalNumberSuffix(trim($data["bdate"]));
            } else {
                $data["cdate"] = '';
            }
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=9")->row_array();
            $data["seo"] = $title;
            $this->load->view('BIRTHDAY_REMINDER/Main_dashboard', $data);
        } else {
            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again to continue");
            redirect(WEB_HOME);
        }
    }

// ====================================================================================================================================//
// -> Function     :add_Birthday
// -> Executed     :On click of Add Reminder this function get call and loads Add_reminder view
// -> description  :Displays reminder view
// ====================================================================================================================================//

    public function add_Birthday() {
        $rem_id = $this->uri->segment(2);
        $reminder_data = array();
        if ($this->logged_in()) {
            if ($rem_id == 0) {
                $data["rem_data"] = array(
                    'reminder_id' => '',
                    'user_id' => '',
                    'plus_id' => '',
                    'first_name' => '',
                    'last_name' => '',
                    'bdate' => '',
                    'bmonth' => '',
                    'byear' => '',
                    'zodiac_id' => '',
                    'mobile_no' => '',
                    'reminder_created_date' => '',
                    'updated_date' => '',
                    'email' => '',
                    'relation_id' => '',
                    'gender' => '',
                    'priority' => '1',
                    'birth_date' => '',
                    'bdate' => '',
                    'bmonth' => '0',
                    'byear' => '',
                    'rem_image_name' => '',
                    'status' => '',
                    'zodiac_sign' => '',
                    'relation_name' => '',
                    'country_id' => 102
                );
                $data["btn_click"] = "add_birthday";
                $data["bdate"] = '';
                $data["monthname"] = '';
            } else {
                $bday_data = $this->Birthday->getUserData("bday_reminder", "reminder_id='$rem_id'")->row_array();
                $birthdate = $bday_data["birth_date"];
                $birth_date = explode('-', $birthdate);
                $data["bdate"] = $birth_date[0];
                $m = $birth_date[1];
                $current_month = date("$m");
                $month_name = date("Y-$current_month-d");
                $name = strtotime($month_name);
                $data["monthname"] = date("F", $name);
                $reminder_data["reminder_data"] = $bday_data;
                $zodiac_id = $bday_data["zodiac_id"];
                $reminder_data["zodiac_sign"] = $this->Birthday->getSelectData("zodiac_sign", "mapzodiac", "zodiac_id='$zodiac_id'")->row_array();
                $relation_id = $bday_data["relation_id"];
                $reminder_data["relation_name"] = $this->Birthday->getSelectData("relation_name", "relationship", "relation_id='$relation_id'")->row_array();
                $data["rem_data"] = array_merge($reminder_data["reminder_data"], $reminder_data["zodiac_sign"]);
                $data["btn_click"] = "update_birthday";
                $data['authUrl'] = '';
            }
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=10")->row_array();
            $data["seo"] = $title;
            $this->load->view('BIRTHDAY_REMINDER/Add_reminder', $data);
        } else {
            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again to continue");
            redirect(WEB_HOME);
        }
    }

// ====================================================================================================================================//
// -> Function     :reminder_success
// -> Executed     :On click of Add in add_reminder view this function get call and adds reminders data
// -> description  :Adds reminder data in database
// ====================================================================================================================================//
    public function reminder_success() {
        $postcount = count($_POST);
//        print_r($_POST);
//        exit;
        if ($postcount > 0) {
            if ($this->logged_in()) {
                $reminder_already_added = $this->input->post("reminder_already_added");
                $reminder_id = $this->input->post("reminder_id");
                $bday_data["user_id"] = $user_id = $this->session->userdata["userdata"]["user_id"];
                $bday_data["first_name"] = check_post($this->input->post("first_name"));
                $bday_data["last_name"] = "";
//  $fullbirthdate = check_post($this->input->post("birth_date"));
                $bday_data["bmonth"] = check_post($this->input->post("bmonth"));
                $bday_data["bdate"] = check_post($this->input->post("bdate"));
                $bday_data["byear"] = check_post($this->input->post("byear"));

                $cbdate = $bday_data["bdate"] . "-" . $bday_data["bmonth"] . "-" . "0000";

//                $newcbdate = strtotime($cbdate);
                $date = date_create($cbdate);
                $bday_data["birth_date"] = date_format($date, 'Y-m-d');

                $bday_data["country_id"] = check_post($this->input->post("country_id"));
                $bday_data["plus_id"] = "";
                $bday_data["rem_image_name"] = "";
                $bday_data["relation_id"] = "";
                $bday_data["mobile_no"] = $mobileno = check_post($this->input->post("mobile_no"));
                $bday_data["email"] = $email = check_post($this->input->post("email"));
                $bday_data["gender"] = check_post($this->input->post("gender"));
                $bday_data["priority"] = check_post($this->input->post("priority"));
                $bday_data["reminder_created_date"] = date("Y-m-d H:i:s");
                $zodiac_sign = $this->input->post("zodiac_sign");
                $where = "zodiac_sign='$zodiac_sign'";
                $zodic_id = $this->Birthday->getUserData("mapzodiac", $where)->row();
                $z_id = $zodic_id->zodiac_id;
                $bday_data["zodiac_id"] = $z_id;
                if ($reminder_already_added == '') {
                    $reminder_already_added = 0;
                }

                if ($reminder_id == '') {
//                    $isexist = $this->Birthday->getSelectData("reminder_id", 'bday_reminder', "email='$email' and mobile_no='$mobileno' and user_id=$user_id")->num_rows();
                    $isexist = $this->Birthday->getSelectData("reminder_id", 'bday_reminder', "mobile_no='$mobileno' and user_id=$user_id")->num_rows();
                    if ($isexist > 0) {
                        if ($reminder_already_added == 0) {
                            $output["success"] = false;
                            $output["message"] = "Reminder already exists.";
                            echo json_encode($output);
                            exit;
                        } else {
                            $this->Birthday->addData("bday_reminder", $bday_data);
                            $output["success"] = TRUE;
                            $output["message"] = "Reminder Added Successfully";
                        }
                    } else {
                        $this->Birthday->addData("bday_reminder", $bday_data);
                        $output["success"] = TRUE;
                        $output["message"] = "Reminder Added Successfully";
                    }
                }

                if ($reminder_id != '') {
                    $reminder_data = $this->Birthday->getSelectData("*", 'bday_reminder', "reminder_id = $reminder_id")->row_array();
//                    print_r($reminder_data);
//                    exit;
                    if ($reminder_data['mobile_no'] == $mobileno) {
                        $where = "reminder_id='$reminder_id'";
                        $this->Birthday->update("bday_reminder", $bday_data, $where);
                        $output["success"] = TRUE;
                        $output["message"] = "Reminder Updated Successfully";
                    } else {
//$isexist = $this->Birthday->getSelectData("reminder_id", 'bday_reminder', "email='$email' and mobile_no='$mobileno' and user_id=$user_id and reminder_id!=$reminder_id")->num_rows();
                        $isexist = $this->Birthday->getSelectData("reminder_id", 'bday_reminder', "mobile_no = '$mobileno' and user_id = $user_id and reminder_id != $reminder_id")->num_rows();
                        if ($isexist > 0) {
                            if ($reminder_already_added == 0) {
                                $output["success"] = false;
                                $output["message"] = "Reminder already exists.";
                                echo json_encode($output);
                                exit;
                            } else {
                                $where = "reminder_id='$reminder_id'";
                                $this->Birthday->update("bday_reminder", $bday_data, $where);
                                $output["success"] = TRUE;
                                $output["message"] = "Reminder Updated Successfully";
                            }
                        } else {
                            $where = "reminder_id='$reminder_id'";
                            $this->Birthday->update("bday_reminder", $bday_data, $where);
                            $output["success"] = TRUE;
                            $output["message"] = "Reminder Updated Successfully";
                        }
                    }
                }
            } else {
                $output["success"] = False;
                $output["message"] = "Sorry your session has been expired.Please login again to continue";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

// ====================================================================================================================================//
// -> Function     :know_horoscope
// -> Executed     :On click of know_horoscope this function get call and loads know_horoscope view
// -> description  :Displays horoscope
// ====================================================================================================================================//


    public function know_horoscope() {
        if ($this->logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $where = "user_id='$user_id'";
            $user_data = $this->Birthday->getUserData("users", $where)->result_array();
            if (($user_data[0]["bdate"] != "") && ($user_data[0]["bmonth"] != "")) {
                $b = $user_data[0]["bdate"];
                $m = $user_data[0]["bmonth"];
                $zodiac_info = $this->Birthday->getUserData("mapzodiac", "(fdate <='$b' and fmonth='$m') or (tdate>='$b' and tmonth ='$m')")->row_array();
                $bdate = $user_data[0]["bdate"];
                $bmonth = $user_data[0]["bmonth"];
                $zodiac_id = $zodiac_info["zodiac_id"];
                $where = "sign_day='$bdate' and sign_month='$bmonth'";
                $zodiac_data = $this->Birthday->getUserData("zodiac_signs", $where)->result_array();
                $data["sign_data"] = $zodiac_data[0]["sign_data"];
                $end = stripos($data["sign_data"], 'Children and Family');
                $data['first_part'] = substr($data["sign_data"], 0, $end);
                $start = stripos($data["sign_data"], 'Children and Family');
                $text = substr($data["sign_data"], $start);
                $word = "Children and Family";
                $data["second_part"] = str_ireplace($word, "<b>$word</b>", $text);
                $data["zodiac_id"] = $zodiac_id;
            } else {
                $data["sign_data"] = '';
                $data["zodiac_id"] = '';
            }
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=12")->row_array();
            $data["seo"] = $title;
            $this->load->view('BIRTHDAY_REMINDER/know_horoscope', $data); //loading signup view
        } else {
            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again to continue");
            redirect(WEB_HOME);
        }
    }

// ====================================================================================================================================//
// -> Function     :know_horoscope_reminder
// -> Executed     :On click of dashboard reminder this function get call 
// -> description  :Displays horoscope
// ====================================================================================================================================//
    public function know_horoscope_reminder($zodiac_id = null, $bdate = null, $bmonth = null) {
        if ($this->logged_in()) {
            if ($bdate != null && $bmonth != null && $zodiac_id != null) {
                $bdate = $bdate;
                $bmonth = $bmonth;
                $zodiac_id = $zodiac_id;
                $where = "sign_day='$bdate' and sign_month='$bmonth'";
                $zodiac_data = $this->Birthday->getUserData("zodiac_signs", $where)->result_array();
                $data["sign_data"] = $zodiac_data[0]["sign_data"];
                $end = stripos($data["sign_data"], 'Children and Family');
                $data['first_part'] = substr($data["sign_data"], 0, $end);
                $start = stripos($data["sign_data"], 'Children and Family');
                $text = substr($data["sign_data"], $start);
                $word = "Children and Family";
                $data["second_part"] = str_ireplace($word, "<b>$word</b>", $text);
                $data["zodiac_id"] = $zodiac_id;
            }
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=12")->row_array();
            $data["seo"] = $title;

            $this->load->view('BIRTHDAY_REMINDER/know_horoscope', $data); //loading signup view
        } else {
            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again to continue");
            redirect(WEB_HOME);
        }
    }

// ====================================================================================================================================//
// -> Function     :view_bday_reminder
// -> Executed     :On click of view reminder this function get call 
// -> description  :Displays added reminders and can update reminders
// ====================================================================================================================================//
    public function view_bday_reminder() {
        if ($this->logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $where = "user_id='$user_id'";
//            $data["reminder_info"] = $this->Birthday->getJoinedDataThreetable("bday_reminder.reminder_id,bday_reminder.user_id,bday_reminder.plus_id,bday_reminder.first_name,bday_reminder.last_name,bday_reminder.bdate,bday_reminder.bmonth,bday_reminder.byear,bday_reminder.zodiac_id,bday_reminder.mobile_no,bday_reminder.email,relationship.relation_id,bday_reminder.gender,bday_reminder.priority,bday_reminder.birth_date,mapzodiac.zodiac_sign,mapzodiac.zimage,bday_reminder.country_id", "bday_reminder", "mapzodiac", "relationship", "bday_reminder.zodiac_id =mapzodiac.zodiac_id", "relationship.relation_id=bday_reminder.relation_id", " bday_reminder.user_id=$user_id ORDER BY STR_TO_DATE(bday_reminder.birth_date,'%d/%m/%Y' ) ASC", "left")->result_array();
            $data["reminder_info"] = $this->Birthday->getJoinedDataThreetable("bday_reminder.reminder_id,bday_reminder.user_id,bday_reminder.plus_id,bday_reminder.first_name,bday_reminder.last_name,bday_reminder.bdate,bday_reminder.bmonth,bday_reminder.byear,bday_reminder.zodiac_id,bday_reminder.mobile_no,bday_reminder.email,relationship.relation_id,bday_reminder.gender,bday_reminder.priority,bday_reminder.birth_date,mapzodiac.zodiac_sign,mapzodiac.zimage,bday_reminder.country_id", "bday_reminder", "mapzodiac", "relationship", "bday_reminder.zodiac_id =mapzodiac.zodiac_id", "relationship.relation_id=bday_reminder.relation_id", " bday_reminder.user_id=$user_id ORDER BY DATE_FORMAT(bday_reminder.birth_date,'%m-%d') ASC", "left")->result_array();
            $data["count"] = count($data['reminder_info']);
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=11")->row_array();
            $data["seo"] = $title;
            $this->load->view('BIRTHDAY_REMINDER/view_reminders', $data); //loading signup view
        } else {
            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again to continue");
            redirect(WEB_HOME);
        }
    }

// ====================================================================================================================================//
// -> Function     :unsubscribe_mail
// -> Executed     :On click of unsubscribe this function get call and load view 
// -> description  :Displays unsubscribe status
// ====================================================================================================================================//

    public function unsubscribe_mail() {
        if ($this->logged_in()) {
            $email = $this->Birthday->decryptPassword($this->uri->segment(4));
            $status = $this->uri->segment(3);
            $where = "email_id='$email'";
            $update["mail_status"] = $status;
            $this->Birthday->update("news_letter", $update, $where);
            if ($status == 1) {
                $data["title"] = "Unsubscribed";
                $data["message"] = "You're unsubscribed.";
                $data["clink"] = BASEURL . "Home_web/unsubscribe_mail/0/" . $this->Birthday->encryptPassword($email);
                $data["link"] = "Re-subscribed?";
            } else {
                $data["title"] = "Subscribed";
                $data["message"] = "You're subscribed!";
                $data["link"] = "";
                $data["clink"] = "";
            }
            $this->load->view("BIRTHDAY_REMINDER/subscribe", $data);
        } else {
            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again to continue");
            redirect(WEB_HOME);
        }
    }

// ====================================================================================================================================//
// -> Function     :unsubscribe
// -> Executed     :On click of unsubscribe this function get call and load view 
// -> description  :Displays unsubscribe status
// ====================================================================================================================================//
    public function unsubscribe() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $email = $this->session->userdata["userdata"]["email"];
            $status = check_post($this->input->post("mail_status"));


            if ($status == '1') {
                $where = "email_id='$email'";
                $subscribed_data = $this->Birthday->getUserData("news_letter", $where)->row_array();
                if ($subscribed_data['mail_status'] == '1') {
                    $output["success"] = false;
                    $output["message"] = "You're already unsubscribed";
                } else {
                    $update["mail_status"] = $status;
                    $this->Birthday->update("news_letter", $update, $where);
                    $output["success"] = true;
                }
            } else {
                $where = "email_id='$email'";
                $update["mail_status"] = $status;
                $this->Birthday->update("news_letter", $update, $where);
                $output["success"] = true;
            }
        }

//        Array
//        (
//        [id] => 245
//        [email_id] => aakashmkaria@gmail.com
//        [first_name] => Aakash
//        [last_name] =>
//        [nimage_name] =>
//        [mail_status] => 0
//        )

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

// ====================================================================================================================================//
// -> Function     :check_birthdate
// -> Executed     :On click of know horoscope if birthdate is not there then it gives following message
// -> description  :checks birthdate
// ====================================================================================================================================//
    public function check_birthdate() {
        if ($this->logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $bcount = $this->Birthday->getSelectData("birth_date,user_id", "users", "user_id='$user_id' and birth_date!=''")->row_array();
            if (count($bcount) == 0) {
                $output["success"] = false;
                $output["message"] = "In order to view your Horoscope ,Please Update  birth date in update profile";
            } else {
                $output["success"] = true;
            }
        } else {
            $output["success"] = false;
            $output["message"] = "Sorry your session has been expired.Please login again to continue";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

// ====================================================================================================================================//
// -> Function     :getStatusByOrders
// -> Executed     :called this function inside order history
// -> description  :It checks status of purchased order and changes status
// ====================================================================================================================================//

    public function getStatusByOrders() {
        if ($this->logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
//Old
//            $data["history"] = $this->Birthday->getJoinedData2("op.order_message,op.delivery_date_time,op.order_pro_id, op.delievery_schedule, op.fphone,op.voucher_pro_name,op.type,op.voucher_img,op.delivery_status,op.selected_amount,op.femail,op.fname, op.unique_oid,op.delivery_date_time, g.front_page_image,op.uploded_img_video,count(op.unique_oid)as quantity", "order_products as op", "greeting_card as g", "op.greeting_id=g.card_id", "op.user_id=$user_id and op.delivery_status!='' group by op.unique_oid order by op.order_pro_id DESC")->result_array();
//New
            $data["history"] = $this->Birthday->getJoinedData2("op.order_message,op.delivery_date_time,op.order_pro_id, op.delievery_schedule, op.fphone,op.voucher_pro_name,op.type,op.voucher_img,op.delivery_status,op.selected_amount,op.femail,op.fname, op.unique_oid,op.delivery_date_time, g.front_page_image,op.uploded_img_video,count(op.unique_oid)as quantity", "order_products as op", "greeting_card as g", "op.greeting_id=g.card_id", "op.user_id=$user_id and op.delivery_status!='' group by op.payment_id order by op.order_pro_id DESC")->result_array();

//            $data["history"] = $this->Birthday->get_order_history($user_id)->result_array();
            $data["count"] = count($data["history"]);
            $data["success"] = "true";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($data));
        }
    }

    public function gift_wallet() {
        if ($this->logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $userinfo = $this->Birthday->getSelectData("email,user_id", "users", "user_id='$user_id'")->row_array();
            $email = $userinfo["email"];
//  $data["ehistory"] = $this->Birthday->getJoinedData2("greeting_card.front_page_image,order_products.uploded_img_video,   order_products.uemail as femail, order_products.uname as fname, order_products.unique_oid, order_products.voucher_img,   order_products.order_pro_id,order_products.voucher_pro_id,order_products.voucher_pro_name,order_products.selected_amount,order_products.greeting_id,order_products.greeting_message,count(order_products.unique_oid) as quantity", "order_products", "greeting_card", "order_products.greeting_id=greeting_card.card_id", "order_products.delivery_status=4 and order_products.femail='$email' group by order_products.unique_oid order by order_products.order_pro_id DESC")->result_array();
            $data["ehistory"] = $this->Birthday->getJoinedDataThreetable("greeting_card.front_page_image,order_products.uploded_img_video, u.mobile_no as fphone, order_products.type, order_products.uemail as femail, order_products.uname as fname, order_products.unique_oid, order_products.voucher_img,   order_products.order_pro_id,order_products.voucher_pro_id,order_products.voucher_pro_name,order_products.selected_amount,order_products.greeting_id,order_products.greeting_message,count(order_products.unique_oid) as quantity", "order_products", "greeting_card", "users as u", "order_products.greeting_id=greeting_card.card_id", "u.user_id=order_products.user_id", "order_products.delivery_status=4 and order_products.femail='$email' group by order_products.unique_oid order by order_products.order_pro_id DESC", "inner")->result_array();
            $data["count"] = count($data["ehistory"]);
            $data["history"] = $this->Birthday->getJoinedData2("f.fid,f.greeting_img,f.message,u.first_name as fname,u.email as femail,u.mobile_no as fphone,f.status,f.type,f.created_date,f.uploded_img,f.delivery_date", "free_greetings f", "users u", "f.user_id=u.user_id", " f.femail='$email'  order by f.fid DESC")->result_array();
            $data["count"] = count($data["history"]);
            $data["flower_cake_history"] = $this->Birthday->simple_query("Select flower_order_id,flower_receiver_email,flower_unique_oid,flower_purchased_image,flower_receiver_name,flower_receiver_phone,flower_transaction_status,flower_sender_id,flower_sender_email from order_to_fnp where flower_sender_email='$email'")->result_array();
            $data["count"] = count($data["flower_cake_history"]);
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=23")->row_array();
            $data["seo"] = $title;
            $this->load->view('BIRTHDAY_REMINDER/giftWallet', $data);
        }
    }

//End-Birthday Reminder
//Start-Zodiac
// ====================================================================================================================================//
// -> Function     :horoscope
// -> Executed     :On click of zodiac this function get call and load view zodiac_list 
// -> description  :Displays all zodiac signs
// ====================================================================================================================================//

    public function horoscope() {
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=2")->row_array();
        $data["seo"] = $title;
        $this->load->view('ZODIAC/zodiac_list', $data);
    }

// ====================================================================================================================================//
// -> Function     :select_zodiac
// -> Executed     :On click of each zodiac sign this function get call and load view inside_zodiac 
// -> description  :Displays information of clicked zodiac sign
// ====================================================================================================================================//
    public function select_zodiac() {
        $zodiac_id = $this->uri->segment(2);
        $where = "zodiac_id='$zodiac_id'";
        $zodiac_range = $this->Birthday->getUserData("mapzodiac", $where)->row_array();
        $data["zodiac_id"] = $zodiac_id;
        $data["fmonth"] = $zodiac_range["fmonth"];
        $data["fdate"] = $zodiac_range["fdate"];
        $data["tmonth"] = $zodiac_range["tmonth"];
        $data["tdate"] = $zodiac_range["tdate"];
        $data["zodiac_sign"] = $zodiac_range["zodiac_sign"];
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=17")->row_array();
        $data["seo"] = $title;
        $this->load->view('ZODIAC/inside_zodiac', $data); //loading How-it-Works
    }

//End-Zodiac
//start-egreeting
// ====================================================================================================================================//
// -> Function     :egreetings
// -> Executed     :On click of free greeting section free_greeting view get load
// -> description  :Displays all greetings
// ====================================================================================================================================//
    public function egreetings() {
//        $where = "greeting_type=1 order by position=0,position ASC";//Old
//        $where = "greeting_type=1 order by card_id DESC"; //New
        $where = "greeting_type=1 ORDER BY position = 0 ASC , position ASC";
        $data["greeting_cards"] = $this->Birthday->getSelectData("card_id,front_page_image,card_name", "greeting_card", $where)->result_array();
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=4")->row_array();
        $data["seo"] = $title;
        $data["fid"] = 0;
        $this->load->view('FREEGREETING/free_greetings', $data);
    }

    public function checkThankyou() {
        $data["url"] = str_replace("checkThankyou", "thankyou", $this->uri->uri_string());
        $this->load->library('user_agent');
//        if ($this->agent->is_mobile()) {
//            $this->load->view("HOME/CommonDeepLinkingView", $data);
//        } else 
        if ($this->agent->is_browser()) {
//            $where = "greeting_type=3 order by position=0,position ASC";//Old
            $where = "greeting_type=3 order by rder by card_id DESC"; //New
            $data["greeting_cards"] = $this->Birthday->getSelectData("card_name,card_id,front_page_image", "greeting_card", $where)->result_array();
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=4")->row_array();
            $data["seo"] = $title;
            $data["fid"] = $this->uri->segment(2);
            $this->load->view("FREEGREETING/free_greetings", $data);
        } else {

            redirect(WEB_HOME);
        }
    }

    public function thankyou() {
        $fid = $this->uri->segment(2);
        if ($this->uri->segment(3) == 2) {
            $fdata = $this->Birthday->getSelectData("free_greetings.user_id,free_greetings.femail", "free_greetings", "fid=$fid")->row_array();
            $luser_id = $fdata["user_id"];
            $lemail = $fdata["femail"];
            $udata = $this->Birthday->getSelectData("users.first_name,users.email,users.mobile_no as fphone", "users", "user_id='$luser_id' ")->row_array();
            $data["femail"] = $udata["email"];
            $data["fname"] = $udata["first_name"];
            $data["fphone"] = $udata["fphone"];
            $fuser_id = $this->Birthday->getSelectData("user_id as user_id", "users", "email='$lemail' and is_guest=0")->row_array();
            if (count($fuser_id) != "") {
                $data["user_id"] = $fuser_id["user_id"];
            } else {
                $data["user_id"] = 0;
            }
            $data["from"] = 4; //change password:1;2:freegreeting 3:Giftgreeting,4:Thank_you
            $this->load->view("HOME/deep_linking", $data);
        } else {
            $where = "greeting_type=3 order by position=0,position ASC";
            $data["greeting_cards"] = $this->Birthday->getSelectData("card_id,front_page_image", "greeting_card", $where)->result_array();
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=4")->row_array();
            $data["seo"] = $title;
            $data["fid"] = $fid;
            $this->load->view('FREEGREETING/free_greetings', $data);
        }
    }

// ====================================================================================================================================//
// -> Function     :freegreeting_details
// -> Executed     :On click of each  greeting fgreeting_inside view get load
// -> description  :Displays information of individual greeting
// ====================================================================================================================================//
    public function freegreeting_details() {
        $fid = $this->uri->segment(4);
        $card_id = $this->uri->segment(2);
        $reminder_id = $this->uri->segment(3);
        $data["card_details"] = $this->Birthday->getSelectData("front_page_image,card_id,greet_cat_id,card_name", "greeting_card", "card_id='$card_id'")->row_array();

        if ($reminder_id == "0" && $fid == 0) {
            $data["gcount"] = $this->Birthday->getSelectData("card_id,front_page_image", "greeting_card", "greeting_type=1")->num_rows();
            $data["reminder_info"] = array(
                'first_name' => '',
                'email' => '',
                'fphone' => ''
            );
            $data["type"] = "1";
            $data["user_id"] = $fid;
            $fuser_id = 0;
        } else if ($fid == 0) {
            $data["gcount"] = $this->Birthday->getSelectData("card_id,front_page_image", "greeting_card", "greeting_type=1")->num_rows();
            $data["reminder_info"] = $this->Birthday->getSelectData("first_name,email,mobile_no as fphone", "bday_reminder", "reminder_id='$reminder_id'")->row_array();
            $data["type"] = "1";
            $data["user_id"] = $fid;
            $fuser_id = 0;
        } else {
            $data["gcount"] = $this->Birthday->getSelectData("card_id,front_page_image", "greeting_card", "greeting_type=3")->num_rows();
            $fdata = $this->Birthday->getSelectData("free_greetings.user_id,free_greetings.femail", "free_greetings", "fid=$fid")->row_array();
            $luser_id = $fdata["user_id"];
            $lemail = $fdata["femail"];
            $data["reminder_info"] = $this->Birthday->getSelectData("users.first_name,users.email,users.mobile_no as fphone", "users", "user_id='$luser_id' ")->row_array();
            $fuser_id_data = $this->Birthday->getSelectData("user_id as user_id", "users", "email='$lemail' and is_guest=0")->row_array();
            $fuser_id = $fuser_id_data["user_id"];
            $data["type"] = "3";
            $data["user_id"] = $fdata["user_id"];
        }
//        if ($this->check_logged_in()) {
//            $user_id = $this->session->userdata["userdata"]["user_id"];
//
//            //Here we are checking wheather logged in user is same or not
//            if ($fuser_id != $user_id && $fuser_id != 0) {
//                $this->session->unset_userdata('userdata');
//            }
//            $getsentcounter = $this->Birthday->getSelectData("sent_greeting_counter,membership_status", "users", "user_id='$user_id'")->row_array();
//            $counter = $getsentcounter["sent_greeting_counter"];
//            $status = $getsentcounter["membership_status"];
//            $data["membership_status"] = $status;
//            if ($counter < 10 && $status == 0) {
//                $count = 10 - $counter;
//                $data["greeting_count"] = "$count <br>free greetings left";
//            } else if ($status == 1) {
//                $data["greeting_count"] = "Enjoy Unlimited Free greetings";
//            } else {
//                $data["greeting_count"] = "Oops!!! <br/>10 Free greetings over";
//            }
//        } else {
//            $data["membership_status"] = 0;
//            $data["greeting_count"] = "Enjoy your first 10 free greetings";
//        }
//New
        //check if session is available for the user data
        if ($this->check_logged_in()) {
            $data['user_logged_in'] = TRUE;
            $session_user_data = $this->session->userdata('userdata');
            $data['session_data'] = $session_user_data;
        } else {
            $data['user_logged_in'] = FALSE;
            $data['session_data'] = array();
        }
        $data["greeting_count"] = "Enjoy Unlimited Free greetings";
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=6")->row_array();
        $title['title'] = $data['card_details']['card_name'];
        $title['description'] = $data['card_details']['card_name'];
        $data["seo"] = $title;
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $this->getTestimonials();
        $this->load->view('FREEGREETING/fgreeting_inside', $data);
    }

    public function freegreeting_details_new() {
        $fid = $this->uri->segment(4);
        $card_id = $this->uri->segment(2);
        $reminder_id = $this->uri->segment(3);
        $data["card_details"] = $this->Birthday->getSelectData("front_page_image,card_id,greet_cat_id,card_name", "greeting_card", "card_id='$card_id'")->row_array();

        if ($reminder_id == "0" && $fid == 0) {
            $data["gcount"] = $this->Birthday->getSelectData("card_id,front_page_image", "greeting_card", "greeting_type=1")->num_rows();
            $data["reminder_info"] = array(
                'first_name' => '',
                'email' => '',
                'fphone' => ''
            );
            $data["type"] = "1";
//            $data["user_id"] = $fid;
            $fuser_id = 0;
        } else if ($fid == 0) {
            $data["gcount"] = $this->Birthday->getSelectData("card_id,front_page_image", "greeting_card", "greeting_type=1")->num_rows();
            $data["reminder_info"] = $this->Birthday->getSelectData("first_name,email,mobile_no as fphone", "bday_reminder", "reminder_id='$reminder_id'")->row_array();
            $data["type"] = "1";
            $data["user_id"] = $fid;
            $fuser_id = 0;
        } else {
            $data["gcount"] = $this->Birthday->getSelectData("card_id,front_page_image", "greeting_card", "greeting_type=3")->num_rows();
            $fdata = $this->Birthday->getSelectData("free_greetings.user_id,free_greetings.femail", "free_greetings", "fid=$fid")->row_array();
            $luser_id = $fdata["user_id"];
            $lemail = $fdata["femail"];
            $data["reminder_info"] = $this->Birthday->getSelectData("users.first_name,users.email,users.mobile_no as fphone", "users", "user_id='$luser_id' ")->row_array();
            $fuser_id_data = $this->Birthday->getSelectData("user_id as user_id", "users", "email='$lemail' and is_guest=0")->row_array();
            $fuser_id = $fuser_id_data["user_id"];
            $data["type"] = "3";
//            $data["user_id"] = $fdata["user_id"];
        }

        $data["greeting_count"] = "Enjoy Unlimited Free greetings";
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=6")->row_array();
        $title['title'] = $data['card_details']['card_name'];
        $title['description'] = $data['card_details']['card_name'];
        $data["seo"] = $title;
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $this->getTestimonials();
        $this->load->view('FREEGREETING/fgreeting_inside', $data);
    }

    public function editegreeting() {
        $fid = $this->uri->segment(2);
        $data["gcount"] = $this->Birthday->getSelectData("card_id,front_page_image", "greeting_card", "greeting_type=1")->num_rows();
        $data["type"] = "1";
        $fdata = $this->Birthday->getSelectData("*", "free_greetings", "fid=$fid")->row_array();
        $data["reminder_info"] = $fdata;

//        $fdata = $this->Birthday->getSelectData("free_greetings.user_id,free_greetings.femail", "free_greetings", "fid=$fid")->row_array();
        $luser_id = $fdata["user_id"];
        $data["user_id"] = $luser_id;
        $lemail = $fdata["femail"];

//        if ($this->check_logged_in()) {
//            $fuser_id_data = $this->Birthday->getSelectData("user_id as user_id", "users", "email='$lemail' and is_guest=0")->row_array();
//            $fuser_id = $fuser_id_data["user_id"];
//            $user_id = $this->session->userdata["userdata"]["user_id"];
//
//            //Here we are checking wheather logged in user is same or not
//            if ($fuser_id != $user_id && $fuser_id != 0) {
//                $this->session->unset_userdata('userdata');
//            }
//            $getsentcounter = $this->Birthday->getSelectData("sent_greeting_counter,membership_status", "users", "user_id='$user_id'")->row_array();
//            $counter = $getsentcounter["sent_greeting_counter"];
//            $status = $getsentcounter["membership_status"];
//            $data["membership_status"] = $status;
//            if ($counter < 10 && $status == 0) {
//                $count = 10 - $counter;
//                $data["greeting_count"] = "$count <br>free greetings left";
//            } else if ($status == 1) {
//                $data["greeting_count"] = "Enjoy Unlimited Free greetings";
//            } else {
//                $data["greeting_count"] = "Oops!!! <br/>10 Free greetings over";
//            }
//            $data["greeting_count"] = "Enjoy Unlimited Free greetings";
//        } else if ($this->check_guest_logged_in()) {
//            $fuser_id_data = $this->Birthday->getSelectData("user_id as user_id", "users", "email='$lemail' and is_guest=1")->row_array();
//            $fuser_id = $fuser_id_data["user_id"];
//            $user_id = $this->session->userdata["userdata"]["user_id"];
//
//            //Here we are checking wheather logged in user is same or not
//            if ($fuser_id != $user_id && $fuser_id != 0) {
//                $this->session->unset_userdata('userdata');
//            }
//            $getsentcounter = $this->Birthday->getSelectData("sent_greeting_counter,membership_status", "users", "user_id='$user_id'")->row_array();
//            $counter = $getsentcounter["sent_greeting_counter"];
//            $status = $getsentcounter["membership_status"];
//            $data["membership_status"] = $status;
//            if ($counter < 10 && $status == 0) {
//                $count = 10 - $counter;
//                $data["greeting_count"] = "$count <br>free greetings left";
//            } else if ($status == 1) {
//                $data["greeting_count"] = "Enjoy Unlimited Free greetings";
//            } else {
//                $data["greeting_count"] = "Oops!!! <br/>10 Free greetings over";
//            }
//        } else {
//            $data["membership_status"] = 0;
//            $data["greeting_count"] = "Enjoy your first 10 free greetings";
//        }
        $data["greeting_count"] = "Enjoy Unlimited Free greetings";
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=6")->row_array();
        $data["seo"] = $title;
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $this->getTestimonials();
        $this->load->view('FREEGREETING/editfgreeting', $data);
    }

// ====================================================================================================================================//
// -> Function     :send_egreeting
// -> Executed     :On click of send button each greeting get sent
// -> description  :adds greeting data in database and sends email
// ====================================================================================================================================//
    public function send_egreeting() {
//        echo '<pre>';
//        print_r($_POST);
//        exit;
        $output = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $rfrom = check_post($this->input->post("rfrom"));
            $card_id = check_post($this->input->post("card_id"));
            $keyid = 'freegreetinglink';
            $type = check_post($this->input->post("gettype"));
            $greeting_cat_id = 0;
            $femail = json_decode(stripslashes($_POST["friend_email"]));
            $fname = json_decode(stripslashes($_POST["friend_name"]));
            $fphone = json_decode(stripslashes($_POST["fphone"]));
            $uploaded_img = check_post($this->input->post("uploaded_finalimg"));

            $fid = check_post($this->input->post("fid"));

            $uploadedpic = str_replace('public/imageVideo', '', $uploaded_img);
            $count = count($fname);
            if ($uploadedpic != "") {
                $data["uploded_img"] = $uploadedpic;
            } else {
                $data["uploded_img"] = check_post($this->input->post("imgcaptureuploaded"));
            }
            $data["type"] = $type;
            $data["greeting_img"] = check_post($this->input->post("greeting_img"));
            $schedule = check_post($this->input->post("schedule"));
//If schedule is 0 that is today then below we are sending greeting
            if ($schedule != "") {
                $date = str_replace('/', '-', $schedule);
                $sdate = date("Y-m-d", strtotime($date));
                $datetime1 = new DateTime($sdate);
                $datetime2 = new DateTime(date("Y-m-d"));
                $interval = $datetime1->diff($datetime2);
                $date_diff = $interval->format('%a');
                if ($date_diff != 0) {
                    $data["delivery_date"] = $sdate;
                    $data["greeting_schedule"] = "1";
                } else {
                    $data["delivery_date"] = $sdate;
                    $data["greeting_schedule"] = "0";
                }
            }
//            else {
//                $data["delivery_date"] = date("Y-m-d");
//                $data["greeting_schedule"] = "0";
//            }
//            $data["title"] = $title = check_post($this->input->post("title"));
            $title = check_post($this->input->post("title"));
            $data["title"] = preg_replace('/[^\da-z ]/i', '', $title);
//            $data["message"] = check_post($this->input->post("message"));
            $message = check_post($this->input->post("message"));
            $data["message"] = preg_replace('/[^\da-z ]/i', '', $message);
            $data["greeting_cat_id"] = $greeting_cat_id;

            if ($fid != "") { //IF user is editing greeting and changed email-id then this condition will become true
                $newgreetinginfo = $this->Birthday->getSelectData("delivery_date,greeting_schedule,uploded_img,message,title,type", "free_greetings", "fid='$fid'")->row_array();
//                    $adddata["delivery_date"] = $newgreetinginfo["delivery_date"]; //2017-06-25
//                    $adddata["greeting_schedule"] = $greeting_schedule = $newgreetinginfo["greeting_schedule"];
                $data["uploded_img"] = $newgreetinginfo["uploded_img"];
                $data["type"] = $newgreetinginfo["type"];
            }

//I have used rfrom to differentiate whether to save data in session or not
            if ($rfrom == 1) {
                if ($this->check_logged_in()) {
                    $mdata["user_id"] = $data["user_id"] = $user_id = $this->session->userdata["userdata"]["user_id"];
                    $data["created_date"] = date("Y-m-d H:i:s");
                    for ($i = 0; $i < $count; $i++) {
                        $data["fname"] = $rec_name = $fname[$i];
                        $data["femail"] = $rec_email = $femail[$i];
                        $data["fphone"] = $rphone = $fphone[$i];
                        $mdata["fid"] = $free_id = $this->Birthday->addData("free_greetings", $data);

//                        if ($is_edit == 0) {
//                        }
                        $mdata["uemail"] = $uemail = $this->session->userdata["userdata"]["email"];
                        $mdata["uname"] = $uname = $this->session->userdata["userdata"]["first_name"];
                        $userphone = $this->session->userdata["userdata"]["mobile_no"];
                        $gcard_id = $this->Birthday->encryptPassword($greeting_cat_id);
                        $freeid = $this->Birthday->encryptPassword($free_id);
                        $key = $this->Birthday->encryptPassword($keyid);
                        $from = $this->Birthday->encryptPassword(0);
                        $updated_data["greeting_info_link"] = $mdata["linkclick"] = $link = BASEURL . "free_greetings_email/$gcard_id/$freeid/$key/$from";
                        $getsentcounter = $this->Birthday->getSelectData("sent_greeting_counter", "users", "user_id='$user_id'")->row_array();
                        $sent_counter = $getsentcounter["sent_greeting_counter"];
                        $mupdated_data["sent_greeting_counter"] = $sent_counter + 1;
                        $this->Birthday->update("users", $mupdated_data, "user_id='$user_id'");
                        if ($data["greeting_schedule"] == "0") {
                            $user_date = date("d F Y");
                            $schedule_Date = date('dS F Y', strtotime($user_date));
                            if ($rec_email != "") {
                                $is_mail_sent = $this->sendGreetingMail($mdata, $rec_email, $rec_name, $uname, $title, $free_id, $link);
                            } else {
                                $is_mail_sent = 0;
                            }

                            if ($rphone != "") {
                                $linkData = $this->google_url_api->shorten("$link");
                                $is_sms_sent = $this->sendGreetingSms($rec_name, $uname, $linkData->id, $rphone, $rec_email, $free_id, $link);
                            } else {
                                $is_sms_sent = 0;
                            }
                            if ($userphone != '') {
                                $show_message = "Dear $uname, \n You have just sent an awesome greeting card to $rec_name and we have notified $rec_name accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                                $this->Birthday->send_sms($userphone, $show_message);
                            }
                            if (($is_mail_sent == 1) || ($is_sms_sent == 1)) {
                                $output["email_list"] = $femail;
                                $output["name_list"] = $fname;
                                $output["mobile_list"] = $fphone;
                                $output["schedule"] = "Today " . $schedule_Date;
                                $output["indicator"] = "0";
                                $output["message"] = "Greeting card sent successfully!.";
                                $output["success"] = true;
                            } else {
                                $output["success"] = false;
                                $output["message"] = "Greeting card  can not send.Try again";
                            }
                        } else {
                            $output["email_list"] = $femail;
                            $output["mobile_list"] = $fphone;
                            $output["name_list"] = $fname;
                            $output["indicator"] = "1";
                            $updated_data["is_sent"] = 2;
                            $where = "fid='$free_id'";
                            $this->Birthday->update("free_greetings", $updated_data, $where);
                            $converted_date = date("dS F Y", strtotime($sdate));
                            $output["success"] = true;
                            $output["schedule"] = "$converted_date";
                            $output["message"] = "Thank You for using BirthdayOwl. Your greeting card will be sent on $converted_date  !";
                        }
                    }
                } else {
                    $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login to send e-greeting!");
                    redirect(BASEURL . "insidegreeting/$card_id/0/0");
                }
            } else {
                $data['card_id'] = $card_id;
                $data['femail'] = $femail;
                $data['fname'] = $fname;
                $data['fphone'] = $fphone;
                $data["schedule"] = $schedule;
                $this->session->set_userdata($data);
                $output["success"] = "true";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function send_egreeting_new() {
//        echo'<pre>';
//        print_r($_POST);
//        exit;

        $output = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $rfrom = check_post($this->input->post("rfrom"));
            $card_id = check_post($this->input->post("card_id"));
            $keyid = 'freegreetinglink';
            $type = check_post($this->input->post("gettype"));
            $greeting_cat_id = 0;
            $femail = json_decode(stripslashes($_POST["friend_email"]));
            $fname = json_decode(stripslashes($_POST["friend_name"]));
            $fphone = json_decode(stripslashes($_POST["fphone"]));
            $uploaded_img = check_post($this->input->post("uploaded_finalimg"));
            $sender_name = check_post($this->input->post("sender_name"));
            $sender_email = check_post($this->input->post("sender_email"));
            $sender_phone = check_post($this->input->post("sender_phone"));

            $fid = check_post($this->input->post("fid"));

            $uploadedpic = str_replace('public/imageVideo', '', $uploaded_img);
            $count = count($fname);
            if ($uploadedpic != "") {
                $data["uploded_img"] = $uploadedpic;
            } else {
                $data["uploded_img"] = check_post($this->input->post("imgcaptureuploaded"));
            }
            $data["type"] = $type;
            $data["greeting_img"] = check_post($this->input->post("greeting_img"));
            $schedule = check_post($this->input->post("schedule"));
//If schedule is 0 that is today then below we are sending greeting
            if ($schedule != "") {
                $date = str_replace('/', '-', $schedule);
                $sdate = date("Y-m-d", strtotime($date));
                $datetime1 = new DateTime($sdate);
                $datetime2 = new DateTime(date("Y-m-d"));
                $interval = $datetime1->diff($datetime2);
                $date_diff = $interval->format('%a');
                if ($date_diff != 0) {
                    $data["delivery_date"] = $sdate;
                    $data["greeting_schedule"] = "1";
                } else {
                    $data["delivery_date"] = $sdate;
                    $data["greeting_schedule"] = "0";
                }
            }
//            else {
//                $data["delivery_date"] = date("Y-m-d");
//                $data["greeting_schedule"] = "0";
//            }
//            $data["title"] = $title = check_post($this->input->post("title"));
            $title = check_post($this->input->post("title"));
            $data["title"] = preg_replace('/[^\da-z ]/i', '', $title);
//            $data["message"] = check_post($this->input->post("message"));
            $message = check_post($this->input->post("message"));
            $data["message"] = preg_replace('/[^\da-z ]/i', '', $message);
            $data["greeting_cat_id"] = $greeting_cat_id;

            if ($fid != "") { //IF user is editing greeting and changed email-id then this condition will become true
                $newgreetinginfo = $this->Birthday->getSelectData("delivery_date,greeting_schedule,uploded_img,message,title,type", "free_greetings", "fid='$fid'")->row_array();
//                    $adddata["delivery_date"] = $newgreetinginfo["delivery_date"]; //2017-06-25
//                    $adddata["greeting_schedule"] = $greeting_schedule = $newgreetinginfo["greeting_schedule"];
                $data["uploded_img"] = $newgreetinginfo["uploded_img"];
                $data["type"] = $newgreetinginfo["type"];
            }

//I have used rfrom to differentiate whether to save data in session or not
            if ($rfrom == 1) {
                if ($this->check_logged_in()) {
                    $mdata["user_id"] = $data["user_id"] = $user_id = $this->session->userdata["userdata"]["user_id"];
                    $data["created_date"] = date("Y-m-d H:i:s");
                    for ($i = 0; $i < $count; $i++) {
                        $data["fname"] = $rec_name = $fname[$i];
                        $data["femail"] = $rec_email = $femail[$i];
                        $data["fphone"] = $rphone = $fphone[$i];
                        $mdata["fid"] = $free_id = $this->Birthday->addData("free_greetings", $data);

//                        if ($is_edit == 0) {
//                        }
                        $mdata["uemail"] = $uemail = $this->session->userdata["userdata"]["email"];
                        $mdata["uname"] = $uname = $this->session->userdata["userdata"]["first_name"];
                        $userphone = $this->session->userdata["userdata"]["mobile_no"];
                        $gcard_id = $this->Birthday->encryptPassword($greeting_cat_id);
                        $freeid = $this->Birthday->encryptPassword($free_id);
                        $key = $this->Birthday->encryptPassword($keyid);
                        $from = $this->Birthday->encryptPassword(0);
                        $updated_data["greeting_info_link"] = $mdata["linkclick"] = $link = BASEURL . "free_greetings_email/$gcard_id/$freeid/$key/$from";
                        $getsentcounter = $this->Birthday->getSelectData("sent_greeting_counter", "users", "user_id='$user_id'")->row_array();
                        $sent_counter = $getsentcounter["sent_greeting_counter"];
                        $mupdated_data["sent_greeting_counter"] = $sent_counter + 1;
                        $this->Birthday->update("users", $mupdated_data, "user_id='$user_id'");
                        if ($data["greeting_schedule"] == "0") {
                            $user_date = date("d F Y");
                            $schedule_Date = date('dS F Y', strtotime($user_date));
                            if ($rec_email != "") {
                                $is_mail_sent = $this->sendGreetingMail($mdata, $rec_email, $rec_name, $uname, $title, $free_id, $link);
                            } else {
                                $is_mail_sent = 0;
                            }

                            if ($rphone != "") {
                                $linkData = $this->google_url_api->shorten("$link");
                                $is_sms_sent = $this->sendGreetingSms($rec_name, $uname, $linkData->id, $rphone, $rec_email, $free_id, $link);
                            } else {
                                $is_sms_sent = 0;
                            }
                            if ($userphone != '') {
                                $show_message = "Dear $uname, \n You have just sent an awesome greeting card to $rec_name and we have notified $rec_name accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                                $this->Birthday->send_sms($userphone, $show_message);
                            }
                            if (($is_mail_sent == 1) || ($is_sms_sent == 1)) {
                                $output["email_list"] = $femail;
                                $output["name_list"] = $fname;
                                $output["mobile_list"] = $fphone;
                                $output["schedule"] = "Today " . $schedule_Date;
                                $output["indicator"] = "0";
                                $output["message"] = "Greeting card sent successfully!.";
                                $output["success"] = true;
                            } else {
                                $output["success"] = false;
                                $output["message"] = "Greeting card  can not send.Try again";
                            }
                        } else {
                            $output["email_list"] = $femail;
                            $output["mobile_list"] = $fphone;
                            $output["name_list"] = $fname;
                            $output["indicator"] = "1";
                            $updated_data["is_sent"] = 2;
                            $where = "fid='$free_id'";
                            $this->Birthday->update("free_greetings", $updated_data, $where);
                            $converted_date = date("dS F Y", strtotime($sdate));
                            $output["success"] = true;
                            $output["schedule"] = "$converted_date";
                            $output["message"] = "Thank You for using BirthdayOwl. Your greeting card will be sent on $converted_date  !";
                        }
                    }
                } else {
//                    $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login to send e-greeting!");
//                    redirect(BASEURL . "insidegreeting/$card_id/0/0");
//                    $getsentcounter = $this->Birthday->getSelectData("counter", "free_greeting_sender", "free_sender_email='$sender_email'")->result_array();
//                    var_dump($getsentcounter);
//                    exit;
                    // the requirement was that to send greeting freely without login so this section sends that.....
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //first check if the email or phone number is of a registered birthday owl user which is not a guest user

                    if ($sender_email != '') {
                        $query = "select user_id,first_name,email,mobile_no from users where email = '$sender_email' and is_guest = '0' and mobile_verify = '1'";
                    } else if ($sender_phone != '') {
                        $query = "select user_id,first_name,email,mobile_no from users where mobile_no ='$sender_phone' and is_guest = '0' and mobile_verify = '1'";
                    } else {
                        $query = "select user_id,first_name,email,mobile_no from users where email = '$sender_email' and is_guest = '0' and mobile_verify = '1'";
                    }

                    // need to check if the user has multipel same phone number for different account
                    $new_result = $this->Birthday->simple_query($query);
//                    print_r($new_result->row_array());
//                    exit;

                    $query_result = $new_result->row_array();


//                    if (empty($query_result)) { // that means he is not a registered user  
//                        $mdata["user_id"] = $data["user_id"] = $user_id = 0;
//                    } else {                       //he is a registered user
//                        $mdata["user_id"] = $data["user_id"] = $user_id = $query_result['user_id'];
//                        $mdata["uemail"] = $uemail = $query_result["email"];
//                        $mdata["uname"] = $uname = $query_result["first_name"];
//                        $userphone = $query_result["mobile_no"];
//                    }
//                    $mdata["user_id"] = $data["user_id"] = $user_id = 0;
                    $data["created_date"] = date("Y-m-d H:i:s");
                    $insert_data["free_sender_email"] = $sender_email;
                    $insert_data["free_sender_name"] = $sender_name;
                    $insert_data["free_sender_phone"] = $sender_phone;
//                    $insert_data["free_sended_fid"] = $fid;
//                    $insert_data["counter"] = 1;
                    $date_time = date("Y-m-d H:i:s");
//                    $insert_data["last_greeting_sent"] = $date_time;
                    $insert_data["created_date"] = $date_time;

                    for ($i = 0; $i < $count; $i++) {

                        $data["fname"] = $rec_name = $fname[$i];
                        $data["femail"] = $rec_email = $femail[$i];
                        $data["fphone"] = $rphone = $fphone[$i];

                        if (empty($query_result)) { // that means he is not a registered user  
                            $mdata["user_id"] = $data["user_id"] = $user_id = 0;
                            $mdata["uemail"] = $uemail = $sender_email;
                            $mdata["uname"] = $uname = $sender_name;
                            $mdata["fid"] = $free_id = $this->Birthday->addData("free_greetings", $data);
                            $userphone = $sender_phone;
                            $gcard_id = $this->Birthday->encryptPassword($greeting_cat_id);
                            $freeid = $this->Birthday->encryptPassword($free_id);
                            $key = $this->Birthday->encryptPassword($keyid);
                            $from = $this->Birthday->encryptPassword(0);
                            $updated_data["greeting_info_link"] = $mdata["linkclick"] = $link = BASEURL . "free_greetings_email/$gcard_id/$freeid/$key/$from";

                            $insert_data['fid_from_freegreeting'] = $free_id;
                            $this->Birthday->addData("free_greeting_sender", $insert_data);
                        } else {                       //he is a registered user and we need to increment his counter in the user table
                            $mdata["user_id"] = $data["user_id"] = $user_id = $query_result['user_id'];
                            $mdata["uemail"] = $uemail = $query_result["email"];
                            $mdata["uname"] = $uname = $query_result["first_name"];
                            $userphone = $query_result["mobile_no"];
                            $mdata["fid"] = $free_id = $this->Birthday->addData("free_greetings", $data);
                            $gcard_id = $this->Birthday->encryptPassword($greeting_cat_id);
                            $freeid = $this->Birthday->encryptPassword($free_id);
                            $key = $this->Birthday->encryptPassword($keyid);
                            $from = $this->Birthday->encryptPassword(0);
                            $updated_data["greeting_info_link"] = $mdata["linkclick"] = $link = BASEURL . "free_greetings_email/$gcard_id/$freeid/$key/$from";
                            $getsentcounter = $this->Birthday->getSelectData("sent_greeting_counter", "users", "user_id='$user_id'")->row_array();
                            $sent_counter = $getsentcounter["sent_greeting_counter"];
                            $mupdated_data["sent_greeting_counter"] = $sent_counter + 1;
                            $this->Birthday->update("users", $mupdated_data, "user_id='$user_id'");
                        }

////                       $mdata["user_id"] = $data["user_id"] = 0; // 0 means no user
//                        $data["fname"] = $rec_name = $fname[$i];
//                        $data["femail"] = $rec_email = $femail[$i];
//                        $data["fphone"] = $rphone = $fphone[$i];
//                        $mdata["fid"] = $free_id = $this->Birthday->addData("free_greetings", $data);
//
//
////                        if ($is_edit == 0) {
////                        }
//                        $mdata["uemail"] = $uemail = $sender_email;
//                        $mdata["uname"] = $uname = $sender_name;
//                        $userphone = $sender_phone;
//                        $gcard_id = $this->Birthday->encryptPassword($greeting_cat_id);
//                        $freeid = $this->Birthday->encryptPassword($free_id);
//                        $key = $this->Birthday->encryptPassword($keyid);
//                        $from = $this->Birthday->encryptPassword(0);
//                        $updated_data["greeting_info_link"] = $mdata["linkclick"] = $link = BASEURL . "free_greetings_email/$gcard_id/$freeid/$key/$from";
//                        $insert_status = FALSE;
//                        $found_email = FAlSE;
//                        $found_phone = FAlSE;
//                        $counter = 0;
//                        // find if the free greeting sender user is sending a greeting for first time or multiple times
//
//                        if ($sender_email != '') {
//                            $getsentcounter = $this->Birthday->getSelectData("counter", "free_greeting_sender", "free_sender_email='$sender_email'")->result_array();
//                            if (empty($getsentcounter)) {
////                                $counter = $getsentcounter['counter'];
//                                $insert_status = TRUE;
//                                $found_email = TRUE;
//                            } else {
//                                $counter = $getsentcounter[0]['counter'];
//                            }
//                        }
//                        if ($sender_phone != '') {
//                            $getsentcounter = $this->Birthday->getSelectData("counter", "free_greeting_sender", "free_sender_phone='$sender_phone'")->result_array();
//                            if (empty($getsentcounter)) {
////                                $counter = $getsentcounter['counter'];
//                                $insert_status = TRUE;
//                                $found_phone = TRUE;
//                            } else {
//                                $counter = $getsentcounter[0]['counter'];
//                            }
//                        }
//                        if ($insert_status) {
//                            $this->Birthday->addData("free_greeting_sender", $insert_data);
//                        } else {
////                            function update($tablename = "", $data = array(), $where = "")
//                            if ($sender_email != '' && $sender_phone != '') {
//                                $where = "free_sender_email = '$sender_email'";
//                            } else if ($sender_email != '') {
//                                $where = "free_sender_email = '$sender_email'";
//                            } else {
//                                $where = "free_sender_phone = $sender_phone";
//                            }
//
//
//                            $this->Birthday->update('free_greeting_sender', array('counter' => $counter + 1, 'last_greeting_sent' => date("Y-m-d H:i:s")), $where);
//                        }
//                        $sent_counter = $getsentcounter["sent_greeting_counter"];
//                        $mupdated_data["sent_greeting_counter"] = $sent_counter + 1;
//                        $this->Birthday->update("users", $mupdated_data, "user_id='$user_id'");
//                        $insert_data['fid_from_freegreeting'] = $free_id;
//                        $this->Birthday->addData("free_greeting_sender", $insert_data);
                        //common
                        if ($data["greeting_schedule"] == "0") {
                            $user_date = date("d F Y");
                            $schedule_Date = date('dS F Y', strtotime($user_date));
                            if ($rec_email != "") {
                                $is_mail_sent = $this->sendGreetingMail($mdata, $rec_email, $rec_name, $uname, $title, $free_id, $link);
                            } else {
                                $is_mail_sent = 0;
                            }

                            if ($rphone != "") {
                                $linkData = $this->google_url_api->shorten("$link");
                                $is_sms_sent = $this->sendGreetingSms($rec_name, $uname, $linkData->id, $rphone, $rec_email, $free_id, $link);
                            } else {
                                $is_sms_sent = 0;
                            }
                            if ($userphone != '') {
                                $show_message = "Dear $uname, \n You have just sent an awesome greeting card to $rec_name and we have notified $rec_name accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                                $this->Birthday->send_sms($userphone, $show_message);
                            }
                            if (($is_mail_sent == 1) || ($is_sms_sent == 1)) {
                                $output["email_list"] = $femail;
                                $output["name_list"] = $fname;
                                $output["mobile_list"] = $fphone;
                                $output["schedule"] = "Today " . $schedule_Date;
                                $output["indicator"] = "0";
                                $output["message"] = "Greeting card sent successfully!.";
                                $output["success"] = true;
                            } else {
                                $output["success"] = false;
                                $output["message"] = "Greeting card  can not send.Try again";
                            }
                        } else {
                            $output["email_list"] = $femail;
                            $output["mobile_list"] = $fphone;
                            $output["name_list"] = $fname;
                            $output["indicator"] = "1";
                            $updated_data["is_sent"] = 2;
                            $where = "fid='$free_id'";
                            $this->Birthday->update("free_greetings", $updated_data, $where);
                            $converted_date = date("dS F Y", strtotime($sdate));
                            $output["success"] = true;
                            $output["schedule"] = "$converted_date";
                            $output["message"] = "Thank You for using BirthdayOwl. Your greeting card will be sent on $converted_date  !";
                        }
                    }
                }
            } else {
                $data['card_id'] = $card_id;
                $data['femail'] = $femail;
                $data['fname'] = $fname;
                $data['fphone'] = $fphone;
                $data["schedule"] = $schedule;
                $this->session->set_userdata($data);
                $output["success"] = "true";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

// ====================================================================================================================================//
// -> Function     :sendGreetingSms
// -> Executed     :called this function inside send_egreeting,send_social_greeting,sendGreetingByDate,resend_greeting
// -> description  :sends sms and updates status in database
// ====================================================================================================================================//

    public function sendGreetingSms($rec_name, $uname, $click_link, $rphone, $rec_email, $free_id, $link) {
        $show_message = "Dear $rec_name, \n You have just received an awesome  Birthday Greeting card from $uname. \n To view your greeting, simply ($click_link).\n - Birthday Owl (www.birthdayowl.com)";
        $result = $this->Birthday->send_sms($rphone, $show_message);
        $updated_data["greeting_info_link"] = $link;
        if ($result == 1) {
            $updated_data["is_sent"] = 1;
            $where = "fid='$free_id'";
            $this->Birthday->update("free_greetings", $updated_data, $where);
            return 1;
        } else {
            $updated_data["is_sent"] = 0;
            $where = "fid='$free_id'";
            $this->Birthday->update("free_greetings", $updated_data, $where);
            return 0;
        }
    }

// ====================================================================================================================================//
// -> Function     :sendGreetingMail
// -> Executed     :called this function inside send_egreeting,send_social_greeting,sendGreetingByDate,resend_greeting
// -> description  :sends email and updates status in database
// ====================================================================================================================================//

    public function sendGreetingMail($mdata = array(), $rec_email, $rec_name, $uname, $title, $free_id, $link) {

        if ($title != '') {
            $gtitle = "$title";
        } else {
            $gtitle = "GREETING CARD FROM BIRTHDAYOWL";
        }
        $updated_data["greeting_info_link"] = $link;

        $view = $this->load->view("email_template/free_greetings", $mdata, true);
        $resp = send_EmailResponse($view, $rec_email, $rec_name, "greetings@birthdayowl.com", $uname, $gtitle);

        $updated_data["is_sent"] = 1;
        $where = "fid='$free_id'";
        $this->Birthday->update("free_greetings", $updated_data, $where);
        return 1;

//        try {
//            $view = $this->load->view("email_template/free_greetings", $mdata, true);
//            $resp = send_EmailResponse($view, $rec_email, $rec_name, "greetings@birthdayowl.com", $uname, $gtitle);
//            if ($resp["code"] == "success") {
//                $updated_data["is_sent"] = 1;
//                $where = "fid='$free_id'";
//                $this->Birthday->update("free_greetings", $updated_data, $where);
//                return 1;
//            } else {
//                $updated_data["is_sent"] = 0;
//                $where = "fid='$free_id'";
//                $this->Birthday->update("free_greetings", $updated_data, $where);
//                return 0;
//            }
//        } catch (Exception $e) {
//            $updated_data["is_sent"] = 0;
//            $where = "fid='$free_id'";
//            $this->Birthday->update("free_greetings", $updated_data, $where);
//            return 0;
//        }
    }

// ====================================================================================================================================//
// -> Function     :check_sent_greetings
// -> Executed     :called this function while sending greeting 
// -> description  :it check membership status and greeting count as only 10 greetings are free per user
// ====================================================================================================================================//

    public function check_sent_greetings() {
        if ($this->check_logged_in()) {
            $select = "sent_greeting_counter";
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $where = "user_id='$user_id'and membership_status='0'";
            $getsentcounter = $this->Birthday->getSelectData($select, "users", $where)->row_array();
            $output["success"] = true;
            if ($getsentcounter["sent_greeting_counter"] < 10) {
                $output["success"] = true;
            } else {
                $output["success"] = false;
                $output["resp"] = "1";
            }
        } else {
            $output["resp"] = "2";
            $output["success"] = false;
            $output["Message"] = "Sorry your session has been expired.Please login to send e-greeting";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

// ====================================================================================================================================//
// -> Function     :sendGreetingByDate
// -> Executed     :called  by cronjob 
// -> description  :As per set date it sends greeting email or failed email delievery
// ====================================================================================================================================//
    public function sendGreetingByDate() {
        $today = date("Y-m-d");
        $gift_vouchers = $this->Birthday->getJoinedData2("free_greetings.greeting_img,free_greetings.fphone,free_greetings.message,free_greetings.greeting_cat_id,free_greetings.fid,free_greetings.user_id,free_greetings.fname,free_greetings.femail,free_greetings.delivery_date,free_greetings.title,users.email,users.first_name,users.mobile_no,users.membership_status", "free_greetings", "users", "free_greetings.user_id=users.user_id ", "(free_greetings.greeting_schedule=1 and free_greetings.delivery_date='$today' and (free_greetings.is_sent='0'or free_greetings.is_sent='2')) ")->result_array();
        $this->CommonGreetingSend($gift_vouchers);
    }

    function ResendFailedGreetings() {
//Old
//        $greetings = $this->Birthday->getJoinedData2("free_greetings.greeting_img,free_greetings.fphone,free_greetings.message,free_greetings.greeting_cat_id,free_greetings.fid,free_greetings.user_id,free_greetings.fname,free_greetings.femail,free_greetings.delivery_date,free_greetings.title,users.email,users.first_name,users.mobile_no,users.membership_status", "free_greetings", "users", "free_greetings.user_id=users.user_id ", "free_greetings.is_sent='0' and (users.membership_status=1 || users.sent_greeting_counter < 10)")->result_array();
//New
        $greetings = $this->Birthday->getJoinedData2("free_greetings.greeting_img,free_greetings.fphone,free_greetings.message,free_greetings.greeting_cat_id,free_greetings.fid,free_greetings.user_id,free_greetings.fname,free_greetings.femail,free_greetings.delivery_date,free_greetings.title,users.email,users.first_name,users.mobile_no,users.membership_status", "free_greetings", "users", "free_greetings.user_id=users.user_id ", "free_greetings.is_sent='0'")->result_array();
        $this->CommonGreetingSend($greetings);
    }

    function CommonGreetingSend($greetings = array()) {
        if (count($greetings) > 0) {
            foreach ($greetings as $gifts) {
                $keyid = 'freegreetinglink';
                $cat_id = $this->Birthday->encryptPassword($gifts["greeting_cat_id"]);
                $free_id = $mdata["fid"] = $gifts["fid"];
                $freeid = $this->Birthday->encryptPassword($gifts["fid"]);
                $key = $this->Birthday->encryptPassword($keyid);
                $rec_name = $gifts["fname"];
                $rec_email = $gifts["femail"];
                $title = $gifts["title"];
                $phone = $gifts["fphone"];
                $mdata["user_id"] = $gifts["user_id"];
                $from = $this->Birthday->encryptPassword(0);
                $mdata["linkclick"] = $link = BASEURL . "free_greetings_email/$cat_id/$freeid/$key/$from";
                $mdata["uemail"] = $uemail = $gifts["email"];
                $mdata["uname"] = $uname = $gifts["first_name"];
                $userphone = $gifts["mobile_no"];
                if ($rec_email != "") {
                    $is_mail_sent = $this->sendGreetingMail($mdata, $rec_email, $rec_name, $uname, $title, $free_id, $link);
                } else {
                    $is_mail_sent = 0;
                }
                if ($phone != "") {
                    $linkData = $this->google_url_api->shorten("$link");
                    $is_sms_sent = $this->sendGreetingSms($rec_name, $uname, $linkData->id, $phone, $rec_email, $free_id, $link);
                } else {
                    $is_sms_sent = 0;
                }
                if ($userphone != '') {
                    $show_message = "You have just sent an awesome greeting card to ($rec_name) and we have notified ($rec_name) accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                    $this->Birthday->send_sms($userphone, $show_message);
                }
            }
        }
    }

// ====================================================================================================================================//
// -> Function     :resend_greeting
// -> Executed     :In order history for egreetings on click of resend greeting function get called
// -> description  :sends email
// ====================================================================================================================================//

    public function resend_greeting() {
        $output = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            if ($this->check_logged_in()) {
                $mdata["fid"] = $fid = check_post($this->input->post("fid"));
                $mdata["user_id"] = $user_id = $this->session->userdata["userdata"]["user_id"];
//                $getsentcounter = $this->Birthday->getSelectData("sent_greeting_counter,membership_status", "users", "user_id='$user_id'")->row_array();
                $getgreetingdata = $this->Birthday->getSelectData("greeting_img,message,fname,femail,fphone,uploded_img,title,type,delivery_date,greeting_cat_id", "free_greetings", "fid='$fid'")->row_array();
                $keyid = 'freegreetinglink';
//                if ($getsentcounter["membership_status"] == 1 || ($getsentcounter["sent_greeting_counter"] < 10 && $getsentcounter["membership_status"] == 0)) {
                $data["fname"] = $rec_name = $getgreetingdata["fname"];
                $data["femail"] = $rec_email = $getgreetingdata["femail"];
                $data["fphone"] = $getgreetingdata["fphone"];
                $mdata["uemail"] = $uemail = $this->session->userdata["userdata"]["email"];
                $mdata["uname"] = $uname = $this->session->userdata["userdata"]["first_name"];
                $userphone = $this->session->userdata["userdata"]["mobile_no"];
                $gcard_id = $this->Birthday->encryptPassword($getgreetingdata["greeting_cat_id"]);
                $freeid = $this->Birthday->encryptPassword($fid);
                $key = $this->Birthday->encryptPassword($keyid);
                $from = $this->Birthday->encryptPassword(0);
                $mdata["linkclick"] = $link = BASEURL . "free_greetings_email/$gcard_id/$freeid/$key/$from";
                $mupdated_data["sent_greeting_counter"] = $getsentcounter["sent_greeting_counter"] + 1;
                $this->Birthday->update("users", $mupdated_data, "user_id='$user_id'");
                $title = $getgreetingdata["title"];
                if ($rec_email != "") {
                    $is_sent = $this->sendGreetingMail($mdata, $rec_email, $rec_name, $uname, $title, $fid, $link);
                } else {
                    $is_sent = 0;
                }
                if ($data["fphone"] != "") {
                    $linkData = $this->google_url_api->shorten("$link");
                    $is_delivered = $this->sendGreetingSms($rec_name, $uname, $linkData->id, $data["fphone"], $rec_email, $fid, $link);
                } else {
                    $is_delivered = 0;
                }
                if ($userphone != '') {
                    $show_message = "Dear $uname, \n You have just sent an awesome greeting card to $rec_name and we have notified $rec_name accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                    $this->Birthday->send_sms($userphone, $show_message);
                }
                if ($is_sent == 1 || $is_delivered == 1) {
                    $output["message"] = "Greeting card resent successfully!.";
                    $output["success"] = true;
                } else {
                    $output["success"] = false;
                    $output["message"] = "Greeting card  can not send.Try again";
                }
//                } else if ($getsentcounter["sent_greeting_counter"] > 10 && $getsentcounter["membership_status"] == 0) {
//                    $output["success"] = false;
//                    $output["message"] = "0"; //Not active
//                } else if ($getsentcounter["sent_greeting_counter"] > 10 && $getsentcounter["membership_status"] == 2) {
//                    $output["success"] = false;
//                    $output["message"] = "2"; //expired
//                }
            } else {
                $output["success"] = false;
                $output["message"] = "3";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

// ====================================================================================================================================//
// -> Function     :send_social_greeting
// -> Executed     :If user is sending email using gmail,facebook this function get called
// -> description  :sends email
// ====================================================================================================================================//


    public function send_social_greeting() {
        $output = array();
        $card_id = $this->session->userdata["card_id"];
        $keyid = 'freegreetinglink';
        $type = $this->session->userdata["type"];
        $data["greeting_cat_id"] = $greeting_cat_id = 0;
        $femail = $this->session->userdata["femail"];
        $fphone = $this->session->userdata["fphone"];
        $fname = $this->session->userdata["fname"];
        $count = count($fname);
        $data["uploded_img"] = $this->session->userdata["uploded_img"];
        $data["type"] = $type;
        $data["greeting_img"] = $this->session->userdata["greeting_img"];
        $data["delivery_date"] = $sdate = $this->session->userdata["delivery_date"];
        $data["greeting_schedule"] = $this->session->userdata["greeting_schedule"];
        $data["title"] = $title = $this->session->userdata["title"];
        $data["message"] = $this->session->userdata["message"];
        $mdata["user_id"] = $data["user_id"] = $user_id = $this->session->userdata["userdata"]["user_id"];
        $data["created_date"] = date("Y-m-d H:i:s");
        if ($count > 0) {
            for ($i = 0; $i < $count; $i++) {
                $data["fname"] = $rec_name = $fname[$i];
                $data["femail"] = $rec_email = $femail[$i];
                $data["fphone"] = $fphone[$i];
                $mdata["fid"] = $free_id = $this->Birthday->addData("free_greetings", $data);
                $mdata["uemail"] = $uemail = $this->session->userdata["userdata"]["email"];
                $mdata["uname"] = $uname = $this->session->userdata["userdata"]["first_name"];
                $userphone = $this->session->userdata["userdata"]["mobile_no"];
                $cat_id = $this->Birthday->encryptPassword($greeting_cat_id);
                $freeid = $this->Birthday->encryptPassword($free_id);
                $key = $this->Birthday->encryptPassword($keyid);
                $where = "user_id='$user_id'";
                $getsentcounter = $this->Birthday->getSelectData("sent_greeting_counter", "users", $where)->row_array();
                $sent_counter = $getsentcounter["sent_greeting_counter"];
                $mupdated_data["sent_greeting_counter"] = $sent_counter + 1;
                $this->Birthday->update("users", $mupdated_data, $where);
                $from = $this->Birthday->encryptPassword(0);
                $updated_data["greeting_info_link"] = $mdata["linkclick"] = $link = BASEURL . "free_greetings_email/$cat_id/$freeid/$key/$from";
                if ($data["greeting_schedule"] == "0") {
                    $user_date = date("d F Y");
                    $schedule_Date = date('dS F Y', strtotime($user_date));
                    if ($rec_email != "") {
                        $is_mail_sent = $this->sendGreetingMail($mdata, $rec_email, $rec_name, $uname, $title, $free_id, $link);
                    } else {
                        $is_mail_sent = 0;
                    }

                    if ($data["fphone"] != "") {
                        $linkData = $this->google_url_api->shorten("$link");
                        $is_sms_sent = $this->sendGreetingSms($rec_name, $uname, $linkData->id, $data["fphone"], $rec_email, $free_id, $link);
                    } else {
                        $is_sms_sent = 0;
                    }
                    if ($userphone != '') {
                        $show_message = "Dear $uname, \n You have just sent an awesome greeting card to $rec_name and we have notified $rec_name accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                        $this->Birthday->send_sms($userphone, $show_message);
                    }
                    if ($is_mail_sent == 1 || $is_sms_sent == 1) {
                        $this->session->set_flashdata('email_list', implode(",", $femail));
                        $this->session->set_flashdata('name_list', implode(",", $fname));
                        $this->session->set_flashdata('mobile_list', implode(",", $fphone));
                        $this->session->set_flashdata('schedule', "Today " . $schedule_Date);
                        $this->session->set_flashdata('indicator', "0");
                        $output["success"] = true;
                        $output["message"] = "Greeting card sent successfully!.";
                    } else {
                        $output["success"] = false;
                        $this->session->set_flashdata('fmsg', "Greeting card  can not send.Try again!");
                    }
                } else {
                    $updated_data["is_sent"] = 2;
                    $where = "fid='$free_id'";
                    $this->Birthday->update("free_greetings", $updated_data, $where);
                    $converted_date = date("d F,Y", strtotime($sdate));
                    $output["success"] = true;
                    $this->session->set_flashdata('email_list', implode(",", $femail));
                    $this->session->set_flashdata('name_list', implode(",", $fname));
                    $this->session->set_flashdata('mobile_list', implode(",", $fphone));
                    $this->session->set_flashdata('schedule', "$converted_date");
                    $this->session->set_flashdata('indicator', "1");
                    $output["message"] = "Thank You for using BirthdayOwl. Your greeting card will be sent on $converted_date  !";
                }
            }
        } else {
            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login to send e-greeting");
        }
        redirect(BASEURL . "insidegreeting/$card_id/0/0");
    }

//End-egreeting
    public function set_sessionData() { //No use
        $output = array();
        if (count($_POST) > 0) {
            $data['card_id'] = check_post($this->input->post("card_id"));
            $type = check_post($this->input->post("gettype"));
            $greeting_cat_id = check_post($this->input->post("cat_id"));
            $data['femail'] = json_decode(stripslashes($_POST["friend_email"]));
            $data['fname'] = json_decode(stripslashes($_POST["friend_name"]));
            $data['fphone'] = json_decode(stripslashes($_POST["fphone"]));
            $uploaded_img = check_post($this->input->post("uploaded_finalimg"));
            $uploadedpic = str_replace('public/imageVideo', '', $uploaded_img);
            if ($uploadedpic != "") {
                $data["uploded_img"] = $uploadedpic;
            } else {
                $data["uploded_img"] = check_post($this->input->post("imgcaptureuploaded"));
            }
            $data["type"] = $type;
            $data["greeting_img"] = check_post($this->input->post("greeting_img"));
//            $data["title"] = check_post($this->input->post("title"));
            $title = check_post($this->input->post("title"));
            $data["title"] = preg_replace('/[^\da-z ]/i', '', $title);
            $data["schedule"] = $schedule = check_post($this->input->post("schedule"));
            if ($schedule != "") {
                $date = str_replace('/', '-', $schedule);
                $sdate = date("Y-m-d", strtotime($date));
                $data["delivery_date"] = $sdate;
                $data["greeting_schedule"] = "1";
            } else {
                $data["delivery_date"] = date("Y-m-d");
                $data["greeting_schedule"] = "0";
            }
//            $data["message"] = check_post($this->input->post("message"));
            $message = check_post($this->input->post("message"));
            $data["message"] = preg_replace('/[^\da-z ]/i', '', $message);
            $data["greeting_cat_id"] = $greeting_cat_id;
            $this->session->set_userdata($data);
            $output["success"] = "true";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function send_membership_renewalEmail() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $mdata["email"] = $email = check_post($this->input->post("email"));
            $mdata["uname"] = $name = check_post($this->input->post("name"));
            $view = $this->load->view("email_template/renew_membership", $mdata, true);
            send_Email($view, $email, $name, "greetings@birthdayowl.com", "birthdayowl.com", "Membership Renewal");
            $output["success"] = true;
            $output["message"] = "Email sent successfully"; //If user name is not in the database
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function paid_membership() {
        $data = array();
        $data["plans"] = $plans = $this->Birthday->getAllData("membership_plans")->result_array();
        if ($this->check_logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $data["user_id"] = $user_id;
            $data["logged_in"] = "1";
        } else {
            $data["logged_in"] = "0";
            $data["hash"] = "";
            $data["type"] = 0;
        }
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=18")->row_array();
        $data["seo"] = $title;
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $this->load->view("FREEGREETING/membership_fee", $data);
    }

    public function check_membership_plan() {
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $plan_purchase_type = check_post($this->input->post("plan_purchase_type"));
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $where = "plan_purchase_type=$plan_purchase_type and user_id=$user_id";
            $check_plan = $this->Birthday->getSelectData("expiry_date", "greeting_membership", $where)->row_array();
            if (count($check_plan) > 0) {
                $date = str_replace('-', '/', $check_plan["expiry_date"]);
                $sdate = date("d/m/Y", strtotime($date));
                $output["expiry_date"] = $sdate;
                $output["success"] = true;
            } else {
                $output["success"] = false;
            }

            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function getTestimonials() {
        $voucher_name = $this->Birthday->getAllData("testimonial")->result_array();
        return $voucher_name;
    }

    public function membership_cart($id) {
        if ($this->check_logged_in()) {
            $data["user_id"] = $data["udf2"] = $user_id = $this->session->userdata["userdata"]["user_id"];
            $where = "user_id='$user_id'";
            $user_info = $this->Birthday->getUserData("users", $where)->row_array();
            $data["membership_status"] = $user_info["membership_status"];
            $data["user_email"] = $user_info["email"];
            $data["user_first_name"] = $user_info["first_name"];
            $data["phone"] = $user_info["mobile_no"];
            $data["product_info"] = "BirthdayOwl egreeting";
            $data["active_id"] = $id;
            $data["active_amount"] = 399;
            $data["plans"] = $this->Birthday->getAllData("membership_plans")->result_array();
            $data["MERCHANT_KEY"] = MERCHANT_KEY;
            $SALT = PSALT;
            $PAYU_BASE_URL = PAYU_BASE_URL;
            $data["action"] = "";
            $data["hash"] = "";
            $data["amount"] = "";
            $data["firstname"] = "";
            $data["email"] = "";
            $data["productinfo"] = "";
            $data["phone"] = "";
            $data["surl"] = "";
            $data["furl"] = "";
            $posted = array();

            if (!empty($_POST)) {
                foreach ($_POST as $key => $value) {
                    $posted[$key] = $value;
                    $data[$key] = $value;
                }
            }
            $data["formError"] = 0;

            if (empty($posted['txnid'])) {
// Generate random transaction id
                $data["txnid"] = $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            } else {
                $data["txnid"] = $txnid = $posted['txnid'];
            }

// Hash Sequence 
            $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
            if (empty($posted['hash']) && sizeof($posted) > 0) {
                if (empty($posted['key']) || empty($posted['txnid']) || empty($posted['amount']) || empty($posted['firstname']) || empty($posted['email']) || empty($posted['productinfo']) || empty($posted['surl']) || empty($posted['furl']) || empty($posted['service_provider'])) {

                    $data["formError"] = 1;
                } else {

                    $hashVarsSeq = explode('|', $hashSequence);
                    $hash_string = '';
                    foreach ($hashVarsSeq as $hash_var) {
                        $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                        $hash_string .= '|';
                    }
                    $hash_string .= $SALT;

                    $data["hash"] = $hash = strtolower(hash('sha512', $hash_string));
                    $data["action"] = $PAYU_BASE_URL . '/_payment';
                }
            } elseif (!empty($posted['hash'])) {
                $data["hash"] = $posted['hash'];
                $data["action"] = $PAYU_BASE_URL . '/_payment';
            }
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=18")->row_array();
            $data["seo"] = $title;
            $this->load->view("FREEGREETING/MembershipCart", $data);
        } else {
            redirect(BASEURL . "paid_membership");
        }
    }

    public function make_payment($id) {
        $data = array();
//        if (count($_POST) > 0) {
        $user_id = $this->session->userdata["userdata"]["user_id"];
        $data["user_id"] = $user_id;
        if ($id == 1) {
            if (SERVER_TYPE == 1) {
                $data["plan_amount"] = "399";
            } else {
                $data["plan_amount"] = "1";
            }
        } else {
//            $data["plan_amount"] = "599";
            $data["plan_amount"] = "2";
        }
        $where = "user_id='$user_id'";
        $user_info = $this->Birthday->getSelectData("email,first_name,mobile_no", "users", $where)->row_array();
        $data["user_email"] = $user_info["email"];
        $data["user_first_name"] = $user_info["first_name"];
        $data["phone"] = $user_info["mobile_no"];
        $data["product_info"] = "BirthdayOwl egreeting";
        $data["MERCHANT_KEY"] = MERCHANT_KEY;
        $SALT = PSALT;
        $PAYU_BASE_URL = PAYU_BASE_URL;
        $data["action"] = "";
        $data["hash"] = "";
        $data["amount"] = "";
        $data["firstname"] = "";
        $data["email"] = "";
        $data["productinfo"] = "";
        $data["phone"] = "";
        $data["surl"] = "";
        $data["furl"] = "";
        $posted = array();
        if (!empty($_POST)) {
            foreach ($_POST as $key => $value) {
                $posted[$key] = $value;
                $data[$key] = $value;
            }
        }

        $data["formError"] = 0;
        if (empty($posted['txnid'])) {
            $data["txnid"] = $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
            $data["txnid"] = $txnid = $posted['txnid'];
        }

// Hash Sequence
        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if (empty($posted['hash']) && sizeof($posted) > 0) {

            if (empty($posted['key']) || empty($posted['txnid']) || empty($posted['amount']) || empty($posted['firstname']) || empty($posted['email']) || empty($posted['productinfo']) || empty($posted['surl']) || empty($posted['furl']) || empty($posted['service_provider'])) {

                $data["formError"] = 1;
            } else {
                $hashVarsSeq = explode('|', $hashSequence);
                $hash_string = '';
                foreach ($hashVarsSeq as $hash_var) {
                    $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                    $hash_string .= '|';
                }
                $hash_string .= $SALT;

                $data["hash"] = $hash = strtolower(hash('sha512', $hash_string));
                $data["action"] = $PAYU_BASE_URL . '/_payment';
            }
        } elseif (!empty($posted['hash'])) {
            $data["hash"] = $posted['hash'];
            $data["action"] = $PAYU_BASE_URL . '/_payment';
        }
    }

    public function success() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $data["payment_mode"] = $status = $_POST["mode"];
            $data["transaction_status"] = $status = $_POST["status"];
            $data["bill_to_name"] = $firstname = $_POST["firstname"];
            $data["total_amount"] = $amount = $_POST["amount"];
            $data["transaction_id"] = $txnid = $_POST["txnid"];
            $posted_hash = $_POST["hash"];
            $key = $_POST["key"];
            $productinfo = $_POST["productinfo"];
            $data["bill_to_email"] = $email = $_POST["email"];
            $data["purchased_date"] = $_POST["addedon"];
            if ($amount == "399") {
                $data["plan_purchase_type"] = "1";
                $cdate = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($data["purchased_date"])));
                $time = strtotime($cdate);
                $time = $time - (60);
                $data["expiry_date"] = date("Y-m-d H:i:s", $time);
            } else {
                $data["plan_purchase_type"] = "2";
                $cdate = date('Y-m-d H:i:s', strtotime('+2 year', strtotime($data["purchased_date"])));
                $time = strtotime($cdate);
                $time = $time - (60);
                $data["expiry_date"] = date("Y-m-d H:i:s", $time);
            }
//live
            $salt = PSALT;
            $data["user_id"] = $user_id = $udf2 = $_POST["udf2"];
            $udf1 = $_POST["udf1"];
            If (isset($_POST["additionalCharges"])) {
                $additionalCharges = $_POST["additionalCharges"];
                $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

// $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
            } else {
                $retHashSeq = $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

//$retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
            }
            $hash = hash("sha512", $retHashSeq);
            if ($hash != $posted_hash) {
                $data["trerror"] = 0;
            } else {

                $where = "user_id='$user_id' and transaction_id='$txnid'";
                $transaction_data = $this->Birthday->getSelectData("transaction_status", "greeting_membership", $where)->row_array();
                $transaction_status = $transaction_data["transaction_status"];
                if (count($transaction_data) > 0) {
                    if ($transaction_status != "success") {
                        $updated_data["transaction_status"] = $status;
                        $where = "user_id='$user_id' and transaction_id='$txnid'";
                        $this->Birthday->update("greeting_membership", $updated_data, $where);
                    }
                } else {

                    $data["order_id"] = time() . mt_rand() . $user_id;
                    $added_id = $this->Birthday->addData("greeting_membership", $data);
                    $updated_data["membership_status"] = 1;
                    $where = "user_id='$user_id'";
                    $this->Birthday->update("users", $updated_data, $where);
                    $data["trerror"] = 1;

                    $where = "membership_id='$added_id' and user_id='$user_id'";
                    $membership_data = $this->Birthday->getSelectData("order_id,bill_to_name,plan_purchase_type,payment_mode,purchased_date,expiry_date", "greeting_membership", $where)->row_array();
//send email after purchasing membership
                    $userinfo = $this->Birthday->getSelectData("email,first_name,mobile_no", "users", "user_id=$user_id")->row_array();
                    $uname = $userinfo["first_name"];
                    $uemail = $userinfo["email"];
                    $userphone = $userinfo["mobile_no"];
                    $mdata["order_id"] = $membership_data["order_id"];
                    $mdata["bill_name"] = $membership_data["bill_to_name"];
                    $mdata["plan_purchase_type"] = $membership_data["plan_purchase_type"];
                    $mdata["payment_mode"] = $membership_data["payment_mode"];
                    $mdata["purchased_date"] = $membership_data["purchased_date"];
                    $mdata["expiry_date"] = $membership_data["expiry_date"];
                    $mdata["name"] = $uname;
                    $mdata["email"] = $uemail;
                    $mdata["amount"] = $amount;
                    $view = $this->load->view("email_template/membership_plan_success", $mdata, true);
                    send_Email($view, $uemail, $uname, "greetings@birthdayowl.com", "birthdayowl.com", "Membership Received (Rs.$amount)");
                    if ($userphone != '') {
                        $show_message = "Dear $uname, \n Thank you for registering with Birthday Owl.\n We confirm receipt of your membership fee of (INR $amount) and have sent you an email to your registered email id.\n - Birthday Owl (www.birthdayowl.com)";
                        $this->Birthday->send_sms($userphone, $show_message);
                    }
                }

                redirect(BASEURL . "Home_web/membership_success/" . $this->Birthday->encryptPassword($added_id));
            }
        } else {
            $data["trerror"] = 0;
            $this->load->view("FREEGREETING/membership_failure", $data);
        }
    }

    public function membership_success($membership_id) {
        if ($this->check_logged_in()) {
            $mid = $this->Birthday->decryptPassword($membership_id);
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $where = "membership_id='$mid' and user_id='$user_id'";
            $membership_data = $this->Birthday->getSelectData("plan_purchase_type,transaction_status,transaction_id,total_amount", "greeting_membership", $where)->row_array();
            $plan_purchase_type = $membership_data["plan_purchase_type"];
            $where = "id=$plan_purchase_type";
            $plandata = $this->Birthday->getSelectData("duration", "membership_plans", $where)->row_array();
            $data["duration"] = $plandata["duration"];
            $data["transaction_status"] = $membership_data["transaction_status"];
            $data["transaction_id"] = $membership_data["transaction_id"];
            $data["total_amount"] = $membership_data["total_amount"];
            $data["trerror"] = 1;
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=18")->row_array();
            $data["seo"] = $title;
            $this->load->view("FREEGREETING/membership_success", $data);
        } else {
            $this->index();
        }
    }

    public function error() {
        $data["user_id"] = $user_id = $udf2 = $_POST["udf2"];
        $udf1 = $_POST["udf1"];
        $data["transaction_status"] = $status = $_POST["status"];
        $firstname = $_POST["firstname"];
        $amount = $_POST["amount"];
        $txnid = $_POST["txnid"];
        $posted_hash = $_POST["hash"];
        $key = $_POST["key"];
        $productinfo = $_POST["productinfo"];
        $email = $_POST["email"];
        $salt = PSALT;
        If (isset($_POST["additionalCharges"])) {
            $additionalCharges = $_POST["additionalCharges"];
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

//  $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        } else {
            $retHashSeq = $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

//   $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        }
        $hash = hash("sha512", $retHashSeq);

        if ($hash != $posted_hash) {
            $data["trerror"] = 2;
        } else {
            $data["trerror"] = 1;
        }
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=18")->row_array();
        $data["seo"] = $title;
        $this->load->view("FREEGREETING/membership_failure", $data);
    }

    public function upload_img() {
        $filename = "";
        $upload = FALSE;
        if (!empty($_FILES)) {
            $tempFile = trim($_FILES['userfile']['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
            $filename = str_replace(" ", "_", $filename);
            $targetPath = 'public/profile_pic/';
            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = false;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES["userfile"]["error"];
                } else {
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $upload = TRUE;
                }
            } else {
                $output["filename"] = $filename;
                $output["success"] = true;
                $upload = TRUE;
            }
            if ($filename != '') {
                $updated_data["user_profile_pic"] = $filename;
                $user_id = $this->session->userdata["userdata"]["user_id"];
                $where = "user_id='$user_id'";
                $this->Birthday->update("users", $updated_data, $where);
                $user_info = $this->Birthday->getUserData("users", $where)->row_array();
                $data["userdata"] = $user_info;
                $data["userdata"]["flower_data"] = $this->session->userdata["userdata"]["flower_data"];
                $data["userdata"]["voucher_data"] = $this->session->userdata["userdata"]["voucher_data"];
                $this->session->set_userdata($data);
            }
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function update_remove_pic() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $updated_data["user_profile_pic"] = check_post($this->input->post("user_profile_pic"));
            $where = "user_id='$user_id'";
            $updated = $this->Birthday->update("users", $updated_data, $where);
            if ($updated == 1) {
                $output["success"] = true;
                $output["message"] = "Profile pic is removed successfully";
                $user_info = $this->Birthday->getUserData("users", $where)->row_array();
                $data["userdata"] = $user_info;
                $data["userdata"]["flower_data"] = $this->session->userdata["userdata"]["flower_data"];
                $data["userdata"]["voucher_data"] = $this->session->userdata["userdata"]["voucher_data"];
                $this->session->set_userdata($data);
            } else {
                $output["success"] = false;
                $output["message"] = "Fail to update";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function addOrdinalNumberSuffix($num) {

        if (!in_array(($num % 100), array(11, 12, 13))) {
            switch ($num % 10) {
                case 1: return $num . 'st';
                case 2: return $num . 'nd';
                case 3: return $num . 'rd';
            }
        }
        return $num . 'th';
    }

    public function get_random_product() {

        $data["product_voucher"] = getAllVouchers();
        $vouchers_array = $data["product_voucher"];
        $numbers = array_column($vouchers_array, 'voucher_pro_id');
        $min = min($numbers);
        $max = max($numbers);
        $id = rand($min, $max);
        $reindexed_array = array_values($vouchers_array);
        $key = array_search($id, array_column($reindexed_array, 'voucher_pro_id')); // $key = 2;
        $product_data = $reindexed_array[$key];
        $output["product"] = $product_data;
        $where = "greeting_type='1'";
        $card_data = $this->Birthday->getUserData("greeting_card", $where)->result_array();
        $gnumbers = array_column($card_data, 'card_id');
        $gid = $gnumbers[array_rand($gnumbers)];
        $where1 = "card_id='$gid'";
        $output["gcards"] = $this->Birthday->getUserData("greeting_card", $where1)->row_array();
        $output["success"] = true;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function get_random_product_new() {

        $flower_cake_array = $this->Birthday->simple_query("Select * from product_new;")->result_array();
        $numbers = array_column($flower_cake_array, 'pro_id');
        $min = min($numbers);
        $max = max($numbers);
        $id = rand($min, $max);
        $reindexed_array = array_values($flower_cake_array);
        $key = array_search($id, array_column($reindexed_array, 'pro_id')); // $key = 2;
        $product_data = $reindexed_array[$key];
        $output["product"] = $product_data;
        $where = "greeting_type='1'";
        $card_data = $this->Birthday->getUserData("greeting_card", $where)->result_array();
        $gnumbers = array_column($card_data, 'card_id');
        $gid = $gnumbers[array_rand($gnumbers)];
        $where1 = "card_id='$gid'";
        $output["gcards"] = $this->Birthday->getUserData("greeting_card", $where1)->row_array();
        $output["success"] = true;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function google_sync() {
//        require_once '../BirthdayOwl/vendor/autoload.php';
        require_once 'vendor/autoload.php';
        if (!isset($_SESSION)) {
            session_start();
        }
        $client_id = '508866260015-qcgkebnipna3iop24tdc95nge7diu5db.apps.googleusercontent.com';
        $client_secret = 'hgMgIAHIgPEcQr6nYMoXXpp';
        $redirect_uri = BASEURL . 'addReminder/0';
        $client = new Google_Client();
        $client->setApplicationName("Client_Library_Examples");
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        $client->setAccessType('offline');   // Gets us our refreshtoken
        $client->setScopes(array('https://www.googleapis.com/auth/calendar.readonly'));
        if (isset($_SESSION['token'])) {
            $client->setAccessToken($_SESSION['token']);
            $service = new Google_Service_Calendar($client);
            $calendarList = $service->calendarList->listCalendarList();

            while (true) {
                foreach ($calendarList->getItems() as $calendarListEntry) {
// get events 
                    $events = $service->events->listEvents($calendarListEntry->id);

                    if ($calendarListEntry->getSummary() == "Contacts") {
                        $bday = array();

                        foreach ($events->getItems() as $event) {
                            $id = $event->getId();
                            $sid = str_replace('_BIRTHDAY_self', '', $id);
                            $plusid = substr($sid, 14);
                            if ($plusid != '') {
                                $birthday = $event->getStart()->date;
                                $name = str_replace("'s birthday", "", $event->getSummary());
                                $bday_data["user_id"] = $user_id = $this->session->userdata["userdata"]["user_id"];
                                $bday_data["first_name"] = $name;
                                $bday_data["last_name"] = '';
//                                $bday_data["birth_date"] = date('d/m/0000', strtotime($birthday));
                                $bday_data["birth_date"] = date('0000-m-d', strtotime($birthday));
                                $birth_date = explode('-', $bday_data["birth_date"]);
                                $bday_data["bmonth"] = $bmonth = $birth_date[1];
                                $bday_data["bdate"] = $bday = $birth_date[0];
                                $bday_data["byear"] = $birth_date[2];
                                $bday_data["relation_id"] = 1;
                                $bday_data["mobile_no"] = '';
                                $bday_data["email"] = '';
                                $bday_data["plus_id"] = $plusid;
                                $bday_data["gender"] = '1';
                                $bday_data["priority"] = '0';
                                $bday_data["rem_image_name"] = '';
                                $bday_data["reminder_created_date"] = date("Y-m-d H:i:s");
                                $zodiac_name = $this->Birthday->getUserData("mapzodiac", "(fdate <='$bday' and fmonth='$bmonth') or (tdate>='$bday' and tmonth ='$bmonth')")->row_array();
                                $bday_data["zodiac_id"] = $zodiac_name['zodiac_id'];
                                $where = "user_id='$user_id' and plus_id='$plusid'";
                                $check_records = $this->Birthday->getUserData("bday_reminder", $where)->num_rows();


                                if ($check_records == 0) {
                                    $this->Birthday->addData("bday_reminder", $bday_data);
                                    $rdata = array(
                                        "syncmsg" => "Google sync is done Successfully!"
                                    );
                                    $this->session->set_userdata($rdata);
// $this->session->set_flashdata('smsg', 'Google sync is done Successfully!');
                                } else {
                                    $rdata = array(
                                        "syncmsg" => "Google sync is done Successfully!"
                                    );
                                    $this->session->set_userdata($rdata);
                                }

//                            $bday_data["email"] = check_post($this->input->post("email"));
//                            $bday_data["gender"] = check_post($this->input->post("gender"));
//                            $bday_data["priority"] = check_post($this->input->post("priority"));
//                           
//                           
//                            $zodiac_sign = $this->input->post("zodiac_sign");
//                            $where = "zodiac_sign='$zodiac_sign'";
//                            $zodic_id = $this->Birthday->getUserData("mapzodiac", $where)->row();
//                            $z_id = $zodic_id->zodiac_id;
//                       
//                                print_r($bday_data);
//                                exit;
                            }
                        }
                    }
                }

                $pageToken = $calendarList->getNextPageToken();
                if ($pageToken) {
                    $optParams = array('pageToken' => $pageToken);
                    $calendarList = $service->calendarList->listCalendarList($optParams);
                } else {
                    break;
                }
            }
        }

        redirect(BASEURL . 'addReminder/0');
    }

    public function logged_in() {
        if ($this->session->userdata('userdata') != NULL) {
            return true;
        } else {
            redirect(WEB_HOME);
        }
    }

    public function check_logged_in() {
        if ($this->session->userdata('userdata') != NULL && $this->session->userdata['userdata']['is_guest'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function check_guest_logged_in() {
        if ($this->session->userdata('userdata') != NULL && $this->session->userdata['userdata']['is_guest'] == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function check_google_logged_in() {
        if ($this->session->userdata('userdata') != NULL && $this->session->has_userdata('is_user_google_login')) {
            return true;
        } else {
            return false;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(WEB_HOME);
    }

    public function logout_with_google() {
        $this->session->sess_destroy();
        $data["success"] = true;
        $data["message"] = "Logout_Successfully";
        $this->output->set_output(json_encode($data));
    }

    public function registration_success() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $updated_data["first_name"] = $user_data["first_name"] = check_post($this->input->post("first_name"));
            $signup_type = check_post($this->input->post("signup_type"));
            $updated_data["username"] = $user_data["username"] = "";
            $updated_data["fb_id"] = $user_data["fb_id"] = "";
            $updated_data["g_id"] = $user_data["g_id"] = "";
            $updated_data["login_with"] = $user_data["login_with"] = "1";
            $updated_data["user_profile_pic"] = $user_data["user_profile_pic"] = "";
            $updated_data["reset_time"] = $user_data["reset_time"] = date("Y-m-d H:i:s");
            $updated_data["reset_link"] = $user_data["reset_link"] = 0;
            $updated_data["reset_activate"] = $user_data["reset_activate"] = 0;
            $updated_data["sent_greeting_counter"] = $user_data["sent_greeting_counter"] = 0;
            $updated_data["membership_status"] = $user_data["membership_status"] = 0;

            $password = check_post($this->input->post("password"));
            $updated_data["country_id"] = $user_data["country_id"] = $country_id = check_post($this->input->post("country_id"));
            $updated_data["gender"] = $user_data["gender"] = check_post($this->input->post("gender"));
            $updated_data["email"] = $user_data["email"] = $email = check_post($this->input->post("email"));
            $bmonth = check_post($this->input->post("bmonth"));
            $bday = check_post($this->input->post("bdate"));
            $byear = check_post($this->input->post("byear"));

            $updated_data["birth_date"] = $user_data["birth_date"] = $bday . "/" . $bmonth . "/" . $byear;
            //Check Email in DB whether it is exist or not
            $where = "email='$email' and is_guest=0 and mobile_verify=1";
            $check_user = $this->Birthday->getSelectData("email", "users", $where)->num_rows();
            if ($check_user > 0) {
                $output["success"] = false;
                $output["message"] = "Email-id already exists.";
                echo json_encode($output);
                exit;
            }
            $mobile_no = check_post($this->input->post("mobile_no"));
            $hashkey = " ";
            $updated_data["mobile_no"] = $user_data["mobile_no"] = $mobile_no;
//Check for country id is INDIA or not
////////////////////////////////////////////////////check the blacklisted email and phone number////////////////////////////////////////////////////////
            $this->load->library('BlackListMobileAndEmail');
            $blackListMobileAndEmail = new BlackListMobileAndEmail();
            $blackListMobileAndEmail->checkBlacklistEmail(trim($email));
            $blackListMobileAndEmail->checkBlaclistMobile(trim($mobile_no));
//                var_dump($blackListMobileAndEmail->isBlackList());
//                exit;
            if ($blackListMobileAndEmail->isBlackList()) {
                $output["success"] = FALSE;
                $output["message"] = "Your Id has been blocked";
                $myoutput = json_encode($output);
                echo $myoutput;
                exit;
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////            
            if ($country_id == 102) {
                //Check Phone in DB whether it is exist or not
                $updated_data["mobile_no"] = $user_data["mobile_no"] = $mobile_no;
                if ($mobile_no != "") {
//                    $where = "mobile_no='$mobile_no' and is_guest=0 and mobile_verify=1";
//                    $where = "email='$email' and mobile_no='$mobile_no' and is_guest = 0 and is_google_login = 0 and mobile_verify=1";
                    $where = "mobile_no='$mobile_no' and is_guest = 0 and mobile_verify = 1";
                    $check_mobile_no = $this->Birthday->getSelectData("mobile_no", "users", $where)->num_rows();
                    if ($check_mobile_no > 0) {
                        $output["success"] = false;
                        $output["message"] = "Mobile Number already exists.";
                        echo json_encode($output);
                        exit;
                    }
                }
            }

            if (($bmonth != '') && ($bday != '')) {
                $zodiac_info = $this->Birthday->getUserData("mapzodiac", "(fdate <='$bday' and fmonth='$bmonth') or (tdate>='$bday' and tmonth ='$bmonth')")->row_array();
                $updated_data["zodiac_id"] = $user_data["zodiac_id"] = $zodiac_info["zodiac_id"];
                $updated_data["bmonth"] = $user_data["bmonth"] = $bmonth;
                $updated_data["bdate"] = $user_data["bdate"] = $bday;
                $updated_data["byear"] = $user_data["byear"] = $byear;
            } else {
                $updated_data["zodiac_id"] = $user_data["zodiac_id"] = '';
                $updated_data["bmonth"] = $user_data["bmonth"] = '';
                $updated_data["bdate"] = $user_data["bdate"] = '';
                $updated_data["byear"] = $user_data["byear"] = '';
            }
            $updated_data["password"] = $user_data["password"] = $this->Birthday->encryptPassword($password);
            $updated_data["bmonth"] = $user_data["bmonth"] = $bmonth;
            $updated_data["bdate"] = $user_data["bdate"] = $bday;
            $updated_data["byear"] = $user_data["byear"] = $byear;
            $updated_data["created_date"] = $user_data["created_date"] = date("Y-m-d H:i:s");
            $new_data["email_id"] = $uemail = $user_data["email"];
            $new_data["first_name"] = $firstname = $user_data["first_name"];
// $new_data["last_name"] = "";
            $new_data["nimage_name"] = "";
            $updated_data["u_last_login"] = $user_data["u_last_login"] = date("Y-m-d H:i:s");
            if ($country_id != 102) {
                $updated_data["mobile_verify"] = $user_data["mobile_verify"] = 1;
            }

            //Check whether email id is exist in guest user table or not
//            $where = "guest_email_id='$email'";
//            $check_user_guest = $this->Birthday->getSelectData("*", "guest_user", $where)->result_array();
            // to check if the gurst exist already
            // need to oheck also if the google exist already

            $this->load->library("CheckAsGuest");
            $check_guest_exist = new CheckAsGuest();
            $check_guest_exist->setGuestEmail($email);
            $check_guest_exist->setMobileNo($mobile_no);
            $check_guest_exist->setCountry_code($country_id);
            $is_guest_exist = $check_guest_exist->runner();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->load->library("CheckAsGoogle");
            $check_google_user = new CheckAsGoogle();
            $check_google_user->setGoogleEmailId($email);
            $check_google_user->setCountry_code($country_id);
            $is_google_exist = $check_google_user->runner();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////the block check any one place either google or guest exit//////////////////////////////
            $getUserId = '';
            if ($is_google_exist || $is_guest_exist) {
                $this->load->library("OtpSenderUpdater");
                $otp_sender = new OtpSenderUpdater();

                switch (true) {
                    case ($is_guest_exist):
                        $getUserId = $check_guest_exist->getUserId();
                        $check_guest_exist->update($updated_data);
                        $sameMoble = $check_guest_exist->checkMobileAreSame();
                        $checkCountryCode = $check_guest_exist->CheckCountryCode();

                        if ($sameMoble || $checkCountryCode) {
//                        $output['success'] = TRUE;
//                        $output['message'] = "Signed Up Successfully";
//                        $output["user_id"] = $getUserId;
                            $check_guest_exist->resetGoogleGuest();
                            $user_info = $check_guest_exist->getUserInfo();
//                            $output['trigger_otp'] = 0;
                            $output['userdata'] = $user_info;
                            $output["is_converted"] = 1;
                            $output["userdata"]['flower_data'] = array();
                            $output["userdata"]['voucher_data'] = array();
                            $output["success"] = true;
//
                            $data["email"] = $email = $user_info["email"];
                            $data["username"] = $first_name = $user_info["first_name"];
                            $data["mobile_no"] = $user_info["mobile_no"];
                            //for platform
                            $data["from_platform"] = "Web";
                            $view = $this->load->view("email_template/registered_success", $data, true);
                            $result = send_EmailResponse($view, $email, $first_name, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");

                            //To Support Birthday Owl
                            $view1 = $this->load->view("email_template/register_support", $data, true);
                            send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $email($first_name).");
                            $this->session->set_userdata($output);
                        } else {
                            $otp_sender->setMobileNumber($mobile_no);
                            $otp_sender->setHashKey($hashkey);
                            $otp_gone = $otp_sender->runner();
                            $output["success"] = $otp_gone ? true : false;
                            $user_info = $check_guest_exist->getUserInfo();
                            $output['userdata'] = $user_info;
//                            $output['trigger_otp'] = 1;
//                            $output['userdata'] = $user_info;
                        }
                        break;
                    case ($is_google_exist):
                        $getUserId = $check_google_user->getUserId();
                        $check_google_user->update($updated_data);
                        if ($check_google_user->CheckCountryCode()) {
                            $check_google_user->reset_Google_Guest();
                            $user_info = $check_google_user->getUserInfo();
//                            $output['trigger_otp'] = 0;
                            $output['userdata'] = $user_info;
                            $output["userdata"]['flower_data'] = array();
                            $output["userdata"]['voucher_data'] = array();
                            $output["is_converted"] = 0;
                            $output["success"] = true;
                            //need to replace this with object for mail
                            $data["email"] = $email = $user_info["email"];
                            $data["username"] = $first_name = $user_info["first_name"];
                            $data["mobile_no"] = $user_info["mobile_no"];
                            //for platform
                            $data["from_platform"] = "Web";

                            $view = $this->load->view("email_template/registered_success", $data, true);
                            $result = send_EmailResponse($view, $email, $first_name, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");

                            //To Support Birthday Owl
                            $view1 = $this->load->view("email_template/register_support", $data, true);
                            send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $email($first_name).");
                            $this->session->set_userdata($output);
                        } else {
                            $otp_sender->setMobileNumber($mobile_no);
                            $otp_sender->setHashKey($hashkey);
                            $otp_gone = $otp_sender->runner();
                            $output["success"] = $otp_gone ? true : false;
                            $user_info = $check_google_user->getUserInfo();
//                            $output['trigger_otp'] = 1;
                            $output['userdata'] = $user_info;
                        }
                        break;
                }
//                $output["message"] = "Registered successfully";
//                $output["success"] = TRUE;
//                $output["user_id"] = $getUserId;
//                $output["plans"] = array();
//                $output["plan_info"] = array();
            } else {  //insert the new record
                //before inserting delete any invalid guest and birthday owl user whose mobile_verfiy  = 0;
                $this->load->library("DeleteInvalidGuest");
                $deleteInvalidGuest = new DeleteInvalidGuest();
                $deleteInvalidGuest->setEmail($email);
                $deleteInvalidGuest->setPhoneNumber($mobile_no);
                $deleteInvalidGuest->runner();

                $this->load->library("DeleteInvalidBirthdayOwlUser");
                $deleteInvalidBirthdayowlUser = new DeleteInvalidBirthdayOwlUser();
                $deleteInvalidBirthdayowlUser->setEmail($email);
                $deleteInvalidBirthdayowlUser->setPhoneNumber($mobile_no);
                $deleteInvalidBirthdayowlUser->runner();

                $this->load->library("OtpSenderUpdater");
                $otp_sender = new OtpSenderUpdater();

                if ($country_id != 102) {
                    $user_data['mobile_verify'] = 1;
                    $user_data['is_guest'] = 0;
                    $user_data['is_google_login'] = 0;
                    $user_data["platform"] = 2; //for web

                    $user_id = $this->Birthday->addData("users", $user_data);
                    $getUserId = $user_id;
                    $user_info = $this->Birthday->getSelectData("*", "users", "user_id = '$getUserId'")->row_array();
                    $output['userdata'] = $user_info;
                    $output["success"] = true;
                    $output["userdata"]['flower_data'] = array();
                    $output["userdata"]['voucher_data'] = array();
                    $output["is_converted"] = 0;
//                    $where = "user_id='$insert_user_id'";
//                    $check_user = $this->Birthday->getUserData("users", $where)->result_array();
//                    $output["userdata"] = $check_user[0];

                    $data["email"] = $email = $user_info["email"];
                    $data["username"] = $first_name = $user_info["first_name"];
                    $data["mobile_no"] = $user_info["mobile_no"];
                    //for platform
                    $data["from_platform"] = "Web";
                    //To User
                    $view = $this->load->view("email_template/registered_success", $data, true);
                    $result = send_EmailResponse($view, $email, $first_name, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");
                    //To Support Birthday Owl
                    $view1 = $this->load->view("email_template/register_support", $data, true);
                    send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $email($first_name).");
                    if ($signup_type == 0) {
                        $output["message"] = "Registered successfully";
                    }
//                    if ($result["code"] == "success") {
//                        $output["success"] = true;
//                    } else {
//                        $output["success"] = false;
//                    }
                    $this->session->set_userdata($output);
                } else {
                    $user_data["platform"] = 2; // for web
                    $user_id = $this->Birthday->addData("users", $user_data);
                    $getUserId = $user_id;
                    $user_info = $this->Birthday->getSelectData("*", "users", "user_id = '$getUserId'")->row_array();
                    $otp_sender->setMobileNumber($mobile_no);
                    $otp_sender->setHashKey($hashkey);
                    $otp_gone = $otp_sender->runner();
                    $output["success"] = $otp_gone ? true : false;
                    $output['userdata'] = $user_info;
                    $output["is_converted"] = 0;
//                    $output['trigger_otp'] = 1;
//                    $output["plans"] = array();
//                    $output["plan_info"] = array();
                }
            }

//            if (count($check_user_guest) > 0) {
//                $user_id = $check_user_guest[0]['user_id'];
//                $where = "user_id=$user_id";
//                $check_user_guest_data = $this->Birthday->getSelectData("*", "users", $where)->result_array();
//                $mobile_number = $check_user_guest_data[0]['mobile_no'];
//                if ($mobile_number == $mobile_no) {//Check whether post mobile no & user tables mobile no is same or not, if same then don't send OTP
//                    $where = "user_id='$user_id'";
//                    $updated_data["is_guest"] = 0;
//                    $updated = $this->Birthday->update("users", $updated_data, $where);
//
//                    $output["success"] = true;
//                    $where = "user_id='$user_id'";
//                    $check_user = $this->Birthday->getUserData("users", $where)->result_array();
//                    $output["userdata"] = $check_user[0];
//                    $output["userdata"]['flower_data'] = array();
//                    $output["userdata"]['voucher_data'] = array();
//                    $data["email"] = $email = $check_user[0]["email"];
//                    $data["username"] = $first_name = $check_user[0]["first_name"];
//                    $data["mobile_no"] = $check_user[0]["mobile_no"];
//                    //To User
//                    $view = $this->load->view("email_template/registered_success", $data, true);
//                    $result = send_EmailResponse($view, $email, $first_name, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");
//
//                    //To Support Birthday Owl
//                    $view1 = $this->load->view("email_template/register_support", $data, true);
//                    send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $email($first_name).");
//                    if ($signup_type == 0) {
//                        $output["message"] = "Registered successfully";
//                    }
//                    if ($result["code"] == "success") {
//                        $output["success"] = true;
//                    } else {
//                        $output["success"] = false;
//                    }
//                    $output["is_converted"] = 1;
//                    $this->session->set_userdata($output);
//                } else {
//                    //==========================OTP==========================
//                    if ($country_id == 102) {                    //If country id is INDIA then send OTP
//                        $data["code"] = $code = $this->generateRandomString(5);
//                        $data["mobile_no"] = $mobile_no;
//                        $otp_data = $this->Birthday->getSelectData("mobile_no", "otp", "mobile_no=$mobile_no")->result_array();
//                        $count = count($otp_data);
//                        if ($count > 0) {
//                            $update["code"] = $code;
//                            $update["sent_time"] = date("Y-m-d H:i:s");
//                            $this->Birthday->update("otp", $update, "mobile_no='$mobile_no'");
//                        } else {
//                            $data["created_date"] = date("Y-m-d H:i:s");
//                            $data["sent_time"] = date("Y-m-d H:i:s");
//                            $this->Birthday->addData("otp", $data);
//                        }
//                        $show_message = "Enter $code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone.";
//                        try {
//                            $resp = $this->Birthday->send_mobile_otp($mobile_no, $show_message);
//                            $output["success"] = true;
////                $output["message"] = "OTP sent successfully";
//                        } catch (Exception $e) {
//                            $output["success"] = false;
////                $output["message"] = "Failed.Please try again";
//                        }
//                        $where = "user_id='$user_id'";
//                        $check_user = $this->Birthday->getUserData("users", $where)->result_array();
//                        $output["userdata"] = $check_user[0];
//
//                        $where = "user_id='$user_id'";
//                        $updated_data["mobile_verify"] = 0; //Update mobile no in user table to 0 because new mobile no for guest is updated
//                        $updated = $this->Birthday->update("users", $updated_data, $where);
//
//                        $output["is_converted"] = 0;
//                    } else {                    //Else don't send OTP
//                        $where = "user_id='$user_id'";
//                        $updated_data["is_guest"] = 0;
//                        $updated = $this->Birthday->update("users", $updated_data, $where);
//                        $output["success"] = true;
//                        $where = "user_id='$insert_user_id'";
//                        $check_user = $this->Birthday->getUserData("users", $where)->result_array();
//                        $output["userdata"] = $check_user[0];
//                        $output["userdata"]['flower_data'] = array();
//                        $output["userdata"]['voucher_data'] = array();
//                        $data["email"] = $email = $check_user[0]["email"];
//                        $data["username"] = $first_name = $check_user[0]["first_name"];
//                        $data["mobile_no"] = $check_user[0]["mobile_no"];
//                        //To User
//                        $view = $this->load->view("email_template/registered_success", $data, true);
//                        $result = send_EmailResponse($view, $email, $first_name, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");
//
//                        //To Support Birthday Owl
//                        $view1 = $this->load->view("email_template/register_support", $data, true);
//                        send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $email($first_name).");
//                        if ($signup_type == 0) {
//                            $output["message"] = "Registered successfully";
//                        }
//                        if ($result["code"] == "success") {
//                            $output["success"] = true;
//                        } else {
//                            $output["success"] = false;
//                        }
//                        $output["is_converted"] = 1;
//                        $this->session->set_userdata($output);
//                    }
//                }
//            } else {
//                $output["is_converted"] = 0;
//                $insert_user_id = $this->Birthday->addData("users", $user_data);
//                $this->Birthday->addData("news_letter", $new_data);
////==========================OTP==========================
//                if ($country_id == 102) {
//                    $data["code"] = $code = $this->generateRandomString(5);
//                    $data["mobile_no"] = $mobile_no;
//                    $otp_data = $this->Birthday->getSelectData("mobile_no", "otp", "mobile_no=$mobile_no")->result_array();
//                    $count = count($otp_data);
//                    if ($count > 0) {
//                        $update["code"] = $code;
//                        $update["sent_time"] = date("Y-m-d H:i:s");
//                        $this->Birthday->update("otp", $update, "mobile_no='$mobile_no'");
//                    } else {
//                        $data["created_date"] = date("Y-m-d H:i:s");
//                        $data["sent_time"] = date("Y-m-d H:i:s");
//                        $this->Birthday->addData("otp", $data);
//                    }
//                    $show_message = "Enter $code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone.";
//                    try {
//                        $resp = $this->Birthday->send_mobile_otp($mobile_no, $show_message);
//                        $output["success"] = true;
////                $output["message"] = "OTP sent successfully";
//                    } catch (Exception $e) {
//                        $output["success"] = false;
////                $output["message"] = "Failed.Please try again";
//                    }
//                    $where = "user_id='$insert_user_id'";
//                    $check_user = $this->Birthday->getUserData("users", $where)->result_array();
//                    $output["userdata"] = $check_user[0];
//                } else {
//                    $output["success"] = true;
//                    $where = "user_id='$insert_user_id'";
//                    $check_user = $this->Birthday->getUserData("users", $where)->result_array();
//                    $output["userdata"] = $check_user[0];
//                    $output["userdata"]['flower_data'] = array();
//                    $output["userdata"]['voucher_data'] = array();
//                    $data["email"] = $email = $check_user[0]["email"];
//                    $data["username"] = $first_name = $check_user[0]["first_name"];
//                    $data["mobile_no"] = $check_user[0]["mobile_no"];
//                    //To User
//                    $view = $this->load->view("email_template/registered_success", $data, true);
//                    $result = send_EmailResponse($view, $email, $first_name, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");
//
//                    //To Support Birthday Owl
//                    $view1 = $this->load->view("email_template/register_support", $data, true);
//                    send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $email($first_name).");
//                    if ($signup_type == 0) {
//                        $output["message"] = "Registered successfully";
//                    }
//                    if ($result["code"] == "success") {
//                        $output["success"] = true;
//                    } else {
//                        $output["success"] = false;
//                    }
//                    $this->session->set_userdata($output);
//                }
////==========================OTP==========================
//            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

// ====================================================================================================================================//
// -> Function     :send_email_reminder.
// -> Executed     :this function is executed by a cron job file in every once or twice per day.
// -> description  :this functions sends reminder email and notification.
// ====================================================================================================================================//
    function send_email_reminder() {
        $this->sendGreetingByDate();
        $this->WishBirthdayToRegUsers();
        $this->Birthday->sendCommonBdayReminder();
        $this->Birthday->check_membership_expiry();
    }

    function WishBirthdayToRegUsers() {
        $today = date("d/m/00");
        $data = $this->Birthday->getSelectData("bdate,bmonth,first_name,email,CONCAT(LPAD(`bdate`, 2, '0'),'/',LPAD(`bmonth`, 2, '0'),'/','00')as ubirthdate", "users", "CONCAT(LPAD(`bdate`, 2, '0'),'/',LPAD(`bmonth`, 2, '0'),'/','00')='$today'")->result_array();
        foreach ($data as $reg_users) {
            $data['email'] = $uemail = $reg_users["email"];
            $data['first_name'] = $uname = $reg_users["first_name"];
            $view = $this->load->view("email_template/RegisteredBirthday_reminder", $data, true);
            send_Email($view, $uemail, $uname, "birthdayreminder@birthdayowl.com", "birthdayowl.com", "BirthdayOwl : Happy Birthday $uname");
        }
    }

// ====================================================================================================================================//
// -> Function     :Not in use
// -> Executed     :Seems not in use
// -> description  :Not in Use
// ====================================================================================================================================//
    public function send_gift() {
        $timezone = 'Asia/Kolkata';
        $now = new DateTime();
        $now->setTimezone(new DateTimeZone($timezone));
        $current_time = $now->format('Y-m-d H:i:s');
//        Select *,TIMESTAMPDIFF(MINUTE,o.delivery_date_time,NOW()) as days_diff  from  order_products o  left join greeting_card g on g.card_id=o.greeting_id  left join payment p on p.payment_id=o.payment_id   where o.selected_orders=1  and (TIMESTAMPDIFF(MINUTE,delivery_date_time,NOW())=1 and (o.delievery_schedule<>0 || o.delievery_schedule<>1) and o.delivery_status=2  )
        $gift_vouchers = $this->Birthday->sendGift("Select o.terms,g.front_page_image,o.voucher_img, o.type,o.greeting_id, o.order_pro_id,o.payment_id, o.order_id, o.greeting_message,o.selected_amount ,o.fphone,o.uname,o.voucher_pro_id,o.voucher_pro_name,o.uemail,o.femail,o.fname,o.user_id from  order_products o  left join greeting_card g on g.card_id=o.greeting_id  left join payment p on p.payment_id=o.payment_id   where  o.selected_orders=1 and (o.`delievery_schedule` != 0 || o.`delievery_schedule` != 1  ) and  o.`delivery_status`=2 and  (TIMESTAMPDIFF(MINUTE,o.`converted_date`,'$current_time') = 0)")->result_array();
        $giftvoucherscount = count($gift_vouchers);
        if ($giftvoucherscount > 0) {
            foreach ($gift_vouchers as $gifts) {
                $user_id = $gifts["user_id"];
                $where = "user_id=$user_id";
                $userdata = $this->Birthday->getSelectData("mobile_no", "users", $where)->row_array();
                $userphone = $userdata["mobile_no"];
                $woohoodata = $this->GenerateWoohooOrder($gifts["greeting_message"], $gifts["selected_amount"], $userphone, $gifts["fphone"], $gifts["uname"], $gifts["voucher_pro_id"], $gifts["voucher_pro_name"], $gifts["uemail"], $gifts["femail"], $gifts["fname"]);
                $woohoo_status = $woohoodata["status"];
                $order_id = $gifts["order_id"];
                $order_pro_id = $gifts["order_pro_id"];
                $payment_id = $gifts["payment_id"];
                if ($woohoo_status == "Complete") {
                    $data["pin"] = $payment["pin"] = $woohoodata["pin"];
                    $data["code"] = $payment["voucher_code"] = $vcode = $woohoodata["cardnumber"];
                    $data["expiry_date"] = $payment["expiry_date"] = $woohoodata["expiry_date"];
                    $data["type"] = $gifts["type"];
//Birthdayowl-order email
                    $woohoo_order_id = $woohoodata["order_id"];
                    $where = "payment_id=$payment_id";
                    $this->Birthday->update("payment", $payment, $where);
                    $data["euser_id"] = $euser_id = $this->Birthday->encryptPassword($user_id);
                    $keyid = "eGift@birthdayOwl";
                    $data["greeting_id"] = $greeting_id = $this->Birthday->encryptPassword($gifts["greeting_id"]);
                    $data["pay_id"] = $pay_id = $this->Birthday->encryptPassword($payment_id);
                    $data["key"] = $key = $this->Birthday->encryptPassword($keyid);
                    $data["opid"] = $opid = $this->Birthday->encryptPassword($order_pro_id);
                    $odata["order_pro_id"] = $data["order_pro_id"] = $order_pro_id;
                    $link = BASEURL . "Home_web/voucher_email/$euser_id/$pay_id/$greeting_id/$opid/$key";
                    $data["message"] = $message = nl2br(trim(strip_tags($gifts["greeting_message"], '<br/>')));
                    $data["product_image"] = $gifts["voucher_img"];
                    $data["voucher_pro_name"] = $voucher_name = $gifts["voucher_pro_name"];
                    $data["total"] = $gifts["selected_amount"];
                    $data["greeting"] = $gifts["front_page_image"];
                    $data["femail"] = $gifts["femail"];
                    $data["fname"] = $rec_name = $gifts["fname"];
                    $data["fphone"] = $fphone = $gifts["fphone"];
                    $data["uemail"] = $uemail = $gifts["uemail"];
                    $data["uname"] = $uname = $gifts["uname"];
                    $data["terms"] = $gifts["terms"];
                    $data["greeting"] = $gifts["front_page_image"];
                    $selected_amount = $gifts["selected_amount"];
                    $where = "order_pro_id=$order_pro_id";
                    $updated_data["woohoo_order_id"] = $woohoodata["worder_id"];
                    $updated_data["delivery_status"] = 4;
                    $updated_data["order_message"] = "eGift sent successfully";
                    $odata["orderstatus"] = 1;

                    $this->Birthday->update("order_products", $updated_data, $where);
                    $linkData = $this->google_url_api->shorten("$link");
                    $data["click_link"] = $click_link = $linkData->id;

                    if ($data["femail"] != '') {
                        $this->generateVoucherPdf($data, $message, $order_pro_id);
                    }
                    if ($fphone != '') {

                        $show_message = "Dear $rec_name, \n You have just received an awesome e-gift voucher from $uname. \n To view and redeem your e-gift voucher, simply ($click_link)  \n- Birthday Owl (www.birthdayowl.com)";
                        $this->Birthday->send_sms($data["fphone"], $show_message);
                    }
                    if ($userphone != '') {
                        $show_message = "Dear $uname, \n You have just sent an awesome e-gift voucher to $rec_name and we have notified $rec_name accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                        $this->Birthday->send_sms($userphone, $show_message);
                    }
                } else if ($woohoo_status == "Processing" || $woohoo_status == "processing") {
                    $odata["orderstatus"] = 6;
                    $woohoo_order_id = $data["order_id"];
                    $where = "order_pro_id=$order_pro_id";
                    $updated_data["order_id"] = $woohoo_order_id; //processing
                    $updated_data["selected_orders"] = 1;
                    $updated_data["delivery_status"] = 6; //processing  //resend
                    $updated_data["order_message"] = "Order is in Processing state.";
                    $this->Birthday->update("order_products", $updated_data, $where);
                } else if ($woohoo_status == "error") {
                    $odata["orderstatus"] = 8;
                    $where = "order_pro_id=$order_pro_id";
                    $updated_data["selected_orders"] = 1;
                    $updated_data["delivery_status"] = 8; //Failed but resend api request
                    $updated_data["order_id"] = $order_id;
                    if ($woohoodata["ordermessage"] != "")
                        $updated_data["order_message"] = $woohoodata["ordermessage"];
                    else
                        $updated_data["order_message"] = "woohoo error";
                    $this->Birthday->update("order_products", $updated_data, $where);
                } else if ($woohoo_status == "Cancelled" || $woohoo_status == "Blocked") {
                    $where = "order_pro_id=$order_pro_id";
                    $updated_data["selected_orders"] = 1;
                    $updated_data["delivery_status"] = 0; //Failed
                    $this->Birthday->update("order_products", $updated_data, $where);
                }
            }
        }
    }

    public function delete_reminder() {
        if (count($_POST) > 0) {
            $reminder_id = check_post($this->input->post("reminder_id"));
            $where = "reminder_id='$reminder_id'";
            $this->Birthday->delete_reminder("bday_reminder", $where);
            $output["success"] = true;
            $output["message"] = "Reminder deleted successfully";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function deactivate_resetpwd_link() {
        $this->Birthday->deactivate_resetpwd_link();
        $this->Birthday->change_membership_status();
    }

    public function get_greeting_cards($greet_cat_id) {
        $data = array();
        if (count($_POST) > 0) {
            $greet_cat_id = check_post($this->input->post("greet_cat_id"));
            redirect(WEB_FREE_GREETING_CARDS, $data);
        }
    }

    public function getGreetingsByLimit() {
        $start = check_post($this->input->post("start"));
        $type = check_post($this->input->post("type"));
        $greetings = $this->Birthday->getSelectData("card_id,front_page_image", "greeting_card", "greeting_type=$type order by card_id desc limit 6 offset $start")->result_array();
        if (count($greetings) < 6) {
            $addgreetings = $this->Birthday->getSelectData("card_id,front_page_image", "greeting_card", "greeting_type=$type order by card_id desc limit 6 offset 0")->result_array();
            $greetings = array_merge($greetings, $addgreetings);
        }
        $output["card_info"] = $greetings;
        $output["success"] = true;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function free_greetings_email() {
        $data["url"] = str_replace("free_greetings_email", "webMobile", $this->uri->uri_string());
        $freeid = $this->uri->segment(3);
        $id = $this->Birthday->decryptPassword($freeid);
        $data["fid"] = $id;

        //find if that particular fid is for a birthday owl user or for a non birthday owl free user  // user_id = 0(no login) or user_id != 0(logged in user)

        $check_user_id = $this->Birthday->getSelectData("user_id , fid", "free_greetings", "fid=$id")->row_array();
//        print_r($check_user_id['user_id']);
//        exit;

        if ($check_user_id['user_id'] != 0) {  // that means he is a non birhtdayowl user(a free gretting sender user so search in the free_greeting_sender table)
            $check_in = 1; //search in users table
        } else {
            $check_in = 2; //search in  table free_greeting_sender
        }

        switch ($check_in) {
            case 1:
                $on = "f.femail=u.email OR f.fphone=u.mobile_no";
                $checkregistereduser = $this->Birthday->getJoinedData2("f.femail,f.fphone,u.user_id", "free_greetings f", "users u", "$on", "f.fid=$id and u.is_guest=0 and u.email=f.femail")->row_array();
                if (count($checkregistereduser) > 0) {
                    $data["user_id"] = $user_id = $checkregistereduser["user_id"];
                } else {
                    $data["user_id"] = $user_id = 0;
                }
                $greetingdata = $this->Birthday->getJoinedData2("free_greetings.greeting_img,free_greetings.delivery_date,free_greetings.mail_open_status,free_greetings.message,free_greetings.type,free_greetings.uploded_img,users.first_name,users.email,free_greetings.created_date", "free_greetings", "users", "free_greetings.user_id=users.user_id", "free_greetings.fid=$id")->row_array();
                $delivery_date = new DateTime($greetingdata["delivery_date"]);
                $today = new DateTime(date("Y-m-d"));
                $interval = $today->diff($delivery_date);
                if ($interval->d <= 28) {  // number of days till the card expires
                    $data["display"] = $display = 1; //not expired
                } else {
                    $data["display"] = $display = 0; //expired
                }
                break;
            case 2:
                $on = "f.fid=fru.fid_from_freegreeting";
                $greetingdata = $this->Birthday->getJoinedData2("free_greetings.greeting_img,free_greetings.delivery_date,free_greetings.mail_open_status,free_greetings.message,free_greetings.type,free_greetings.uploded_img,free_greeting_sender.free_sender_name as first_name,free_greeting_sender.free_sender_email as email,free_greetings.created_date", "free_greetings", "free_greeting_sender", "free_greetings.fid=free_greeting_sender.fid_from_freegreeting", "free_greetings.fid=$id")->row_array();
                $delivery_date = new DateTime($greetingdata["delivery_date"]);
                $today = new DateTime(date("Y-m-d"));
                $interval = $today->diff($delivery_date);
                if ($interval->d <= 28) {  // number of days till the card expires
                    $data["display"] = $display = 1; //not expired
                } else {
                    $data["display"] = $display = 0; //expired
                }
                break;
        }


//        $on = "f.femail=u.email OR f.fphone=u.mobile_no";
//        $checkregistereduser = $this->Birthday->getJoinedData2("f.femail,f.fphone,u.user_id", "free_greetings f", "users u", "$on", "f.fid=$id and u.is_guest=0 and u.email=f.femail")->row_array();
//        if (count($checkregistereduser) > 0) {
//            $data["user_id"] = $user_id = $checkregistereduser["user_id"];
//        } else {
//            $data["user_id"] = $user_id = 0;
//        }
//        $greetingdata = $this->Birthday->getJoinedData2("free_greetings.greeting_img,free_greetings.delivery_date,free_greetings.mail_open_status,free_greetings.message,free_greetings.type,free_greetings.uploded_img,users.first_name,users.email,free_greetings.created_date", "free_greetings", "users", "free_greetings.user_id=users.user_id", "free_greetings.fid=$id")->row_array();
//        $delivery_date = new DateTime($greetingdata["delivery_date"]);
//        $today = new DateTime(date("Y-m-d"));
//        $interval = $today->diff($delivery_date);
//        if ($interval->d <= 28) {  // number of days till the card expires
//            $data["display"] = $display = 1; //not expired
//        } else {
//            $data["display"] = $display = 0; //expired
//        }
        $this->load->library('user_agent');
//        if ($this->agent->is_mobile()) {
//            $data["detail"] = $detail = $this->agent->mobile();
//            $this->load->view("HOME/CommonDeepLinkingView", $data);
//        } else 
        if ($this->agent->is_browser()) {
            $data["detail"] = "browser";
            $check = $this->Birthday->decryptPassword($this->uri->segment(5));
            $data["greeting_data"] = $greetingdata;
            $data["message"] = nl2br(trim(strip_tags($greetingdata["message"], '<br/>')));
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=1")->row_array();
            $data["seo"] = $title;
            $data["check"] = $check;
            $data["mail_open_status"] = $greetingdata["mail_open_status"];
            $this->load->view("FREEGREETING/freegreeting_email", $data);
        } else {
            $data["detail"] = 'Unidentified User Agent';
            redirect(WEB_HOME);
        }
    }

    public function webMobile() {
        $freeid = $this->uri->segment(3);
        $id = $this->Birthday->decryptPassword($freeid);
        $data["fid"] = $id;
        $on = "f.femail=u.email OR f.fphone=u.mobile_no";
        $checkregistereduser = $this->Birthday->getJoinedData2("f.femail,f.fphone,u.user_id", "free_greetings f", "users u", "$on", "f.fid=$id and u.is_guest=0 and u.email=f.femail")->row_array();
        if (count($checkregistereduser) > 0) {
            $data["user_id"] = $user_id = $checkregistereduser["user_id"];
        } else {
            $data["user_id"] = $user_id = 0;
        }
        $greetingdata = $this->Birthday->getJoinedData2("free_greetings.greeting_img,free_greetings.delivery_date,free_greetings.mail_open_status,free_greetings.message,free_greetings.type,free_greetings.uploded_img,users.first_name,users.email,free_greetings.created_date", "free_greetings", "users", "free_greetings.user_id=users.user_id", "free_greetings.fid=$id")->row_array();
        $delivery_date = new DateTime($greetingdata["delivery_date"]);
        $today = new DateTime(date("Y-m-d"));
        $interval = $today->diff($delivery_date);
        if ($interval->d != 0 || $today == $delivery_date) {
            $data["display"] = $display = 1; //not expired
        } else {
            $data["display"] = $display = 0; //expired
        }
        if ($this->uri->segment(6) == 2) {
            $data["from"] = 2; //change password:1;2:freegreeting 3:Giftgreeting
            $this->load->view("HOME/deep_linking", $data);
        } else {
            $check = $this->Birthday->decryptPassword($this->uri->segment(5));
            $data["greeting_data"] = $greetingdata;
            $data["message"] = nl2br(trim(strip_tags($greetingdata["message"], '<br/>')));
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=1")->row_array();
            $data["seo"] = $title;
            $data["check"] = $check;
            $data["mail_open_status"] = $greetingdata["mail_open_status"];
            $this->load->view("FREEGREETING/freegreeting_email", $data);
        }
    }

    public function voucher_email() {
        $data["url"] = str_replace("Home_web/voucher_email", "voucherMobile", $this->uri->uri_string());

        $order_pro_id = $this->uri->segment(6);
        $data["order_pro_id"] = $orderproid = $this->Birthday->decryptPassword($order_pro_id);
        $checkregistereduser = $this->Birthday->getJoinedData2("o.femail,o.fphone,u.user_id", "order_products o", "users u", "o.femail=u.email OR o.fphone=u.mobile_no", "o.order_pro_id=$orderproid and u.email=o.femail")->row_array();
        if (count($checkregistereduser) > 0) {
            $data["user_id"] = $checkregistereduser["user_id"];
        } else {
            $data["user_id"] = 0;
        }
        $this->load->library('user_agent');
//        if ($this->agent->is_mobile()) {
//            $data["detail"] = $this->agent->mobile();
//            $this->load->view("HOME/CommonDeepLinkingView", $data);
//        } else 
        if ($this->agent->is_browser()) {
            $voucher_id = $this->uri->segment(3);
            $pay_id = $this->uri->segment(4);
            $fgreeting_id = $this->uri->segment(5);
            $data["user_id"] = $user_id = $this->Birthday->decryptPassword($voucher_id);
            $data["payment_id"] = $payment_id = $this->Birthday->decryptPassword($pay_id);
            $data["fgreeting_id"] = $greeting_id = $this->Birthday->decryptPassword($fgreeting_id);
            $voucher_data = $this->Birthday->get_send_giftdata($payment_id, $greeting_id)->result_array();
            $data["gift_data"] = $voucher_data[0];
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=1")->row_array();
            $data["seo"] = $title;
            $this->load->view('EGIFT/voucher_email', $data);
        } else {
            $data["detail"] = 'Unidentified User Agent';
            redirect(WEB_HOME);
        }
    }

    public function voucherMobile() {
        $order_pro_id = $this->uri->segment(5);
        $data["order_pro_id"] = $orderproid = $this->Birthday->decryptPassword($order_pro_id);
        $checkregistereduser = $this->Birthday->getJoinedData2("o.femail,o.fphone,u.user_id", "order_products o", "users u", "o.femail=u.email OR o.fphone=u.mobile_no", "o.order_pro_id=$orderproid and u.email=o.femail")->row_array();
        if (count($checkregistereduser) > 0) {
            $data["user_id"] = $checkregistereduser["user_id"];
        } else {
            $data["user_id"] = 0;
        }
        if ($this->uri->segment(7) == 2) {
            $data["from"] = 3; //change password:1;2:freegreeting 3:Giftgreeting
            $this->load->view("HOME/deep_linking", $data);
        } else {
            $voucher_id = $this->uri->segment(3);
            $pay_id = $this->uri->segment(2);
            $fgreeting_id = $this->uri->segment(4);
            $data["user_id"] = $user_id = $this->Birthday->decryptPassword($voucher_id);
            $data["payment_id"] = $payment_id = $this->Birthday->decryptPassword($pay_id);
            $data["fgreeting_id"] = $greeting_id = $this->Birthday->decryptPassword($fgreeting_id);
            $voucher_data = $this->Birthday->get_send_giftdata($payment_id, $greeting_id)->result_array();
            $data["gift_data"] = $voucher_data[0];
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=1")->row_array();
            $data["seo"] = $title;
            $this->load->view('EGIFT/voucher_email', $data);
        }
    }

    public function passwordMobile() {

        $data["user_id"] = $this->uri->segment(2);
        if ($this->uri->segment(3) == 2) {
            $data["from"] = 1; //change password:1;2:freegreeting 3:Giftgreeting
            $this->load->view("HOME/deep_linking", $data);
        } else {
            redirect(WEB_DASHBOARD);
        }
    }

    public function change_status() {
        $fid = check_post($this->input->post("fid"));
        $check = check_post($this->input->post("check"));
        $mail_open_status = check_post($this->input->post("mail_open_status"));
        if ($check != 1) {
            if ($mail_open_status == "0") {
                $where = "fid=$fid";
                $updated_data["mail_open_status"] = 1;
                $this->Birthday->update("free_greetings", $updated_data, $where);
            }
        }
    }

// ====================================================================================================================================//
// -> Function     :sendThankOpendEmail.
// -> Executed     :this function is executed by a cron job file in every 2 minutes.
// -> description  :this functions sends greeting and sends sender opened mail status.
// ====================================================================================================================================//
    public function sendThankOpendEmail() {
        $this->send_greeting_open_mail();
//        $this->ResendFailedGreetings();
//        $this->sendegiftOpenedMail();
    }

    function send_greeting_open_mail() {
        $greetingdata = $this->Birthday->getJoinedData2("free_greetings.fname,free_greetings.greeting_img,free_greetings.fid,free_greetings.mail_open_status,free_greetings.message,free_greetings.type,free_greetings.uploded_img,users.first_name,users.email", "free_greetings", "users", "free_greetings.user_id=users.user_id", "free_greetings.mail_open_status=1")->result_array();
        if (count($greetingdata) > 0) {
            foreach ($greetingdata as $greetingdata) {
                $data["uname"] = $uname = $greetingdata["first_name"];
                $uemail = $greetingdata["email"];
                $data["fname"] = $fname = $greetingdata["fname"];
                $fid = $greetingdata["fid"];
                try {
                    $nbody = $this->load->view("email_template/egreeting_opened", $data, true);
                    $subjectopen = ucfirst($fname) . " opened your e-greeting. ";
                    $resp = send_EmailResponse($nbody, $uemail, $uname, "greetings@birthdayowl.com", "birthdayowl.com", $subjectopen);
                    $where = "fid=$fid";
                    if ($resp["code"] == "success") {
                        $updated_data["mail_open_status"] = 2;
                        $this->Birthday->update("free_greetings", $updated_data, $where);
                    } else {
                        $updated_data["mail_open_status"] = 1;
                        $this->Birthday->update("free_greetings", $updated_data, $where);
                    }
                } catch (Exception $e) {
                    $where = "fid=$fid";
                    $up["mail_open_status"] = "1";
                    $this->Birthday->update("free_greetings", $up, $where);
                }
            }
        }
        $output["success"] = TRUE;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function sendegiftOpenedMail() {
        $order_pro_id = $this->uri->segment(3);
        $where = "mail_open_status=0 and order_pro_id=$order_pro_id";
        $paid_orders = $this->Birthday->getSelectData("uname,uemail,order_pro_id,fname,selected_amount,voucher_pro_name", "order_products", $where)->row_array();
        $paidorderscount = count($paid_orders);
        if ($paidorderscount > 0) {
            $data["uname"] = $uname = $paid_orders["uname"];
            $uemail = $paid_orders["uemail"];
            $order_pro_id = $paid_orders["order_pro_id"];
            $data["fname"] = $fname = $paid_orders["fname"];
            $data["amount"] = $paid_orders["selected_amount"];
            $data["cname"] = $paid_orders["voucher_pro_name"];
            $nbody = $this->load->view("email_template/egift_opened", $data, true);
            $subjectopen = ucfirst($fname) . " opened your gift. ";
            $resp = send_EmailResponse($nbody, $uemail, $uname, "orders@birthdayowl.com", "birthdayowl.com", $subjectopen);
            if ($resp["code"] == "success") {
                $where = "order_pro_id=$order_pro_id";
                $updated_data["mail_open_status"] = 1;
                $this->Birthday->update("order_products", $updated_data, $where);
            } else {
                $where = "order_pro_id=$order_pro_id";
                $updated_data["mail_open_status"] = 0;
                $this->Birthday->update("order_products", $updated_data, $where);
            }
        }
    }

    public function sendegiftThankMail() {
        $order_pro_id = check_post($this->input->post("order_pro_id"));
        $where = "mail_open_status=1 and order_pro_id=$order_pro_id";
        $paid_orders = $this->Birthday->getSelectData("uname,uemail,order_pro_id,fname,selected_amount,voucher_pro_name", "order_products", $where)->row_array();
        if (count($paid_orders) > 0) {
            $data["uname"] = $uname = $paid_orders["uname"];
            $uemail = $paid_orders["uemail"];
            $order_pro_id = $paid_orders["order_pro_id"];
            $where = "order_pro_id=$order_pro_id";
            $updated_data["mail_open_status"] = 3;
            $this->Birthday->update("order_products", $updated_data, $where);
            $data["fname"] = $fname = $paid_orders["fname"];
            $data["amount"] = $paid_orders["selected_amount"];
            $data["cname"] = $paid_orders["voucher_pro_name"];
            $subject = ucfirst($fname) . " says Thank You!";
            $nbody = $this->load->view("email_template/egift_thank_you", $data, true);
            $resp = send_EmailResponse($nbody, $uemail, $uname, "orders@birthdayowl.com", "birthdayowl.com", $subject);
            if ($resp["code"] == "success") {
                $where = "order_pro_id=$order_pro_id";
                $updated_data["mail_open_status"] = 2;
                $this->Birthday->update("order_products", $updated_data, $where);
            } else {
                $where = "order_pro_id=$order_pro_id";
                $updated_data["mail_open_status"] = 1;
                $this->Birthday->update("order_products", $updated_data, $where);
            }
        }
        $output["success"] = TRUE;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function download_pdf() {
        $fid = $this->uri->segment(3);
        $from = $this->uri->segment(4);
        if ($from == 1) {
            $greetingdata = $this->Birthday->getSelectData("greeting_img as front_page_image,message as greeting_message ", "free_greetings", "fid=$fid")->row_array();
        } else {
            $greetingdata = $this->Birthday->getJoinedDataByGroup("greeting_card.front_page_image,order_products.greeting_message  ", "order_products", "greeting_card", "order_products.greeting_id=greeting_card.card_id", " order_products.order_pro_id=$fid")->row_array();
        }
        $gimage = $greetingdata["front_page_image"];
        $gmessage = $greetingdata["greeting_message"];
        $this->load->library('Pdf'); // Load library
        $img = BASEURL_GREETINGS . $gimage;
        if (ob_get_length() > 0) {
            ob_end_clean();
        }

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);

        $pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);
        $pdf->AddPage("L", " A4");
        $pdf->Rect(0, 0, 2000, 250, 'F', array(), array(58, 58, 58));
        $text = nl2br(trim(strip_tags($gmessage, '<br/>')));
        $html = <<<EOD
<div style="text-align:center;">$text</div>
EOD;

        $pdf->SetFont('times', 'i', 14);
        $pdf->Image($img, 8, 7.5, 281, 196, "", "", "M", FALSE, 15);
        $pdf->writeHTMLCell(90, 100, 172, 48, $html, 0, 0, false, true, 'center', true);
        $filename = "files/Greeting.pdf";
        $pdf->Output("$filename", 'D');
    }

    public function check_email() {
        if (count($_POST) > 0) {
            $email = $this->input->post("email");
            $check_user = $this->Birthday->getSelectData("email", "users", "email='$email'")->num_rows();
            if ($check_user > 0) {
                $output["success"] = false;
                $output["message"] = "Email-id already exists.";
            } else {
                $output["success"] = true;
                $output["message"] = "hello";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function check_mobile_number() {
        if (count($_POST) > 0) {
            $mobile_no = $this->input->post("mobile_no");
            $check_user = $this->Birthday->getSelectData("mobile_no", "users", "mobile_no='$mobile_no'")->num_rows();
            if ($check_user > 0) {
                $output["success"] = false;
                $output["message"] = "Mobile Number already exists.";
            } else {
                $output["success"] = true;
                $output["message"] = "hello";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function check_update_email() {
        if (count($_POST) > 0) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $email = $this->input->post("email");
            $check_user = $this->Birthday->getSelectData("email", "users", "email='$email' and user_id!='$user_id' and is_guest=0")->num_rows();
            if ($check_user > 0) {
                $output["success"] = false;
                $output["message"] = "Email-id already exists.";
            } else {
                $output["success"] = true;
                $output["message"] = "hello";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function get_zodiac() {
        if (count($_POST) > 0) {
            $bday = check_post($this->input->post("bday"));
            $bmonth = check_post($this->input->post("bmonth"));
            $zodiac_name = $this->Birthday->getUserData("mapzodiac", "(fdate <='$bday' and fmonth='$bmonth') or (tdate>='$bday' and tmonth ='$bmonth')")->row_array();

            if (count($zodiac_name) > 0) {
                $output["success"] = true;
                $output["message"] = $zodiac_name["zodiac_sign"];
            } else {
                $output["success"] = false;
                $output["message"] = "zodiac sign is not found"; //If user name is not in the database
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function newsletter_success() {
        if (count($_POST) > 0) {
            $data["email_id"] = $email = check_post($this->input->post("email_id"));
            $data["first_name"] = $first_name = check_post($this->input->post("first_name"));
            $check_email = $this->Birthday->getSelectData("email_id", "news_letter", "email_id='$email'")->row_array();
            if (count($check_email) < 0) {
                $this->Birthday->addData("news_letter", $data);
                $output["success"] = true;
                $output["message"] = "Thanks for subscribing!."; //If user name is not in the database
            } else {
                $output["success"] = true;
                $output["message"] = "You are already subscribed!."; //If user name is in the database
            }


            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function get_zodiac_info() {
        if (count($_POST) > 0) {
            $sign_day = check_post($this->input->post("sign_day"));
            $sign_month = check_post($this->input->post("sign_month"));
            $where = "sign_day='$sign_day'and sign_month='$sign_month'";
            $zodiac_info = $this->Birthday->getUserData("zodiac_signs", $where)->row_array();
            if (count($zodiac_info) > 0) {
                $output["success"] = true;
                $output["sign_info"] = $zodiac_info["sign_data"];
                $end = stripos($output["sign_info"], 'Children and Family');
                $output["first_part"] = substr($output["sign_info"], 0, $end);
                $start = stripos($output["sign_info"], 'Children and Family');
                $text = substr($output["sign_info"], $start);
                $word = "Children and Family";
                $output["second_part"] = str_ireplace($word, "<b>$word</b>", $text);
            } else {
                $output["success"] = false;
                $output["message"] = "zodiac sign is not found";
            }


            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function faq() {
        $data["faq"] = $this->Birthday->getAllData("faq")->result_array();
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=16")->row_array();
        $data["seo"] = $title;
        $this->load->view('HOME/faq', $data);
    }

    public function shipping_policy_flower() {
        $shipping_policy = $this->Birthday->getAllData("shipping_policy")->row_array();
        $data["shipping_policy"] = $shipping_policy["shipping_policy_data"];
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=21")->row_array();
        $data["seo"] = $title;
        $this->load->view('HOME/shipping-policy', $data);
    }

    public function refund_policy() {
        $refund_policy = $this->Birthday->getAllData("refund_policy")->row_array();
        $data["refund_policy"] = $refund_policy["refund_policy_data"];
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=20")->row_array();
        $data["seo"] = $title;
        $this->load->view('HOME/refund-policy', $data);
    }

    public function about_us() {
        $aboutus = $this->Birthday->getAllData("about_us")->row_array();
        $data["about_us"] = $aboutus["about_data"];
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=14")->row_array();
        $data["seo"] = $title;
        $this->load->view('HOME/about-us', $data); //about us 
    }

    public function add_reminder_without_login() {
        if ($this->check_guest_logged_in() || $this->check_google_logged_in() || $this->check_logged_in()) {
            redirect(BASEURL . "addReminder/0");
        } else {
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=22")->row_array();
            $data["seo"] = $title;
            $this->load->view('BIRTHDAY_REMINDER/Add_reminder_without_login', $data); //Add Reminder Without Login
        }
    }

    public function terms_n_conditions() {
        $terms = $this->Birthday->getAllData("terms")->row_array();
        $data["terms"] = $terms["terms"];
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=15")->row_array();
        $data["seo"] = $title;
        $this->load->view('HOME/terms_n_conditions', $data); //terms_n_conditions 
    }

    public function terms_n_conditions_egift() {
        $terms = $this->Birthday->getAllData("terms_egift")->row_array();
        $data["terms"] = $terms["terms"];
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=25")->row_array();
        $data["seo"] = $title;
        $this->load->view('HOME/terms_n_conditions_egift', $data); //terms_n_conditions_egift 
    }

    public function terms_n_conditions_other() {
        $terms = $this->Birthday->getAllData("terms_other")->row_array();
        $data["terms"] = $terms["terms"];
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=24")->row_array();
        $data["seo"] = $title;
        $this->load->view('HOME/terms_n_conditions_other', $data); //terms_n_conditions_other 
    }

    public function privacy_policy() {
        $data["heading"] = "PRIVACY & SECURITY POLICY";
        $privacy = $this->Birthday->getAllData("privacy")->row_array();
        $data["privacy"] = $privacy["privacy_data"];
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=8")->row_array();
        $data["seo"] = $title;
        $this->load->view('HOME/privacy_policy', $data); //about us 
    }

    public function shipping_policy() {
        $data["heading"] = "SHIPPING POLICY";
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $this->load->view('HOME/privacy_policy', $data); //about us 
    }

    public function callWoohooProductsApi_new() {
        $temporaryCredentialsRequestUrl = temporaryCredentialsRequestUrl; //request token url
        $authorizationUrl = authorizationUrl; //authorize url
        $accessTokenRequestUrl = accessTokenRequestUrl; //access token url
//live creds as provided by Qwikcilver
        $consumerKey = consumerKey;
        $consumerSecret = consumerSecret;
        $username = Wusername;
        $password = Wpassword;
        if (!isset($_SESSION)) {
            session_start();
        }
//  session_write_close();
        try {
            $authType = OAUTH_AUTH_TYPE_URI;
            $oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);
            $requestToken = $oauthClient->getRequestToken($temporaryCredentialsRequestUrl); // return type array
            $ch = curl_init();
            $url = $authorizationUrl . $requestToken['oauth_token'] . '&username=' . $username . '&password=' . $password;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0); //1 for a post request
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            $verifierResponse = json_decode(curl_exec($ch));
            curl_close($ch);

//step 3:if received a success parameter as true then call the access token url and exchange the request tokens for access tokens
            if ($verifierResponse->success == 1) {
                $oauthClient->setToken($requestToken['oauth_token'], $requestToken['oauth_token_secret']);
                $verifier_token = $verifierResponse->verifier;
                $accessToken = $oauthClient->getAccessToken($accessTokenRequestUrl, null, $verifier_token); // return type array
                $_SESSION['oauth_token'] = $acc_token = $accessToken['oauth_token'];
                $_SESSION['oauth_token_secret'] = $acces_sec_key = $accessToken["oauth_token_secret"];
                $headers = array();
                $headers[] = "Content-Type: application/json";
                $headers[] = "Accept: */*";
                $oauthClient->setAuthType(OAUTH_AUTH_TYPE_AUTHORIZATION);
                $oauthClient->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
                $murl = wurl;
                $oauthClient->fetch($murl . "settings", array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));
                $settings = json_decode($oauthClient->getLastResponse(), TRUE);
                $payment_method["payment_method"] = $settings["payment_method"]["value"];
                $sfile = "files/settings.txt";
                $settingresults = json_encode($payment_method);
                file_put_contents($sfile, $settingresults);
                $oauthClient->fetch($murl . "category", array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));
                $categoryList = json_decode($oauthClient->getLastResponse(), TRUE);

                if (count($categoryList) > 0) {
                    $category_id = $categoryList["root_category"]["category_id"];
                    $oauthClient->fetch($murl . "category/" . $category_id, array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));
                    $productsList = json_decode($oauthClient->getLastResponse(), TRUE);
                    $woohooprocount = $productsList["count"];
//                    $file = "files/products.txt";
//                    $fileproducts = file_get_contents($file);
//                    if (SERVER_TYPE == 1)
                    $file = BASEURL . "files/products_veena.txt";
//                    else
//                        $file = BASEURL . "files/products1.txt";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_URL, $file);
                    $fileproducts = curl_exec($ch);
                    curl_close($ch);

                    $fileproducts_count = count(json_decode($fileproducts, True));
//                    if ($woohooprocount > $fileproducts_count) {
                    $products = $productsList["_embedded"]["product"];
                    $pro_count = count($products);
                    $product_Array = array();
                    for ($i = 0; $i < $pro_count; $i++) {
                        if ($products[$i]["custom_denominations"] != "") {
                            $product_Array ["voucher_pro_id"] = $products[$i]["id"];
                            $product_Array ["voucher_pro_name"] = $products[$i]["name"];
                            $product_Array ["product_image"] = $products[$i]["images"]["small_image"];
                            $product_Array ["amount"] = $products[$i]["custom_denominations"];
                            $product_Array ["min_custom_price"] = $products[$i]["min_custom_price"];
                            $product_Array ["max_custom_price"] = $products[$i]["max_custom_price"];
                            $product_Array ["max_custom_price"] = $products[$i]["max_custom_price"];
                            $product_Array ["voucher_url"] = $products[$i]["navigation_urlpath"];
                            $oauthClient->fetch($murl . "product/" . $products[$i]["id"], array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));

                            $productinfo = json_decode($oauthClient->getLastResponse(), TRUE);
//                            $product_Array ["description"] = $productinfo["description"];
                            $product_Array ["terms_conditions"] = $productinfo["tnc_web"];
                            $product_Array ["vdescription"] = $productinfo["description"];

                            $product_Array ["order_handling_charge"] = $productinfo["order_handling_charge"];
                            $product_Array ["voucher_cat_id"] = "";
                            $listofproducts[$i] = $product_Array;
                        }
                    }
//                    if (SERVER_TYPE == 1)
                    $file = "files/products_veena.txt";
//                    else
//                        $file = "files/products1.txt";
                    $results = json_encode($listofproducts);
                    file_put_contents($file, $results);
                }
                $output['success'] = true;
                $output['message'] = "Vouchers Synced successfully";
                $output['payment_method'] = $settingresults;
//                }
            }
        } catch (OAuthException $e) {
            print_r($e->getMessage());
            echo "<br/>";
            print_r($e->lastResponse);
//              
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function callWoohooProductsApi() {
        $temporaryCredentialsRequestUrl = temporaryCredentialsRequestUrl; //request token url
        $authorizationUrl = authorizationUrl; //authorize url
        $accessTokenRequestUrl = accessTokenRequestUrl; //access token url
//live creds as provided by Qwikcilver
        $consumerKey = consumerKey;
        $consumerSecret = consumerSecret;
        $username = Wusername;
        $password = Wpassword;
        if (!isset($_SESSION)) {
            session_start();
        }
//  session_write_close();
        try {
//            $authType = OAUTH_AUTH_TYPE_URI;
            $oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);
            $requestToken = $oauthClient->getRequestToken($temporaryCredentialsRequestUrl); // return type array
            $ch = curl_init();
            $url = $authorizationUrl . $requestToken['oauth_token'] . '&username=' . $username . '&password=' . $password;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0); //1 for a post request
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            $verifierResponse = json_decode(curl_exec($ch));
            curl_close($ch);

//step 3:if received a success parameter as true then call the access token url and exchange the request tokens for access tokens
            if ($verifierResponse->success == 1) {
                $oauthClient->setToken($requestToken['oauth_token'], $requestToken['oauth_token_secret']);
                $verifier_token = $verifierResponse->verifier;
                $accessToken = $oauthClient->getAccessToken($accessTokenRequestUrl, null, $verifier_token); // return type array
                $_SESSION['oauth_token'] = $acc_token = $accessToken['oauth_token'];
                $_SESSION['oauth_token_secret'] = $acces_sec_key = $accessToken["oauth_token_secret"];
                $headers = array();
                $headers[] = "Content-Type: application/json";
                $headers[] = "Accept: */*";
                $oauthClient->setAuthType(OAUTH_AUTH_TYPE_AUTHORIZATION);
                $oauthClient->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
                $murl = wurl;
                $oauthClient->fetch($murl . "settings", array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));
                $settings = json_decode($oauthClient->getLastResponse(), TRUE);
                $payment_method["payment_method"] = $settings["payment_method"]["value"];
                $sfile = "files/settings.txt";
                $settingresults = json_encode($payment_method);
                file_put_contents($sfile, $settingresults);
                $oauthClient->fetch($murl . "category", array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));
                $categoryList = json_decode($oauthClient->getLastResponse(), TRUE);

                if (count($categoryList) > 0) {
                    $category_id = $categoryList["root_category"]["category_id"];
                    $oauthClient->fetch($murl . "category/" . $category_id, array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));
                    $productsList = json_decode($oauthClient->getLastResponse(), TRUE);
                    $woohooprocount = $productsList["count"];
//                    $file = "files/products.txt";
//                    $fileproducts = file_get_contents($file);
                    if (SERVER_TYPE == 3)
                        $file = BASEURL . "files/products.txt";
                    else
                        $file = BASEURL . "files/products1.txt";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_URL, $file);
                    $fileproducts = curl_exec($ch);
                    curl_close($ch);

                    $fileproducts_count = count(json_decode($fileproducts, True));
//                    if ($woohooprocount > $fileproducts_count) {
                    $products = $productsList["_embedded"]["product"];
                    $pro_count = count($products);
                    $product_Array = array();
                    for ($i = 0; $i < $pro_count; $i++) {
                        if ($products[$i]["custom_denominations"] != "") {
                            $product_Array ["voucher_pro_id"] = $products[$i]["id"];
                            $product_Array ["voucher_pro_name"] = $products[$i]["name"];
                            $product_Array ["product_image"] = $products[$i]["images"]["small_image"];
                            $product_Array ["amount"] = $products[$i]["custom_denominations"];
                            $product_Array ["min_custom_price"] = $products[$i]["min_custom_price"];
                            $product_Array ["max_custom_price"] = $products[$i]["max_custom_price"];
                            $product_Array ["max_custom_price"] = $products[$i]["max_custom_price"];
                            $product_Array ["voucher_url"] = $products[$i]["navigation_urlpath"];
                            $oauthClient->fetch($murl . "product/" . $products[$i]["id"], array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));

                            $productinfo = json_decode($oauthClient->getLastResponse(), TRUE);
//                            $product_Array ["description"] = $productinfo["description"];
                            $product_Array ["terms_conditions"] = $productinfo["tnc_web"];
                            $product_Array ["vdescription"] = $productinfo["description"];

                            $product_Array ["order_handling_charge"] = $productinfo["order_handling_charge"];
                            $product_Array ["voucher_cat_id"] = "";
                            $listofproducts[$i] = $product_Array;
                        }
                    }
                    if (SERVER_TYPE == 3)
                        $file = "files/products.txt";
                    else
                        $file = "files/products1.txt";
                    $results = json_encode($listofproducts);
                    file_put_contents($file, $results);
                }
                $output['success'] = true;
                $output['message'] = "Vouchers Synced successfully";
                $output['payment_method'] = $settingresults;
//                }
            }
        } catch (OAuthException $e) {
            print_r($e->getMessage());
            echo "<br/>";
            print_r($e->lastResponse);
//              
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function GenerateWoohooOrder($gift_message, $amount, $sender_mobile, $receiver_mobile, $sender_firstname, $product_id, $product_name, $senderemail, $receiver_email, $receiver_fname) {

        $temporaryCredentialsRequestUrl = temporaryCredentialsRequestUrl; //request token url
        $authorizationUrl = authorizationUrl; //authorize url
        $accessTokenRequestUrl = accessTokenRequestUrl; //access token url
//live creds as provided by Qwikcilver
        $consumerKey = consumerKey;
        $consumerSecret = consumerSecret;
        $username = Wusername;
        $password = Wpassword;

        if (!isset($_SESSION)) {
            session_start();
        }
//   session_write_close();
        try {
//token generation will consist of three steps
//step 1:generate request tokens
            $authType = OAUTH_AUTH_TYPE_URI;
//OAuth is a php oauthClient Library which consists methods
//for generating Request Tokens and Access Tokens, the
//library also consists methods for making api calls
            $oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);

            $requestToken = $oauthClient->getRequestToken($temporaryCredentialsRequestUrl); // return type array
//
//step 2:do a curl call to authorizationUrl which will return verifer and success parameter
            $ch = curl_init();
            $url = $authorizationUrl . $requestToken['oauth_token'] . '&username=' . $username . '&password=' . $password;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0); //1 for a post request
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            $verifierResponse = json_decode(curl_exec($ch));
            curl_close($ch);

//step 3:if received a success parameter as true then call the access token url and exchange the request tokens for access tokens
            if ($verifierResponse->success == 1) {
// print_r($requestToken['oauth_token']);
                $oauthClient->setToken($requestToken['oauth_token'], $requestToken['oauth_token_secret']);
                $verifier_token = $verifierResponse->verifier;
                $accessToken = $oauthClient->getAccessToken($accessTokenRequestUrl, null, $verifier_token); // return type array
//                $acc_token = $myaccess_token = $accessToken['oauth_token'];
//                $acces_sec_key = $myaccess_secret_key = $accessToken["oauth_token_secret"];
                $_SESSION['oauth_token'] = $acc_token = $accessToken['oauth_token'];
                $_SESSION['oauth_token_secret'] = $acces_sec_key = $accessToken["oauth_token_secret"];
                $headers = array();
                $headers[] = "Content-Type: application/json";
                $headers[] = "Accept: */*";
                $oauthClient->setAuthType(OAUTH_AUTH_TYPE_AUTHORIZATION);
                $oauthClient->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
                $murl = wurl;
//                $oauthClient->fetch($murl . "settings", array(), 'GET', $headers);
//                $settings = json_decode($oauthClient->getLastResponse(), TRUE);
//Write now we are not using because commented on 07-aug-2018
//                $file = "files/settings.txt";
//                $products = json_decode(file_get_contents($file), True);
//                $payment_method = $products["payment_method"];

                $oauthClient->fetch($murl . "refno", array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));
                $refno = json_decode($oauthClient->getLastResponse(), TRUE);
                $ref_no = $refno["refno"];

                $receiver_mobile = $sender_mobile = "8451811902";
//  $receiver_mobile = "8451811902";
//sender
                $spend_request = json_encode(array(
                    "billing" => array(
                        'firstname' => "sujit",
                        'lastname' => "Subramanian",
                        'email' => "$senderemail",
                        'telephone' => "$sender_mobile",
                        'line_1' => "address details1",
                        'line_2' => "address details1",
                        'city' => "Mumbai",
                        'region' => "Maharashtra",
                        'country_id' => "IN",
                        'postcode' => "400097"
                    ),
                    //receiver
                    "shipping" => array(
                        'firstname' => "sujit",
                        'lastname' => "Subramanian",
                        'email' => "$receiver_email",
                        'telephone' => "$receiver_mobile",
                        'line_1' => "address details1",
                        'line_2' => "address details1",
                        'city' => "Mumbai",
                        'region' => "Maharashtra",
                        'country_id' => "IN",
                        'postcode' => "400097"
                    ),
                    "payment_method" => array(array(
//                            'method' => "$payment_method",
                            'method' => "svc",
                            'amount_to_redem' => $amount,
                            'po_number' => "1672637263278"
                        )
                    ),
                    'refno' => "$ref_no",
                    'delivery_mode' => "API",
                    "products" => array(array(
                            'product_id' => "$product_id",
                            'price' => "$amount",
                            'theme' => "",
                            'qty' => "1",
                            'gift_message' => "$gift_message"
                        )
                    ),
                ));

                $oauthClient->fetch($murl . "spend", $spend_request, 'POST', array('Content-Type' => 'application/json', 'Accept' => '*/*'));
                $spendresponse = json_decode($oauthClient->getLastResponse(), TRUE);
                $data["success_status"] = $spendresponse["success"];
                $data["status"] = $spendresponse["status"];
                if ($data["status"] == "Complete") {
                    $data["worder_id"] = $spendresponse["order_id"];
                    $data["expiry_date"] = $spendresponse["carddetails"][$product_name][0]["expiry_date"];
                    $data["cardnumber"] = $spendresponse["carddetails"][$product_name][0]["cardnumber"];
                    $data["pin"] = $spendresponse["carddetails"][$product_name][0]["pin_or_url"];
                }
                $data["order_id"] = $spendresponse["order_id"];
                return $data;
            }
        } catch (OAuthException $e) {
            $data["success_status"] = "error";
            $data["status"] = "error";
// return $data;
//            print_r($e->lastResponse);
//            exit;

            $Failresponse = json_decode($e->lastResponse, true);
            if (is_array($Failresponse)) {
                if (array_key_exists("message", $Failresponse)) {
                    $data["ordermessage"] = $Failresponse["message"];
                }
            }

            return $data;
//             print_r($data);
//            exit;
        }
    }

    public function resend_woohoo_api() {

        if (count($_POST) > 0) {
            $data["order_pro_id"] = $order_pro_id = check_post($this->input->post("order_pro_id"));
            $where = "order_pro_id='$order_pro_id'";
            $order_productsdata = $this->Birthday->getorderPaymentData($order_pro_id)->row_array();
            $woohoo_orderid = $order_productsdata["woohoo_order_id"];
            $product_name = $order_productsdata["voucher_pro_name"];
            $user_id = $order_productsdata["user_id"];
            $userphone = $this->session->userdata["userdata"]["mobile_no"];
            $temporaryCredentialsRequestUrl = temporaryCredentialsRequestUrl; //request token url
            $authorizationUrl = authorizationUrl; //authorize url
            $accessTokenRequestUrl = accessTokenRequestUrl; //access token url
//live creds as provided by Qwikcilver
            $consumerKey = consumerKey;
            $consumerSecret = consumerSecret;
            $username = Wusername;
            $password = Wpassword;

            if (!isset($_SESSION)) {
                session_start();
            }
            session_write_close();
            try {
//token generation will consist of three steps
//step 1:generate request tokens
                $authType = OAUTH_AUTH_TYPE_URI;
//OAuth is a php oauthClient Library which consists methods
//for generating Request Tokens and Access Tokens, the
//library also consists methods for making api calls
                $oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);

                $requestToken = $oauthClient->getRequestToken($temporaryCredentialsRequestUrl); // return type array
//step 2:do a curl call to authorizationUrl which will return verifer and success parameter
                $ch = curl_init();
                $url = $authorizationUrl . $requestToken['oauth_token'] . '&username=' . $username . '&password=' . $password;
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0); //1 for a post request
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                $verifierResponse = json_decode(curl_exec($ch));
                curl_close($ch);

//step 3:if received a success parameter as true then call the access token url and exchange the request tokens for access tokens
                if ($verifierResponse->success == 1) {
// print_r($requestToken['oauth_token']);
                    $oauthClient->setToken($requestToken['oauth_token'], $requestToken['oauth_token_secret']);
                    $verifier_token = $verifierResponse->verifier;
                    $accessToken = $oauthClient->getAccessToken($accessTokenRequestUrl, null, $verifier_token); // return type array
                    $_SESSION['oauth_token'] = $acc_token = $accessToken['oauth_token'];
                    $_SESSION['oauth_token_secret'] = $acces_sec_key = $accessToken["oauth_token_secret"];
                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = "Accept: */*";
                    $oauthClient->setAuthType(OAUTH_AUTH_TYPE_AUTHORIZATION);
                    $oauthClient->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
                    $murl = wurl;
                    $oauthClient->fetch($murl . "resend/$woohoo_orderid", array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));
                    $resend_data = json_decode($oauthClient->getLastResponse(), TRUE);
                    $data["success_status"] = $resend_data["success"];
                    $data["status"] = $resend_data["status"];
                    $data["order_id"] = $resend_data["order_id"];

                    if ($data["status"] == "Complete") {
                        $data["worder_id"] = $resend_data["order_id"];
                        $data["expiry_date"] = $resend_data["carddetails"][$product_name][0]["expiry_date"];
                        $data["code"] = $resend_data["carddetails"][$product_name][0]["cardnumber"];
                        $data["pin"] = $resend_data["carddetails"][$product_name][0]["pin_or_url"];
                        $data["euser_id"] = $euser_id = $this->Birthday->encryptPassword($user_id);
                        $keyid = "eGift@birthdayOwl";
                        $data["greeting_id"] = $greeting_id = $this->Birthday->encryptPassword($order_productsdata["greeting_id"]);

                        $data["pay_id"] = $pay_id = $this->Birthday->encryptPassword($order_productsdata["payment_id"]);
                        $data["key"] = $key = $this->Birthday->encryptPassword($keyid);
                        $data["opid"] = $opid = $this->Birthday->encryptPassword($order_pro_id);
                        $data["linkclick"] = $link = BASEURL . "Home_web/voucher_email/$euser_id/$pay_id/$greeting_id/$opid/$key";
// $data["order_id"] = $order_id;
                        $text = nl2br(trim(strip_tags($order_productsdata["greeting_message"], '<br/>')));
                        $data["message"] = $message = $text;
                        $data["product_image"] = $order_productsdata["voucher_img"];
                        $data["voucher_pro_name"] = $voucher_name = $order_productsdata["voucher_pro_name"];
                        $data["total"] = ($order_productsdata["selected_amount"] );
                        $data["greeting"] = $order_productsdata["front_page_image"];
                        $data["femail"] = $femail = $order_productsdata["femail"];
                        $data["fname"] = $rec_name = $order_productsdata["fname"];
                        $data["fphone"] = $fphone = $order_productsdata["fphone"];
                        $data["uemail"] = $uemail = $order_productsdata["uemail"];
                        $data["uname"] = $uname = $order_productsdata["uname"];
                        $data["terms"] = $order_productsdata["terms"];
                        $data["type"] = $order_productsdata["type"];
                        $linkData = $this->google_url_api->shorten("$link");
                        $data["click_link"] = $click_link = $linkData->id;

                        $img = BASEURL_GREETINGS . $data["greeting"];
                        if ($femail != '') {
                            $retval = $this->generateVoucherPdf($data, $message, $order_pro_id);
                            if ($retval == 1) {
                                $output["success"] = true;
                                $output["message"] = "eGift resent Successfully!";
                            } else {
                                $output["success"] = false;
                                $output["message"] = "eGift can not resend ";
                            }
                        }
                        if ($fphone != '') {

                            $show_message = "Dear $rec_name, \n You have just received an awesome e-gift voucher from $uname. \n To view and redeem your e-gift voucher, simply ($click_link)  \n- Birthday Owl (www.birthdayowl.com)";
                            $this->Birthday->send_sms($data["fphone"], $show_message);
                        }
                        if ($userphone != '') {
                            $show_message = "Dear $uname, \n You have just sent an awesome e-gift voucher to $rec_name and we have notified $rec_name accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                            $this->Birthday->send_sms($userphone, $show_message);
                        }
                    }
                }
            } catch (OAuthException $e) {
                $output["success"] = false;
                $output["message"] = "eGift can not resend ";

// $Failresponse = json_decode($e->lastResponse, true);
            }

            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function callWoohooProductsApi1() {
        $temporaryCredentialsRequestUrl = temporaryCredentialsRequestUrl; //request token url
        $authorizationUrl = authorizationUrl; //authorize url
        $accessTokenRequestUrl = accessTokenRequestUrl; //access token url
//live creds as provided by Qwikcilver
        $consumerKey = consumerKey;
        $consumerSecret = consumerSecret;
        $username = Wusername;
        $password = Wpassword;
        if (!isset($_SESSION)) {
            session_start();
        }
//  session_write_close();
        try {
            $authType = OAUTH_AUTH_TYPE_URI;
            $oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);
            $requestToken = $oauthClient->getRequestToken($temporaryCredentialsRequestUrl); // return type array
            $ch = curl_init();
            $url = $authorizationUrl . $requestToken['oauth_token'] . '&username=' . $username . '&password=' . $password;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0); //1 for a post request
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            $verifierResponse = json_decode(curl_exec($ch));
            curl_close($ch);

//step 3:if received a success parameter as true then call the access token url and exchange the request tokens for access tokens
            if ($verifierResponse->success == 1) {
                $oauthClient->setToken($requestToken['oauth_token'], $requestToken['oauth_token_secret']);
                $verifier_token = $verifierResponse->verifier;
                $accessToken = $oauthClient->getAccessToken($accessTokenRequestUrl, null, $verifier_token); // return type array
                $_SESSION['oauth_token'] = $acc_token = $accessToken['oauth_token'];
                $_SESSION['oauth_token_secret'] = $acces_sec_key = $accessToken["oauth_token_secret"];
                $headers = array();
                $headers[] = "Content-Type: application/json";
                $headers[] = "Accept: */*";
                $result = $oauthClient->setAuthType(OAUTH_AUTH_TYPE_AUTHORIZATION);
                $oauthClient->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
                $murl = wurl;
                $oauthClient->fetch($murl . "settings", array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));
                $settings = json_decode($oauthClient->getLastResponse(), TRUE);
                $payment_method["payment_method"] = $settings["payment_method"]["value"];
                $sfile = "files/settings.txt";
                $settingresults = json_encode($payment_method);
                file_put_contents($sfile, $settingresults);
                $oauthClient->fetch($murl . "category", array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));
                $categoryList = json_decode($oauthClient->getLastResponse(), TRUE);

                if (count($categoryList) > 0) {
                    $category_id = $categoryList["root_category"]["category_id"];
                    $oauthClient->fetch($murl . "category/" . $category_id, array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));
                    $productsList = json_decode($oauthClient->getLastResponse(), TRUE);
                    $woohooprocount = $productsList["count"];
//                    $file = "files/products_1.txt";
//                    $fileproducts = file_get_contents($file);
                    $file = BASEURL . "files/products_1.txt";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_URL, $file);
                    $fileproducts = curl_exec($ch);
                    curl_close($ch);

                    $fileproducts_count = count(json_decode($fileproducts, True));
//                    if ($woohooprocount > $fileproducts_count) {
                    $products = $productsList["_embedded"]["product"];


                    $pro_count = count($products);
                    $product_Array = array();
                    for ($i = 0; $i < $pro_count; $i++) {

                        $product_Array ["voucher_pro_id"] = $products[$i]["id"];
                        $product_Array ["voucher_pro_name"] = $products[$i]["name"];
                        $product_Array ["product_image"] = $products[$i]["images"]["small_image"];
                        $product_Array ["amount"] = $products[$i]["custom_denominations"];
                        $product_Array ["min_custom_price"] = $products[$i]["min_custom_price"];
                        $product_Array ["max_custom_price"] = $products[$i]["max_custom_price"];
                        $product_Array ["max_custom_price"] = $products[$i]["max_custom_price"];
                        $product_Array ["voucher_url"] = $products[$i]["navigation_urlpath"];
                        $oauthClient->fetch($murl . "product/" . $products[$i]["id"], array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));

                        $productinfo = json_decode($oauthClient->getLastResponse(), TRUE);
//                            $product_Array ["description"] = $productinfo["description"];
                        $product_Array ["terms_conditions"] = $productinfo["tnc_web"];
                        $product_Array ["vdescription"] = $productinfo["description"];

                        $product_Array ["order_handling_charge"] = $productinfo["order_handling_charge"];
                        $product_Array ["voucher_cat_id"] = "";
                        $listofproducts[$i] = $product_Array;
                    }


                    $file = "files/products_1.txt";
                    $results = json_encode($listofproducts);

                    file_put_contents($file, $results);
                }
//                }
            }
        } catch (OAuthException $e) {
            print_r($e->getMessage());
            echo "<br/>";
            print_r($e->lastResponse);
//              
        }
    }

//=====================================================================================================================================================================================
    public function send_flower_to_cart($is_edit) {
        $flower_cart = array();

        if (count($_POST) > 0) {
// take all the value from the post.
            $flower_cart["delivery_address_line1"] = $this->input->post("delivery_address_line1");

            $flower_cart["delivery_address_line2"] = $this->input->post("delivery_address_line2");

            $flower_cart["delivery_city"] = trim(str_replace('+', '', $this->input->post("delivery_city")));

            $flower_cart["delivery_date"] = $this->input->post("delivery_date");

            $flower_cart["delivery_pincode"] = $this->input->post("delivery_pincode");

            $flower_cart["delivery_shipping_method"] = str_replace('+', ' ', $this->input->post("delivery_shipping_method"));

            $flower_cart["delivery_time_slot"] = str_replace('+-+', ' ', $this->input->post("delivery_time_slot"));

            $flower_cart["message_on_card"] = $this->input->post("message_on_card");

            $flower_cart["flower_code"] = $this->input->post("flower_code");

            $flower_cart["flower_id"] = $this->input->post("flower_id");

            $flower_cart["flower_image"] = $this->input->post("flower_image");

            $flower_cart["flower_name"] = $this->input->post("flower_name");

            $flower_cart["flower_price"] = $this->input->post("flower_price");

            $flower_cart["selected_amount_new"] = $this->input->post("flower_price");

            $flower_cart["guest_session"] = $this->input->post("guest_session");

            $flower_cart["is_guest"] = $this->session->userdata['userdata']['is_guest'];

            $flower_cart["receiver_email"] = $this->input->post("receiver_email");

            $flower_cart["receiver_name"] = $this->input->post("receiver_name");

            $flower_cart["receiver_phone"] = $this->input->post("receiver_phone");

            $flower_cart["redirect"] = $this->input->post("redirect");

            $flower_cart["timezone"] = $this->input->post("timezone");

            $flower_cart["quantity"] = 1;

            $flower_cart["unique_id"] = substr(hash('sha256', mt_rand() . microtime()), 0, 5);

            $user_id = $this->session->userdata["userdata"]["user_id"];
            $first_name = $this->session->userdata["userdata"]["first_name"];
            $email = $this->session->userdata["userdata"]["email"];
            $mobile_no = $this->session->userdata["userdata"]["mobile_no"];

            $flower_cart["user_id"] = $user_id;
            $flower_cart["sender_name"] = $first_name;
            $flower_cart["sender_email"] = $email;
            $flower_cart["sender_phone"] = $mobile_no;

            if ($is_edit == "0") {
// add session detials of the flower.
                if (empty($this->session->userdata['userdata']['flower_data'])) {
                    $this->session->userdata['userdata']['flower_data'] = array();
                    $this->session->userdata['userdata']['flower_data'][] = $flower_cart;
                } else {
                    $this->session->userdata['userdata']['flower_data'][] = $flower_cart;
                }
            } else {
//Edit Details
                $flower_data = $this->session->userdata["userdata"]["flower_data"];
                $flower_data_unique_array = array_column($flower_data, 'unique_id');
                if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
                    $order_id = $is_edit;
                    $flower_data_index = array_search("$order_id", $flower_data_unique_array, TRUE);
                    $flower_cart["quantity"] = $this->session->userdata["userdata"]["flower_data"][$flower_data_index]["quantity"];
                    $flower_cart["unique_id"] = $is_edit;
                    $flower_cart["guest_session"] = $this->session->userdata["userdata"]["flower_data"][$flower_data_index]["guest_session"];
                    $flower_cart["is_guest"] = $this->session->userdata["userdata"]["flower_data"][$flower_data_index]["is_guest"];
                    $flower_cart["selected_amount_new"] = $this->session->userdata["userdata"]["flower_data"][$flower_data_index]["selected_amount_new"];
                    $this->session->userdata["userdata"]["flower_data"][$flower_data_index] = $flower_cart;
                } else {
                    $data['edit_flower_data'] = array();
                }
            }
            $output["success"] = true;
            $output["redirect"] = $flower_cart["redirect"];

//            echo '<pre>';
//            print_r($this->session->all_userdata());
//            exit;
        } else {
            $output["success"] = false;
            $output["message"] = "No input found";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function send_voucher_to_cart() {
        $voucher_cart = array();

        if (count($_POST) > 0) {

            if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
// take all the value from the post.
                $quantity = $this->input->post("quantity_new");
                $redirect = $this->input->post("redirect");
                $is_guest = check_post($this->input->post("is_guest"));
                $timezone = check_post($this->input->post("timezone"));
                $type = check_post($this->input->post("type"));
                $receiver_name = check_post($this->input->post("fname"));
                $receiver_email = check_post($this->input->post("femail"));
                $receiver_phone = check_post($this->input->post("fphone"));
                $greeting_message = check_post($this->input->post("greeting_message"));
                $selected_amount = check_post($this->input->post("selected_amount"));
                $guest_session = check_post($this->input->post("guest_session"));
                $greeting_id = check_post($this->input->post("greeting_id"));
                $delivery_date_time = check_post($this->input->post("delivery_date_time"));

                $uploaded_img = check_post($this->input->post("uploaded_finalimg"));

                $user_id = $this->session->userdata["userdata"]["user_id"];
                $first_name = $this->session->userdata["userdata"]["first_name"];
                $email = $this->session->userdata["userdata"]["email"];
                $mobile_no = $this->session->userdata["userdata"]["mobile_no"];
//                $voucher_pro_id = $this->session->userdata("voucher_pro_id");//Old
                $voucher_pro_id = $this->session->userdata["product_data"]["voucher_pro_id"];
                $voucher_img = $this->session->userdata["product_data"]["product_image"];
//                $terms = $this->session->userdata("terms_conditions");//Old
                $terms = $this->session->userdata["product_data"]["terms_conditions"];
                $voucher_pro_name = $this->session->userdata["product_data"]["voucher_pro_name"];

                $voucher_cart["voucher_pro_id"] = $voucher_pro_id;
                $voucher_cart["voucher_pro_name"] = $voucher_pro_name;
                $voucher_cart["selected_amount"] = $selected_amount;
                $voucher_cart["selected_amount_new"] = $selected_amount;
                $voucher_cart["greeting_id"] = $greeting_id;

                $where = "card_id = $greeting_id";
                $greeting_data = $this->Birthday->getSelectData('front_page_image', 'greeting_card', $where)->row_array();
                $voucher_cart['front_page_image'] = $greeting_data['front_page_image'];

                $voucher_cart["greeting_message"] = $greeting_message;
//                $voucher_cart["vtitle"] = "";
                $voucher_cart["fname"] = $receiver_name;
                $voucher_cart["femail"] = $receiver_email;
                $voucher_cart["fphone"] = $receiver_phone;
                $voucher_cart["uemail"] = $email;
                $voucher_cart["uname"] = $first_name;
                $voucher_cart["selected_orders"] = 0;
                $voucher_cart["unique_oid"] = "";
                $voucher_cart["order_id"] = "";
                $voucher_cart["voucher_img"] = $voucher_img;


                $triggerOn = date('Y-m-d H:i:s', strtotime($delivery_date_time));
                $sdate = date("Y-m-d", strtotime($triggerOn));
                $datetime1 = new DateTime($sdate);
                $datetime2 = new DateTime(date("Y-m-d"));
                $interval = $datetime1->diff($datetime2);
                $date_diff = $interval->format('%a');
                switch ($date_diff) {
                    case 0:
                        $delievery_schedule = 1;
                        break;
                    case 1:
                        $delievery_schedule = 2;
                        break;
                    default:
                        $delievery_schedule = 3;
                        break;
                }
                $voucher_cart["delievery_schedule"] = $delievery_schedule;

                $voucher_cart["user_id"] = $user_id;
                if ($is_guest == 1) {
                    $voucher_cart["uguest_user_id"] = $user_id;
                } else {
                    $voucher_cart["uguest_user_id"] = 0;
                }
                $voucher_cart["guest_session"] = $guest_session;
                $voucher_cart["payment_id"] = 0;


                $uploadedpic = str_replace('public/imageVideo', '', $uploaded_img);
                $voucher_cart["uploded_img_video"] = $uploadedpic;

                $voucher_cart["created_date"] = date("Y-m-d H:i:s");

// $returndata = DateConversion($delievery_schedule, $delivery_date, $delivery_time, $timezone);
                $returndata = DateConversion_new($delivery_date_time, $timezone);
                $voucher_cart["delivery_date_time"] = $returndata["delivery_date_time"];
                $voucher_cart["delivery_status"] = 0;
                $voucher_cart["type"] = $type;
                $voucher_cart["quantity"] = $quantity;
                $voucher_cart["same_quantity_id"] = "";
//                $voucher_cart["updated_date"] = "";
                $voucher_cart["order_message"] = "";
                $voucher_cart["woohoo_order_id"] = "";
                $voucher_cart["terms"] = $terms;
                $voucher_cart["mail_open_status"] = 0;
                $voucher_cart["payment_mail_status"] = 1;
                $voucher_cart["egift_order_mail_status"] = 1;
                $voucher_cart["timezone"] = $timezone;
                $voucher_cart["voucher_info_link"] = "";
                $voucher_cart["converted_date"] = $returndata["converted_date"];
                $voucher_cart["unique_id"] = substr(hash('sha256', mt_rand() . microtime()), 0, 5);

//                if ($is_edit == "0") {
// add session detials of the flower.
                if (empty($this->session->userdata['userdata']['voucher_data'])) {
                    $this->session->userdata['userdata']['voucher_data'] = array();
                    $this->session->userdata['userdata']['voucher_data'][] = $voucher_cart;
                } else {
                    $this->session->userdata['userdata']['voucher_data'][] = $voucher_cart;
                }
//                } else {
//Edit Details
//                    $flower_data = $this->session->userdata["userdata"]["voucher_data"];
//                    $flower_data_unique_array = array_column($flower_data, 'unique_id');
//                    if ($this->check_logged_in() || $this->check_guest_logged_in()) {
//                        $order_id = $is_edit;
//                        $flower_data_index = array_search("$order_id", $flower_data_unique_array, TRUE);
//                        $voucher_cart["quantity"] = $this->session->userdata["userdata"]["voucher_data"][$flower_data_index]["quantity"];
//                        $voucher_cart["unique_id"] = $is_edit;
//                        $voucher_cart["guest_session"] = $this->session->userdata["userdata"]["voucher_data"][$flower_data_index]["guest_session"];
//                        $voucher_cart["is_guest"] = $this->session->userdata["userdata"]["voucher_data"][$flower_data_index]["is_guest"];
//                        $voucher_cart["selected_amount_new"] = $this->session->userdata["userdata"]["voucher_data"][$flower_data_index]["selected_amount_new"];
//                        $this->session->userdata["userdata"]["flower_data"][$flower_data_index] = $voucher_cart;
//                    } else {
//                        $data['edit_flower_data'] = array();
//                    }
//                }
                $output["success"] = true;
                $output["redirect"] = $redirect;

//                echo '<pre>';
//                print_r($this->session->all_userdata());
//                exit;
            } else {
                $output["success"] = false;
//                $output["message"] = "Sorry your session has been expired.Please login again to send eGift!";
                $output["message"] = "Sorry your session has been expired.Please login again!";
            }
        } else {
            $output["success"] = false;
            $output["message"] = "No input found";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function delete_flower_cart() {
        $postcount = count($_POST);
        $flower_data = $this->session->userdata["userdata"]["flower_data"];
        $flower_data_unique_array = array_column($flower_data, 'unique_id');
        if ($postcount > 0) {
            if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
                $order_id = check_post($this->input->post("same_quantity_id"));
                $flower_data_index = array_search("$order_id", $flower_data_unique_array, TRUE);
                unset($this->session->userdata["userdata"]["flower_data"][$flower_data_index]);
                $this->session->userdata["userdata"]["flower_data"] = array_values($this->session->userdata["userdata"]["flower_data"]);
                $output["success"] = true;
                $output["message"] = "Removed successfully";
            } else {
                $output["success"] = false;
//                $output["message"] = "Sorry your session has been expired.Please login again to send eGift!";
                $output["message"] = "Sorry your session has been expired.Please login again!";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function delete_voucher_cart() {
        $postcount = count($_POST);
        $voucher_data = $this->session->userdata["userdata"]["voucher_data"];
        $voucher_data_unique_array = array_column($voucher_data, 'unique_id');
        if ($postcount > 0) {
            if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
                $order_id = check_post($this->input->post("same_quantity_id"));
                $voucher_data_index = array_search("$order_id", $voucher_data_unique_array, TRUE);
                unset($this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]);
                $this->session->userdata["userdata"]["voucher_data"] = array_values($this->session->userdata["userdata"]["voucher_data"]);
                $output["success"] = true;
                $output["message"] = "Removed successfully";
            } else {
                $output["success"] = false;
//                $output["message"] = "Sorry your session has been expired.Please login again to send eGift!";
                $output["message"] = "Sorry your session has been expired.Please login again!";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function update_flower_cart() {
        $postcount = count($_POST);
        $flower_data = $this->session->userdata["userdata"]["flower_data"];
        $flower_data_unique_array = array_column($flower_data, 'unique_id');
        if ($postcount > 0) {
            if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
                $order_id = check_post($this->input->post("same_quantity_id"));
                $quantity = check_post($this->input->post("quantity"));
                $flower_data_index = array_search("$order_id", $flower_data_unique_array, TRUE);
                $flower_data_quantity_value = $this->session->userdata["userdata"]["flower_data"][$flower_data_index]["quantity"] = $quantity;
                $flower_price = $this->session->userdata["userdata"]["flower_data"][$flower_data_index]["flower_price"];
                $flower_selected_amount_new_value = $this->session->userdata["userdata"]["flower_data"][$flower_data_index]["selected_amount_new"] = $flower_price * $quantity;
                $output["success"] = true;
                $output["message"] = "Update successfully";
            } else {
                $output["success"] = false;
//                $output["message"] = "Sorry your session has been expired.Please login again to send eGift!";
                $output["message"] = "Sorry your session has been expired.Please login again!";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function update_voucher_cart() {
        $postcount = count($_POST);
        $voucher_data = $this->session->userdata["userdata"]["voucher_data"];
        $voucher_data_unique_array = array_column($voucher_data, 'unique_id');
        if ($postcount > 0) {
            if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
                $order_id = check_post($this->input->post("same_quantity_id"));
                $quantity = check_post($this->input->post("quantity"));

//                echo $quantity;
//                print_r($this->session->all_userdata());
//                exit;
                $voucher_data_index = array_search("$order_id", $voucher_data_unique_array, TRUE);
                $voucher_data_quantity_value = $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["quantity"] = $quantity;
                $voucher_price = $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["selected_amount"];
                $voucher_selected_amount_new_value = $this->session->userdata["userdata"]["voucher_data"][$voucher_data_index]["selected_amount_new"] = $voucher_price * $quantity;
//                echo '<pre>';
//                print_r($this->session->all_userdata());
//                exit;
                $output["success"] = true;
                $output["message"] = "Update successfully";
            } else {
                $output["success"] = false;
//                $output["message"] = "Sorry your session has been expired.Please login again to send eGift!";
                $output["message"] = "Sorry your session has been expired.Please login again!";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function show_cart() {
        $is_purchased = 0;
        if ($this->check_logged_in() || $this->check_guest_logged_in() || $this->check_google_logged_in()) {
            $data = array();
            $data["product_info"] = "";
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $isguest = $this->session->userdata["userdata"]["is_guest"];
//Question=================================
            if ($isguest == 1) {
                $guestsession = $this->Birthday->getSelectData("user_id,uguest_user_id,guest_session", "order_products", "user_id='$user_id'")->row_array();
                $guest_session = $guestsession["guest_session"];
                $guest_users = $this->Birthday->getSelectData("user_id,uguest_user_id,guest_session", "order_products", "guest_session='$guest_session'")->result_array();
                if (count($guest_users) > 0) { // try to watch this
                    $updata = array();
                    for ($i = 0; $i < count($guest_users); $i++) {
                        $updateArray[] = array(
                            'guest_session' => $guest_session,
                            'user_id' => $user_id
                        );
                    }
                    $this->Birthday->updateBatch("order_products", $updateArray, "guest_session");
                }
            }
//=======================================


            $data['vouchers'] = array();   //voucher is key for both flower and voucher so for view we only send $voucher array
            $data["udf2"] = $data["user_id"] = $user_id;  //need to check if user id is available for flower
// need to consider the upper onece again
//for voucher
//            $voucher = $this->Birthday->getJoinDataGroupBy("u.is_guest,o.order_pro_id,o.voucher_pro_id,o.payment_id,o.same_quantity_id,o.quantity,o.order_id,o.voucher_pro_name,o.unique_oid,o.selected_orders,o.delievery_schedule,o.delivery_date_time,o.selected_amount,o.voucher_img,o.femail,o.fname,o.fphone,g.front_page_image ,sum(o.selected_amount) as selected_amount_new", "order_products o", "greeting_card g", "users u", "g.card_id=o.greeting_id", "u.user_id=o.user_id", "o.user_id=$user_id and o.selected_orders=0 and (o.delivery_status='' or delivery_status=3 ) and u.is_guest= $isguest ", "o.same_quantity_id", "`o`.`order_pro_id` ASC")->result_array();
            $voucher = $this->session->userdata["userdata"]["voucher_data"];
//            echo '<pre>';
//            print_r($voucher);
//            exit;
            $count_voucher = count($voucher);

            if ($count_voucher != 0) {

                for ($i = 0; $i < $count_voucher; $i++) {
                    $voucher[$i]['is_flower'] = 0;      // these keys area aded to every voucher comming from the data base
                    $voucher[$i]['is_voucher'] = 1;
                }
                $data["product_info"] = "BirthdayOwl egift vouchers";
                $is_purchased = 1; // only voucher purchased
            }

//for flower
//            if (count($this->session->userdata["userdata"]["flower_data"]) != 0) {
            $flower = $this->session->userdata["userdata"]["flower_data"];  // take this flower data when similar to voucher on view side
//            } else {
//                $flower = array();
//            }

            $count_flower = count($flower);

            if ($count_flower != 0) {
                for ($j = 0; $j < $count_flower; $j++) {
                    $flower[$j]['is_flower'] = 1;      // these keys area aded to every voucher comming from the data base
                    $flower[$j]['is_voucher'] = 0;
                }

                $data["product_info"] = "BirthdayOwl flower";
//                $data["udf2"] = $data["user_id"] = $user_id;
                $is_purchased = 2; // only flower purchased
            }

            if (!empty($flower) && !empty($voucher)) {
                $total_count = $count_voucher + $count_flower;
                $data['gift_count'] = $total_count;
                $data['vouchers'] = array_merge($voucher, $flower);
                $data["product_info"] = "BirthdayOwl voucher and flower";
            } else {
                if ($is_purchased == 1) {
                    $data['vouchers'] = $voucher;
                    $data['gift_count'] = $count_voucher;
                }

                if ($is_purchased == 2) {
                    $data['vouchers'] = $flower;
                    $data['gift_count'] = $count_flower;
                }
            }

            $user_info = $this->Birthday->getSelectData("email,first_name,mobile_no", "users", "user_id='$user_id'")->row_array(); // need to check if user is added in table or session
            $data["user_email"] = $user_info["email"];
            $data["user_first_name"] = $user_info["first_name"];
            $data["uphone"] = $user_info["mobile_no"];

            $data["MERCHANT_KEY"] = MERCHANT_KEY;
            $SALT = PSALT;
            $PAYU_BASE_URL = PAYU_BASE_URL;
            $data["action"] = "";
            $data["hash"] = "";
            $data["amount"] = "";
            $data["firstname"] = "";
            $data["email"] = "";
            $data["productinfo"] = "";
            $data["phone"] = "";
            $data["surl"] = "";
            $data["furl"] = "";
            $posted = array();

            if (!empty($_POST)) {
                foreach ($_POST as $key => $value) {
                    $posted[$key] = $value;
                    $data[$key] = $value;
                }
            }
            $data["formError"] = 0;
            if (empty($posted['txnid'])) {
// Generate random transaction id
                $data["txnid"] = $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            } else {
                $data["txnid"] = $txnid = $posted['txnid'];
            }

// the transaction id is not the value of the 

            $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
            if (empty($posted['hash']) && sizeof($posted) > 0) {
                if (empty($posted['key']) || empty($posted['txnid']) || empty($posted['amount']) || empty($posted['firstname']) || empty($posted['email']) || empty($posted['productinfo']) || empty($posted['surl']) || empty($posted['furl']) || empty($posted['service_provider'])) {
                    $data["formError"] = 1;
                } else {
                    $hashVarsSeq = explode('|', $hashSequence);
                    $hash_string = '';
                    foreach ($hashVarsSeq as $hash_var) {
                        $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                        $hash_string .= '|';
                    }
                    $hash_string .= $SALT;

                    $data["hash"] = $hash = strtolower(hash('sha512', $hash_string));


                    $data["action"] = $PAYU_BASE_URL . '/_payment';
                }
            } elseif (!empty($posted['hash'])) {
                $data["hash"] = $posted['hash'];

                $data["action"] = $PAYU_BASE_URL . '/_payment';
            }
            $data["voucherNames"] = $this->getVoucherNames();
            $data["testimonials"] = $test = $this->getTestimonials();
            $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=7")->row_array();
            $data["seo"] = $title;
            $this->load->view('EGIFT/my_cart', $data);
        } else {
//            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again to send eGift!");
            $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again!");
            redirect(WEB_HOME);
        }
    }

    public function get_cart_count() {
        $total = 0;
        $is_purchased = 0;
        $output["carts"] = array();
        if ($this->check_logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
//Old Noraml user login Query for retriving cart count
//$this->Birthday->getJoinDataGroupBy("u.is_guest,o.order_pro_id,o.voucher_pro_id,o.payment_id,o.same_quantity_id,o.quantity,o.order_id,o.voucher_pro_name,o.unique_oid,o.selected_orders,o.delievery_schedule,o.delivery_date_time,o.selected_amount,o.voucher_img,o.femail,o.fname,o.fphone,g.front_page_image ,sum(o.selected_amount) as selected_amount_new", "order_products o", "greeting_card g", "users u", "g.card_id=o.greeting_id", "u.user_id=o.user_id", "o.user_id=$user_id and o.selected_orders=0 and (o.delivery_status='' or delivery_status=3) and u.is_guest=0 ", "o.same_quantity_id", "`o`.`order_pro_id` ASC")->result_array();
//for voucher
//            $voucher = $this->Birthday->getJoinDataGroupBy("u.is_guest,o.order_pro_id,o.voucher_pro_id,o.payment_id,o.same_quantity_id,o.quantity,o.order_id,o.voucher_pro_name,o.unique_oid,o.selected_orders,o.delievery_schedule,o.delivery_date_time,o.selected_amount,o.voucher_img,o.femail,o.fname,o.fphone,g.front_page_image ,sum(o.selected_amount) as selected_amount_new", "order_products o", "greeting_card g", "users u", "g.card_id=o.greeting_id", "u.user_id=o.user_id", "o.user_id=$user_id and o.selected_orders=0 and (o.delivery_status='' or delivery_status=3) and u.is_guest=0 ", "o.same_quantity_id", "`o`.`order_pro_id` ASC")->result_array();
            $voucher = $this->session->userdata["userdata"]["voucher_data"];

            $count_voucher = count($voucher);

            if ($count_voucher != 0) {
                for ($i = 0; $i < $count_voucher; $i++) {
                    $voucher[$i]['is_flower'] = 0;      // these keys area aded to every voucher comming from the data base
                    $voucher[$i]['is_voucher'] = 1;
                    $greeting_id = $voucher[$i]['greeting_id'];
//                    $where = "card_id = $greeting_id";
//                    $greeting_data = $this->Birthday->getSelectData('front_page_image', 'greeting_card', $where)->row_array();
//                    $voucher[$i]['front_page_image'] = $greeting_data['front_page_image'];
                }
                $is_purchased = 1; // only voucher purchased
            }

//for flower
            if (count($this->session->userdata["userdata"]["flower_data"]) != 0) {
                $flower = $this->session->userdata["userdata"]["flower_data"];  // take this flower data when similar to voucher on view side
            } else {
                $flower = array();
            }
            $count_flower = count($flower);
            if ($count_flower != 0) {
                for ($j = 0; $j < $count_flower; $j++) {
                    $flower[$j]['is_flower'] = 1;      // these keys area aded to every voucher comming from the data base
                    $flower[$j]['is_voucher'] = 0;
                }
                $is_purchased = 2; // only flower purchased
            }

            if (!empty($flower) && !empty($voucher)) {
                $output["carts"] = array_merge($voucher, $flower);
            } else {
                if ($is_purchased == 1) {
                    $output["carts"] = $voucher;
                }

                if ($is_purchased == 2) {
                    $output["carts"] = $flower;
                }
            }
            $output["carts"] = array_values($output["carts"]);
//            $output["carts"] = $cart_count = $this->Birthday->getJoinDataGroupBy("u.is_guest,o.order_pro_id,o.voucher_pro_id,o.payment_id,o.same_quantity_id,o.quantity,o.order_id,o.voucher_pro_name,o.unique_oid,o.selected_orders,o.delievery_schedule,o.delivery_date_time,o.selected_amount,o.voucher_img,o.femail,o.fname,o.fphone,g.front_page_image ,sum(o.selected_amount) as selected_amount_new", "order_products o", "greeting_card g", "users u", "g.card_id=o.greeting_id", "u.user_id=o.user_id", "o.user_id=$user_id and o.selected_orders=0 and (o.delivery_status='' or delivery_status=3) and u.is_guest=0 ", "o.same_quantity_id", "`o`.`order_pro_id` ASC")->result_array();
        } else if ($this->check_guest_logged_in()) {
            $user_id = $this->session->userdata["userdata"]["user_id"];
            $guest_session = check_post($this->input->post("guest_session"));
//Question=========================
            $guest_users = $this->Birthday->getSelectData("user_id,uguest_user_id,guest_session", "order_products", "guest_session='$guest_session'")->result_array();
            $gecount = count($guest_users);
            if ($gecount > 0) {
                $updata = array();
                for ($i = 0; $i < $gecount; $i++) {
                    $updateArray[] = array(
                        'guest_session' => $guest_session,
                        'user_id' => $user_id
                    );
                }
                $this->Birthday->updateBatch("order_products", $updateArray, "guest_session");
            }
//==========================
//Old guest Query for retriving cart count
//            $output["carts"] = $cart_count = $this->Birthday->getJoinDataGroupBy("u.is_guest,o.order_pro_id,o.voucher_pro_id,o.payment_id,o.same_quantity_id,o.quantity,o.order_id,o.voucher_pro_name,o.unique_oid,o.selected_orders,o.delievery_schedule,o.delivery_date_time,o.selected_amount,o.voucher_img,o.femail,o.fname,o.fphone,g.front_page_image ,sum(o.selected_amount) as selected_amount_new", "order_products o", "greeting_card g", "users u", "g.card_id=o.greeting_id", "u.user_id=o.user_id", "o.user_id=$user_id and o.selected_orders=0 and (o.delivery_status='' or delivery_status=3) and u.is_guest=1 ", "o.same_quantity_id", "`o`.`order_pro_id` ASC")->result_array();
//for voucher
//            $voucher = $this->Birthday->getJoinDataGroupBy("u.is_guest,o.order_pro_id,o.voucher_pro_id,o.payment_id,o.same_quantity_id,o.quantity,o.order_id,o.voucher_pro_name,o.unique_oid,o.selected_orders,o.delievery_schedule,o.delivery_date_time,o.selected_amount,o.voucher_img,o.femail,o.fname,o.fphone,g.front_page_image ,sum(o.selected_amount) as selected_amount_new", "order_products o", "greeting_card g", "users u", "g.card_id=o.greeting_id", "u.user_id=o.user_id", "o.user_id=$user_id and o.selected_orders=0 and (o.delivery_status='' or delivery_status=3) and u.is_guest=1 ", "o.same_quantity_id", "`o`.`order_pro_id` ASC")->result_array();
            $voucher = $this->session->userdata["userdata"]["voucher_data"];
//            print_r($voucher);
//            exit;
            $count_voucher = count($voucher);

            if ($count_voucher != 0) {
                for ($i = 0; $i < $count_voucher; $i++) {
                    $voucher[$i]['is_flower'] = 0;      // these keys area aded to every voucher comming from the data base
                    $voucher[$i]['is_voucher'] = 1;
                }
                $is_purchased = 1; // only voucher purchased
            }

//for flower
            if (count($this->session->userdata["userdata"]["flower_data"]) != 0) {
                $flower = $this->session->userdata["userdata"]["flower_data"];  // take this flower data when similar to voucher on view side
            } else {
                $flower = array();
            }
            $count_flower = count($flower);
            if ($count_flower != 0) {
                for ($j = 0; $j < $count_flower; $j++) {
                    $flower[$j]['is_flower'] = 1;      // these keys area aded to every voucher comming from the data base
                    $flower[$j]['is_voucher'] = 0;
                }
                $is_purchased = 2; // only flower purchased
            }

            if (!empty($flower) && !empty($voucher)) {
                $output["carts"] = array_merge($voucher, $flower);
            } else {
                if ($is_purchased == 1) {
                    $output["carts"] = $voucher;
                }

                if ($is_purchased == 2) {
                    $output["carts"] = $flower;
                }
            }
            $output["carts"] = array_values($output["carts"]);
        } else {
            $output["carts"] = array();
        }

        if (count($output["carts"]) > 0) {
            foreach ($output["carts"] as $carts) {
                $total += $carts["selected_amount_new"];
            }
        }
        $processing_fee = round(($total * 0.035), 2);
        $total_all = ($total + $processing_fee);
        $output["total"] = $total_all;
        $output["cart_count"] = count($output["carts"]);
        $output["message"] = "";
        $output["success"] = true;
//        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function thanx_for_purchase() {
        $postcount = count($_POST);
        if ($postcount > 0) {
//            if ($this->check_logged_in()) {
            $orders = array();
            $payment["payment_mode"] = $update_pay["payment_mode"] = $payment_for_voucher_mode = $payment_for_flower_mode = $_POST["mode"];
            $payment["transaction_status"] = $update_pay["transaction_status"] = $payment_for_voucher_status = $payment_for_flower_status = $status = $_POST["status"];
            $payment["bill_to_fname"] = $firstname = $update_pay["bill_to_fname"] = $payment_for_voucher_fname = $payment_for_flower_fname = $_POST["firstname"];
            $payment["total_amount"] = $amount = $payment_for_voucher_total_amount_voucher = $payment_for_flower_total_amount_flower = $_POST["amount"];
            $payment["transaction_id"] = $update_pay["transaction_id"] = $txnid = $payment_for_voucher_txnid = $payment_for_flower_txnid = $_POST["txnid"];
            $payment["bill_to_email"] = $email = $update_pay["bill_to_email"] = $payment_for_voucher_email = $payment_for_flower_email = $_POST["email"];
            $payment["purchased_date"] = $update_pay["purchased_date"] = $payment_for_voucher_purchased_date = $payment_for_flower_purchased_date = $_POST["addedon"];
            $payment["payuId"] = $payment_for_voucher_payuid = $payment_for_flower_payuid = $_POST["payuMoneyId"];
            $mode = $_POST["mode"];
            $bankcode = $_POST["bankcode"];
// the above is for payment related value

            $data["processing_fee"] = $udf1 = $_POST["udf1"];
            $posted_hash = $_POST["hash"];
            $key = $_POST["key"];
            $productinfo = $_POST["productinfo"];

// the below orders data will be added to the orders table.
            $orders["status"] = "1";
            $orders["user_id"] = $data["user_id"] = $user_id = $udf2 = $_POST["udf2"];
            $orders["order_created_date"] = date("Y-m-d H:i:s");
            $order_id = $this->Birthday->addData("orders", $orders);
            $orderid = $this->Birthday->encryptPassword($order_id);
            $salt = PSALT;
            //////////////////////////////////////////////audit for voucher code 
            $voucherdataInsession = $this->session->userdata['userdata']['voucher_data'];
            $this->load->library('ThanksForPurchaseAuditCreator');
            $setaudit = new ThanksForPurchaseAuditCreator();
            $setaudit->createAuditArray($payment, $user_id, $productinfo, $voucherdataInsession);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////            

            If (isset($_POST["additionalCharges"])) {
                $additionalCharges = $_POST["additionalCharges"];
                $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

//                $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
            } else {
                $retHashSeq = $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

//                $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
            }

            $hash = hash("sha512", $retHashSeq);
            $what_was_transaction = $this->what_was_transaction_for($productinfo);
            $what_was_transaction_encrypt = $this->Birthday->encryptPassword($what_was_transaction);

            //now test if voucher and flower data session is not empty
            ////////////////////////////////////////////////////////////////////////Total purchase validation/////////////////////////////////////////////////////////////////
//            $amount = 10;  //for manually testing wrong value. this amount value gets overrided by the amount value coming from the top.
            $this->load->library("TotalPurchase"); //this class will validate the amount of value purchased
            $totalpurchase = new TotalPurchase();
            $totalpurchase->setTotalValueOfProductPayU($amount);
            $is_amountValid = $totalpurchase->runner();
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if ($what_was_transaction == 1 || $what_was_transaction == 3) {
// voucher was there in the production.
                $add_to_payment_table = array();
                $add_to_order_products_table = array();

                $voucher_purchased_array = $this->session->userdata['userdata']['voucher_data'];
                $is_guest = $this->session->userdata['userdata']['is_guest'];

                $number_of_voucher_purchased = count($voucher_purchased_array);

// this loop will add the voucher details of payment into the table payment
                if ($hash != $posted_hash) {
                    for ($i = 0; $i < $number_of_voucher_purchased; $i++) {
                        $add_to_payment_table[$i]["total_amount"] = $payment_for_voucher_total_amount_voucher;
                        $add_to_payment_table[$i]["user_id"] = $user_id;
                        $add_to_payment_table[$i]["product_id"] = $voucher_purchased_array[$i]['voucher_pro_id'];
                        $add_to_payment_table[$i]["order_id"] = $order_id;
                        $add_to_payment_table[$i]["transaction_id"] = $payment_for_voucher_txnid;
                        $add_to_payment_table[$i]["transaction_status"] = $payment_for_voucher_status;
                        $add_to_payment_table[$i]["bill_to_fname"] = $payment_for_voucher_fname;
                        $add_to_payment_table[$i]["bill_to_email"] = $payment_for_voucher_email;
                        $add_to_payment_table[$i]["payment_mode"] = $payment_for_voucher_mode;
                        $add_to_payment_table[$i]["payuId"] = $payment_for_voucher_payuid;
                        $add_to_payment_table[$i]["purchased_date"] = $payment_for_voucher_purchased_date;

                        $this->db->insert('payment', $add_to_payment_table[$i]);
                        $inserted_payment_id = $this->db->insert_id();
//now lets add the flower to th flower table if it is not sucessfull
// use the same loop to put data into the table.
                    }
                    redirect(BASEURL . "payment-failed/" . $orderid); // when the transaction fails.....
                } else if (!$is_amountValid) {
                    for ($i = 0; $i < $number_of_voucher_purchased; $i++) {
                        $add_to_payment_table[$i]["total_amount"] = $payment_for_voucher_total_amount_voucher;
                        $add_to_payment_table[$i]["user_id"] = $user_id;
                        $add_to_payment_table[$i]["product_id"] = $voucher_purchased_array[$i]['voucher_pro_id'];
                        $add_to_payment_table[$i]["order_id"] = $order_id;
                        $add_to_payment_table[$i]["transaction_id"] = $payment_for_voucher_txnid;
                        $add_to_payment_table[$i]["transaction_status"] = $payment_for_voucher_status . " " . "Amount Altered";
                        $add_to_payment_table[$i]["bill_to_fname"] = $payment_for_voucher_fname;
                        $add_to_payment_table[$i]["bill_to_email"] = $payment_for_voucher_email;
                        $add_to_payment_table[$i]["payment_mode"] = $payment_for_voucher_mode;
                        $add_to_payment_table[$i]["payuId"] = $payment_for_voucher_payuid;
                        $add_to_payment_table[$i]["purchased_date"] = $payment_for_voucher_purchased_date;

                        $this->db->insert('payment', $add_to_payment_table[$i]);
                        $inserted_payment_id = $this->db->insert_id();
                    }
//now lets add the flower to th flower table if it is not sucessfull
// use the same loop to put data into the table.
                    redirect(BASEURL . "payment-failed/" . $orderid); // when the transaction fails.....
                } else if ($number_of_voucher_purchased == 0) {

                    $add_to_payment_table["total_amount"] = $payment_for_voucher_total_amount_voucher;
                    $add_to_payment_table["user_id"] = $user_id;
                    $add_to_payment_table["product_id"] = 0;
                    $add_to_payment_table["order_id"] = $order_id;
                    $add_to_payment_table["transaction_id"] = $payment_for_voucher_txnid;
                    $add_to_payment_table["transaction_status"] = $payment_for_voucher_status . " " . "session cleared";
                    $add_to_payment_table["bill_to_fname"] = $payment_for_voucher_fname;
                    $add_to_payment_table["bill_to_email"] = $payment_for_voucher_email;
                    $add_to_payment_table["payment_mode"] = $payment_for_voucher_mode;
                    $add_to_payment_table["payuId"] = $payment_for_voucher_payuid;
                    $add_to_payment_table["purchased_date"] = $payment_for_voucher_purchased_date;

                    $this->db->insert('payment', $add_to_payment_table);
                    $inserted_payment_id = $this->db->insert_id();

                    redirect(BASEURL . "payment-failed/" . $orderid); // when the transaction fails.....
                } else {
                    if ($status == 'success') {
                        for ($j = 0; $j < $number_of_voucher_purchased; $j++) {
                            // to find if there are multiple quantity of the same voucher
                            $current_voucher_total_quantity = (int) $voucher_purchased_array[$j]["quantity"];
                            $current_voucher_total_value = (int) $voucher_purchased_array[$j]['selected_amount_new'];
                            $single_voucher_price = $current_voucher_total_value / $current_voucher_total_quantity;

                            //this loop will run based on the quantity value
                            for ($q = 0; $q < $current_voucher_total_quantity; $q++) {
                                //adding in the payment table
                                $add_to_payment_table[$j]["total_amount"] = $payment_for_voucher_total_amount_voucher;
                                $add_to_payment_table[$j]["user_id"] = $user_id;
                                $add_to_payment_table[$j]["product_id"] = $voucher_purchased_array[$j]['voucher_pro_id'];
                                $add_to_payment_table[$j]["order_id"] = $order_id;
                                $add_to_payment_table[$j]["transaction_id"] = $payment_for_voucher_txnid;
                                $add_to_payment_table[$j]["transaction_status"] = $payment_for_voucher_status;
                                $add_to_payment_table[$j]["bill_to_fname"] = $payment_for_voucher_fname;
                                $add_to_payment_table[$j]["bill_to_email"] = $payment_for_voucher_email;
                                $add_to_payment_table[$j]["payment_mode"] = $payment_for_voucher_mode;
                                $add_to_payment_table[$j]["payuId"] = $payment_for_voucher_payuid;
                                $add_to_payment_table[$j]["purchased_date"] = $payment_for_voucher_purchased_date;

                                $this->db->insert('payment', $add_to_payment_table[$j]);
                                $inserted_payment_id = $this->db->insert_id();

                                //adding the data in the voucher_product table
                                $add_to_order_products_table[$j]["voucher_pro_id"] = $voucher_purchased_array[$j]['voucher_pro_id'];
                                $add_to_order_products_table[$j]["voucher_pro_name"] = $voucher_purchased_array[$j]['voucher_pro_name'];
//                                $add_to_order_products_table[$j]["selected_amount"] = $voucher_purchased_array[$j]['selected_amount_new'];
                                $add_to_order_products_table[$j]["selected_amount"] = $single_voucher_price;
                                $add_to_order_products_table[$j]["greeting_id"] = $voucher_purchased_array[$j]['greeting_id'];
                                $add_to_order_products_table[$j]["greeting_message"] = $voucher_purchased_array[$j]['greeting_message'];
                                $add_to_order_products_table[$j]["fname"] = $voucher_purchased_array[$j]['fname'];
                                $add_to_order_products_table[$j]["femail"] = $voucher_purchased_array[$j]['femail'];
                                $add_to_order_products_table[$j]["fphone"] = $voucher_purchased_array[$j]['fphone'];
                                $add_to_order_products_table[$j]["uemail"] = $voucher_purchased_array[$j]['uemail'];
                                $add_to_order_products_table[$j]["uname"] = $voucher_purchased_array[$j]['uname'];
                                $add_to_order_products_table[$j]["selected_orders"] = 1;

                                $add_to_order_products_table[$j]["unique_oid"] = "BHOWL00$order_id";

                                $add_to_order_products_table[$j]["order_id"] = $order_id;
                                $add_to_order_products_table[$j]["voucher_img"] = $voucher_purchased_array[$j]['voucher_img'];
                                $add_to_order_products_table[$j]["delievery_schedule"] = $voucher_purchased_array[$j]['delievery_schedule'];
                                $add_to_order_products_table[$j]["user_id"] = $user_id;
                                $add_to_order_products_table[$j]["uguest_user_id"] = $voucher_purchased_array[$j]['uguest_user_id'];
                                $add_to_order_products_table[$j]["guest_session"] = $voucher_purchased_array[$j]['guest_session'];
                                $add_to_order_products_table[$j]["payment_id"] = $inserted_payment_id;
                                $add_to_order_products_table[$j]["uploded_img_video"] = $voucher_purchased_array[$j]['uploded_img_video'];
                                $add_to_order_products_table[$j]["created_date"] = $voucher_purchased_array[$j]['created_date'];
                                $add_to_order_products_table[$j]["delivery_date_time"] = $voucher_purchased_array[$j]['delivery_date_time'];

                                if ($voucher_purchased_array[$j]["delievery_schedule"] == '0') {
                                    $add_to_order_products_table[$j]["delivery_status"] = 1; //payment done and sending egift
                                    $add_to_order_products_table[$j]["order_message"] = "Sending egift...";
                                } else {
                                    if ($voucher_purchased_array[$j]["delievery_schedule"] == '1') {
                                        $delivery_date_time = $voucher_purchased_array[$j]["delivery_date_time"];
                                        $triggerOn = date('Y-m-d H:i:s', strtotime($delivery_date_time));
                                        $date1 = $triggerOn;
                                        $date2 = date("Y-m-d H:i:s");
//Convert them to timestamps.
                                        $date1Timestamp = strtotime($date1);
                                        $date2Timestamp = strtotime($date2);
//Calculate the difference.
                                        $difference = $date1Timestamp - $date2Timestamp;
                                        $minutes = floor($difference / 60);
                                        if ($minutes <= 30) {
                                            $add_to_order_products_table[$j]["delivery_status"] = 1;  // instantly 
                                        } else {
                                            $add_to_order_products_table[$j]["delivery_status"] = 2; // after some while
                                        }
                                        $delieverydate = $voucher_purchased_array[$j]["delivery_date_time"];
                                        $add_to_order_products_table[$j]["order_message"] = "eGift will be delievered on $delieverydate";
                                    } else {
                                        $add_to_order_products_table[$j]["delivery_status"] = 2;
                                        $delieverydate = $voucher_purchased_array[$j]["delivery_date_time"];
                                        $add_to_order_products_table[$j]["order_message"] = "eGift will be delievered on $delieverydate";
                                    }
                                }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                $add_to_order_products_table[$j]["type"] = $voucher_purchased_array[$j]['type'];
                                $add_to_order_products_table[$j]["quantity"] = $voucher_purchased_array[$j]['quantity'];
                                $add_to_order_products_table[$j]["terms"] = $voucher_purchased_array[$j]['terms'];
                                $add_to_order_products_table[$j]["timezone"] = $voucher_purchased_array[$j]['timezone'];
                                $add_to_order_products_table[$j]["converted_date"] = $voucher_purchased_array[$j]['converted_date'];

                                $this->db->insert('order_products', $add_to_order_products_table[$j]);
                                $inserted_voucher_id = $this->db->insert_id();
//                        $payment_data = $this->Birthday->getorderPaymentFlowerData($inserted_flower_id)->row_array();

                                $where = "order_pro_id = $inserted_voucher_id";
                                $update_data["same_quantity_id"] = $inserted_voucher_id;
                                $this->Birthday->update("order_products", $update_data, $where);

                                $payment_data = $this->Birthday->getorderPaymentData($inserted_voucher_id)->row_array();

                                $userinfo = $this->Birthday->getSelectData("email, first_name", "users", "user_id = $user_id")->row_array();
                                $data["uemail"] = $uemail = $userinfo["email"];
                                $data["sender_name"] = $data["uname"] = $uname = $userinfo["first_name"];
                                $data["order_id"] = $order_id;
                                $data["bill_to_name"] = $payment_data["bill_to_fname"];
//                            $data["samount"] = $data["total_amount"] = $amount;
                                $data["payment_mode"] = $payment_data["payment_mode"];
                                $data["purchased_date"] = $payment_data["purchased_date"];
                                $data["voucher_img"] = $payment_data["voucher_img"];
                                $data["greeting_img"] = $payment_data["front_page_image"];
                                $data["voucher_name"] = $payment_data["voucher_pro_name"];
                                $data["fname"] = $payment_data["fname"];
                                $data["selected_amount"] = $payment_data["selected_amount"];
                                $data["femail"] = $payment_data["femail"];
                                $data["fphone"] = $payment_data["fphone"];
                                $data["greetmessage"] = $payment_data["greeting_message"];
                                $data["type"] = $payment_data["type"];
                                $data["media"] = $payment_data["uploded_img_video"];
                                $data["quantity"] = $payment_data["quantity"];
                                $data["processing_fee1"] = round(($data["selected_amount"] * 3.5) / 100, 2);
                                $data["samount"] = $data["total_amount"] = ($payment_data["selected_amount"]) + $data["processing_fee1"];
//$data["samount"] = $amount;
//payment confirmation email
                                try {
                                    $view = $this->load->view("email_template/Payment_received", $data, true);
                                    $response = send_EmailResponse($view, $uemail, $uname, "orders@birthdayowl.com", "birthdayowl.com", "Payment Received (Rs.$amount) from BirthdayOwl");
                                    if (SERVER_TYPE == "1") {
//                                        $admin_email = "abdul@birthdayowl.com";
                                    } else {
                                        $admin_email = "vpatekar@techathalon.com";
                                    }
                                    $viewn = $this->load->view("email_template/Order_placed", $data, true);
                                    send_Email($viewn, $admin_email, "abdul", "orders@birthdayowl.com", "birthdayowl.com", "Order has been placed by $uname($uemail)");
                                    $where = "order_pro_id = $inserted_voucher_id";
                                    if ($response["code"] == "success") {
                                        $up["payment_mail_status"] = "1";
                                        $this->Birthday->update("order_products", $up, $where);
                                    } else {
                                        $up["payment_mail_status"] = "0";
                                        $this->Birthday->update("order_products", $up, $where);
                                    }
                                } catch (Exception $e) {
                                    $where = "order_pro_id = $inserted_voucher_id";
                                    $up["payment_mail_status"] = "0";
                                    $this->Birthday->update("order_products", $up, $where);
                                }
                            }
                        }
                        $this->session->userdata['userdata']['voucher_data'] = array();
                    }
                }
            }

            if ($what_was_transaction == 2 || $what_was_transaction == 3) {
// now the transaction is related to flower and its respective.
                $add_to_payment_flower_table = array();
                $add_to_order_to_fnp_table = array();

                $flower_purchased_array = $this->session->userdata['userdata']['flower_data'];
//                $user_id = $this->session->userdata['userdata']['user_id'];
                $is_guest = $this->session->userdata['userdata']['is_guest'];
                $number_of_flower_purchased = count($flower_purchased_array);
// this loop will add the flower details of payment into the table flower_payment
                if ($hash != $posted_hash) {
                    for ($i = 0; $i < $number_of_flower_purchased; $i++) {
                        $add_to_payment_flower_table[$i]["total_amount"] = $payment_for_flower_total_amount_flower;
                        $add_to_payment_flower_table[$i]["user_id"] = $user_id;
                        $add_to_payment_flower_table[$i]["flower_id"] = $flower_purchased_array[$i]['flower_id'];
                        $add_to_payment_flower_table[$i]['flower_code'] = $flower_purchased_array[$i]['flower_code'];    // i will get the flower code.
                        $add_to_payment_flower_table[$i]["order_id"] = $order_id;
                        $add_to_payment_flower_table[$i]["transaction_id"] = $payment_for_flower_txnid;
                        $add_to_payment_flower_table[$i]["transaction_status"] = $payment_for_flower_status;
                        $add_to_payment_flower_table[$i]["bill_to_fname"] = $payment_for_flower_fname;
                        $add_to_payment_flower_table[$i]["bill_to_email"] = $payment_for_flower_email;
                        $add_to_payment_flower_table[$i]["payment_mode"] = $payment_for_flower_mode;
                        $add_to_payment_flower_table[$i]["payuId"] = $payment_for_flower_payuid;
                        $add_to_payment_flower_table[$i]["purchased_date"] = $payment_for_flower_purchased_date;
                        $this->db->insert('flower_payment', $add_to_payment_flower_table[$i]);
                        $inserted_payment_id = $this->db->insert_id();
//now lets add the flower to th flower table if it is not sucessfull
// use the same loop to put data into the table.
                    }
                    redirect(BASEURL . "payment-failed/" . $orderid); // when the transaction fails.....
                } else if (!$is_amountValid) {
                    for ($i = 0; $i < $number_of_flower_purchased; $i++) {
                        $add_to_payment_flower_table[$i]["total_amount"] = $payment_for_flower_total_amount_flower;
                        $add_to_payment_flower_table[$i]["user_id"] = $user_id;
                        $add_to_payment_flower_table[$i]["flower_id"] = $flower_purchased_array[$i]['flower_id'];
                        $add_to_payment_flower_table[$i]['flower_code'] = $flower_purchased_array[$i]['flower_code'];    // i will get the flower code.
                        $add_to_payment_flower_table[$i]["order_id"] = $order_id;
                        $add_to_payment_flower_table[$i]["transaction_id"] = $payment_for_flower_txnid;
                        $add_to_payment_flower_table[$i]["transaction_status"] = $payment_for_flower_status . " " . "Amount Altered";
                        $add_to_payment_flower_table[$i]["bill_to_fname"] = $payment_for_flower_fname;
                        $add_to_payment_flower_table[$i]["bill_to_email"] = $payment_for_flower_email;
                        $add_to_payment_flower_table[$i]["payment_mode"] = $payment_for_flower_mode;
                        $add_to_payment_flower_table[$i]["payuId"] = $payment_for_flower_payuid;
                        $add_to_payment_flower_table[$i]["purchased_date"] = $payment_for_flower_purchased_date;
                        $this->db->insert('flower_payment', $add_to_payment_flower_table[$i]);
                        $inserted_payment_id = $this->db->insert_id();
//now lets add the flower to th flower table if it is not sucessfull
// use the same loop to put data into the table.
                    }
                    redirect(BASEURL . "payment-failed/" . $orderid); // when the transaction fails.....
                } else if ($number_of_flower_purchased == 0) {
                    // this condition is to avoid hacking if there is no session of flower and user refreshes same payumoney url which should be not allowed
                    $add_to_payment_flower_table["total_amount"] = $payment_for_flower_total_amount_flower;
                    $add_to_payment_flower_table["user_id"] = $user_id;
                    $add_to_payment_flower_table["flower_id"] = 0;      // since we wont get flower code if session has cleared
                    $add_to_payment_flower_table['flower_code'] = 0;    // since we wont get flower code if session has cleared
                    $add_to_payment_flower_table["order_id"] = $order_id;
                    $add_to_payment_flower_table["transaction_id"] = $payment_for_flower_txnid;
                    $add_to_payment_flower_table["transaction_status"] = $payment_for_flower_status . " " . "session cleared";
                    $add_to_payment_flower_table["bill_to_fname"] = $payment_for_flower_fname;
                    $add_to_payment_flower_table["bill_to_email"] = $payment_for_flower_email;
                    $add_to_payment_flower_table["payment_mode"] = $payment_for_flower_mode;
                    $add_to_payment_flower_table["payuId"] = $payment_for_flower_payuid;
                    $add_to_payment_flower_table["purchased_date"] = $payment_for_flower_purchased_date;
                    $this->db->insert('flower_payment', $add_to_payment_flower_table);
                    $inserted_payment_id = $this->db->insert_id();

                    redirect(BASEURL . "payment-failed/" . $orderid); // when the transaction fails.....
                } else {
                    if ($status == 'success') {



                        for ($j = 0; $j < $number_of_flower_purchased; $j++) {

                            $current_flower_total_quantity = (int) $flower_purchased_array[$j]["quantity"];
                            $current_flower_total_value = (int) $flower_purchased_array[$j]['selected_amount_new'];
                            $single_flower_price = $current_flower_total_value / $current_flower_total_quantity;
                            for ($q = 0; $q < $current_flower_total_quantity; $q++) {

                                $add_to_payment_flower_table[$j]["total_amount"] = $payment_for_flower_total_amount_flower;
                                $add_to_payment_flower_table[$j]["user_id"] = $user_id;
                                $add_to_payment_flower_table[$j]["flower_id"] = $flower_purchased_array[$j]['flower_id'];
                                $add_to_payment_flower_table[$j]['flower_code'] = $flower_purchased_array[$j]['flower_code'];    // i will get the flower code.
                                $add_to_payment_flower_table[$j]["order_id"] = $order_id;
                                $add_to_payment_flower_table[$j]["transaction_id"] = $payment_for_flower_txnid;
                                $add_to_payment_flower_table[$j]["transaction_status"] = $payment_for_flower_status;
                                $add_to_payment_flower_table[$j]["bill_to_fname"] = $payment_for_flower_fname;
                                $add_to_payment_flower_table[$j]["bill_to_email"] = $payment_for_flower_email;
                                $add_to_payment_flower_table[$j]["payment_mode"] = $payment_for_flower_mode;
                                $add_to_payment_flower_table[$j]["payuId"] = $payment_for_flower_payuid;
                                $add_to_payment_flower_table[$j]["purchased_date"] = $payment_for_flower_purchased_date;
                                $this->db->insert('flower_payment', $add_to_payment_flower_table[$j]);
                                $inserted_payment_id = $this->db->insert_id();

//now lets add the flower to th flower table if it is not sucessfull
// use the same loop to put data into the order_to_fnp.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                        $add_to_order_to_fnp_table[$j]["order_id_from_orders"] = $order_id;

                                $add_to_order_to_fnp_table[$j]["flower_purchased_id"] = $flower_purchased_array[$j]['flower_id'];
                                $add_to_order_to_fnp_table[$j]["flower_purchased_code"] = $flower_purchased_array[$j]['flower_code'];
                                $add_to_order_to_fnp_table[$j]["flower_purchased_image"] = $flower_purchased_array[$j]['flower_image'];
                                $add_to_order_to_fnp_table[$j]["flower_purchased_name"] = $flower_purchased_array[$j]['flower_name'];
                                $add_to_order_to_fnp_table[$j]["flower_purchased_price"] = $flower_purchased_array[$j]['flower_price'];
                                $add_to_order_to_fnp_table[$j]["flower_purchased_quantity"] = $flower_purchased_array[$j]['quantity'];
                                $add_to_order_to_fnp_table[$j]["flower_sender_id"] = $user_id;
                                $add_to_order_to_fnp_table[$j]["flower_guest_session"] = $flower_purchased_array[$j]['guest_session'];
                                $add_to_order_to_fnp_table[$j]["flower_sender_name"] = $flower_purchased_array[$j]['sender_name'];
                                $add_to_order_to_fnp_table[$j]["flower_receiver_name"] = $flower_purchased_array[$j]['receiver_name'];
                                $add_to_order_to_fnp_table[$j]["flower_is_delivered_status"] = 1;
                                $add_to_order_to_fnp_table[$j]["flower_receiver_address1"] = $flower_purchased_array[$j]['delivery_address_line1'];
                                $add_to_order_to_fnp_table[$j]["flower_receiver_address2"] = $flower_purchased_array[$j]['delivery_address_line2'];
                                $add_to_order_to_fnp_table[$j]["flower_delivery_city"] = $flower_purchased_array[$j]['delivery_city'];
                                $add_to_order_to_fnp_table[$j]["flower_delivery_pincode"] = $flower_purchased_array[$j]['delivery_pincode'];
                                $add_to_order_to_fnp_table[$j]["flower_delivery_shipping_method"] = $flower_purchased_array[$j]['delivery_shipping_method'];
                                $add_to_order_to_fnp_table[$j]["flower_delivery_time_slot"] = $flower_purchased_array[$j]['delivery_time_slot'];
                                $add_to_order_to_fnp_table[$j]["message_on_card"] = $flower_purchased_array[$j]['message_on_card'];
                                $add_to_order_to_fnp_table[$j]["flower_delivery_date"] = $flower_purchased_array[$j]['delivery_date'];
                                $add_to_order_to_fnp_table[$j]["flower_sender_phone"] = $flower_purchased_array[$j]['sender_phone'];
                                $add_to_order_to_fnp_table[$j]["flower_receiver_phone"] = $flower_purchased_array[$j]['receiver_phone'];
                                $add_to_order_to_fnp_table[$j]["order_id_from_orders"] = $order_id;
                                $add_to_order_to_fnp_table[$j]["flower_sender_email"] = $flower_purchased_array[$j]['sender_email'];
                                $add_to_order_to_fnp_table[$j]["flower_receiver_email"] = $flower_purchased_array[$j]['receiver_email'];
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                $add_to_order_to_fnp_table[$j]["flower_transaction_status"] = $payment_for_flower_status;
                                $add_to_order_to_fnp_table[$j]["flower_payu_id"] = $payment_for_flower_payuid;
                                $add_to_order_to_fnp_table[$j]["flower_purchased_is_guest"] = $flower_purchased_array[$j]['is_guest'];
                                $add_to_order_to_fnp_table[$j]["flower_purchased_guest_session"] = $flower_purchased_array[$j]['guest_session'];
                                $add_to_order_to_fnp_table[$j]["flower_purchased_date_time"] = $payment_for_flower_purchased_date;
                                $add_to_order_to_fnp_table[$j]["payment_id_from_flower_payment"] = $inserted_payment_id;  // this is added for inserted payment_id
                                $this->db->insert('order_to_fnp', $add_to_order_to_fnp_table[$j]);
                                $inserted_flower_id = $this->db->insert_id();
//                        $payment_data = $this->Birthday->getorderPaymentFlowerData($inserted_flower_id)->row_array();
                                $data_update["flower_unique_oid"] = "BHOWLFL00$inserted_flower_id";
                                $where = "flower_order_id = $inserted_flower_id";
                                $this->Birthday->update("order_to_fnp", $data_update, $where);

                                $payment_data = $this->Birthday->getorderPaymentFlowerData($inserted_flower_id)->row_array();

                                $userinfo = $this->Birthday->getSelectData("email, first_name,mobile_no", "users", "user_id = $user_id")->row_array();
                                $data["sender_email"] = $sender_email = $userinfo["email"];
                                $data["sender_name"] = $sender_name = trim($userinfo["first_name"]);
                                $data["sender_phone"] = $sender_mobile_no = $userinfo["mobile_no"];
                                $flower_unique_oid = $data["order_id"] = $payment_data["flower_unique_oid"];
                                $data["bill_to_name"] = $payment_data["bill_to_fname"];
//                        $data["samount"] = $data["total_amount"] = $amount;
                                $data["payment_mode"] = $payment_data["payment_mode"];
                                $data["purchased_date"] = $payment_data["purchased_date"];
                                $data["flower_delivery_date"] = $payment_data["flower_delivery_date"];
                                $data["flower_delivery_time_slot"] = $payment_data["flower_delivery_time_slot"];
                                $data["message_on_card"] = $payment_data['message_on_card'];

                                $data["flower_code"] = $payment_data["flower_purchased_code"];
                                $data["flower_image"] = $payment_data["flower_purchased_image"];
                                $data["flower_name"] = $payment_data["flower_purchased_name"];
                                $data["flower_price"] = $payment_data["flower_purchased_price"];
                                $data["flower_quantity"] = $payment_data["flower_purchased_quantity"];

                                $data["receiver_name"] = $receiver_name = $payment_data["flower_receiver_name"];
                                $data["receiver_email"] = $receiver_email = $payment_data["flower_receiver_email"];
                                $data["receiver_phone"] = $receiver_phone = $payment_data["flower_receiver_phone"];
                                $data["flower_receiver_address1"] = $payment_data["flower_receiver_address1"];
                                $data["flower_receiver_address2"] = $payment_data["flower_receiver_address2"];
                                $data["flower_delivery_city"] = $payment_data["flower_delivery_city"];
                                $data["flower_delivery_pincode"] = $payment_data["flower_delivery_pincode"];

                                $data["processing_fee1"] = round(($data["flower_price"] * $payment_data["flower_purchased_quantity"] * 3.5) / 100, 2);

                                $data["samount"] = $data["total_amount"] = ($payment_data["flower_purchased_price"] * $payment_data["flower_purchased_quantity"]) + $data["processing_fee1"];
                            }
                            //FNP's & Other's Email IDs
//                                $fnp_emails[] = array('order4@fnp.com', 'sonal.j@fnp.com');
//                                $fnp_names[] = array('Orders FNP', 'Sonal FNP');
//                                $fnp_cc_emails[] = array('Bibek.banerjee@fnp.com', 'support@birthdayowl.com', 'abdul@birthdayowl.com', 'Devesh.m@fnp.com');
//                                $fnp_cc_names[] = array('Bibek FNP', 'Support Birthday Owl', 'Abdul Birthday Owl', 'Devesh');
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                            For Testing Purpose
                            $fnp_emails[] = array('hnadar@techathalon.com');
                            $fnp_names[] = array('Hareesh');
                            $fnp_cc_emails[] = array('vpatekar@techathalon.com');
                            $fnp_cc_names[] = array('Vina');
//Payment Confirmation Email
                            try {
//TO FNP
                                $view_flower = $this->load->view("email_template/Order_placed_flower", $data, true);
                                send_EmailResponse_with_cc($view_flower, $fnp_emails, $fnp_names, $fnp_cc_emails, $fnp_cc_names, "orders@birthdayowl.com", "birthdayowl.com", "$sender_name($sender_email), Your order $flower_unique_oid is confirmed.");

//To Customer Order Placed SMS & EMAIL
                                send_EmailResponse($view_flower, $sender_email, $sender_name, "orders@birthdayowl.com", "birthdayowl.com", "$sender_name($sender_email), Your order $flower_unique_oid is confirmed.");

                                if ($sender_mobile_no != '') {
                                    $show_message = "Thank You $sender_name! Your order #$flower_unique_oid is confirmed. We will keep you posted on the further status of your order. \n- Birthday Owl (www.birthdayowl.com)";
                                    $this->Birthday->send_sms($sender_mobile_no, $show_message);
                                }

//To Customer Order Received Receipt - Bill
                                $view = $this->load->view("email_template/Payment_received_flower", $data, true);
                                $response = send_EmailResponse($view, $sender_email, $sender_name, "orders@birthdayowl.com", "birthdayowl.com", "Payment Received (Rs.$amount) from BirthdayOwl");

                                if (SERVER_TYPE == "1") {
//                                    $admin_email = "abdul@birthdayowl.com";
                                } else {
                                    $admin_email = "vpatekar@techathalon.com";
                                }
                                $viewn = $this->load->view("email_template/Order_placed_1", $data, true);
                                send_Email($viewn, $admin_email, "abdul", "orders@birthdayowl.com", "birthdayowl.com", "Order has been placed by $sender_name($sender_email)");
                                $where = "flower_order_id = $inserted_flower_id";
                                if ($response["code"] == "success") {
                                    $up_flower["flower_is_mail_sent"] = "1";
                                    $this->Birthday->update("order_to_fnp", $up_flower, $where);
                                } else {
                                    $up_flower["flower_is_mail_sent"] = "0";
                                    $this->Birthday->update("order_to_fnp", $up_flower, $where);
                                }
                            } catch (Exception $e) {
                                $where = "flower_order_id = $inserted_flower_id";
                                $up_flower["flower_is_mail_sent"] = "0";
                                $this->Birthday->update("order_to_fnp", $up_flower, $where);
                            }
//                            }
                        }
                        $this->session->userdata['userdata']['flower_data'] = array();
                    }
                }
            }
            $userid = $this->Birthday->encryptPassword($user_id);
            redirect(BASEURL . "payment-success/" . $orderid . "/" . $userid . "/" . $what_was_transaction_encrypt);
//            } else {
//                $this->session->set_flashdata('fmsg', "Sorry your session has been expired.Please login again to continue");
//                redirect(WEB_HOME);
//            }
        } else {
            redirect(BASEURL . "payment-failed/" . 0); // when the transaction fails.....
//            $data["trerror"] = 0;
//            $data["transaction_status"] = "fail";
//            $this->load->view("EGIFT/failed_transaction", $data);
        }
    }

    function what_was_transaction_for($product_info) {

        $what_was_purchased_status = 0;

        switch ($product_info) {

            case "BirthdayOwl egift vouchers":
//only voucher purchased
                $what_was_purchased_status = 1;
                break;
            case "BirthdayOwl flower":
//only flower purchased
                $what_was_purchased_status = 2;
                break;
            case "BirthdayOwl voucher and flower":
//both flower and voucher purchased
                $what_was_purchased_status = 3;
                break;
        }

        return $what_was_purchased_status;
    }

    function get_birthday_data() {
        $userinfo = $this->Birthday->getAllData("birthday")->result_array();
        $output["success"] = true;
        $output["birthday_data"] = $userinfo;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function check_session() {
        echo '<pre>';
        $result = $this->session->userdata();
        print_r($result);
        exit;

//        session format
//        Array
//        (
//        [__ci_last_regenerate] => 1536320065
//        [btoken] => 4abe17a1c80cbdd2aa241b70840879de
//        [userdata] => Array
//        (
//        [user_id] => 437
//        [first_name] => Hareeshkumar
//        [username] =>
//        [password] => aGFyZWVzaA == [email] => hnadar@techathalon.com
//        [mobile_no] => 8898853030
//        [zodiac_id] => 4
//        [gender] => 1
//        [birth_date] => 12/7/1991
//        [bdate] => 12
//        [bmonth] => 7
//        [byear] => 1991
//        [fb_id] =>
//        [g_id] =>
//        [login_with] => 1
//        [user_profile_pic] =>
//        [created_date] => 2018-09-07 07:38:06
//        [updated_date] => 2018-09-07 17:09:12
//        [u_last_login] => 2018-09-07 07:38:06
//        [reset_time] => 2018-09-07 07:38:06
//        [reset_link] => 0
//        [reset_activate] => 0
//        [sent_greeting_counter] => 0
//        [membership_status] => 0
//        [country_id] => 102
//        [is_guest] => 0
//        [mobile_verify] => 1
//        [flower_data] => Array
//        ( )
//
//        [voucher_data] => Array
//        ( )
//
//        )
//
//        [success] => 1
//        [login_type] => 0
//        )
    }

//    public function google_url() {
//        $client = new Google_Client();
//        $client->setClientId(CLIENT_ID);
//        $client->setClientSecret(CLIENT_SECRET);
//        $client->setRedirectUri(REDIRECT_FLOWERS_CAKES);
//        $client->setScopes("email");
//
////        $service = new Google_Service_Oauth2($client);
//
//        $url = $client->createAuthUrl();
//        $data['google_url'] = $url;
//        print_r($url);
//    }
//    public function encode() {
//        echo base64_encode(3);
//    }
//
//    public function decode() {
//        echo base64_decode('Mw==');
//    }
//    public function redirect_google_login() {
//        $this->load->library('GoogleFunctionality');
//        $googleFunctionality = new GoogleFunctionality();
//        $redirect_to = $this->uri->segment('3');
//        $redirect = base64_decode($redirect_to);
////        print_r($redirect);
////        print_r(($_GET['code']));
////        exit;
//        $client = new Google_Client();
//        $client->setClientId(CLIENT_ID);
//        $client->setClientSecret(CLIENT_SECRET);
//        $redirect_to_view = "";
//
//        switch ($redirect) {
//            case 0:
//                $client->setRedirectUri(HOMEPAGE);
//                $redirect_to_view = BASEURL;
//                break;
//            case 1:
//                $client->setRedirectUri(REDIRECT_CART);
//                $redirect_to_view = BASEURL . 'cart';
//                break;
//            case 2:
//                $client->setRedirectUri(REDIRECT_REMINDER);
//                $redirect_to_view = BASEURL . 'addReminder/0';
//                break;
//            case 3:
//                $client->setRedirectUri(REDIRECT_FLOWERS_CAKES);
//                $redirect_to_view = BASEURL . 'delivery_address/0';
//        }
//
//        $client->setScopes("email");
//
//        $service = new Google_Service_Oauth2($client);
//
//        $code = isset($_GET['code']) ? $_GET['code'] : NULL;
////        var_dump($code);
//
//        if (isset($code)) {
//
//            try {
//                $client->authenticate($code);   //this is important part to be kept in mind this authenticate the code
//                $access_token = $client->getAccessToken(); //  store access token in session
////                echo $access_token;
//            } catch (Exception $ex) {
//                echo $ex->getMessage();   // redirect to home page
//            }
//
//            try {
////                $payload = $client->verifyIdToken();
//                $user = $service->userinfo->get();
////                echo '<pre>';
////                echo '<br>';
////                print_r($user->email);
////                echo '<br>';
////                print_r($user->name);
////                echo '<br>';
////                print_r($user->id);
//                $user_email = $user->email;
//                $user_name = $user->name;
//
//                $googleFunctionality->setUser_email($user_email);
//                $googleFunctionality->setUser_name($user_name);
////                $googleFunctionality->runner();
//                $return_output = $googleFunctionality->runner();
//                $this->session->set_userdata($return_output);
//            } catch (Exception $ex1) {
//                echo $ex1->getMessage();
//            }
//        } else {
//            
//        }
//        redirect($redirect_to_view);
//    }

    public function jquery_redirect_google_login() {

        $user_id = check_post($this->input->post("user_id"));
        $user_email = check_post($this->input->post("user_email"));
        $user_name = check_post($this->input->post("user_name"));
        $user_image_url = check_post($this->input->post("user_image_url"));

        $this->load->library("GoogleFunctionality");
        $google_functionality = new GoogleFunctionality();

        $google_functionality->setUser_email($user_email);
        $google_functionality->setUser_name($user_name);
        $output = $google_functionality->runner();

        $this->session->set_userdata($output);

        $output["success"] = true;
        $output["message"] = "Login Successful through Google";

        $this->output->set_output(json_encode($output));
    }

    public function jquery_redirect_google_login_flowers_cakes() {
        $user_id = check_post($this->input->post("user_id"));
        $user_email = check_post($this->input->post("user_email"));
        $user_name = check_post($this->input->post("user_name"));
        $user_image_url = check_post($this->input->post("user_image_url"));

        $this->load->library("GoogleFunctionality");
        $google_functionality = new GoogleFunctionality();

        $google_functionality->setUser_email($user_email);
        $google_functionality->setUser_name($user_name);
        $output = $google_functionality->runner();

        if (array_key_exists("is_user_google_login", $output)) {
            $output["islogin"] = $output["userdata"]["user_id"];
            $output["user_type"] = 2; //google user
        } else {
            $output["islogin"] = $output["userdata"]["user_id"];
            $output["user_type"] = 1; //birthdayowl user
        }
        $this->session->set_userdata($output);

        $output["success"] = true;
        $output["message"] = "Login Successful through Google";
        $this->output->set_output(json_encode($output));
    }

    public function jquery_redirect_google_login_egift_vouchers() {
        $user_id = check_post($this->input->post("user_id"));
        $user_email = check_post($this->input->post("user_email"));
        $user_name = check_post($this->input->post("user_name"));
        $user_image_url = check_post($this->input->post("user_image_url"));

        $this->load->library("GoogleFunctionality");
        $google_functionality = new GoogleFunctionality();

        $google_functionality->setUser_email($user_email);
        $google_functionality->setUser_name($user_name);
        $output = $google_functionality->runner();

        if (array_key_exists("is_user_google_login", $output)) {
            $output["islogin"] = $output["userdata"]["user_id"];
            $output["user_type"] = 2; //google user
        } else {
            $output["islogin"] = $output["userdata"]["user_id"];
            $output["user_type"] = 1; //birthdayowl user
        }
        $this->session->set_userdata($output);

        $output["success"] = true;
        $output["message"] = "Login Successful through Google";

        $this->output->set_output(json_encode($output));
    }

//    public function google_verification_isset_code($value) {
//        $email = "";
//        $name = "";
//        $id = "";
//        $client = new Google_Client();
//        $client->setClientId(CLIENT_ID);
//        $client->setClientSecret(CLIENT_SECRET);
//        switch ($value){
//           case 1:
//             $client->setRedirectUri(HOMEPAGE);
//               break;
//           case 2:
//               $client->setRedirectUri(REDIRECT_CART);
//               break;
//           case 3:
//               $client->setRedirectUri(REDIRECT_REMINDER);
//        }
//        
//        
//        $client->setScopes("email");
//
//        $service = new Google_Service_Oauth2($client);
//
//        $code = isset($_GET['code']) ? $_GET['code'] : NULL;
////        var_dump($code); 
//
//        if (isset($code)) {
//
//            try {
//                $client->authenticate($code);   //this is important part to be kept in mind this authenticate the code
//                $access_token = $client->getAccessToken(); //  store access token in session
////                echo $access_token;
//            } catch (Exception $ex) {
//                echo $ex->getMessage();   // redirect to home page
//            }
//
//            try {
//                $payload = $client->verifyIdToken();
//                $user = $service->userinfo->get();
//
//                $email = $user->email;
//                $name = $user->name;
//                $id = $user->id;
////                print_r($email);
////                echo '<br>';
////                print_r($name);
//            } catch (Exception $ex1) {
//                echo $ex1->getMessage();
//            }
//        } else {
//            $payload = null;
//        }
//        $data = array(
//            "email_id" => $email,
//            "name" => $name,
//            "id" => $id
//        );
//
//        $this->session->set_userdata("google_login_users", $data);
//        print_r($this->session->userdata("google_login_users"));
//    }

    public function testcountryCode() {
        $country = get_country_info();
        print_r($country);
    }

    public function test_session() {
        echo '<pre>';
        print_r($this->session->userdata);
    }

    public function getCountryCodeData() {
        $result = get_country_info();
        print_r($result);
    }

    public function deeplinkBirthdayReminderDeviceAndWeb() {
        $this->load->library('CommonHelperLibrary');
        $commonHelperLibrary = new CommonHelperLibrary();
        $commonHelperLibrary->isMobileDevice();

        if ($commonHelperLibrary->isMobileDevice()) {
            $data['from'] = 5;
            $this->load->view("HOME/deep_linking", $data);
        } else {
            redirect(BASEURL . "birthday-reminder");
        }
    }

}
