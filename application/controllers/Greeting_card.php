<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Greeting_card extends CI_Controller {

    public function index() {
        
    }

//free e-greeting start
    public function greeting_category() { //function to display greeting category
        $data['title'] = "BirthdayOwl";
        $data['greeting_category'] = $this->Birthday->getAllData("greeting_category")->result_array();
        $this->load->view('admin_panel/add_greeting_category', $data);
    }

    public function add_greeting_category() { //function to add greeting category
        if (count($_POST) > 0) {
            $data["greet_cat_name"] = check_post($this->input->post("greet_cat_name"));
            $data["greeting_type"] = check_post($this->input->post("greeting_type"));
            $this->Birthday->addData("greeting_category", $data);
            $output["success"] = true;
            $output["message"] = "Greeting Category added Successfully!.";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function delete_greeting_category() {
        if (count($_POST) > 0) {
            $greet_cat_id = check_post($this->input->post("greet_cat_id"));
            $count = count($greet_cat_id);
            for ($i = 0; $i < $count; $i++) {
                $where = "greet_cat_id='$greet_cat_id[$i]'";
                $this->Birthday->delete_reminder("greeting_category", $where);
            }
            $output["success"] = true;
            $output["message"] = "Greeting Category Deleted SuccessFully";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function get_greeting_category_details() {
        if (count($_POST) > 0) {
            $greet_cat_id = check_post($this->input->post("greet_cat_id"));
            $where = "greet_cat_id='$greet_cat_id'";
            $greetingcategory_data = $this->Birthday->getUserData("greeting_category", $where)->row_array();
            $output["success"] = true;
            $output["message"] = "";
            $output["greetingcategory"] = $greetingcategory_data;
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function update_greeting_category() {
        if (count($_POST) > 0) {
            $greet_cat_id = check_post($this->input->post("greet_cat_id"));
            $update["greet_cat_name"] = check_post($this->input->post("greet_cat_name"));
            $where = "greet_cat_id='$greet_cat_id'";
            $updated = $this->Birthday->update("greeting_category", $update, $where);
            if ($updated == 1) {
                $output["success"] = true;
                $output["message"] = "Greeting Category updated successfully";
            } else {
                $output["success"] = false;
                $output["message"] = "Fail to update";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function greeting($type) {//1:Free Greeting Upload,2:Product Voucher Upload,3:Free Greeting Thankyou
        if ($this->is_admin_logged_in()) {
            $data['title'] = "BirthdayOwl";
            if ($type == 1) {//Free Greeting Upload
                $where = "greeting_type='1' order by card_id DESC ";
            } else if ($type == 2) {//Product Voucher Upload
                $where = "greeting_type='2' order by card_id DESC";
            } else {//Free Greeting Thankyou
                $where = "greeting_type='3' order by card_id DESC";
            }
            $data['greeting_card'] = $this->Birthday->getUserData("greeting_card", $where)->result_array();
            $data['heading'] = "Upload Greetings";
//        $data['greeting_category'] = $this->Birthday->getAllData("greeting_category")->result_array();
            $data["card_data"] = array(
                'card_id' => '',
                'card_name' => '',
                'keywords' => '',
                'description' => '',
                'alt_tag' => '',
                'title_tag' => '',
                'message' => '',
                'front_page_image' => '',
            );
            $data["btn_click"] = "submit_greeting_card";
            $data["id"] = "card_name_submit";
            $data["type"] = $type;
            $data["action"] = "Greeting_card/upload_greeting_card";
            $this->load->view('admin_panel/upload_greeting_card_image', $data);
        }
    }

    public function get_greeting_type() {
        if (count($_POST) > 0) {
            $greeting_type = check_post($this->input->post("greeting_type"));
            $where = "greeting_type=$greeting_type";
            $greetings = $this->Birthday->getUserData("greeting_category", $where)->result_array();
            if (count($greetings) > 0) {
                $output["greetings"] = $greetings;
                $output["count"] = count($greetings);
                $output["success"] = true;
                $output["message"] = "";
            } else {
                $output["count"] = 0;
                $output["success"] = FALSE;
                $output["message"] = "No Greetings";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function upload_greeting_card() {

        $filename = "";
        $upload = FALSE;
        if (!empty($_FILES)) {
            $tempFile = trim($_FILES['userfile']['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
            $filename = str_replace(" ", "_", $filename);
            $check_img_name = $this->Birthday->getSelectData("front_page_image", "greeting_card", "front_page_image='$filename'")->result_array();

            if (count($check_img_name) > 0) {
                $output["success"] = false;
                $output["message"] = "Greeting card with name $filename already exist.";
                echo json_encode($output);
                exit();
            }


            $targetPath = 'public/greetings/';
            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = false;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES["userfile"]["error"];
                } else {
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $output["message"] = "Greeting Card Uploaded successfully";
                    $upload = TRUE;
                }
            } else {

                $output["filename"] = $filename;
                $output["success"] = true;
                $upload = TRUE;
            }
        }
        if (count($_POST) > 0) {
            $this->imagecrop($filename);
            $data["greet_cat_id"] = 0;
            $data["greeting_type"] = $type = check_post($this->input->post("greeting_type"));
            $data["card_name"] = check_post($this->input->post("card_name"));
            $data["card_url"] = str_replace(" ", "_", check_post($this->input->post("card_name")));
            $data["keywords"] = check_post($this->input->post("keywords"));
            $data["description"] = check_post($this->input->post("description"));
            $data["alt_tag"] = check_post($this->input->post("alt_tag"));
            $data["title_tag"] = check_post($this->input->post("title_tag"));
            $data["message"] = check_post($this->input->post("message"));
            $data["front_page_image"] = $filename;
            $data["card_created_date"] = date("Y-m-d H:i:s");
            $this->Birthday->addData("greeting_card", $data);
            $output["message"] = "Greeting Card Uploaded successfully";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function imagecrop($imgname) {

        $imgSrc = "public/greetings/" . $imgname;

        $filename = basename($imgSrc);
        $file_extension = strtolower(substr(strrchr($filename, "."), 1));
        switch ($file_extension) {
            case "gif": $ctype = "image/gif";
                //saving the image into memory (for manipulation with GD Library)
                $myImage = imagecreatefromgif($imgSrc);
                break;
            case "png": $ctype = "image/png";
                //saving the image into memory (for manipulation with GD Library)
                $myImage = imagecreatefrompng($imgSrc);
                break;
            case "jpg": $ctype = "image/jpg";
                //saving the image into memory (for manipulation with GD Library)
                $myImage = imagecreatefromjpeg($imgSrc);
                break;
            case "jpeg": $ctype = "image/jpeg";
                //saving the image into memory (for manipulation with GD Library)
                $myImage = imagecreatefromjpeg($imgSrc);
                break;
            default:
        }

//getting the image dimensions
        list($width, $height) = getimagesize($imgSrc);

// calculating the part of the image to use for thumbnail
        if ($width > $height) {
            $y = 0;
            $x = ($width - ($height * 4) / 2) / 2;

            $smallestSide = $height;
        } else {
            $x = 0;
            $y = ($height - $width) / 2;
            $smallestSide = $width;
        }

// copying the part into thumbnail

        $thumbSize = 600;

        $thumb = imagecreatetruecolor(400, $thumbSize);
        //earlier it was 800
        imagecopyresampled($thumb, $myImage, 0, 0, 0, 0, 400, $thumbSize, $smallestSide - 380, $smallestSide);

//final output
        header("Content-type:  . $ctype");
//        $filename="public/cropped_greetings/";
        switch ($file_extension) {
            case "gif": imagegif($thumb, 'public/cropped_greetings/' . $filename);
                break;
            case "png": imagepng($thumb, 'public/cropped_greetings/' . $filename, 9);
                break;
            case "jpg": imagejpeg($thumb, 'public/cropped_greetings/' . $filename, 100);
                break;
            case "jpeg": imagejpeg($thumb, 'public/cropped_greetings/' . $filename, 100);
                break;
            default:
        }
//        imagejpeg($thumb);
    }

    public function check_card_name() {
        if (count($_POST) > 0) {
            $card_name = check_post($this->input->post("card_name"));
            $where = "card_name='$card_name'";
            $check_user = $this->Birthday->getUserData("greeting_card", $where)->num_rows();
            if ($check_user > 0) {
                $output["success"] = false;
                $output["message"] = "Card Name already exist ";
            } else {
                $output["success"] = true;
                $output["message"] = "hello"; //If user name is not in the database
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function Edit_check_card_name() {
        if (count($_POST) > 0) {
            $card_name = check_post($this->input->post("card_name"));
            $card_id = check_post($this->input->post("card_id"));
            $where = "card_name='$card_name' and card_id!='$card_id'";
            $check_user = $this->Birthday->getUserData("greeting_card", $where)->num_rows();
            if ($check_user > 0) {
                $output["success"] = false;
                $output["message"] = "Card Name already exist ";
            } else {
                $output["success"] = true;
                $output["message"] = "hello"; //If user name is not in the database
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function delete_greeting_card() {
        if (count($_POST) > 0) {
            $card_id = check_post($this->input->post("card_id"));
            $count = count($card_id);
            for ($i = 0; $i < $count; $i++) {
                $where = "card_id='$card_id[$i]'";
                $this->Birthday->delete_reminder("greeting_card", $where);
            }
            $output["success"] = true;
            $output["message"] = "Greeting Card Deleted SuccessFully";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function edit_card($card_id, $type) {
        $data = array();
        $where = "card_id='$card_id'";
        $data['heading'] = "Edit Greeting Card Image";
        $card_data = $this->Birthday->getUserData("greeting_card", $where)->row_array();
        $data["card_data"] = $card_data;
        $data["type"] = $type;
        $data["btn_click"] = "edit_greeting_card";
        $data["id"] = "card_name_edit";
        $data["title"] = "BirthdayOwl";
        if ($type == 1) {
            $where = "greeting_type='1'  order by card_id DESC";
            $data['greeting_card'] = $this->Birthday->getUserData("greeting_card", $where)->result_array();
        } else {
            $where = "greeting_type='2'  order by card_id DESC";
            $data['greeting_card'] = $this->Birthday->getUserData("greeting_card", $where)->result_array();
        }
        $data["action"] = "Greeting_card/card_edit_success/$card_id";
        $this->load->view('admin_panel/upload_greeting_card_image', $data);
    }

    public function card_edit_success($card_id) {
        $type = check_post($this->input->post("greeting_type"));
        $filename = "";
        $upload = FALSE;
        if (!empty($_FILES)) {
            $tempFile = trim($_FILES['userfile']['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
            $filename = str_replace(" ", "_", $filename);
            $check_img_name = $this->Birthday->getSelectData("front_page_image", "greeting_card", "front_page_image='$filename' and card_id!='$card_id'")->result_array();
            if (count($check_img_name) > 0) {
                $output["success"] = false;
                $output["message"] = "Greeting card with name $filename already exist.";
                echo json_encode($output);
                exit();
            }
            $targetPath = 'public/greetings/';
            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = false;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES["userfile"]["error"];
                } else {
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $upload = TRUE;
                }
            } else {
                $output["filename"] = $filename;
                $output["success"] = true;
                $upload = TRUE;
            }
        }
        if (count($_POST) > 0) {
//            $updated_data["greet_cat_id"] = check_post($this->input->post("greet_cat_id"));
            $updated_data["card_name"] = check_post($this->input->post("card_name"));
            $updated_data["card_url"] = str_replace(" ", "_", check_post($this->input->post("card_name")));
            $updated_data["keywords"] = check_post($this->input->post("keywords"));
            $updated_data["description"] = check_post($this->input->post("description"));
            $updated_data["alt_tag"] = check_post($this->input->post("alt_tag"));
            $updated_data["title_tag"] = check_post($this->input->post("title_tag"));
            $updated_data["greeting_type"] = $type = check_post($this->input->post("greeting_type"));
//            $updated_data["message"] = check_post($this->input->post("message"));
            $message = check_post($this->input->post("message"));
            $updated_data["message"] = preg_replace('/[^\da-z ]/i', '', $message);
            if ($filename != '') {
                $updated_data["front_page_image"] = $filename;
                $this->imagecrop($updated_data["front_page_image"]);
            } else {
                $updated_data["front_page_image"] = check_post($this->input->post("userfile_old"));
            }
            $where = "card_id='$card_id'";
            $this->Birthday->update("greeting_card", $updated_data, $where);
            $output["success"] = TRUE;
            $output["message"] = "Greeting card updated successfully";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function sent_greetings() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
//        $user_data = $this->Birthday->getSelectData("first_name,email,user_id,sent_greeting_counter", "users", " sent_greeting_counter > 0 order by user_id DESC")->result_array();
            $user_data = $this->Birthday->getJoinedData2("users.first_name,users.email,users.user_id,users.sent_greeting_counter,users.mobile_no,Max(`free_greetings`.`updated_date`)as last_sent", "users", "free_greetings", "users.user_id=free_greetings.user_id", " users.sent_greeting_counter > 0 group by free_greetings.user_id order by users.user_id DESC")->result_array();
            $data["user_info"] = $user_data;
            $this->load->view('admin_panel/sent_greetings', $data);
        }
    }

    public function sent_free_greetings() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";

            $query = "select free_sender_email as email , free_sender_phone as phone, free_sender_name as name , max(created_date) as last_send_date , count(free_sender_email) counter from free_greeting_sender WHERE ((free_sender_email != '' and free_sender_phone = '') or (free_sender_email != '' and free_sender_phone != '')) group by free_sender_email
	UNION
select free_sender_email , free_sender_phone ,free_sender_name,  max(created_date) as last_send_date , count(free_sender_phone) from free_greeting_sender where free_sender_phone != '' and free_sender_email = '' group by free_sender_phone ORDER BY last_send_date DESC";

            $result = $this->Birthday->simple_query($query)->result_array();

            foreach ($result as $key => $value) {
                if ($value["name"] != "") {
                    $value["name"] = preg_replace('/[^\da-z ]/i', '', $value["name"]);
                    $results[] = $value;
                }
            }
            $data["user_info"] = $result;
            $this->load->view('admin_panel/sent_free_greetings', $data);
        }
    }

    public function thankyou() {
        
    }

    public function getgreetingreceivers($user_id) {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $user_data = $this->Birthday->getSelectData("greeting_img,fname,femail,fphone,delivery_date,fid,platform", "free_greetings", " user_id =$user_id order by delivery_date DESC")->result_array();

            $data["user_info"] = $user_data;
            $this->load->view('admin_panel/get_greeting_receivers', $data);
        }
    }

    public function getgreetingreceivers_new($user_email, $user_phone) {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";

            if ($user_email != '0') {
                $where = "where free_sender_email = '$user_email'";
            }
            if ($user_email == '0' && $user_phone != '0') {
                $where = "where free_sender_phone = '$user_phone' and free_sender_email = '' ";
            }

            $query = "select free_greeting_sender.free_sender_name,free_greeting_sender.free_sender_email,free_greeting_sender.free_sender_phone,free_greetings.greeting_info_link,free_greetings.delivery_date,free_greetings.platform,free_greetings.fid,free_greetings.fname,free_greetings.fphone,free_greetings.femail FROM free_greetings JOIN free_greeting_sender ON free_greeting_sender.fid_from_freegreeting=free_greetings.fid $where";
            $result = $this->Birthday->simple_query($query)->result_array();
            $data["user_info"] = $result;
            $this->load->view('admin_panel/get_greeting_free_receivers', $data);
        }
    }

    public function is_admin_logged_in() {
        if ($this->session->userdata('admin_data') != NULL) {
            return true;
        } else {
            redirect(BASEURL . "owl");
        }
    }

    //free e-greeting admin end
}
