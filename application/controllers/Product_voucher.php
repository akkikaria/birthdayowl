<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Product_voucher extends CI_Controller {

    public function index() {
        
    }

//Product Voucher start
    public function voucher_category() { //function to display Voucher category
        $data['title'] = "BirthdayOwl";
        $data['voucher_category'] = $this->Birthday->getAllData("voucher_category")->result_array();
        $data["btn_click"] = "submit_voucher_category";
        $data['heading'] = "Add Product Voucher Category";
        $data["title"] = "BirthdayOwl";
        $data["voucher_data"] = array(
            'voucher_cat_id' => '',
            'voucher_cat_name' => '',
            'voucher_image_icon' => ''
        );
        $data["action"] = "Product_voucher/add_voucher_category";
        $this->load->view('admin_panel/add_voucher_category', $data);
    }

    public function add_voucher_category() { //function to add Voucher category
        $filename = "";
        $upload = FALSE;
        if (!empty($_FILES)) {
            $tempFile = trim($_FILES['userfile']['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
            $filename = str_replace(" ", "_", $filename);
            $targetPath = 'public/voucher_category/';
            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = false;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES["userfile"]["error"];
                } else {
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $upload = TRUE;
                }
            } else {
                $output["filename"] = $filename;
                $output["success"] = true;
                $upload = TRUE;
            }
        }
        if (count($_POST) > 0) {
            $data["voucher_cat_name"] = check_post($this->input->post("voucher_cat_name"));
            $data["voucher_image_icon"] = $filename;
            $this->Birthday->addData("voucher_category", $data);
            redirect(BASEURL . "Product_voucher/voucher_category");
        }
    }

    public function edit_voucher_category($voucher_cat_id) {
        $data = array();
        $where = "voucher_cat_id='$voucher_cat_id'";
        $data['heading'] = "Edit Product Voucher Category";
        $voucher_data = $this->Birthday->getUserData("voucher_category", $where)->row_array();
        $data["voucher_data"] = $voucher_data;
        $data["btn_click"] = "edit_voucher_category";
        $data["title"] = "BirthdayOwl";
        $data['voucher_category'] = $this->Birthday->getAllData("voucher_category")->result_array();
        $data["action"] = "Product_voucher/voucherCategory_edit_success/$voucher_cat_id";
        $this->load->view('admin_panel/add_voucher_category', $data);
    }

    public function voucherCategory_edit_success($voucher_cat_id) {
        $filename = "";
        $upload = FALSE;
        if (!empty($_FILES)) {
            $tempFile = trim($_FILES['userfile']['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
            $filename = str_replace(" ", "_", $filename);
            $targetPath = 'public/voucher_category/';
            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = false;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES["userfile"]["error"];
                } else {
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $upload = TRUE;
                }
            } else {
                $output["filename"] = $filename;
                $output["success"] = true;
                $upload = TRUE;
            }
        }
        if (count($_POST) > 0) {
            $updated_data["voucher_cat_name"] = check_post($this->input->post("voucher_cat_name"));
            if ($filename != '') {
                $updated_data["voucher_image_icon"] = $filename;
            } else {
                $updated_data["voucher_image_icon"] = check_post($this->input->post("userfile_old"));
            }
            $where = "voucher_cat_id='$voucher_cat_id'";
            $this->Birthday->update("voucher_category", $updated_data, $where);
            redirect(BASEURL . "Product_voucher/voucher_category");
        }
    }

    public function delete_voucher_category() {
        if (count($_POST) > 0) {
            $voucher_cat_id = check_post($this->input->post("voucher_cat_id"));
            $count = count($voucher_cat_id);
            for ($i = 0; $i < $count; $i++) {
                $where = "voucher_cat_id='$voucher_cat_id[$i]'";
                $this->Birthday->delete_reminder("voucher_category", $where);
            }
            $output["success"] = true;
            $output["message"] = "Voucher Category Deleted SuccessFully";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function voucher_product() {
        $data['title'] = "BirthdayOwl";
        $data["heading"] = "Add Product Voucher";
        $data['voucher_category'] = $this->Birthday->getAllData("voucher_category")->result_array();
        $where = "greeting_type=2";
        $data["greeting_card"] = $this->Birthday->getUserData("greeting_card", $where)->result_array();
        $data['voucher_product'] = $this->Birthday->_get_voucher_cat_pro()->result_array();
        $data["btn_click"] = "submit_product_voucher";
        $data["voucher_data"] = array(
            'voucher_pro_id' => '',
            'voucher_pro_name' => '',
            'pro_tagline' => '',
            'keywords' => '',
            'voucher_cat_id' => '0',
            'description' => '',
            'terms_conditions' => '',
            'alt_tag' => '',
            'title_tag' => '',
            'redemption_details' => '',
            // 'amount' => '',
            'locations' => '',
            'card_id' => '',
            'product_image' => '',
        );
        $data["checkview"] = "1";
        $data["voucher_amount"] = "";

        $data["card_id"] = array();
        $data["action"] = "Product_voucher/add_product_voucher";
        $data["id"] = "voucher_pro_name_submit";
        $this->load->view('admin_panel/add_product_voucher', $data);
    }

    public function amount_voucher() {
        $data['title'] = "BirthdayOwl";
        $data["heading"] = "Add Amount";
        $data["btn_click"] = "submit_amount";
        $data["voucher_amt"] = $this->Birthday->get_voucher_amount()->result_array();
        $data["voucher_data"] = array(
            'id' => '',
            'voucher_id' => '0',
            'pamount' => '',
            'pin' => '',
            'code' => '',
            'expiry_date' => ''
        );
        $data["vouchers"] = $this->Birthday->getAllData("voucher_product")->result_array();
        $this->load->view('admin_panel/add_amount_voucher', $data);
    }

    public function add_voucher_amount() {
        if (count($_POST) > 0) {
            $data["voucher_id"] = check_post($this->input->post("voucher_pro_id"));
            $data["pamount"] = check_post($this->input->post("pamount"));
            $data["pin"] = $pin = check_post($this->input->post("pin"));
            $data["expiry_date"] = check_post($this->input->post("expiry_date"));
            $data["code"] = check_post($this->input->post("code"));
            $data["status"] = "1";
            $data["created_date"] = date("Y-m-d H:i:s");
            $where = "pin='$pin'";
            $check_pin = $this->Birthday->getUserData("voucher_amount", $where)->num_rows();
            if ($check_pin > 0) {
                $output["success"] = false;
                $output["message"] = "Voucher with given PIN is already exist";
                echo json_encode($output);
                exit;
            }
            $this->Birthday->addData("voucher_amount", $data);
            $output["success"] = true;
            $output["message"] = "Amount added Successfully";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function edit_voucher_amount($voucher_id) {
        $data = array();
        $where = "voucher_id='$voucher_id'";
        $data["heading"] = "Edit Voucher";
        $data["btn_click"] = "edit_voucher";
        $data["voucher_data"] = $this->Birthday->getUserData("voucher_amount", $where)->row_array();

        $data["voucher_amt"] = $this->Birthday->get_voucher_amount()->result_array();
        $data["vouchers"] = $this->Birthday->getAllData("voucher_product")->result_array();
        $data["title"] = "BirthdayOwl";
        $this->load->view('admin_panel/add_amount_voucher', $data);
    }

    public function voucher_amount_edit_success() {

        if (count($_POST) > 0) {
            $id = check_post($this->input->post("id"));
            $updated_data["voucher_id"] = check_post($this->input->post("voucher_pro_id"));
            $updated_data["pamount"] = check_post($this->input->post("pamount"));
            $updated_data["pin"] = $pin = check_post($this->input->post("pin"));
            $updated_data["expiry_date"] = check_post($this->input->post("expiry_date"));
            $updated_data["code"] = check_post($this->input->post("code"));
            $updated_data["updated_date"] = date("Y-m-d H:i:s");
            $where = " pin='$pin' and id!='$id'";
            $check_brand = $this->Birthday->getUserData("voucher_amount", $where)->num_rows();
            if ($check_brand > 0) {
                $output["success"] = FALSE;
                $output["message"] = "Voucher with given PIN is already exist";
                echo json_encode($output);
                exit;
            }

            $where = "id='$id'";
            $this->Birthday->update("voucher_amount", $updated_data, $where);
            $output["success"] = true;
            $output["message"] = "Amount Updated Successfully!."; //If user name is not in the database
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function stock() {
        $data['title'] = "BirthdayOwl";
        $data["heading"] = "check_voucher_stock";
        $data["btn_click"] = "submit_amount";
        $data["voucher_amt"] = $this->Birthday->get_voucher_amount()->result_array();

        $data["vouchers"] = $this->Birthday->getAllData("voucher_product")->result_array();
        $this->load->view('admin_panel/voucher_Stock', $data);
    }

    public function get_voucher_amount() {
        if (count($_POST) > 0) {
            $voucher_pro_id = check_post($this->input->post("voucher_pro_id"));
            $where = "voucher_id='$voucher_pro_id'";
            $stock = $this->Birthday->get_distinct_data("voucher_amount", $where, "pamount")->result_array();
            if (count($stock) > 0) {
                $output["success"] = true;
                $output["voucher_amt"] = $stock;
            } else {
                $output["success"] = false;
                $output["voucher_amt"] = "0";
            }
            $output["success"] = true;

            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function check_stock() {
        if (count($_POST) > 0) {
            $pamount = check_post($this->input->post("pamount"));
            $voucher_pro_id = check_post($this->input->post("voucher_pro_id"));
            $stock = $this->Birthday->get_stock_vouchers($pamount, $voucher_pro_id)->result_array();

            if (count($stock) > 0) {
                $output["success"] = true;
                $output["vstock"] = $stock;
            } else {
                $output["success"] = false;
                $output["vstock"] = "0";
            }
            $output["success"] = true;

            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function add_product_voucher() {
        $filename = "";
        $upload = FALSE;
        if (!empty($_FILES)) {
            $tempFile = trim($_FILES['userfile']['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
            $filename = str_replace(" ", "_", $filename);
            $targetPath = 'public/voucher_product/';
            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = false;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES["userfile"]["error"];
                } else {
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $upload = TRUE;
                }
            } else {
                $output["filename"] = $filename;
                $output["success"] = true;
                $upload = TRUE;
            }
        }
        if (count($_POST) > 0) {
            $data["voucher_pro_name"] = ltrim(check_post($this->input->post("voucher_pro_name")));
            $data["voucher_url"] = str_replace(" ", "_", check_post($this->input->post("voucher_pro_name")));
            $data["pro_tagline"] = check_post($this->input->post("pro_tagline"));
            $data["keywords"] = check_post($this->input->post("keywords"));
            $data["voucher_cat_id"] = check_post($this->input->post("voucher_cat_id"));
            $data["keywords"] = check_post($this->input->post("keywords"));
            $data["description"] = check_post($this->input->post("description"));
            $data["terms_conditions"] = check_post($this->input->post("terms_conditions"));
            $data["alt_tag"] = check_post($this->input->post("alt_tag"));
            $data["title_tag"] = check_post($this->input->post("title_tag"));
            $data["redemption_details"] = check_post($this->input->post("redemption_details"));
            $data["locations"] = check_post($this->input->post("locations"));
            $data["product_image"] = $filename;
            $data["voucher_created_date"] = date("Y-m-d H:i:s");
            $voucher_pro_id = $this->Birthday->addData("voucher_product", $data);
            $amount = check_post($this->input->post("amount"));
            $count = count($amount);
            $pin = check_post($this->input->post("pin"));
            $expiry = check_post($this->input->post("expiry"));
            $code = check_post($this->input->post("code"));
            for ($i = 0; $i < $count; $i++) {
                $adata = array();
                $adata["voucher_id"] = $voucher_pro_id;
                $adata["pamount"] = $amount[$i];
                $adata["pin"] = $pin[$i];
                $adata["expiry_date"] = $expiry[$i];
                $adata["code"] = $code[$i];
                $adata["status"] = "1";
                $adata["created_date"] = date("Y-m-d H:i:s");
                $this->Birthday->addData("voucher_amount", $adata);
            }
            redirect(BASEURL . "Product_voucher/display_products");
        }
    }

    public function display_products() {
        if ($this->is_admin_logged_in()) {
            $data['title'] = "BirthdayOwl";
            $data["heading"] = "Manage Product Voucher";
            $data['voucher_product'] = $this->Birthday->_get_voucher_cat_pro()->result_array();
            $this->load->view('admin_panel/display_products', $data);
        }
    }

    public function check_voucher_product_name() {
        if (count($_POST) > 0) {
            $voucher_pro_name = ltrim(check_post($this->input->post("voucher_pro_name")));
            $where = "voucher_pro_name='$voucher_pro_name'";
            $check_user = $this->Birthday->getUserData("voucher_product", $where)->num_rows();
            if ($check_user > 0) {
                $output["success"] = false;
                $output["message"] = "Voucher Product Name already exist ";
            } else {
                $output["success"] = true;
                $output["message"] = "hello"; //If user name is not in the database
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function Edit_check_voucher_product_name() {
        if (count($_POST) > 0) {
            $voucher_pro_name = ltrim(check_post($this->input->post("voucher_pro_name")));
            $voucher_pro_id = check_post($this->input->post("voucher_pro_id"));
            $where = "voucher_pro_name='$voucher_pro_name' and voucher_pro_id!='$voucher_pro_id'";
            $check_user = $this->Birthday->getUserData("voucher_product", $where)->num_rows();
            if ($check_user > 0) {
                $output["success"] = false;
                $output["message"] = "Voucher Product Name already exist ";
            } else {
                $output["success"] = true;
                $output["message"] = "hello"; //If user name is not in the database
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function edit_voucher_product($voucher_pro_id) {
        $data = array();
        $where = "voucher_pro_id='$voucher_pro_id'";
        $data["heading"] = "Edit Product Voucher";
        $voucher_data = $this->Birthday->getUserData("voucher_product", $where)->row_array();
        $data["voucher_data"] = $voucher_data;
        $data["checkview"] = "2";
        $data['voucher_product'] = $this->Birthday->_get_voucher_cat_pro()->result_array();
        $where = "greeting_type=2";
        $data["greeting_card"] = $this->Birthday->getUserData("greeting_card", $where)->result_array();
        $where = "voucher_id='$voucher_pro_id' and status='1'";
        $data["voucher_amount"] = $this->Birthday->getUserData("voucher_amount", $where)->result_array();

        $data["btn_click"] = "edit_voucher_product";
        $data["id"] = "voucher_pro_name_edit";
        $data["title"] = "BirthdayOwl";
        $data['voucher_category'] = $this->Birthday->getAllData("voucher_category")->result_array();
        $data["action"] = "Product_voucher/voucherProduct_edit_success/$voucher_pro_id";
        $this->load->view('admin_panel/add_product_voucher', $data);
    }

    public function voucherProduct_edit_success($voucher_pro_id) {
        $filename = "";
        $upload = FALSE;
        if (!empty($_FILES)) {
            $tempFile = trim($_FILES['userfile']['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
            $filename = str_replace(" ", "_", $filename);
            $targetPath = 'public/voucher_product/';
            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = false;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES["userfile"]["error"];
                } else {
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $upload = TRUE;
                }
            } else {
                $output["filename"] = $filename;
                $output["success"] = true;
                $upload = TRUE;
            }
        }
        if (count($_POST) > 0) {

            $updated_data["voucher_pro_name"] = ltrim(check_post($this->input->post("voucher_pro_name")));
            $updated_data["voucher_url"] = str_replace(" ", "_", ltrim(check_post($this->input->post("voucher_pro_name"))));
            $updated_data["pro_tagline"] = check_post($this->input->post("pro_tagline"));
            $updated_data["keywords"] = check_post($this->input->post("keywords"));
            $updated_data["voucher_cat_id"] = check_post($this->input->post("voucher_cat_id"));
            $updated_data["keywords"] = check_post($this->input->post("keywords"));
            $updated_data["description"] = check_post($this->input->post("description"));
            $updated_data["terms_conditions"] = check_post($this->input->post("terms_conditions"));
            $updated_data["alt_tag"] = check_post($this->input->post("alt_tag"));
            $updated_data["title_tag"] = check_post($this->input->post("title_tag"));
            $updated_data["redemption_details"] = check_post($this->input->post("redemption_details"));
            $updated_data["locations"] = check_post($this->input->post("locations"));

            if ($filename != '') {
                $updated_data["product_image"] = $filename;
            } else {
                $updated_data["product_image"] = check_post($this->input->post("userfile_old"));
            }
            $where = "voucher_pro_id='$voucher_pro_id'";
            $this->Birthday->update("voucher_product", $updated_data, $where);

            $amount = check_post($this->input->post("amount"));
            $count = count($amount);

            $pin = check_post($this->input->post("pin"));
            $expiry = check_post($this->input->post("expiry"));
            $code = check_post($this->input->post("code"));
            $where = "voucher_id='$voucher_pro_id'";
            $delete_amount_data = $this->Birthday->getUserData("voucher_amount", $where)->result_array();
            foreach ($delete_amount_data as $amounts) {
                $voucher_pro_id = $amounts["voucher_id"];
                $where = "voucher_id='$voucher_pro_id'";
                $this->Birthday->delete_reminder("voucher_amount", $where);
            }

            for ($i = 0; $i < $count; $i++) {
                $adata = array();
                $adata["voucher_id"] = $voucher_pro_id;
                $adata["pamount"] = $amount[$i];
                $adata["pin"] = $pin[$i];
                $adata["expiry_date"] = $expiry[$i];
                $adata["code"] = $code[$i];
                $adata["status"] = "1";
                $adata["created_date"] = date("Y-m-d H:i:s");
                $where = "voucher_id='$voucher_pro_id'";
                $this->Birthday->addData("voucher_amount", $adata);
            }
            redirect(BASEURL . "Product_voucher/display_products");
        }
    }

    public function delete_voucher_product() {
        if (count($_POST) > 0) {
            $voucher_pro_id = check_post($this->input->post("voucher_pro_id"));
            $count = count($voucher_pro_id);
            for ($i = 0; $i < $count; $i++) {
                $where = "voucher_pro_id='$voucher_pro_id[$i]'";
                $this->Birthday->delete_reminder("voucher_product", $where);
                $where = "voucher_id='$voucher_pro_id[$i]'";
                $this->Birthday->delete_reminder("voucher_amount", $where);
                $where = "voucher_id='$voucher_pro_id[$i]'";
                $this->Birthday->delete_reminder("voucher_amount", $where);
            }

            $output["success"] = true;
            $output["message"] = "Voucher Product Deleted SuccessFully";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function is_admin_logged_in() {
        if ($this->session->userdata('admin_data') != NULL) {
            return true;
        } else {
            redirect(BASEURL . "Admin_panel/admin_login");
        }
    }

    public function admin_logout() {
        $this->session->sess_destroy();
        redirect(BASEURL . "Admin_panel/admin_login");
    }

    public function change_popularity() {
        if (count($_POST) > 0) {
            $voucher_pro_id = check_post($this->input->post("voucher_pro_id"));
            $update["click_counter"] = check_post($this->input->post("click_counter"));
            $where = "voucher_pro_id='$voucher_pro_id'";
            $updated = $this->Birthday->update("voucher_product", $update, $where);
            if ($updated == 1) {
                $output["success"] = true;
                $output["message"] = "";
            } else {
                $output["success"] = false;
                $output["message"] = "Fail to update";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

}
