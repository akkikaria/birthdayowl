<?php

//

Class Home_api extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $a = array("a" => "red", "b" => "green");
        array_push($a, "blue", "black");
        echo "<pre>";
        print_r($a);
    }

    //Used in registration process

    function createIndividualAccount() {
        //email,first_name,password,gender,bmonth,bdate,mobile_no,device_registration,platform
        $output = array();
        $output["url"] = BASEURL . "Home_api/createIndividualAccount";
        $postcount = count($_POST);
        if ($postcount > 0) {
            //email
            $im_details["email"] = trim(check_post($this->input->post("email")));
            $this->validate($im_details["email"], "Email id is required", $output, 1);
            $this->validate($im_details["email"], "Email id is invalid", $output, 3);
            //name
            $im_details["first_name"] = trim(check_post($this->input->post("first_name")));
            $this->validate($im_details["first_name"], "first_name is required", $output, 1);
            //normal default
            $im_details["fb_id"] = "";
            $im_details["g_id"] = "";
            $im_details["username"] = "";
            $im_details["login_with"] = "1";
            $im_details["user_profile_pic"] = "";
            $im_details["reset_time"] = date("Y-m-d H:i:s");
            $im_details["reset_link"] = 0;
            $im_details["reset_activate"] = 0;
            $im_details["sent_greeting_counter"] = 0;
            $im_details["membership_status"] = 0;
            //password
            $password = trim(check_post($this->input->post("password")));
            $im_details["password"] = trim(check_post($this->Birthday->encryptPassword($password)));
            $this->validate($password, "Password is required", $output, 1);
            $platform = check_post($this->input->post("platform"));
            $hash_key = check_post($this->input->post("hash_key"));
            //country code
            if (check_post($this->input->post("country_id")) != "")
                $im_details["country_id"] = $country_id = check_post($this->input->post("country_id"));
            else
                $im_details["country_id"] = $country_id = '102';

            $email = $im_details["email"];
            $where = "email='$email' and is_guest = 0 and mobile_verify = 1";
            $check_user = $this->Birthday->getSelectData("email", "users", $where)->num_rows();
            if ($check_user > 0) {
                $output["success"] = FALSE;
                $output["message"] = "Email-id already exists.";
                echo json_encode($output);
                exit;
            }
            //Delete the entry if the email id is there in the table but he has not verified the phone number
//            $where = "email='$email' and is_guest = 0 and is_google_login = 0 and mobile_verify = 0";
//            $check_user = $this->Birthday->getSelectData("user_id", "users", $where)->row_array();
//            if (!empty($check_user)) {
//                $id = $check_user['user_id'];
//                //if id is found then delete the entry for the particular id and continue
//                $where = "user_id = '$id'";
//                $this->Birthday->delete_reminder('users', $where);
//            }
            $im_details["gender"] = trim(check_post($this->input->post("gender")));
            $this->validate($im_details["gender"], "Gender  value is required", $output, 1);
            if ($im_details["gender"] != 1 && $im_details["gender"] != 2) {
                $output["success"] = FALSE;
                $output["message"] = "Gender value should be 1 or 2";
                echo json_encode($output);
                exit;
            }
            $im_details["bmonth"] = $m = trim(check_post($this->input->post("bmonth")));
            $this->validate($im_details["bmonth"], "Birth month is required", $output, 1);
            $dm = DateTime::createFromFormat('m', $m);
            if ($dm && $dm->format('m') == $m) {
                
            } else {
                $output["success"] = FALSE;
                $output["message"] = "Invalid month number";
                echo json_encode($output);
                exit;
            }

            $im_details["bdate"] = $d = trim(check_post($this->input->post("bdate")));
            $this->validate($im_details["bdate"], "Birth date is required", $output, 1);
            $im_details["byear"] = $y = trim(check_post($this->input->post("byear")));
            $im_details["mobile_no"] = $mobile_no = trim(check_post($this->input->post("mobile_no")));
            // $this->validate($im_details["mobile_no"], "Mobile Number is required", $output, 1);
//            if ($mobile_no != "") {
//                $where = "email='$email' and mobile_no='$mobile_no' and is_guest = 0 and is_google_login = 0 and mobile_verify=1";
//                $check_mobile = $this->Birthday->getSelectData("mobile_no", "users", $where)->num_rows();
//                if ($check_mobile > 0) {
//                    $output["success"] = FALSE;
//                    $output["message"] = "Mobile Number already exists.";
//                    echo json_encode($output);
//                    exit;
//                }
//            }


            if ($country_id == '102') {
                //Check Phone in DB whether it is exist or not
//                $updated_data["mobile_no"] = $user_data["mobile_no"] = $mobile_no;
                if ($mobile_no != "") {
//                    $where = "mobile_no='$mobile_no' and is_guest=0 and mobile_verify=1";
//                    $where = "email='$email' and mobile_no='$mobile_no' and is_guest = 0 and is_google_login = 0 and mobile_verify=1";
                    $where = "mobile_no='$mobile_no' and is_guest = 0 and mobile_verify = 1";
                    $check_mobile_no = $this->Birthday->getSelectData("mobile_no", "users", $where)->num_rows();
                    if ($check_mobile_no > 0) {
                        $output["success"] = false;
                        $output["message"] = "Mobile Number already exists.";
                        echo json_encode($output);
                        exit;
                    }
                }
            }

            if (ctype_space($m)) {
                $m = "";
            }
            if (ctype_space($d)) {
                $d = "";
            }
            if (ctype_space($y)) {
                $y = "";
            }

            if (($m != '') && ($d != '')) {
                $zodiac_info = $this->Birthday->getUserData("mapzodiac", "(fdate <='$d' and fmonth='$m') or (tdate>='$d' and tmonth ='$m')")->row_array();
                $im_details["zodiac_id"] = $zodiac_info["zodiac_id"];
            } else {
                $im_details["zodiac_id"] = '';
            }
            $im_details["birth_date"] = "$m/$d/$y";
            $im_details["created_date"] = date("Y-m-d H:i:s");
            $im_details["u_last_login"] = date("Y-m-d H:i:s");
            //validation are done till here
            //here we check the availabiliy of the data of user as already guest user
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->load->library("CheckAsGuest");
            $check_guest_exist = new CheckAsGuest();
            $check_guest_exist->setGuestEmail($email);
            $check_guest_exist->setCountry_code($country_id);
            $is_guest_exist = $check_guest_exist->runner();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->load->library("CheckAsGoogle");
            $check_google_user = new CheckAsGoogle();
            $check_google_user->setGoogleEmailId($email);
            $check_google_user->setCountry_code($country_id);
            $is_google_exist = $check_google_user->runner();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////the block check any one place either google or guest exit////////////////////////////////////                 
            $getUserId = '';
            if ($is_google_exist || $is_guest_exist) {
                $this->load->library("OtpSenderUpdater");
                $otp_sender = new OtpSenderUpdater();
                //update
//                $checked_once = TRUE;
//                if ($is_guest_exist) {
//                    $checked_once = FALSE;
//                    $getUserId = $check_guest_exist->getUserId();
//                    $check_guest_exist->update($im_details);
//                    $sameMoble = $check_guest_exist->checkMobileAreSame();
//                    $checkCountryCode = $check_guest_exist->CheckCountryCode();
//
//                    if ($sameMoble || $checkCountryCode) {
////                        $output['success'] = TRUE;
////                        $output['message'] = "Signed Up Successfully";
////                        $output["user_id"] = $getUserId;
//                        $check_guest_exist->resetGoogleGuest();
//                        $user_info = $check_guest_exist->getUserInfo();
//                        $output['trigger_otp'] = 0;
//                        $output['user_info'] = $user_info;
//                    } else {
//                        $otp_sender->setMobileNumber($im_details["mobile_no"]);
//                        $otp_sender->runner();
//                        $user_info = $check_guest_exist->getUserInfo();
//                        $output['trigger_otp'] = 1;
//                        $output['user_info'] = $user_info;
//                    }
//
////                    $data["email"] = $email = $im_details["email"];
////                    $data["username"] = $first_name = $im_details["first_name"];
////                    $data["mobile_no"] = $im_details["mobile_no"];
////                    $view = $this->load->view("email_template/registered_success", $data, true);
////                    $result = send_EmailResponse($view, $email, $first_name, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");
////
////                    //To Support Birthday Owl
////                    $view1 = $this->load->view("email_template/register_support", $data, true);
////                    send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $email($first_name).");
//                }
//
//                if ($is_google_exist && $checked_once) {
//                    $getUserId = $check_google_user->getUserId();
//                    $check_google_user->update($im_details);
//                    if ($check_google_user->CheckCountryCode()) {
//                        $check_google_user->reset_Google_Guest();
//                        $user_info = $check_google_user->getUserInfo();
//                        $output['trigger_otp'] = 0;
//                        $output['user_info'] = $user_info;
//                    } else {
//                        $otp_sender->setMobileNumber($im_details["mobile_no"]);
//                        $otp_sender->runner();
//                        $user_info = $check_google_user->getUserInfo();
//                        $output['trigger_otp'] = 1;
//                        $output['user_info'] = $user_info;
//                    }
//
//
//
////                    $data["email"] = $email = $im_details["email"];
////                    $data["username"] = $first_name = $im_details["first_name"];
////                    $data["mobile_no"] = $im_details["mobile_no"];
////                    $view = $this->load->view("email_template/registered_success", $data, true);
////                    $result = send_EmailResponse($view, $email, $first_name, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");
////
////                    //To Support Birthday Owl
////                    $view1 = $this->load->view("email_template/register_support", $data, true);
////                    send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $email($first_name).");
//                }
                switch (true) {
                    case ($is_guest_exist):
                        $getUserId = $check_guest_exist->getUserId();
                        $check_guest_exist->update($im_details);
                        $sameMoble = $check_guest_exist->checkMobileAreSame();
                        $checkCountryCode = $check_guest_exist->CheckCountryCode();

                        if ($sameMoble || $checkCountryCode) {
//                        $output['success'] = TRUE;
//                        $output['message'] = "Signed Up Successfully";
//                        $output["user_id"] = $getUserId;
                            $check_guest_exist->resetGoogleGuest();
                            $user_info = $check_guest_exist->getUserInfo();
                            $output['trigger_otp'] = 0;
                            $output['user_info'] = $user_info;
                        } else {
                            $otp_sender->setMobileNumber($im_details["mobile_no"]);
                            $otp_sender->setHashKey($hash_key);
                            $otp_sender->runner();
                            $user_info = $check_guest_exist->getUserInfo();
                            $output['trigger_otp'] = 1;
                            $output['user_info'] = $user_info;
                        }
                        break;
                    case ($is_google_exist):
                        $getUserId = $check_google_user->getUserId();
                        $check_google_user->update($im_details);
                        if ($check_google_user->CheckCountryCode()) {
                            $check_google_user->reset_Google_Guest();
                            $user_info = $check_google_user->getUserInfo();
                            $output['trigger_otp'] = 0;
                            $output['user_info'] = $user_info;
                        } else {
                            $otp_sender->setMobileNumber($im_details["mobile_no"]);
                            $otp_sender->setHashKey($hash_key);
                            $otp_sender->runner();
                            $user_info = $check_google_user->getUserInfo();
                            $output['trigger_otp'] = 1;
                            $output['user_info'] = $user_info;
                        }
                        break;
                }

                $output["message"] = "Registered successfully";
                $output["success"] = TRUE;
                $output["user_id"] = $getUserId;
                $output["plans"] = array();
                $output["plan_info"] = array();
            }
            /////////////////////////////////////////////////////////////////////////////////////block end ////////////////////////////////////////////////////////////////////////////////
            else {

                // now delete if there is any email whose mobile verify = 0 form this email just to avoid multiple entries.
                $this->load->library("DeleteInvalidGuest");
                $deleteInvalidGuest = new DeleteInvalidGuest();
                $deleteInvalidGuest->setEmail($email);
                $deleteInvalidGuest->setPhoneNumber($mobile_no);
                $deleteInvalidGuest->runner();

                $this->load->library("DeleteInvalidBirthdayOwlUser");
                $deleteInvalidBirthdayowlUser = new DeleteInvalidBirthdayOwlUser();
                $deleteInvalidBirthdayowlUser->setEmail($email);
                $deleteInvalidBirthdayowlUser->setPhoneNumber($mobile_no);
                $deleteInvalidBirthdayowlUser->runner();

                //insert
                $this->load->library("OtpSenderUpdater");
                $otp_sender = new OtpSenderUpdater();

                if ($country_id != 102) {
                    $im_details['mobile_verify'] = 1;
                    $im_details['is_guest'] = 0;
                    $im_details['is_google_login'] = 0;
                    $im_details['platform'] = $platform;

                    $user_id = $this->Birthday->addData("users", $im_details);
                    $getUserId = $user_id;
                    $user_info = $this->Birthday->getSelectData("*", "users", "user_id = '$getUserId'")->row_array();
                    $output['user_info'] = $user_info;
                    $output['trigger_otp'] = 0;
                    $output["plans"] = "";
                    $output["plan_info"] = "";
                } else {
                    $im_details['platform'] = $platform;
                    $user_id = $this->Birthday->addData("users", $im_details);
                    $getUserId = $user_id;
                    $user_info = $this->Birthday->getSelectData("*", "users", "user_id = '$getUserId'")->row_array();
                    $otp_sender->setMobileNumber($im_details["mobile_no"]);
                    $otp_sender->setHashKey($hash_key);
                    $otp_sender->runner();
                    $output['user_info'] = $user_info;
                    $output['trigger_otp'] = 1;
                    $output["plans"] = array();
                    $output["plan_info"] = array();
                }


//                $data["email"] = $email = $im_details["email"];
//                $data["username"] = $first_name = $im_details["first_name"];
//                $data["mobile_no"] = $im_details["mobile_no"];
                $output["user_id"] = $getUserId;
                $output["message"] = "Registered successfully";
                $output["success"] = TRUE;
                $output["plans"] = array();
                $output["plan_info"] = array();


//                $view = $this->load->view("email_template/registered_success", $data, true);
//                $result = send_EmailResponse($view, $email, $first_name, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");
//
//                //To Support Birthday Owl
//                $view1 = $this->load->view("email_template/register_support", $data, true);
//                send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $email($first_name).");
            }

//            $user_id = $this->Birthday->addData("users", $im_details);
            $ndr_data["user_id"] = $getUserId;
            $ndr_data["device_registration"] = $reg_id = check_post($this->input->post("device_registration"));
            $ndr_data["platform"] = $platform = check_post($this->input->post("platform"));
            $check_ndr = $this->Birthday->getSelectData("device_registration", "notify_device_registartion", "user_id = '$getUserId' and device_registration = '$reg_id' and platform = $platform")->result_array();
            if (count($check_ndr) == 0 && $reg_id != "0" && $reg_id != "") {
                $this->Birthday->addData("notify_device_registartion", $ndr_data);
            }
            $new_data["email_id"] = $email;
            $new_data["first_name"] = $im_details["first_name"];
            $new_data["nimage_name"] = "";
            $this->Birthday->addData("news_letter", $new_data);
//            $where = "user_id='$user_id'";
//            $user_searched_data = $this->Birthday->getSelectData("*", "users", $where)->result_array();
//            $user_data = $user_searched_data[0];
//            $data["email"] = $user_data["email"];
//            $data["realName"] = $data["username"] = $firstname = $user_data["first_name"];
//            $data["password"] = $this->Birthday->decryptPassword($user_data["password"]);
//            $view = $this->load->view("email_template/registered_success", $data, true);
//            send_Email($view, $email, $firstname, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");
//            $output["message"] = "Registered successfully";
//            $output["success"] = TRUE;
//            $output["user_id"] = $getUserId;
//            $where = "guest_email_id='$email'";
//            $check_user_guest = $this->Birthday->getSelectData("*", "guest_user", $where)->result_array();
//            if (count($check_user_guest) > 0) { //
//                $user_id = $check_user_guest[0]['user_id'];
//                $where = "user_id=$user_id";
//                $check_user_guest_data = $this->Birthday->getSelectData("*", "users", $where)->result_array();
//                $mobile_number = $check_user_guest_data[0]['mobile_no'];
//                if ($mobile_number == $mobile_no) {//Check whether post mobile no & user tables mobile no is same or not, if same then don't send OTP
//                    $where = "user_id='$user_id'";
//                    $updated_data["is_guest"] = 0;
//                    $updated = $this->Birthday->update("users", $im_details, $where);
//
//                    $output["success"] = true;
//                    //To User
//                    $view = $this->load->view("email_template/registered_success", $data, true);
//                    $result = send_EmailResponse($view, $email, $first_name, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");
//
//                    //To Support Birthday Owl
//                    $view1 = $this->load->view("email_template/register_support", $data, true);
//                    send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $email($first_name).");
////                    if ($signup_type == 0) {
////                        $output["message"] = "Registered successfully";
////                    }
////                    if ($result["code"] == "success") {
////                        $output["success"] = true;
////                    } else {
////                        $output["success"] = false;
////                    }
////                    $output["is_converted"] = 1;
//                } else {
//                    //==========================OTP==========================
//                    if ($country_id == 102) {                    //If country id is INDIA then send OTP
//                        $data["code"] = $code = $this->generateRandomString(5);
//                        $data["mobile_no"] = $mobile_no;
//                        $otp_data = $this->Birthday->getSelectData("mobile_no", "otp", "mobile_no=$mobile_no")->result_array();
//                        $count = count($otp_data);
//                        if ($count > 0) {
//                            $update["code"] = $code;
//                            $update["sent_time"] = date("Y-m-d H:i:s");
//                            $this->Birthday->update("otp", $update, "mobile_no='$mobile_no'");
//                        } else {
//                            $data["created_date"] = date("Y-m-d H:i:s");
//                            $data["sent_time"] = date("Y-m-d H:i:s");
//                            $this->Birthday->addData("otp", $data);
//                        }
//                        $show_message = "Enter $code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone.";
//                        try {
//                            $resp = $this->Birthday->send_mobile_otp($mobile_no, $show_message);
//                            $output["success"] = true;
////                $output["message"] = "OTP sent successfully";
//                        } catch (Exception $e) {
//                            $output["success"] = false;
////                $output["message"] = "Failed.Please try again";
//                        }
//                        $where = "user_id='$user_id'";
//                        $check_user = $this->Birthday->getUserData("users", $where)->result_array();
//                        $output["userdata"] = $check_user[0];
//
//                        $where = "user_id='$user_id'";
//                        $updated_data["mobile_verify"] = 0; //Update mobile no in user table to 0 because new mobile no for guest is updated
//                        $updated = $this->Birthday->update("users", $im_details, $where);
//
////                        $output["is_converted"] = 0;
//                    } else {                    //Else don't send OTP
//                        $where = "user_id='$user_id'";
//                        $updated_data["is_guest"] = 0;
//                        $updated = $this->Birthday->update("users", $im_details, $where);
//                        $output["success"] = true;
//                        $where = "user_id='$insert_user_id'";
//                        $check_user = $this->Birthday->getUserData("users", $where)->result_array();
//                        $data["email"] = $email = $check_user[0]["email"];
//                        $data["username"] = $first_name = $check_user[0]["first_name"];
//                        $data["mobile_no"] = $check_user[0]["mobile_no"];
//                        //To User
//                        $view = $this->load->view("email_template/registered_success", $data, true);
//                        $result = send_EmailResponse($view, $email, $first_name, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");
//
//                        //To Support Birthday Owl
//                        $view1 = $this->load->view("email_template/register_support", $data, true);
//                        send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $email($first_name).");
//                        if ($signup_type == 0) {
//                            $output["message"] = "Registered successfully";
//                        }
//                        if ($result["code"] == "success") {
//                            $output["success"] = true;
//                        } else {
//                            $output["success"] = false;
//                        }
//                        $output["is_converted"] = 1;
//                    }
//                }
//            } else {
//                $output["is_converted"] = 0;
//                $insert_user_id = $this->Birthday->addData("users", $im_details);
////                $this->Birthday->addData("news_letter", $new_data);
////==========================OTP==========================
//                if ($country_id == 102) {
//                    $data["code"] = $code = $this->generateRandomString(5);
//                    $data["mobile_no"] = $mobile_no;
//                    $otp_data = $this->Birthday->getSelectData("mobile_no", "otp", "mobile_no=$mobile_no")->result_array();
//                    $count = count($otp_data);
//                    if ($count > 0) {
//                        $update["code"] = $code;
//                        $update["sent_time"] = date("Y-m-d H:i:s");
//                        $this->Birthday->update("otp", $update, "mobile_no='$mobile_no'");
//                    } else {
//                        $data["created_date"] = date("Y-m-d H:i:s");
//                        $data["sent_time"] = date("Y-m-d H:i:s");
//                        $this->Birthday->addData("otp", $data);
//                    }
//                    $show_message = "Enter $code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone.";
//                    try {
//                        $resp = $this->Birthday->send_mobile_otp($mobile_no, $show_message);
//                        $output["success"] = true;
////                $output["message"] = "OTP sent successfully";
//                    } catch (Exception $e) {
//                        $output["success"] = false;
////                $output["message"] = "Failed.Please try again";
//                    }
//                    $where = "user_id='$insert_user_id'";
//                    $check_user = $this->Birthday->getUserData("users", $where)->result_array();
//                } else {
//                    $output["success"] = true;
//                    $where = "user_id='$insert_user_id'";
//                    $check_user = $this->Birthday->getUserData("users", $where)->result_array();
//                    $data["email"] = $email = $check_user[0]["email"];
//                    $data["username"] = $first_name = $check_user[0]["first_name"];
//                    $data["mobile_no"] = $check_user[0]["mobile_no"];
//                    //To User
//                    $view = $this->load->view("email_template/registered_success", $data, true);
//                    $result = send_EmailResponse($view, $email, $first_name, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");
//
//                    //To Support Birthday Owl
//                    $view1 = $this->load->view("email_template/register_support", $data, true);
//                    send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $email($first_name).");
////                    if ($signup_type == 0) {
////                        $output["message"] = "Registered successfully";
////                    }
////                    if ($result["code"] == "success") {
////                        $output["success"] = true;
////                    } else {
////                        $output["success"] = false;
////                    }
//                }
////==========================OTP==========================
//            }
//            $this->output->set_content_type('application/json');
//            $this->output->set_output(json_encode($output));
//
//
//            $user_id = $this->Birthday->addData("users", $im_details);
//            $ndr_data["user_id"] = $user_id;
//            $ndr_data["device_registration"] = $reg_id = check_post($this->input->post("device_registration"));
//            $ndr_data["platform"] = $platform = check_post($this->input->post("platform"));
//            $check_ndr = $this->Birthday->getSelectData("device_registration", "notify_device_registartion", "user_id = $user_id and device_registration = '$reg_id' and platform = $platform")->result_array();
//            if (count($check_ndr) == 0 && $reg_id != "0" && $reg_id != "") {
//                $this->Birthday->addData("notify_device_registartion", $ndr_data);
//            }
//            $new_data["email_id"] = $email;
//            $new_data["first_name"] = $im_details["first_name"];
//            $new_data["nimage_name"] = "";
//            $this->Birthday->addData("news_letter", $new_data);
//            $where = "user_id='$user_id'";
//            $select = "users.user_id,users.first_name,users.password,users.email,users.mobile_no,users.birth_date,users.gender,users.bdate,users.bmonth,users.byear,users.fb_id,users.g_id,users.login_with,users.user_profile_pic,users.sent_greeting_counter,users.membership_status,country.country_name,users.country_id";
//            $user_data = $this->Birthday->getJoinedData2($select, "users", "country", "users.country_id=country.country_id", $where)->row_array();
//            $newsletter_data = $this->Birthday->getSelectData("mail_status", "news_letter", "email_id='$email'")->row_array();
//            $user_data["mail_status"] = $newsletter_data["mail_status"];
//            $output["user_info"] = $user_data;
//            $plans = $this->Birthday->getSelectData("amount,duration", "membership_plans", "status=1")->result_array();
//            if ($user_data["membership_status"] == 1) {
//                $selected = "greeting_membership.membership_id,greeting_membership.plan_purchase_type,greeting_membership.expiry_date";
//                $where = "greeting_membership.user_id=$user_id and greeting_membership.transaction_status='success'";
//                $planinfo = $this->Birthday->getSelectData($selected, "greeting_membership", $where)->result_array();
//                $output["plan_info"] = $planinfo;
//                $output["plans"] = $plans;
//            } else {
//                $output["plans"] = $plans;
//                $output["plan_info"] = array();
//            }
//            $user_searched_data = $this->Birthday->getSelectData("*", "users", $where)->result_array();
//            $user_data = $user_searched_data[0];
//            $data["email"] = $user_data["email"];
//            $data["realName"] = $data["username"] = $firstname = $user_data["first_name"];
//            $data["password"] = $this->Birthday->decryptPassword($user_data["password"]);
//            $view = $this->load->view("email_template/registered_success", $data, true);
//            send_Email($view, $email, $firstname, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");
//            $output["message"] = "Registered successfully";
//            $output["success"] = TRUE;
        } else {
            $output["success"] = FALSE;
            $output["error_no"] = 1;
            $output["message"] = "No input data found.";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    //Common function for validation
    function validate($var, $message, $output, $type) {
        switch ($type) {
            case 1: {
                    if (empty($var)) {
                        $output["success"] = FALSE;
                        $output["message"] = $message;
                        echo json_encode($output);
                        exit;
                    }
                }
                break;
            case 2: {
                    if (($var != 1) && ($var != 2) && ($var != 3) && ($var != 4)) {
                        $output["success"] = FALSE;
                        $output["message"] = $message;
                        echo json_encode($output);
                        exit;
                    }
                }
                break;
            case 3: {
                    if (!filter_var($var, FILTER_VALIDATE_EMAIL) || preg_match('/[\'\`]/', $var)) {
                        $output["success"] = FALSE;
                        $output["message"] = $message;
                        echo json_encode($output);
                        exit;
                    }
                }
                break;
        }
    }

    //checking whether user has been purchased membership or not
    public function check_membership_status() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $output = array();
            $user_id = check_post($this->input->post("user_id"));
            $this->validate($user_id, "user_id is required", $output, 1);
            $check_user_id = $this->Birthday->getSelectData("user_id", "users", "user_id='$user_id'")->num_rows();
            if ($check_user_id == 0) {
                $output["success"] = FALSE;
                $output["message"] = "user_id does not exist";
                echo json_encode($output);
                exit;
            }
            $planinfo = array();
            $output["url"] = BASEURL . "Home_api/check_membership_status";
            $user_info = $this->Birthday->getSelectData("membership_status", "users", "user_id='$user_id'")->row_array();
            $plans = $this->Birthday->getSelectData("amount,duration", "membership_plans", "status=1")->result_array();
            $data["membership_status"] = $user_info["membership_status"];
            if ($data["membership_status"] == 1) {
                //1 means user has purchased membership
                $selected = "greeting_membership.membership_id,greeting_membership.plan_purchase_type,greeting_membership.expiry_date";
                $on = "membership_plans.duration=greeting_membership.plan_purchase_type";
                $where = "membership_plans.status=1 and greeting_membership.user_id=$user_id";
                $planinfo = $this->Birthday->getSelectedTwoTableJoin($selected, "greeting_membership", "membership_plans", $where, $on)->result_array();
                $output["plan_info"] = $planinfo;
                $output["success"] = true;
                $output["mstatus"] = $data["membership_status"];
                $output["plans"] = $plans;
            } else {
                $output["plans"] = $plans;
                $output["plan_info"] = "";
                $output["success"] = FALSE;
                $output["mstatus"] = $data["membership_status"];
            }

            if ($user_id == "") {
                $output["plans"] = $plans;
                $output["plan_info"] = "";
                $output["success"] = FALSE;
                $output["mstatus"] = "";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

//Normal/fb/gmail/guest login. Depending on login_with value condition will evaluate. 
    public function login_old() {
//first_name,email,password,login_with,fb_id,g_id,gender,bmonth,bdate,byear,device_registration,platform
        $output = array();
        $output["url"] = BASEURL . "Home_api/login";
        $postcount = count($_POST);
        if ($postcount > 0) {
            $new_data["first_name"] = $updated_data['first_name'] = $sdata['first_name'] = $first_name = check_post($this->input->post("first_name"));
            $new_data["email_id"] = $updated_data['email'] = $sdata['email'] = $email = trim(check_post($this->input->post("email")));
            if ($email != "") {
                $newsletter_data = $this->Birthday->getSelectData("mail_status", "news_letter", "email_id='$email'")->row_array();
            }

            $is_guest = check_post($this->input->post("is_guest"));
            $password = check_post($this->input->post("password"));
            $updated_data['login_with'] = $sdata['login_with'] = $loginwith = check_post($this->input->post("login_with"));
            if ($loginwith == 1 || $loginwith == 2 || $loginwith == 3) {
                $this->validate($email, "Email address is required", $output, 1);
                $this->validate($email, "Email id is invalid", $output, 3);
            }
            $fb_id = check_post($this->input->post("fb_id"));
            $g_id = check_post($this->input->post("g_id"));
            $gender = check_post($this->input->post("gender"));
            $mb_no = check_post($this->input->post("mobile_no"));
            if ($mb_no == "") {
                $sdata["mobile_no"] = "";
            } else {
                $sdata["mobile_no"] = $mb_no;
            }
            if ($gender == "") {
                $sdata["gender"] = 1;
            } else {
                $sdata["gender"] = $gender;
            }
            $m = check_post($this->input->post("bmonth"));
            $b = check_post($this->input->post("bdate"));
            $y = check_post($this->input->post("byear"));
            if (ctype_space($m)) {
                $m = "";
            }
            if (ctype_space($b)) {
                $b = "";
            }
            if (ctype_space($y)) {
                $y = "";
            }
            if ($is_guest != "")
                $sdata['is_guest'] = $is_guest;
            else {
                $sdata['is_guest'] = 0;
            }
            $sdata["username"] = "";
            $sdata["password"] = "";
            $sdata["user_profile_pic"] = "";
            $sdata["reset_time"] = date("Y-m-d H:i:s");
            $sdata["reset_link"] = 0;
            $sdata["reset_activate"] = 0;
            $sdata["sent_greeting_counter"] = 0;
            $sdata["membership_status"] = 0;
            $sdata["mobile_no"] = check_post($this->input->post("mobile_no"));
            $sdata["created_date"] = date("Y-m-d H:i:s");
            $month = date("m");
            $reg_id = check_post($this->input->post("device_registration"));
            $platform = check_post($this->input->post("platform"));
            $pass = $this->Birthday->encryptPassword($password);
            $this->validate($loginwith, "login_with is required", $output, 1);
            $this->validate($loginwith, "login_with value should be 1,2,3 or 4", $output, 2);
            $select = "users.user_id,users.first_name,users.password,users.email,users.mobile_no,users.birth_date,users.gender,users.bdate,users.bmonth,users.byear,users.fb_id,users.g_id,users.login_with,users.user_profile_pic,users.sent_greeting_counter,users.membership_status,users.is_guest";
            switch ($loginwith) {
                case "1": {
                        if ($is_guest == 1) {
                            $sdata['g_id'] = "";
                            $sdata["zodiac_id"] = 0;
                            $sdata["bmonth"] = '';
                            $sdata["bdate"] = '';
                            $sdata["birth_date"] = "";
                            $sdata["fb_id"] = "";
                            $insert_id = $this->Birthday->addData("users", $sdata);
                            $where = "user_id='$insert_id'";
                            $user_info = $this->Birthday->getSelectData($select, "users", $where)->row_array();

                            $output["user_info"] = $user_info;
                        } else {
                            $this->validate($password, "Password is required", $output, 1);
                            $check_email = $this->Birthday->getSelectData($select, "users", "email='$email'")->row_array();
                            if (count($check_email) == "0") {
                                $output["success"] = FALSE;
                                $output["message"] = "Account is not registered using above email-id ";
                                echo json_encode($output);
                                exit;
                            }
                            $where = "email='$email' and password='$pass' and is_guest=0";
                            $user_info = $this->Birthday->getSelectData($select, "users", $where)->row_array();

                            if (count($user_info) == "0") {
                                $output["success"] = FALSE;
                                $output["message"] = "Invalid email-id or password";
                                echo json_encode($output);
                                exit;
                            } else {
                                $user_details_update["u_last_login"] = date("Y-m-d H:i:s");
                                $this->Birthday->update("users", $user_details_update, "email='$email'");
                            }
                            $user_info["mail_status"] = $newsletter_data["mail_status"];
                            $output["user_info"] = $user_info;
                        }
                    }
                    break;
                case "2":  //2-facebook
                case "3": { //3-gmail
                        $this->validate($first_name, "First Name is required", $output, 1);

                        if ($loginwith == 2) {
                            $this->validate($fb_id, "Facebook id is required", $output, 1);
                            $sdata['g_id'] = "";
                            $sdata['fb_id'] = $updated_data['fb_id'] = $fb_id;
                        } else {
                            $this->validate($g_id, "Google id is required", $output, 1);
                            $sdata['fb_id'] = "";
                            $sdata['g_id'] = $updated_data['g_id'] = $g_id;
                        }
                        if (($m != '') && ($b != '')) {

                            $zodiac_info = $this->Birthday->getUserData("mapzodiac", "(fdate <='$b' and fmonth='$m') or (tdate>='$b' and tmonth ='$m')")->row_array();

                            $sdata["zodiac_id"] = $zodiac_info["zodiac_id"];
                            $sdata["bmonth"] = $m;
                            $sdata["bdate"] = $b;
                            $sdata["byear"] = $y;
                            $sdata["birth_date"] = "$m/$b/$y";
                        } else {
                            $sdata["zodiac_id"] = 0;
                            $sdata["bmonth"] = '';
                            $sdata["bdate"] = '';
                            $sdata["byear"] = "";
                            $sdata["birth_date"] = "";
                        }

                        $where = "email='$email'";
                        $query = $this->Birthday->getSelectData("email", "users", $where)->result_array();
                        $sdata["u_last_login"] = $updated_data["u_last_login"] = date("Y-m-d H:i:s");

                        if (count($query) > 0) {
                            $updated = $this->Birthday->update("users", $updated_data, $where);
                            if ($updated == 1) {
                                $user_info = $this->Birthday->getSelectData($select, "users", $where)->row_array();
                            }
                        } else {
                            $insert_id = $this->Birthday->addData("users", $sdata);
                            $new_data["nimage_name"] = "";
                            $this->Birthday->addData("news_letter", $new_data);
                            $where = "user_id='$insert_id'";
                            $user_info = $this->Birthday->getSelectData($select, "users", $where)->row_array();
                        }

                        $user_info["mail_status"] = $newsletter_data["mail_status"];
                        $output["user_info"] = $user_info;
                    }
                    break;

                case "4": { //login after deep linking
                        $user_id = check_post($this->input->post("user_id"));
                        $this->validate($user_id, "user_id is required", $output, 1);
                        $where = "user_id='$user_id'";
                        $user_info = $this->Birthday->getSelectData($select, "users", $where)->row_array();
                        if (count($user_info) == 0) {
                            $output["success"] = FALSE;
                            $output["message"] = "user_id does not exist";
                            echo json_encode($output);
                            exit;
                        }
                        $email = $user_info["email"];
                        $newsletter_data = $this->Birthday->getSelectData("mail_status", "news_letter", "email_id='$email'")->row_array();
                        $user_info["mail_status"] = $newsletter_data["mail_status"];
                        $output["user_info"] = $user_info;
                    }
                    break;
            }

            $ndr_data["user_id"] = $data["user_id"] = $user_id = $user_info["user_id"];
            $plans = $this->Birthday->getSelectData("amount,duration", "membership_plans", "status=1")->result_array();
            $membership_status = $user_info["membership_status"];
            if ($membership_status == 1) {
                $selected = "greeting_membership.membership_id,greeting_membership.plan_purchase_type,greeting_membership.expiry_date";
                $where = "greeting_membership.user_id=$user_id and greeting_membership.transaction_status='success'";
                $planinfo = $this->Birthday->getSelectData($selected, "greeting_membership", $where)->result_array();
                $output["plan_info"] = $planinfo;
                $output["plans"] = $plans;
            } else {
                $output["plans"] = $plans;
                $output["plan_info"] = array();
            }


            if ($platform == 1) {
                $data["ios_device_id"] = $reg_id;
                $data["android_device_id"] = "";
            } else {
                $data["ios_device_id"] = "";
                $data["android_device_id"] = $reg_id;
            }
            $ndr_data["platform"] = $data["platform"] = $platform;
            $data["registration_date"] = date("Y-m-d H:i:s");
            $this->Birthday->addData("users_device_registration", $data);
            $ndr_data["device_registration"] = $reg_id;

            $check_ndr = $this->Birthday->getSelectData("ndr_id", "notify_device_registartion", "user_id = $user_id and device_registration = '$reg_id' and platform = $platform")->result_array();
            if (count($check_ndr) == 0 && $reg_id != "0" && $reg_id != "") {
                $this->Birthday->addData("notify_device_registartion", $ndr_data);
            }
            $output["success"] = TRUE;
            $output["message"] = "Login Successfully!";
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found.";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function login_new() {
//first_name,email,password,login_with,fb_id,g_id,gender,bmonth,bdate,byear,device_registration,platform
        $output = array();
        $output["url"] = BASEURL . "Home_api/login";
        $postcount = count($_POST);
        if ($postcount > 0) {
            $new_data["first_name"] = $updated_data['first_name'] = $sdata['first_name'] = $first_name = check_post($this->input->post("first_name"));
            $new_data["email_id"] = $updated_data['email'] = $sdata['email'] = $email = trim(check_post($this->input->post("email")));
            if ($email != "") {
                $newsletter_data = $this->Birthday->getSelectData("mail_status", "news_letter", "email_id='$email'")->row_array();
            }

            $is_guest = check_post($this->input->post("is_guest"));
            $password = check_post($this->input->post("password"));
            $updated_data['login_with'] = $sdata['login_with'] = $loginwith = check_post($this->input->post("login_with"));
            if ($loginwith == 1 || $loginwith == 2 || $loginwith == 3) {
                $this->validate($email, "Email address is required", $output, 1);
                $this->validate($email, "Email id is invalid", $output, 3);
            }
            $fb_id = check_post($this->input->post("fb_id"));
            $g_id = check_post($this->input->post("g_id"));
            $gender = check_post($this->input->post("gender"));
            $mb_no = check_post($this->input->post("mobile_no"));
            if ($mb_no == "") {
                $sdata["mobile_no"] = "";
            } else {
                $sdata["mobile_no"] = $mb_no;
            }
            if ($gender == "") {
                $sdata["gender"] = 1;
            } else {
                $sdata["gender"] = $gender;
            }
            $m = check_post($this->input->post("bmonth"));
            $b = check_post($this->input->post("bdate"));
            $y = check_post($this->input->post("byear"));
            if (ctype_space($m)) {
                $m = "";
            }
            if (ctype_space($b)) {
                $b = "";
            }
            if (ctype_space($y)) {
                $y = "";
            }
            if ($is_guest != "")
                $sdata['is_guest'] = $is_guest;
            else {
                $sdata['is_guest'] = 0;
            }
            $sdata["username"] = "";
            $sdata["password"] = "";
            $sdata["user_profile_pic"] = "";
            $sdata["reset_time"] = date("Y-m-d H:i:s");
            $sdata["reset_link"] = 0;
            $sdata["reset_activate"] = 0;
            $sdata["sent_greeting_counter"] = 0;
            $sdata["membership_status"] = 0;
            $sdata["mobile_no"] = check_post($this->input->post("mobile_no"));
            $sdata["created_date"] = date("Y-m-d H:i:s");
            $month = date("m");
            $reg_id = check_post($this->input->post("device_registration"));
            $platform = check_post($this->input->post("platform"));
            $pass = $this->Birthday->encryptPassword($password);
            $this->validate($loginwith, "login_with is required", $output, 1);
            $this->validate($loginwith, "login_with value should be 1,2,3 or 4", $output, 2);
            $select = "users.user_id,users.first_name,users.password,users.email,users.mobile_no,users.birth_date,users.gender,users.bdate,users.bmonth,users.byear,users.fb_id,users.g_id,users.login_with,users.user_profile_pic,users.sent_greeting_counter,users.membership_status,users.is_guest";
            switch ($loginwith) {
                case "1": {
                        if ($is_guest == 1) {
                            $sdata['g_id'] = "";
                            $sdata["zodiac_id"] = 0;
                            $sdata["bmonth"] = '';
                            $sdata["bdate"] = '';
                            $sdata["birth_date"] = "";
                            $sdata["fb_id"] = "";
                            $insert_id = $this->Birthday->addData("users", $sdata);
                            $where = "user_id='$insert_id'";
                            $user_info = $this->Birthday->getSelectData($select, "users", $where)->row_array();

                            $output["user_info"] = $user_info;
                        } else {
                            $this->validate($password, "Password is required", $output, 1);
                            $check_email = $this->Birthday->getSelectData($select, "users", "email='$email'")->row_array();
                            if (count($check_email) == "0") {
                                $output["success"] = FALSE;
                                $output["message"] = "Account is not registered using above email-id ";
                                echo json_encode($output);
                                exit;
                            }
                            $where = "email='$email' and password='$pass' and is_guest=0";
                            $user_info = $this->Birthday->getSelectData($select, "users", $where)->row_array();

                            if (count($user_info) == "0") {
                                $output["success"] = FALSE;
                                $output["message"] = "Invalid email-id or password";
                                echo json_encode($output);
                                exit;
                            } else {
                                $user_details_update["u_last_login"] = date("Y-m-d H:i:s");
                                $this->Birthday->update("users", $user_details_update, "email='$email'");
                            }
                            $user_info["mail_status"] = $newsletter_data["mail_status"];
                            $output["user_info"] = $user_info;
                        }
                    }
                    break;
                case "2":  //2-facebook
                case "3": { //3-gmail
                        $this->validate($first_name, "First Name is required", $output, 1);

                        if ($loginwith == 2) {
                            $this->validate($fb_id, "Facebook id is required", $output, 1);
                            $sdata['g_id'] = "";
                            $sdata['fb_id'] = $updated_data['fb_id'] = $fb_id;
                        } else {
                            $this->validate($g_id, "Google id is required", $output, 1);
                            $sdata['fb_id'] = "";
                            $sdata['g_id'] = $updated_data['g_id'] = $g_id;
                        }
                        if (($m != '') && ($b != '')) {

                            $zodiac_info = $this->Birthday->getUserData("mapzodiac", "(fdate <='$b' and fmonth='$m') or (tdate>='$b' and tmonth ='$m')")->row_array();

                            $sdata["zodiac_id"] = $zodiac_info["zodiac_id"];
                            $sdata["bmonth"] = $m;
                            $sdata["bdate"] = $b;
                            $sdata["byear"] = $y;
                            $sdata["birth_date"] = "$m/$b/$y";
                        } else {
                            $sdata["zodiac_id"] = 0;
                            $sdata["bmonth"] = '';
                            $sdata["bdate"] = '';
                            $sdata["byear"] = "";
                            $sdata["birth_date"] = "";
                        }

                        $where = "email='$email'";
                        $query = $this->Birthday->getSelectData("email", "users", $where)->result_array();
                        $sdata["u_last_login"] = $updated_data["u_last_login"] = date("Y-m-d H:i:s");

                        if (count($query) > 0) {
                            $updated = $this->Birthday->update("users", $updated_data, $where);
                            if ($updated == 1) {
                                $user_info = $this->Birthday->getSelectData($select, "users", $where)->row_array();
                            }
                        } else {
                            $insert_id = $this->Birthday->addData("users", $sdata);
                            $new_data["nimage_name"] = "";
                            $this->Birthday->addData("news_letter", $new_data);
                            $where = "user_id='$insert_id'";
                            $user_info = $this->Birthday->getSelectData($select, "users", $where)->row_array();
                        }

                        $user_info["mail_status"] = $newsletter_data["mail_status"];
                        $output["user_info"] = $user_info;
                    }
                    break;

                case "4": { //login after deep linking
                        $user_id = check_post($this->input->post("user_id"));
                        $this->validate($user_id, "user_id is required", $output, 1);
                        $where = "user_id='$user_id'";
                        $user_info = $this->Birthday->getSelectData($select, "users", $where)->row_array();
                        if (count($user_info) == 0) {
                            $output["success"] = FALSE;
                            $output["message"] = "user_id does not exist";
                            echo json_encode($output);
                            exit;
                        }
                        $email = $user_info["email"];
                        $newsletter_data = $this->Birthday->getSelectData("mail_status", "news_letter", "email_id='$email'")->row_array();
                        $user_info["mail_status"] = $newsletter_data["mail_status"];
                        $output["user_info"] = $user_info;
                    }
                    break;
            }

            $ndr_data["user_id"] = $data["user_id"] = $user_id = $user_info["user_id"];
            $plans = $this->Birthday->getSelectData("amount,duration", "membership_plans", "status=1")->result_array();
            $membership_status = $user_info["membership_status"];
            if ($membership_status == 1) {
                $selected = "greeting_membership.membership_id,greeting_membership.plan_purchase_type,greeting_membership.expiry_date";
                $where = "greeting_membership.user_id=$user_id and greeting_membership.transaction_status='success'";
                $planinfo = $this->Birthday->getSelectData($selected, "greeting_membership", $where)->result_array();
                $output["plan_info"] = $planinfo;
                $output["plans"] = $plans;
            } else {
                $output["plans"] = $plans;
                $output["plan_info"] = array();
            }


            if ($platform == 1) {
                $data["ios_device_id"] = $reg_id;
                $data["android_device_id"] = "";
            } else {
                $data["ios_device_id"] = "";
                $data["android_device_id"] = $reg_id;
            }
            $ndr_data["platform"] = $data["platform"] = $platform;
            $data["registration_date"] = date("Y-m-d H:i:s");
            $this->Birthday->addData("users_device_registration", $data);
            $ndr_data["device_registration"] = $reg_id;

            $check_ndr = $this->Birthday->getSelectData("ndr_id", "notify_device_registartion", "user_id = $user_id and device_registration = '$reg_id' and platform = $platform")->result_array();
            if (count($check_ndr) == 0 && $reg_id != "0" && $reg_id != "") {
                $this->Birthday->addData("notify_device_registartion", $ndr_data);
            }
            $output["success"] = TRUE;
            $output["message"] = "Login Successfully!";
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found.";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

//At the time of forgot password.Taking input as email and sending reset password link if email-id is exist.
    public function forgotPassword() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/forgotPassword";
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $email = trim(check_post($this->input->post("email")));
            $this->validate($email, "email-id is required", $output, 1);
            $this->validate($email, "Email id is invalid", $output, 3);
            $check_email = $this->Birthday->getSelectData("password,email,first_name,user_id,login_with", "users", "email='$email' and mobile_verify = 1 and is_guest != 1")->result_array();
            if (count($check_email) == 0) {
                $output["success"] = FALSE;
                $output["message"] = "No Birthday owl registered account found for this email";
            } else {
                $login_with = $check_email[0]["login_with"];
                $password = $check_email[0]["password"];
                if ($password != "") {
                    $pass = $this->Birthday->decryptPassword($password);
                    $data["Password"] = $pass;
                    $data["username"] = $check_email[0]["email"];
                    $data["realName"] = $name = $check_email[0]["first_name"];
                    $user_id = $check_email[0]['user_id'];
                    $euserid = $this->Birthday->encryptPassword($user_id);
                    $time_created = $this->Birthday->encryptPassword(date("Y-m-d H:i:s"));
                    $data["link"] = BASEURL . "Home_web/change_password/$euserid/$time_created";
                    try {
                        $view = $this->load->view("email_template/ResetPassword", $data, true);
                        $result = send_EmailResponse($view, $email, $name, "resetpassword@birthdayowl.com", "", "BirthdayOwl : Reset Password");

                        if ($result["code"] == "success") {
                            $reset["reset_activate"] = 1;
                            $reset["reset_time"] = date("Y-m-d H:i:s");
                            $reset["reset_link"] = $data["link"];
                            $this->Birthday->update("users", $reset, "user_id = $user_id");
                            $output["success"] = true;
                            $output["message"] = "Check your Email address to Reset Password"; //If user name is not in the database
                        } else {
                            $output["success"] = false;
                            $output["message"] = "Please try again after some time"; //If user name is not in the database
                        }
                    } catch (Exception $e) {
                        $output["success"] = false;
                        $output["message"] = "Please try again after some time";
                    }
                } else {
                    switch ($login_with) {
                        case "2": {
                                $output["success"] = true;
                                $output["message"] = "You have registered using Facebook account.Please log in using Facebook account"; //If user name is not in the database
                            }
                            break;
                        case "3": {
                                $output["success"] = true;
                                $output["message"] = "You have registered using Google account.Please log in using Google account"; //If user name is not in the database
                            }
                            break;
                    }
                }
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No Input Found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    //changed password
    public function changePassword() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/changePassword";

        $postcount = count($_POST);
        if ($postcount > 0) {
            $user_id = check_post($this->input->post("user_id"));
            $this->validate($user_id, "user_id is required", $output, 1);

            $check_user_id = $this->Birthday->getSelectData("user_id", "users", "user_id='$user_id'")->num_rows();
            if ($check_user_id == 0) {
                $output["success"] = FALSE;
                $output["message"] = "user_id does not exist";
                echo json_encode($output);
                exit;
            }

            $im_details["password"] = $this->Birthday->encryptPassword(check_post($this->input->post("new_password")));
            $this->Birthday->update("users", $im_details, "user_id = $user_id");
            $output["success"] = TRUE;
            $output["message"] = "Password Changed Successfully";
            $where = "user_id='$user_id'";
            $select = "users.user_id,users.first_name,users.password,users.email,users.mobile_no,users.birth_date,users.gender,users.bdate,users.bmonth,users.byear,users.fb_id,users.g_id,users.login_with,users.user_profile_pic,users.sent_greeting_counter,users.membership_status,users.is_guest";
            //  $user_data = $this->Birthday->getJoinedData2($select, "users", "country", "users.country_id=country.country_id", $where)->row_array();
            $user_data = $this->Birthday->getSelectData($select, "users", $where)->row_array();
            $email = $user_data["email"];
            $newsletter_data = $this->Birthday->getSelectData("mail_status", "news_letter", "email_id='$email'")->row_array();
            $user_data["mail_status"] = $newsletter_data["mail_status"];
            $output["user_info"] = $user_data;
            $plans = $this->Birthday->getSelectData("amount,duration", "membership_plans", "status=1")->result_array();

            if ($user_data["membership_status"] == 1) {
                $selected = "greeting_membership.membership_id,greeting_membership.plan_purchase_type,greeting_membership.expiry_date";
                $where = "greeting_membership.user_id=$user_id and greeting_membership.transaction_status='success'";
                $planinfo = $this->Birthday->getSelectData($selected, "greeting_membership", $where)->result_array();
                $output["plan_info"] = $planinfo;
                $output["plans"] = $plans;
            } else {
                $output["plans"] = $plans;
                $output["plan_info"] = array();
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function updateProfile() {
        //user_id,first_name,bmonth,bdate,byear,mobile_no,email,gender
        $output = array();
        $output["url"] = BASEURL . "Home_api/updateProfile";
        $postcount = count($_POST);
        if ($postcount > 0) {
            $user_id = $updated_data["user_id"] = check_post($this->input->post("user_id"));
            $updated_data["first_name"] = check_post($this->input->post("first_name"));
            // $password = check_post($this->input->post("password"));
            $updated_data["country_id"] = check_post($this->input->post("country_id"));
            $updated_data["bmonth"] = check_post($this->input->post("bmonth"));
            $updated_data["bdate"] = check_post($this->input->post("bdate"));
            $updated_data["byear"] = check_post($this->input->post("byear"));
            $updated_data["mobile_no"] = $mobile_no = check_post($this->input->post("mobile_no"));
            $email = $updated_data["email"] = trim(check_post($this->input->post("email")));
            $this->validate($email, "Email id is invalid", $output, 3);
            $updated_data["gender"] = check_post($this->input->post("gender"));
            $updated_data["user_profile_pic"] = check_post($this->input->post("user_profile_pic"));
            $updated_data["updated_date"] = date("Y-m-d H:i:s");
            if ($email != "") {
                $where = "email='$email' and user_id <> $user_id  and is_guest=0";
                $check_email = $this->Birthday->getSelectData("email", "users", $where)->num_rows();
                if ($check_email > 0) {
                    $output["success"] = FALSE;
                    $output["message"] = "Email-id already exists.";
                    echo json_encode($output);
                    exit;
                }
            }
            if ($mobile_no != "") {
                $where = "mobile_no='$mobile_no'  and user_id!='$user_id' and is_guest=0";
                $check_mobile = $this->Birthday->getSelectData("mobile_no", "users", $where)->num_rows();
                if ($check_mobile > 0) {
                    $output["success"] = FALSE;
                    $output["message"] = "Mobile Number already exists.";
                    echo json_encode($output);
                    exit;
                }
            }
            $updated_data = $this->Birthday->update("users", $updated_data, "user_id='$user_id'");
            if ($updated_data == 1) {
                $select = "users.country_id,users.user_id,users.first_name,users.password,users.email,users.mobile_no,users.birth_date,users.gender,users.bdate,users.bmonth,users.byear,users.fb_id,users.g_id,users.login_with,users.user_profile_pic,users.created_date,users.updated_date,users.reset_time,users.reset_activate,users.sent_greeting_counter,users.membership_status";
                $updated_result = $this->Birthday->getSelectData($select, "users", "user_id = $user_id")->result_array();
                $output["updated_data"] = $updated_result;
                $output["success"] = true;
                $output["message"] = "Profile is updated successfully!";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No Input Found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function profile_pic_upload() {
        $output["url"] = BASEURL . "Home_api/profile_pic_upload";
        $filename = "";
        $upload = FALSE;
        if (!empty($_FILES)) {
            $tempFile = trim($_FILES['userfile']['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
            $filename = str_replace(" ", "_", $filename);
            $targetPath = "public/profile_pic/";
            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = FALSE;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES["userfile"]["error"];
                } else {
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $upload = TRUE;
                }
            } else {
                $output["filename"] = $filename;
                $output["success"] = true;
                $upload = TRUE;
            }
        } else {
            $output["success"] = FALSE;
            $output["error_no"] = 1;
            $output["message"] = "No Input Found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    //Add and update birthday reminder.If reminder-id is null we are adding reminder else updating reminder
    public function AddUpdateBirthdayReminder() {
        $output["url"] = BASEURL . "Home_api/AddUpdateBirthdayReminder";
        if (count($_POST) > 0) {
            $reminder_id = check_post($this->input->post("reminder_id"));
            $bday_details["user_id"] = $user_id = check_post($this->input->post("user_id"));
            $this->validate($bday_details["user_id"], "user_id is required", $output, 1);
            $check_user_id = $this->Birthday->getSelectData("user_id", "users", "user_id='$user_id'")->num_rows();
            if ($check_user_id == 0) {
                $output["success"] = FALSE;
                $output["message"] = "user_id does not exist";
                echo json_encode($output);
                exit;
            }
            $bday_details["first_name"] = check_post($this->input->post("first_name"));
            $bday_details["last_name"] = check_post($this->input->post("last_name"));
            $bday_details["bdate"] = str_replace(" ", "", check_post($this->input->post("bdate")));
            $bday_details["bmonth"] = str_replace(" ", "", check_post($this->input->post("bmonth")));
            $bday_details["byear"] = str_replace(" ", "", check_post($this->input->post("byear")));
            $bday_details["relation_id"] = check_post($this->input->post("relation_id"));
            $bday_details["mobile_no"] = $mobileno = check_post($this->input->post("mobile_no"));
            $bday_details["email"] = $email = check_post($this->input->post("email"));
            // $this->validate($bday_details["email"], "Email id is invalid", $output, 3);
            $bday_details["gender"] = check_post($this->input->post("gender"));
            $bday_details["zodiac_id"] = check_post($this->input->post("zodiac_id"));
            $bday_details["priority"] = check_post($this->input->post("priority"));
            $bday_details["rem_image_name"] = check_post($this->input->post("rem_image_name"));
            $bday_details["plus_id"] = "";
            $month = $this->Birthday->checkLength2($bday_details["bmonth"]);
            $date = $this->Birthday->checkLength2($bday_details["bdate"]);
            $bday_details["birth_date"] = "0000-" . $month . "-" . $date;
            $bday_details["reminder_created_date"] = date("Y-m-d H:i:s");
            if ($reminder_id == '') {
                $isexist = $this->Birthday->getSelectData("reminder_id", 'bday_reminder', "email='$email' and mobile_no='$mobileno' and user_id=$user_id")->num_rows();
                if ($isexist > 0) {
                    $output["success"] = false;
                    $output["message"] = "Reminder already exists.";
                    echo json_encode($output);
                    exit;
                } else {
                    $this->Birthday->addData("bday_reminder", $bday_details);
                    $output["success"] = TRUE;
                    $output["message"] = "Reminder Added Successfully";
                }
            }

            if ($reminder_id != '') {
                $isexist = $this->Birthday->getSelectData("reminder_id", 'bday_reminder', "email='$email' and mobile_no='$mobileno' and user_id=$user_id and reminder_id!=$reminder_id")->num_rows();
                if ($isexist > 0) {
                    $output["success"] = false;
                    $output["message"] = "Reminder already exists.";
                    echo json_encode($output);
                    exit;
                } else {
                    $this->Birthday->update("bday_reminder", $bday_details, "reminder_id='$reminder_id'");
                    $output["success"] = TRUE;
                    $output["message"] = "Reminder Updated Successfully";
                }
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "There is no data";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function DeleteReminder() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/DeleteReminder";
        $postcount = count($_POST);
        if ($postcount > 0) {
            $reminder_id = check_post($this->input->post("reminder_id"));
            $checkReminder_idexist = $this->Birthday->getSelectData("reminder_id", "bday_reminder", "reminder_id=$reminder_id")->num_rows();
            if ($checkReminder_idexist == 0) {
                $output["success"] = FALSE;
                $output["message"] = "Reminder id does not exist";
                echo json_encode($output);
                exit;
            }
            $where = "reminder_id='$reminder_id'";
            $this->Birthday->delete_reminder("bday_reminder", $where);
            $output["success"] = true;
            $output["message"] = "Reminder deleted successfully";
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    //It gives information of sent greeting on the basis of fid
    public function getGreetingInfo() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/getGreetingInfo";
        $postcount = count($_POST);
        if ($postcount > 0) {
            $fid = check_post($this->input->post("fid"));
            $check_fid = $this->Birthday->getSelectData("fid", "free_greetings", "fid=$fid")->num_rows();
            if ($check_fid == 0) {
                $output["success"] = FALSE;
                $output["message"] = "Greeting  id does not exist";
                echo json_encode($output);
                exit;
            }
            $greetingdata = $this->Birthday->getJoinedData2("free_greetings.greeting_img,free_greetings.uploded_img,free_greetings.delivery_date,free_greetings.fphone,free_greetings.created_date,free_greetings.type,free_greetings.fid,free_greetings.status,free_greetings.mail_open_status,free_greetings.message,free_greetings.type,free_greetings.uploded_img,users.first_name,users.email,free_greetings.created_date", "free_greetings", "users", "free_greetings.user_id=users.user_id", "free_greetings.fid=$fid")->row_array();
            if (count($greetingdata) > 0) {
                $output["greeting_data"] = $greetingdata;
                $output["success"] = TRUE;
            } else {
                $output["greeting_data"] = array();
                $output["success"] = FALSE;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    //It gives information of sent voucher on the basis of order_pro_id

    public function getVoucherInfo() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/getVoucherInfo";
        $postcount = count($_POST);
        if ($postcount > 0) {
            $order_pro_id = check_post($this->input->post("order_pro_id"));
            $order_data = $this->Birthday->getSelectData("payment_id,greeting_id", "order_products", "order_pro_id=$order_pro_id")->row_array();
            if (count($order_data) == 0) {
                $output["success"] = FALSE;
                $output["message"] = "order_pro_id does not exist";
                echo json_encode($output);
                exit;
            }

            $payment_id = $order_data["payment_id"];
            $greeting_id = $order_data["greeting_id"];
            $voucher_data = $this->Birthday->get_send_giftdata($payment_id, $greeting_id)->row_array();
            if (count($voucher_data) > 0) {
                $output["gift_data"] = $voucher_data;
                $output["success"] = TRUE;
            } else {
                $output["gift_data"] = array();
                $output["success"] = FALSE;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function viewBdayReminder() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/viewBdayReminder";
        $postcount = count($_POST);
        if ($postcount > 0) {
            $user_id = check_post($this->input->post("user_id"));
            $month = check_post($this->input->post("bmonth"));
            $date = check_post($this->input->post("bdate"));
            $checkUserNameValidity = $this->Birthday->getSelectData("user_id", "users", "user_id=$user_id")->num_rows();
            if ($checkUserNameValidity == 1) {
                $bday_data = $this->Birthday->getBdayInfo($user_id, $month, $date)->result_array();
                if ((count($bday_data) > 0)) {
                    $output["bday_info"] = $bday_data;
                    $output["success"] = true;
                    $output["message"] = "";
                    echo json_encode($output);
                    exit;
                } else {
                    $output["success"] = FALSE;
                    $output["message"] = "There are no Birthdays in this month.";
                }
            } else {
                $output["success"] = FALSE;
                $output["message"] = "Invalid user";
                echo json_encode($output);
                exit;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function reminder_pic_upload() {
        $output["url"] = BASEURL . "Home_api/reminder_pic_upload";
        $filename = "";
        $upload = FALSE;
        if (!empty($_FILES)) {
            $tempFile = trim($_FILES['userfile']['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
            $filename = str_replace(" ", "_", $filename);
            $targetPath = 'public/reminder_pic/';

            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = FALSE;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES["userfile"]["error"];
                } else {
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $upload = TRUE;
                }
            } else {
                $output["filename"] = $filename;
                $output["success"] = true;
                $upload = TRUE;
            }
        } else {
            $output["success"] = FALSE;
            $output["error_no"] = 1;
            $output["message"] = "No Input Found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

//based on index and limit value sending vouchers
    public function getVouchersByLimit() {
        $output = array();

        $output["url"] = BASEURL . "Home_api/getVouchersByLimit";
        $index = check_post($this->input->post("index"));
        $this->validate($index, "Index value is required", $output, 1);
        if ($index < 1) {
            $output["success"] = FALSE;
            $output["message"] = "Index value should be greater than 0";
            echo json_encode($output);
            exit;
        }
        $limit = check_post($this->input->post("limit"));
        $this->validate($limit, "Limit value is required", $output, 1);
        if ($limit < 1) {
            $output["success"] = FALSE;
            $output["message"] = "Limit value should be greater than 0";
            echo json_encode($output);
            exit;
        }
        $output["success"] = TRUE;
        if (SERVER_TYPE == 3)
            $countp = filesize("files/products.txt");
        else
            $countp = filesize("files/products1.txt");

        if ($countp > 0) {
            $vouchers = array_values(getAllVouchers());
            $vcount = count($vouchers);
            if ($vcount > 0) {
                $sliced_array = array();
                $i = $index - 1;
                foreach ($vouchers as $key => $value) {
                    if ($i < $limit && $i < $vcount) {
                        $sliced_array[$key] = $vouchers[$i];
                    }
                    $i++;
                }
                $output["vouchers"] = $sliced_array;
                $output["vcount"] = $vcount;
            } else {
                $output["vouchers"] = array();
                $output["vcount"] = $vcount;
            }
        } else {
            $output["vouchers"] = array();
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

//There are two types of greetings:1.free greetings 2.voucher greetings.On the basis of greeting_type sending greetings
    public function getGreetingcard() {
        $output = array();
        $greeting_type = check_post($this->input->post("greeting_type"));
        $output["url"] = BASEURL . "Home_api/getGreetingcard";
        $output["success"] = TRUE;
        if ($greeting_type == "") {
            $greeting_type = 1;
        }
        $vouchers = $this->Birthday->getSelectData("card_id,card_name,front_page_image,greeting_type,greet_cat_id", "greeting_card", "greeting_type=$greeting_type order by position=0,position ASC")->result_array();
        if (count($vouchers) > 0) {
            $output["greeting_card"] = $vouchers;
            $output["greeting_card_count"] = count($vouchers);
        } else {
            $output["greeting_card"] = array();
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    //calling this api at the time of send greeting.Here if fid is not null means user sending greeting else updating greeting.
    public function send_greeting() {
        $output = array();
        $postcount = count($_POST);
        if ($postcount > 11) {
//femail,fname,message,greeting_img,user_id,greeting_schedule,uploded_img,title,gettype,delivery_date
            $output["url"] = BASEURL . "Home_api/send_greeting";
            $femail = trim(check_post($this->input->post("femail")));
            $fphone = check_post($this->input->post("fphone"));
            $is_edit = check_post($this->input->post("is_edit"));
            $fid = check_post($this->input->post("fid"));
            if ($is_edit == "") {
                $is_edit = 0;
            }
            $is_email_sent = 0;
            $is_sms_sent = 0;
            $fname = check_post($this->input->post("fname"));
            $adddata["delivery_date"] = check_post($this->input->post("delivery_date")); //2017-06-25
            $adddata["greeting_schedule"] = $greeting_schedule = check_post($this->input->post("greeting_schedule"));
            $this->validate($fname, "Friend's name  is required", $output, 1);
            $adddata["greeting_img"] = $greeting_img = check_post($this->input->post("greeting_img"));
            $this->validate($greeting_img, "greeting image name is required", $output, 1);
            $adddata["user_id"] = $user_id = check_post($this->input->post("user_id"));
            $this->validate($user_id, "user_id is required", $output, 1);
            $user_info = $this->Birthday->getSelectData("email,sent_greeting_counter,first_name,mobile_no,membership_status,user_id", "users", "user_id='$user_id'")->row_array();
            if (count($user_info) == 0) {
                $output["success"] = FALSE;
                $output["message"] = "user_id does not exist";
                echo json_encode($output);
                exit;
            }

            $data["uemail"] = $uemail = $user_info["email"];
            $data["uname"] = $uname = $user_info["first_name"];
            $userphone = $user_info["mobile_no"];
//            $adddata["message"] = $data["message"] = check_post($this->input->post("message"));
            $message = check_post($this->input->post("message"));
            $adddata["message"] = $data["message"] = preg_replace('/[^\da-z ]/i', '', $message);
            $keyid = 'freegreetinglink';
            $greeting_cat_id = 0;
//            $eupdated_data["title"] = $adddata["title"] = $title = check_post($this->input->post("title"));
            $title = check_post($this->input->post("title"));
            $eupdated_data["title"] = $adddata["title"] = preg_replace('/[^\da-z ]/i', '', $title);
            if (check_post($this->input->post("gettype")) == "") {
                $adddata["type"] = 0;
            } else {
                $adddata["type"] = check_post($this->input->post("gettype"));
            }
            $eupdated_data["fname"] = $data["fname"] = $adddata["fname"] = $fr_name = $fname;
            $data["femail"] = $adddata["femail"] = $fr_email = $femail;
            $adddata["uploded_img"] = check_post($this->input->post("uploded_img"));
            $adddata["fphone"] = $fphone;
            $adddata["greeting_cat_id"] = 0;
            $adddata["created_date"] = date("Y-m-d H:i:s");
            $adddata["platform"] = check_post($this->input->post("platform"));
            if ($is_edit == 0) {
                if ($fid != "") { //IF user is editing greeting and changed email-id then this condition will become true
                    $newgreetinginfo = $this->Birthday->getSelectData("delivery_date,greeting_schedule,uploded_img,message,title,type", "free_greetings", "fid='$fid'")->row_array();
//                    $adddata["delivery_date"] = $newgreetinginfo["delivery_date"]; //2017-06-25
//                    $adddata["greeting_schedule"] = $greeting_schedule = $newgreetinginfo["greeting_schedule"];
                    $adddata["uploded_img"] = $newgreetinginfo["uploded_img"];
                    $adddata["message"] = $data["message"] = $newgreetinginfo["message"];
                    $adddata["type"] = $newgreetinginfo["type"];
                }
                $data["fid"] = $free_id = $this->Birthday->addData("free_greetings", $adddata);
            } else {
                $data["fid"] = $free_id = $fid;
                $this->Birthday->update("free_greetings", $eupdated_data, "fid='$free_id'");
            }
            $gcard_id = $this->Birthday->encryptPassword($greeting_cat_id);
            $freeid = $this->Birthday->encryptPassword($free_id);
            $key = $this->Birthday->encryptPassword($keyid);
            $from = $this->Birthday->encryptPassword(0);
            $updated_data["greeting_info_link"] = $data["linkclick"] = $link = BASEURL . "free_greetings_email/$gcard_id/$freeid/$key/$from";
            if ($title != '') {
                $subject = "$title";
            } else {
                $subject = "GREETING CARD FROM BIRTHDAYOWL";
            }
            $data["user_id"] = $user_id;
            $sent_counter = $user_info["sent_greeting_counter"];
            $mupdated_data["sent_greeting_counter"] = $sent_counter + 1;
            $this->Birthday->update("users", $mupdated_data, "user_id='$user_id'");
//            if (($user_info["sent_greeting_counter"] >= 0 && $user_info["membership_status"] == "0") || $user_info["membership_status"] == "1") {
            if ($user_info["sent_greeting_counter"] >= 0) {
                if ($adddata["greeting_schedule"] == "0") {
                    if ($femail != "") {
                        try {
                            $view = $this->load->view("email_template/free_greetings", $data, true);
                            $resp = send_EmailResponse($view, $fr_email, $fr_name, "greetings@birthdayowl.com", $uname, $subject);
                            if ($resp["code"] == "success") {
                                $is_email_sent = 1;
                            } else {
                                $is_email_sent = 0;
                            }
                        } catch (Exception $e) {
                            $is_email_sent = 0;
                        }
                    }

                    if (SERVER_TYPE != "3") {
                        if ($fphone != "") {
                            $linkData = $this->google_url_api->shorten("$link");
                            $click_link = $linkData->id;
                            $show_message = "Dear $fname, \n You have just received an awesome Birthday Greeting card from $uname. \n To view your greeting, simply ($click_link).\n - Birthday Owl (www.birthdayowl.com)";
                            $result = $this->Birthday->send_sms($fphone, $show_message);
                            if ($result == 1) {
                                $is_sms_sent = 1;
                            } else {
                                $is_sms_sent = 0;
                            }
                        }
                    }

                    if ($is_email_sent == 1 || $is_sms_sent == 1) {
                        $updated_data["is_sent"] = 1;
                        $this->Birthday->update("free_greetings", $updated_data, "fid='$free_id'");
                    } else {
                        $updated_data["is_sent"] = 0;
                        $where = "fid='$free_id'";
                        $this->Birthday->update("free_greetings", $updated_data, $where);
                    }
                    $user_info = $this->Birthday->getSelectData("email,sent_greeting_counter,first_name,mobile_no,membership_status", "users", "user_id='$user_id'")->row_array();
                    if ($userphone != '') {
                        $show_message = "Dear $uname, \n You have just sent an awesome greeting card to $fr_name and we have notified $fname accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                        $this->Birthday->send_sms($userphone, $show_message);
                    }
                    $linkData = $this->google_url_api->shorten("$link");
                    $click_link = $linkData->id;
                    $received_message = "Dear $fname, \n You have just received an awesome Birthday Greeting card from $uname. \n To view your greeting, simply ($click_link).\n - Birthday Owl (www.birthdayowl.com)";
                    $output["sent_greeting_counter"] = $user_info["sent_greeting_counter"];
                    $output["membership_status"] = $user_info["membership_status"];
                    $output["email_list"] = $femail;
                    $output["name_list"] = $fname;
                    $output["schedule"] = "Today";
                    $output["indicator"] = "0";
                    $output["free_greeting_link"] = $updated_data["greeting_info_link"];
                    $output["phone_no"] = $fphone;
                    $output["sent_message"] = $received_message;
                    $output["greeting_img"] = $greeting_img;
                    $output["success"] = true;
                    $output["post_data"] = $_POST;
                    $output["message"] = "Greeting card sent successfully!.";
                } else {
                    $user_info = $this->Birthday->getSelectData("email, sent_greeting_counter, first_name, mobile_no, membership_status", "users", "user_id = '$user_id'")->row_array();
                    $output["sent_greeting_counter"] = $user_info["sent_greeting_counter"];
                    $output["membership_status"] = $user_info["membership_status"];
                    $updated_data["is_sent"] = 2;
                    $output["phone_no"] = $fphone;
                    $output["free_greeting_link"] = "";
                    $this->Birthday->update("free_greetings", $updated_data, "fid = '$free_id'");
                    $converted_date = date("dS F Y", strtotime($adddata["delivery_date"]));
                    $output["success"] = true;
                    $output["schedule"] = "$converted_date";
                    $output["message"] = "Thank You for using BirthdayOwl. Your greeting card will be sent on $converted_date!";
                }
            } else {
                $output["sent_greeting_counter"] = $user_info["sent_greeting_counter"];
                $output["membership_status"] = $user_info["membership_status"];
                $updated_data["is_sent"] = 0;
                $this->Birthday->update("free_greetings", $updated_data, "fid = '$free_id'");
                $output["phone_no"] = $fphone;
                $output["free_greeting_link"] = "";
                $output["success"] = true;
                $output["message"] = "Greeting card can not send.Try again";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    //calling this api at the time of resend greeting
    public function resendGreeting() {
        //user_id,fid
        $postcount = count($_POST);
        if ($postcount > 0) {
            $output = array();
            $output["url"] = BASEURL . "Home_api/resendGreeting";
            $mdata["fid"] = $fid = check_post($this->input->post("fid"));
            $mdata["user_id"] = $user_id = check_post($this->input->post("user_id"));
            $is_email_sent = 0;
            $is_sms_sent = 0;
            $this->validate($user_id, "user_id is required", $output, 1);
            $check_user_id = $this->Birthday->getSelectData("user_id", "users", "user_id='$user_id'")->num_rows();
            if ($check_user_id == 0) {
                $output["success"] = FALSE;
                $output["message"] = "user_id does not exist";
                echo json_encode($output);
                exit;
            }
            $getsentcounter = $this->Birthday->getSelectData("sent_greeting_counter,membership_status,email,first_name,mobile_no", "users", "user_id='$user_id'")->row_array();

            $getgreetingdata = $this->Birthday->getSelectData("greeting_img,message,fname,femail,fphone,uploded_img,title,type,delivery_date,greeting_cat_id", "free_greetings", "fid='$fid'")->row_array();
            $keyid = 'freegreetinglink';
            $output["membership_status"] = $getsentcounter["membership_status"];
            if ($getsentcounter["membership_status"] == 1 || ($getsentcounter["sent_greeting_counter"] < 30 && $getsentcounter["membership_status"] == 0)) {
                $data["fname"] = $rec_name = $getgreetingdata["fname"];
                $data["femail"] = $rec_email = $getgreetingdata["femail"];
                $data["fphone"] = $getgreetingdata["fphone"];
                $mdata["uemail"] = $uemail = $getsentcounter["email"];
                $mdata["uname"] = $uname = $getsentcounter["first_name"];
                $userphone = $getsentcounter["mobile_no"];
                $gcard_id = $this->Birthday->encryptPassword($getgreetingdata["greeting_cat_id"]);
                $freeid = $this->Birthday->encryptPassword($fid);
                $key = $this->Birthday->encryptPassword($keyid);
                $from = $this->Birthday->encryptPassword(0);
                $mdata["linkclick"] = $link = BASEURL . "free_greetings_email/$gcard_id/$freeid/$key/$from";
                $mupdated_data["sent_greeting_counter"] = $output["sent_greeting_counter"] = $getsentcounter["sent_greeting_counter"] + 1;
                $this->Birthday->update("users", $mupdated_data, "user_id='$user_id'");
                $title = $getgreetingdata["title"];
                if ($rec_email != "") {
                    if ($title != '') {
                        $gtitle = "$title";
                    } else {
                        $gtitle = "birthdayowl.com :e-Greeting Card";
                    }
                    $updated_data["greeting_info_link"] = $link;
                    try {
                        $view = $this->load->view("email_template/free_greetings", $mdata, true);
                        $resp = send_EmailResponse($view, $rec_email, $rec_name, "greetings@birthdayowl.com", $uname, $gtitle);
                        if ($resp["code"] == "success") {
                            $is_email_sent = 1;
                        } else {
                            $is_email_sent = 0;
                        }
                    } catch (Exception $e) {
                        $is_email_sent = 0;
                    }
                }

                if (SERVER_TYPE == "1") {
                    if ($data["fphone"] != "") {
                        $linkData = $this->google_url_api->shorten("$link");
                        $click_link = $linkData->id;
                        $show_message = "Dear $rec_name, \n You have just received an awesome  Birthday Greeting from $uname. \n To view your greeting, simply ($click_link).\n - Birthday Owl (www.birthdayowl.com)";
                        $result = $this->Birthday->send_sms($data["fphone"], $show_message);
                        $updated_data["greeting_info_link"] = $link;
                        if ($result == 1) {
                            $is_sms_sent = 1;
                        } else {
                            $is_sms_sent = 0;
                        }
                    }
                    if ($userphone != '') {
                        $show_message = "Dear $uname, \n You have just sent an awesome greeting card to $rec_name and we have notified $rec_name accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                        $this->Birthday->send_sms($userphone, $show_message);
                    }
                }
                if ($is_email_sent == 1 || $is_sms_sent == 1) {
                    $updated_data["is_resent"] = 1;
                    $where = "fid='$fid'";
                    $this->Birthday->update("free_greetings", $updated_data, $where);
                    $output["message"] = "Greeting card resent successfully!.";
                    $output["success"] = true;
                } else {
                    $updated_data["is_resent"] = 2;
                    $where = "fid='$fid'";
                    $this->Birthday->update("free_greetings", $updated_data, $where);
                    $output["success"] = false;
                    $output["message"] = "Greeting card can not send.Try again";
                }
                $output["is_purchase"] = 1;
            } else if ($getsentcounter["sent_greeting_counter"] >= 10 && $getsentcounter["membership_status"] == 0) {
                $output["sent_greeting_counter"] = $getsentcounter["sent_greeting_counter"];
                $output["message"] = "Greeting card can not send.Please purchase membership plan";
                $output["is_purchase"] = 0; //not purchased
                $output["success"] = false;
            } else if ($getsentcounter["sent_greeting_counter"] >= 10 && $getsentcounter["membership_status"] == 2) {
                $output["sent_greeting_counter"] = $getsentcounter["sent_greeting_counter"];
                $output["success"] = false;
                $output["is_purchase"] = 2; //expired
                $output["message"] = "Greeting card can not send.Your membership plan has been expired"; //expired
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    //use to upload image in free greetings and vouchers
    public function freegreeting_imgupload() {

        $output["url"] = BASEURL . "Home_api/freegreeting_imgupload";
        $platform = check_post($this->input->post("platform"));
        $filename = "";
        $upload = FALSE;

        if (!empty($_FILES)) {
            $tempFile = trim($_FILES['userfile']['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
            $filename = str_replace(" ", "_", $filename);
            $targetPath = "public/imageVideo/";
            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = FALSE;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES["userfile"]["error"];
                } else {
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $upload = TRUE;
                }
            } else {
                $file_parts = pathinfo($filename);
                $type = $file_parts["extension"];
                $file_new = $file_parts["filename"];
                $new_file_name = $targetPath . $file_new . '_comp.webm';
                if ($type == "mp4" && $platform == 2) {
                    exec("ffmpeg -i '$targetFile' -vcodec libvpx -cpu-used -5 -deadline realtime '$new_file_name'", $output, $return);
                    //  exec("ffmpeg -i '$targetFile' -tune zerolatency -x264opts bitrate=1500:vbv-maxrate=1500:vbv-bufsize=3000:nal-hrd=vbr -codec:v libx264 -profile:v high -level 3.1 -movflags +faststart -pix_fmt yuv420p   -ac 2 -ar 48000 -codec:a aac -ab 64k -strict experimental -y '$new_file_name'", $output, $return);
                    if (!$return) {
                        $output["filename"] = $file_new . '_comp.webm';
                        $output["message"] = $file_new . '_comp.webm' . " uploaded successfully";
                    } else {
                        $output["filename"] = $file_new . '_comp.webm';
                        $output["message"] = $file_new . '_comp.webm' . " is not uploaded successfully";
                    }
                } else {
                    $output["filename"] = $filename;
                    $output["message"] = "$filename uploaded successfully";
                }


                $output["success"] = true;
                $upload = TRUE;
            }
        } else {
            $output["success"] = FALSE;
            $output["error_no"] = 1;
            $output["message"] = "No Input Found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

//Get greeting history
    public function get_sent_greetings() {
        $postcount = count($_POST);
        $output = array();
        $output["url"] = BASEURL . "Home_api/get_sent_greetings";
        if ($postcount > 0) {
            $empty = array();
            $user_id = check_post($this->input->post("user_id"));
            $this->validate($user_id, "user_id is required", $output, 1);
            $check_user_id = $this->Birthday->getSelectData("user_id", "users", "user_id='$user_id'")->num_rows();
            if ($check_user_id == 0) {
                $output["success"] = FALSE;
                $output["message"] = "user_id does not exist";
                echo json_encode($output);
                exit;
            }
            $output["success"] = TRUE;
            $vouchers = $this->Birthday->getSelectData("fid,greeting_img,message,fname,femail,fphone,status,type,title,created_date,uploded_img,delivery_date", "free_greetings", "user_id = '$user_id and is_sent=1 ' order by fid Desc")->result_array();
            if (count($vouchers) > 0) {
                $output["sent_greetings"] = $vouchers;
                $output["count"] = count($vouchers);
            } else {
                $output["sent_greetings"] = $empty;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function greetingWallet() {
        $postcount = count($_POST);
        $output = array();
        $output["url"] = BASEURL . "Home_api/greetingWallet";
        if ($postcount > 0) {
            $empty = array();
            $user_id = check_post($this->input->post("user_id"));
            $this->validate($user_id, "user_id is required", $output, 1);
            $userinfo = $this->Birthday->getSelectData("email,user_id", "users", "user_id='$user_id'")->row_array();
            if (count($userinfo) == 0) {
                $output["success"] = FALSE;
                $output["message"] = "user_id does not exist";
                echo json_encode($output);
                exit;
            }
            $email = $userinfo["email"];
            $orders = $this->Birthday->getJoinedData2("f.fid,f.greeting_img,f.message,u.first_name,u.email,u.mobile_no as fphone,f.status,f.type,f.delivery_date,f.created_date,f.uploded_img,f.delivery_date", "free_greetings f", "users u", "f.user_id=u.user_id", " f.femail='$email'  order by f.fid DESC")->result_array();
            if (count($orders) > 0) {
                $output["success"] = TRUE;
                $output["order_history"] = $orders;
                $output["count"] = count($orders);
            } else {
                $output["success"] = FALSE;
                $output["order_history"] = $empty;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function getZodiacSigns() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/getZodiacSigns";
        $zodiacSigns = $this->Birthday->getAllData("mapzodiac");
        if ($zodiacSigns->num_rows() > 0) {
            $output["success"] = TRUE;
            $output["zodiac_signs"] = $zodiacSigns->result_array();
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function getCountryAndOccupation() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/getCountryAndOccupation";
        $country_info = $this->Birthday->getAllData("country");
        $occupation = $this->Birthday->getAllData("occupation");
        if (($country_info->num_rows() > 0) || ($occupation->num_rows() > 0)) {
            $output["success"] = TRUE;
            $output["country_info"] = $country_info->result_array();
            $output["occupation"] = $occupation->result_array();
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function get_random_greeting() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/get_random_greeting";
        $card_data = $this->Birthday->getUserData("greeting_card", "greeting_type='1'")->result_array();
        $gnumbers = array_column($card_data, 'card_id');
        $gid = $gnumbers[array_rand($gnumbers)];
        $gcards = $this->Birthday->getUserData("greeting_card", "card_id='$gid'")->row_array();
        if (count($gcards) > 0) {
            $output["success"] = TRUE;
            $output["gcard_name"] = $gcards["front_page_image"];
            $output["gcard_url"] = BASEURL_CROPPED_GREET . $gcards["front_page_image"];
        } else {
            $output["success"] = FALSE;
            $output["gcards"] = "";
            $output["gcard_url"] = "";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function getUpcomingBirthdays() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/getUpcomingBirthdays";
        $postcount = count($_POST);
        if ($postcount > 0) {
            $user_id = check_post($this->input->post("user_id"));
            $this->validate($user_id, "user_id is required", $output, 1);
            $month = check_post($this->input->post("month"));
            $is_all = check_post($this->input->post("is_all"));
            if ($month == "") {
                $month = date("m");
            }
            if ($is_all == "" || $is_all == 0) {
                $getUpcomingBdays = $this->Birthday->getJoinedDataThreetable("bday_reminder.reminder_id,bday_reminder.user_id,bday_reminder.rem_image_name,bday_reminder.plus_id,bday_reminder.first_name,bday_reminder.last_name,bday_reminder.bdate,bday_reminder.bmonth,bday_reminder.byear,bday_reminder.zodiac_id,bday_reminder.mobile_no,bday_reminder.email,relationship.relation_id,bday_reminder.gender,bday_reminder.priority,bday_reminder.birth_date,mapzodiac.zodiac_sign,mapzodiac.zimage,bday_reminder.country_id", "bday_reminder", "mapzodiac", "relationship", "bday_reminder.zodiac_id =mapzodiac.zodiac_id", "relationship.relation_id=bday_reminder.relation_id", "bday_reminder.bmonth=$month and bday_reminder.user_id=$user_id ORDER BY STR_TO_DATE(bday_reminder.birth_date,'%d/%m/%Y' ) ,bdate ASC", "left")->result_array();
            } else {
                $getUpcomingBdays = $this->Birthday->getJoinedDataThreetable("bday_reminder.reminder_id,bday_reminder.user_id,bday_reminder.rem_image_name,bday_reminder.plus_id,bday_reminder.first_name,bday_reminder.last_name,bday_reminder.bdate,bday_reminder.bmonth,bday_reminder.byear,bday_reminder.zodiac_id,bday_reminder.mobile_no,bday_reminder.email,relationship.relation_id,bday_reminder.gender,bday_reminder.priority,bday_reminder.birth_date,mapzodiac.zodiac_sign,mapzodiac.zimage,bday_reminder.country_id", "bday_reminder", "mapzodiac", "relationship", "bday_reminder.zodiac_id =mapzodiac.zodiac_id", "relationship.relation_id=bday_reminder.relation_id", " bday_reminder.user_id=$user_id ORDER BY STR_TO_DATE(bday_reminder.birth_date,'%d/%m/%Y' ) ,bdate ASC", "left")->result_array();
            }
            if (count($getUpcomingBdays) > 0) {
                $output["success"] = TRUE;
                $output["bday_info"] = $getUpcomingBdays;
            } else {
                $output["success"] = FALSE;
                $output["bday_info"] = array();
                $output["message"] = "No upcoming Birthdays.";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function getZodiacSignsDetails() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/getZodiacSignsDetails";
        $postcount = count($_POST);
        if ($postcount > 0) {
            $im_select["sign_day"] = check_post($this->input->post("sign_day"));
            $im_select["sign_month"] = check_post($this->input->post("sign_month"));
            $zodiacSigns = $this->Birthday->get_zodiac_info($im_select["sign_day"], $im_select["sign_month"]);
            if ($zodiacSigns->num_rows() > 0) {
                $output["success"] = TRUE;
                $output["zodic_sign_details"] = $zodiacSigns->result_array();
            } else {
                $output["success"] = FALSE;
                $output["message"] = "No data found";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found.";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function getBroadcastDetails() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/getBroadcastDetails";
        $broadcasts = $this->Birthday->getUserData("marque", "status=1");
        if ($broadcasts->num_rows() > 0) {
            $output["success"] = TRUE;
            $output["broadcasts"] = $broadcasts->result_array();
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function getAllRelationship() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/getAllRelationship";
        $output["success"] = TRUE;
        $output["relationship"] = $this->Birthday->getAllData("relationship")->result_array();
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    //If egift order got failed and user clicked "Try again" then this api get called
    public function updateEgiftOrders() {
        //order_pro_id,delievery_time,delivery_date,delievery_schedule,user_id,status,payment_mode,transaction_id,payuId
        $postcount = count($_POST);
        if ($postcount > 0) {
            $output = array();
            $output["url"] = BASEURL . "Home_api/updateEgiftOrders";
            $order_pro_id = check_post($this->input->post("order_pro_id"));
            $delivery_time = check_post($this->input->post("delievery_time"));
            $delivery_date = check_post($this->input->post("delivery_date"));
            $delievery_schedule = check_post($this->input->post("delievery_schedule"));
            $user_id = check_post($this->input->post("user_id"));
            $this->validate($user_id, "user id is required", $output, 1);
            $status = check_post($this->input->post("status"));
            $order_data = $this->Birthday->getSelectData("payment_id,selected_amount,timezone,same_quantity_id,order_pro_id", "order_products", "same_quantity_id='$order_pro_id'")->result_array();
            $count_update = count($order_data);
            $timezone = $order_data[0]["timezone"];
            $returndata = DateConversion($delievery_schedule, $delivery_date, $delivery_time, $timezone);
            $payment_id = $order_data[0]["payment_id"];
            $pay_data = $this->Birthday->getSelectData("total_amount", "payment", "payment_id='$payment_id'")->row_array();
            $amount = $pay_data["total_amount"];
            $user_info = $this->Birthday->getSelectData("email,first_name,mobile_no", "users", "user_id='$user_id'")->row_array();
            $uemail = $data["uemail"] = $user_info["email"];
            $data["sender_name"] = $uname = $data["uname"] = $data["firstname"] = $user_info["first_name"];
            $data["phone"] = $user_info["mobile_no"];
            if ($status == "1") {
                $payment["transaction_status"] = "success";
            } else {
                $payment["transaction_status"] = "Failure";
            }

            $payment["payment_mode"] = check_post($this->input->post("payment_mode"));
            $payment["transaction_id"] = check_post($this->input->post("transaction_id"));
            $payment["payuId"] = check_post($this->input->post("payuId"));
            $this->Birthday->update("payment", $payment, "payment_id=$payment_id");
            if ($status == "1") {
                for ($i = 0; $i < $count_update; $i++) {
                    $orderpro_data[$i]["delivery_date_time"] = $delivery_date_time = $returndata["delivery_date_time"];
                    $orderpro_data[$i]["order_pro_id"] = $order_data[$i]["order_pro_id"];
                    if ($delievery_schedule == '0' || $delievery_schedule == '1') {
                        $orderpro_data[$i]["delivery_status"] = 1; //payment done and sending egift
                        $orderpro_data[$i]["order_message"] = "Sending egift...";
                    } else {
                        $orderpro_data[$i]["delivery_status"] = 2;
                        $orderpro_data[$i]["order_message"] = "eGift will be delievered on $delivery_date_time";
                    }
                }
                $this->Birthday->updateBatch("order_products", $orderpro_data, "order_pro_id");
                $payment_data = $this->Birthday->getorderPaymentData($order_pro_id)->row_array();
                $data["order_id"] = $payment_data["order_id"];
                $data["bill_to_name"] = $payment_data["bill_to_fname"];
                $data["payment_mode"] = $payment_data["payment_mode"];
                $data["purchased_date"] = $payment_data["purchased_date"];
                $data["voucher_img"] = $payment_data["voucher_img"];
                $data["greeting_img"] = $payment_data["front_page_image"];
                $data["voucher_name"] = $payment_data["voucher_pro_name"];
                $data["fname"] = $payment_data["fname"];
                $data["samount"] = $data["selected_amount"] = $payment_data["selected_amount"];
                $data["femail"] = $payment_data["femail"];
                $data["greetmessage"] = $payment_data["greeting_message"];
                $data["type"] = $payment_data["type"];
                $data["media"] = $payment_data["uploded_img_video"];
                //payment confirmation email
                try {
                    $view = $this->load->view("email_template/Payment_received", $data, true);
                    $response = send_EmailResponse($view, $uemail, $uname, "orders@birthdayowl.com", "birthdayowl.com", "Payment Received (Rs.$amount) from BirthdayOwl");
                    if (SERVER_TYPE == "1") {
                        $admin_email = "abdul@birthdayowl.com";
                    } else {
                        $admin_email = "sbhuvad@techathalon.com";
                    }
                    $viewn = $this->load->view("email_template/Order_placed", $data, true);
                    send_Email($viewn, $admin_email, "abdul", "orders@birthdayowl.com", "birthdayowl.com", "Order has been placed  by $uname($uemail)");
                    $where = "order_pro_id=$order_pro_id";
                    if ($response["code"] == "success") {
                        $up["payment_mail_status"] = "1";
                        $this->Birthday->update("order_products", $up, $where);
                    } else {
                        $up["payment_mail_status"] = "0";
                        $this->Birthday->update("order_products", $up, $where);
                    }
                } catch (Exception $e) {
                    $where = "order_pro_id=$order_pro_id";
                    $up["payment_mail_status"] = "0";
                    $this->Birthday->update("order_products", $up, $where);
                }
                $output["success"] = TRUE;
                $output["message"] = "egift sent Successfully";
            } else {
                for ($i = 0; $i < $count_update; $i++) {
                    $orderpro_data[$i]["delivery_date_time"] = $delivery_date_time = $returndata["delivery_date_time"];
                    $orderpro_data[$i]["order_pro_id"] = $order_data[$i]["order_pro_id"];
                    $orderpro_data[$i]["payment_id"] = $payment_id;
                    $orderpro_data[$i]["delivery_status"] = 3; //payment done and sending egift
                    $orderpro_data[$i]["order_message"] = "Payment failed";
                }

                $this->Birthday->updateBatch("order_products", $orderpro_data, "order_pro_id");
                try {
                    $view = $this->load->view("email_template/Payment_failed", $data, true);
                    send_Email($view, $uemail, $uname, "orders@birthdayowl.com", "birthdayowl.com", "Payment failure (Rs.$amount)");
                } catch (Exception $e) {
                    
                }
                $output["success"] = FALSE;
                $output["message"] = "egift can not send";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    //After egift purchased to add the data in database
    public function addEgiftOrders() {

        $postcount = count($_POST);
// user_id,timezone,quantity,status,is_guest,fname,femail,greeting_message,selected_amount,voucher_name,voucher_img,uploded_img_video,greeting_id,fphone,terms,type,delievery_schedule,voucher_pro_id,delievery_time,delivery_date,payment_mode,transaction_id,payuId
        if ($postcount > 0) {
            $output = array();
            $output["url"] = BASEURL . "Home_api/addEgiftOrders";
            $data["user_id"] = $orderpro_data["user_id"] = $orders["user_id"] = $payment["user_id"] = $user_id = check_post($this->input->post("user_id"));
            $orderpro_data["timezone"] = $timezone = check_post($this->input->post("timezone"));
            $quantity = check_post($this->input->post("quantity"));
            $status = check_post($this->input->post("status"));
            $is_guest = check_post($this->input->post("is_guest"));
            $orderpro_data["fname"] = $payment["bill_to_fname"] = check_post($this->input->post("fname"));
            $orderpro_data["femail"] = $payment["bill_to_email"] = check_post($this->input->post("femail"));
            $orderpro_data["vtitle"] = check_post($this->input->post("vtitle"));
            $orderpro_data["greeting_message"] = check_post($this->input->post("greeting_message"));
            $orderpro_data["selected_amount"] = $data["total_amount"] = $amount = check_post($this->input->post("selected_amount"));
            $orderpro_data["voucher_pro_name"] = check_post($this->input->post("voucher_name"));
            $orderpro_data["voucher_img"] = check_post($this->input->post("voucher_img"));
            $orderpro_data["uploded_img_video"] = check_post($this->input->post("uploded_img_video"));
            $orderpro_data["greeting_id"] = check_post($this->input->post("greeting_id"));
            $orderpro_data["fphone"] = check_post($this->input->post("fphone"));
            $orderpro_data["terms"] = check_post($this->input->post("terms"));
            $orderpro_data["type"] = check_post($this->input->post("type"));
            $orderpro_data["delievery_schedule"] = $delievery_schedule = check_post($this->input->post("delievery_schedule"));
            $orderpro_data["voucher_pro_id"] = $payment["product_id"] = $vid = check_post($this->input->post("voucher_pro_id"));
            $delivery_time = check_post($this->input->post("delievery_time"));
            $delivery_date = check_post($this->input->post("delivery_date"));

            $this->validate($user_id, "user id is required", $output, 1);
            $check_user_id = $this->Birthday->getSelectData("user_id", "users", "user_id='$user_id'")->num_rows();
            if ($check_user_id == 0) {
                $output["success"] = FALSE;
                $output["message"] = "user_id does not exist";
                echo json_encode($output);
                exit;
            }


            $this->validate($orderpro_data["greeting_message"], "Greeting message is required", $output, 1);
            $this->validate($amount, "selected_amount is required", $output, 1);
            $this->validate($orderpro_data["voucher_pro_name"], "voucher_pro_name is required", $output, 1);
            $this->validate($orderpro_data["voucher_img"], "voucher_img is required", $output, 1);
            $this->validate($orderpro_data["greeting_id"], "greeting_id is required", $output, 1);
            $this->validate($orderpro_data["fname"], "fname is required", $output, 1);
            //  $this->validate($delievery_schedule, "delievery_schedule is required", $output, 1);
            $user_info = $this->Birthday->getSelectData("email,first_name,mobile_no", "users", "user_id='$user_id'")->row_array();
            $data["user_email"] = $uemail = $orderpro_data["uemail"] = $data["email"] = $user_info["email"];
            $data["user_first_name"] = $uname = $data["uname"] = $orderpro_data["uname"] = $data["firstname"] = $user_info["first_name"];
            $data["phone"] = $user_info["mobile_no"];
            $orderpro_data["created_date"] = date("Y-m-d H:i:s");
            $returndata = DateConversion($delievery_schedule, $delivery_date, $delivery_time, $timezone);
            $orderpro_data["delivery_date_time"] = $delivery_date_time = $returndata["delivery_date_time"];
            $orderpro_data["converted_date"] = $returndata["converted_date"];
            if ($status == "1") {
                $payment["transaction_status"] = "success";
            } else {
                $payment["transaction_status"] = "Failure";
            }
            $payment["purchased_date"] = date("Y-m-d H:i:s");
            $payment["payment_mode"] = check_post($this->input->post("payment_mode"));
            $payment["transaction_id"] = check_post($this->input->post("transaction_id"));
            $payment["payuId"] = check_post($this->input->post("payuId"));
            $orders["status"] = "1";
            $orders["order_created_date"] = date("Y-m-d H:i:s");
            $payment["order_id"] = $orderpro_data["order_id"] = $order_id = $this->Birthday->addData("orders", $orders);
            $payment["total_amount"] = $final_amt = (($amount + ($amount * 0.035)) * $quantity);
            $payment_id = $this->Birthday->addData("payment", $payment);
            if ($is_guest == 1)
                $orderpro_data["uguest_user_id "] = $user_id;
            else
                $orderpro_data["uguest_user_id "] = 0;
            if ($status == "1") {
//                if ($delievery_schedule == '0' || $delievery_schedule == '1') {
                if ($delievery_schedule == '0') {
                    $orderpro_data["delivery_status"] = 1; //payment done and sending egift
                    $orderpro_data["order_message"] = "Sending egift...";
                } else {
                    $orderpro_data["delivery_status"] = 2;
                    $orderpro_data["order_message"] = "eGift will be delievered on $delivery_date_time";
                }
                $orderpro_data["unique_oid"] = "BHOWL00$order_id";
                $orderpro_data["order_id"] = $order_id;
                $orderpro_data["payment_id"] = $payment_id;
                $orderpro_data["selected_orders"] = 1;
                $order_pro_id = $this->Birthday->addData("order_products", $orderpro_data);
                $updated_data["same_quantity_id"] = "$order_pro_id";
                $this->Birthday->update("order_products", $updated_data, "order_pro_id='$order_pro_id'");
                $payment_data = $this->Birthday->getorderPaymentData($order_pro_id)->row_array();
                $data["order_id"] = $payment_data["order_id"];
                $data["bill_to_name"] = $payment_data["bill_to_fname"];
                $data["payment_mode"] = $payment_data["payment_mode"];
                $data["purchased_date"] = $payment_data["purchased_date"];
                $data["voucher_img"] = $payment_data["voucher_img"];
                $data["greeting_img"] = $payment_data["front_page_image"];
                $data["voucher_name"] = $payment_data["voucher_pro_name"];
                $data["fname"] = $payment_data["fname"];
                $data["samount"] = $data["selected_amount"] = $payment_data["selected_amount"];
                $data["femail"] = $payment_data["femail"];
                $data["greetmessage"] = $payment_data["greeting_message"];
                $data["type"] = $payment_data["type"];
                $data["media"] = $payment_data["uploded_img_video"];
                $data["uname"] = $data["sender_name"] = $uname;
                $data["uemail"] = $uemail;
                //payment confirmation email
                try {
                    $view = $this->load->view("email_template/Payment_received", $data, true);
                    $response = send_EmailResponse($view, $uemail, $uname, "orders@birthdayowl.com", "birthdayowl.com", "Payment Received (Rs.$final_amt) from BirthdayOwl");
                    if (SERVER_TYPE == "1") {
                        $admin_email = "abdul@birthdayowl.com";
                    } else {
                        $admin_email = "sbhuvad@techathalon.com";
                    }
                    $viewn = $this->load->view("email_template/Order_placed", $data, true);
                    send_Email($viewn, $admin_email, "abdul", "orders@birthdayowl.com", "birthdayowl.com", "Order has been placed  by $uname($uemail)");
                    $where = "order_pro_id=$order_pro_id";
                    $up["quantity"] = $quantity;
                    if ($response["code"] == "success") {
                        $up["payment_mail_status"] = "1";
                        $this->Birthday->update("order_products", $up, $where);
                    } else {
                        $up["payment_mail_status"] = "0";
                        $this->Birthday->update("order_products", $up, $where);
                    }
                } catch (Exception $e) {
                    $where = "order_pro_id=$order_pro_id";
                    $up["payment_mail_status"] = "0";
                    $this->Birthday->update("order_products", $up, $where);
                }
                $output["success"] = TRUE;
                $output["message"] = "egift sent Successfully";
            } else {
                $orderpro_data["selected_orders"] = 0;
                $orderpro_data["unique_oid"] = "BHOWL00$order_id";
                $orderpro_data["payment_id"] = $payment_id;
                $orderpro_data["delivery_status"] = 3; //payment done and sending egift
                $orderpro_data["order_message"] = "Payment failed";
                $order_pro_id = $this->Birthday->addData("order_products", $orderpro_data);
                $updated_data["same_quantity_id"] = "$order_pro_id";
                $updated_data["quantity"] = $quantity;
                $this->Birthday->update("order_products", $updated_data, "order_pro_id='$order_pro_id'");

                try {
                    $view = $this->load->view("email_template/Payment_failed", $data, true);
                    send_Email($view, $uemail, $uname, "orders@birthdayowl.com", "birthdayowl.com", "Payment failure (Rs.$final_amt)");
                } catch (Exception $e) {
                    
                }
                $output["success"] = FALSE;
                $output["message"] = "egift can not send";
            }

            if ($quantity > 1) {
                for ($i = 0; $i < ($quantity - 1); $i++) {
                    $qdata[$i]["same_quantity_id"] = $order_pro_id;
                    $qdata[$i]["quantity"] = $quantity;
                    $qdata[$i]["timezone"] = $timezone;
                    $qdata[$i]["order_id"] = $order_id;
                    $qdata[$i]["user_id"] = $user_id;
                    $qdata[$i]["voucher_pro_id"] = $vid;
                    $qdata[$i]["voucher_img"] = $orderpro_data["voucher_img"];
                    $qdata[$i]["terms"] = $orderpro_data["terms"];
                    $qdata[$i]["voucher_pro_name"] = $orderpro_data["voucher_pro_name"];
                    $qdata[$i]["uemail"] = $user_info["email"];
                    $qdata[$i]["uname"] = $user_info["first_name"];
                    $qdata[$i]["uploded_img_video"] = $orderpro_data["uploded_img_video"];
                    $qdata[$i]["type"] = $orderpro_data["type"];
                    $qdata[$i]["created_date"] = $orderpro_data["created_date"];
                    $qdata[$i]["fname"] = $orderpro_data["fname"];
                    $qdata[$i]["femail"] = $orderpro_data["femail"];
                    $qdata[$i]["greeting_message"] = $orderpro_data["greeting_message"];
                    $qdata[$i]["selected_amount"] = $orderpro_data["selected_amount"];
                    $qdata[$i]["fphone"] = $orderpro_data["fphone"];
                    $qdata[$i]["greeting_id"] = $orderpro_data["greeting_id"];
                    $qdata[$i]["delievery_schedule"] = $orderpro_data["delievery_schedule"];
                    $qdata[$i]["delivery_date_time"] = $orderpro_data["delivery_date_time"];
                    $qdata[$i]["converted_date"] = $orderpro_data["converted_date"];
                    $qdata[$i]["payment_id"] = $orderpro_data["payment_id"];
                    $qdata[$i]["unique_oid"] = $orderpro_data["unique_oid"];
                    $qdata[$i]["delivery_status"] = $orderpro_data["delivery_status"];
                    $qdata[$i]["order_message"] = $orderpro_data["order_message"];
                }
                $this->Birthday->addAData("order_products", $qdata);
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function gen_random_code($length) {
        $characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $randomString = "";
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    public function egift_failure() {
        $data["transaction_status"] = "failed";
        $data["trerror"] = 0;
        $this->load->view("EGIFT/mfailure", $data);
    }

//To add membership detail of user in database this api is required
    public function addMembershipInfo() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $output = array();
            $output["url"] = BASEURL . "Home_api/addMembershipInfo";
            $timezone = check_post($this->input->post("timezone"));
            $payment["user_id"] = $user_id = check_post($this->input->post("user_id"));
            $payment["plan_purchase_type"] = check_post($this->input->post("plan_purchase_type"));
            $status = check_post($this->input->post("status"));
            if ($status == "1") {
                $payment["transaction_status"] = "Success";
            } else {
                $payment["transaction_status"] = "Failure";
            }
            $payment["purchased_date"] = date("Y-m-d H:i:s");
            $payment["total_amount"] = $amount = check_post($this->input->post("total_amount"));
            $payment["payment_mode"] = check_post($this->input->post("payment_mode"));
            $payment["transaction_id"] = check_post($this->input->post("transaction_id"));
            $payment["payuId"] = check_post($this->input->post("payuId"));
            $this->validate($user_id, "user id is required", $output, 1);
            $this->validate($payment["plan_purchase_type"], "plan_purchase_type is required", $output, 1);
            $this->validate($payment["total_amount"], "total_amount is required", $output, 1);
            $this->validate($payment["payment_mode"], "payment_mode is required", $output, 1);
            $this->validate($payment["transaction_id"], "transaction_id is required", $output, 1);
            $this->validate($payment["payuId"], "payuId is required", $output, 1);
            $triggerOn = date('Y-m-d H:i:s');
            $schedule_date = new DateTime($triggerOn, new DateTimeZone($timezone));
            $schedule_date->setTimeZone(new DateTimeZone('Asia/Kolkata')); //server
            $new_date = $schedule_date->format('Y-m-d H:i:s');
            $payment["purchased_date"] = $new_date;
            if ($payment["plan_purchase_type"] == "1") {
                $cdate = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($payment["purchased_date"])));
            } else {
                $cdate = date('Y-m-d H:i:s', strtotime('+2 year', strtotime($payment["purchased_date"])));
            }
            $time = strtotime($cdate);
            $time = $time - (60);
            $payment["expiry_date"] = date("Y-m-d H:i:s", $time);
            $user_info = $this->Birthday->getSelectData("email,first_name,mobile_no", "users", "user_id='$user_id'")->row_array();
            $payment["bill_to_email"] = $uemail = $user_info["email"];
            $payment["bill_to_name"] = $mdata["name"] = $uname = $user_info["first_name"];
            $userphone = $user_info["mobile_no"];
            $payment["order_id"] = time() . mt_rand() . $user_id;
            $added_id = $this->Birthday->addData("greeting_membership", $payment);
            $where = "membership_id='$added_id' and user_id='$user_id'";
            $membership_data = $this->Birthday->getSelectData("order_id,bill_to_name,plan_purchase_type,payment_mode,purchased_date,expiry_date", "greeting_membership", $where)->row_array();
            //send email after purchasing membership
            $mdata["order_id"] = $membership_data["order_id"];
            $mdata["bill_name"] = $membership_data["bill_to_name"];
            $mdata["plan_purchase_type"] = $membership_data["plan_purchase_type"];
            $mdata["payment_mode"] = $membership_data["payment_mode"];
            $mdata["purchased_date"] = $membership_data["purchased_date"];
            $mdata["expiry_date"] = $membership_data["expiry_date"];
            $mdata["amount"] = $amount;
            if ($status == "1") {
                try {
                    $view = $this->load->view("email_template/membership_plan_success", $mdata, true);
                    $response = send_EmailResponse($view, $uemail, $uname, "greetings@birthdayowl.com", "birthdayowl.com", "Membership Received (Rs.$amount)");
                    $where = "membership_id=$added_id";
                    if ($response["code"] == "success") {
                        $up["is_mail_sent"] = "1";
                        $this->Birthday->update("greeting_membership", $up, $where);
                    } else {
                        $up["is_mail_sent"] = "0";
                        $this->Birthday->update("greeting_membership", $up, $where);
                    }
                } catch (Exception $e) {
                    $where = "membership_id=$added_id";
                    $up["is_mail_sent"] = "0";
                    $this->Birthday->update("greeting_membership", $up, $where);
                }
                //sms
                if ($userphone != '') {
                    $show_message = "Dear $uname, \n Thank you for registering with Birthday Owl.\n We confirm receipt of your membership fee of (INR $amount) and have sent you an email to your registered email id.\n - Birthday Owl (www.birthdayowl.com)";
                    $this->Birthday->send_sms($userphone, $show_message);
                }
                $updated_data["membership_status"] = 1;
                $this->Birthday->update("users", $updated_data, "user_id='$user_id'");
            } else {
                $updated_data["membership_status"] = 0;
                $this->Birthday->update("users", $updated_data, "user_id='$user_id'");
            }
            $membership_statusinfo = $this->Birthday->getSelectData("membership_status", "users", "user_id='$user_id'")->row_array();
            $plans = $this->Birthday->getSelectData("amount,duration", "membership_plans", "status=1")->result_array();
            $data["membership_status"] = $membership_statusinfo["membership_status"];
            if ($data["membership_status"] == 1) {
                $selected = "greeting_membership.membership_id,greeting_membership.plan_purchase_type,greeting_membership.expiry_date";
                $where = "greeting_membership.user_id=$user_id and greeting_membership.transaction_status='success'";
                $planinfo = $this->Birthday->getSelectData($selected, "greeting_membership", $where)->result_array();
                $output["plan_info"] = $planinfo;
                $output["plans"] = $plans;
                $output["membership_status"] = $membership_statusinfo["membership_status"];

                $output["success"] = TRUE;
                $output["message"] = "Membership purchased Successfully";
            } else {
                $output["membership_status"] = $membership_statusinfo["membership_status"];
                $output["plans"] = $plans;
                $output["plan_info"] = array();
                $output["success"] = FALSE;
                $output["message"] = "Membership purchased Failed";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function membership_failure() {
        $data["trerror"] = 0;
        $data["transaction_status"] = "failed";
        $this->load->view("EGIFT/egift_failure", $data);
    }

    public function send_success_response() {
        $data["transaction_status"] = "success";
        $data["trerror"] = 1;
        $this->load->view("EGIFT/egift_success", $data);
    }

    public function display_order_history() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $output = array();
            $user_id = check_post($this->input->post("user_id"));
            $output["url"] = BASEURL . "Home_api/display_order_history";
            $output["success"] = TRUE;
            $history = $this->Birthday->get_order_history($user_id)->result_array();
            if (count($history) > 0) {
                $output["orders"] = $history;
                $output["count"] = count($history);
            } else {
                $output["vouchers"] = count($history);
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function get_random_greetingsNVouchers() {
        $pro_data = $this->Birthday->getMinMaxVouchers("voucher_product")->result_array();
        $min = $pro_data[0]["Min(voucher_pro_id)"];
        $max = $pro_data[0]["Max(voucher_pro_id)"];
        $id = rand($min, $max);
        $where = "voucher_pro_id='$id'";
        $output["product"] = $this->Birthday->getUserData("voucher_product", $where)->row_array();
        $card_data = $this->Birthday->getMinMaxGreetings("greeting_card")->result_array();
        $min_greet = $card_data[0]["Min(card_id)"];
        $max_greet = $card_data[0]["Max(card_id)"];
        $gid = rand($min_greet, $max_greet);
        $where1 = "card_id='$gid'";
        $output["gcards"] = $this->Birthday->getUserData("greeting_card", $where1)->row_array();
        $output["url"] = BASEURL . "Home_api/get_random_greetingsNVouchers";
        $output["success"] = true;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function redemption() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $output = array();
            $voucher_pro_id = check_post($this->input->post("voucher_pro_id"));
            $output["url"] = BASEURL . "Home_api/redemption";
            $output["success"] = true;
            $output["no"] = "1";
            $vouchers = $this->Birthday->geRedmetion("voucher_product", "voucher_pro_id = $voucher_pro_id")->row_array();
            $string = strip_tags($vouchers["redemption_details"]);
            $text = str_replace("\r", '', $string);
            $output["redemption"] = $text;
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function location() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $output = array();
            $voucher_pro_id = check_post($this->input->post("voucher_pro_id"));
            $output["url"] = BASEURL . "Home_api/location";
            $output["success"] = true;
            $output["no"] = "2";
            $vouchers = $this->Birthday->geRedmetion("voucher_product", "voucher_pro_id = $voucher_pro_id")->row_array();
            $string = strip_tags($vouchers["locations"]);
            $text = str_replace("\r", '', $string);
            $output["locations"] = $text;
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function Terms() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $output = array();
            $voucher_pro_id = check_post($this->input->post("voucher_pro_id"));
            $output["url"] = BASEURL . "Home_api/Terms";
            $output["success"] = true;
            $output["no"] = "3";
            $vouchers = $this->Birthday->geRedmetion("voucher_product", "voucher_pro_id = $voucher_pro_id")->row_array();
            $string = strip_tags($vouchers["terms_conditions"]);
            $text = str_replace("\r", '', $string);
            $output["terms"] = $text;
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function faq() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $output = array();
            $empty = array();
            $output["url"] = BASEURL . "Home_api/faq";
            $output["success"] = true;
            $info = $this->Birthday->getAllData("faq")->result_array();
            if (count($info) > 0) {
                $output["faq"] = $info;
            } else {
                $output["faq"] = $empty;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function order_history() {
        $postcount = count($_POST);
        $output = array();
        $output["url"] = BASEURL . "Home_api/order_history";
        if ($postcount > 0) {
            $empty = array();
            $user_id = check_post($this->input->post("user_id"));
            $this->validate($user_id, "user_id is required", $output, 1);
            $check_user_id = $this->Birthday->getSelectData("user_id", "users", "user_id='$user_id'")->num_rows();
            if ($check_user_id == 0) {
                $output["success"] = FALSE;
                $output["message"] = "user_id does not exist";
                echo json_encode($output);
                exit;
            }
            $output["success"] = TRUE;
            $orders = $this->Birthday->getJoinedData2("op.order_message,op.delivery_date_time,op.order_pro_id, op.delievery_schedule, op.fphone,op.voucher_pro_name,op.type,op.voucher_img,op.delivery_status,op.selected_amount,op.femail,op.fname, op.unique_oid,op.delivery_date_time, g.front_page_image,op.uploded_img_video,count(op.unique_oid)as quantity", "order_products as op", "greeting_card as g", "op.greeting_id=g.card_id", "op.user_id=$user_id and op.delivery_status!='' group by op.unique_oid order by op.order_pro_id DESC")->result_array();
            if (count($orders) > 0) {
                $output["order_history"] = $orders;
                $output["count"] = count($orders);
            } else {
                $output["success"] = FALSE;
                $output["order_history"] = $empty;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function giftWallet() {
        $postcount = count($_POST);
        $output = array();
        $output["url"] = BASEURL . "Home_api/giftWallet";
        if ($postcount > 0) {
            $empty = array();
            $user_id = check_post($this->input->post("user_id"));
            $this->validate($user_id, "user_id is required", $output, 1);
            $userinfo = $this->Birthday->getSelectData("email,user_id", "users", "user_id='$user_id'")->row_array();
            if (count($userinfo) == 0) {
                $output["success"] = FALSE;
                $output["message"] = "user_id does not exist";
                echo json_encode($output);
                exit;
            }
            $email = $userinfo["email"];

            // $orders = $this->Birthday->getJoinedData2("greeting_card.front_page_image,order_products.uploded_img_video,   order_products.uemail,order_products.type, order_products.uname, order_products.unique_oid, order_products.voucher_img,   order_products.order_pro_id,order_products.voucher_pro_id,order_products.voucher_pro_name,order_products.selected_amount,order_products.greeting_id,order_products.greeting_message,count(order_products.unique_oid) as quantity", "order_products", "greeting_card", "order_products.greeting_id=greeting_card.card_id", "order_products.delivery_status=4 and order_products.femail='$email' group by order_products.unique_oid order by order_products.order_pro_id DESC")->result_array();
            $orders = $this->Birthday->getJoinedDataThreetable("greeting_card.front_page_image,order_products.uploded_img_video,order_products.uemail,order_products.type, order_products.uname, order_products.unique_oid, order_products.voucher_img,   order_products.order_pro_id,order_products.voucher_pro_id,order_products.voucher_pro_name,order_products.selected_amount,order_products.greeting_id,order_products.greeting_message,payment.voucher_code,payment.pin,payment.expiry_date,count(order_products.unique_oid) as quantity, CASE WHEN order_products.delivery_date_time  = '0000-00-00 00:00:00' THEN DATE_FORMAT(order_products.created_date, '%Y-%m-%d %h:%i:%s %p')  ELSE DATE_FORMAT(order_products.delivery_date_time, '%Y-%m-%d %h:%i:%s %p')  END AS delivery_datetime", "order_products", "greeting_card", "payment", "order_products.greeting_id=greeting_card.card_id", "order_products.payment_id=payment.payment_id", "order_products.delivery_status=4 and order_products.femail='$email' group by order_products.unique_oid order by order_products.order_pro_id DESC", "inner")->result_array();
            if (count($orders) > 0) {
                $output["success"] = TRUE;
                $output["order_history"] = $orders;
                $output["count"] = count($orders);
            } else {
                $output["success"] = FALSE;
                $output["order_history"] = $empty;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function calculateHash() {
        $key = $_POST["key"];
        if (SERVER_TYPE == '3') {
            $salt = PSALT;
        } else {
            $salt = PSALT;
        }
        $txnId = $_POST["txnid"];
        $amount = $_POST["amount"];
        $productName = $_POST["productInfo"];
        $firstName = $_POST["firstName"];
        $email = $_POST["email"];
        $udf1 = $_POST["udf1"];
        $udf2 = $_POST["udf2"];
        $udf3 = $_POST["udf3"];
        $udf4 = $_POST["udf4"];
        $udf5 = $_POST["udf5"];
        $payhash_str = $key . '|' . $txnId . '|' . $amount . '|' . $productName . '|' . $firstName . '|' . $email . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '|' . $salt;
        $hash = strtolower(hash('sha512', $payhash_str));
        $arr['result'] = $hash;
        $arr['status'] = 0;
        $arr['errorCode'] = null;
        $arr['responseCode'] = null;
        $output = $arr;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

//This api is used  when user wants OTP.We are taking mobile number as a input and sending OTP to that number.
    public function send_otp() {
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $output["url"] = BASEURL . "Home_api/send_otp";
            $data["mobile_no"] = $mobile_no = check_post($this->input->post("mobile_no"));
            $hash_key = check_post($this->input->post("hash_key"));
            $check_mb = $this->Birthday->getSelectData("mobile_no", "users", "mobile_no=$mobile_no")->result_array(); //is_guest=0
            if (count($check_mb) > 0) {
                $data["code"] = $code = $this->generateRandomString(5);
                $otp_data = $this->Birthday->getSelectData("mobile_no", "otp", "mobile_no=$mobile_no")->result_array();
                $count = count($otp_data);
                if ($count > 0) {
                    $update["code"] = $code;
                    $update["sent_time"] = date("H:i:s");
                    $this->Birthday->update("otp", $update, "mobile_no=$mobile_no");
                } else {
                    $data["created_date"] = date("Y-m-d H:i:s");
                    $data["sent_time"] = date("H:i:s");
                    $this->Birthday->addData("otp", $data);
                }

                $show_message = "<#> Enter the following code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone. $code $hash_key";
//                $show_message = "Enter $code as your verification code to login to your BirthdayOwl account.Valid for 5 minutes.Do not share it with anyone.";
                try {
                    $resp = $this->Birthday->send_mobile_otp($data["mobile_no"], $show_message);
                    $output["success"] = true;
                    $output["message"] = "OTP sent successfully";
                } catch (Exception $e) {
                    $output["success"] = true;
                    $output["message"] = "Failed.Please try again";
                }
            } else {
                $output["success"] = FALSE;
                $output["message"] = "Given Mobile Number is not registered in BirthdayOwl.com";
                echo json_encode($output);
                exit;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    //To verify sent OTP this api is used
    public function verify_otp() {
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $output["url"] = BASEURL . "Home_api/verify_otp";
            $otp = check_post($this->input->post("otp"));
            $otp_data = $this->Birthday->getSelectData("mobile_no", "otp", "code='$otp'")->row_array();
            $count = count($otp_data);
            if ($count > 0) {
                $mobile_no = $otp_data["mobile_no"];
                $userdata = $this->Birthday->getSelectData("mobile_no,email,first_name,user_id", "users", "mobile_no=$mobile_no")->row_array();
                $userdatacount = count($userdata);
                if ($userdatacount > 0) {
                    $data["username"] = $email = $userdata["email"];
                    $data["realName"] = $name = $userdata["first_name"];
                    $user_id = $userdata['user_id'];
                    $euserid = $this->Birthday->encryptPassword($user_id);
                    $data["link"] = BASEURL . "Home_web/change_password/$euserid";
                    $view = $this->load->view("email_template/ResetPassword", $data, true);
                    $result = send_EmailResponse($view, $email, $name, "resetpassword@birthdayowl.com", "birthdayowl.com", "BirthdayOwl : Reset Password");
                    if ($result["code"] == "success") {
                        $reset["reset_activate"] = 1;
                        $reset["reset_time"] = date("Y-m-d H:i:s");
                        $this->Birthday->update("users", $reset, "user_id = $user_id");
                    }
                    $output["success"] = true;
                    $output["message"] = "We have sent Password recovery details to $email ";
                } else {
                    $output["success"] = true;
                    $output["message"] = "We do not have any account associated with $mobile_no.Please signup and create new account.";
                }
            } else {
                $output["success"] = true;
                $output["message"] = "Wrong OTP.Try again";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

//    public function generateRandomString($length) {
//        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
//        $charactersLength = strlen($characters);
//        $randomString = '';
//        for ($i = 0; $i < $length; $i++) {
//            $randomString .= $characters[rand(0, $charactersLength - 1)];
//        }
//        return $randomString;
//    }
//    
    //To check sent greeting count this api is used
    public function check_greeting_count() {
        $select = "sent_greeting_counter,membership_status";
        $output["url"] = BASEURL . "Home_api/check_greeting_count";
        $user_id = check_post($this->input->post("user_id"));
        $this->validate($user_id, "user_id is required", $output, 1);
        $check_user_id = $this->Birthday->getSelectData("user_id", "users", "user_id='$user_id'")->num_rows();
        if ($check_user_id == 0) {
            $output["success"] = FALSE;
            $output["message"] = "user_id does not exist";
            echo json_encode($output);
            exit;
        }
        $getsentcounter = $this->Birthday->getSelectData($select, "users", "user_id='$user_id'")->row_array();
        $output["sent_greeting_counter"] = $getsentcounter["sent_greeting_counter"];
        $output["membership_status"] = $getsentcounter["membership_status"];
        $output["success"] = true;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    //if user wants to send egift again then this api is used
    public function resend_gift() {
        if (count($_POST) > 0) {
            $data["order_pro_id"] = $order_pro_id = check_post($this->input->post("order_pro_id"));
            $where = "order_pro_id='$order_pro_id'";
            $order_productsdata = $this->Birthday->getorderPaymentData($order_pro_id)->row_array();
            $woohoo_orderid = $order_productsdata["woohoo_order_id"];
            $product_name = $order_productsdata["voucher_pro_name"];
            $user_id = $order_productsdata["user_id"];
            $userinfo = $this->Birthday->getSelectData("mobile_no", "users", "user_id='$user_id'")->row_array();
            $userphone = $userinfo["mobile_no"];
            $temporaryCredentialsRequestUrl = temporaryCredentialsRequestUrl; //request token url
            $authorizationUrl = authorizationUrl; //authorize url
            $accessTokenRequestUrl = accessTokenRequestUrl; //access token url
            $consumerKey = consumerKey;
            $consumerSecret = consumerSecret;
            $username = Wusername;
            $password = Wpassword;
            if (!isset($_SESSION)) {
                session_start();
            }
            session_write_close();
            try {
                $authType = OAUTH_AUTH_TYPE_URI;
                $oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);
                $requestToken = $oauthClient->getRequestToken($temporaryCredentialsRequestUrl); // return type array
                $ch = curl_init();
                $url = $authorizationUrl . $requestToken['oauth_token'] . '&username=' . $username . '&password=' . $password;
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0); //1 for a post request
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                $verifierResponse = json_decode(curl_exec($ch));
                curl_close($ch);
                if ($verifierResponse->success == 1) {
                    $oauthClient->setToken($requestToken['oauth_token'], $requestToken['oauth_token_secret']);
                    $verifier_token = $verifierResponse->verifier;
                    $accessToken = $oauthClient->getAccessToken($accessTokenRequestUrl, null, $verifier_token); // return type array
                    $_SESSION['oauth_token'] = $acc_token = $accessToken['oauth_token'];
                    $_SESSION['oauth_token_secret'] = $acces_sec_key = $accessToken["oauth_token_secret"];
                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = "Accept: */*";
                    $oauthClient->setAuthType(OAUTH_AUTH_TYPE_AUTHORIZATION);
                    $oauthClient->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
                    $murl = wurl;
                    $oauthClient->fetch($murl . "resend/$woohoo_orderid", array(), 'GET', array('Content-Type' => 'application/json', 'Accept' => '*/*'));
                    $resend_data = json_decode($oauthClient->getLastResponse(), TRUE);
                    $data["success_status"] = $resend_data["success"];
                    $data["status"] = $resend_data["status"];
                    $data["order_id"] = $resend_data["order_id"];
                    if ($data["status"] == "Complete") {
                        $data["worder_id"] = $resend_data["order_id"];
                        $data["expiry_date"] = $resend_data["carddetails"][$product_name][0]["expiry_date"];
                        $data["code"] = $resend_data["carddetails"][$product_name][0]["cardnumber"];
                        $data["pin"] = $resend_data["carddetails"][$product_name][0]["pin_or_url"];
                        $data["euser_id"] = $euser_id = $this->Birthday->encryptPassword($user_id);
                        $keyid = "eGift@birthdayOwl";
                        $data["greeting_id"] = $greeting_id = $this->Birthday->encryptPassword($order_productsdata["greeting_id"]);
                        $data["pay_id"] = $pay_id = $this->Birthday->encryptPassword($order_productsdata["payment_id"]);
                        $data["key"] = $key = $this->Birthday->encryptPassword($keyid);
                        $data["opid"] = $opid = $this->Birthday->encryptPassword($order_pro_id);
                        $data["linkclick"] = $link = BASEURL . "Home_web/voucher_email/$euser_id/$pay_id/$greeting_id/$opid/$key";
                        $text = nl2br(trim(strip_tags($order_productsdata["greeting_message"], '<br/>')));
                        $data["message"] = $message = $text;
                        $data["product_image"] = $order_productsdata["voucher_img"];
                        $data["voucher_pro_name"] = $voucher_name = $order_productsdata["voucher_pro_name"];
                        $data["total"] = ($order_productsdata["selected_amount"] );
                        $data["greeting"] = $order_productsdata["front_page_image"];
                        $data["femail"] = $femail = $order_productsdata["femail"];
                        $data["fname"] = $rec_name = $order_productsdata["fname"];
                        $data["fphone"] = $fphone = $order_productsdata["fphone"];
                        $data["uemail"] = $uemail = $order_productsdata["uemail"];
                        $data["uname"] = $uname = $order_productsdata["uname"];
                        $data["terms"] = $order_productsdata["terms"];
                        $data["type"] = $order_productsdata["type"];
                        $linkData = $this->google_url_api->shorten("$link");
                        $data["click_link"] = $click_link = $linkData->id;
                        $img = BASEURL_GREETINGS . $data["greeting"];
                        if ($femail != '') {
                            require_once(APPPATH . "libraries/Mailin.php" );
                            $this->load->library('Pdf'); // Load library
                            $img = BASEURL_CROPPED_GREET . $data["greeting"];
                            if (ob_get_length() > 0) {
                                ob_end_clean();
                            }
                            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                            $pdf->SetPrintHeader(false);
                            $pdf->SetPrintFooter(false);
                            $pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);
                            $pdf->AddPage("P", " A4");

                            $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 5, 10, 'color' => array(0, 0, 0));
                            $style1 = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 5, 10, 'color' => array(0, 0, 0));

///////////////////
                            $cardimg = $data["product_image"];
                            $barcode = BASEURL_OIMG . "barcode.png";
                            $logo = BASEURL_OIMG . "logo.png";
                            $barcode_scanner = BASEURL_OIMG . "barcode_scanner.jpeg";
                            $termsDisplay = $data['terms'];
                            $total = $data["total"];
                            $codeDisplay = $data["code"];
                            $pinDisplay = $data["pin"];
                            $valid = $data["expiry_date"];
                            $type = $data["type"];
                            $linkd = $data["click_link"];
                            $uname = $data["uname"];
                            if ($type == 1) {
                                $message_type = "$uname sent you image for you that is available on this link ";
                            } else if ($type == 2) {
                                $message_type = "$uname recorded a video message for you that is available on this link";
                            } else if ($type == 3) {
                                $message_type = "$uname recorded a audio message for you that is available on this link";
                            } else {
                                $message_type = "";
                            }


                            $terms = <<<EOD
<div style="text-align:left;font-size:8px;color:black;">$termsDisplay</div>
EOD;

                            $html = <<<EOD
<div style="text-align:center;font-size:7px;color:black;">Your Gift Card Value</div>
EOD;

                            $amt = <<<EOD
<div style="text-align:center;font-size:16px;font-weight:bold;color:#5CAD01;">Rs.$total</div>
EOD;
                            $codelabel = <<<EOD
<div style="text-align:center;font-size:7px;color:black;">Gift Code</div>
EOD;

                            $code = <<<EOD
<div style="text-align:center;font-size:10px;font-weight:800;color:#5CAD01;">$codeDisplay</div>
EOD;

                            $pinlabel = <<<EOD
<div style="text-align:center;font-size:7px;color:black;">PIN</div>
EOD;


                            $pin = <<<EOD
<div style="text-align:center;font-size:10px;font-weight:800;color:#5CAD01;">$pinDisplay</div>
EOD;



                            $validity = <<<EOD
<div style="text-align:center;font-size:11px;font-weight:600;color:black;">Valid until $valid</div>
EOD;

                            $exp = <<<EOD
<div style="text-align:center;font-size:8px;font-weight:600;color:#5CAD01;">Experiance your Gift Card online</div>
EOD;
                            $link = <<<EOD
<div style="text-align:center;font-size:10px;font-weight:600;color:black;"><i>$linkd</i></div>
EOD;
                            $linkn = <<<EOD
<div style="text-align:center;font-size:10px;font-weight:600;color:black;">$linkd</div>
EOD;
                            $messageDisplay = <<<EOD
<div style="text-align:center;font-size:10px;font-weight:600;color:black;"><i>$message</i></div>
EOD;
                            $termslabel = <<<EOD
<div style="text-align:left;font-size:10px;font-weight:bold;color:black;">Terms & Conditions</div>
EOD;
                            $mediamessage = <<<EOD
<div style="text-align:center;font-size:10px;font-weight:600;color:black;"><i>$message_type</i></div>
EOD;
                            $pdf->StartTransform();
                            $pdf->Rotate(-180, 53, 73);
                            $pdf->Image($img, 8, 8, 90, 130, "", "", "M", FALSE, 8);
                            $pdf->Image($logo, -70, 128, 20, 8, "", "", "M", FALSE, 8);

// Stop Transformation
                            $pdf->StopTransform();
                            $pdf->StartTransform();
                            $pdf->Rotate(180, 158, 104);

                            $pdf->writeHTMLCell(100, 220, 110, 75, $terms, 0, 0, false, true, 'center', true);
                            $pdf->writeHTMLCell(100, 220, 110, 70, $termslabel, 0, 0, false, true, 'center', true);
                            $pdf->StopTransform();
                            $pdf->Line(80, 10800, 105.5, 1, $style1);
                            $pdf->Line(500, 146, 1, 146, $style);
                            $style3 = array('width' => 6, 'cap' => 'round', 'join' => 'miter', 'dash' => '0', 'color' => array(0, 0, 0));
                            $pdf->Rect(11, 155, 91, 132, 'D', array('all' => $style3));
                            $pdf->Image($cardimg, 120, 170, 70, 40, "", "", "M", FALSE, 8);
                            $pdf->writeHTMLCell(45, 20, 35, 180, $messageDisplay, 0, 0, false, true, 'center', true);
                            $pdf->writeHTMLCell(45, 20, 35, 225, $mediamessage, 0, 0, false, true, 'center', true);
                            $pdf->Image($barcode_scanner, 50, 250, 12, 10, "", "", "M", FALSE, 8);
                            $pdf->writeHTMLCell(45, 20, 35, 245, $link, 0, 0, false, true, 'center', true);
                            $pdf->writeHTMLCell(190, 100, 60, 210, $html, 0, 0, false, true, 'center', true);
                            $pdf->writeHTMLCell(190, 100, 60, 215, $amt, 0, 0, false, true, 'center', true);
                            $pdf->writeHTMLCell(190, 100, 60, 224, $codelabel, 0, 0, false, true, 'center', true);
                            $pdf->Image($barcode, 132, 228, 45, 10, "", "", "M", FALSE, 8);
                            $pdf->writeHTMLCell(190, 100, 60, 238, $code, 0, 0, false, true, 'center', true);
                            $pdf->writeHTMLCell(190, 100, 60, 245, $pinlabel, 0, 0, false, true, 'center', true);
                            $pdf->writeHTMLCell(190, 100, 60, 248, $pin, 0, 0, false, true, 'center', true);
                            $pdf->writeHTMLCell(190, 100, 60, 255, $validity, 0, 0, false, true, 'center', true);
// $pdf->Image($barcode_scanner, 112, 256, 12, 10, "", "", "M", FALSE, 8);
                            $pdf->writeHTMLCell(175, 100, 60, 260, $exp, 0, 0, false, true, 'center', true);
                            $pdf->writeHTMLCell(168, 100, 60, 264, $link, 0, 0, false, true, 'center', true);
                            $pdf->Image($barcode_scanner, 112, 260, 12, 10, "", "", "M", FALSE, 8);
                            $pdf->Image($logo, 180, 259, 20, 8, "", "", "M", FALSE, 8);
                            $lstyle = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '0', 'color' => array(79, 160, 3));
                            $pdf->Line(110, 272, 180, 272, $lstyle);

                            $filename = "files/Voucher_greeting.pdf";
                            $contentPdf = $pdf->Output($filename, 'S');
                            $attachment_content = chunk_split(base64_encode($contentPdf));
                            $attachment = array('Voucher_greeting.pdf' => $attachment_content);
                            $femail = $data["femail"];
                            $rec_name = $data["fname"];
                            $voucher_name = $data["voucher_pro_name"];
                            $data["order_pro_id"] = $order_pro_id;
                            try {
                                $subject = "$voucher_name From BirthdayOwl";
                                $mailin = new Mailin('https://api.sendinblue.com/v2.0', SENDINKEY);
                                $view = $this->load->view("email_template/Product_voucher", $data, true);
                                $sdata = array("to" => array("$femail" => "$rec_name"),
                                    "from" => array("orders@birthdayowl.com", "$uname"),
                                    "subject" => "$subject",
                                    "text" => "$view",
                                    "html" => "$view",
                                    "attachment" => $attachment
                                );
                                $resp = $mailin->send_email($sdata);
                                $where = "order_pro_id=$order_pro_id";
                                if ($resp["code"] == "success") {

                                    $up["egift_order_mail_status"] = 1;
                                    $this->Birthday->update("order_products", $up, $where);
                                } else {
                                    $up["egift_order_mail_status"] = 0;
                                    $this->Birthday->update("order_products", $up, $where);
                                }
                            } catch (Exception $e) {
                                $where = "order_pro_id=$order_pro_id";
                                $up["egift_order_mail_status"] = 1;
                                $this->Birthday->update("order_products", $up, $where);
                            }
                        }
                        if ($fphone != '') {
                            $show_message = "Dear $rec_name, \n You have just received an awesome e-gift voucher from $uname. \n To view and redeem your e-gift voucher, simply ($click_link)  \n- Birthday Owl (www.birthdayowl.com)";
                            $this->Birthday->send_sms($data["fphone"], $show_message);
                        }
                        if ($userphone != '') {
                            $show_message = "Dear $uname, \n You have just sent an awesome e-gift voucher to $rec_name and we have notified $rec_name accordingly.\n - Birthday Owl (www.birthdayowl.com)";
                            $this->Birthday->send_sms($userphone, $show_message);
                        }

                        $output["success"] = true;
                        $output["message"] = "eGift resent Successfully!";
                    }
                }
            } catch (OAuthException $e) {
                $output["success"] = false;
                $output["message"] = "eGift can not resend ";
            }

            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

//To subscirbe/unsubscribe this api is used
    public function unsubscribe() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/unsubscribe";
        $email_id = check_post($this->input->post("email_id"));
        $status = check_post($this->input->post("mail_status")); //subscribe=0/ unsubscribe=1
        $where = "email_id='$email_id'";
        $update["mail_status"] = $status;
        $this->Birthday->update("news_letter", $update, $where);
        if ($status == 0) {
            $output["message"] = "You're unsubscribed.";
            $output["mail_status"] = 1;
        } else {
            $output["message"] = "You're subscribed!";
            $output["mail_status"] = 0;
        }
        $output["success"] = "true";
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    //unused right now

    public function getGreetingCategory() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/getGreetingCategory";
        $output["success"] = TRUE;
        $where = "greeting_type='1'";
        $vouchers = $this->Birthday->getUserData("greeting_category", $where)->result_array();
        if (count($vouchers) > 0) {
            $output["greeting_category"] = $vouchers;
        } else {

            $output["greeting_category"] = count($vouchers);
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function get_slideImages() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/get_slideImages";
        $output["success"] = TRUE;
        $vouchers_cat = $this->Birthday->get_active_banner()->result_array();
        if (count($vouchers_cat) > 0) {
            $output["screens"] = $vouchers_cat;
        } else {
            $output["screens"] = "";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function getVoucherCategory() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/getVoucherCategory";
        $output["success"] = TRUE;
        $vouchers_cat = $this->Birthday->getAllData("voucher_category")->result_array();
        if (count($vouchers_cat) > 0) {
            $output["voucher_category"] = $vouchers_cat;
        } else {
            $output["voucher_category"] = "";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function getMostPopularVouchers() {
        $output = array();
        $output["url"] = BASEURL . "Home_api/getMostPopularVouchers";
        $output["success"] = TRUE;
        $vouchers = $this->Birthday->get_most_popularvouchers()->result_array();
        $where = "status='1'";
        $amountinfo = $this->Birthday->getUserData("voucher_amount", $where)->result_array();
        $info = array_column($amountinfo, "voucher_id");
        for ($i = 0; $i < count($vouchers); $i++) {
            if (in_array($vouchers[$i]["voucher_pro_id"], $info)) {
                $vouchers[$i]['stock_status'] = "1";
            } else {
                $vouchers[$i]['stock_status'] = "0";
            }
        }

        if (count($vouchers) > 0) {
            $output["vouchers"] = $vouchers;
        } else {
            $output["vouchers"] = count($vouchers);
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function getVoucherGreetingcard() {
        $output = array();
        $empty = array();
//  $greet_cat_id = check_post($this->input->post("greet_cat_id"));
        $output["url"] = BASEURL . "Home_api/getVoucherGreetingcard";
        $output["success"] = true;
//$where = "greet_cat_id = '$greet_cat_id 'and greeting_type=1";
        $where = "greeting_type=2";
        $vouchers = $this->Birthday->getUserData("greeting_card", $where)->result_array();

        if (count($vouchers) > 0) {
            $output["success"] = true;
            $output["greeting_card"] = $vouchers;
            $output["greeting_card_count"] = count($vouchers);
        } else {
            $output["greeting_card"] = $empty;
            $output["success"] = true;
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function check_voucher_Stock() {
        $postcount = count($_POST);
        if ($postcount > 0) {
            $output = array();
            $empty = array();
            $voucher_id = check_post($this->input->post("voucher_id"));
            $output["url"] = BASEURL . "Home_api/check_voucher_Stock";
            $output["success"] = TRUE;
            $where = "voucher_id='$voucher_id and status='1'";
            $vouchers = $this->Birthday->getUserData("voucher_amount", $where)->result_array();
            if (count($vouchers) > 0) {
                $output["vcount"] = count($vouchers);
            } else {
                $output["vcount"] = $empty;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function getProductVouchers() {
        $output = array();
        $empty = array();
        $output["url"] = BASEURL . "Home_api/getProductVouchers";
        $output["success"] = TRUE;
        $file = BASEURL . "files/products.txt";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $file);
        $products = curl_exec($ch);
        curl_close($ch);
        $vouchers = array_values(json_decode($products, True));
        if (count($vouchers) > 0) {
            $output["vouchers"] = $vouchers;
        } else {
            $output["vouchers"] = $empty;
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function getAllProductVouchers() {
        $vouchers = array_values(getAllVouchers());
        $output = array();
        $empty = array();
        $output["url"] = BASEURL . "Home_api/getAllProductVouchers";
        $output["success"] = TRUE;
        if (count($vouchers) > 0) {
            $output["vouchers"] = $vouchers;
        } else {
            $output["vouchers"] = $empty;
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // for flowers and cakes

    public function getFlowerAndCakes() {

//        if (count($_POST) > 0) {
//            
//        } else {
//            $output["success"] = FALSE;
//            $output["message"] = "No data found";
//        }
        //the below part only provides limited data for the flower and cakes mostviewed and details
        $data = array();
//        $data["flower_product_min_price"] = $this->Birthday->getMin_price("product_new")->row_array();
//        $data["flower_product_max_price"] = $this->Birthday->getMax_price("product_new")->row_array();
//        $data["flower_product"] = $this->Birthday->getLimitedData_new("product_new")->result_array();
//        $data["most_viewed"] = $this->Birthday->getMostViewedDataWithOrderBy("product_new")->result_array();
//        $data["best_selling"] = $this->Birthday->getBestSellingDataWithOrderBy("product_new")->result_array();
//        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //the above part provides detailed viewed of all the records without limit and offset
        $where = "status = 'Active' and 1 ORDER BY RAND()";
        $flower_product = $this->Birthday->getFlowerCategory("product_new", $where)->result_array();
        if (count($flower_product) > 0) {
            $data["success"] = TRUE;
            $data["count"] = count($flower_product);
            $data['flower_product'] = $flower_product;
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
    }

    public function getSelectedFlowerAndCakes() {

        //need to take any previous product array
        if (count($_POST) > 0) {
            $pro_id = $this->input->post('product_id');
            $where = "status = 'Active' and pro_id = '$pro_id'";
            $output['zip_data'] = $this->Birthday->getAllData("zip_code")->result_array();

            $flower_product = $this->Birthday->getUserData("product_new", $where)->result_array();
            $updated_data['most_viewed'] = $flower_product[0]['most_viewed'] + 1;

            //just need to increment the product count in our table
            $updated_data['most_viewed'] = $flower_product[0]['most_viewed'] + 1;
            $flower_product_update = $this->Birthday->update("product_new", $updated_data, $where);

//            $flower_product = $this->Birthday->getUserData("product_new", $where)->result_array();
//            $output["title"] = $flower_product[0]['product_name'];
//            $output['product_description'] = $flower_product[0]['product_description'];
//            $output['category_name'] = $flower_product[0]['category_name'];
//            $output['price'] = $flower_product[0]['price'];
//            $output['stock_status_id'] = $flower_product[0]['stock_status_id'];
//            $output['product_image'] = $flower_product[0]['product_image'];
//            $output['HSN_code_flower'] = $flower_product[0]['HSN_code_flower'];
            $date = date("Y-m-d H:i:s");
            $output["server_time"] = $date;
            $output["success"] = TRUE;
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function checkoutToCart() {
        //take the post parameter
        $output = array();
    }

    public function generateRandomString($length) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function resend_otp_old() {
        $data = array();
        $postcount = count($_POST);

        if ($postcount > 0) {
            $data["mobile_no"] = $mobile_no = check_post($this->input->post("mobile_no"));   //now need phone number too while sending otp
            $user_id = check_post($this->input->post("user_id"));
//            $check_mb = $this->Birthday->getSelectData("mobile_no", "users", "mobile_no=$mobile_no and (is_guest=0 or is_guest=1) and mobile_verify=0")->result_array();
            $check_mb = $this->Birthday->getSelectData("mobile_no", "users", "user_id = '$user_id' and mobile_verify = '0'")->result_array();

            if (count($check_mb) > 0) {
                $mobile_no = $data["mobile_no"] = $check_mb[0]["mobile_no"];
                $data["code"] = $code = $this->generateRandomString(5);
                $otp_data = $this->Birthday->getSelectData("*", "otp", "mobile_no=$mobile_no")->result_array();
                $count = count($otp_data);
                if ($count > 0) {

                    $delivery_date_time = $otp_data[0]["sent_time"];
                    $triggerOn = date('Y-m-d H:i:s', strtotime($delivery_date_time));
                    $date1 = $triggerOn;
                    $date2 = date("Y-m-d H:i:s");
//Convert them to timestamps.
                    $date1Timestamp = strtotime($date1);
                    $date2Timestamp = strtotime($date2);
//Calculate the difference.
                    $difference = $date1Timestamp - $date2Timestamp;
                    $minutes = floor($difference / 60);

                    if ($minutes < -5) {
                        $update["code"] = $code;
                        $update["sent_time"] = date("Y-m-d H:i:s");
                        $this->Birthday->update("otp", $update, "mobile_no='$mobile_no'");
                    } else {
                        $data["code"] = $code = $otp_data[0]["code"];
                    }
                } else {
                    $data["created_date"] = date("Y-m-d H:i:s");
                    $data["sent_time"] = date("Y-m-d H:i:s");
                    $this->Birthday->addData("otp", $data);
                }
                $show_message = "Enter $code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone.";
                try {
                    $resp = $this->Birthday->send_mobile_otp($data["mobile_no"], $show_message);
                    $output["success"] = true;
                    $output["message"] = "OTP sent successfully";
                } catch (Exception $e) {
                    $output["success"] = true;
                    $output["message"] = "Failed.Please try again";
                }
            } else {
                $output["success"] = FALSE;
//                $output["message"] = "Given Mobile Number is not registered in BirthdayOwl.com";
                $output["message"] = "No account found for this Mobile Number";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input found";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function resend_otp() {
        $data = array();
        $postcount = count($_POST);

        if ($postcount > 0) {
            $data["mobile_no"] = $mobile_no = check_post($this->input->post("mobile_no"));   //now need phone number too while sending otp
            $hash_key = check_post($this->input->post("hash_key"));
            $otp_data = $this->Birthday->getSelectData("*", "otp", "mobile_no=$mobile_no")->result_array();

//            $user_id = check_post($this->input->post("user_id"));
//            $check_mb = $this->Birthday->getSelectData("mobile_no", "users", "mobile_no=$mobile_no and (is_guest=0 or is_guest=1) and mobile_verify=0")->result_array();
//            $check_mb = $this->Birthday->getSelectData("mobile_no", "users", "user_id = '$user_id' and mobile_verify = '0'")->result_array();

            if (count($otp_data) > 0) {
                $data["code"] = $code = $this->generateRandomString(5);
                $count = count($otp_data);
                $delivery_date_time = $otp_data[0]["sent_time"];
                $triggerOn = date('Y-m-d H:i:s', strtotime($delivery_date_time));
                $date1 = $triggerOn;
                $date2 = date("Y-m-d H:i:s");

                $date1Timestamp = strtotime($date1);
                $date2Timestamp = strtotime($date2);
//Calculate the difference.
                $difference = $date1Timestamp - $date2Timestamp;
                $minutes = floor($difference / 60);

                if ($minutes < -5) {
                    $update["code"] = $code;
                    $update["sent_time"] = date("Y-m-d H:i:s");
                    $this->Birthday->update("otp", $update, "mobile_no='$mobile_no'");
                } else {
                    $data["code"] = $code = $otp_data[0]["code"];
                }

                $show_message = "<#> Enter the following code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone. $code $hash_key";
//                $show_message = "Enter $code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone.";
                try {
                    $resp = $this->Birthday->send_mobile_otp($data["mobile_no"], $show_message);
                    $output["success"] = true;
                    $output["message"] = "OTP sent successfully";
                } catch (Exception $e) {
                    $output["success"] = true;
                    $output["message"] = "Failed.Please try again";
                }
            } else {
                $output["success"] = FALSE;
//                $output["message"] = "Given Mobile Number is not registered in BirthdayOwl.com";
                $output["message"] = "No account found for this Mobile Number";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input found";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function verify_otp_signup() {
        $data = array();
        $postcount = count($_POST);
        if ($postcount > 0) {
            $otp = check_post($this->input->post("otp"));
            $user_id = check_post($this->input->post("user_id"));
            $is_for_guest = check_post($this->input->post("is_guest"));

            $user_mobile_number = check_post($this->input->post("mobile_no"));
//            $update_phone_value_guest = check_post($this->input->post("update_phone_value_guest"));
//            $is_update_phone_number_guest = (check_post($this->input->post("is_update_phone_number_guest")) == 'true' ? true : false);
//            $is_update_phone_number_guest = check_post($this->input->post("is_update_phone_number_guest"));
//Get Mobile number of User whose OTP has to verify
            $where = "user_id='$user_id'";
            $get_user_details = $this->Birthday->getUserData("users", $where)->row_array();

//            $mobile_no_detail = $get_user_details['mobile_no'];
//            $mobile_no_detail = ($is_update_phone_number_guest ? $update_phone_value_guest : $get_user_details['mobile_no']);
            $mobile_no_detail = $user_mobile_number;
//Check whether OTP & mobile number is same or not                
            $otp_data = $this->Birthday->getSelectData("mobile_no,oid", "otp", "code='$otp' and mobile_no = '$mobile_no_detail'")->row_array();
            $count = count($otp_data);

            if ($count > 0) {
//                $mobile_no = $otp_data["mobile_no"];
//                $where = "mobile_no=$mobile_no and user_id='$user_id'";
//Update is_guest key if user is not guest
                if ($is_for_guest == 1) {
                    $update["mobile_verify"] = 1;
                    $update["u_last_login"] = date("Y-m-d H:i:s");
                    $update["mobile_no"] = $user_mobile_number;
//                    if ($is_update_phone_number_guest) {
//                        $update["mobile_no"] = $update_phone_value_guest;
//                    }
                } else {
                    $update["is_guest"] = 0;
                    $update["mobile_verify"] = 1;
                    $update["mobile_no"] = $user_mobile_number;
                    $update["is_google_login"] = 0;
                    $update["u_last_login"] = date("Y-m-d H:i:s");
                }

                $this->Birthday->update("users", $update, $where);
//Get User data for setting session
                $check_user = $this->Birthday->getUserData("users", $where)->result_array();
                $userdatacount = count($check_user);
                if ($userdatacount > 0) {

                    if ($is_for_guest != 1) {
                        $data["email"] = $uemail = $check_user[0]["email"];
                        $data["username"] = $firstname = $check_user[0]["first_name"];
                        $data["mobile_no"] = $check_user[0]["mobile_no"];
                        //for platform
                        $data["from_platform"] = "Device";
                        $password = $check_user[0]["password"];
                        $pass = $this->Birthday->decryptPassword($password);
                        $data["password"] = $pass;
                        $data["username"] = $check_user[0]["first_name"];
                        $view = $this->load->view("email_template/registered_success", $data, true);

                        $result = send_EmailResponse($view, $uemail, $firstname, "register@birthdayowl.com", "birthdayowl.com", "Congratulations You Have Registered Successfully with birthdayowl.com");
                        //To Support Birthday Owl
                        $view1 = $this->load->view("email_template/register_support", $data, true);
                        send_Email($view1, "support@birthdayowl.com", "Support Birthday Owl", "register@birthdayowl.com", "birthdayowl.com", "User Registered Successfully with birthdayowl.com by $uemail($firstname).");

//                            if ($result["code"] == "success") {
//                            $output["success"] = true;
//                            $output["message"] = "Registered successfully";
//                            $output["user_info"] = $check_user[0];
//                        } else {
//                            $output["success"] = false;
//                        }
                        $output["success"] = true;
                        $output["message"] = "Registered successfully";
                        $output["user_info"] = $check_user[0];
                    } else {
                        $output["success"] = true;
                        $output["message"] = "Registered successfully";
                        $output["user_info"] = $check_user[0];
                    }
                }
            } else {
                $output["success"] = false;
                $output["message"] = "Wrong OTP.Try again";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input found";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function login() {
//first_name,email,password,login_with,fb_id,g_id,gender,bmonth,bdate,byear,device_registration,platform
        $output = array();
//        $output["url"] = BASEURL . "Home_api/login";
        $postcount = count($_POST);
        if ($postcount > 0) {
            $new_data["first_name"] = $updated_data['first_name'] = $sdata['first_name'] = $first_name = check_post($this->input->post("first_name"));
            $new_data["email_id"] = $updated_data['email'] = $sdata['email'] = $email = $gsdata["email"] = trim(check_post($this->input->post("email")));
            if ($email != "") {
                $newsletter_data = $this->Birthday->getSelectData("mail_status", "news_letter", "email_id='$email'")->row_array();
            }

            $is_guest = check_post($this->input->post("is_guest"));
            $password = check_post($this->input->post("password"));
            $updated_data['login_with'] = $sdata['login_with'] = $loginwith = check_post($this->input->post("login_with"));
            $mobile_no = $gsdata['mobile_no'] = $phone = check_post($this->input->post("mobile_no"));
            $hash_key = check_post($this->input->post("hash_key"));
            $login_type = check_post($this->input->post("login_type"));
            $userid = check_post($this->input->post("user_id"));

            if ($is_guest == 1) {//Guest User Login
//Already User is Exist or not for is_guest = 0
//                $where = "(email='$email' and is_guest=0 and mobile_verify=1)";  // the user exist as a birthday owl user
//                $check_user = $this->Birthday->getSelectData("*", "users", $where)->result_array();
//
//                if (count($check_user) > 0) {
////                    $output["userdata"] = $check_user[0];
////                    $output["userdata"]['flower_data'] = array();
////                    $output["userdata"]['voucher_data'] = array();
//                    $output["success"] = true;
////                    $output["login_type"] = $login_type;
//                    $output["is_user_exist"] = 1; //User is Exist as Registered User
//                    $output['trigger_otp'] = 0;
//                    $output["user_info"] = $check_user[0];
//
//                    $user_id = $check_user[0]['user_id'];
//                    $user_details_update["u_last_login"] = date("Y-m-d H:i:s");
//                    $this->Birthday->update("users", $user_details_update, "user_id = '$user_id'");
////                    if ($this->session->userdata() != NULL) {
////                        $this->session->unset_userdata('userdata');
////                    }
////                    $this->session->set_userdata($output);
//                    $output["message"] = "Login Successfully";
                $this->load->library("CheckAsBirthdayowl");
                $check_birthdayowl_user = new CheckAsBirthdayowl();
                $check_birthdayowl_user->setEmail($email);
                $check_birthdayowl_user->setMobileNo($mobile_no);
                $is_birthdayowl_user = $check_birthdayowl_user->runner();

                if ($is_birthdayowl_user) {
                    $output["success"] = true;
                    $output["is_user_exist"] = 1; //User is Exist as Registered User
                    $output['trigger_otp'] = 0;
                    $output["user_info"] = $check_birthdayowl_user->getResult();
                    $user_id = $check_birthdayowl_user->getUserId();
//                    $user_details_update["u_last_login"] = date("Y-m-d H:i:s");
//                    $this->Birthday->update("users", $user_details_update, "user_id = '$user_id'");
//                    $user_details_update["u_last_login"] = date("Y-m-d H:i:s");
//                    $this->Birthday->update("users", $user_details_update, "user_id = '$user_id'");
                    $output["message"] = "Login Successfully";
                } else {
//Already User is Exist or not for is_guest = 1

                    $this->load->library("CheckAsGuest");
                    $check_guest_exist = new CheckAsGuest();
                    $check_guest_exist->setGuestEmail($email);
                    $check_guest_exist->setMobileNo($mobile_no);
                    $is_guest_exist = $check_guest_exist->runner();

                    if ($is_guest_exist) {
                        $getUserId = $check_guest_exist->getUserId();
                        $sameMoble = $check_guest_exist->checkMobileAreSame();
                        if ($sameMoble) {
                            $output["success"] = true;
                            $output["is_user_exist"] = 2; //User is Exist as Guest User
                            $output['trigger_otp'] = 0;
                            $output["user_info"] = $check_guest_exist->getResult();
//                            $output['update_phone_status'] = 0;
//                            $output['update_phone'] = "";
                            $user_id = $getUserId;
                            $user_details_update["u_last_login"] = date("Y-m-d H:i:s");
                            $this->Birthday->update("users", $user_details_update, "user_id = '$user_id'");
                        } else {
                            // need to update the phone number of the particular user id on verify otp
                            $this->load->library("OtpSenderUpdater");
                            $otp_sender = new OtpSenderUpdater();
                            $otp_sender->setMobileNumber($mobile_no);
                            $otp_sender->setHashKey($hash_key);
                            $otp_gone = $otp_sender->runner();
                            $output["success"] = $otp_gone ? true : false;
                            $output['trigger_otp'] = 1;
//                            $output['update_phone_status'] = 1;
//                            $output['update_phone'] = $phone;
                            $output["user_info"] = $check_guest_exist->getResult();
                            $output["is_user_exist"] = 0; //User is Exist as Guest User
//                            $output["session_guest_id"] = session_id();
                        }
                    }

//                    $where = "(email='$email' and is_guest=1 and mobile_verify=1)";
//                    $check_user = $this->Birthday->getSelectData("*", "users", $where)->result_array();
//                    if (count($check_user) > 0) {
////                        $output["userdata"] = $check_user[0];
////                        $output["userdata"]['flower_data'] = array();
////                        $output["userdata"]['voucher_data'] = array();
//                        $output["success"] = true;
//                        $output["is_user_exist"] = 2; //User is Exist as Guest User
//                        $output['trigger_otp'] = 0;
//                        $output["user_info"] = $check_user[0];
//
//                        $user_id = $check_user[0]['user_id'];
//                        $user_details_update["u_last_login"] = date("Y-m-d H:i:s");
//                        $this->Birthday->update("users", $user_details_update, "user_id = '$user_id'");
//                        $output["session_guest_id"] = session_id();
//                        $this->session->set_userdata($output);
//                    }
                    else {
                        $this->load->library("CheckAsGoogle");
                        $check_google_user = new CheckAsGoogle();
                        $check_google_user->setGoogleEmailId($email);
                        $is_google_exist = $check_google_user->runner();
                        if ($is_google_exist) {
                            $date = date("Y-m-d H:i:s");
                            $update_array['u_last_login'] = $date;
                            $update_array['is_guest'] = 1;
//                            $update_array['login_with'] = $login_with;
                            $update_array['first_name'] = $first_name;
                            $update_array['mobile_no'] = $mobile_no;
                            $user_id = $check_google_user->getUserId();
                            $this->Birthday->update("users", $update_array, "user_id = '$user_id'");
                            $result = $check_google_user->getResult();
//                                $result['u_last_login'] = $date;
//                                $result['is_guest'] = 1;
//                                $output["userdata"] = $result;
//                                $output["userdata"] = $check_user[0];
//                                $output["userdata"]['flower_data'] = array();
//                                $output["userdata"]['voucher_data'] = array();
//                                $output["success"] = true;
//                                $output["is_user_exist"] = 2; //User is Exist as Guest User
//                                $output["session_guest_id"] = session_id();

                            $this->load->library("OtpSenderUpdater");
                            $otp_sender = new OtpSenderUpdater();
                            $otp_sender->setMobileNumber($mobile_no);
                            $otp_sender->setHashKey($hash_key);
                            $otp_sender->runner();
                            $output["success"] = true;
                            $output["is_user_exist"] = 2; //User is Exist as Guest User
                            $output['trigger_otp'] = 1;
                            $output["user_info"] = $result;
                        } else {
                            $this->load->library("DeleteInvalidGuest");
                            $deleteInvalidGuest = new DeleteInvalidGuest();
                            $deleteInvalidGuest->setEmail($email);
//                            $deleteInvalidGuest->setPhoneNumber($mobile_no);
                            $deleteInvalidGuest->runner();

                            $this->load->library("DeleteInvalidBirthdayOwlUser");
                            $deleteInvalidBirthdayowlUser = new DeleteInvalidBirthdayOwlUser();
                            $deleteInvalidBirthdayowlUser->setEmail($email);
                            $deleteInvalidBirthdayowlUser->setPhoneNumber($mobile_no);
                            $deleteInvalidBirthdayowlUser->runner();

//User not exist as is_guest=0 & is_guest=1 then do registration
                            $gsdata["username"] = "";
                            $gsdata["password"] = "";
                            $gsdata['first_name'] = check_post($this->input->post("first_name"));
                            $gsdata['login_with'] = $loginwith;
                            $gsdata['g_id'] = "";
                            $gsdata["user_profile_pic"] = "";
                            $gsdata["reset_time"] = date("Y-m-d H:i:s");
                            $gsdata["reset_link"] = 0;
                            $gsdata["reset_activate"] = 0;
                            $gsdata["sent_greeting_counter"] = 0;
                            $gsdata["membership_status"] = 0;
                            $gsdata["gender"] = 1;
                            $gsdata["zodiac_id"] = 0;
                            $gsdata["bmonth"] = '';
                            $gsdata["bdate"] = '';
                            $gsdata["birth_date"] = "";
                            $gsdata["fb_id"] = "";
                            $gsdata["byear"] = "";
                            $gsdata["is_guest"] = 1;
                            $gsdata["created_date"] = date("Y-m-d H:i:s");
                            $gsdata["u_last_login"] = date("Y-m-d H:i:s");
                            $gsdata["platform"] = $this->input->post("platform");

//Send OTP to verify mobile number while purchasing Voucher or Flowers
//==========================OTP==========================
                            $data["code"] = $code = $this->generateRandomString(5);
                            $data["mobile_no"] = $mobile_no;
                            $otp_data = $this->Birthday->getSelectData("mobile_no", "otp", "mobile_no=$mobile_no")->result_array();
                            $count = count($otp_data);
                            if ($count > 0) {
                                $update["code"] = $code;
                                $update["sent_time"] = date("Y-m-d H:i:s");
                                $this->Birthday->update("otp", $update, "mobile_no='$mobile_no'");
                            } else {
                                $data["created_date"] = date("Y-m-d H:i:s");
                                $data["sent_time"] = date("Y-m-d H:i:s");
                                $this->Birthday->addData("otp", $data);
                            }
                            $show_message = "<#> Enter the following code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone. $code $hash_key";
//                            $show_message = "Enter $code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone.";
                            try {
                                $resp = $this->Birthday->send_mobile_otp($mobile_no, $show_message);
                                $output["success"] = true;
//                $output["message"] = "OTP sent successfully";
                            } catch (Exception $e) {
                                $output["success"] = false;
//                $output["message"] = "Failed.Please try again";
                            }
//==========================OTP==========================
                            $insert_id = $this->Birthday->addData("users", $gsdata);
                            $where = "user_id = '$insert_id'";
                            $user_info = $this->Birthday->getUserData("users", $where)->row_array();
//                        $output["userdata"] = $guesdata["userdata"] = $user_info;
                            $output["success"] = true;
//                        $output["session_guest_id"] = session_id();
                            $output["is_user_exist"] = 0; //User is not Exist as Registered User & Guest User
                            $output["user_info"] = $user_info;
                            $output["success"] = true;
                            $output["message"] = "OTP sent successfully";
                            $output["user_id"] = $insert_id;
                            $output['trigger_otp'] = 1;

                            //insert device registration to recececive notification
                            $reg_id = check_post($this->input->post("device_registration"));
                            $platform = check_post($this->input->post("platform"));
                            $ndr_data["user_id"] = $data["user_id"] = $user_id = $user_info["user_id"];
                            if ($platform == 1) {
                                $data["ios_device_id"] = $reg_id;
                                $data["android_device_id"] = "";
                            } else {
                                $data["ios_device_id"] = "";
                                $data["android_device_id"] = $reg_id;
                            }
                            $ndr_data["platform"] = $data["platform"] = $platform;
                            $data["registration_date"] = date("Y-m-d H:i:s");
                            $this->Birthday->addData("users_device_registration", $data);
                            $ndr_data["device_registration"] = $reg_id;

                            $check_ndr = $this->Birthday->getSelectData("ndr_id ", "notify_device_registartion ", "user_id = $user_id and device_registration = '$reg_id' and platform = $platform")->result_array();
                            if (count($check_ndr) == 0 && $reg_id != "0" && $reg_id != "") {
                                $this->Birthday->addData("notify_device_registartion", $ndr_data);
                            }

//                        $output["otp"] = $code;
                        }
                    }
                }
                echo json_encode($output);
                exit;
            }

            if ($loginwith == 1 || $loginwith == 2 || $loginwith == 3) {
                $this->validate($email, "Email address is required", $output, 1);
                $this->validate($email, "Email id is invalid", $output, 3);
            }
            $fb_id = check_post($this->input->post("fb_id"));
            $g_id = check_post($this->input->post("g_id"));
            $gender = check_post($this->input->post("gender"));
            $mb_no = check_post($this->input->post("mobile_no"));
            if ($mb_no == "") {
                $sdata["mobile_no"] = "";
            } else {
                $sdata["mobile_no"] = $mb_no;
            }
            if ($gender == "") {
                $sdata["gender"] = 1;
            } else {
                $sdata["gender"] = $gender;
            }
            $m = check_post($this->input->post("bmonth"));
            $b = check_post($this->input->post("bdate"));
            $y = check_post($this->input->post("byear"));
            if (ctype_space($m)) {
                $m = "";
            }
            if (ctype_space($b)) {
                $b = "";
            }
            if (ctype_space($y)) {
                $y = "";
            }
            if ($is_guest != "")
                $sdata['is_guest'] = $is_guest;
            else {
                $sdata['is_guest'] = 0;
            }
            $sdata["username"] = "";
            $sdata["password"] = "";
            $sdata["user_profile_pic"] = "";
            $sdata["reset_time"] = date("Y-m-d H:i:s");
            $sdata["reset_link"] = 0;
            $sdata["reset_activate"] = 0;
            $sdata["sent_greeting_counter"] = 0;
            $sdata["membership_status"] = 0;
            $sdata["mobile_no"] = check_post($this->input->post("mobile_no"));
            $sdata["created_date"] = date("Y-m-d H:i:s");
            $month = date("m");
            $reg_id = check_post($this->input->post("device_registration"));
            $platform = check_post($this->input->post("platform"));
            $pass = $this->Birthday->encryptPassword($password);
            $this->validate($loginwith, "login_with is required", $output, 1);
            $this->validate($loginwith, "login_with value should be 1, 2, 3 or 4", $output, 2);
//            $select = "users.user_id, users.first_name, users.password, users.email, users.mobile_no, users.birth_date, users.gender, users.bdate, users.bmonth, users.byear, users.fb_id, users.g_id, users.login_with, users.user_profile_pic, users.sent_greeting_counter, users.membership_status, users.is_guest";
            switch ($loginwith) {
                case "1": {
//                        if ($is_guest == 1) {
//                            $sdata['g_id'] = "";
//                            $sdata["zodiac_id"] = 0;
//                            $sdata["bmonth"] = '';
//                            $sdata["bdate"] = '';
//                            $sdata["birth_date"] = "";
//                            $sdata["fb_id"] = "";
//                            $insert_id = $this->Birthday->addData("users", $sdata);
//                            $where = "user_id = '$insert_id'";
//                            $user_info = $this->Birthday->getSelectData("*", "users", $where)->row_array();
//
//                            $output["user_info"] = $user_info;
//                        } else {
//                            $this->validate($password, "Password is required", $output, 1);
//                            $check_email = $this->Birthday->getSelectData("*", "users", "email = '$email'")->row_array();
//                            if (count($check_email) == "0") {
//                                $output["success"] = FALSE;
//                                $output["message"] = "Account is not registered using above email-id ";
//                                echo json_encode($output);
//                                exit;
//                            }
//                            $where = "email = '$email' and password = '$pass' and is_guest = 0 and mobile_verify = '1'";
//                            $user_info = $this->Birthday->getSelectData("*", "users", $where)->row_array();
//
//                            if (count($user_info) == "0") {
//                                $output["success"] = FALSE;
//                                $output["message"] = "Invalid email-id or password";
//                                echo json_encode($output);
//                                exit;
//                            } else {
//                                $user_details_update["u_last_login"] = date("Y-m-d H:i:s");
//                                $this->Birthday->update("users", $user_details_update, "email = '$email'");
//                            }
//                            $user_info["mail_status"] = $newsletter_data["mail_status"];
//                            $output["user_info"] = $user_info;
//                        }

                        $this->validate($password, "Password is required", $output, 1);
                        $check_email = $this->Birthday->getSelectData("* ", "users ", "email = '$email'")->row_array();
                        if (count($check_email) == "0") {
                            $output["success"] = FALSE;
                            $output["message"] = "Account is not registered using above email-id ";
                            echo json_encode($output);
                            exit;
                        }
                        $where = "email = '$email' and password = '$pass' and is_guest = 0 and mobile_verify = '1'";
                        $user_info = $this->Birthday->getSelectData("* ", "users", $where)->row_array();

                        if (count($user_info) == "0") {
                            $output["success"] = FALSE;
                            $output["message"] = "Invalid email-id or password";
                            echo json_encode($output);
                            exit;
                        } else {
                            $user_id = $user_info['user_id'];
                            $user_details_update["u_last_login"] = date("Y-m-d H:i:s");
                            $this->Birthday->update("users", $user_details_update, "user_id = '$user_id'");

//                            $user_details_update["u_last_login"] = date("Y-m-d H:i:s");
//                            $this->Birthday->update("users", $user_details_update, "email = '$email'");
                        }
                        $user_info["mail_status"] = $newsletter_data["mail_status"];
                        $output["user_info"] = $user_info;
                    }
                    break;
                case "2":  //2-facebook
                case "3": { //3-gmail
                        $this->validate($first_name, "First Name is required", $output, 1);

                        if ($loginwith == 2) {
                            $this->validate($fb_id, "Facebook id is required", $output, 1);
                            $sdata['g_id'] = "";
                            $sdata['fb_id'] = $updated_data['fb_id'] = $fb_id;
                        } else {
                            $this->validate($g_id, "Google id is required", $output, 1);
                            $sdata['fb_id'] = "";
                            $sdata['g_id'] = $updated_data['g_id'] = $g_id;
                        }
                        if (($m != '') && ($b != '')) {

                            $zodiac_info = $this->Birthday->getUserData("mapzodiac ", "(fdate <= '$b' and fmonth = '$m') or (tdate >= '$b' and tmonth = '$m')")->row_array();

                            $sdata["zodiac_id"] = $zodiac_info["zodiac_id"];
                            $sdata["bmonth"] = $m;
                            $sdata["bdate"] = $b;
                            $sdata["byear"] = $y;
                            $sdata["birth_date"] = "$m/$b/$y";
                        } else {
                            $sdata["zodiac_id"] = 0;
                            $sdata["bmonth"] = '';
                            $sdata["bdate"] = '';
                            $sdata["byear"] = "";
                            $sdata["birth_date"] = "";
                        }

                        switch (TRUE) {
                            case TRUE:
                                $this->load->library("CheckAsBirthdayowl");
                                $check_as_birthday_owl = new CheckAsBirthdayowl();
                                $check_as_birthday_owl->setEmail($email);
                                $isBirthdayOwlUser = $check_as_birthday_owl->runner();
                                //
                                if ($isBirthdayOwlUser) {
                                    $output["success"] = true;
                                    $output["is_user_exist"] = 1; //User is Exist as Registered User
                                    $output['trigger_otp'] = 0;
                                    $output["user_info"] = $user_info = $check_as_birthday_owl->getResult();

                                    $user_id = $check_as_birthday_owl->getUserId();
                                    $user_details_update["u_last_login"] = date("Y-m-d H:i:s");
                                    $this->Birthday->update("users", $user_details_update, "user_id = '$user_id'");
                                    $output["message"] = "Login Successfully";
                                    break;
                                }
                            case TRUE:
                                $this->load->library("CheckAsGuest");
                                $check_guest_exist = new CheckAsGuest();
                                $check_guest_exist->setGuestEmail($email);
                                $is_guest_exist = $check_guest_exist->runner();

                                if ($is_guest_exist) {
                                    $update['is_google_login'] = 1;
                                    $update['u_last_login'] = date("Y-m-d H:i:s");
                                    $user_id = $check_guest_exist->getUserId();
                                    $this->Birthday->update("users", $update, "user_id = '$user_id'");
                                    $output["success"] = true;
                                    $output["is_user_exist"] = 3; //User is Exist guest user but since it is google authenticated we put is_user_exist = 3
                                    $output['trigger_otp'] = 0;
                                    $output["user_info"] = $user_info = $check_guest_exist->getUserInfo();
                                    $output["user_id"] = $user_id;
                                    break;
                                }
                            case TRUE:
                                $this->load->library("CheckAsGoogle");
                                $check_google_user = new CheckAsGoogle();
                                $check_google_user->setGoogleEmailId($email);
                                $is_google_exist = $check_google_user->runner();
                                if ($is_google_exist) {
                                    $update['is_google_login'] = 1;
                                    $update['u_last_login'] = date("Y-m-d H:i:s");
                                    $user_id = $check_guest_exist->getUserId();
                                    $this->Birthday->update("users", $update, "user_id = '$user_id'");
                                    $output["success"] = true;
                                    $output["is_user_exist"] = 3; //it is google authenticated we put is_user_exist = 3
                                    $output['trigger_otp'] = 0;
                                    $output["user_info"] = $user_info = $check_google_user->getUserInfo();
                                    $output["user_id"] = $check_google_user->getUserId();
                                    break;
                                }

                            case TRUE:
                                // now delete if there is any email whose mobile verify = 0 form this email just to avoid multiple entries.
                                $this->load->library("DeleteInvalidGuest");
                                $deleteInvalidGuest = new DeleteInvalidGuest();
                                $deleteInvalidGuest->setEmail($email);
//                                $deleteInvalidGuest->setPhoneNumber('');            //// since google logiin doen't have phone number
                                $deleteInvalidGuest->runner();

                                $this->load->library("DeleteInvalidBirthdayOwlUser");
                                $deleteInvalidBirthdayowlUser = new DeleteInvalidBirthdayOwlUser();
                                $deleteInvalidBirthdayowlUser->setEmail($email);
//                                $deleteInvalidBirthdayowlUser->setPhoneNumber('');  // since google logiin doen't have phone number
                                $deleteInvalidBirthdayowlUser->runner();

                                // Insert new google login
                                $sdata['is_google_login'] = '1';
                                $sdata['u_last_login'] = date("Y-m-d H:i:s");
                                $insert_id = $this->Birthday->addData("users", $sdata);
                                $new_data["nimage_name"] = "";
                                $this->Birthday->addData("news_letter", $new_data);
                                $where = "user_id = '$insert_id'";
                                $user_info = $this->Birthday->getSelectData("* ", "users", $where)->row_array();
                                $user_info["mail_status"] = $newsletter_data["mail_status"];
                                $output["user_info"] = $user_info;
                                $output["success"] = true;
                                $output["is_user_exist"] = 3; //it is google authenticated we put is_user_exist = 3
                                $output['trigger_otp'] = 0;
                                break;
                        }

//                        $run = TRUE;   // just to make the loop run and break when the particular codition encounters
//                        while ($run) {
//                            $this->load->library("CheckAsBirthdayowl");
//                            $check_as_birthday_owl = new CheckAsBirthdayowl();
//                            $check_as_birthday_owl->setEmail($email);
//                            $isBirthdayOwlUser = $check_as_birthday_owl->runner();
//
//                            if ($isBirthdayOwlUser) {
//                                $output["success"] = true;
//                                $output["is_user_exist"] = 1; //User is Exist as Registered User
//                                $output['trigger_otp'] = 0;
//                                $output["user_info"] = $check_as_birthday_owl->getResult();
//
//                                $user_id = $check_as_birthday_owl->getUserId();
//                                $user_details_update["u_last_login"] = date("Y-m-d H:i:s");
//                                $this->Birthday->update("users", $user_details_update, "user_id = '$user_id'");
//                                $output["message"] = "Login Successfully";
//                                break;
//                            }
//
//                            // now check if the email exist as a guest previously and update his google login key in the records of table user
//                            $this->load->library("CheckAsGuest");
//                            $check_guest_exist = new CheckAsGuest();
//                            $check_guest_exist->setGuestEmail($email);
//                            $is_guest_exist = $check_guest_exist->runner();
//
//                            if ($is_guest_exist) {
//                                break;
//                            }
//
//                            //now check if the email exist as already as a google user with the same email
//                            $this->load->library("CheckAsGoogle");
//                            $check_google_user = new CheckAsGoogle();
//                            $check_google_user->setGoogleEmailId($email);
//                            $is_google_exist = $check_google_user->runner();
//
//                            if ($is_google_exist) {
//                                break;
//                            }
//                            $run = FALSE;
//                        }                        
//                        $where = "email = '$email'";
//                        $query = $this->Birthday->getSelectData("email", "users", $where)->result_array();
//                        $sdata["u_last_login"] = $updated_data["u_last_login"] = date("Y-m-d H:i:s");
//
//                        if (count($query) > 0) {
//                            $updated = $this->Birthday->update("users", $updated_data, $where);
//                            if ($updated == 1) {
//                                $user_info = $this->Birthday->getSelectData("*", "users", $where)->row_array();
//                            }
//                        } else {
//                            $insert_id = $this->Birthday->addData("users", $sdata);
//                            $new_data["nimage_name"] = "";
//                            $this->Birthday->addData("news_letter", $new_data);
//                            $where = "user_id = '$insert_id'";
//                            $user_info = $this->Birthday->getSelectData("*", "users", $where)->row_array();
//                        }
//
//                        $user_info["mail_status"] = $newsletter_data["mail_status"];
//                        $output["user_info"] = $user_info;
                    }
                    break;

                case "4": { //login after deep linking
                        $user_id = check_post($this->input->post("user_id"));
                        $this->validate($user_id, "user_id is required", $output, 1);
                        $where = "user_id = '$user_id'";
                        $user_info = $this->Birthday->getSelectData($select, "users", $where)->row_array();
                        if (count($user_info) == 0) {
                            $output["success"] = FALSE;
                            $output["message"] = "user_id does not exist";
                            echo json_encode($output);
                            exit;
                        }
                        $email = $user_info["email"];
                        $newsletter_data = $this->Birthday->getSelectData("mail_status ", "news_letter ", "email_id = '$email'")->row_array();
                        $user_info["mail_status"] = $newsletter_data["mail_status"];
                        $output["user_info"] = $user_info;
                    }
                    break;
            }

            $ndr_data["user_id"] = $data["user_id"] = $user_id = $user_info["user_id"];
//            $plans = $this->Birthday->getSelectData("amount, duration", "membership_plans", "status = 1")->result_array();
//            $membership_status = $user_info["membership_status"];
//            if ($membership_status == 1) {
//                $selected = "greeting_membership.membership_id, greeting_membership.plan_purchase_type, greeting_membership.expiry_date";
//                $where = "greeting_membership.user_id = $user_id and greeting_membership.transaction_status = 'success'";
//                $planinfo = $this->Birthday->getSelectData($selected, "greeting_membership", $where)->result_array();
//                $output["plan_info"] = $planinfo;
//                $output["plans"] = $plans;
//            } else {
//                $output["plans"] = $plans;
//                $output["plan_info"] = array();
//            }
            $output["plans"] = array();
            $output["plan_info"] = array();

            if ($platform == 1) {
                $data["ios_device_id"] = $reg_id;
                $data["android_device_id"] = "";
            } else {
                $data["ios_device_id"] = "";
                $data["android_device_id"] = $reg_id;
            }
            $ndr_data["platform"] = $data["platform"] = $platform;
            $data["registration_date"] = date("Y-m-d H:i:s");
            $this->Birthday->addData("users_device_registration", $data);
            $ndr_data["device_registration"] = $reg_id;

            $check_ndr = $this->Birthday->getSelectData("ndr_id ", "notify_device_registartion ", "user_id = $user_id and device_registration = '$reg_id' and platform = $platform")->result_array();
            if (count($check_ndr) == 0 && $reg_id != "0" && $reg_id != "") {
                $this->Birthday->addData("notify_device_registartion", $ndr_data);
            }
            $output["success"] = TRUE;
            $output["message"] = "Login Successfully!";
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found.";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output
        ));
    }

    public function thanx_for_purchase() {

        $postcount = count($_POST);
        if ($postcount > 15) {
//post values from payumoney 
//            $_POST["mode"];
//            $_POST["status"];
//            $_POST["firstname"];
//            $_POST["amount"];
//            $_POST["txnid"];
//            $_POST["email"];
//            $_POST["addedon"];
//            $_POST["payuMoneyId"];
//            $_POST["bankcode"];
//            $data["processing_fee"] = $udf1 = $_POST["udf1"];
//            $key = $_POST["key"];
//            $_POST["hash"];
//            $productinfo = $_POST["productinfo"];
//            $payment_for_flower_total_amount_flower = $_POST["amount"];
            //missing post value
//            $user_id available but as the root udf data
//            
//            $payment_for_voucher_mode = $payment_for_flower_mode = $_POST["mode"];
            $payment_for_voucher_mode = $payment_for_flower_mode = check_post($this->input->post("mode"));
//            $payment_for_voucher_status = $payment_for_flower_status = $_POST["status"];
            $payment_for_voucher_status = $payment_for_flower_status = check_post($this->input->post("status"));
//            $payment_for_voucher_fname = $payment_for_flower_fname = $_POST["firstname"];
            $payment_for_voucher_fname = $payment_for_flower_fname = check_post($this->input->post("firstname"));
//            $payment_for_voucher_total_amount_voucher = $payment_for_flower_total_amount_flower = $_POST["amount"];
            $payment_for_voucher_total_amount_voucher = $payment_for_flower_total_amount_flower = check_post($this->input->post("amount"));
//            $payment_for_voucher_txnid = $payment_for_flower_txnid = $_POST["txnid"];
            $payment_for_voucher_txnid = $payment_for_flower_txnid = check_post($this->input->post("txnid"));
//            $payment_for_voucher_email = $payment_for_flower_email = $_POST["email"];
            $payment_for_voucher_email = $payment_for_flower_email = check_post($this->input->post("email"));
//            $payment_for_voucher_purchased_date = $payment_for_flower_purchased_date = $_POST["addedon"];
            $payment_for_voucher_purchased_date = $payment_for_flower_purchased_date = check_post($this->input->post("addedon"));
//            $payment_for_voucher_payuid = $payment_for_flower_payuid = $_POST["payuMoneyId"];
            $payment_for_voucher_payuid = $payment_for_flower_payuid = check_post($this->input->post("payuMoneyId"));


            //now add the related a single record for the orders table
            $orders["status"] = "1";    // status 1 payment done....
//            $orders["user_id"] = $data["user_id"] = $user_id = $udf2 = $_POST["udf2"];                                 //need to look about payment
            $orders["user_id"] = $data["user_id"] = $user_id = $udf2 = $_POST["user_id"];                                 //need to look about payment
            $orders["order_created_date"] = date("Y-m-d H:i:s");
            $order_id = $this->Birthday->addData("orders", $orders);

//post values from payumoney 
//post values from user input
            $flower_id = check_post($this->input->post("flower_id"));
            $flower_code = check_post($this->input->post("flower_code"));
            $flower_image = check_post($this->input->post("flower_image"));
            $flower_name = check_post($this->input->post("flower_name"));
            $flower_price = check_post($this->input->post("flower_price"));
            $quantity = check_post($this->input->post("quantity"));
            $user_id = check_post($this->input->post("user_id"));
            $sender_name = check_post($this->input->post("sender_name"));
            $receiver_name = check_post($this->input->post("receiver_name"));
            $delivery_status = check_post($this->input->post("delivery_status"));
            $delivery_address_line1 = check_post($this->input->post("delivery_address_line1"));
            $delivery_address_line2 = check_post($this->input->post("delivery_address_line2"));
            $delivery_city = check_post($this->input->post("delivery_city"));
            $delivery_pincode = check_post($this->input->post("delivery_pincode"));
            $delivery_shipping_method = check_post($this->input->post("delivery_shipping_method"));
            $delivery_time_slot = check_post($this->input->post("delivery_time_slot"));
            $message_on_card = check_post($this->input->post("message_on_card"));
            $delivery_date = check_post($this->input->post("delivery_date"));
            $sender_phone = check_post($this->input->post("sender_phone"));
            $receiver_phone = check_post($this->input->post("receiver_phone"));
            $sender_email = check_post($this->input->post("sender_email"));
            $receiver_email = check_post($this->input->post("receiver_email"));
            $is_guest = check_post($this->input->post("is_guest"));
            $productinfo = $_POST["productinfo"];


//            $add_to_payment_flower_table["total_amount"] = $payment_for_flower_total_amount_flower;
//            $add_to_payment_flower_table["user_id"] = $user_id;
//            $add_to_payment_flower_table["flower_id"] = $flower_id;
//            $add_to_payment_flower_table['flower_code'] = $flower_code;    // i will get the flower code.
//            $add_to_payment_flower_table["order_id"] = $order_id;
//            $add_to_payment_flower_table["transaction_id"] = $payment_for_flower_txnid;
//            $add_to_payment_flower_table["transaction_status"] = $payment_for_flower_status;
//            $add_to_payment_flower_table["bill_to_fname"] = $payment_for_flower_fname;
//            $add_to_payment_flower_table["bill_to_email"] = $payment_for_flower_email;
//            $add_to_payment_flower_table["payment_mode"] = $payment_for_flower_mode;
//            $add_to_payment_flower_table["payuId"] = $payment_for_flower_payuid;
//            $add_to_payment_flower_table["purchased_date"] = $payment_for_flower_purchased_date;
//            $this->db->insert('flower_payment', $add_to_payment_flower_table);
//            $inserted_payment_id = $this->db->insert_id();

            $what_was_transaction = $this->what_was_transaction_for($productinfo);

//post values from user input
//            $what_was_transaction_encrypt = $this->Birthday->encryptPassword($what_was_transaction);
            // now check what was the transaction for 
            if ($what_was_transaction == 2 || $what_was_transaction == 3) {

                if ($payment_for_flower_status == 'success') {

                    $add_to_payment_flower_table = array();
                    $add_to_order_to_fnp_table = array();

                    $add_to_payment_flower_table["total_amount"] = $payment_for_flower_total_amount_flower;
                    $add_to_payment_flower_table["user_id"] = $user_id;
                    $add_to_payment_flower_table["flower_id"] = $flower_id;
                    $add_to_payment_flower_table['flower_code'] = $flower_code;    // i will get the flower code.
                    $add_to_payment_flower_table["order_id"] = $order_id;
                    $add_to_payment_flower_table["transaction_id"] = $payment_for_flower_txnid;
                    $add_to_payment_flower_table["transaction_status"] = $payment_for_flower_status;
                    $add_to_payment_flower_table["bill_to_fname"] = $payment_for_flower_fname;
                    $add_to_payment_flower_table["bill_to_email"] = $payment_for_flower_email;
                    $add_to_payment_flower_table["payment_mode"] = $payment_for_flower_mode;
                    $add_to_payment_flower_table["payuId"] = $payment_for_flower_payuid;
                    $add_to_payment_flower_table["purchased_date"] = $payment_for_flower_purchased_date;
                    $this->db->insert('flower_payment', $add_to_payment_flower_table);
                    $inserted_payment_id = $this->db->insert_id();

                    // now insert into order_to_fnp tables
                    $add_to_order_to_fnp_table["flower_purchased_id"] = $flower_id;
                    $add_to_order_to_fnp_table["flower_purchased_code"] = $flower_code;
                    $add_to_order_to_fnp_table["flower_purchased_image"] = $flower_image;
                    $add_to_order_to_fnp_table["flower_purchased_name"] = $flower_name;
                    $add_to_order_to_fnp_table["flower_purchased_price"] = $flower_price;
                    $add_to_order_to_fnp_table["flower_purchased_quantity"] = $quantity;
                    $add_to_order_to_fnp_table["flower_sender_id"] = $user_id;
                    $add_to_order_to_fnp_table["flower_guest_session"] = "";
                    $add_to_order_to_fnp_table["flower_sender_name"] = $sender_name;
                    $add_to_order_to_fnp_table["flower_receiver_name"] = $receiver_name;
                    $add_to_order_to_fnp_table["flower_is_delivered_status"] = 1;
                    $add_to_order_to_fnp_table["flower_receiver_address1"] = $delivery_address_line1;
                    $add_to_order_to_fnp_table["flower_receiver_address2"] = $delivery_address_line2;
                    $add_to_order_to_fnp_table["flower_delivery_city"] = $delivery_city;
                    $add_to_order_to_fnp_table["flower_delivery_pincode"] = $delivery_pincode;
                    $add_to_order_to_fnp_table["flower_delivery_shipping_method"] = $delivery_shipping_method;
                    $add_to_order_to_fnp_table["flower_delivery_time_slot"] = $delivery_time_slot;
                    $add_to_order_to_fnp_table["message_on_card"] = $message_on_card;
                    $add_to_order_to_fnp_table["flower_delivery_date"] = $delivery_date;
                    $add_to_order_to_fnp_table["flower_sender_phone"] = $sender_phone;
                    $add_to_order_to_fnp_table["flower_receiver_phone"] = $receiver_phone;
                    $add_to_order_to_fnp_table["order_id_from_orders"] = $order_id;
                    $add_to_order_to_fnp_table["flower_sender_email"] = $sender_email;
                    $add_to_order_to_fnp_table["flower_receiver_email"] = $receiver_email;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    $add_to_order_to_fnp_table["flower_transaction_status"] = $payment_for_flower_status;
                    $add_to_order_to_fnp_table["flower_payu_id"] = $payment_for_flower_payuid;
                    $add_to_order_to_fnp_table["flower_purchased_is_guest"] = $is_guest;
                    $add_to_order_to_fnp_table["flower_purchased_guest_session"] = "";
                    $add_to_order_to_fnp_table["flower_purchased_date_time"] = $payment_for_flower_purchased_date;
                    $add_to_order_to_fnp_table["payment_id_from_flower_payment"] = $inserted_payment_id;  // this is added for inserted payment_id
                    $this->db->insert('order_to_fnp', $add_to_order_to_fnp_table);
                    $inserted_flower_id = $this->db->insert_id();
//                        $payment_data = $this->Birthday->getorderPaymentFlowerData($inserted_flower_id)->row_array();
                    $data_update["flower_unique_oid"] = "BHOWLFL00$inserted_flower_id";
                    $where = "flower_order_id = $inserted_flower_id";
                    $this->Birthday->update("order_to_fnp", $data_update, $where);

                    // now for sending mail
                    $payment_data = $this->Birthday->getorderPaymentFlowerData($inserted_flower_id)->row_array();

                    $userinfo = $this->Birthday->getSelectData("email, first_name, mobile_no ", "users ", "user_id = $user_id")->row_array();
                    $data["sender_email"] = $sender_email = $userinfo["email"];
                    $data["sender_name"] = $sender_name = trim($userinfo["first_name"]);
                    $data["sender_phone"] = $sender_mobile_no = $userinfo["mobile_no"];
                    $flower_unique_oid = $data["order_id"] = $payment_data["flower_unique_oid"];
                    $data["bill_to_name"] = $payment_data["bill_to_fname"];
//                    $data["samount"] = $data["total_amount"] = $amount;
                    $data["payment_mode"] = $payment_data["payment_mode"];
                    $data["purchased_date"] = $payment_data["purchased_date"];
                    $data["flower_delivery_date"] = $payment_data["flower_delivery_date"];
                    $data["flower_delivery_time_slot"] = $payment_data["flower_delivery_time_slot"];
                    $data["message_on_card"] = $payment_data['message_on_card'];

                    $data["flower_code"] = $payment_data["flower_purchased_code"];
                    $data["flower_image"] = $payment_data["flower_purchased_image"];
                    $data["flower_name"] = $payment_data["flower_purchased_name"];
                    $data["flower_price"] = $payment_data["flower_purchased_price"];
                    $data["flower_quantity"] = $payment_data["flower_purchased_quantity"];

                    $data["receiver_name"] = $receiver_name = $payment_data["flower_receiver_name"];
                    $data["receiver_email"] = $receiver_email = $payment_data["flower_receiver_email"];
                    $data["receiver_phone"] = $receiver_phone = $payment_data["flower_receiver_phone"];
                    $data["flower_receiver_address1"] = $payment_data["flower_receiver_address1"];
                    $data["flower_receiver_address2"] = $payment_data["flower_receiver_address2"];
                    $data["flower_delivery_city"] = $payment_data["flower_delivery_city"];
                    $data["flower_delivery_pincode"] = $payment_data["flower_delivery_pincode"];

                    $data["processing_fee1"] = round(($data["flower_price"] * $payment_data["flower_purchased_quantity"] * 3.5) / 100, 2);

                    $data["samount"] = $data["total_amount"] = ($payment_data["flower_purchased_price"] * $payment_data["flower_purchased_quantity"]) + $data["processing_fee1"];


                    // send email to the fnp and the client
                    //FNP's & Other's Email IDs
//                    $fnp_emails[] = array('order4@fnp.com', 'sonal.j@fnp.com');
//                    $fnp_names[] = array('Orders FNP', 'Sonal FNP');
//                    $fnp_cc_emails[] = array('Bibek.banerjee@fnp.com', 'support@birthdayowl.com', 'abdul@birthdayowl.com', 'Devesh.m@fnp.com');
//                    $fnp_cc_names[] = array('Bibek FNP', 'Support Birthday Owl', 'Abdul Birthday Owl', 'Devesh');
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                    For testing Purpose
                    $fnp_emails[] = array('hnadar@techathalon.com');
                    $fnp_names[] = array('Hareesh');
                    $fnp_cc_emails[] = array('vpatekar@techathalon.com');
                    $fnp_cc_names[] = array('Vina');

                    try {
//TO FNP
                        $view_flower = $this->load->view("email_template/Order_placed_flower", $data, true);
                        send_EmailResponse_with_cc($view_flower, $fnp_emails, $fnp_names, $fnp_cc_emails, $fnp_cc_names, "orders@birthdayowl.com", "birthdayowl.com", "$sender_name($sender_email), Your order $flower_unique_oid is confirmed.");
//To Customer Order Placed SMS & EMAIL
                        send_EmailResponse($view_flower, $sender_email, $sender_name, "orders@birthdayowl.com", "birthdayowl.com", "$sender_name($sender_email), Your order $flower_unique_oid is confirmed.");

                        if ($sender_mobile_no != '') {
                            $show_message = "Thank You $sender_name!Your order #$flower_unique_oid is confirmed. We will keep you posted on the further status of your order. \n- Birthday Owl (www.birthdayowl.com)";
                            $this->Birthday->send_sms($sender_mobile_no, $show_message);
                        }

//To Customer Order Received Receipt - Bill
                        $view = $this->load->view("email_template/Payment_received_flower", $data, true);
                        $response = send_EmailResponse($view, $sender_email, $sender_name, "orders@birthdayowl.com", "birthdayowl.com", "Payment Received (Rs.$payment_for_flower_total_amount_flower) from BirthdayOwl");

//                        if (SERVER_TYPE == "1") {
                        $admin_email = "abdul@birthdayowl.com";
//                        } else {
//                            $admin_email = "vpatekar@techathalon.com";
//                            $admin_email = "vpatekar@techathalon.com";
//                        }
                        $viewn = $this->load->view("email_template/Order_placed_1", $data, true);
                        send_Email($viewn, $admin_email, "abdul", "orders@birthdayowl.com", "birthdayowl.com", "Order has been placed by $sender_name($sender_email)");
                        $where = "flower_order_id = $inserted_flower_id";


//                        for temporary
//                        $response["code"] = "success";
                        if ($response["code"] == "success") {
                            $up ["flower_is_mail_sent"] = "1";
                            $this->Birthday->update("order_to_fnp", $up, $where);
                        } else {
                            $up["flower_is_mail_sent"] = "0";
                            $this->Birthday->update("order_to_fnp", $up, $where);
                        }
                    } catch (Exception $e) {
                        $where = "flower_order_id = $inserted_flower_id";
                        $up["flower_is_mail_sent"] = "0";
                        $this->Birthday->update("order_to_fnp", $up, $where);
                    }
                } else {
                    //just add to the payment table with the value of failure
                    $add_to_payment_flower_table["total_amount"] = $payment_for_flower_total_amount_flower;
                    $add_to_payment_flower_table["user_id"] = $user_id;
                    $add_to_payment_flower_table["flower_id"] = $flower_id;
                    $add_to_payment_flower_table['flower_code'] = $flower_code;    // i will get the flower code.
                    $add_to_payment_flower_table["order_id"] = $order_id;
                    $add_to_payment_flower_table["transaction_id"] = $payment_for_flower_txnid;
                    $add_to_payment_flower_table["transaction_status"] = $payment_for_flower_status;
                    $add_to_payment_flower_table["bill_to_fname"] = $payment_for_flower_fname;
                    $add_to_payment_flower_table["bill_to_email"] = $payment_for_flower_email;
                    $add_to_payment_flower_table["payment_mode"] = $payment_for_flower_mode;
                    $add_to_payment_flower_table["payuId"] = $payment_for_flower_payuid;
                    $add_to_payment_flower_table["purchased_date"] = $payment_for_flower_purchased_date;
                    $this->db->insert('flower_payment', $add_to_payment_flower_table);
                    $inserted_payment_id = $this->db->insert_id();
                }

                $output['message'] = "entries added to the table";
                $output['product_info'] = $productinfo;
                $output['success'] = $payment_for_flower_status;
            }
        } else {
//            redirect(BASEURL . "payment-failed/" . 0);
            $output['success'] = false;
            $output['message'] = "no input found";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function what_was_transaction_for($product_info) {

        $what_was_purchased_status = 0;

        switch ($product_info) {

            case "BirthdayOwl egift vouchers":
//only voucher purchased
                $what_was_purchased_status = 1;
                break;
            case "BirthdayOwl flower":
//only flower purchased
                $what_was_purchased_status = 2;
                break;
            case "BirthdayOwl voucher and flower":
//both flower and voucher purchased
                $what_was_purchased_status = 3;
                break;
        }
        return $what_was_purchased_status;
    }

    public function orderHistoryFlowerCakes() {
        $postcount = count($_POST);
        $output = array();
        $output["url"] = BASEURL . "Home_api/orderHistoryFlowerCakes";
        if ($postcount > 0) {
            $empty = array();
            $user_id = check_post($this->input->post("user_id"));
            $this->validate($user_id, "user_id is required", $output, 1);
            $check_user_id = $this->Birthday->getSelectData("user_id", "users", "user_id='$user_id'")->num_rows();
            if ($check_user_id == 0) {
                $output["success"] = FALSE;
                $output["message"] = "user_id does not exist";
                echo json_encode($output);
                exit;
            }
            $where_flower = "flower_sender_id=$user_id order by flower_order_id desc";
            $result = $this->Birthday->getUserData("order_to_fnp", $where_flower)->result_array();

            if (!empty($result)) {
                $output["success"] = TRUE;
                $output["flower_history"] = $result;
                $output["count"] = count($output["flower_history"]);
            } else {
                $output["success"] = FALSE;
                $output["flower_history"] = array();
                $output["count"] = 0;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function walletDetailsFlowerCakes() {
        $postcount = count($_POST);
        $output = array();
        $output["url"] = BASEURL . "Home_api/walletDetailsFlowerCakes";
        if ($postcount > 0) {
            $empty = array();
            $user_id = check_post($this->input->post("user_id"));
            $user_email = check_post($this->input->post("email"));
            $phone = check_post($this->input->post("mobile_no"));

            $this->validate($user_id, "user_id is required", $output, 1);

            $check_user_id = $this->Birthday->getSelectData("user_id", "users", "user_id='$user_id'")->num_rows();
            if ($check_user_id == 0) {
                $output["success"] = FALSE;
                $output["message"] = "user_id does not exist";
                echo json_encode($output);
                exit;
            }
            if ($phone = '') {
                $where = "flower_receiver_email='$user_email' order by flower_order_id desc";
            } else {
                $where = "flower_receiver_email='$user_email' or flower_receiver_phone = '$phone' order by flower_order_id desc";
            }
            $result = $this->Birthday->getUserData("order_to_fnp", $where)->result_array();

            if (!empty($result)) {
                $output["success"] = TRUE;
                $output["flower_wallet"] = $result;
                $output["count"] = count($output["flower_wallet"]);
            } else {
                $output["success"] = FALSE;
                $output["flower_wallet"] = array();
                $output["count"] = 0;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function terms_n_conditions_egift() {
        $terms = $this->Birthday->getAllData("terms_egift")->row_array();
        $data["terms"] = $terms["terms"];
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=25")->row_array();
        $data["seo"] = $title;
        $this->load->view('HOME/terms_n_conditions_egift_api', $data);
    }

    public function terms_n_conditions_flowers_cakes() {
        $terms = $this->Birthday->getAllData("terms_other")->row_array();
        $data["terms"] = $terms["terms"];
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=24")->row_array();
        $data["seo"] = $title;
        $this->load->view('HOME/terms_n_conditions_flower_cake_api', $data);
    }

    public function getTestimonials() {
        $voucher_name = $this->Birthday->getAllData("testimonial")->result_array();
        return $voucher_name;
    }

    function getVoucherNames() {
        $product_voucher = getAllVouchers();
        $voucher_name = array_column($product_voucher, 'voucher_pro_name');
        return $voucher_name;
    }

    public function privacy_policy() {
        $data["heading"] = "PRIVACY & SECURITY POLICY";
        $privacy = $this->Birthday->getAllData("privacy")->row_array();
        $data["privacy"] = $privacy["privacy_data"];
        $data["voucherNames"] = $this->getVoucherNames();
        $data["testimonials"] = $test = $this->getTestimonials();
        $title = $this->Birthday->getSelectData("title,description,keywords", "seo", "id=8")->row_array();
        $data["seo"] = $title;
        $this->load->view('HOME/privacy_policy_api', $data);
    }

    public function countryfinder() {
        $country = get_country_info();
        $output["country_id"] = trim($country["country_id"]);
        $output["country_name"] = trim($country["country_name"]);
        $output["nationality_code"] = trim($country["nationality_code"]);

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function sendNotification() {
        $message = "happy birthday";
        $type = 1;
        $user_id = 553;
        $this->Birthday->sendNotification($user_id, $message, $type);
    }

// helper function to send notification just change the user id for which you want notification
    public function sendNotificationAndMail() {
        require_once(APPPATH . "libraries/Mailin.php" );
        //  date_default_timezone_set('Asia/Kolkata');
        $current_month = date("0000-m-d");
        $bday_data = $this->db->query("select user_id,bdate,bmonth,first_name,last_name,reminder_id,birth_date,DATEDIFF(birth_date,'$current_month') as days_diff from bday_reminder where (DATEDIFF(birth_date,'$current_month')=priority) or (DATEDIFF(birth_date,'$current_month')= 1) or (birth_date='$current_month') ")->result_array();

        if (count($bday_data) > 0) {
            foreach ($bday_data as $data) {
                $user_id = $data["user_id"];
                $bdate = $data["bdate"];
                $bmonth = $data["bmonth"];
                $firstname = $data["first_name"];
                $lastname = $data["last_name"];
                $year = date("Y");
                $bdata["name"] = $name = '"' . $firstname . " " . $lastname . '"';
                $reminder_id = $data["reminder_id"];
                $birth_date = $data["birth_date"];
                $current_year = date("Y");
                $new_date = date("dS F $current_year", strtotime($birth_date));
                $bdata["bdate"] = $new_date;
                //send sms
                $cmonth = date("$bmonth");
                $mname = date("Y-$cmonth-d");
                $name = strtotime($mname);
                $mn = date("F", $name);
                $email_info = $this->Birthday->getSelectData("email,first_name,mobile_no,sent_greeting_counter,membership_status", "users", "user_id='$user_id'")->row_array();
                $username = $email_info["first_name"];
                $email = $email_info["email"];
                $mobile_no = $email_info["mobile_no"];
                $status = $email_info["membership_status"];
                if ($data["days_diff"] == 0) {
                    $bdata["message"] = "$firstname's birthday is Today";
                    $bdata["suffix"] = "i.e";
                    $show_message = "Hello $username, \n Birthday Owl reminds you that today is $firstname's birthday. \nLog in www.birthdayowl.com now to send your wishes to $firstname and view other upcoming birthdays \n-Birthday Owl";
                } else if ($data["days_diff"] == 1) {
                    $bdata["message"] = "$firstname's birthday will be Tomorrow";
                    $bdata["suffix"] = "i.e";
                    $show_message = "Hello $username, \n Birthday Owl reminds you that tomorrow will be $firstname's birthday. \nLog in www.birthdayowl.com now to send your wishes to $firstname and view other upcoming birthdays \n-Birthday Owl";
                } else {
                    $current_month = date("$bmonth");
                    $month_name = date("Y-$current_month-d");
                    $name = strtotime($month_name);
                    $m = date("F", $name);
                    $bdata["message"] = "$firstname's birthday is coming up ";
                    $bdata["suffix"] = "on";
                    $show_message = "Hello $username, \n Birthday Owl reminds you of upcoming birthday of $firstname on $bdate, $mn \nLog in www.birthdayowl.com now to send your wishes to $firstname and view other upcoming birthdays \n-Birthday Owl";
                }

                if ($status == 1 && $user_id == '532') {
                    $this->Birthday->send_sms($mobile_no, $show_message);
                }

                //send notification
                if ($user_id == '532') {
                    $this->Birthday->sendNotification($user_id, $bdata["message"], "1");
//
                    //send email
                    $mailin = new Mailin('https://api.sendinblue.com/v2.0', SENDINKEY);
//                $view = $this->load->view("email_template/reminder_success", $bdata, true);
                    $view = $this->load->view("email_template/Birthday_reminder_new", $bdata, true);
                    $sdata = array("to" => array("$email" => "$username"),
                        "from" => array("birthdayreminder@birthdayowl.com", "birthdayowl.com"),
                        "subject" => "Birthday Reminder From www.birthdayowl.com",
                        "text" => "$view",
                        "html" => "$view"
                    );
                    $resp = $mailin->send_email($sdata);
                    if ($resp["code"] == "success") {
                        $updated_data["email_status"] = 1;
                        $where = "reminder_id='$reminder_id'";
                        $this->Birthday->update("bday_reminder", $updated_data, $where);
                    } else {
                        $updated_data["email_status"] = 0;
                        $where = "reminder_id='$reminder_id'";
                        $this->Birthday->update("bday_reminder", $updated_data, $where);
                    }
                }
            }
        }
    }

    //for hash key validation
    public function calculateHashAndValidate() {
        $key = $_POST["key"];
        if (SERVER_TYPE == '3') {
            $salt = PSALT;
        } else {
            $salt = PSALT;
        }
        $txnId = $_POST["txnid"];
        $amount = $_POST["amount"];
        $productName = $_POST["productInfo"];
        $firstName = $_POST["firstName"];
        $email = $_POST["email"];
        $udf1 = $_POST["udf1"];
        $udf2 = $_POST["udf2"];
        $udf3 = $_POST["udf3"];
        $udf4 = $_POST["udf4"];
        $udf5 = $_POST["udf5"];

        $beforeTracnactionHash = trim($_POST["hash"]);
        $payment_mode = $_POST["pay_mode"];
        $status = $_POST["status"];
        $payId = $_POST["pay_id"];


        $payhash_str = $key . '|' . $txnId . '|' . $amount . '|' . $productName . '|' . $firstName . '|' . $email . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '|' . $salt;
        $hash = strtolower(hash('sha512', $payhash_str));

        if ($beforeTracnactionHash == $hash) {
            $arr['success'] = TRUE;
            $arr["hash"] = $hash;
            $arr["amount"] = $amount;
            $arr["pay_id"] = $payId;
            $arr["status"] = $status;
            $arr["txnid"] = $txnId;
            $arr["pay_mode"] = $payment_mode;
        } else {
            $arr['success'] = FALSE;
            $arr["hash"] = $hash;
            $arr["amount"] = $amount;
            $arr["pay_id"] = $payId;
            $arr["status"] = $status;
            $arr["txnid"] = $txnId;
            $arr["pay_mode"] = $payment_mode;
        }
        $output = $arr;
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

//    For searching birthday reminders
    public function searchBirthdayReminders() {
        $user_id = $this->input->post("user_id");

        $bday_reminder = $this->Birthday->simple_query("SELECT reminder_id,first_name,last_name,bdate,bmonth,byear,zodiac_id,mobile_no,email,gender,birth_date,rem_image_name  FROM `bday_reminder` WHERE `user_id` = $user_id ORDER BY `reminder_id` ASC")->result_array();

        foreach ($bday_reminder as $key => $value) {
            $bday_reminder[$key]['birth_date'] = $bday_reminder[$key]["bdate"] . "-" . $bday_reminder[$key]["bmonth"] . "-" . $bday_reminder[$key]["byear"];
        }

        if (empty($bday_reminder)) {
            $output["success"] = false;
            $output["message"] = "Birthday reminders not found.";
            $output["bday_reminders"] = $bday_reminder;
        } else {
            $output["success"] = true;
            $output["message"] = "Birthday reminders found.";
            $output["bday_reminders"] = $bday_reminder;
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

}
