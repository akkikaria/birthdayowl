<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">

        <!-- START PAGE SIDEBAR -->
        <div class="page-sidebar">
            <!-- START X-NAVIGATION -->
            <ul class="x-navigation">
                <li>
                    <div style="height:100px;background: white;"> <a href="<?php echo BASEURL; ?>Admin_panel/admin_dashboard"><img src="<?php echo BASEURL_OIMG; ?>logo.png"  style=" width: 82%; margin-left: 15px; margin-top: 5px;"></a></div>
                </li>
                <li class="xn-title"></li>


                <li class="xn-openable">
                    <a href="<?php echo BASEURL; ?>Admin_panel/admin_dashboard"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Birthday Reminder</span></a>
                    <ul>
                        <li> <a href="<?php echo BASEURL; ?>Admin_panel/users"><span class="glyphicon glyphicon-plus-sign"></span>Users</a></li>
                    </ul>
                    <ul>
                        <li>  <a href="<?php echo BASEURL; ?>Admin_panel/company"><span class="glyphicon glyphicon-plus-sign"></span>Company Register</a></li>
                    </ul>
                    <ul>
                        <li>  <a href="<?php echo BASEURL; ?>Admin_panel/newsletter"><span class="glyphicon glyphicon-plus-sign"></span>Newsletter</a></li>
                    </ul>
                    <ul>
                        <li>  <a href="<?php echo BASEURL; ?>Admin_panel/friends"><span class="glyphicon glyphicon-plus-sign"></span>Refer Friend List</a></li>
                    </ul>
                    <ul>
                        <li>  <a href="<?php echo BASEURL; ?>Admin_panel/marquee"><span class="glyphicon glyphicon-plus-sign"></span>Marquee</a></li>
                    </ul>
                    <ul>
                        <li>   <a href="<?php echo BASEURL; ?>Admin_panel/add_sign_data"><span class="glyphicon glyphicon-plus-sign"></span> Add Sign Data</a></li>
                    </ul>
                    <ul>
                        <li> <a href="<?php echo BASEURL; ?>Admin_panel/relationship"><span class="glyphicon glyphicon-plus-sign"></span> Add Relationship</a></li>
                    </ul>
                    <ul>
                        <li>   <a href="<?php echo BASEURL; ?>Admin_panel/occupation"><span class="glyphicon glyphicon-plus-sign"></span> Add Occupation</a></li>
                    </ul>

                </li>

                <!--                <li class="xn-openable ">
                                    <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Menu</span></a>
                                    <ul>
                                        <li><a href="<?php echo BASEURL; ?>Admin_panel/banner"><span class="glyphicon glyphicon-plus-sign"></span> Add Banner</a></li>
                                    </ul>
                                    <ul>
                                        <li class="active"><a href="<?php echo BASEURL; ?>Admin_panel/menu"><span class="glyphicon glyphicon-plus-sign"></span> Add Main Menu</a></li>
                                    </ul>
                                    <ul>
                                        <li class="xn-openable">
                                            <a href="#"><span class="glyphicon glyphicon-folder-close"></span>Categories</a>
                                            <ul>
                                                <li><a href="<?php echo BASEURL; ?>Admin_panel/category"><span class="glyphicon glyphicon-plus-sign"></span> Add Category</a></li>
                                            </ul>
                                        </li>
                                        <li class="xn-openable">
                                            <a href="#"><span class="glyphicon glyphicon-folder-close"></span> Sub-Categories</a>
                                            <ul>
                                                <li><a href="<?php echo BASEURL; ?>Admin_panel/sub_category"><span class="glyphicon glyphicon-plus-sign"></span> Add  Sub-Category</a></li>
                                            </ul>
                                        </li>
                                        <li class="xn-openable">
                                            <a href="#"><span class="glyphicon glyphicon-folder-close"></span>Products</a>
                                            <ul>
                                                <li><a href="<?php echo BASEURL; ?>Admin_panel/product"><span class="glyphicon glyphicon-plus-sign"></span> Add Product</a></li>
                                                <li><a href="<?php echo BASEURL; ?>Admin_panel/view_product"><span class="fa fa-list"></span> View Products</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>-->
                <!--START  GREETING CARDS-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">E-Greetings</span></a>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Greeting_card/greeting_category"><span class="glyphicon glyphicon-plus-sign"></span> Add Greeting Category </a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Greeting_card/greeting"><span class="glyphicon glyphicon-plus-sign"></span>Upload Greeting Card Image</a></li>
                    </ul>
                </li>
                <!--END  GREETING CARDS-->
                <!--START  Product Voucher-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Product Voucher</span></a>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Product_voucher/voucher_category"><span class="glyphicon glyphicon-plus-sign"></span> Add Voucher Category </a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Product_voucher/voucher_product"><span class="glyphicon glyphicon-plus-sign"></span>Add Product Voucher</a></li>
                    </ul>
                    <ul>
                        <li ><a href="<?php echo BASEURL; ?>Product_voucher/amount_voucher"><span class="glyphicon glyphicon-plus-sign"></span>Add amount</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Product_voucher/display_products"><span class="glyphicon glyphicon-plus-sign"></span>Display Product Vouchers</a></li>
                    </ul>
                    <ul>
                        <li ><a href="<?php echo BASEURL; ?>Product_voucher/stock"><span class="glyphicon glyphicon-plus-sign"></span>Check Vouchers stock</a></li>
                    </ul>
                </li>
                <!--END  Product Voucher-->
                <!-- Brands-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span>Brands</a>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/brands"><span class="glyphicon glyphicon-plus-sign"></span> Add Brand</a></li>
                    </ul>
                </li>
                <!-- End-Brands-->
                <!-- Banners-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span>Banners</a>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/banner"><span class="glyphicon glyphicon-plus-sign"></span>Add Banners</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/festivals"><span class="glyphicon glyphicon-plus-sign"></span> Add Festival Banners</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/activate_banners"><span class="glyphicon glyphicon-plus-sign"></span>Activate Banners</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/change_homepage"><span class="glyphicon glyphicon-plus-sign"></span>Change Home Screen Banners</a></li>
                    </ul>
                </li>
                <!-- End Banners-->
                <!-- sales-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span>Sales</a>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/orders"><span class="glyphicon glyphicon-plus-sign"></span>Orders</a></li>
                    </ul>

                </li>
                <!-- End sales-->
                <!-- Reports-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span>Reports</a>
                    <ul>
                        <li>

                            <a href=""><span class="fa fa-th-list"></span>Products</a>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/best_sell"><span class="fa fa-angle-right"></span>Best Seller</a></li>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/most_view"><span class="fa fa-angle-right"></span>Most Viewed</a></li>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/ordered_product"><span class="fa fa-angle-right"></span>Products Ordered</a></li>
                            </ul>


                        </li>
                    </ul>

                </li>
                <!-- End reports-->

                <!-- orders-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span>Orders</a>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/invoice"><span class="glyphicon glyphicon-plus-sign"></span>Invoice</a></li>
                    </ul>

                </li>
                <!-- End orders-->
            </ul>
            <!-- END X-NAVIGATION -->
        </div>
        <!-- END PAGE SIDEBAR -->

        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     

            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span class="fa fa-arrow-circle-o-left"></span><?php echo $heading; ?></h2>
            </div>
            <!-- END PAGE TITLE -->                

            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-body">    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Product Voucher </label>
                                        <div class="col-md-3 col-xs-12">    
                                            <select id="voucher_pro_id" name="voucher_pro_id" class="form-control">
                                                <option selected="selected" value="0" >Select Vouchers</option>
                                                <?php foreach ($vouchers as $vc) { ?>
                                                    <option value="<?php echo $vc["voucher_pro_id"]; ?>"><?php echo $vc["voucher_pro_name"]; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="error_r1" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Brand Name</label>
                                        <div class="col-md-3 col-xs-12">                                        
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="brand_name" id="brand_name" value="<?php echo $brands["brand_name"] ?>"/>
                                            </div> 
                                        </div>
                                        <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="col-md-5">
                                        <button class="btn btn-primary pull-right" id="<?php echo $btn_click; ?>" brand_id="<?php echo $brands["brand_id"] ?>">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="delete_menu()" style="margin-left: 1100px;"><span class="fa fa-times"> DELETE</span></button>
                                <h3 class="panel-title">Manage Menu</h3>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    

                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>SELECT BRAND</th>
                                                <th>BRAND ID</th>
                                                <th>BRAND NAME</th>
                                                <th>PRODUCT VOUCHER</th>
                                                <th>Edit</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>

                                            <?php foreach ($voucher_product as $brand) { ?>
                                                <tr>
                                                    <td><input type="checkbox" class="checkbox" value="<?php echo $brand["brand_id"]; ?>" /></td>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $brand["brand_name"]; ?></td>
                                                    <td><?php echo $brand["voucher_pro_name"]; ?></td>
                                                    <td> <button class="btn btn-default btn-rounded btn-condensed btn-sm edit_brand" brand_id="<?php echo $brand["brand_id"]; ?>"><span class="fa fa-pencil"></span></button></td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>

                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>

            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->
    <script>
        var voucher_pro_id = "<?php echo $brands["voucher_pro_id"]; ?>";
        $("#voucher_pro_id").val(voucher_pro_id);
        $("#add_brand").click(function () {
            var brand_name = $("#brand_name").val();
            var voucher_pro_id = $("#voucher_pro_id").val();
            if (voucher_pro_id == '0') {
                $(".error_r1").html("Select Product Voucher ");
            }
            $('#voucher_pro_id').change(function () {
                $(".error_r1").hide();
            });
            if (brand_name == '') {
                $(".error_r2").html("Brand Name is required");
            }
            $('#brand_name').change(function () {
                $(".error_r2").hide();
            });
            if ((voucher_pro_id != '0') && (brand_name != '')) {
                var data = {
                    brand_name: brand_name,
                    voucher_pro_id: voucher_pro_id
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/addUpdatebrand",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            alert(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/brands";
                        }
                    }
                });
            }
        });
        $(".edit_brand").click(function () {
            var brand_id = $(this).attr("brand_id");
            window.location = "<?php echo BASEURL; ?>Admin_panel/brands/" + brand_id;
        });
        $("#edit_brand").click(function () {
            var brand_name = $("#brand_name").val();
            var brand_id = $(this).attr("brand_id");
            var voucher_pro_id = $("#voucher_pro_id").val();
            if (voucher_pro_id == '0') {
                $(".error_r1").html("Select Product Voucher ");
            }
            $('#voucher_pro_id').change(function () {
                $(".error_r1").hide();
            });
            if (brand_name == '') {
                $(".error_r2").html("Brand Name is required");
            }
            $('#brand_name').change(function () {
                $(".error_r2").hide();
            });
            if ((voucher_pro_id != '0') && (brand_name != '')) {
                var data = {
                    brand_name: brand_name,
                    voucher_pro_id: voucher_pro_id,
                    brand_id: brand_id
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/addUpdatebrand",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            alert(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/brands";
                        }
                    }
                });
            }

        });
        function delete_menu() {
            var checkboxArray = new Array();
            var checkedValues = $('.checkbox:checked').map(function () {
                checkboxArray.push(this.value);
            });
            if (checkboxArray == '') {
                alert("Select at least one brand");
            } else {
                var ask = window.confirm("Are you sure you want to delete?");
                if (ask) {
                    var data = {
                        id: checkboxArray,
                        from: 3
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo BASEURL; ?>Admin_panel/CommonDelete",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.success.toString() === "false") {
                                $("#message").html(r.message.toString());
                                return false;
                            } else {
                                alert(r.message.toString());
                                window.location = "<?php echo BASEURL; ?>Admin_panel/brands";
                            }
                        }
                    });

                } else {
                    window.location = "<?php echo BASEURL; ?>Admin_panel/brands";
                }
            }
        }
    </script>