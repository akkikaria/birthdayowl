<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Home extends CI_Controller {

    public function index() {
        $this->load->view("Home");
    }

    function createIndividualAccount() {
        $output = array();
        $function_name = 'createIndividualAccount';
        if (count($_POST) > 0) {
            //$im_details["m_id"] = 15;
            $im_details["m_email"] = $this->input->post("m_email");
            $im_details["m_username"] = $this->input->post("m_username");
            $im_details["m_password"] = $this->input->post("m_password");
            $im_details["m_country"] = $this->input->post("m_country");
            $im_details["m_city"] = $this->input->post("m_city");
            $im_details["m_phone"] = $this->input->post("m_phone");
            $im_details["ConPassword"] = $this->input->post("ConPassword");
            $im_details["ConEmail"] = $this->input->post("ConEmail");
            $where = "m_email='" . $im_details["m_email"] . "'";
            $retval = $this->birthday_common->getUserData("dbo.IndividualRegister", $where)->row();
            if (count($retval) > 0) {
                $output["success"] = false;
                $output["error_no"] = 3;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["message"] = "User Email  already exists.";
                echo json_encode($output);
                exit;
            }
            $where = "m_username='" . $im_details["m_username"] . "'";
            $retval = $this->birthday_common->getUserData("dbo.IndividualRegister", $where)->row();
            if (count($retval) > 0) {
                $output["success"] = false;
                $output["error_no"] = 2;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["message"] = "User Name Not Available.";
                echo json_encode($output);
                exit;
            }
            $im_details["Occupation"] = $this->input->post("Occupation");
            $im_details["Love"] = $this->input->post("Love");
            $im_details["RealName"] = $this->input->post("RealName");
            $im_details["CountryOpt"] = $this->input->post("CountryOpt");
            $im_details["CreationDate"] = date("Y-m-d H:i:s");
            $im_details["Sex"] = $this->input->post("Sex");
            $im_details["delstatus"] = 0;
            $im_details["bday"] = $this->input->post("bday");
            $im_details["bmonth"] = $this->input->post("bmonth");
            $im_details["byear"] = $this->input->post("byear");
            $im_details["ispaid"] = 0;
            $m_id = $this->birthday_common->addData("dbo.IndividualRegister", $im_details);
            $output = $this->birthday_common->getIndividualAccountDetails($m_id);
            $data["email"] = $output["m_details"][0]["m_email"];
            $data["username"] = $output["m_details"][0]["m_username"];
            $data["password"] = $output["m_details"][0]["m_password"];
            $data["realName"] = $output["m_details"][0]["RealName"];
            $this->load->library('email');
            $this->email->set_newline("\r\n");
            $this->email->from('swati@birthday-matters.com', 'Birthday-matters');
            $this->email->to($im_details["m_email"]);
            $this->email->subject('Birthday-matters : Registration Successful!');
            $this->email->message($this->load->view("email_template/registered_success", $data, true));
            $this->email->send();
            $this->email->clear();
            $output["success"] = TRUE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["message"] = "Registered successfully";
        } else {
            $output["success"] = FALSE;
            $output["error_no"] = 1;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["message"] = "No input data found.";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function login() {
        $output = array();
        $function_name = 'login';
        if (count($_POST) > 0) {
            $m_username = $this->input->post("m_username");
            $m_password = $this->input->post("m_password");
            $month = $this->input->post("Rmonth");
            $reg_id = $this->input->post("registration_id");
            $where = "m_username='$m_username'and m_password='$m_password'";
            $user_info = $this->birthday_common->getUserData("dbo.IndividualRegister", $where)->result_array();
            if (count($user_info) == 0) {
                $output["success"] = false;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["message"] = "Invalid username or password!";
            } else {
                $output["user_info"] = $user_info;
                $where = "UserName='$m_username' and Rmonth='$month'";
                $bday_data = $this->birthday_common->getUserData("TblRelation", $where)->result_array();
                if (count($bday_data) == 0) {
                    $output["bday_info"] = 'birthday data not available for this month ';
                } else {
                    $output["bday_info"] = $bday_data;
                }
                $data["m_id"] = $user_info[0]["m_id"];
                $data["registration_id"] = $reg_id;
                $data["registration_date"] = date("Y-m-d H:i:s");
                $check_reg_id = $this->birthday_common->getUserData1("dbo.users_device_registration", "registration_id Like '$reg_id'");
                if ($check_reg_id == FALSE) {
                    $this->birthday_common->addData("users_device_registration", $data);
                }
                //    $this->birthday_common->sendPushMessages($m_id, $message, "1");
                $output["success"] = TRUE;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["message"] = "Login Successfully!";
            }
        } else {
            $output["success"] = FALSE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["message"] = "No input data found.";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function forgotPassword() {
        $output = array();
        $function_name = 'forgotPassword';
        $data = array();
        if (count($_POST) > 0) {
            $m_email = $this->input->post("m_email");
            $where = "m_email='$m_email'";
            $user_details = $this->birthday_common->getUserData("IndividualRegister", $where)->result_array();
            if (count($user_details) == 0) {
                $output["success"] = false;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["message"] = "No account found for this email";
            } else {
                $this->load->library('email');
                $this->email->set_newline("\r\n");
                $this->email->from('swati@birthday-matters.com', 'Birthday-matters');
                $this->email->to($m_email);
                $this->email->subject('Birthday-matters : Forgot Password');
                $data["Password"] = $user_details[0]["m_password"];
                $data["username"] = $user_details[0]["m_username"];
                $data["realName"] = $user_details[0]["RealName"];
                $this->email->message($this->load->view("email_template/forgotPassword", $data, true));
                $this->email->send();
                //   echo $this->email->print_debugger();
                $this->email->clear();
                $output["success"] = TRUE;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["message"] = "Your  password has been sent to your email: $m_email";
            }
        } else {
            $output["success"] = FALSE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["message"] = "No Input Found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function updateProfile() {
        $output = array();
        $function_name = 'updateProfile';
        /*  if (!empty($_FILES)) {

          $config['upload_path'] = '../../../LoginCustomer/';
          //$config['allowed_types'] = 'gif|png|jpg|jpeg';
          $config["allowed_types"] = "*";
          $config['max_size'] = '100000';
          $config['max_width'] = '1024';
          $config['max_height'] = '768';
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if (!$this->upload->do_upload()) {
          $error = array('error' => $this->upload->display_errors());
          $error['success'] = FALSE;
          echo json_encode($error);
          exit();
          } else {
          $data = $this->upload->data();
          $file = array(
          'image' => $data['fileName'],

          );
          $this->Birthday_common->add_image($file);
          $data['success'] = TRUE;

          echo json_encode($data);
          exit();
          }
          } */
        $filename = "";
        $upload = FALSE;
        if (!empty($_FILES)) {
            $tempFile = trim($_FILES['userfile']['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
            $filename = str_replace(" ", "_", $filename);
            $targetPath = $_SERVER["DOCUMENT_ROOT"] . "/LoginCustomer/";
            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = false;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES["userfile"]["error"];
                    $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                    echo json_encode($output);
                    exit;
                } else {
                    // $this->common->create_thumb("store_icon", $filename);
                    //$this->birthdacommon->create_medium_image("LoginCustomer", $filename);
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                    $upload = TRUE;
                    echo json_encode($output);
                    exit;
                }
            } else {
                // $this->common->create_thumb("store_icon", $filename);
                // $this->common->create_medium_image("LoginCustomer", $filename);
                $output["filename"] = $filename;
                $output["success"] = true;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $upload = TRUE;
                echo json_encode($output);
                exit;
            }
        }

        if (count($_POST) > 0) {
            $filename = "";
            $m_id = $this->input->post("m_id");
            $realName = $this->input->post("RealName");
            $m_username = $this->input->post("m_username");
            $m_password = $this->input->post("m_password");
            //$conPassword = $this->input->post("conPassword");
            $m_country = $this->input->post("m_country");
            $m_city = $this->input->post("m_city");
            $bday = $this->input->post("bday");
            $bmonth = $this->input->post("bmonth");
            $byear = $this->input->post("byear");
            $Occupation = $this->input->post("Occupation");
            $hobby = $this->input->post("Love");
            $m_email = $this->input->post("m_email");
            $conEmail = $this->input->post("conEmail");
            $m_phone = $this->input->post("m_phone");

            //Check username
            $where = "m_username='$m_username'and m_id!=$m_id";
            $check_username = $this->birthday_common->getUserData("IndividualRegister", $where)->result_array();
            if (count($check_username) == 1) {
                $output["success"] = false;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["message"] = "Username  already exists.";
                echo json_encode($output);
                exit;
            }
            //Check email
            $where = "m_email='$m_email' and m_id!=$m_id";
            $check_email = $this->birthday_common->getUserData("IndividualRegister", $where)->result_array();
            if (count($check_email) == 1) {
                $output["success"] = false;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["message"] = "Email-id already exists.";
                echo json_encode($output);
                exit;
            }
            $user_info["RealName"] = $realName;
            $user_info["m_username"] = $m_username;
            $user_info["m_password"] = $m_password;
            // $user_info["conPassword"] = $conPassword;
            $user_info["m_country"] = $m_country;
            $user_info["m_city"] = $m_city;
            $user_info["bday"] = $bday;
            $user_info["bmonth"] = $bmonth;
            $user_info["byear"] = $byear;
            $user_info["Occupation"] = $Occupation;
            $user_info["Love"] = $hobby;
            $user_info["m_email"] = $m_email;
            $user_info["conEmail"] = $conEmail;
            $user_info["m_phone"] = $m_phone;
            if ($upload == TRUE) {
                $user_info["FileName"] = $filename;
            } else {
                $user_info["FileName"] = $this->input->post("FileName");
            }
            $updated_data = $this->birthday_common->update("IndividualRegister", $user_info, "m_id = $m_id");
            if ($updated_data == 1) {
                $updated_result = $this->birthday_common->getUserData("IndividualRegister", "m_id = $m_id")->result_array();
                $output["updated_data"] = $updated_result;
                $output["success"] = true;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["message"] = "Profile is updated successfully!";
            }
        } else {
            $output["success"] = FALSE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["message"] = "No Input Found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function AddBirthdayReminder() {
        $function_name = 'AddBirthdayReminder';
        if (count($_POST) > 0) {
            $bday_details["fname"] = $this->input->post("fname");
            $bday_details["lname"] = $this->input->post("lname");
            $bday_details["Rmonth"] = $this->input->post("Rmonth");
            $bday_details["Rdate"] = $this->input->post("Rdate");
            $bday_details["RRelation"] = $this->input->post("RRelation");
            $bday_details["Rgender"] = $this->input->post("Rgender");
            $bday_details["MonthText"] = $this->input->post("MonthText");
            $bday_details["LoginId"] = $this->input->post("LoginId");
            $bday_details["UserName"] = $this->input->post("UserName");
            $bday_details["ZodaicSign"] = $this->input->post("ZodaicSign");
            $this->birthday_common->addData("TblRelation", $bday_details);
            $output["success"] = true;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["message"] = "Reminder Added successfully!";
        } else {
            $output["success"] = FALSE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["message"] = "No Input Found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function GetProfileInfo() {
        $function_name = 'GetProfileInfo';
        $output = array();
        if (count($_POST) > 0) {
            $UserName = $this->input->post("UserName");
            $m_id = $this->input->post("m_id");
            $month = $this->input->post("Rmonth");
            $where = "m_username='$UserName'and m_id=$m_id";
            $profile_data = $this->birthday_common->getUserData("IndividualRegister", $where);
            $where = "UserName='$UserName' and loginId=$m_id and Rmonth=$month";
            $bday_data = $this->birthday_common->getUserData("TblRelation", $where);
            // $profile_info=   $this->Birthday_common->getUserProfileInfo($UserName,$m_id)->result_array();
            if ((count($profile_data) > 0 ) || (count($bday_data) > 0)) {
                $output["profile_info"] = $profile_data->result_array();
                if (count($bday_data) == 0) {
                    $output["bday_info"] = 'birthday data not available for this month ';
                } else {
                    $output["bday_info"] = $bday_data->result_array();
                }
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["success"] = true;
                $output["message"] = "";
            }
        } else {
            $output["success"] = FALSE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["message"] = "No Input Found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function viewBdayReminder() {
        $function_name = 'viewBdayReminder';
        $output = array();
        if (count($_POST) > 0) {
            $UserName = $this->input->post("m_username");
            $m_id = $this->input->post("m_id");
            $month = $this->input->post("Rmonth");
            $date = $this->input->post("Rdate");
            $where = "m_username ='$UserName' and m_id=$m_id";
            $checkUserNameValidity = $this->birthday_common->getUserData("IndividualRegister", $where)->result_array();
            if (count($checkUserNameValidity) == 1) {
                $where = "UserName='$UserName' and loginId=$m_id and Rmonth='$month' and Rdate='$date'";
                $bday_data = $this->birthday_common->getUserData("TblRelation", $where)->result_array();
                if ((count($bday_data) > 0)) {
                    $output["bday_info"] = $bday_data;
                    $output["success"] = true;
                    $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                    $output["message"] = "";
                    echo json_encode($output);
                    exit;
                } else {
                    $output["success"] = FALSE;
                    $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                    $output["message"] = "There are no birthdays in this month.";
                }
            } else {
                $output["success"] = FALSE;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["message"] = "Invalid username or m_id";
                echo json_encode($output);
                exit;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function getUpcomingBirthdays() {
        $function_name = 'getUpcomingBirthdays';
        $output = array();
        if (count($_POST) > 0) {
            $UserName = $this->input->post("UserName");
            $LoginId = $this->input->post("LoginId");
            //  $Rmonth = $this->input->post("Rmonth");
            $where = "UserName='$UserName' and LoginId=$LoginId and Rmonth >=1 and Rmonth<=12";
            $getUpcomingBdays = $this->birthday_common->getUserData("TblRelation", $where)->result_array();
            if (count($getUpcomingBdays) > 0) {
                $output["success"] = TRUE;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["upcoming_bdays"] = $getUpcomingBdays;
            } else {
                $output["success"] = FALSE;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["message"] = "No upcoming birthdays.";
            }
        } else {
            $output["success"] = FALSE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function getCountryAndOccupation() {
        $function_name = 'getCountryAndOccupation';
        $output = array();
        $country_info = $this->birthday_common->getAllData("dbo.TblCountry");
        $occupation = $this->birthday_common->getAllData("dbo.TblOccupation");
        if (($country_info->num_rows() > 0) || ($occupation->num_rows() > 0)) {
            $output["success"] = TRUE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["country_info"] = $country_info->result_array();
            $output["occupation"] = $occupation->result_array();
        } else {
            $output["success"] = FALSE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["message"] = "No data found";
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function getZodiacSigns() {
        $function_name = 'getZodiacSigns';
        $output = array();
        $zodiacSigns = $this->birthday_common->getAllData("dbo.TblMapZodiac");
        if ($zodiacSigns->num_rows() > 0) {
            $output["success"] = TRUE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["zodic_signs"] = $zodiacSigns->result_array();
        } else {
            $output["success"] = FALSE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function getZodiacSignsDetails() {
        $function_name = 'getZodiacSignsDetails';
        $output = array();
        if (count($_POST) > 0) {
            $im_select["SignDay"] = $this->input->post("SignDay");
            $im_select["SignMonth"] = $this->input->post("SignMonth");
            $im_select["SignId"] = $this->input->post("SignId");
            $zodiac_name = $this->birthday_common->getZodiac_name($im_select["SignDay"], $im_select["SignMonth"]);
            if ($zodiac_name->num_rows() > 0) {
                $output["success"] = TRUE;
                $output["zodiac_name"] = $zodiac_name->result_array();
            }
            $where = "SignDay='" . $im_select["SignDay"] . "' and SignMonth='" . $im_select["SignMonth"] . "'";
            $zodiacSigns = $this->birthday_common->getUserData("dbo.TblSigns", $where);
            if ($zodiacSigns->num_rows() > 0) {
                $output["success"] = TRUE;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["zodic_sign_details"] = $zodiacSigns->result_array();
            } else {
                $output["success"] = FALSE;
                $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
                $output["message"] = "No data found";
            }
        } else {
            $output["success"] = FALSE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["message"] = "No input data found.";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function getBroadcastDetails() {
        $function_name = 'getBroadcastDetails';
        $output = array();
        $broadcasts = $this->birthday_common->getAllData("dbo.TblMarque");
        if ($broadcasts->num_rows() > 0) {
            $output["success"] = TRUE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["broadcasts"] = $broadcasts->result_array();
        } else {
            $output["success"] = FALSE;
            $output["url"] = "http://birthday-matters.com/birthday/index.php/home/$function_name";
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

}
