<?php

require(APPPATH . 'libraries/Rest_Controller.php');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Sync_Users extends Rest_Controller {

    function user_get() {
        if (!$this->get('id')) {
            $this->response(array('status' => 'failed', 'message' => 'invalid id'));
        }

        $user = $this->Sync_model->getUser($this->get('id'));

        if ($user) {
            $user = $user->result_array();
            $output = array();
            $output["success"] = true;
            $output["user"] = $user;
            $output["time"] = date_default_timezone_get();
            $this->response($output, 200); // 200 being the HTTP response code
        } else {
            $this->response(array('status' => 'failed', 'message' => 'Invalid db operation'));
        }
    }

    function user_post() {
        if (!$this->post('id')) {
            $this->response(array('status' => 'failed', 'message' => 'invalid id'));
        }

        $update = array(
            'name' => $this->post('name'),
            'phone_no' => $this->post('phone_no')
        );
        $user_id = $this->post('id');
        $result = $this->Sync_model->update("users", $update, "user_id = $user_id");

        if ($result === FALSE) {
            $this->response(array('status' => 'failed', 'message' => 'Invalid db operation'));
        } else {
            $user = $this->Sync_model->getUser($user_id);
            $user = $user->result_array();
            $output = array();
            $output["success"] = true;
            $output["user"] = $user;
            $output["message"] = "Updated successfully";
            $this->response($output, 200);
        }
    }

    function user_put() {

        $update = array(
            'name' => $this->put('name'),
            'phone_no' => $this->put('phone_no'),
            'status' => '1'
        );
        $user_id = $this->Sync_model->addData("users", $update);

        if ($user_id === FALSE) {
            $this->response(array('status' => 'failed'));
        } else {
            $user = $this->Sync_model->getUser($user_id);
            $user = $user->result_array();
            $output = array();
            $output["success"] = true;
            $output["user"] = $user;
            $output["message"] = "Added successfully";
            $this->response($output, 200);
        }
    }

    function user_delete() {
        if (!$this->delete('id')) {
            $this->response(array('status' => 'failed', 'message' => 'invalid id'));
        }

        $update = array(
            'status' => 0
        );
        $user_id = $this->delete('id');
        $result = $this->Sync_model->update("users", $update, "user_id = $user_id");

        if ($result === FALSE) {
            $this->response(array('status' => 'failed', 'message' => 'Invalid db operation'));
        } else {

            $output = array();
            $output["success"] = true;
            $output["message"] = "Deleted successfully";
            $this->response($output, 200);
        }
    }

    function users_get() {
        $users = $this->Sync_model->getData("users", "status = 1");
        $output = array();
        if ($users) {
            $output["success"] = true;
            $output["users"] = $users->result_array();
            $output["message"] = "";
        } else {
            $output["success"] = false;
            $output["message"] = "No users";
        }
        $this->response($output);
    }

}
