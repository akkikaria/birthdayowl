<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DemoLogin extends CI_Controller {

    public function index() {
        $this->load->View("login");
    }
    
    public function afterLogin() {
        require_once(APPPATH. "libraries/Hybrid/Auth.php" );
        require_once(APPPATH. "libraries/Hybrid/Endpoint.php" );
//           require_once 'vendor/autoload.php';
        Hybrid_Endpoint::process();
    }

}
