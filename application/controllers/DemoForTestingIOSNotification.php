<?php

class DemoForTestingIOSNotification extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function TestNotification() {
        $deviceToken = "3e2f6fc9ededf7bf871702c6759d3b8847c1472a4ec3ec260a5f7268cb31e133";
        //  $deviceToken="15364d888a5593b7ec8384eee7ead588a3920327e3c08f8f56c54a5d5803c54f";
        //  $deviceToken="042793e2511b1aff139ab33ef0f728a05dd8ff6740104fe1b8ee378bbba0108b";
        // Put your private key's passphrase here:
        $passphrase = 'Tech1mini';
        //echo "Hello";
        // Put your alert message here:
        //$message = 'My first push notification!';
        ////////////////////////////////////////////////////////////////////////////////

        $ctx = stream_context_create();
        //production
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'SandboxDemoServer.pem');
//        stream_context_set_option($ctx, 'ssl', 'local_cert', 'pushNotifictnProdnBirthdayMattersCertificate.pem');
//                stream_context_set_option($ctx, 'ssl', 'local_cert', '/home/birthadp/demo.birthdayowl.com/BIrthayMattersPushNotiProdCertificate.pem');
//                stream_context_set_option($ctx, 'ssl', 'local_cert', '/home/birthadp/demo.birthdayowl.com/PushCertificates.pem');
        //development
//        stream_context_set_option($ctx, 'ssl', 'local_cert', 'BMCertificates.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        // Open a connection to the APNS server
////                //production
//                $fp = stream_socket_client(
//                        'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
//                //Development
        $fp = stream_socket_client(
                'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
//
        if (!$fp) {
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        }
        //echo 'Connected to APNS' . PHP_EOL;
        // Create the payload body

        $type = '1';
        $user_id = '580';

        $message = "Hello";
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'badge' => "1",
            'type' => $type,
            'user_id' => $user_id
                /* 'alert' => "",
                  "badge" => "0",
                  "content-available" => "1",
                  "sound" => "" */
        );


        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        if (!$result)
            echo 'Message not delivered' . PHP_EOL;
        else
            echo 'Message successfully delivered' . PHP_EOL;
        fclose($fp);
    }

}
