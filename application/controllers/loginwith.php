<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class loginwith extends CI_Controller {

    function index($provider) {
        if (!isset($_SESSION)) {
            session_start();
        }
  $config = array("base_url" => "http://localhost/BirthdayOwl/DemoLogin/afterLogin",
          //$config = array("base_url" => "http://wwww.birthdayowl.com/DemoLogin/afterLogin",
            "providers" => array(
                "Google" => array(
                    "enabled" => true,
                    "keys" => array("id" => "499869126958-gk572al1rogqi90ltfq6orlekegic99f.apps.googleusercontent.com", "secret" => "q4WDRmFB3iXBw8IHr4LR3Bxs"),
                ),
                "Facebook" => array(
                    "enabled" => true,
                    "keys" => array("id" => "669064123234059", "secret" => "8658d66c4d7e1b192a02d464622df796"),
                    "scope" => "email, user_about_me, user_birthday, user_hometown"  //optional.              
                ),
                "Twitter" => array(
                    "enabled" => true,
                    "keys" => array("key" => "8af50260ae5444bdc34665c2b6e6daa9", "secret" => "93c1d8f362749dd1fe0a819ae8b5de95")
                ),
               
            ),
            // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
            "debug_mode" => false,
            "debug_file" => "debug.log",
        );

        require_once(APPPATH . 'libraries/Hybrid/Auth.php');

        try {
            $hybridauth = new Hybrid_Auth($config);
            $authProvider = $hybridauth->authenticate($provider);
            $user_profile = $authProvider->getUserProfile();
            $output["success"] = "true";
            $output["userdata"] = $user_profile;
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
//            if ($user_profile && isset($user_profile->identifier)) {
//                echo "<b>Name</b> :" . $user_profile->displayName . "<br>";
//                echo "<b>Profile URL</b> :" . $user_profile->profileURL . "<br>";
//                echo "<b>Image</b> :" . $user_profile->photoURL . "<br> ";
//                echo "<img src='" . $user_profile->photoURL . "'/><br>";
//                echo "<b>Email</b> :" . $user_profile->email . "<br>";
//                echo "<br> <a href='" . BASEURL . 'loginwith/logout' . "'>Logout</a>";
//         
//            }
        } catch (Exception $e) {
            switch ($e->getCode()) {
                case 0 : echo "Unspecified error.";
                    break;
                case 1 : echo "Hybridauth configuration error.";
                    break;
                case 2 : echo "Provider not properly configured.";
                    break;
                case 3 : echo "Unknown or disabled provider.";
                    break;
                case 4 : echo "Missing provider application credentials.";
                    break;
                case 5 : echo "Authentication failed The user has canceled the authentication or the provider refused the connection.";
                    break;
                case 6 : echo "User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.";
                    $authProvider->logout();
                    break;
                case 7 : echo "User not connected to the provider.";
                    $authProvider->logout();
                    break;
                case 8 : echo "Provider does not support this feature.";
                    break;
            }
            $output["success"] = "false";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));

//            echo "<br /><br /><b>Original error message:</b> " . $e->getMessage();
//
//            echo "<hr /><h3>Trace</h3> <pre>" . $e->getTraceAsString() . "</pre>";
        }
    }

    public function logout() {
        session_start();
        session_destroy();
        redirect(BASEURL . "DemoLogin");
    }

}

//    }
//}