<?php

//Live birthdayowl
Class Admin_panel extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    /* Admin Panel */

    public function admin_dashboard() {
        if ($this->is_admin_logged_in()) {

            $data["count_users"] = $this->Birthday->getUserData("users", "is_guest=0")->num_rows();
            $data["gcount_users"] = $this->Birthday->getUserData("users", "is_guest=1")->num_rows();
            $data["orders"] = $this->Birthday->getAllOrderPayment()->result_array();
            $data["vouchers_count"] = count($data["orders"]);
//        $data["vouchers_count"] = $this->Birthday->get_available_vouchers()->num_rows();
            $data["freegreeting_count"] = $this->Birthday->getUserData("free_greetings", "status='1'")->num_rows();
            $data["paid_members"] = $this->Birthday->getUserData("greeting_membership", "transaction_status='success'")->num_rows();
            $this->load->view('admin_panel/index', $data); //loading signup view
        }
    }

    public function admin_login() {
        $data['title'] = "BirthdayOwl";
        $this->load->view('admin_panel/pages-login', $data);
    }

    public function is_admin_logged_in() {
        if ($this->session->userdata('admin_data') != NULL) {
            return true;
        } else {
            redirect(BASEURL . "owl");
        }
    }

    public function admin_logout() {
        $this->session->sess_destroy();
        redirect(BASEURL . "owl");
    }

    public function admin_success() {
        if (count($_POST) > 0) {
            $username = check_post($this->input->post("username"));
            $password = check_post($this->input->post("password"));

            $where = "username='$username' and password='$password'";
            $check_user = $this->Birthday->getUserData("admin", $where)->result_array();
            if (count($check_user) == 0) {
                $output["success"] = false;
                $output["message"] = "Invalid Username or Password.";
            } else {
                $output["admin_data"] = $check_user[0];
                $output["success"] = true;
                $output["message"] = "Login Successfully";
                $this->session->set_userdata($output);
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function users() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
//            $user_data = $this->Birthday->getUserData("users", "1 order by user_id DESC")->result_array();
//            SELECT  FROM `users` as u LEFT JOIN bday_reminder as b on u.user_id=b.user_id group by u.user_id
            $user_data = $this->Birthday->getJoinedDataByGroup("count(b.reminder_id)as rem_count,  u.*", "users as u ", "bday_reminder as b", "u.user_id=b.user_id", " 1 and is_guest=0 group by u.user_id order by user_id DESC")->result_array();

            $data["user_info"] = $user_data;
            $this->load->view('admin_panel/users', $data);
        }
    }

    public function guest_users() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $user_data = $this->Birthday->getSelectData("user_id,email,created_date,first_name", "users", "is_guest=1")->result_array();
            $data["user_info"] = $user_data;
            $this->load->view('admin_panel/gusers', $data);
        }
    }

    public function friends() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $user_data = $this->Birthday->getAllData("users")->result_array();
// $refer_data = $this->Birthday->get_refer_data()->result_array();
            $data["user_info"] = $user_data;
            $this->load->view('admin_panel/friends', $data);
        }
    }

    public function get_refer_friend($user_id) {
        $data = array();
        $data["title"] = "BirthdayOwl";
        $where = "user_id=$user_id";
        $user_data = $this->Birthday->getUserData("refer_friend", $where)->result_array();
        $data["refer_info"] = $user_data;
        $this->load->view('admin_panel/refer_friend', $data);
    }

    public function newsletter() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $news_data = $this->Birthday->getUserData("news_letter", "mail_status=0")->result_array();
            $data["news_data"] = $news_data;
            $this->load->view('admin_panel/newsletter', $data);
        }
    }

    public function send_newsletter() {
        require_once(APPPATH . "libraries/Mailin.php" );
        $filename = "";
        $upload = FALSE;
        if (!empty($_FILES)) {

            $tempFile = trim($_FILES['userfile']['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
            $filename = str_replace(" ", "_", $filename);
//  $targetPath = $_SERVER["DOCUMENT_ROOT"] . "/LoginCustomer/";
            $targetPath = 'public/newsletter_img/';
            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = false;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES["userfile"]["error"];
//                    echo json_encode($output);
//                    exit;
                } else {
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $upload = TRUE;
//                    echo json_encode($output);
//                    exit;
                }
            } else {
                $output["filename"] = $filename;
                $output["success"] = true;
                $upload = TRUE;
//                echo json_encode($output);
//                exit;
            }
        }


        if (count($_POST) > 0) {
            $user_id = check_post($this->input->post("id"));
            $description = check_post($this->input->post("description"));
            $title = check_post($this->input->post("title"));
            $link = check_post($this->input->post("link"));
            if ($filename != '') {
                $image = $filename;
            } else {
                $image = "";
            }
            $count = count($user_id);
            for ($i = 0; $i < $count; $i++) {
                $where = "id='$user_id[$i]'";
                $news_data = $this->Birthday->getUserData("news_letter", $where)->row_array();
                $email = $news_data["email_id"];
                $name = $news_data["first_name"];
                $data["description"] = $description;
                $data["image"] = $image;
                $data["link"] = $link;
                $data["ulink"] = BASEURL . "Admin_panel/unsubscribe_mail/1/" . $this->Birthday->encryptPassword($email);
                try {
                    $mailin = new Mailin('https://api.sendinblue.com/v2.0', 'tsyShZ5wg1Ix6d09');
                    $nbody = $this->load->view("email_template/newsletter", $data, true);

                    $sdata = array("to" => array("$email" => "$name"),
                        "from" => array("newsletter@birthdayowl.com", "birthdayowl.com"),
                        "subject" => "$title",
                        "text" => "$nbody",
                        "html" => "$nbody"
                    );
                    $response = $mailin->send_email($sdata);


                    if ($response["code"] == "success") {
                        $up["mail_status"] = "1";
                        $this->Birthday->update("news_letter", $up, $where);
                    } else {
                        $up["mail_status"] = "0";
                        $this->Birthday->update("news_letter", $up, $where);
                    }
                } catch (Exception $e) {

                    $up["mail_status"] = "0";
                    $this->Birthday->update("news_letter", $up, $where);
                }
            }
            $output["success"] = true;
            $output["message"] = "Newsletter sent successfully!.";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function add_sign_data() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $this->load->view('admin_panel/add_sign_data', $data);
        }
    }

    public function display_zodiac_data() {
        if (count($_POST) > 0) {
            $sign_day = check_post($this->input->post("sign_day"));
            $sign_month = check_post($this->input->post("sign_month"));
            $where = "sign_day='$sign_day' and sign_month='$sign_month'";
            $check_user = $this->Birthday->getSelectData("sign_data", "zodiac_signs", $where)->result_array();
            if (count($check_user) == 0) {
                $output["success"] = false;
                $output["message"] = "No data";
            } else {
                $output["success"] = true;
                $output["zodiac_data"] = $check_user;
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function marquee() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $marque_data = $this->Birthday->getAllData("marque")->result_array();
            $data["marque"] = $marque_data;
            $this->load->view('admin_panel/marquee', $data);
        }
    }

    public function add_marquee() {
        if (count($_POST) > 0) {
            $data["marque_desc"] = check_post($this->input->post("marque_desc"));
            $notify_url = check_post($this->input->post("notify_url"));
            if ($notify_url == '') {
                $notify_url = "";
            }
            $data["created_date"] = date("Y-m-d H:i:s");
            $this->Birthday->addData("marque", $data);
            $desc = $data["marque_desc"];
            $new_desc = str_replace(" ", "@", $desc);

//xampp
//exec('C:\xampp\php\php.exe "C:\xampp\htdocs\Birthday\send_broadcast_notification.php" ' . $new_desc . ' ' . $notify_url . ' > C:\xampp\htdocs\Birthday\file.log & printf "%u" $!&');
//
// //for mac (using XAMPP)
            exec('/Applications/XAMPP/xamppfiles/bin/php "/Applications/XAMPP/htdocs/BirthdayOwl/send_broadcast_notification.php" ' . $new_desc . ' ' . $notify_url . '  > /dev/null &', $arrOutput);
// server linode
//            exec('/usr/bin/php "/var/www/storegrunt/public_html/Birthday/send_broadcast_notification.php" ' . $new_desc . ' ' . $notify_url . '  > /dev/null &', $arrOutput);
//            exec('/usr/bin/php "/home/birtdtho/public_html/send_broadcast_notification.php" ' . $new_desc . ' ' . $notify_url . '  > /dev/null &', $arrOutput);
            $output["success"] = true;
            $output["message"] = "Marque added Successfully!."; //If user name is not in the database
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));           // 
        }
    }

    function send_notification() {

        $description = check_post($this->input->post("marque_desc"));
        $new_desc = str_replace("@", " ", $description);
        $notify_url = check_post($this->input->post("notify_url"));
        $this->Birthday->send_broadcast($new_desc, "2", $notify_url);
    }

    public function change_marquee_status() {
        if (count($_POST) > 0) {
            $m_id = check_post($this->input->post("m_id"));
            $update["status"] = check_post($this->input->post("status"));
            $where = "m_id='$m_id'";
            $updated = $this->Birthday->update("marque", $update, $where);
            if ($updated == 1) {
                $output["success"] = true;
                $output["message"] = "";
            } else {
                $output["success"] = false;
                $output["message"] = "Fail to update";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function company() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
//        $marque_data = $this->Birthday->getAllData("marque")->result_array();
//        $data["marque"] = $marque_data;
            $this->load->view('admin_panel/company', $data);
        }
    }

    public function update_zodiac_data() {
        if (count($_POST) > 0) {
            $sign_day = check_post($this->input->post("sign_day"));
            $sign_month = check_post($this->input->post("sign_month"));
            $update["sign_data"] = check_post($this->input->post("sign_data"));
            $where = "sign_day='$sign_day' and sign_month='$sign_month'";
            $updated = $this->Birthday->update("zodiac_signs", $update, $where);
            if ($updated == 1) {
                $output["success"] = true;
                $output["message"] = "Zodiac Data updated successfully";
            } else {
                $output["success"] = false;
                $output["message"] = "Fail to update";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function menu($menu_id = NULL) {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $data["menu"] = $this->Birthday->getAllData("menu")->result_array();
            if ($menu_id == NULL) {
                $data["heading"] = "Add Menu";
            } else {
                $where = "menu_id=$menu_id";
                $data["heading"] = "Edit Menu";
                $menu_data = $this->Birthday->getUserData("menu", $where)->row_array();
                $data["menu_name"] = $menu_data["menu_name"];
                $data["menu_id"] = $menu_data["menu_id"];
            }
            $this->load->view('admin_panel/add_menu', $data);
        }
    }

    public function addUpdatemenu() {
        if (count($_POST) > 0) {
            $data["menu_name"] = check_post($this->input->post("menu_name"));
            $menu_id = check_post($this->input->post("menu_id"));
            if ($menu_id == "") {
                $data["menu_created_date"] = date("Y-m-d H:i:s");
                $this->Birthday->addData("menu", $data);
                $output["message"] = "Menu added Successfully!."; //If user name is not in the database
            } else {
                $where = "menu_id='$menu_id'";
                $updated = $this->Birthday->update("menu", $data, $where);
                if ($updated == 1) {
                    $output["success"] = true;
                    $output["message"] = "Menu updated successfully";
                } else {
                    $output["success"] = false;
                    $output["message"] = "Fail to update";
                }
            }

            $output["success"] = true;
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function category() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $data["heading"] = "ADD CATEGORY";
            $menu_data = $this->Birthday->getAllData("menu")->result_array();
            $data["menu"] = $menu_data;
            $cat_data = $this->Birthday->getCategoryMenuData()->result_array();
            $data["categories"] = $cat_data;
            $data2 = array(
                "category_id" => 0,
                "category_name" => '',
                "description" => '',
            );
            $data["category"] = $data2;

            $this->load->view('admin_panel/add_category', $data);
        }
    }

    public function add_category() {
        if (count($_POST) > 0) {
            $data["menu_id"] = check_post($this->input->post("menu_id"));
            $data["category_name"] = check_post($this->input->post("category_name"));
            $data["description"] = check_post($this->input->post("description"));
            $data["cat_created"] = date("Y-m-d H:i:s");
            $this->Birthday->addData("categories", $data);
            $output["success"] = true;
            $output["message"] = "Category added Successfully!."; //If user name is not in the database
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function update_category() {
        if (count($_POST) > 0) {
            $category_id = check_post($this->input->post("category_id"));
            $update["category_name"] = check_post($this->input->post("category_name"));
            $update["description"] = check_post($this->input->post("description"));
            $update["cat_updated"] = date("Y-m-d H:i:s");
            $where = "category_id='$category_id'";
            $updated = $this->Birthday->update("categories", $update, $where);
            if ($updated == 1) {
                $output["success"] = true;
                $output["message"] = "Category updated successfully";
            } else {
                $output["success"] = false;
                $output["message"] = "Fail to update";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function sub_category() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $menu_data = $this->Birthday->getCategoryMenu()->result_array();
            $data["menu_list"] = $menu_data;
            $category_data = $this->Birthday->getAllData("categories")->result_array();
            $data["category_list"] = $category_data;
            $data["sub_category_list"] = $category_data;
            $cat_data = $this->Birthday->getSubcategoryMenuData()->result_array();
            $data["sub_categories"] = $cat_data;
            $this->load->view('admin_panel/add_subcategory', $data);
        }
    }

    public function get_sub_catDetails() {
        $category_id = check_post($this->input->post("category_id"));
        $where = "category_id='$category_id'";
        $category_data = $this->Birthday->getUserData("categories", $where)->row_array();
        $output["success"] = true;
        $output["message"] = "";
        $output["subcategory"] = $category_data;

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    public function add_subcategory() {
        if (count($_POST) > 0) {
            $data["menu_id"] = check_post($this->input->post("menu_id"));
            $data["is_Subcategory"] = check_post($this->input->post("is_Subcategory"));
            $data["category_name"] = check_post($this->input->post("category_name"));
            $data["description"] = check_post($this->input->post("description"));
            $data["cat_created"] = date("Y-m-d H:i:s");
            $this->Birthday->addData("categories", $data);
            $output["success"] = true;
            $output["message"] = "Sub-Category added Successfully!."; //If user name is not in the database
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    function get_category_name() {
        if (count($_POST) > 0) {
            $menu_id = check_post($this->input->post("menu_id"));
            $where = "menu_id=$menu_id and is_Subcategory=0";
            $sub_category = $this->Birthday->getSelectData("category_name,category_id", "categories", $where)->result_array();
            $output["subcategory"] = $sub_category;
            $output["success"] = true;
            $output["message"] = "";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    function get_SubCategory_name() {
        if (count($_POST) > 0) {
            $menu_id = check_post($this->input->post("menu_id"));
            $sub_category = $this->Birthday->getSubcategoryNameMenuData($menu_id)->result_array();
            $output["subcategory"] = $sub_category;
            $output["success"] = true;
            $output["message"] = "";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    function SubCategory_name() {
        if (count($_POST) > 0) {
            $is_Subcategory = check_post($this->input->post("is_Subcategory"));
            $where = "is_Subcategory=$is_Subcategory";
            $sub_category = $this->Birthday->getSelectData("category_name,category_id", "categories", $where)->result_array();
            $output["subcategory"] = $sub_category;
            $output["success"] = true;
            $output["message"] = "";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function update_subcategory() {
        if (count($_POST) > 0) {
            $category_id = check_post($this->input->post("category_id"));
            $update["category_name"] = check_post($this->input->post("category_name"));
            $update["menu_id"] = check_post($this->input->post("menu_id"));
            $update["description"] = check_post($this->input->post("description"));
            $update["cat_updated"] = date("Y-m-d H:i:s");
            $where = "category_id='$category_id'";
            $updated = $this->Birthday->update("categories", $update, $where);
            if ($updated == 1) {
                $output["success"] = true;
                $output["message"] = "Sub-Category updated successfully";
            } else {
                $output["success"] = false;
                $output["message"] = "Fail to update";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function product() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $menu_data = $this->Birthday->getCategoryMenu()->result_array();
            $data["menu_list"] = $menu_data;

            $data["stock"] = $this->Birthday->getAllData("stock_status")->result_array();
            $data["product_data"] = array(
                '0' => Array
                    (
                    'pro_id' => '',
                    'product_code' => '',
                    'menu_id' => '',
                    'product_name' => '',
                    'meta_tags' => '',
                    'product_desc' => '',
                    'product_size' => '',
                    'price' => '',
                    'discount' => '',
                    'stock_status_id' => '',
                    'tax' => '',
                    'additional_charges' => '',
                    'brand' => '',
                    'is_feature_product' => '',
                    'is_special_product' => '',
                    'is_best_selling' => '',
                    'is_latest' => '',
                    'product_image' => '',
                    'additional_image' => ''
            ));
            $data["btn_click"] = "submit_product";
            $data["action"] = "Admin_panel/product_success";
            $this->load->view('admin_panel/add_product', $data);
        }
    }

    public function edit_product($pro_id) {
        $data = array();
        $where = "pro_id='$pro_id'";
        $product_data = $this->Birthday->getUserData("product", $where)->result_array();

//        //      $data["product_data"]=$product_data;
//        $menu_id = $product_data[0]["menu_id"];
//        $category_id = $product_data[0]["category_id"];
//        $where = "menu_id='$menu_id'";
//        $menu_data = $this->Birthday->getUserData("menu", $where)->result_array();
//        $where = "category_id='$category_id'";
//        $cat_data = $this->Birthday->getUserData("categories", $where)->result_array();
        $data["product_data"] = $product_data;

        $data["btn_click"] = "edit_product";
        $data["title"] = "BirthdayOwl";
        $menu_data = $this->Birthday->getCategoryMenu()->result_array();
        $data["menu_list"] = $menu_data;
        $data["action"] = "Admin_panel/productEdit_success/$pro_id";
        $data["stock"] = $this->Birthday->getAllData("stock_status")->result_array();
        $this->load->view('admin_panel/add_product', $data);
    }

    public function edit_subcatproduct($pro_id) {
        $data = array();
        $where = "pro_id='$pro_id'";
        $product_data = $this->Birthday->getUserData("product", $where)->result_array();

        $data["product_data"] = $product_data;

        $data["btn_click"] = "edit_product";
        $data["title"] = "BirthdayOwl";
        $menu_data = $this->Birthday->getCategoryMenu()->result_array();
        $data["menu_list"] = $menu_data;
        $data["action"] = "Admin_panel/productEdit_success/$pro_id";
        $data["stock"] = $this->Birthday->getAllData("stock_status")->result_array();
        $this->load->view('admin_panel/add_product', $data);
    }

    public function relationship() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $relation_data = $this->Birthday->getAllData("relationship")->result_array();
            $data["relation"] = $relation_data;
            $this->load->view('admin_panel/add_relationship', $data);
        }
    }

    public function add_relationship() {
        if (count($_POST) > 0) {
            $data["relation_name"] = check_post($this->input->post("relation_name"));
            $this->Birthday->addData("relationship", $data);
            $output["success"] = true;
            $output["message"] = "Relation added Successfully!."; //If user name is not in the database
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function occupation() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $occupation_data = $this->Birthday->getAllData("occupation")->result_array();
            $data["occupation"] = $occupation_data;
            $this->load->view('admin_panel/add_occupation', $data);
        }
    }

    public function add_occupation() {
        if (count($_POST) > 0) {
            $data["occupation"] = check_post($this->input->post("occupation"));
            $this->Birthday->addData("occupation", $data);
            $output["success"] = true;
            $output["message"] = "occupation added Successfully!."; //If user name is not in the database
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function product_success() {

        $filename = "";
        $file_count = count($_FILES["userfile"]["name"]);
        for ($i = 0; $i < $file_count; $i++) {
            $upload = FALSE;
            if (!empty($_FILES)) {
                $tempFile = trim($_FILES['userfile']['tmp_name'][$i]);
                $filename = str_replace("%20", "_", $_FILES['userfile']['name'][$i]);
                $filename = str_replace(" ", "_", $filename);
                $targetPath = 'public/product_img/';
                if (!file_exists($targetPath)) {
                    mkdir($targetPath, 0777, true);
                }
                $targetFile = str_replace('//', '/', $targetPath) . $filename;
                if (!@copy($tempFile, $targetFile)) {
                    if (!@move_uploaded_file($tempFile, $targetFile)) {
                        $output["success"] = false;
                        $output["error_no"] = 6;
                        $output["message"] = "File Cannot Be Uploaded";
                        $output["error"] = $_FILES["userfile"]["error"][$i];
                    } else {
                        $output["filename"][$i] = $filename;
                        $output["success"] = true;
                        $upload = TRUE;
                    }
                } else {
                    $output["filename"][$i] = $filename;
                    $output["success"] = true;
                    $upload = TRUE;
                }
            }
        }
        if (count($_POST) > 0) {
            $data["product_code"] = check_post($this->input->post("product_code"));
            $data["menu_id"] = check_post($this->input->post("menu_id"));
            $data["category_id"] = check_post($this->input->post("category_id"));
            $data["subcategory_id"] = check_post($this->input->post("subcategory_id"));
            $data["product_name"] = check_post($this->input->post("product_name"));
            $data["meta_tags"] = check_post($this->input->post("meta_tags"));
            $data["product_desc"] = check_post($this->input->post("product_desc"));
            $data["price"] = check_post($this->input->post("price"));
            $data["discount"] = check_post($this->input->post("discount"));
            $data["stock_status_id"] = check_post($this->input->post("stock_status_id"));
            $data["tax"] = check_post($this->input->post("tax"));
            $data["additional_charges"] = check_post($this->input->post("additional_charges"));
            $data["brand"] = check_post($this->input->post("brand"));
            $data["is_feature_product"] = check_post($this->input->post("is_feature_product"));
            $data["is_special_product"] = check_post($this->input->post("is_special_product"));
            $data["is_best_selling"] = check_post($this->input->post("is_best_selling"));
            $data["is_latest"] = check_post($this->input->post("is_latest"));
            $data["product_image"] = $output["filename"][0];
            $data["additional_image"] = $output["filename"][1];
            $data["product_created"] = date("Y-m-d H:i:s");
            $this->Birthday->addData("product", $data);
            redirect(BASEURL . "Admin_panel/product");
        }
    }

    public function productEdit_success($pro_id) {
        $filename = "";
        $file_count = count($_FILES["userfile"]["name"]);
        for ($i = 0; $i < $file_count; $i++) {
            $upload = FALSE;
            if (!empty($_FILES)) {
                $tempFile = trim($_FILES['userfile']['tmp_name'][$i]);
                $filename = str_replace("%20", "_", $_FILES['userfile']['name'][$i]);
                $filename = str_replace(" ", "_", $filename);
                $targetPath = 'public/product_img/';
                if (!file_exists($targetPath)) {
                    mkdir($targetPath, 0777, true);
                }
                $targetFile = str_replace('//', '/', $targetPath) . $filename;
                if (!@copy($tempFile, $targetFile)) {
                    if (!@move_uploaded_file($tempFile, $targetFile)) {
                        $output["success"] = false;
                        $output["error_no"] = 6;
                        $output["message"] = "File Cannot Be Uploaded";
                        $output["error"] = $_FILES["userfile"]["error"][$i];
                    } else {
                        $output["filename"][$i] = $filename;
                        $output["success"] = true;
                        $upload = TRUE;
                    }
                } else {
                    $output["filename"][$i] = $filename;
                    $output["success"] = true;
                    $upload = TRUE;
                }
            }
        }
        if (count($_POST) > 0) {
//  $update["pro_id"]=$this->input->post("pro_id");
//  $pro_id=  $update["pro_id"];
            $update["product_code"] = check_post($this->input->post("product_code"));
            $update["menu_id"] = check_post($this->input->post("menu_id"));
            $update["category_id"] = check_post($this->input->post("category_id"));
            $update["subcategory_id"] = check_post($this->input->post("subcategory_id"));
            $update["product_name"] = check_post($this->input->post("product_name"));
            $update["meta_tags"] = check_post($this->input->post("meta_tags"));
            $update["product_desc"] = check_post($this->input->post("product_desc"));
            $update["price"] = check_post($this->input->post("price"));
            $update["discount"] = check_post($this->input->post("discount"));
            $update["stock_status_id"] = check_post($this->input->post("stock_status_id"));
            $update["tax"] = check_post($this->input->post("tax"));
            $update["additional_charges"] = check_post($this->input->post("additional_charges"));
            $update["brand"] = check_post($this->input->post("brand"));
            $update["is_feature_product"] = check_post($this->input->post("is_feature_product"));
            $update["is_special_product"] = check_post($this->input->post("is_special_product"));
            $update["is_best_selling"] = check_post($this->input->post("is_best_selling"));
            $update["is_latest"] = check_post($this->input->post("is_latest"));
            if ($output["filename"][0] != '') {
                $update["product_image"] = $output["filename"][0];
            } else {
                $update["product_image"] = check_post($this->input->post("userfile_old1"));
            }
            if ($output["filename"][1] != '') {
                $update["additional_image"] = $output["filename"][1];
            } else {
                $update["additional_image"] = check_post($this->input->post("userfile_old2"));
            }
            $where = "pro_id='$pro_id'";
            $updated = $this->Birthday->update("product", $update, $where);
            redirect(BASEURL . "Admin_panel/edit_product/" . $pro_id);
        }
    }

    public function view_product() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $this->load->view('admin_panel/view_product', $data);
        }
    }

    public function get_product_data() {
        if (count($_POST) > 0) {
            $id = $this->input->post("id");
            if ($id == 0) {
                $category_data = $this->Birthday->get_subcategorywise_data()->result_array();
//  $subcategory_id=$category_data["subcategory_id"];
                $output["subcategory"] = $category_data;
                $output["success"] = true;
                $output["message"] = '';
            } else if ($id == 1) {
                $category_data = $this->Birthday->get_categorywise_data()->result_array();
                $output["category"] = $category_data;
                $output["success"] = true;
                $output["message"] = '';
            } else if ($id == 2) {
                
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

//brand start
    public function brands($brand_id = NULL) {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $data["vouchers"] = $this->Birthday->getAllData("voucher_product")->result_array();
            if ($brand_id == NULL) {
                $data["heading"] = "Add Brand";
                $data["voucher_product"] = $this->Birthday->get_brand_voucher_data()->result_array();
                $data["btn_click"] = "add_brand";
                $data["brands"] = array(
                    'brand_id' => '',
                    'brand_name' => '',
                    'voucher_pro_id' => '0'
                );
            } else {
                $where = "brand_id='$brand_id'";
                $data["heading"] = "Edit Brand";
                $data["btn_click"] = "edit_brand";
                $brand_data = $this->Birthday->getUserData("brands", $where)->row_array();
                $data["brands"] = $brand_data;
                $vouchers = $this->Birthday->get_brand_voucher_data()->result_array();
                $data["voucher_product"] = $vouchers;
            }

            $this->load->view('admin_panel/add_brand', $data);
        }
    }

    public function addUpdatebrand() {
        if (count($_POST) > 0) {
            $data["brand_name"] = $brand_name = check_post($this->input->post("brand_name"));
            $data["voucher_pro_id"] = $voucher_pro_id = check_post($this->input->post("voucher_pro_id"));
            $data["brand_created_date"] = date("Y-m-d H:i:s");
            $brand_id = check_post($this->input->post("brand_id"));

            if ($brand_id == "") {
                $where = "brand_name='$brand_name' and voucher_pro_id='$voucher_pro_id'";
                $check_brand = $this->Birthday->getUserData("brands", $where)->num_rows();
                if ($check_brand > 0) {
                    $output["success"] = FALSE;
                    $output["message"] = "Product voucher with current brand name is already exist";
                    echo json_encode($output);
                    exit;
                }
                $this->Birthday->addData("brands", $data);
                $output["message"] = "Brand added Successfully!."; //If user name is not in the database  
            } else {
                $where = "brand_name='$brand_name' and voucher_pro_id='$voucher_pro_id' and brand_id!='$brand_id'";
                $check_brand = $this->Birthday->getUserData("brands", $where)->num_rows();
                if ($check_brand > 0) {
                    $output["success"] = FALSE;
                    $output["message"] = "Product voucher with current brand name is already exist";
                    echo json_encode($output);
                    exit;
                }
                $updated_data["brand_name"] = $brand_name;
                $updated_data["voucher_pro_id"] = $voucher_pro_id;
                $where = "brand_id='$brand_id'";
                $this->Birthday->update("brands", $updated_data, $where);
                $output["message"] = "Brand Updated Successfully!."; //If user name is not in the database
            }

            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

//brand-end
//banner start
    public function banner() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $data["heading"] = "ADD HOME BANNER";
            $data["banner"] = $this->Birthday->getAllData("festival_banners")->result_array();
            $data["btn_click"] = "submit_banner";
            $data["action"] = "Admin_panel/add_banner";
            $data["id"] = "festival_name";
            $data["count"] = '';
            $this->load->view('admin_panel/add_banner', $data);
//             $this->load->view('admin_panel/demo1');
        }
    }

    public function add_banner() {


        $filename = "";
        $banner_data = array();

        $check_last_position = $this->Birthday->get_last_record("festival_banners")->result_array();

        if (count($check_last_position) == 0) {
            $position_count = 1;
        } else {
            $position_count = $check_last_position[0]["position"] + 1;
        }

        $file_count = count($_FILES['userfile']['name']);
        if (!empty($_FILES)) {
            for ($i = 0; $i < $file_count; $i++) {
                if ($_FILES['userfile']['name'][$i] != '') {
                    $tempFile = trim($_FILES['userfile']['tmp_name'][$i]);
                    $filename = str_replace("%20", "_", $_FILES['userfile']['name'][$i]);
                    $filename = str_replace(" ", "_", $filename);
                    $targetPath = 'public/banner_pic/';
                    if (!file_exists($targetPath)) {
                        mkdir($targetPath, 0777, true);
                    }
                    $targetFile = str_replace('//', '/', $targetPath) . $filename;
                    if (!@copy($tempFile, $targetFile)) {
                        if (!@move_uploaded_file($tempFile, $targetFile)) {
                            $output["success"] = false;
                            $output["error_no"] = 6;
                            $output["message"] = "File Cannot Be Uploaded";
                            $output["error"] = $_FILES['userfile']["error"][$i];
                        } else {
                            $output["filename"][$i] = $filename;
                            $output["success"] = true;
                            $banner_data["banner_image"] = $filename;
                        }
                    } else {
                        $output["filename"][$i] = $filename;
                        $output["success"] = true;
                        $banner_data["banner_image"] = $filename;
                    }
                    $banner_data["banner_created_date"] = date("Y-m-d H:i:s");
                    $banner_data["position"] = $position_count;

                    $this->Birthday->addData("festival_banners", $banner_data);
                }
                $position_count++;
            }
        }
        redirect(BASEURL . "Admin_panel/banner");
    }

    public function Change_banner_img($fb_id) {
        $filename = "";
        $upload = FALSE;
        if (!empty($_FILES)) {
            $tempFile = trim($_FILES['bannerimg' . $fb_id]['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['bannerimg' . $fb_id]['name']);
            $filename = str_replace(" ", "_", $filename);
            $targetPath = 'public/banner_pic/';
            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = false;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES["bannerimg" . $fb_id]["error"];
                } else {
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $upload = TRUE;
                }
            } else {
                $output["filename"] = $filename;
                $output["success"] = true;
                $upload = TRUE;
            }
            $updated_data["banner_image"] = $filename;
            $where = "fb_id='$fb_id'";
            $this->Birthday->update("festival_banners", $updated_data, $where);
        }
        redirect(BASEURL . "Admin_panel/banner");
    }

    public function check_festival_name() {
        if (count($_POST) > 0) {
            $festival_name = $this->input->post("festival_name");
            $banner_type = $this->input->post("banner_type");
            $where = "festival_name='$festival_name'";
            $check_user = $this->Birthday->getUserData("festivals", $where)->num_rows();
            if ($check_user > 0) {
                if ($banner_type == "1") {
                    $output["success"] = false;
                    $output["message"] = "Banner name already exists ";
                } else {
                    $output["success"] = false;
                    $output["message"] = "Festival name already exists ";
                }
            } else {
                $output["success"] = true;
                $output["message"] = ""; //If user name is not in the database
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function Edit_check_festival_name() {
        if (count($_POST) > 0) {
            $festival_name = ltrim(check_post($this->input->post("festival_name")));
            $banner_type = $this->input->post("banner_type");
            $festival_id = check_post($this->input->post("festival_id"));
            $where = "festival_name='$festival_name' and festival_id!='$festival_id'";
            $check_user = $this->Birthday->getUserData("festivals", $where)->num_rows();
            if ($check_user > 0) {
                if ($banner_type == "1") {
                    $output["success"] = false;
                    $output["message"] = "Banner name already exists ";
                } else {
                    $output["success"] = false;
                    $output["message"] = "Festival name already exists ";
                }
            } else {
                $output["success"] = true;
                $output["message"] = ""; //If user name is not in the database
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function update_position() {
        if (count($_POST) > 0) {
            $fb_id = ltrim(check_post($this->input->post("fb_id")));
            $update["position"] = $this->input->post("position");
            $type = $this->input->post("type"); //1-banner 2-greetings
            switch ($type) {
                case 1: {
                        $where = "fb_id='$fb_id'";
                        $updated = $this->Birthday->update("festival_banners", $update, $where);
                    }
                    break;
                case 2: {
                        $where = "card_id='$fb_id'";
                        $updated = $this->Birthday->update("greeting_card", $update, $where);
                    }
                    break;
                default:
                    break;
            }


            if ($updated == 1) {
                $output["success"] = true;
                $output["message"] = "Position updated";
            } else {
                $output["success"] = false;
                $output["message"] = "Fail to update";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function festivals() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $data["heading"] = "ADD FESTIVAL BANNER";
            $where = "banner_type=2";
            $product_voucher = $this->Birthday->getAllData("voucher_product")->result_array();
            $data["product_voucher"] = $product_voucher;
            $banner_data = $this->Birthday->getUserData("festivals", $where)->result_array();
            $data["id"] = "festival_name";
            $data["banner"] = $banner_data;
            $data["btn_click"] = "submit_banner";
            $data["action"] = "Admin_panel/add_festival_banner";
            $data["banner_data"] = array(
                'festival_id' => '',
                'festival_name' => '',
                'url' => ''
            );
            $data["banner_festival"] = array(
                "0" => Array(
                    "banner_image" => '',
                    "festival_id" => ''
                ),
                "1" => Array(
                    "banner_image" => '',
                    "festival_id" => ''
                ),
                "2" => Array(
                    "banner_image" => '',
                    "festival_id" => ''
                ),
                "3" => Array(
                    "banner_image" => '',
                    "festival_id" => ''
                ),
            );
            $data['voucher_id'] = '';
            $this->load->view('admin_panel/add_festival_banner', $data);
        }
    }

    public function add_festival_banner() {
        if (count($_POST) > 0) {
            $data["festival_name"] = check_post($this->input->post("festival_name"));
            $data["banner_type"] = check_post($this->input->post("banner_type"));
            $data["url"] = check_post($this->input->post("url"));
            $data["created_date"] = date("Y-m-d H:i:s");
            $festival_id = $this->Birthday->addData("festivals", $data);
            $voucher_pro_id = check_post($this->input->post("voucher_pro_id"));
            if ($voucher_pro_id != '') {
                $v_id = implode(",", $voucher_pro_id);
                $voucher["voucher_id"] = $v_id;
                $voucher["festival_id"] = $festival_id;
                $voucher["fv_created_date"] = date("Y-m-d H:i:s");
                $this->Birthday->addData("festival_vouchers", $voucher);
            }
//          
//
//            $count = count($voucher_pro_id);
//            for ($i = 0; $i < $count; $i++) {
//                $voucher["voucher_id"] = $voucher_pro_id[$i];
//                $voucher["festival_id"] = $festival_id;
//                $voucher["fv_created_date"] = date("Y-m-d H:i:s");
//                $this->Birthday->addData("festival_vouchers", $voucher);
//            }
        }
        if ($festival_id != '') {
            $filename = "";
            $banner_data = array();
            $file_count = count($_FILES['userfile']['name']);
            if (!empty($_FILES)) {
                for ($i = 0; $i < $file_count; $i++) {
                    if ($_FILES['userfile']['name'][$i] != '') {
                        $tempFile = trim($_FILES['userfile']['tmp_name'][$i]);
                        $filename = str_replace("%20", "_", $_FILES['userfile']['name'][$i]);
                        $filename = str_replace(" ", "_", $filename);
                        $targetPath = 'public/banner_pic/';
                        if (!file_exists($targetPath)) {
                            mkdir($targetPath, 0777, true);
                        }
                        $targetFile = str_replace('//', '/', $targetPath) . $filename;
                        if (!@copy($tempFile, $targetFile)) {
                            if (!@move_uploaded_file($tempFile, $targetFile)) {
                                $output["success"] = false;
                                $output["error_no"] = 6;
                                $output["message"] = "File Cannot Be Uploaded";
                                $output["error"] = $_FILES['userfile']["error"][$i];
                            } else {
                                $output["filename"][$i] = $filename;
                                $output["success"] = true;
                                $banner_data["banner_image"] = $filename;
                            }
                        } else {
                            $output["filename"][$i] = $filename;
                            $output["success"] = true;
                            $banner_data["banner_image"] = $filename;
                        }
                        $banner_data["banner_created_date"] = date("Y-m-d H:i:s");
                        $banner_data["festival_id"] = $festival_id;
                        $this->Birthday->addData("festival_banners", $banner_data);
                    }
                }
            }
            redirect(BASEURL . "Admin_panel/festivals");
        }
    }

    public function edit__festival_banner($festival_id) {
        $data = array();
        $where = "festival_id='$festival_id'";
        $banner = $this->Birthday->getUserData("festivals", $where)->row_array();
        $banner_data = $this->Birthday->getUserData("festival_banners", $where)->result_array();
        $voucher_id = $this->Birthday->getUserData("festival_vouchers", $where)->row_array();
        if (count($voucher_id) > 0) {
            $data["voucher_id"] = $voucher_id["voucher_id"];
        } else {
            $data["voucher_id"] = '';
        }
        $data["banner_data"] = $banner;
        $data["banner_festival"] = $banner_data;
        $data["count"] = count($banner_data);
        $data["btn_click"] = "edit_banner";
        $data["heading"] = "EDIT FESTIVAL BANNER";
        $where = "banner_type=2";
        $banner_data1 = $this->Birthday->getUserData("festivals", $where)->result_array();
        $data["banner"] = $banner_data1;
        $product_voucher = $this->Birthday->getAllData("voucher_product")->result_array();
        $data["product_voucher"] = $product_voucher;
        $data["action"] = "Admin_panel/festival_edit_success/$festival_id";
        $data["title"] = "BirthdayOwl";
        $data["id"] = "festival_name_edit";
        $this->load->view('admin_panel/add_festival_banner', $data);
    }

    public function festival_edit_success($festival_id) {
        if (count($_POST) > 0) {
            $updated_data["festival_name"] = check_post($this->input->post("festival_name"));
            $updated_data["url"] = check_post($this->input->post("url"));
            $userfile_old = $this->input->post("userfile_old");
            $where = "festival_id='$festival_id'";
            $this->Birthday->update("festivals", $updated_data, $where);
            $this->Birthday->delete_reminder("festival_banners", $where);
            $voucher_id = check_post($this->input->post("voucher_pro_id"));
            if ($voucher_pro_id != '') {
                $v_id = implode(",", $voucher_id);
                $check_fest_id = $this->Birthday->getUserData("festival_vouchers", $where)->num_rows();
                if ($check_fest_id > 0) {
                    $voucher_update["festival_id"] = $festival_id;
                    $voucher_update["voucher_id"] = $v_id;
                    $this->Birthday->update("festival_vouchers", $voucher_update, $where);
                } else {
                    $voucher["festival_id"] = $festival_id;
                    $voucher["fv_created_date"] = date("Y-m-d H:i:s");
                    $voucher["voucher_id"] = $v_id;
                    $this->Birthday->addData("festival_vouchers", $voucher);
                }
            }
//            $count = count($voucher_pro_id);
//            for ($i = 0; $i < $count; $i++) {
//                $voucher["voucher_id"] = $voucher_pro_id[$i];
//                $voucher["festival_id"] = $festival_id;
//                $voucher["fv_created_date"] = date("Y-m-d H:i:s");
//                $this->Birthday->addData("festival_vouchers", $voucher);
//            }
        }
        if ($festival_id != '') {
            $filename = "";
            $banner_data = array();
            $file_count = count($_FILES['userfile']['name']);
            if (!empty($_FILES)) {
                for ($i = 0; $i < $file_count; $i++) {
                    $upload = FALSE;
                    if ($_FILES['userfile']['name'][$i] != '') {
                        $tempFile = trim($_FILES['userfile']['tmp_name'][$i]);
                        $filename = str_replace("%20", "_", $_FILES['userfile']['name'][$i]);
                        $filename = str_replace(" ", "_", $filename);
                        $targetPath = 'public/banner_pic/';
                        if (!file_exists($targetPath)) {
                            mkdir($targetPath, 0777, true);
                        }
                        $targetFile = str_replace('//', '/', $targetPath) . $filename;
                        if (!@copy($tempFile, $targetFile)) {
                            if (!@move_uploaded_file($tempFile, $targetFile)) {
                                $output["success"] = false;
                                $output["error_no"] = 6;
                                $output["message"] = "File Cannot Be Uploaded";
                                $output["error"] = $_FILES['userfile']["error"][$i];
                            } else {
                                $output["filename"][$i] = $filename;
                                $output["success"] = true;
                                $upload = TRUE;
//$banner_data["banner_image"] = $filename;
                            }
                        } else {
                            $output["filename"][$i] = $filename;
                            $output["success"] = true;
//$banner_data["banner_image"] = $filename;
                        }
                        $banner_data["banner_image"] = $filename;
                    } else {
                        if ($upload == FALSE) {
                            $banner_data["banner_image"] = $userfile_old[$i];
                        }
                    }
                    if ($banner_data["banner_image"] != '') {
                        $banner_data["banner_created_date"] = date("Y-m-d H:i:s");
                        $banner_data["festival_id"] = $festival_id;
                        $this->Birthday->addData("festival_banners", $banner_data);
                    }
                }
            }
            redirect(BASEURL . "Admin_panel/festivals");
        }
    }

    public function activate_banners() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $banner_data = $this->Birthday->getAllData("festivals")->result_array();
            $data["banner"] = $banner_data;
            $this->load->view('admin_panel/activate_banners', $data);
        }
    }

    public function do_activate_banner() {
        if (count($_POST) > 0) {
            $festival_id = check_post($this->input->post("festival_id"));
            $update["active"] = 1;
            $where = "festival_id='$festival_id'";
            $check_active_status = $this->Birthday->getUserData("festivals", "active=1")->num_rows();
            if ($check_active_status > 0) {
                $before_update = array();
                $before_update["active"] = 0;
                $this->Birthday->update_banner_status("festivals", $before_update);
            }
            $updated = $this->Birthday->update("festivals", $update, $where);
            if ($updated == 1) {
                $output["success"] = true;
                $output["message"] = "Banner is activated";
            } else {
                $output["success"] = false;
                $output["message"] = "Fail to update";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function change_homepage() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $data["heading"] = "Change Home Screen Banners";
            $data["btn_click"] = "submit_hpages";
            $data["id"] = "festival_name";
            $screens = $this->Birthday->getAllData("home_screens")->result_array();
            $screen_count = count($screens);
            if ($screen_count > 0) {
                $data["screens"] = $screens;
                $data["action"] = "Admin_panel/edit_home_screens";
            } else {
                $data["screens"] = array();
                $data["action"] = "Admin_panel/add_home_screens";
            }
            $data["screen_count"] = $screen_count;
            $this->load->view('admin_panel/change_homepage', $data);
        }
    }

    public function add_home_screen($id) {
        $filename = "";
        $hp_data = array();
        if (!empty($_FILES)) {
            $tempFile = trim($_FILES['userfile']['tmp_name']);
            $filename = str_replace("%20", "_", $_FILES['userfile']['name']);
            $filename = str_replace(" ", "_", $filename);
            $targetPath = 'public/home_screens/';
            if (!file_exists($targetPath)) {
                mkdir($targetPath, 0777, true);
            }
            $targetFile = str_replace('//', '/', $targetPath) . $filename;
            if (!@copy($tempFile, $targetFile)) {
                if (!@move_uploaded_file($tempFile, $targetFile)) {
                    $output["success"] = false;
                    $output["error_no"] = 6;
                    $output["message"] = "File Cannot Be Uploaded";
                    $output["error"] = $_FILES['userfile']["error"];
                } else {
                    $output["filename"] = $filename;
                    $output["success"] = true;
                    $hp_data["image"] = $filename;
                }
            } else {
                $output["filename"] = $filename;
                $output["success"] = true;
                $hp_data["image"] = $filename;
            }
// $hp_data["position"] = $id;
            $hp_data["created_date"] = date("Y-m-d H:i:s");

            $where = "position='$id'";
            $this->Birthday->update("home_screens", $hp_data, $where);
        }

        redirect(BASEURL . "Admin_panel/change_homepage");
    }

    public function edit_home_screens() {
        $userfile_old = $this->input->post("userfile_old");
        $this->Birthday->truncate("home_screens");
        $filename = "";
        $hp_data = array();
        $file_count = count($_FILES['userfile']['name']);
        if (!empty($_FILES)) {
            for ($i = 0; $i < $file_count; $i++) {
                $upload = FALSE;
                if ($_FILES['userfile']['name'][$i] != '') {
                    $tempFile = trim($_FILES['userfile']['tmp_name'][$i]);
                    $filename = str_replace("%20", "_", $_FILES['userfile']['name'][$i]);
                    $filename = str_replace(" ", "_", $filename);
                    $targetPath = 'public/home_screens/';
                    if (!file_exists($targetPath)) {
                        mkdir($targetPath, 0777, true);
                    }
                    $targetFile = str_replace('//', '/', $targetPath) . $filename;
                    if (!@copy($tempFile, $targetFile)) {
                        if (!@move_uploaded_file($tempFile, $targetFile)) {
                            $output["success"] = false;
                            $output["error_no"] = 6;
                            $output["message"] = "File Cannot Be Uploaded";
                            $output["error"] = $_FILES['userfile']["error"][$i];
                        } else {
                            $output["filename"][$i] = $filename;
                            $output["success"] = true;
                            $upload = TRUE;
//$banner_data["banner_image"] = $filename;
                        }
                    } else {
                        $output["filename"][$i] = $filename;
                        $output["success"] = true;
//$banner_data["banner_image"] = $filename;
                    }
                    $hp_data["image"] = $filename;
                } else {
                    if ($upload == FALSE) {
                        $hp_data["image"] = $userfile_old[$i];
                    }
                }
                if ($hp_data["image"] != '') {
                    $hp_data["created_date"] = date("Y-m-d H:i:s");
                    $this->Birthday->addData("home_screens", $hp_data);
                }
            }
        }
        redirect(BASEURL . "Admin_panel/change_homepage");
    }

    public function orders() {

        $data["orders"] = $this->Birthday->getAllOrderPayment()->result_array();
        $data["count"] = count($data["orders"]);
        $this->load->view("admin_panel/display_orders", $data);
    }

    function paid_members() {
        $select = "t1.bill_to_name,t1.bill_to_email,t2.email,t1.total_amount,t1.transaction_status,t1.payment_mode,t1.membership_id,t1.plan_purchase_type,t1.purchased_date,t2.membership_status,t1.expiry_date,(SELECT TIMESTAMPDIFF(MONTH, NOW(),t1.expiry_date)) as days_left";
        $data["orders"] = $this->Birthday->getTwotableJoinData("greeting_membership", "users", $select, "t1.user_id=t2.user_id")->result_array();

        $data["count"] = count($data["orders"]);
        $this->load->view("admin_panel/paid_members", $data);
    }

    function get_membership_data() {
        if (count($_POST) > 0) {
            $from = strtr(check_post($this->input->post("order_start")), '/', '-');
            $sdate = date("Y-m-d", strtotime($from));
            $to = strtr(check_post($this->input->post("order_end")), '/', '-');
            $edate = date("Y-m-d", strtotime($to));
            $where = "DATE(purchased_date) between '$sdate' and '$edate'";
            $best_seller = $this->Birthday->getUserData('greeting_membership', $where)->result_array();
            if (count($best_seller) > 0) {
                $output["success"] = true;
                $output["membership"] = $best_seller;
            } else {
                $output["success"] = false;
                $output["message"] = "No Orders";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function get_orderData() {

        if (count($_POST) > 0) {
            $from = strtr(check_post($this->input->post("order_start")), '/', '-');
            $sdate = date("Y-m-d", strtotime($from));
            $to = strtr(check_post($this->input->post("order_end")), '/', '-');
            $edate = date("Y-m-d", strtotime($to));
            $where = "DATE(p.purchased_date) between '$sdate' and '$edate'";
            $best_seller = $this->Birthday->getIndividualOrderPayment($where)->result_array();

            if (count($best_seller) > 0) {
                $output["success"] = true;
                $output["vorders"] = $best_seller;
            } else {
                $output["success"] = false;
                $output["message"] = "No Orders";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function get_processing_orders() {

        if (count($_POST) > 0) {
            $from = strtr(check_post($this->input->post("order_start")), '/', '-');
            $sdate = date("Y-m-d", strtotime($from));
            $to = strtr(check_post($this->input->post("order_end")), '/', '-');
            $edate = date("Y-m-d", strtotime($to));
            $where = "DATE(p.purchased_date) between '$sdate' and '$edate' and o.delivery_status=6";
            $best_seller = $this->Birthday->getProcessingPaymentOrders($where)->result_array();
            if (count($best_seller) > 0) {
                $output["success"] = true;
                $output["vorders"] = $best_seller;
            } else {
                $output["success"] = false;
                $output["message"] = "No Orders";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function view_order($order_pro_id) {
        $where = "order_pro_id='$order_pro_id'";
        $data["payment_detail"] = $this->Birthday->getUserData("order_products", $where)->row_array();

        // $pro_id = $data["payment_detail"]["product_id"];
        // $user_id = $data["payment_detail"]["user_id"];
        // $where = "voucher_pro_id='$pro_id' and user_id='$user_id'";
        //$data["pro_detail"] = $this->Birthday->Voucher_paymentData($where)->result_array();

        $this->load->view("admin_panel/view_order", $data);
    }

    public function invoice() {
        $data["orders"] = $this->Birthday->getAllData("payment")->result_array();


        $data["count"] = count($data["orders"]);
        $this->load->view("admin_panel/invoice", $data);
    }

    public function view_invoice($payment_id) {
        $where = "payment_id='$payment_id'";
        $data["payment_detail"] = $this->Birthday->getVoucher_paymentData($where)->row_array();
//        echo "<pre>";
//        print_r($data);
//        exit();
        $this->load->view("admin_panel/view_invoice", $data);
    }

    public function best_sell() {
        $this->load->view("admin_panel/best_sell");
    }

    public function most_view() {
        $this->load->view("admin_panel/most_view");
    }

    public function ordered_product() {
        $this->load->view("admin_panel/ordered_product");
    }

    public function get_best_seller() {
        if (count($_POST) > 0) {
            $from = check_post($this->input->post("from"));
            $to = check_post($this->input->post("to"));

//            $best_seller = $this->Birthday->getBestSeller($from,$to)->result_array();
            $where = "best_seller_date between '$from' and '$to' and best_seller>=2";
            $best_seller = $this->Birthday->getUserData('voucher_product', $where)->result_array();

            if (count($best_seller) > 0) {
                $output["success"] = true;
                $output["best"] = $best_seller;
            } else {
                $output["success"] = false;
                $output["message"] = "No Best Seller Product Found";
            }


            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function get_ordered_products() {
        if (count($_POST) > 0) {
            $from = check_post($this->input->post("from"));
            $to = check_post($this->input->post("to"));

//            $best_seller = $this->Birthday->getBestSeller($from,$to)->result_array();
            $where = "best_seller_date between '$from' and '$to' and best_seller>=1";
            $best_seller = $this->Birthday->getUserData('voucher_product', $where)->result_array();


            if (count($best_seller) > 0) {
                $output["success"] = true;
                $output["best"] = $best_seller;
            } else {
                $output["success"] = false;
                $output["message"] = "No Best Seller Product Found";
            }


            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function get_most_viewed() {
        if (count($_POST) > 0) {
            $from = check_post($this->input->post("from"));
            $to = check_post($this->input->post("to"));

//            $best_seller = $this->Birthday->getBestSeller($from,$to)->result_array();
            $where = "most_viewed_date between '$from' and '$to' and click_counter>=5";
            $best_seller = $this->Birthday->getUserData('voucher_product', $where)->result_array();


            if (count($best_seller) > 0) {
                $output["success"] = true;
                $output["best"] = $best_seller;
            } else {
                $output["success"] = false;
                $output["message"] = "No Best Seller Product Found";
            }


            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function add_aboutus() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $aboutdata = $this->Birthday->getAllData("about_us")->row_array();
            $data["aboutdata"] = $aboutdata["about_data"];


            $this->load->view('admin_panel/add_aboutus', $data);
        }
    }

    public function update_about_us() {
        if (count($_POST) > 0) {
            $update["about_data"] = check_post($this->input->post("about_data"));
            $where = "id='1'";
            $updated = $this->Birthday->update("about_us", $update, $where);
            if ($updated == 1) {
                $output["success"] = true;
                $output["message"] = "About us Data updated successfully";
            } else {
                $output["success"] = false;
                $output["message"] = "Fail to update";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function getReminders() {
        $data = array();
        $user_id = $this->uri->segment(3);
        $getReminders = $this->Birthday->getUserData("bday_reminder", "user_id=$user_id")->result_array();

        if (count($getReminders) > 0) {
            $data["reminders"] = $getReminders;
            $data["rcount"] = count($getReminders);
        } else {
            $data["rcount"] = count($getReminders);
        }
        $this->load->view("admin_panel/Reminders", $data);
    }

    public function add_privacy() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $aboutdata = $this->Birthday->getAllData("privacy")->row_array();
            $data["privacy"] = $aboutdata["privacy_data"];
            $this->load->view('admin_panel/add_privacy', $data);
        }
    }

    public function update_privacy() {
        if (count($_POST) > 0) {
            $update["privacy_data"] = check_post($this->input->post("privacy"));
            $where = "id='1'";
            $updated = $this->Birthday->update("privacy", $update, $where);
            if ($updated == 1) {
                $output["success"] = true;
                $output["message"] = "Privacy Data updated successfully";
            } else {
                $output["success"] = false;
                $output["message"] = "Fail to update";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function add_terms() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $aboutdata = $this->Birthday->getAllData("terms")->row_array();
            $data["terms"] = $aboutdata["terms"];
            $this->load->view('admin_panel/add_terms', $data);
        }
    }

    public function update_terms() {
        if (count($_POST) > 0) {
            $update["terms"] = check_post($this->input->post("terms"));
            $where = "id='1'";
            $updated = $this->Birthday->update("terms", $update, $where);
            if ($updated == 1) {
                $output["success"] = true;
                $output["message"] = "Terms updated successfully";
            } else {
                $output["success"] = false;
                $output["message"] = "Fail to update";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function add_faq() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $faqdata = $this->Birthday->getAllData("faq")->result_array();
            $data["faq"] = $faqdata;
            $this->load->view('admin_panel/add_faq', $data);
        }
    }

    public function add_faqData() {
        if (count($_POST) > 0) {
            $data["title"] = check_post($this->input->post("title"));
            $data["description"] = check_post($this->input->post("description"));

            $this->Birthday->addData("faq", $data);
            $output["success"] = true;
            $output["message"] = "FAQ added Successfully!."; //If user name is not in the database
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function contactUs($contact_id = NULL) {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $data['contact'] = $this->Birthday->getAllData("contact_us")->result_array();
            if ($contact_id == NULL) {

                $data["btn_click"] = "add_contactus";
                $data["contact_data"] = array(
                    'email' => '',
                    'phone_no' => '',
                    'address' => '',
                    'id' => ''
                );
            } else {
                $where = "id='$contact_id'";
                $data['heading'] = "Edit Contact us";
                $voucher_data = $this->Birthday->getUserData("contact_us", $where)->row_array();
                $data["contact_data"] = $voucher_data;
                $data["btn_click"] = "edit_contact_us";
                $data["action"] = "Product_voucher/voucherCategory_edit_success/$contact_id";
            }
            $this->load->view('admin_panel/contact_us', $data);
        }
    }

    public function addUpdateContactus() {
        if (count($_POST) > 0) {
            $data["email"] = check_post($this->input->post("email"));
            $data["phone_no"] = check_post($this->input->post("phone_no"));
            $data["address"] = check_post($this->input->post("address"));
            $cid = check_post($this->input->post("id"));
            if ($cid == "") {
                $this->Birthday->addData("contact_us", $data);
                $output["message"] = "Added Successfully";
            } else {
                $where = "id='$cid'";
                $this->Birthday->update("contact_us", $data, $where);
                $output["message"] = "Updated Successfully";
            }
            $output["success"] = true;

            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function testimonial() {
        if ($this->is_admin_logged_in()) {
            $data = array();
            $data["title"] = "BirthdayOwl";
            $faqdata = $this->Birthday->getAllData("testimonial")->result_array();
            $data["testimonial"] = $faqdata;
            $this->load->view('admin_panel/testimonial', $data);
        }
    }

    public function add_testimonial() {
        if (count($_POST) > 0) {
            $data["description"] = check_post($this->input->post("description"));
            $data["witness_name"] = check_post($this->input->post("witness_name"));

            $this->Birthday->addData("testimonial", $data);
            $output["success"] = true;
            $output["message"] = "Added Successfully";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function membership_plan($mid = NULL) {
        $data = array();
        $data['title'] = "BirthdayOwl";
        $data['membership_plans'] = $this->Birthday->getAllData("membership_plans")->result_array();
        if ($mid == NULL) {
            $data["btn_click"] = "submit_voucher_category";
            $data['heading'] = "Add Product Voucher Category";
            $data["title"] = "BirthdayOwl";
            $data["plan_data"] = array(
                'id' => '',
                'plan_name' => '',
                'amount' => '',
                'duration' => ''
            );
        } else {
            $where = "id='$mid'";
            $data['heading'] = "Edit Membership Plan";
            $voucher_data = $this->Birthday->getUserData("membership_plans", $where)->row_array();
            $data["plan_data"] = $voucher_data;
            $data["btn_click"] = "edit_membership";
            $data['contact'] = $this->Birthday->getAllData("contact_us")->result_array();
        }


        $this->load->view('admin_panel/add_membership_plans', $data);
    }

    public function Change_plan_status() {

        if (count($_POST) > 0) {
            $id = check_post($this->input->post("id"));
            $update["status"] = check_post($this->input->post("status"));
            $where = "id=$id";
            $updated = $this->Birthday->update("membership_plans", $update, $where);
            if ($updated == 1) {
                $output["success"] = true;
                $output["message"] = "";
            } else {
                $output["success"] = false;
                $output["message"] = "Fail to update";
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function addUpdateMembership() {
        if (count($_POST) > 0) {
            $data["plan_name"] = check_post($this->input->post("plan_name"));
            $data["amount"] = check_post($this->input->post("amount"));
            $data["duration"] = check_post($this->input->post("duration"));
            $data["created_date"] = date("Y-m-d H:i:s");
            $cid = check_post($this->input->post("id"));
            if ($cid == "") {
                $this->Birthday->addData("membership_plans", $data);
                $output["message"] = "Membership Plan added Successfully!."; //If user name is not in the database
            } else {
                $where = "id='$cid'";
                $this->Birthday->update("membership_plans", $data, $where);
                $output["message"] = "Updated Successfully";
            }

            $output["success"] = true;
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function processing_orders() {
        $data["orders"] = $this->Birthday->getProcessingOrders()->result_array();
        $data["count"] = count($data["orders"]);
        $this->load->view("admin_panel/processing_orders", $data);
    }

    public function refund_view($order_pro_id) {

        //$order_pro_id = check_post($this->input->post("order_pro_id"));
        // $order_data = $this->Birthday->getJoinedData2("order_products.unique_oid,order_products.selected_amount,order_products.voucher_pro_name,order_products.uname,order_products.uemail,payment.payuId,", "order_products", "payment", "order_products.payment_id=payment.payment_id", "order_products.order_pro_id=$order_pro_id ")->result_array();
        $order_data = $this->Birthday->getSelectData("unique_oid,order_id,selected_amount,voucher_pro_name,uname,uemail", "order_products", "order_pro_id=$order_pro_id")->result_array();
        $orders["orders"] = $order_data;
//        $orders["payuid"] = $order_data[0]["payuId"];
        $orders["order_pro_id"] = $order_pro_id;
//        $data["delivery_status"] = "10";
//        $where = "order_pro_id='$order_pro_id'";
        // $this->Birthday->update("order_products", $data, $where);
        $this->load->view("admin_panel/refund", $orders);
    }

    public function mycartorders() {
        $data = array();
        $data["title"] = "BirthdayOwl";
        $user_data = $this->Birthday->getJoinedData2("users.first_name,users.email,users.user_id", "order_products", "users", "users.user_id=order_products.user_id", " order_products.selected_orders=0")->result_array();
        $data["user_info"] = $user_data;
        $this->load->view("admin_panel/add_to_wish_list", $data);
    }

    public function getMyCartOrders($user_id) {
        $data = array();
        $data["title"] = "BirthdayOwl";
        $user_data = $this->Birthday->getSelectData("order_products.order_pro_id,order_products.user_id,order_products.payment_id,order_products.greeting_id,order_products.selected_amount,order_products.voucher_pro_name,order_products.voucher_img,order_products.fname,order_products.femail,order_products.fphone", "order_products", " user_id =$user_id and selected_orders=0")->result_array();
        $data["user_info"] = $user_data;
        $this->load->view('admin_panel/get_mycartorderinfo', $data);
    }

    public function refunded_orders() {
        $order_pro_id = check_post($this->input->post("order_pro_id"));
        $order_data = $this->Birthday->getJoinedData2("order_products.unique_oid,order_products.fname,order_products.femail,order_products.selected_amount,payment.total_amount,order_products.voucher_pro_name,order_products.uname,order_products.uemail,payment.payuId,payment.payment_mode", "order_products", "payment", "order_products.payment_id=payment.payment_id", "order_products.order_pro_id=$order_pro_id ")->row_array();
        //$order_data = $this->Birthday->getSelectData("unique_oid,selected_amount,voucher_pro_name,uname,uemail", "order_products", "order_pro_id=$order_pro_id")->result_array();
        $PayuMoney_BASE_URL = REFUND_URL;
        $postData = array();
        $postData['merchantKey'] = MERCHANT_KEY;
        $postData['paymentId'] = $payuId = $order_data['payuId'];
        $postData['refundAmount'] = $total_amount = $order_data['total_amount'];
        $postNow = http_build_query($postData);
        $postUrl = $PayuMoney_BASE_URL . $postNow;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, TRUE);
        curl_setopt($ch, CURLOPT_URL, $postUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (SERVER_TYPE == '3') {
            $header = array(
                'Authorization: BzWNoiHfG7YJa43w6yDLA3H+xaktjPO9ezOAn9OiRKw=' //PUT YOUR Authorization HERE
            );
        } else {
            $header = array(
                'Authorization: CFz6T+hpiBAWWU2plKXYlmCDaGzKO5KN+K9mHAHwugQ=' //PUT YOUR Authorization HERE
            );
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $out = curl_exec($ch);
        $out = json_decode(trim($out), true);
        $status = $out["status"];
        if ($status == 0) {
            $data["delivery_status"] = "10";
            $where = "order_pro_id='$order_pro_id'";
            $this->Birthday->update("order_products", $data, $where);
            $output["success"] = true;
            $output["message"] = $out["message"];
            if ($out["message"] == "Refund Initiated") {
                require_once(APPPATH . "libraries/Mailin.php" );
                $postData["payment_mode"] = $order_data["payment_mode"];
                $mailin = new Mailin('https://api.sendinblue.com/v2.0', 'tsyShZ5wg1Ix6d09');
                $view = $this->load->view("email_template/refund_success", $postData, true);
                $name = $order_data['uname'];
                $email = $order_data['uemail'];
                $sdata = array("to" => array("$email" => "$name"),
                    "from" => array("refund@birthdayowl.com", "birthdayowl.com"),
                    "subject" => "Refund of payment $payuId with BirthdayOwl of amount $total_amount ",
                    "text" => "$view",
                    "html" => "$view",
                );
                $mailin->send_email($sdata);
            }
        } else {
            $output["success"] = false;
            $output["message"] = $out["message"];
        }



        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function show_refunded_orders() {

        $order_data = $this->Birthday->getSelectData("unique_oid,selected_amount,voucher_pro_name,uname,uemail", "order_products", "delivery_status=10")->result_array();
        $orders["orders"] = $order_data;
        $this->load->view("admin_panel/refunded_orders", $orders);
    }

    public function seo() {
        $data = array();
        $data['menus'] = $this->Birthday->getAllData("seo")->result_array();
        // $data["action"] = "Product_voucher/voucherCategory_edit_success/$contact_id";
        $this->load->view('admin_panel/seo', $data);
    }

    public function add_seo() {
        if (count($_POST) > 0) {
            $data["title"] = check_post($this->input->post("title"));
            $data["description"] = check_post($this->input->post("description"));
            $data["keywords"] = check_post($this->input->post("keywords"));
            $cid = check_post($this->input->post("menu_id"));
            $where = "id='$cid'";
            $this->Birthday->update("seo", $data, $where);
            $output["success"] = true;
            $output["message"] = "Updated Successfully";
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    public function logged_in() {
        if ($this->session->userdata('userdata') != NULL) {
            return true;
        } else {
            redirect(BASEURL);
        }
    }

    public function check_logged_in() {
        if ($this->session->userdata('userdata') != NULL && $this->session->userdata['userdata']['is_guest'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function check_guest_logged_in() {
        if ($this->session->userdata('userdata') != NULL && $this->session->userdata['userdata']['is_guest'] == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(BASEURL);
    }

    public function CommonDelete() {
        if (count($_POST) > 0) {
            $id = check_post($this->input->post("id"));
            $from = check_post($this->input->post("from")); //users:1
            $count = count($id);
            switch ($from) {
                case "1": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "user_id='$id[$i]'";
                            $this->Birthday->delete_reminder("users", $where);
                        }
                        $output["message"] = "User Deleted SuccessFully";
                    }
                    break;
                case "2": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "id='$id[$i]'";
                            $this->Birthday->delete_reminder("news_letter", $where);
                        }
                        $output["message"] = "Newsletter Deleted SuccessFully";
                    }
                    break;
                case "3": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "brand_id='$id[$i]'";
                            $this->Birthday->delete_reminder("brands", $where);
                        }
                        $output["message"] = "Brand Deleted SuccessFully";
                    }
                    break;
                case "4": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "menu_id='$id[$i]'";
                            $this->Birthday->delete_reminder("menu", $where);
                        }
                        $output["message"] = "Menu Deleted SuccessFully";
                    }
                    break;
                case "5": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "id='$id[$i]'";
                            $this->Birthday->delete_reminder("membership_plans", $where);
                        }
                        $output["message"] = "Membership Plan Deleted Successfully";
                    }
                    break;
                case "6": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "category_id='$id[$i]'";
                            $this->Birthday->delete_reminder("categories", $where);
                        }
                        $output["message"] = "Category Deleted SuccessFully";
                    }
                    break;
                case "7": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "category_id='$id[$i]'";
                            $this->Birthday->delete_reminder("categories", $where);
                        }
                        $output["message"] = "Sub-Category Deleted SuccessFully";
                    }
                    break;
                case "8": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "relation_id='$id[$i]'";
                            $this->Birthday->delete_reminder("relationship", $where);
                        }
                        $output["message"] = "Relationship Deleted SuccessFully";
                    }
                    break;

                case "9": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "id='$id[$i]'";
                            $this->Birthday->delete_reminder("contact_us", $where);
                        }
                        $output["success"] = true;
                        $output["message"] = "Contact info Deleted Successfully";
                    }
                    break;
                case "10": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "occup_id='$id[$i]'";
                            $this->Birthday->delete_reminder("occupation", $where);
                        }
                        $output["success"] = true;
                        $output["message"] = "Occupation Deleted SuccessFully";
                    }
                    break;
                case "11": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "fid='$id[$i]'";
                            $this->Birthday->delete_reminder("faq", $where);
                        }
                        $output["message"] = "FAQ Deleted Successfully";
                    }
                    break;
                case "12": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "id='$id[$i]'";
                            $this->Birthday->delete_reminder("testimonial", $where);
                        }
                        $output["message"] = "testimonial Deleted Successfully";
                    }
                    break;
                case "13": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "pro_id='$id[$i]'";
                            $this->Birthday->delete_reminder("product", $where);
                        }
                        $output["success"] = true;
                        $output["message"] = "Product Deleted SuccessFully";
                    }
                    break;
                case "14": {
                        for ($i = 0; $i < $count; $i++) {
                            $where = "fb_id='$id[$i]'";
                            $this->Birthday->delete_reminder("festival_banners", $where);
                        }
                        $output["message"] = "Banner Deleted SuccessFully";
                    }
                    break;
            }


            $output["success"] = true;

            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

}
