<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PayUMoney extends CI_Controller {

    public function index() {
        $data = array();
        // Merchant key here as provided by Payu
        $data["MERCHANT_KEY"] = "d48dWpmr";
// Merchant Salt as provided by Payu
        $SALT = "g2axUToKIf";
// End point - change to https://secure.payu.in for LIVE mode
        $PAYU_BASE_URL = "https://test.payu.in";
        $data["action"] = "";
        $data["hash"] = "";
        $data["amount"] = "10";
        $data["firstname"] = "swati";
        $data["email"] = "sbhuvad@techathalon.com";
        $data["productinfo"] = "bday";
        $data["phone"] = "8082335582";
        $data["surl"] = "http://192.168.1.139/BirthdayOwl/PayUMoney/success/";
        $data["furl"] = "http://192.168.1.139/BirthdayOwl/PayUMoney/error/";
        $data["udf2"] = "1";

        $posted = array();
        if (!empty($_POST)) {

            foreach ($_POST as $key => $value) {
                $posted[$key] = $value;
                $data[$key] = $value;
            }
        }
        $data["formError"] = 0;
        if (empty($posted['txnid'])) {
            // Generate random transaction id
            $data["txnid"] = $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
            $data["txnid"] = $txnid = $posted['txnid'];
        }

// Hash Sequence
        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if (empty($posted['hash']) && sizeof($posted) > 0) {
            if (empty($posted['key']) || empty($posted['txnid']) || empty($posted['amount']) || empty($posted['firstname']) || empty($posted['email']) || empty($posted['phone']) || empty($posted['productinfo']) || empty($posted['surl']) || empty($posted['furl']) || empty($posted['service_provider'])) {
                $data["formError"] = 1;
            } else {
                $hashVarsSeq = explode('|', $hashSequence);
                $hash_string = '';
                foreach ($hashVarsSeq as $hash_var) {
                    $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                    $hash_string .= '|';
                }
                $hash_string .= $SALT;
                $data["hash"] = $hash = strtolower(hash('sha512', $hash_string));
                $data["action"] = $PAYU_BASE_URL . '/_payment';
            }
        } elseif (!empty($posted['hash'])) {
            $data["hash"] = $posted['hash'];
            $data["action"] = $PAYU_BASE_URL . '/_payment';
        }
        $this->load->view("PayUMoney", $data);
    }

    public function success() {

        $status = $_POST["status"];
        $firstname = $_POST["firstname"];
        $amount = $_POST["amount"];
        $txnid = $_POST["txnid"];
        $posted_hash = $_POST["hash"];
        $key = $_POST["key"];
        $productinfo = $_POST["productinfo"];
        $email = $_POST["email"];
        $udf2 = $_POST["udf2"];
            $udf1 = $_POST["udf1"];
        $salt = "g2axUToKIf";
        echo $posted_hash . "\n";
        If (isset($_POST["additionalCharges"])) {
            $additionalCharges = $_POST["additionalCharges"];
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
            //$retHashSeq = $additionalCharges . '|' . $salt . '|' . $status .'||'. $udf2.'|||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        } else {
            $retHashSeq = $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

            // $retHashSeq = $salt . '|' . $status .'||'. $udf2. '|||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        }
        $hash = hash("sha512", $retHashSeq);

        if ($hash != $posted_hash) {
            echo "Invalid Transaction. Please try again";
        } else {
            echo "<h3>Thank You. Your order status is " . $status . ".</h3>";
            echo "<h4>Your Transaction ID for this transaction is " . $txnid . ".</h4>";
            echo "<h4>We have received a payment of Rs. " . $amount . ". Your order will soon be shipped.</h4>";
        }
    }

    public function error() {
        $status = $_POST["status"];
        $firstname = $_POST["firstname"];
        $amount = $_POST["amount"];
        $txnid = $_POST["txnid"];
        $posted_hash = $_POST["hash"];
        $key = $_POST["key"];
        $productinfo = $_POST["productinfo"];
        $email = $_POST["email"];
        $salt = "g2axUToKIf";
        If (isset($_POST["additionalCharges"])) {
            $additionalCharges = $_POST["additionalCharges"];
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        } else {
            $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        }
        $hash = hash("sha512", $retHashSeq);

        if ($hash != $posted_hash) {
            echo "Invalid Transaction. Please try again";
        } else {
            echo "<h3>Your order status is " . $status . ".</h3>";
            echo "<h4>Your transaction id for this transaction is " . $txnid . ". You may try making the payment by clicking the link below.</h4>";
        }
    }

}
