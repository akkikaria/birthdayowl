<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="p:domain_verify" content="fc8ec52fb30348ffaffb7084f0375c68"/>
        <?php if (array_key_exists("description", $seo)) { ?>
            <meta name="description" content="<?php echo $seo['description']; ?>" />
        <?php } else { ?>
            <meta name="description" content="BirthdayOwl" id="mdesc"/>
        <?php } ?>
        <?php if (array_key_exists("keywords", $seo)) { ?>
            <meta name="keywords" content="<?php echo $seo['keywords']; ?>" />
        <?php } else { ?>
            <meta name="description" content="BirthdayOwl" />
        <?php } ?>

        <?php if (array_key_exists("title", $seo)) { ?>
            <title><?php echo $seo["title"]; ?></title>
        <?php } else { ?>
            <title>BirthdayOwl</title>
        <?php } ?>

        <!--=======================================NEW============================================-->

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="p:domain_verify" content="fc8ec52fb30348ffaffb7084f0375c68"/>

        <meta name="google-signin-client_id" content="600697078610-f9lv1aa39q12e2rrut87pfa7eqscvgbi.apps.googleusercontent.com"/>

        <link rel="canonical" href="<?php echo "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" />
        <link rel="alternate" href="<?php echo "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" hreflang="en-IN" />

        <link rel="shortcut icon" href="<?php echo BASEURL_OIMG; ?>main_icn.png" type="image/png" />

        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>style.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>grid.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>jquery-ui.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo BASEURL_OCSS; ?>my_style.css"  type="text/css"/>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>bootstrap.min.css" type="text/css"/>
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>buttons.css" type="text/css"/>
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>owl.carousel.min.css" type="text/css"/>
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>owl.theme.default.css" type="text/css"/>

        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>nouislider.css" type="text/css"/>
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>nouislider.pips.css" type="text/css"/>

        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>bootstrap-datepicker.min.css" type="text/css"/>
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>default.css" type="text/css"/>
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>default.date.css" type="text/css"/>

        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>easy-autocomplete.min.css" type="text/css"/>
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>colorbox.css" type="text/css"/>
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>home-style.css" type="text/css"/>
        <!--=======================================NEW============================================-->

        <link href="<?PHP echo BASEURL_OCSS; ?>style_1.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>default.css" />
        <link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>default.date.css" />

        <link href="<?PHP echo BASEURL_OCSS; ?>style_popup.css" rel="stylesheet" type="text/css"/>


        <script src="<?PHP echo BASEURL_OJS; ?>jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <!--for google login-->
        <script src="https://apis.google.com/js/platform.js" async defer></script>

        <!--Social Meta tags-->
        <!--<meta property="fb:admins" content="birthdayowl"/>
        <!-- <meta property="og:Title" content="Set FREE Birthday Reminder and Send FREE Birthday Greeting Cards"/>-->
        <!--<meta property="og:type" content="image"/>-->
        <!--<meta property="og:url" content="https://www.birthdayowl.com/"/>-->
        <!--<meta property="og:image" content="https://www.birthdayowl.com/public/cropped_greetings/Birthday_greeting_cards.jpg"/>-->
        <!-- <meta property="og:description" content="Share this with your loved ones and tell then how much you care them"/>-->
        <!-- <meta property="og:site_name" content="Birthdayowl"/>-->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-123408772-1"></script>
        <script>
//    $(function() {
//        $(this).bind("contextmenu", function(e) {
//            e.preventDefault();
//            alert("Sorry! You can't right click.")
//        });
//    }); 
        </script>
        <script>


            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-123408772-1');
        </script>

        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s)
            {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1851804038212254');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1851804038212254&ev=PageView&noscript=1"
                       /></noscript>
        <!-- End Facebook Pixel Code -->


    </head>
    <!--=======================================NEW============================================-->
    <body class="bg">

        <div class="showloader" style="display:none">
            <div class="loader_div">
                <img src="<?php echo BASEURL_OIMG; ?>89.gif" alt="Loading" class="ajax-loader " />
            </div>
        </div>

        <div id="internet-box" class="internet-popup" style="background-color: #000000;cursor: pointer;height: 45px;margin: auto;padding: 7px;position: fixed;text-align: center;transition: all 1s ease 0s;width: 100%;z-index: 2;top:0;color: white;font-size:1.1em !important;font-family:Helvetica Neue ,Helvetica,Arial,sans-serif !important;font-weight: 300 !important;"></div>
        <!-- Top Menu -->
        <div class="container-fluid top-navbar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 top-left-con">
                        <ul class="top-left">
                            <?php if ($this->session->userdata('userdata') == NULL) { ?>
                                <li id="howitworksmodal" data-link="How it works"><a href="#howitworks" class="inline_colorbox cboxElement">How it works</a></li>
                                <li data-link="Sign Up"><a href="#SignUp" class="inline_colorbox cboxElement">Sign Up</a></li>
                                <li data-link="Login"><a href="#Login" class="inline_colorbox cboxElement" data-width="">Login</a></li>
                                <!--<li><a href="#">Membership</a></li>-->
                            <?php } else if ($this->session->userdata["userdata"]['is_guest'] == 1 && ($this->session->userdata('userdata') != NULL)) { ?>
                                <li id="howitworksmodal" data-link="How it works"><a href="#howitworks" class="inline_colorbox cboxElement">How it works</a></li>
                                <li data-link="Sign Up"><a href="#SignUp" class="inline_colorbox cboxElement">Sign Up</a></li>
                                <!--<li data-link="Login"><a href="#Login" class="inline_colorbox cboxElement" data-width="">Login</a></li>-->
                                <li data-link="Logout" onclick="signOut()"><a href="<?php echo BASEURL; ?>logout" class="i">Logout As Guest</a></li>
                            <?php } else if ($this->session->has_userdata('is_user_google_login') && ($this->session->userdata('userdata') != NULL)) { ?>
                                <li id="howitworksmodal" data-link="How it works"><a href="#howitworks" class="inline_colorbox cboxElement">How it works</a></li>
                                <li data-link="Sign Up"><a href="#SignUp" class="inline_colorbox cboxElement">Sign Up</a></li>
                                <!--<li data-link="Login"><a href="#Login" class="inline_colorbox cboxElement" data-width="">Login</a></li>-->
                                <li data-link="Logout" onclick="signOut()"><a href="<?php echo BASEURL; ?>logout" class="i">Logout From Google</a></li>
                            <?php } else if (($this->session->userdata('userdata') != NULL) && (!$this->session->has_userdata('is_user_google_login')) && (!$this->session->userdata["userdata"]['is_guest'] == 1)) { ?>
                                <li id="howitworksmodal" data-link="How it works"><a href="#howitworks" class="inline_colorbox cboxElement">How it works</a></li>
                                <li data-link="Welcome User"><a href="<?php echo WEB_DASHBOARD; ?>" class="">Welcome <?php echo ucfirst($this->session->userdata['userdata']['first_name']); ?> </a></li>
                                <li data-link="My Account"><a href="<?php echo WEB_DASHBOARD; ?>" class="">My Account</a></li>
                                <li data-link="Logout" onclick="signOut()"><a href="<?php echo BASEURL; ?>logout" class="i">Logout</a></li>
                                <!--<li><a href="#">Membership</a></li>-->
                            <?php } ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>	
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <ul class="top-right">
                            <?php $contact_us = get_contact_info(); ?>
                            <?php if ($contact_us["phone_no"] != "") { ?>
                                <li><a><img src="<?PHP echo BASEURL_ONEWIMG ?>phone.png" class="img-responsive top-head-img">+<?php echo $contact_us["phone_no"]; ?></a></li>
                            <?php } ?>

                            <?php if ($contact_us["email"] != "") { ?>
                                <li><img src="<?PHP echo BASEURL_ONEWIMG ?>mail.png" class="img-responsive top-head-img" /><a style="" href="mailto:<?php echo $contact_us["email"]; ?>"><?php echo $contact_us["email"]; ?></a></li>
                            <?php } ?>
                            <li data-link="Cart" class="menu-top-cart" id="displaycart">
                                <a id='view_Cart' class="view_cart clickcart" style="cursor:pointer;">
                                    Cart
                                    <img style="padding: 0px;" src="<?PHP echo BASEURL_ONEWIMG ?>cart.png" class="img-responsive top-head-img" />
                                    <span class="icon_cart"></span>
                                    <small>
                                        <cite class="button__badge ccount"></cite>
                                    </small>
                                </a>
                                <div class="cartcontainer" >
                                    <div class="shopping-cart">
                                        <ol class="shopping-cart-items" id="shopingitems" style="display:inline-block;text-align: center;list-style-type: decimal">
                                        </ol>
                                        <div  style="display:block;width:100%;height: 55px;padding-top: 6px;">
                                            <div style="float:left;text-align: center;width: 50%;line-height: 46px;">
                                                <span >Total:</span>
                                                <span id="stotalamt"></span>
                                            </div>
                                            <div style="float:right;text-align: center;width: 50%"> 
                                                <div class="arrowbtn" id="mycartcheckout" style="padding-top: 13px; padding-bottom: 9px; padding-right: 53px;">Checkout</div></div>
                                        </div> 
                                    </div> <!--end shopping-cart -->
                                </div>
                            </li>
                        </ul>

                    </div>
                    <div class="clearfix"></div>	
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <a href="#Login" id="mlogin_popup" class="inline_colorbox memlogin" ></a>

        <div id="hpopup">  </div>
        <div class="inner_box" >
            <div class="hclose" id='hclosepopup'>
                <img src="<?php echo BASEURL_OIMG; ?>closeh.png"  style="width:75%;"/>
            </div>
            <div>
                <img src="<?php echo BASEURL_OIMG ?>steps.jpg" alt="Loading"  class="ahow " />
            </div>
        </div>

        <!-- Main Menu -->
        <nav class="navbar navbar-static-top">
            <div class="container">
                <div class="navigation-container">
                    <div class="navbar-header col-xs-12 col-md-3">

                        <a class="navbar-brand logo" href="<?php echo WEB_HOME; ?>">
                            <img src="<?PHP echo BASEURL_ONEWIMG ?>logo.png" class="img-responsive" alt="Logo">
                                <span class="clearfix"></span>
                        </a>

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                    </div>

                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav main-menu col-xs-12 col-md-12">
                            <li class="active"><a href="<?php echo WEB_HOME; ?>">Home</a></li>
                            <!--<li><a href="<?php echo WEB_EGIFTCARD_VOUCHERS; ?>">e-Gift Cards</a></li>-->
                            <li><a href="<?php echo WEB_EGIFTCARD_VOUCHERS; ?>">Printed Cards</a></li>
                            <li><a href="<?php echo WEB_FREE_GREETING_CARDS; ?>">Free Greeting Cards</a></li>
                            <li><a href="<?php echo WEB_FLOWERS_AND_CAKES; ?>">Flowers &amp; Cakes</a></li>
                            <li><a href="<?php echo WEB_ZODIAC_SIGNS; ?>">Zodiac Signs</a></li>
                            <?php // if (($this->session->userdata('userdata') != NULL) && ($this->session->userdata['userdata']['is_guest'] == 0)) { ?>
                                <!--<li><a href="<?php // echo BASEURL;    ?>addReminder/0" class="memlogin">Birthday Reminders</a></li>-->
                            <?php // } else { ?>
                            <li><a href="<?php echo WEB_BIRTHDAY_REMINDER_WITHOUT_LOGIN; ?>" class="">Birthday Reminders</a></li>
                            <!--<li><a href="#Login" class="bday_reminderpopup inline_colorbox cboxElement">Birthday Reminders</a></li>-->
                            <?php // } ?>
                            <li><input placeholder="search here" id="searchval" class="" style="padding: 8px 10px; border-radius: 5px; border: 1px solid #ccc; width: 158px; margin-left: 0px;" type="text"> <i class="fa fa-search" style="margin-left: 5px;"></i></li>
                        </ul>

                    </div><!--/.nav-collapse -->

                </div>

            </div>
        </nav>	

        <!--=======================================NEW============================================-->