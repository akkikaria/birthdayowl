<!--lightbox-->
<?php
//echo '<pre>';
//print_r($this->session->all_userdata());
//exit;
?>
<style>
    /*for removing select arrow in dropdown of phone number*/
    select {
        /* for Firefox */
        -moz-appearance: none;
        /* for Chrome */
        -webkit-appearance: none;
    }
    video {
        width: 100%;
        height: auto;
    }
</style>
<a href="#forgetpass" class="inline_colorbox" id="forgotpopup"></a>
<!--yes no popup-->

<div class="cd-popup cd-popup-un" role="alert">
    <div class="cd-popup-container" style="color: black;">
        <p>Are you sure, you want to Unsubscribe ?
            <br/>
        <ul class="cd-buttons">
            <li><a id="yes_un" style="cursor:pointer;" >Yes</a></li>
            <li><a id="no_un" style="cursor:pointer;" >No</a></li>
        </ul>
        <!--        <a href="#0" class="cd-popup-close img-replace">Close</a>-->
    </div>
</div>
<div class="lb_con">
    <div id="howitworks">
        <!--<img src="<?php // echo BASEURL_OIMG ?>steps.jpg" alt="How it works" class="img-responsive">-->

        <video width="900" height="371" controls>
            <source src="<?php echo BASEURL_VIDEO ?>BirthdayBoy.mp4">                                                                                                        ?>BirthdayBoy.mp4" type="video/mp4">
        </video>

        <!--
                <iframe width="900" height="371" src="https://youtube/01_4MUazGMk">
                </iframe>-->

<!--        <iframe id="youtube" width="900" height="371" src="https://www.youtube.com/embed/01_4MUazGMk">
        </iframe>-->
    </div>
    <div id="forgetpass">
        <div id="recerror" style="display:none;color: red;text-align: center;font-weight: bold;"></div>
        <div class="forgetpass" id="otpresetDiv" style="display:none;">
            <div class="heading_1 ">
                <span>Enter  OTP</span>
            </div>
            <ul class="Signin">
                <li>
                    <input type="text" placeholder="OTP"  id="verifyOtpVal" style="padding-left: 30px;"/>
                </li>
                <li>
                    <input type="submit" value="submit"  id="verifyresetrequest"/>
                </li>
                <li>
                    <input type="submit" value="Resend OTP" id="resent_otp" />
                </li>
                <li>
                    <input type="submit" value="Change Mobile Number"  id="change_mb_no" />
                </li>
            </ul>
        </div>
        <div class="forgetpass" id="forgetpassDiv" >
            <p>
                Enter your email address to recieve instruction
                <br />
                on how to change your password
            </p>
            <div class="heading_1 ">
                <span>Request a new Password </span>
            </div>
            <ul class="Signin">
                <li data-icon="&#xf003;">
                    <input type="email" placeholder="Email Address"   id="femail"/>
                </li>
                <li>
                    <input type="submit" value="submit" id="forgot" class="forgetpass"/>
                </li>
            </ul>
            <div style="padding:4px;">OR</div>
            <div class="heading_1 ">
                <span>Request  OTP</span>
            </div>
            <ul class="Signin">
                <li data-icon="&#xf10b;">
                    <input type="text" placeholder="Mobile Number" id="otp" style="padding-left: 30px;"/>
                </li>
                <li>
                    <input type="submit" value="submit" id="otpsubmit" class="otpsubmit"/>
                </li>
            </ul>
        </div>
        <div class="forgetpass" id="verifyMobileDiv" style="display:none;">
            <div class="heading_1">
                <span>Enter OTP</span>
            </div>
            <ul class="Signin">
                <li>
                    <input type="text" placeholder="OTP" id="verifyOtpValMobile" style="padding-left: 30px;"/>
                </li>
                <li>
                    <input type="submit" value="submit" id="verifyOtpMobile"/>
                </li>
                <li>
                    <input type="submit" value="Resend OTP" id="resendOtpMobile" />
                </li>
            </ul>
        </div>
    </div>
    <div id="SignUp">
        <ul class="lbcon wrapper">
            <li class="full">
                <img src="<?PHP echo BASEURL_OIMG ?>bdaysignup.jpg" />
            </li>
            <li class="full">Gender :
                <label>
                    <input type="radio" name="Gender" value="1" id="Gender_0" checked="true" />
                    Male</label> &nbsp;
                <label>
                    <input type="radio" name="Gender" value="2" id="Gender_1" />
                    Female</label>
                </p> </li>
            <li data-icon="&#xf007;">
                <input type="text" placeholder="Your Name *" id="first_name" name="first_name" size="2" />
            </li>
            <li data-icon="&#xf003;">
                <input type="email" placeholder="Email address *" id="sign_email" name="email" />
            </li>
            <li data-icon="&#xf023;">
                <input type="password" placeholder="Password *" id="sign_password" name="password"/>
            </li>
            <li data-icon="&#xf023;">
                <input type="password" placeholder="Confirm Password *" id="conf_password" name="conf_password"/>
            </li>
            <li>
                <div style="width: 100%;display: flex;">
                    <select id="dobday" class="sbdate" name="sbdate"></select>
                    <select id="dobmonth" class="sbmonth" name="sbmonth"></select>
                    <select id="dobyear" class="sbyear" name="sbyear"></select>
                </div>
                <div style="text-align: left; font-weight: 600;">Note: Enter date of birth above.</div>
            </li>
            <li data-icon="&#xf10b;">
                <div style="width: 100%;display: flex;">
                    <select id="CmbCountryOption" name="nationality_id" class="countrylist" disabled>
                                               <!--<option value=""><?php // echo $country_name . " (+" . $nationality_code . ")"; ?></option>-->
                        <?php // $countryList = get_countryinfo(); ?>
                        <?php // foreach ($countryList as $country) { ?>
                                                                      <!--<option value="<?php // echo $country["country_id"];            ?>"><?php // echo $country['country_name'] . " (+" . $country["nationality_code"] . ")";            ?></option>-->
                        <?php // } ?>
                        <?php $country = get_country_info(); ?>
                        <option value="<?php echo $country["country_id"]; ?>"> <?php echo $country['country_name'] . " (+" . $country["nationality_code"] . ")"; ?></option>
                    </select>
                    <input type="text" placeholder="Mobile Number *"   id="mobile_no" name="mobile_no" onkeypress="return isNumberKey(event)"  class="mbno" />
                </div>
                <div style="">&nbsp;</div>
            </li>

            <li class="full text-center">
                <div class="signerror" style=" height: 20px; color: red; font-weight: bold;text-align: center;"></div>
                <input type="submit" value="Sign Up"  id="submit" redirect="0" />
            </li>

            <li class="full text-center">
                <div style="text-align: center;color:red ;width: 100%" >
                    Note : We will use sms / whatsapp / email to send notifications of your sent greetings/gifts.
                    <br/>
                    We recommended to input both email and mobile info, although you may choose to input either one.
                </div>
            </li>
        </ul>
    </div>
    <div id="Login">
        <!-- Login Modal -->
        <div class = "loginbox" id="loginbox">

            <div class = "heading_1 grey">
                <span>Log In to your Account</span>
            </div>
            <ul class = "Signin">
                <li data-icon = "&#xf003;">
                    <input type = "email" placeholder = "Email Address " name = "email" id = "lemail" login_type = '0' class = "login_input"/>
                </li>
                <li data-icon = "&#xf023;">
                    <input type = "password" placeholder = "Password" name = "password" id = "lpassword" class = "login_input"/>
                </li>
                <li>
                    <input type = "submit" value = "Sign in" id = "login" />
                </li>
                <li>
                    <a class="vgoogleLogin g-signin2 social_signin" onclick="ClickLogin()" data-onsuccess="onSignIn" style="cursor: pointer;" red_type="33">
                    </a>
                </li>
            </ul>
            <ul class = "Forgot_password">
                <li><a href = "#forgetpass" class = "inline_colorbox" data-width = "30%" id = "fpasslink">Forgot password?</a></li>
                <li><a href = "#SignUp" data-width = "70%" class ="inline_colorbox">New here? Sign Up</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>

    <div id="Login_birthdayowl_user_flower_cake">
        <!--Login Modal-->
        <div class = "loginbox" id="loginbox">
            <div class = "heading_1 grey">
                <span>You Are Already Registered User</span>
            </div>
            <ul class = "Signin">
                <li data-icon = "&#xf003;">
                    <input type = "email" placeholder = "Email Address " name = "email" id = "lemail_flower_cake" login_type = '0' required="" class = "login_input"/>
                </li>
                <li data-icon = "&#xf023;">
                    <input type = "password" placeholder = "Password" name = "password" id = "lpassword_flower_cake" required="" class = "login_input"/>
                </li>
                <li>
                    <input type = "submit" value = "Sign in" id = "login_guest_flower_cake" />
                </li>
            </ul>
            <ul class = "Forgot_password">
                <li><a href = "#forgetpass" class = "inline_colorbox" data-width = "30%" id = "fpasslink_login">Forgot password?</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>

    <div id="Login_birthdayowl_user_voucher">
        <!--Login Modal-->
        <div class = "loginbox" id="loginbox">
            <div class = "heading_1 grey">
                <span>You Are Already Registered User</span>
            </div>
            <ul class = "Signin">
                <li data-icon = "&#xf003;">
                    <input type = "email" placeholder = "Email Address " name = "email" id = "lemail_voucher" login_type = '0' required="" class = "login_input"/>
                </li>
                <li data-icon = "&#xf023;">
                    <input type = "password" placeholder = "Password" name = "password" id = "lpassword_voucher" required="" class = "login_input"/>
                </li>
                <li>
                    <input type = "submit" value = "Sign in" id = "login_guest_voucher" />
                </li>
            </ul>
            <ul class = "Forgot_password">
                <li><a href = "#forgetpass" class = "inline_colorbox" data-width = "30%" id = "fpasslink_login">Forgot password?</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>

    <div id="Login_Reminder" style="">
        <!-- Login Modal -->
        <div class = "loginbox" id="loginbox">

            <div class = "heading_1 grey">
                <span>Log In to your Account</span>
            </div>
            <ul class = "Signin">
                <li data-icon = "&#xf003;">
                    <input type = "email" placeholder = "Email Address " name = "email" id ="lemail_rem" login_type = '0' class="login_input"/>
                </li>
                <li data-icon = "&#xf023;">
                    <input type = "password" placeholder = "Password" name = "password" id = "lpassword_rem" class = "login_input"/>
                </li>
                <li>
                    <input type = "submit" value = "Sign in" id = "login_reminder" />
                </li>
                <li>
                    <a class="vgoogleLogin g-signin2 social_signin" onclick="ClickLogin()" data-onsuccess="onSignIn" style="cursor: pointer;" red_type="33">
                    </a>
                </li>
            </ul>
            <ul class = "Forgot_password">
                <li><a href = "#forgetpass" class = "inline_colorbox" data-width = "30%" id = "fpasslink">Forgot password?</a></li>
                <li><a href = "#SignUp" data-width = "70%" class ="inline_colorbox">New here? Sign Up</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
    <div id="openpopup" class="llbox">
        <div class="heading_1 ">
            <span>Log In to your Account</span>
        </div>
        <ul class="Signin">
            <li data-icon="&#xf003;">
                <input type="email" placeholder="Email Address " name="email" id="remail" class="reminderLogin" /></li>
            <input type="hidden" name="login_type" value="0" id="login_type">
            <li data-icon="&#xf023;">
                <input type="password" placeholder="Password" name="password" id="rpassword" class="reminderLogin" /></li>
            <li>
                <input type="submit" value="Sign in" id="reminderlogin" class="change_id" voucher_url=''/></li>
        </ul>
        <ul class="Forgot_password">
            <li><a href="#forgetpass" class="inline_colorbox" data-width="30%">Forgot password?</a></li>
            <li><a href="#SignUp" class="inline_colorbox" data-width="50%">New here? Sign Up</a></li>
        </ul>
        <ul>
            <li><a class="facebookLogin" style="cursor: pointer;" red_type="11" >
                    <img src="<?PHP echo BASEURL_OIMG; ?>sign-facebook.png" class="social_footer" /></a></li>
            <li><a class="googleLogin" style="cursor: pointer;" red_type="11">
                    <img src="<?PHP echo BASEURL_OIMG; ?>sign-plus.png" class="social_footer" /></a></li>
        </ul>
    </div>

    <div class="loginsuccessbox">
        <div id="login_success" style="color:#51AB03;font-weight: bold;">
            Login Successfully!
        </div>
    </div>
    <div class="loginalertBox"></div>
    <div id="pickcard">
        <div class="prd_lb_l">
            <div class="wrapper lblogowrap ">
                <div class="lblogo">
                    <img src="<?PHP echo BASEURL_OIMG ?>logo.png" />
                </div>
                <div class="lblogo_con">
                    <strong class="pro_name"></strong><br />
                    <i>one card redeemable at any of our partners stores</i>
                </div>
            </div>
            <ul class="tlink">
                <li><a rel="locations" class="taburl active" style="cursor: pointer;">Description</a></li>
                <li><a rel="redemption" class="taburl  " style="cursor: pointer;">Terms & Conditions </a></li>

            </ul>
            <div class="tabwrapp" >
                <div class="tabcon" id='redemption' >
                    <ul class="disc" id="redemption_data"  >
                    </ul>
                </div>
                <div class="tabcon" id='locations'>
                    <ul class="disc" id="location_data">
                    </ul>
                </div>
            </div>
        </div>
        <div class="prd_lb_r">
            <h4>
                <span> e-Gift Cards</span>
                <br />
<!--                <div class="amtrange">Rs.<span class="minamt"> </span> - Rs.<span class="maxamt"></span></div>-->
                <div class="amtrange">INR<span class="minamt"> </span></div>

            </h4>
            <div class="wrapper">
                <img src="" id="pro_img" /></div>
            <p>
                One card redeemable at any
                <br />
                of our partner stores</p>
            <div class="wrapper mt10">
                <input type="submit" value="PICK THIS CARD" voucher_url="" class="selected_card" />
            </div>
            <div class="wrapper mt10">
                <h4>
                    Instant Delivery</h4>
                <ul class="con_link">
                    <li><span class="selected_card" style="cursor:pointer"> <img src="<?PHP echo BASEURL_OIMG ?>envolop.jpg" /></span></li>

                    <li><span class="selected_card" style="cursor:pointer"> <img src="<?PHP echo BASEURL_OIMG ?>smm_grey.jpg" /></span></li>
                </ul>
            </div>
            <div class="wrapper mt10">
                <ul class="con_link_bt">
                    <li>  <span class="pro_name"></span><br />
                        Gift Card is an online<br />
                        store</li>
                    <li>Redeemable at any of our
                        <br />
                        stores in India

                </ul>
            </div>
        </div>
    </div>
</div>
<div style="clear: both;"></div>

<footer class="footer">
    <div class="container foot-space">

        <ul class="footer_top col-md-12 col-xs-6 row">
            <li><span>Perfect Gift</span>Lots of Gift Options</li>
            <li><span>Instant Delivery</span>Email, SMS, &amp; Facebook</li>
            <li><span>Secure</span>Visa &amp; MasterCard</li>
        </ul>

        <div class="clearfix"></div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="fb-page"
                 data-href="https://www.facebook.com/birthdayowl/"
                 data-width="340"
                 data-hide-cover="false"
                 data-show-facepile="true">
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 footer-link-sec">
            <h2 class="foot-white-title">Links</h2>
            <!--<p class="foot-subtitle e-gift egiftcards">eGIFT CARDS</p>-->
            <!--<p class="foot-subtitle clickworks">How it works</p>-->
            <a href="<?PHP echo WEB_ABOUT_US; ?>"><p class="foot-subtitle">About Us</p></a>
            <a href="<?PHP echo WEB_PRIVACY_AND_POLICY; ?>"><p class="foot-subtitle">Privacy &amp; Security Policy</p></a>
            <a href="<?PHP echo WEB_FAQS; ?>"><p class="foot-subtitle">FAQ</p></a>
            <!--<p class="foot-subtitle shipping-policy">Shipping Policy</p>-->
            <!--<p class="foot-subtitle refund-policy">Refund Policy</p>-->
            <h2 class="foot-white-title">Terms &amp; Conditions</h2>
            <a href="<?PHP echo WEB_TERMS_OF_USE_EGIFT; ?>"><p class="foot-subtitle">Printed Cards</p></a>
            <a href="<?PHP echo WEB_TERMS_OF_USE_OTHER; ?>"><p class="foot-subtitle">Flowers, Cakes &amp; Gifts</p></a>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="row">

                <div class="col-xs-12 col-sm-6 col-md-12">

                    <h2 class="foot-white-title">CONTACT US</h2>
                    <?php $contact_us = get_contact_info(); ?>
                    <?php if ($contact_us["phone_no"] != "") { ?>
                        <p class="foot-mob"><img src="<?PHP echo BASEURL_ONEWIMG ?>foot-mob.png" class="img-responsive">+<?php echo $contact_us["phone_no"]; ?></p>
                    <?php } ?>
                    <?php if ($contact_us["email"] != "") { ?>
                        <a href="mailto:<?php echo $contact_us["email"]; ?>"><p class="foot-email"><img src="<?PHP echo BASEURL_ONEWIMG ?>foot-mail.png" class="img-responsive"><?php echo $contact_us["email"]; ?></p></a>
                    <?php } ?>
                    <div class="spacer-20"></div>

                </div>

                <div class="col-xs-12 col-sm-6 col-md-12">

                    <h2 class="foot-white-title social-title">Follow us on</h2>
                    <div class="foot-socials">
                        <a href="https://www.instagram.com/birthday_owl_reminder/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="https://www.facebook.com/birthdayowl/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/birthday_owl" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="https://www.pinterest.com/birthdyowl/" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                        <!-- <i class="fa fa-pinterest-p" aria-hidden="true"></i> -->
                    </div>

                </div>

            </div>

        </div>
    </div>
</footer>

<div class="alertPopupDiv2 alertPopupDiv" >
    <div class="alertPopupMessage2 alertPopupMessage">
        <div class="closepopup" id="close_popup"> <img src="<?php echo BASEURL_OIMG; ?>closer.png" /> </div>
        <br/>
        <br/>
        <div style="text-align: center; margin: auto auto; width: 100%; height:120px;line-height: 29px;">
            <div style="font-size: 20px;" id='cmessage'>You're unsubscribed.</div>
            <div style="text-decoration: underline;cursor: pointer;" id='resubscribe'>Re-subscribed?</div>
            <br/>
        </div>
    </div>
</div>



<div class="foot-bottom">Copyright © 2016 BirthdayOwl. All rights Reserved.</div>

<script src="//v2.zopim.com/?4SAHhour1NhEySeFtWQTwbPfRWzpeyxV" charset="utf-8" type="text/javascript"></script>

<script type="text/javascript" src="<?PHP echo BASEURL_OJS ?>jquery-ui.min.js"></script>
<script type="text/javascript" src="<?PHP echo BASEURL_OJS ?>jquery.bxslider.min.js" ></script>

<script src="<?PHP echo BASEURL_OJS ?>nouislider.js" type="text/javascript"></script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>-->

<script src="<?PHP echo BASEURL_OJS ?>bootstrap.min.js"></script>
<script src="<?PHP echo BASEURL_OJS ?>owl.carousel.min.js"></script>

<script type="text/javascript" src="<?PHP echo BASEURL_OJS ?>flowtype.js"></script>
<script src="<?PHP echo BASEURL_OJS ?>jquery.colorbox-min.js"></script>
<script src="<?PHP echo BASEURL_OJS ?>animate.js"></script>

<script src="<?PHP echo BASEURL_OJS ?>picker.js"></script>
<script src="<?PHP echo BASEURL_OJS ?>picker.date.js"></script>
<script src="<?PHP echo BASEURL_OJS ?>legacy.js"></script>

<script src="<?PHP echo BASEURL_OJS ?>jquery.easy-autocomplete.js" type="text/javascript"></script>
<script src="<?PHP echo BASEURL_OJS ?>dobPicker.js" type="text/javascript"></script>

<script src="<?PHP echo BASEURL_OJS ?>main_popup.js" type="text/javascript"></script>
<script type="text/javascript">
                        $("#unsubscribe").click(function () {
                            $("body").css("overflow", "hidden");
                            $('.cd-popup').addClass('is-visible');
                            $('.cd-popup-ck').hide();
                            $('.cd-popup-add').hide();
                        });
                        $(document).on("click", "#yes_un", function (event) {
                            $("body").css("overflow", "auto");
                            var data = {
                                mail_status: 1
                            }
                            $.ajax({
                                type: "POST",
                                url: "<?php echo BASEURL; ?>unsubscribe",
                                data: data,
                                dataType: "json",
                                success: function (r) {
                                    if (r.success.toString() == "false") {
                                        event.preventDefault();
                                        $('.cd-popup').removeClass('is-visible');
                                        $("#cmessage").html(r.message.toString());
                                        $(".alertPopupDiv2").addClass("show_overlay").show();
                                        $('.alertPopupMessage2').animate({top: '34%'}, 500);
                                        $("body").css("overflow", "auto");
                                    } else {
                                        event.preventDefault();
                                        $('.cd-popup').removeClass('is-visible');
                                        $("#resubscribe").show();
                                        $("#cmessage").html("You're unsubscribed!");
                                        $(".alertPopupDiv2").addClass("show_overlay").show();
                                        $('.alertPopupMessage2').animate({top: '34%'}, 500);
                                    }
                                }
                            });
                        });
                        $('#no_un').click(function (event) {
                            $("body").css("overflow", "auto");
                            event.preventDefault();
                            $('.cd-popup').removeClass('is-visible');
                        });
                        $(document).on("click", "#resubscribe", function () {
                            var data = {
                                mail_status: 0
                            }
                            $.ajax({
                                type: "POST",
                                url: "<?php echo BASEURL; ?>unsubscribe",
                                data: data,
                                dataType: "json",
                                success: function (r) {
                                    if (r.success.toString() == "false") {
                                        alert(r.message.toString());
                                    } else {
                                        $("#resubscribe").hide();
                                        $("#cmessage").html("You're subscribed!");
                                    }
                                }
                            });
                        });
                        $(document).on("click", "#close_popup", function () {
                            $('.alertPopupMessage2').animate({top: '0'}, 500);
                            $(".alertPopupDiv2").removeClass("show_overlay").css("display", "none");
                        });
                        //                        var otp_reset = 0;
                        //                        if (otp_reset == 1) {
                        //                            window.setInterval(function () {
                        //                                $("#resendOtpMobile").removeAttr("disabled");
                        //                            }, 30000);
                        //                        }

                        //                        window.onload = function () {
                        //                            window.setTimeout(setDisabled, 10000);
                        //                        }
                        //                        function setDisabled() {
                        ////                            $("button").removeAttr("disabled");
                        //                            document.getElementById('resendOtpMobile').disabled = false;
                        //                        }
                        $('#fpasslink').click(function () {
                            $('#forgotpopup').click();
                            $("#otpresetDiv").hide();
                            $("#forgetpassDiv").show();
                            $("#verifyMobileDiv").hide();
                        });
                        $(document).ready(function () {
                            $.dobPicker({
                                daySelector: '#dobday', /* Required */
                                monthSelector: '#dobmonth', /* Required */
                                yearSelector: '#dobyear', /* Required */
                                //                                dayDefault: 'Day', /* Optional */
                                //                                monthDefault: 'Month', /* Optional */
                                //                                yearDefault: 'Year', /* Optional */
                                minimumAge: 8, /* Optional */
                                maximumAge: 60 /* Optional */
                            });
                        });
                        (function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id))
                                return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11';
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript">
    $('.about-us').click(function () {
        window.location = "<?php echo WEB_ABOUT_US; ?>";
    });
    $('.privacy-policy').click(function () {
        window.location = "<?php echo WEB_PRIVACY_AND_POLICY; ?>";
    });
    $('.terms-of-use').click(function () {
        window.location = "<?php echo BASEURL; ?>terms-of-use";
    });
    $('.terms-of-use-egift').click(function () {
        window.location = "<?php echo WEB_TERMS_OF_USE_EGIFT; ?>";
    });
    $('.terms-of-use-other').click(function () {
        window.location = "<?php echo WEB_TERMS_OF_USE_OTHER; ?>";
    });
    $('.faqs').click(function () {
        window.location = "<?php echo WEB_FAQS; ?>";
    });
    $('.egiftcards').click(function () {
        window.location = "<?php echo WEB_EGIFTCARD_VOUCHERS; ?>";
    });
    $('.shipping-policy').click(function () {
        window.location = "<?php echo BASEURL; ?>shipping-policy";
    });
    $('.refund-policy').click(function () {
        window.location = "<?php echo BASEURL; ?>refund-policy";
    });
    $(document).on("click", "#cboxOverlay", function () {
//        $("body").css("overflow", "auto");
        $('video').trigger('pause');
//        stopVideo($('#youtube'));
    });
    $(document).on("click", "#cboxClose", function () {
//        $("body").css("overflow", "auto");
        $('video').trigger('pause');
//        stopVideo($('#youtube'));
    });
//    $(document).on("click", "#howitworksmodal", function () {
//        var url = "https://www.youtube.com/embed/01_4MUazGMk?autoplay=0";
//        $("#youtube").attr("src", url);
//    });

    $(document).on("click", "#howitworksmodal", function () {
//        $('video').attr("autoplay", "1");
        $('video').trigger('play');      
    });

    $(document).on("click", ".inline_colorbox", function () {
        //        $("body").css("overflow", "hidden");
    });
    $('#mobile_no,#sbyear,#sbdate,#conf_password,#sign_password').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
    $(document).ready(function () {


        //old session concept
        //        $(window).bind("beforeunload", function () {
        //            if ((islogin != "") && (isGuest == 1)) {
<?php
//$this->session->unset_userdata('userdata');
?>
        //                return confirm("Do you really want to close?");
        //            }
        //        });

        var resend_mobile_number = "";
        $("#footertesti").show();
        $("#footertesti").css("display", "block");
        $('img').bind('contextmenu', function (e) {
            return false;
        });
        $("#CmbCountryOption option[value=102]").attr("selected", "selected");
    });
    $(".facebookLogin").click(function () {
        $("#cboxOverlay, #cboxWrapper, #colorbox").css("z-index", "2147483647");
        var red_type = $(this).attr("red_type");
        alert(red_type);
        $("#colorbox").css("display", "none");
        if (red_type == '33' || red_type == '44') {
            set_sessiondata(red_type, 99); //facebook
        } else if (red_type == "22") {
            set_gsessiondata(red_type, "4");
        } else {
            window.location = "<?php echo BASEURL; ?>Home_web/social_login_data/2/" + red_type;
        }

    });
    $(".googleLogin").click(function () {
        var red_type = $(this).attr("red_type");
        $("#colorbox").css("display", "none");
        if (red_type == 33 || red_type == 44) {
            set_sessiondata(red_type, 88); //google
        } else if (red_type == "22") {
            set_gsessiondata(red_type, "5");
        } else {
            window.location = "<?php echo BASEURL; ?>Home_web/social_login_data/3/" + red_type;
        }

    });
   function set_gsessiondata(red_type, from) {
        var frnames = $("input[name='friend_name[]']").map(function () {
            return $(this).val();
        }).get();
        var fremails = $("input[name='friend_email[]']").map(function () {
            return $(this).val();
        }).get();
        var frphones = $("input[name='fphone[]']").map(function () {
            return $(this).val();
        }).get();
        var data = {
            rfrom: 0, //When r=0 we are saving details in sessions ( controllers send_egreeting)
            card_id: $("#card_id").val(),
            cat_id: $("#cat_id").val(),
            message: $("#message").val(),
            friend_name: JSON.stringify(frnames),
            friend_email: JSON.stringify(fremails),
            title: $("#gtitle").val(),
            schedule: $("#greetingSchedule").val(),
            greeting_img: $("#greeting_img").val(),
            gettype: $("#checktype").val(),
            imgcaptureuploaded: $("#imgcaptureuploaded").val(),
            uploaded_finalimg: $("#uploaded_finalimg").val(),
            fphone: JSON.stringify(frphones),
        }
        var url = "<?php echo BASEURL; ?>send-egreeting";
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $("#demo").fadeIn(5000);
                $("#colorbox").css("display", "none");
            },
            success: function (msg) {
                if (from == "5")
                    window.location = "<?php echo BASEURL; ?>Home_web/social_login_data/3/" + red_type;
                else
                    window.location = "<?php echo BASEURL; ?>Home_web/social_login_data/2/" + red_type;
            }, });
    }



    function set_sessiondata(red_type, from) {
        var tz = jstz.determine(); // Determines the time zone of the browser client
        var timezone = tz.name();
        var data = {
            quantity_new: $("#quantity_new").val(),
            selected_amount: $("#setamount").val(),
            greeting_message: $("#htmlContent").val(),
            fname: $("#pname").val(),
            femail: $("#eemail").val(),
            delievery_schedule: $("#delivery_schedule").val(),
            delievery_time: $("#time").val(),
            delivery_date: $("#ddate").val(),
            type: $("#checktype").val(),
            greeting_id: $("#set_greeting_id").val(),
            uploaded_finalimg: $("#uploaded_finalimg").val(),
            fphone: $("#phone").val(),
            redirect: $("#urlaction").attr("redirectval"),
            timezone: timezone,
        }
        var url = "<?php echo BASEURL; ?>Home_web/set_Voucher_sessionData";
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $(".showloader").addClass("show_overlay").show();
            },
            success: function (msg) {
                if (from == "88")
                    window.location = "<?php echo BASEURL; ?>Home_web/social_login_data/Google/" + red_type;
                else
                    window.location = "<?php echo BASEURL; ?>Home_web/social_login_data/Facebook/" + red_type;
            }, });
    }

    $(".selected_card").click(function () {
        var voucher_url = $(this).attr("voucher_url");
        window.location = "<?php echo BASEURL; ?>voucher/" + voucher_url;
    });
    $(document).on('change', '#CmbCountryOption', function () {
        $('#mobile_no').val('');
        var curr_val = $(this).val();
        if (curr_val != 102) {
            $('#mobile_no').attr('readonly', '');
        } else {
            $('#mobile_no').removeAttr('readonly');
        }
    });
    var timer = null;
    $('#submit').click(function () {
        var selectedCategory = '';
        $('input[name="Gender"]:checked').each(function () {
            selectedCategory = (this.value);
        });
        //Old
        //        doSignup($("#first_name").val(), $("#sign_password").val(), $("#mobile_no").val(), $("#sign_email").val(), $("#conf_password").val(), $("#sbdate").val(), $("#sbmonth").val(), $("#sbyear").val(), selectedCategory, $("#CmbCountryOption").val(), $("#submit").attr("redirect"));
        //New
        doSignup($("#first_name").val(), $("#sign_password").val(), $("#mobile_no").val(), $("#sign_email").val(), $("#conf_password").val(), $(".sbdate").val(), $(".sbmonth").val(), $(".sbyear").val(), selectedCategory, $("#CmbCountryOption").val(), $("#submit").attr("redirect"));
    });
    $('.submit').keypress(function (e) {
        if (e.which == 13) {
            var selectedCategory = '';
            $('input[name="Gender"]:checked').each(function () {
                selectedCategory = (this.value);
            });
            //Old
            //            doSignup($("#first_name").val(), $("#sign_password").val(), $("#mobile_no").val(), $("#sign_email").val(), $("#conf_password").val(), $("#sbdate").val(), $("#sbmonth").val(), $("#sbyear").val(), selectedCategory, $("#CmbCountryOption").val(), $("#submit").attr("redirect"));
            //New
            doSignup($("#first_name").val(), $("#sign_password").val(), $("#mobile_no").val(), $("#sign_email").val(), $("#conf_password").val(), $(".sbdate").val(), $(".sbmonth").val(), $(".sbyear").val(), selectedCategory, $("#CmbCountryOption").val(), $("#submit").attr("redirect"));
        }
    });
    function diff_minutes(dt2, dt1)
    {
        var diff = (dt2.getTime() - dt1.getTime()) / 1000;
        diff /= 60;
        return Math.abs(Math.round(diff));
    }
    $("#forgot").click(function () {
        forgot_login();
    });
    $("#forgot").keydown(function (e) {
        if (e.which == 13) {
            forgot_login();
        }
    });
    var kcount = 0;
    $("#otpsubmit").click(function () {
        var otp = $("#otp").val();
        if (otp == '') {
            alert("Enter Mobile Number");
        } else if (!(checkMobileValid(otp)) && otp != "") {
            alert("Enter valid Mobile No");
        } else {
            var currentTime = new Date();
            dt1 = new Date(currentTime);
            dt2 = new Date(localStorage.getItem("cookie"));
            console.log(kcount + "lkk" + localStorage.getItem("cookie") + "////" + dt2, dt1 + "////" + diff_minutes(dt2, dt1));
            if (diff_minutes(dt2, dt1) > 5) {
                localStorage.setItem("cookie", "");
                kcount = 0;
            }
            kcount++;
            if (kcount == 3) {
                var today = new Date();
                localStorage.setItem("cookie", today);
            } else {
                if (localStorage.getItem("cookie")) {
                    //                    $("#recerror").css('display', 'block');
                    //                    $("#recerror").html("Try again after 5 minutes");
                    alert("Try again after 5 minutes");
                } else {
                    Otp_request(otp);
                }
            }
        }

    });
    $("#verifyresetrequest").click(function () {
        var otp = $("#verifyOtpVal").val();
        if (otp == '') {
            alert("Enter OTP");
        } else {
            var data = {
                otp: otp
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>verify-otp-reset",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $(".showloader").removeClass("show_overlay").hide();
                        $("#verifyOtpVal").val("");
                        alert(r.message.toString());
                        return false;
                    } else {
                        $(".showloader").removeClass("show_overlay").hide();
                        alert(r.message.toString());
                        $('#forgotpopup').click();
                        $("#otpresetDiv").show();
                        $("#forgetpassDiv").hide();
                        $.colorbox.resize({width: "400px", height: "380px"});
                        window.location = "<?php echo WEB_HOME; ?>";
                    }
                }

            });
        }
    });
    var inserted_user_id = "";
    var is_profile_edit = "";
    //    var is_guest = "";
    //update profile values
    var first_name_profile = "";
    var email_profile = "";
    var password_profile = "";
    var mobile_no_profile = "";
    var bdate_profile = "";
    var bmonth_profile = "";
    var byear_profile = "";
    var gender_profile = "";
    var country_id_profile = "";
    //////////////////////////////////////////the above two variable is for guest mobile update////////////////////////////////////////////////////////////
//    var is_update_phone_number_guest = false;
    //    var update_phone_value_guest = "";

    function verifyOtpMobile(redirect_key) {
        var user_id = inserted_user_id;
        var otp = $("#verifyOtpValMobile").val();
        var is_profile_edit1 = is_profile_edit;
        if (otp == '') {
            alert("Enter OTP");
        } else {
            if (is_profile_edit1 == "0") {
                var data = {
                    user_id: user_id,
                    otp: otp,
                    is_profile_edit: is_profile_edit,
                    is_guest: is_guest,
//                    is_update_phone_number_guest: is_update_phone_number_guest,
                    //                    update_phone_value_guest: update_phone_value_guest,
                    mobile_no: resend_mobile_number
                }
            } else {
                var data = {
                    user_id: user_id,
                    otp: otp,
                    is_profile_edit: is_profile_edit,
                    first_name: first_name_profile,
                    email: email_profile,
                    password: password_profile,
                    mobile_no: mobile_no_profile,
                    bdate: bdate_profile,
                    bmonth: bmonth_profile,
                    byear: byear_profile,
                    gender: gender_profile,
                    country_id: country_id_profile
                }
            }

            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>verify-otp-signup",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $(".showloader").removeClass("show_overlay").hide();
                        $("#verifyOtpVal").val("");
                        alert(r.message.toString());
                        return false;
                    } else {
                        if (redirect_key == 0) {
                            $(".showloader").removeClass("show_overlay").hide();
                            alert(r.message.toString());
                            window.location = "<?php echo WEB_DASHBOARD; ?>";
                        } else if (redirect_key == 1) {
                            call_send_greeting(0, 1);
                        } else if (redirect_key == 10) {
                            send_flower_to_cart(0, 1);
                        } else if (redirect_key == 11) {
                            call_send_greeting();
                        } else {

                        }

                    }
                }

            });
        }
    }
    var redirect_value = "";
    $("#verifyOtpMobile").click(function () {
        verifyOtpMobile(redirect_value);
    });
    $("#resent_otp").click(function () {
        var otp = $("#otp").val();
        var currentTime = new Date();
        dt1 = new Date(currentTime);
        dt2 = new Date(localStorage.getItem("cookie"));
        //        console.log(kcount + "lkk" + localStorage.getItem("cookie") + "////" + dt2, dt1 + "////" + diff_minutes(dt2, dt1));
        var seconds = (dt1 - dt2) / 1000;
        if (seconds >= 60) {
            localStorage.setItem("cookie", "");
            kcount = 0;
        }
        kcount++;
        //        if (kcount == 3) {
        //            alert('karia');
        //            alert("Try again after 1 minutes");
        ////            var today = new Date();
        ////            localStorage.setItem("cookie", today);
        //        } else {
        if (localStorage.getItem("cookie")) {
            var remaining = 60 - Math.round(seconds);
            //                $("#recerror").css('display', 'block');
            //                $("#recerror").html("Try again after 5 minutes");
            alert("Try again after " + remaining + " seconds");
        } else {
            Otp_request(otp);
            var today = new Date();
            localStorage.setItem("cookie", today);
        }
        //        }
    });
    $("#resendOtpMobile").click(function () {
        var otp = resend_mobile_number;
        var currentTime = new Date();
        dt1 = new Date(currentTime);
        dt2 = new Date(localStorage.getItem("cookie"));
        console.log(kcount + "lkk" + localStorage.getItem("cookie") + "////" + dt2, dt1 + "////" + diff_minutes(dt2, dt1));
        var seconds = (dt1 - dt2) / 1000;
        if (seconds >= 60) {
            localStorage.setItem("cookie", "");
            kcount = 0;
        }
        //        if (diff_minutes(dt2, dt1) > 5) {
        //            localStorage.setItem("cookie", "");
        //            kcount = 0;
        //        }
        kcount++;
        //        if (kcount == 3) {
        //            var today = new Date();
        //            localStorage.setItem("cookie", today);
        //        } else {
        if (localStorage.getItem("cookie")) {
            var remaining = 60 - Math.round(seconds);
            //                $("#recerror").css('display', 'block');
            //                $("#recerror").html("Try again after 5 minutes");
            alert("Try again after " + remaining + " seconds");
        } else {
            Otp_request_mobile(otp);
            var today = new Date();
            localStorage.setItem("cookie", today);
        }
        //        }
    });
    $("#change_mb_no").click(function () {
        kcount = 0;
        //        $("#recerror").css('display', 'none');
        localStorage.setItem("cookie", "");
        $('#forgotpopup').click();
        $("#otpresetDiv").hide();
        $("#forgetpassDiv").show();
        $.colorbox.resize({width: "500px", height: "480px"});
    });
    function  Otp_request(otp) {
        var data = {
            mobile_no: otp
        }

        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>forgot-mobile",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $(".showloader").addClass("show_overlay").show();
            },
            success: function (r) {
                if (r.success.toString() === "false") {
                    $(".showloader").removeClass("show_overlay").hide();
                    alert(r.message.toString());
                    return false;
                } else {
                    $(".showloader").removeClass("show_overlay").hide();
                    alert(r.message.toString());
                    $('#forgotpopup').click();
                    $("#otpresetDiv").show();
                    $("#forgetpassDiv").hide();
                    $.colorbox.resize({width: "400px", height: "380px"});
                    //

                }
            }

        });
    }
    function  Otp_request_mobile(otp) {
        var data = {
            mobile_no: otp
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>resend-otp",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $(".showloader").addClass("show_overlay").show();
            },
            success: function (r) {
                if (r.success.toString() === "false") {
                    $(".showloader").removeClass("show_overlay").hide();
                    alert(r.message.toString());
                    return false;
                } else {
                    $(".showloader").removeClass("show_overlay").hide();
                    alert(r.message.toString());
                    $('#forgotpopup').click();
                    $("#otpresetDiv").hide();
                    $("#forgetpassDiv").hide();
                    //                            $("#SignUp").hide();
                    $("#verifyMobileDiv").show();
                    //                            $.colorbox({overlayClose: false});
                    $.colorbox({
                        width: "400px",
                        height: "320px",
                        inline: true,
                        href: $("#verifyMobileDiv").show(),
                        overlayClose: false
                    });
                }
            }
        });
    }

    $("#reminder_Click").click(function () {
        $(window).scrollTop(0); //To scroll at the top
    });
    $("#reminderlogin").click(function () {
        commonLogin($("#remail").val(), $("#rpassword").val(), $("#login_type").val());
    });
    $("#login_reminder").click(function () {
        commonLogin($("#lemail_rem").val(), $("#lpassword_rem").val(), '7');
    });
    $("#login_guest").click(function () {
        commonLogin($("#lemail_login").val(), $("#lpassword_login").val(), $("#login_type").val());
    });
    $(".reminderLogin").keypress(function (e) {
        if (e.which == 13) {

            commonLogin($("#remail").val(), $("#rpassword").val(), $("#login_type").val());
        }
    });
    $('#gfirst_name,#gsign_email,#gmobile_no,#first_name,#sign_password,#gsign_password,#conf_password,#gconf_password,#sign_email,#mobile_no,#gsbdate,#gsbmonth,#sbdate,#sbmonth,#sbyear,#gsbyear').keyup(function (e) {
        $(".signerror,.insidesignuperror").html("").removeClass("showerror");
    });
    function  doSignup(first_name, sign_password, mobile_no, sign_email, conf_password, sbdate, sbmonth, sbyear, selectedCategory, country_id, redirect)
    {
        //        var country_drop_down_value = $("#CmbCountryOption").val();
        //redirect 0:dashoboard,1-egift,2-egreetings,4-membership
        if (country_id == 102) {
            if (mobile_no == '') {
                $(".signerror,.insidesignuperror").html("Enter Mobile Number").addClass("showerror");
                return false;
            }
        }
        if (first_name == '') {
            $(".signerror,.insidesignuperror").html("Enter Your  Name").addClass("showerror");
        } else if (sign_email == '') {
            $(".signerror,.insidesignuperror").html("Enter Email").addClass("showerror");
        } else if (!(validateEmail(sign_email)) && sign_email != "") {
            $(".signerror,.insidesignuperror").html("Invalid Email Address").addClass("showerror");
        } else if (sign_password == '') {
            $(".signerror,.insidesignuperror").html("Enter Password").addClass("showerror");
        } else if (conf_password == '') {
            $(".signerror,.insidesignuperror").html("Enter Confirm Password").addClass("showerror");
        } else if (sign_password != conf_password) {
            $(".signerror,.insidesignuperror").html("Passwords do not match").addClass("showerror");
        } else if (sbdate == "") {
            $(".signerror,.insidesignuperror").html("Enter Birth date").addClass("showerror");
        } else if (sbmonth == "") {
            $(".signerror,.insidesignuperror").html("Select Birth Month").addClass("showerror");
        } else if (sbyear == "") {
            $(".signerror,.insidesignuperror").html("Select Birth Year").addClass("showerror");
        } else if (!(checkMobileValidAndCountryCode(mobile_no,country_id)) && mobile_no != "") {
            $(".signerror,.insidesignuperror").html("Enter valid Mobile No").addClass("showerror");
        } else {
            var data = {
                first_name: first_name,
                password: sign_password,
                email: sign_email,
                gender: selectedCategory,
                bdate: sbdate,
                bmonth: sbmonth,
                byear: sbyear,
                country_id: country_id,
                mobile_no: mobile_no,
                signup_type: redirect
            }

            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>registration",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    if (r.success.toString() === "true") {
                        var country_id = r.userdata['country_id'];
                        var is_converted = r.is_converted;
                        inserted_user_id = r.userdata['user_id'];
                        is_profile_edit = 0;
                        //                        alert(r.userdata['user_id']);
                        $(".showloader").removeClass("show_overlay").hide();
                        if (redirect == 0) {
                            if (country_id == 102) {
                                if (is_converted == 1) {
                                    alert("Registered successfully");
                                    window.location = "<?php echo WEB_DASHBOARD; ?>";
                                } else {
                                    is_guest = 0;
                                    redirect_value = redirect;
                                    $('#forgotpopup').click();
                                    $("#otpresetDiv").hide();
                                    $("#forgetpassDiv").hide();
                                    //                            $("#SignUp").hide();
                                    $("#verifyMobileDiv").show();
                                    //                            $.colorbox({overlayClose: false});
                                    $.colorbox({
                                        width: "400px",
                                        height: "320px",
                                        inline: true,
                                        href: $("#verifyMobileDiv").show(),
                                        overlayClose: false
                                    });
                                    resend_mobile_number = mobile_no;
                                }
                            } else {
                                alert("Registered successfully");
                                window.location = "<?php echo WEB_DASHBOARD; ?>";
                            }
                            //                            $.colorbox.resize({width: "400px", height: "320px"});
                        } else if (redirect == 4) {
                            window.location = "<?php echo BASEURL; ?>membershipcart/1";
                        } else if (redirect == 1) {
                            if (country_id == 102) {
                                if (is_converted == 1) {
                                    call_send_greeting(0, 1);
                                } else {
                                    is_guest = 0;
                                    redirect_value = redirect;
                                    $('#forgotpopup').click();
                                    $("#otpresetDiv").hide();
                                    //                            $("#forgetpassDiv").hide();
                                    $("#verifyMobileDiv").show();
                                    $.colorbox({
                                        width: "400px",
                                        height: "320px",
                                        inline: true,
                                        href: $("#verifyMobileDiv").show(),
                                        overlayClose: false
                                    });
                                    resend_mobile_number = mobile_no;
                                }
                            } else {
                                call_send_greeting(0, 1);
                            }
                            //     call_send_greeting(0, 1);
                        } else if (redirect == 10) {
                            if (country_id == 102) {
                                if (is_converted == 1) {
                                    send_flower_to_cart(0, 1);
                                } else {
                                    is_guest = 0;
                                    redirect_value = redirect;
                                    $('#forgotpopup').click();
                                    $("#otpresetDiv").hide();
                                    //                            $("#forgetpassDiv").hide();
                                    $("#verifyMobileDiv").show();
                                    $.colorbox({
                                        width: "400px",
                                        height: "320px",
                                        inline: true,
                                        href: $("#verifyMobileDiv").show(),
                                        overlayClose: false
                                    });
                                    resend_mobile_number = mobile_no;
                                }

                            } else {
                                send_flower_to_cart(0, 1);
                            }
                            //                            send_flower_to_cart(0, 1);
                        } else if (redirect == 11) {
                            if (country_id == 102) {
                                if (is_converted == 1) {
                                    call_send_greeting();
                                } else {
                                    is_guest = 0;
                                    redirect_value = redirect;
                                    $('#forgotpopup').click();
                                    $("#otpresetDiv").hide();
                                    //                            $("#forgetpassDiv").hide();
                                    $("#verifyMobileDiv").show();
                                    $.colorbox({
                                        width: "400px",
                                        height: "320px",
                                        inline: true,
                                        href: $("#verifyMobileDiv").show(),
                                        overlayClose: false
                                    });
                                    resend_mobile_number = mobile_no;
                                }

                            } else {
                                call_send_greeting();
                            }
                            //                            call_send_greeting();
                        } else {

                        }
                    } else {
                        $(".showloader").removeClass("show_overlay").hide();
                        alert(r.message.toString());
                        return false;
                    }
                }
            });
        }
    }

    function forgot_login() {
        var femail = $("#femail").val();
        if (femail == '') {
            alert("Enter Email Address");
        } else if (!(validateEmail(femail)) && femail != "") {
            alert("Invalid Email Address");
        } else {
            var data = {
                email: femail
            }

            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>forgot-email",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $(".showloader").removeClass("show_overlay").hide();
                        alert(r.message.toString());
                        return false;
                    } else {
                        $(".showloader").removeClass("show_overlay").hide();
                        alert(r.message.toString());
                        window.location = "<?php echo WEB_HOME; ?>";
                    }
                }

            });
        }
        $('#femail').keyup(function (e) {

            $(".error6").html("");
        });
    }
    function commonLogin(email, password, ltype) {
        if (email == '') {
            alert("Enter Email Address");
        } else if (!(validateEmail(email)) && email != "") {
            alert("Invalid Email Address");
        } else if (password == '') {
            alert("Enter Password");
        } else {
            var data = {
                email: email,
                password: password,
                login_type: ltype,
                is_guest: 0
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>login/1",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $(".showloader").removeClass("show_overlay").hide();
                        alert(r.message.toString());
                        $('#password').val("");
                        $('#lpassword').val("");
                        return false;
                    } else {
                        var url = window.location.href;
                        $(".showloader").removeClass("show_overlay").hide();
                        if (r.login_type.toString() == '0') {
                            //New
                            window.location = url;
                            //Old
                            //                          window.location = "<?php // echo BASEURL;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ?>dashboard";
                        } else if (r.login_type.toString() == '3') {
                            window.location = "<?php echo WEB_HOME; ?>";
                        } else if (r.login_type.toString() == '6') {
                            window.location = "<?php echo BASEURL; ?>membershipcart/1";
                        } else if (r.login_type.toString() == '7') {
                            window.location = "<?php echo BASEURL; ?>addReminder/0";
                        }
                        //                                        }
                        //                                    }, 1000);
                    }
                }
            });
        }
    }

    function Login_Guest_Flowers_Cakes(email, password, ltype) {
//        if (email == '') {
//            alert("Enter Email Address");
//        } else if (!(validateEmail(email)) && email != "") {
//            alert("Invalid Email Address");
//        } else if (password == '') {
//            alert("Enter Password");
        //        } else {
        var data = {
            email: email,
            password: password,
            login_type: ltype,
            is_guest: 0
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>login/1",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $(".showloader").addClass("show_overlay").show();
            },
            success: function (r) {
                if (r.success.toString() === "false") {
                    $(".showloader").removeClass("show_overlay").hide();
                    alert(r.message.toString());
                    $('#password').val("");
                    $('#lpassword').val("");
                    return false;
                } else {
                    send_flower_to_cart(0, 1);
                }
            }
        });
        //        }
    }

    function Login_Guest_Vouchers(email, password, ltype) {
//        if (email == '') {
//            alert("Enter Email Address");
//        } else if (!(validateEmail(email)) && email != "") {
//            alert("Invalid Email Address");
//        } else if (password == '') {
//            alert("Enter Password");
        //        } else {
        var data = {
            email: email,
            password: password,
            login_type: ltype,
            is_guest: 0
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>login/1",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $(".showloader").addClass("show_overlay").show();
            },
            success: function (r) {
                if (r.success.toString() === "false") {
                    $(".showloader").removeClass("show_overlay").hide();
                    alert(r.message.toString());
                    $('#password').val("");
                    $('#lpassword').val("");
                    return false;
                } else {
                    call_send_greeting(0, 1);
                }
            }
        });
        //        }
    }

    $('#semail').focusout(function () {
        var email = $("#semail").val();
        if (!(validateEmail(email))) {
            $(".error_n3").show();
            alert("Invalid Email Address");
        }
    });
    function check_confirm(sign_password, conf_password) {

        if (sign_password != conf_password) {
            $(".errors3").html("Passwords do not match").addClass("showerror");
        } else {
            $(".errors3").html("").removeClass("showerror");
        }
        if (conf_password == '') {
            $(".errors3").html("").removeClass("showerror");
        }
    }
    function isEmailValid(sEmail) {
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if (filter.test(sEmail)) {
            return true;
        } else {
            $(".errors4").html("Invalid Email Address");
        }
    }

    function checkMobileValid(mobile) {

        var filter = /^([+][9][1]|[9][1]|[0]){0,1}([7-9]{1})([0-9]{9})$/;
        if (filter.test(mobile)) {
            return true;
        } else {
            return false;
        }
    }

    function checkMobileValidAndCountryCode(mobile, userCountryCode) {
        if (userCountryCode != 102) {
            return true;
        } else {
        var filter = /^([+][9][1]|[9][1]|[0]){0,1}([7-9]{1})([0-9]{9})$/;
        if (filter.test(mobile)) {
            return true;
        } else {
            return false;
        }
    }
    }

    function validateEmail(sEmail) {
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if (filter.test(sEmail)) {
            return true;
        } else {
            return false;
        }
    }
    function isNumberKey(evt)
    {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 37 && charCode != 38 && charCode != 39 && charCode != 40)
            return false;
    }


    //check internet connection
    if ($(window).width() > 768) {


        setInterval(function () {
            var online = navigator.onLine;
            if (online == false) {

                //offline notification
                $('#internet-box').css('background-color', '#000000');
                $('#internet-box').html('No internet connection please wait. connecting...');
                $('#internet-box').slideDown('slow');
                //freeze whole page
                $('body').css('cursor', 'wait');
                $('body').css('pointer-events', 'none');
                //remove loader
                $.LoadingOverlay("hide");
            } else {

                //online notification
                $('#internet-box').css('background-color', '#87C540');
                $('#internet-box').html('Connected');
                $('#internet-box').slideUp('slow');
                //un-freeze whole page
                $('body').css('cursor', 'default');
                $('body').css('pointer-events', 'auto');
            }
        }, 1000);
    }



    getScrollPosition();
    $(window).on("scroll", function () {
        getScrollPosition();
    });
    function getScrollPosition() {
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        //Down
        if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
            // when scroll to bottom of the page
            $('#ss_menu').css('display', 'block');
            $('#ss_menu').removeClass('ss_menu_down');
            $('#ss_menu').addClass('ss_menu_up');
        } else if ($(window).scrollTop() == 0) {
            $('#ss_menu').css('display', 'block');
            $('#ss_menu').removeClass('ss_menu_up');
            $('#ss_menu').addClass('ss_menu_down');
        } else {
            $('#ss_menu').css('display', 'none');
        }
    }

    $('#ss_menu').click(function () {
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        //Down
        if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
            // when scroll to bottom of the page
            $('#ss_menu').css('display', 'block');
            $('#ss_menu').removeClass('ss_menu_up');
            $('#ss_menu').addClass('ss_menu_down');
            $('html, body').animate({scrollTop: 0}, 500);
            //        $(window).scrollTop(0);
        } else if ($(window).scrollTop() == 0) {
            $('#ss_menu').css('display', 'block');
            $('#ss_menu').removeClass('ss_menu_down');
            $('#ss_menu').addClass('ss_menu_up');
            $('html, body').animate({scrollTop: scrollHeight}, 1000);
            //        $(window).scrollTop(scrollHeight);
        } else {
            $('#ss_menu').css('display', 'none');
        }
    });
    function loadjscssfile(filename, filetype) {
        if (filetype == "js") { //if filename is a external JavaScript file
            var fileref = document.createElement('script');
            fileref.setAttribute("type", "text/javascript");
            fileref.setAttribute("src", filename);
        } else if (filetype == "css") { //if filename is an external CSS file
            var fileref = document.createElement("link");
            fileref.setAttribute("rel", "stylesheet");
            fileref.setAttribute("type", "text/css");
            fileref.setAttribute("href", filename);
        }
        if (typeof fileref != "undefined")
            document.getElementsByTagName("head")[0].appendChild(fileref);
    }


    //google analytics

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-92768886-1', 'auto');
    ga('send', 'pageview');</script>
 <!--//LightWidget WIDGET <script src="//lightwidget.com/widgets/lightwidget.js"></script>-->
<script type="text/javascript">
    var islogin = "<?php echo $this->session->userdata('userdata')['user_id'] ?>";
    $("#fpasslink").click(function () {
        $("#otp").val("");
        kcount = 0;
        //        $("#recerror").css('display', 'none');
        localStorage.setItem("cookie", "");
        $('#forgotpopup').click();
        $("#otpresetDiv").hide();
        $("#forgetpassDiv").show();
        $.colorbox.resize({width: "500px", height: "480px"});
    });
    var cart_count = "";
    $(document).ready(function () {
<?php if ($this->session->flashdata('fmsg')) { ?>
            alert("<?php echo $this->session->flashdata('fmsg'); ?>");
<?php } ?>

        $('#searchval1').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "<?php echo BASEURL . "Home_web/getVoucher"; ?>",
                    type: "GET",
                    dataType: "json",
                    delay: 500,
                    data: {term: request.term},
                    success: function (data) {
                        response($.map(data.marray, function (item) {
                            return {
                                value: item.vname,
                                key: item.vurl
                            }
                        }));
                    }
                });
            },
            minLength: 1,
            list: {
                match: {
                    enabled: true
                }
            },
            select: function (event, ui) {
                alert('akki');
                if (ui.item.key != "") {
                    window.location = "<?php echo BASEURL; ?>search/" + ui.item.key;
                } else {
                    alert("Enter search keyword");
                }
            }
        });
        $(".clickworks").click(function () {
            //            $("body").css("overflow", "hidden");
            $(".active").css("z-index", "0");
            $(".dash").css("z-index", "0");
            $("#hpopup").addClass("hwpopup");
            $('.inner_box').show();
            $('.inner_box').animate({top: '4%'}, 500);
            $('html').animate({
                scrollTop: $(".inner_box").offset().top
            }, 2000);
        });
        $(".hclose").click(function () {
            $("body").css("overflow", "auto");
            $(".active").css("z-index", "1");
            $(".dash").css("z-index", "1");
            $(" #hpopup").removeClass("hwpopup");
            $('.inner_box').css("display", "none");
            $('.inner_box').animate({top: '0'}, 500);
        });
        $("#searchanything").click(function () {
            var searchval1 = $("#searchval1").val();
            if (searchval1 != '') {
                if (searchval1.includes("'s") == true) {
                    searchval1 = searchval1.value.replace(/'/g, '-');
                }
                window.location = "<?php echo BASEURL; ?>search/" + searchval1;
            } else {
                alert("Enter search keyword");
            }
        });
        $("#mycartcheckout").click(function () {
            window.location = "<?php echo WEB_SHOW_CART; ?>";
        });
        $("#searchval1").keypress(function (e) {
            if (e.which == 13) {

                if ($("#searchval1").val() != '') {

                    if ($("#searchval1").val().includes("'s") == true) {
                        $("#searchval1").val() = $("#searchval1").val().value.replace(/'/g, '-');
                    }
                    window.location = "<?php echo BASEURL; ?>search/" + $("#searchval1").val();
                } else {
                    alert("Enter search keyword");
                }
            }
        });
        $("#click_welcome").click(function () {
            window.location = "<?php echo WEB_DASHBOARD; ?>";
        });
        $(document).ready(function () {
            get_cart_count();
            $(".clickcart").click(function () {
<?php if ($this->session->userdata('userdata') != NULL) { ?>
                    var data = {
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo BASEURL; ?>cart-view",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.success.toString() === "true") {
                                var count = r.view_cart.length;
                                $("#cart_count").attr("count", count);
                                var cart = '';
                                if (count > 0) {
                                    window.location = "<?php echo WEB_SHOW_CART; ?>";
                                } else {
                                    alert("Cart is Empty");
                                }

                            }
                        }
                    });
<?php } else { ?>
                    $("#login_type").val('3');
                    $('#mlogin_popup').click();
<?php } ?>
            });
        });
        $(document).on('click', ".list_close", function () {
            var order_pro_id = $(this).attr("order_pro_id");
            var cart_countn = $("#cart_count").attr("count");
            var ask = window.confirm("Are you sure you want to delete?");
            if (ask) {
                var data = {
                    order_pro_id: order_pro_id
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Home_web/delete_orders",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#message").html(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            $(".show_cart").slideToggle("slow").css("display", "none");
                            if (cart_countn == "1") {
                                window.location = "<?php echo WEB_HOME; ?>";
                            } else {
                                window.location = "<?php echo WEB_SHOW_CART; ?>";
                            }
                        }
                    }
                });
            } else {
                window.location = "<?php echo WEB_HOME; ?>";
            }
        });
        $("#displaycart").hover(function () {
            if (islogin != "") {
                if (cart_count > 0) {
                    $(".cartcontainer").show();
                } else {
                    $(".cartcontainer").hide();
                }
            } else {
                $(".cartcontainer").hide();
            }
        }, function () {
            $(".cartcontainer").hide();
        });
        function get_cart_count() {
<?php
$token = md5(rand(1000, 9999)); //you can use any encryption
$sdata["btoken"] = $token;
$this->session->set_userdata($sdata);
?>


            var data = {
                guest_session: localStorage.getItem("guest_session"),
                btoken: "<?php echo $token; ?>"
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>cart-count",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() == "true") {
                        if (islogin != "") {
                            cart_count = r.cart_count;
                            var sallcarts = "", gimg = '', vpimg = '', emailimg = '', smsimg;
                            var sub_total = "";
                            if (cart_count > 0) {
                                for (i = 0; i < cart_count; i++) {
                                    if (r.carts[i]['is_voucher'] == 1 && r.carts[i]['is_flower'] == 0) {
                                        gimg = "<?php echo BASEURL_CROPPED_GREET; ?>" + r.carts[i]['front_page_image'];
                                        if (r.carts[i]['femail'] != "") {
                                            emailimg = "<img src='<?PHP echo BASEURL_OIMG ?>envo.png' style='width:10px;' />";
                                        } else {
                                            emailimg = "";
                                        }
                                        if (r.carts[i]['fphone'] != "") {
                                            smsimg = " <img src='<?PHP echo BASEURL_OIMG ?>envo.png' />";
                                        } else {
                                            smsimg = "";
                                        }
                                        sub_total = r.carts[i]['selected_amount'] * r.carts[i]['quantity'];
                                        sallcarts +=
                                                '<li><div style="text-align:left;width: 100%;display: flex;padding-bottom: 8px;padding-top:8px;border-bottom: 1px solid gray;">'
                                                + '<div style="width:50%;padding-left:12px;">'
                                                + '<span class="cartitem">' + r.carts[i]['voucher_pro_name'] + '</span><br/>'
                                                + '<span class="cartitem">Rs.' + sub_total + '</span><br/>'
                                                + '<span class="cartitem">' + r.carts[i]['fname'] + '</span></div><br/>'

                                                + '<div style="width:45%;"> <div style="transform: rotate(-8deg);  width: 25%;" >'
                                                + '<img src="' + gimg + '" />  </div>'
                                                + '<div style = "margin-top: -40px; position: relative;margin-left:37px;" ><img src = "' + r.carts[i]['voucher_img'] + '"   style="height:50px" /> </div></div>'
                                                + '<div style = "width: 5%; margin: auto 5px; text-align: left;cursor:pointer; font-weight:bold;" order_pro_id="' + r.carts[i]['unique_id'] + '"  same_quantity_id="' + r.carts[i]['unique_id'] + '"  class="delete_mycartitems">x</div>'
                                                + '</div></li>';
                                    } else if (r.carts[i]['is_voucher'] == 0 && r.carts[i]['is_flower'] == 1) {
                                        gimg = "<?php echo BASEURL_PRODUCT_IMG; ?>" + r.carts[i]['flower_image'];
                                        if (r.carts[i]['receiver_email'] != "") {
                                            emailimg = "<img src='<?PHP echo BASEURL_OIMG ?>envo.png' style='width:10px;' />";
                                        } else {
                                            emailimg = "";
                                        }
                                        if (r.carts[i]['receiver_phone'] != "") {
                                            smsimg = " <img src='<?PHP echo BASEURL_OIMG ?>envo.png' />";
                                        } else {
                                            smsimg = "";
                                        }
                                        sub_total = r.carts[i]['flower_price'] * r.carts[i]['quantity'];
                                        sallcarts +=
                                                '<li><div style="text-align:left;width: 100%;display: flex;padding-bottom: 8px;padding-top:8px;border-bottom: 1px solid gray;">'
                                                + '<div style="width:50%;padding-left:12px;">'
                                                + '<span class="cartitem">' + r.carts[i]['flower_name'] + '</span><br/>'
                                                + '<span class="cartitem">Rs.' + sub_total + '</span><br/>'
                                                + '<span class="cartitem">' + r.carts[i]['receiver_name'] + '</span></div><br/>'
                                                + '<div style="">'
                                                //                                                <div style="transform: rotate(-8deg);  width: 25%;" >'
                                                //                                                + '<img src="' + gimg + '" />  </div>'
                                                + '<div style = "position: relative;margin-left:25px;" ><img height="50" width="50" src = "<?PHP echo BASEURL_PRODUCT_IMG ?>' + r.carts[i]['flower_image'] + '"   style="" /> </div></div>'
                                                + '<div style = "width: 5%; margin: auto; text-align: left;cursor:pointer; font-weight:bold;" order_pro_id="' + r.carts[i]['unique_id'] + '"  same_quantity_id="' + r.carts[i]['unique_id'] + '"  class="delete_flower">x</div>'
                                                + '</div></li>';
                                    }



                                }
                                $("#shopingitems").html(sallcarts);
                            } else {
                                $(".cartcontainer").css("display", "none");
                            }
                            var totalamt = r.total;
                            $("#stotalamt").html("Rs." + totalamt);
                            $("#cart").html("(" + cart_count + ") " + "My Cart:Rs." + totalamt);
                            $(".ccount").html(cart_count);
                        } else {
                            $("#cart").html("(0) " + "My Cart:Rs.0");
                            $(".ccount").html("0");
                        }
                    } else {
                        alert(r.message.toString());
                    }
                }
            });
        }
        $(document).on('click', ".delete_mycartitems", function () {
            var same_quantity_id = $(this).attr("same_quantity_id");
            var data = {
                same_quantity_id: same_quantity_id,
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>voucher-delete",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $(".showloader").removeClass("show_overlay").hide();
                        alert(r.message.toString());
                        return false;
                    } else {
                        $(".showloader").removeClass("show_overlay").hide();
                        alert(r.message.toString());
                        window.location = "<?php echo WEB_HOME; ?>";
                    }
                }
            });
        });
        $(document).on('click', ".delete_flower", function () {
            var same_quantity_id = $(this).attr("same_quantity_id");
            var data = {
                same_quantity_id: same_quantity_id,
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>flower-delete",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    $(".showloader").removeClass("show_overlay").hide();
                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        return false;
                    } else {
                        alert(r.message.toString());
                        window.location = "<?php echo WEB_HOME; ?>";
                    }
                }
            });
        });
        $("#login").click(function (e) {
            commonLogin($("#lemail").val(), $("#lpassword").val(), $("#lemail").attr("login_type"));
        });
        $("#login_guest_flower_cake").click(function (e) {
            Login_Guest_Flowers_Cakes($("#lemail_flower_cake").val(), $("#lpassword_flower_cake").val(), $("#lemail_flower_cake").attr("login_type"));
        });
        $("#login_guest_voucher").click(function (e) {
            Login_Guest_Vouchers($("#lemail_voucher").val(), $("#lpassword_voucher").val(), $("#lemail_voucher").attr("login_type"));
        });
        $(".login_input").keypress(function (e) {
            if (e.which == 13) {
                commonLogin($("#lemail").val(), $("#lpassword").val(), $("#lemail").attr("login_type"));
            }
        });
    });
    var isGuest = "<?php echo $this->session->userdata('userdata')['is_guest'] ?>";
    $(".bday_reminderpopup").click(function () {
        if ((islogin != '') && (isGuest == 0)) {
            window.location = "<?php echo BASEURL; ?>addReminder/0";
        } else {
            $("#login_type").val('7');
            $("#facebook").attr("red_type", "77");
            $("#google").attr("red_type", "77");
            $('#login_popup').click();
        }
    });
    var options = {
        url: "<?php echo BASEURL; ?>search-data-flower-voucher",
        getValue: "name",
        list: {
            maxNumberOfElements: 10,
            match: {
                enabled: true
            },
            onClickEvent: function (e) {
                $('#searchval').on('change', function () {
                    var value_id = $("#searchval").getSelectedItemData().id;
                    var value_url = $("#searchval").getSelectedItemData().url;
                    //                    alert(value_url);
                    if (value_id != '' && value_url == '') {
                        window.location = "<?php echo BASEURL; ?>flowers_detail/" + value_id + "/0";
                    }
                    if (value_url != '') {
                        $(document).on("click", ".showPopup", function () {
                            $("body").css("overflow", "hidden");
                            //                            var voucher_pro_id = $(this).attr("voucher_pro_id");

                            var data = {
                                voucher_pro_id: value_id
                            }
                            $.ajax({
                                type: "POST",
                                url: "<?php echo BASEURL; ?>voucher-data",
                                data: data,
                                dataType: "json",
                                success: function (r) {
                                    if (r.success.toString() == "false") {
                                        alert(r.message.toString());
                                        return false;
                                    } else {
                                        var voucher_url = r.product_data["voucher_url"];
                                        var voucher_pro_name = r.product_data["voucher_pro_name"];
                                        var redemption_details = r.product_data["terms_conditions"];
                                        var locations = r.product_data["locations"];
                                        var vdescription = r.product_data["vdescription"];
                                        var product_image;
                                        if (r.product_data["product_image"] != "") {
                                            product_image = r.product_data["product_image"];
                                        } else {
                                            product_image = "<?php echo BASEURL_OIMG . "unnamed.png"; ?>";
                                        }

                                        $(".pro_name").text(voucher_pro_name);
                                        $(".minamt").text(" " + r.product_data["min_custom_price"] + " onwards");
                                        $("#pro_img").attr("src", product_image);
                                        $(".selected_card").attr("voucher_url", voucher_url);
                                        var data1 = '';
                                        data1 += '<div>' + redemption_details + '</div>';
                                        $("#redemption_data").html(data1);
                                        var data2 = '';
                                        data2 += '<div>' + vdescription + '</div>';
                                        $("#location_data").html(data2);
                                        $.colorbox({
                                            width: "50%",
                                            height: "550px",
                                            inline: true,
                                            href: "#pickcard"
                                        });
                                        if ($(window).width() < 1280) {

                                            $.colorbox({
                                                width: "60%",
                                                height: "500px",
                                                inline: true,
                                                href: "#pickcard"
                                            });
                                        }

                                        if ($(window).width() < 480) {
                                            $.colorbox({
                                                width: "90%",
                                                height: "90%",
                                                inline: true,
                                                href: "#pickcard"
                                            });
                                        }
                                    }
                                }
                            });
                        });
                        $(".selected_card").click(function () {
                            var voucher_url = $(this).attr("voucher_url");
                            window.location = "<?php echo BASEURL; ?>voucher/" + voucher_url;
                        });
                    }
                }).change();
            }
        }
    };
    $("#searchval").easyAutocomplete(options);
    $(document).ready(function () {

        if ($(window).width() <= 739) {

            //Find and replace top headertext with icons

            $(".top-left li a").html(function () {
                return $(this).html().replace("How it works", "<i class='fa fa-question-circle-o'></i>");
            });
            $(".top-left li a").html(function () {
                return $(this).html().replace("Membership", "<i class='fa fa-users'></i>");
            });
            $(".top-left li a").html(function () {
                return $(this).html().replace("Sign Up", "<i class='fa fa-sign-in'></i>");
            });
            $(".top-left li a").html(function () {
                return $(this).html().replace("Login", "<i class='fa fa-user-circle-o'></i>");
            });
            $(".top-left li a").html(function () {
                return $(this).html().replace("My Account", "<i class='fa fa-address-card-o'></i>");
            });
            $(".top-left li a").html(function () {
                return $(this).html().replace("Logout", "<i class='fa fa-sign-out'></i>");
            });
            $(".top-left li.welcome_text").insertBefore('.top-left');
            $(".top-right li.menu-top-cart").appendTo(".top-left");
            $(".top-left li a").html(function () {
                return $(this).html().replace("Cart", "");
            });
            $(".egift-products li span").text(function (index, currentText) {
                return currentText.substr(0, 30);
            });
        } else {

            console.log('Not a Mobile Device!');
        }

        // Create the dropdown base
        $("<select />").appendTo(".dashboard-res-menu");
        // Create default option "Go to..."
        $("<option />", {
            "selected": "selected",
            "value": "",
            "text": "Select..."
        }).appendTo(".dashboard-res-menu select");
        $(".dashbord_nav a").each(function () {
            var value = $(this).attr("href");
            var el = $(this);
            $("<option />", {
                "data-value": el.text(),
                "value": value,
                "text": el.text()
            }).appendTo(".dashboard-res-menu select");
        });
        $(".dashboard-res-menu select").on('change', function () {
            var address = $(this).val();
            if (address == "UNSUBSCRIBE") {
                $("#unsubscribe").click();
            } else {
                if (address) {
                    window.location = address;
                }
                setNavigation();
            }
            return false;
        });
        setNavigation();
        function setNavigation() {
            var path = window.location.href;
            $(".dashboard-res-menu option").each(function () {
                var value = $(this).val();
                if (path === value) {
                    $(this).attr('selected', 'selected');
                }
            });
        }
    });
    var clicked = false; //Global Variable
    function ClickLogin() {
        clicked = true;
    }
    function onSignIn(googleUser) {
        if (clicked) {
            var profile = googleUser.getBasicProfile();
            console.log('ID: ' + profile.getId());
            var user_id = profile.getId();
            console.log('Name: ' + profile.getName());
            var user_name = profile.getName();
            console.log('Image URL: ' + profile.getImageUrl());
            var user_image_url = profile.getImageUrl();
            console.log('Email: ' + profile.getEmail());
            var user_email = profile.getEmail();
            var profile_array = {
                user_id: user_id,
                user_email: user_email,
                user_name: user_name
            }
            update_user_data(profile_array);
        }
    }

    function update_user_data(pro_array)
    {
        $.ajax({
            type: "POST",
            dataType: 'json',
            data: pro_array,
            url: "<?php echo BASEURL; ?>Home_web/jquery_redirect_google_login",
            success: function (r) {
                if (r.success.toString() == "false") {
                    alert(r.message.toString());
                    return false;
                } else {
                    location.reload();
                }
            }

        });
    }

    function signOut()
    {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log('User signed out.');
            auth2.disconnect();
        });
        auth2.disconnect();
    }

//for stoping the youtube video in header
//    var stopVideo = function (player) {
//        var vidSrc = player.prop('src').replace('autoplay=1', 'autoplay=0');
//        player.prop('src', vidSrc);
//    };

</script>
</body>
</html>
