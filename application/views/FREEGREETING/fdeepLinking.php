<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
         <script type="text/javascript" src="<?php echo BASEURL_OJS ?>redirect.js"></script>
        <title></title>
    </head>
    <body>
        <p>Redirecting...</p>
        <script>
         
                var qs = AppRedirect.queryString;

                AppRedirect.redirect({
                    iosApp: "birthdayowl://FreeGreetingReceived?<?php echo $fid; ?>/<?php echo $user_id; ?>/<?php echo $display; ?>",
                    iosAppStore: 'https://itunes.apple.com/il/app/twitter/id333903271?mt=8&message=' + qs['message'],
                    // For this, your app need to have category filter: android.intent.category.BROWSABLE
                    android: {
                        'host': "FreeGreetingReceived/<?php echo $fid; ?>/<?php echo $user_id; ?>/<?php echo $display; ?>",
                        'scheme': 'birthdayowl', // Scheme part in a custom scheme URL
                        'package': 'com.tech.birthdayowl', // Package name in Play store
                        'fallback': 'https://play.google.com/store/apps/details?id=com.tech.birthdayowl&hl=en'
                    }

                });
          
        </script>
    </body>
</body>
</html>
