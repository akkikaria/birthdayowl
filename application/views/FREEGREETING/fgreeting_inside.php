<?php $this->load->view("new_templates/header_inside"); ?>
<link rel="stylesheet" href="<?php echo BASEURL_DATE; ?>metallic.css" type="text/css"/>
<script src="<?php echo BASEURL_OJS; ?>RecordRTC.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_OJS; ?>webcam.js"></script>
<script src="<?php echo BASEURL_OJS; ?>gumadapter.js"></script>
<link rel="stylesheet" href="<?php echo BASEURL_OCSS; ?>flipclock.css" />
<script src="<?php echo BASEURL_OJS; ?>flipclock.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_OJS ?>mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="<?php echo BASEURL_OCSS; ?>videostyle.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo BASEURL_OCSS ?>pstyles.css"/>
<style>
    #items li{
        float: left;
    }
    .button_items li{
        float: left;
    }
</style>
<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            Birthday<span> Greeting Card</span>
        </div>
    </div>
</div>
<form method="post" id="greeting_submit" name='greeting_data'  enctype="multipart/form-data" >
    <div class="wrapper">
        <div class="container_12">
            <div class="inner_midd dashbord">
                <div class="in_f_l">
                    <div class="leftside">
                        <div class="free_left">
                            <img src="<?php echo BASEURL_CROPPED_GREET . $card_details["front_page_image"]; ?>" class="selected_greeting"  />
                            <input type="hidden" name="greeting_img" id="greeting_img" value="<?php echo $card_details["front_page_image"]; ?>">

                            <div class="photoupload">
                                <img src="<?php echo BASEURL_OIMG ?>frame.png" class="backcorners"/>

                                <div class="photo_img">
                                    <img src="<?php echo BASEURL_OIMG ?>img_before_upload.png" id="defaultpic" class="sampleimg" >
                                    <div style="width:80%;margin: auto;">
                                        <div id="mediaprogress" class="mediauploadhide">
                                            <div id="mediabar"></div>
                                            <div id="mediapercent">0%</div >
                                        </div>
                                    </div>
                                    <div class="mpv">Uploading...</div>

                                    <div class="playerbox" >
                                        <div class="preview_box">
                                            <div class="playaudioContainer" >
                                                <div class="audioControlPlayer audioPlayer" rel="play" id="playit"></div>
                                                <audio id="playaudioplayer"  preload="auto" class="a" >
                                                    <source src="" >
                                                    Your browser does not support the audio element.
                                                </audio>
                                            </div> 
                                        </div>
                                        <div  class="stopPlayer">Remove Audio</div>
                                    </div>

                                    <div class="video_container">
                                        <video src="" controls id="videof" class="display_video" type='video/mp4'>
                                            Your browser does not support the <code>video</code> element.
                                        </video>
                                        <div  class="stopVideo" id="stop_video">Remove Video</div>
                                    </div>
                                    <div class="remove_pic" id="removePhoto" style="cursor:pointer;">Remove Photo</div>

                                    <div class="maincircle">
                                        <img src="<?php echo BASEURL_OIMG ?>image.png" id="icircle" class="icircle"/>    
                                        <img src="<?php echo BASEURL_OIMG ?>video.png" id="ivideo" class="icircle" />
                                        <img src="<?php echo BASEURL_OIMG ?>audio.png" id="iaudio" class="icircle" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="in_f_r free_card_con">
                    <div class="owl_bg wrapper" style="display:none;">
                        <div class="outer_preview_div" style="background:url('<?php echo BASEURL_OIMG; ?>overlay.png')" >
                            <div class="inner_preview_div">
                                <div style="position: absolute; right: -1%; top: -1%;width: 35px;cursor: pointer;" id="close_preview">
                                    <img src="<?php echo BASEURL_OIMG; ?>closeh.png"  style=""/>
                                </div>
                                <div class="outer_greeting_div">
                                    <div class="greeting_full">
                                        <div class="g_left">
                                            <img src="<?php echo BASEURL_CROPPED_GREET . $card_details["front_page_image"]; ?>" />
                                        </div>
                                        <div class="g_right">
                                            <div class="r_con scroll_issue">
                                                <div class="disp_msgarea">
                                                    <div class="display_msg" id="disp_sms">
                                                    </div>
                                                </div>
                                                <div class="media_section">
                                                    <div class="img_section"  style="position: relative; left: 0; top: 0;">
                                                        <img src="<?php echo BASEURL_OIMG; ?>imgframe.png" class="ebackcorners" style="position: relative; top: 0; left: 0;"/>
                                                        <img src="" id="edefaultpic" class="esampleimg"  >
                                                    </div>
                                                    <div class="video_section" >
                                                        <div class="evideo_container">
                                                            <img src="<?php echo BASEURL_OIMG; ?>imgframe.png" class="ebackcorners"/>
                                                            <video src="" controls id="evideof" class="edisplay_video" autoplay="" > </video>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="demo" >
                            <div class="loader_div">
                                <img src="<?php echo BASEURL_OIMG ?>sending_email.jpg" alt="Loading" class="ajax-loader " />
                            </div>
                        </div>
                        <div class="alertMessageOuterDiv">
                            <div class="alertMessage">
                                <div class="alertclose" id="alertclose">X</div>
                                <div style="  border-bottom: 1px solid transparent;  min-height: 16.846px;  padding: 15px;"><h4 class="titlemessage">Your Greeting Card has been sent.</h4></div>
                                <div class="alertInnerMessage">
                                    <ul style=" padding-left: 0" id="emaillist">
                                        <li class="list-group-item">
                                            Your Ecard will be delivered to the following recipient(s)
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--Image capture:start-->
                        <?php
                        $data['from'] = 2;
                        $this->load->view("EGIFT/image_capture", $data);
                        ?>
                        <!--Image capture:End-->
                        <!--audio Recording:start-->
                        <?php $this->load->view("EGIFT/audio_recording"); ?>
                        <!--Recording:End-->
                        <!--video-recording:start-->
                        <?php $this->load->view("EGIFT/video_recording"); ?>
                        <!--video-recording:end-->
                        <div class="section_card wrapper">
                            <h3>
                                MAKE IT PERSONAL : Design your greeting</h3>
                        </div>
                        <div class="section_card wrapper bg_grey">
                            <h4>Recipients</h4>
                            <ul class="mt10 two" id="items">
                                <li>
                                    <input class="name2" type="text"  placeholder="To Name" name="friend_name[]" id="fname1"  value="<?php echo $reminder_info["first_name"] ?>"/>
                                    <div id="fname1_error_msg" style=" color: red; width:100%; height: 16px;font-size: 12px;"></div>
                                </li>
                                <li>
                                    <input type="email" class="emailerror"  placeholder="To Email" name="friend_email[]" id="femail1" value="<?php echo $reminder_info["email"] ?>"/>
                                    <div id="femail1_error_msg" style=" color: red; width: 100%; height: 16px;font-size: 12px;"></div>
                                </li>
                                <li>
                                    <input type="text" class="fphoneerror" placeholder="Friend‘s No"   name="fphone[]" value="<?php echo $reminder_info["fphone"] ?>" id="fphone1" eid="1" onkeypress="return isNumberKey(event)"/>
                                    <div id="fphone1_error_msg" style=" color: red; width: 100%; height: 16px;font-size: 12px;"></div>
                                </li>
                            </ul>
                            <ul class="mt10 two button_items">
                                <li>
                                    <a style="cursor:pointer;" id="add_more" class="add_recipient">+ Add Recipient</a>
                                </li>
                                <li>
                                    <a style="cursor:pointer;" id="remove_users" class="remove_recipient">- Remove Recipient</a>
                                </li>
                            </ul>
                        </div>
                        <div class="section_card wrapper">
                            <ul class="info_con">
                                <li style=" width: 80%;">
                                    <h2>
                                        When would you like this delivered (DD/MM/YYYY)?</h2>
                                    <input id="greetingSchedule" type="text" placeholder="<?php echo date('d/m/Y'); ?>" value="<?php echo date('d/m/Y'); ?>" name="schedule" style="cursor:pointer;" >
                                    <div><b>Note:</b>&nbsp;Your Delivery will be done between 10am to 10pm.</div>
                                </li>
                                <li style=" width: 80%;">
                                    <h2>
                                        Subject</h2>
                                    <input type="text"  placeholder="Your card is waiting for your !" name="title" id="gtitle"/>
                                </li>
                                <li style=" width: 80%;">
                                    <h2>
                                        Add your personal message</h2>
                                    <!--<textarea  rows="8" maxlength="240" name="message" id="message">You’re the sunshine of our family,And we love you as big as the sky!</textarea>-->
                                    <textarea  rows="8" maxlength="240" name="message" id="message">Happy Birthday To You!</textarea>
                                    <div id="count" class="count"></div>
                                    <div id="greet_text" style="display:none;"></div>
                                </li>
                                <li>
                                    <input type="hidden"  name="gettype" id="checktype" value="0"  />
                                    <input type="hidden" id="uploaded_finalimg" name="uploaded_finalimg"  value="">
                                    <input type="hidden" name="cat_id" value="<?php echo $card_details["greet_cat_id"]; ?>">
                                    <input type="hidden"  name="card_id" id="card_id" value="<?php echo $card_details["card_id"]; ?>">
                                    <input type="hidden" name="email" id="freeemail"  value="" >
                                    <input type="hidden" name="password" id="freepass" value="" >
                                    <!--                                    <div><b>Note:</b>&nbsp;Only 10 greetings will be free.</div>
                                                                        <div class="greetingsignup"> 
                                                                            <a href="#SignUp" class="inline_colorbox memsignup" id="signuppopup" data-width="30%">  </a>
                                                                            For unlimited sending of greetings,<a id="signupforgreetings" style="cursor: pointer;"> (sign up)</a> now.
                                                                        </div>-->
                                </li>
                                <!--<div class="clearfix"></div>-->
                                <li class="wrapper mb20" style="width: 90%;">
                                    <div class="section_card wrapper bg_grey">
                                        <h4>Sender</h4>
                                        <ul class="mt10 two button_items">
                                            <li>
                                                <input class="name2" type="text"  placeholder="From Name" id="sender_name" value="<?php if ($user_logged_in) {
                            echo $session_data['first_name'];
                        } ?>" <?php if ($user_logged_in) {
                            echo "readonly";
                        } ?> />
                                                <div id="sender_name_error_msg" style=" color: red; width:100%; height: 16px;font-size: 12px;"></div>
                                            </li>
                                            <li>
                                                <input type="email" class="emailerror"  placeholder="From Email" id="sender_email" value="<?php if ($user_logged_in) {
                            echo $session_data['email'];
                        } ?>" <?php if ($user_logged_in) {
                            echo "readonly";
                        } ?>/>
                                                <div id="sender_email_error_msg" style=" color: red; width: 100%; height: 16px;font-size: 12px;"></div>
                                            </li>
                                            <li>
                                                <input type="text" class="fphoneerror" placeholder="Your No" value="<?php if ($user_logged_in) {
                            echo $session_data['mobile_no'];
                        } ?>" <?php if ($user_logged_in) {
                            echo "readonly";
                        } ?> id="sender_phone" eid="1" onkeypress="return isNumberKey(event)"/>
                                                <div id="sender_phone_error_msg" style=" color: red; width: 100%; height: 16px;font-size: 12px;"></div>
                                            </li>
                                        </ul>
                                    </div>

                                    <!--                                    <div id="guestLoginDiv" class="wrapper" >
                                                                            <h2 style="margin-left: 10px;">Sender's Details</h2>
                                                                            <div class="guestinnerDiv" style="margin: 0 !important;">
                                                                                <ul class="guestradio">
                                                                                    <li>
                                                                                        <input type="radio" id="guestLogin" value="1" name="loginradio" class="gcheckbox" checked=""/>
                                                                                        <label for="guestLogin" class="glable">Continue as Guest</label>
                                                                                    </li>
                                                                                    <li>
                                                                                        <input type="radio" id="checkbox2" value="2" name="loginradio" class="gcheckbox"/>
                                                                                        <label for="checkbox2"  class="glable">Sign up</label>
                                                                                    </li>
                                                                                    <li>
                                                                                        <input type="radio" id="checkbox3" value="3" name="loginradio" class="gcheckbox"  checked=""/>
                                                                                        <label for="checkbox3"  class="glable">Login </label>
                                                                                    </li>
                                                                                </ul>
                                    
                                                                                <ul  class="guestinput"  id="socialdiv" style="display:none;" >
                                                                                    <li>
                                                                                        <a class="vfacebookLogin" style="cursor: pointer;" red_type="33" > 
                                                                                            <img src="<?php // echo BASEURL_OIMG . "fb.png"                                                                                                                                                                      ?>" />
                                                                                        </a>
                                                                                    </li>
                                                                                    <li>
                                                                                        <a class="vgoogleLogin" style="cursor: pointer;" red_type="33">
                                                                                            <img src="<?php // echo BASEURL_OIMG . "google.png"                                                                                                                                                                      ?>" />
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                                <ul class="guestinput"  style="text-align:center;">
                                                                                    <li>
                                                                                        <label id="gemail-label" data-icon="&#xf0e0;">
                                                                                            <input style="padding: 10px;" type="email" id="gemail"  name="gemail" placeholder="YOUR EMAIL ID" />
                                                                                        </label>
                                                                                    </li>
                                    
                                                                                    <li id="passwordBox" style="">
                                                                                        <label id="gname-label" data-icon="&#xf023;">
                                                                                            <input id="gpassword" type="password" name="password" placeholder="PASSWORD" /> 
                                                                                        </label>
                                                                                    </li> 
                                                                                </ul>
                                                                                <ul class="genderBox" style="display:none;">
                                                                                    <li>Gender :
                                                                                        <label id="gname-label">
                                                                                            <input type="radio" name="gGender" value="1" id="Gender_0 " checked="true" />
                                                                                            Male &nbsp;
                                    
                                                                                            <input type="radio" name="gGender" value="2" id="Gender_1 " />
                                                                                            Female
                                                                                        </label>
                                                                                    </li>
                                                                                </ul>
                                    
                                                                                <ul class="signupinput" style="text-align:center;display: none;">
                                                                                    <li style="padding-bottom:7px;">
                                                                                        <label id="gname-label" data-icon="&#xf007;"><input id="gfirst_name" type="text" name="gfirst_name" placeholder="YOUR NAME" /></label>   
                                                                                    </li>
                                                                                    <li style="padding-bottom:7px;">
                                                                                        <label id="gname-label" data-icon="&#xf0e0;">
                                                                                            <input type="email" id="gsign_email"  name="gsign_email" placeholder="YOUR EMAIL ID" />
                                                                                        </label> 
                                                                                    </li>
                                                                                    <li style="padding-bottom:7px;" >
                                                                                        <label id="gname-label" data-icon="&#xf023;">
                                                                                            <input id="gsign_password" type="password" name="gsign_password" placeholder="PASSWORD" /> 
                                                                                        </label>
                                                                                    </li>
                                                                                    <li style="padding-bottom:7px;" >
                                                                                        <label id="gname-label" data-icon="&#xf023;">
                                                                                            <input id="gconf_password" type="password" name="gconf_password" placeholder="CONFIRM PASSWORD" /> 
                                                                                        </label>
                                                                                    </li>
                                    
                                                                                    <li style="padding-bottom:7px;">
                                                                                        <div style="width: 100%;display: flex;">
                                                                                            <select id="dobday1" class="gsbdate" name="gsbdate"></select>
                                                                                            <select id="dobmonth1" class="gsbmonth" name="gsbmonth"></select>
                                                                                            <select id="dobyear1" class="gsbyear" name="gsbyear"></select>
                                                                                        </div>
                                                                                        <div style="text-align: left; font-weight: 600;">Note: Enter date of birth above.</div>
                                                                                    </li>
                                                                                    <li data-icon="&#xf10b;" style="padding-bottom:7px;">
                                                                                        <div style="width: 100%;display: flex;">
                                                                                            <select id="gCmbCountryOption" name="nationality_id" class="countrylist" style="margin-right: 10px;">
<?php // $countryList = get_countryinfo();  ?>
<?php // foreach ($countryList as $country) {  ?>
                                                                                                            <option value="<?php // echo $country["country_id"];                                            ?>"><?php echo $country['country_name'] . " (+" . $country["nationality_code"] . ")"; ?></option>
<?php // }  ?>
                                                                                            </select>
                                                                                            <input type="text" placeholder="Mobile No" id="gmobile_no" name="gmobile_no" onkeypress="return isNumberKey(event)"  class="mbno" />
                                                                                        </div>
                                                                                        <div style="">&nbsp;</div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="insidesignuperror" style=" height: 20px; font-weight: bold; text-align: center;margin-top: 10px;"></div>
                                                                                    </li>
                                    
                                                                                </ul>
                                                                                <div id="gEmailValidation" style="font-weight: bold; height: 15px;"></div>
                                                                            </div>
                                                                        </div>-->
                                </li>
                                <li class=" wrapper mb20 text-center" style="text-align: center; width: 60%;">
                                    <a href="#Login" class="inline_colorbox memlogin" id="login_popup" data-width="">  </a>
                                    <div class="arrowbtn" id="send_card" >SUBMIT</div>  
                                    <div class="arrowbtn" id="preview" >PREVIEW</div>
                                    <div style="height:108px;"></div>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="slider_free wrapper" style="display:none" >
                        <b>You May also like</b> <span id="prdnext"></span><span id="prdprv"></span>

                        <ul id="mloadgreetings">
                        </ul>
                    </div>
                </div>
                <div class="in_f_l greeting_counterimg" style="text-align: center;width:15%;display:none;position: relative;" id="blahnew">
                    <div  class="insideowl_text"><div class="otext"><?php echo $greeting_count; ?></div></div>
                </div>
            </div>
        </div>
    </div>
</form>   
<div class="alertBox"></div>
<div class='BackgroundMedia'></div>
<div class='BackgroundMediaPreview'></div>

<?php $this->load->view("new_templates/vouchers_footer"); ?>
<link href="<?php echo BASEURL_OCSS; ?>jquery.loader.css" rel="stylesheet" />  <!--page loading -->

<script type="text/javascript">
    $(document).on('change', '#gCmbCountryOption', function () {
        $('#gmobile_no').val('');
        var curr_val = $(this).val();
        if (curr_val != 102) {
            $('#gmobile_no').attr('readonly', '');
        } else {
            $('#gmobile_no').removeAttr('readonly');
        }
    });
    $('input').focus(function () {
        $('.signerror_rec_first_name').html('');
        $('.signerror_rec_email').html('');
        $('.signerror_rec_mobile').html('');
        $('.signerror_line1').html('');
        $('.signerror_city').html('');
        $('.signerror_pin_code').html('');
        $('#gEmailValidation').html('');
        $('.insidesignuperror').html('');
    });

    $("input[name='loginradio']").change(function () {
        $('.ierror').html('');
        $('.showerror').html('');
        var radioValue = $("input[name='loginradio']:checked").val();
        switch (radioValue) {
            case "1":
                {
                    $(".guestinput").show();
                    $("#socialdiv").hide();
                    $("#passwordBox").hide();
                    $("#nameBox").show();
                    $(".signupinput").hide();
                    $(".genderBox").hide();
                    $("#numberBox").show();
                }
                break;
            case "2":
                {
                    $(".guestinput").hide();
                    $(".signupinput").show()
                    $("#socialdiv").hide();
                    $("#passwordBox").hide();
                    $("#nameBox").hide();
                    $(".genderBox").show();
                }
                break;
            case "3":
                {
                    $(".guestinput").show();
                    $("#socialdiv").show();
                    $("#passwordBox").show();
                    $("#nameBox").hide();
                    $(".signupinput").hide();
                    $(".genderBox").hide();
                    $("#numberBox").hide();
                }
                break;
        }
    });

    $(document).ready(function () {
        $.dobPicker({
            daySelector: '#dobday1', /* Required */
            monthSelector: '#dobmonth1', /* Required */
            yearSelector: '#dobyear1', /* Required */
            //                dayDefault: birth_date, /* Optional */
            //                monthDefault: birth_month, /* Optional */
            //                yearDefault: birth_year, /* Optional */
            minimumAge: 8, /* Optional */
            maximumAge: 60 /* Optional */
        });
    });
    $("#gCmbCountryOption option[value=102]").attr("selected", "selected");

    if (islogin != "") {
        $("#guestLoginDiv").hide();
    } else {
        $("#guestLoginDiv").show();
    }

    function doLogin(from) {
        var gpassword = $("#gpassword").val();
        var gemail = $("#gemail").val();
        if (gemail == '') {
            $("#gEmailValidation").html("Enter Email Address").addClass("ierror");
        } else if (gpassword == '') {
            $("#gEmailValidation").html("Enter Password ").addClass("ierror");
        } else if (!(validateEmail(gemail)) && gemail != "") {
            $("#gEmailValidation").html("Invalid Email Address").addClass("ierror");
        } else {
            var data = {
                email: gemail,
                password: gpassword,
                login_type: 2,
                is_guest: 0
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>login/1",
                data: data,
                dataType: "json", success: function (r) {
                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        $('#gpassword').val("");
                        return false;
                    } else {
                        $("#flemail").val(gemail);
                        $("#flpass").val(gpassword);
                        call_send_greeting();
                    }
                }
            });
        }
    }

    function performCheckout() {
        var guest = 0;

        var count = counter - 1;
        var message1 = '';
        var message2 = '';
        var message3 = '';
        var message4 = '';
        var message5 = '';
        var success = false;
        var noname = 0, noemail = 0, nophone = 0;
        var femailsame = true;
        var fphonesame = true;

        var sender_name = $("#sender_name").val();
        var sender_email = $("#sender_email").val();
        var sender_phone = $("#sender_phone").val();

        var div_status = true;
        for (i = 1; i <= count; i++) {
            var femail = $("#femail" + i).val();
            var fname = $("#fname" + i).val();
            var fphone = $("#fphone" + i).val();

            message1 = "Enter Friend's Name";
            message2 = "Enter Friend's Email or  Mobile No";
            message3 = "Enter Friend's Mobile No";
            message4 = "Invalid Email Address";
            message5 = "Please enter valid Indian Mobile No";
            
            
            if (fname == '') {
                noname = 1;
                $("#fname" + i + "_error_msg").html(message1);
                div_status = false;
            }
            if (femail == "" && fphone == "") {
                noemail = 1;
                $("#femail" + i + "_error_msg").html(message2);
                div_status = false;
                $('html, body').animate({
                    scrollTop: $(".bg_grey").offset().top
                }, 2000);
            } else if ((!(validateEmail(femail))) && (femail != '')) {
                noemail = 1;
                $("#femail" + i + "_error_msg").html(message4);
                div_status = false;
                $('html, body').animate({
                    scrollTop: $(".bg_grey").offset().top
                }, 2000);
            } else if ((!(checkMobileValid(fphone))) && (fphone != '')) {
                nophone = 1;
                $("#fphone" + i + "_error_msg").html(message5);
                div_status = false;
                $('html, body').animate({
                    scrollTop: $(".bg_grey").offset().top
                }, 2000);
            }



            if (femail != '') {
                for (j = i + 1; j <= count; j++) {
                    if ($("#femail" + [i]).val() == $("#femail" + [j]).val()) {
                        $("#femail" + i + "_error_msg").html("Email-address can't be same");
                        $("#femail" + j + "_error_msg").html("Email-address can't be same");
                        femailsame = false;
                        div_status = false;
                        $('html, body').animate({
                            scrollTop: $(".bg_grey").offset().top
                        }, 2000);
                    } else {
                        $("#femail" + i + "_error_msg").html("");
                        $("#femail" + j + "_error_msg").html("");
                        femailsame = true;
                    }
                }
            }
            if (fphone != "") {
                for (j = i + 1; j <= count; j++) {
                    if ($("#fphone" + [i]).val() == $("#fphone" + [j]).val()) {
                        $("#fphone" + i + "_error_msg").html("Mobile numbers can't be same");
                        $("#fphone" + j + "_error_msg").html("Mobile numbers can't be same");
                        fphonesame = false;
                        div_status = false;
                        $('html, body').animate({
                            scrollTop: $(".bg_grey").offset().top
                        }, 2000);
                    } else {
                        $("#fphone" + i + "_error_msg").html("");
                        $("#fphone" + j + "_error_msg").html("");
                        fphonesame = true;
                    }
                }
            }
        }

//        alert("hello");
//        e.preventDefault();
        // for validating sender's details
        var status = true;

        if (sender_name == '') {
            $("#sender_name_error_msg").html("Enter Your Name");
            status = false;
//            return false;
        }
        if (sender_email === '' && sender_phone === '') {
            $("#sender_email_error_msg").html("Enter Your Email or Mobile No");
            status = false;
//            return false;
        }
        if ((!(validateEmail(sender_email))) && (sender_email !== '')) {
            $("#sender_email_error_msg").html("Invalid Email Address");
            status = false;
//            return false;
        }

        if ((!(checkMobileValid(sender_phone))) && (sender_phone !== '')) {
            $("#sender_email_error_msg").html("Enter Your Valid Mobile No");
            status = false;
        }

        if (status === false || div_status === false) {     // temporary solution need to enahce it
            e.preventDefault();   // veena i would like you to see it
        }

//        if (noname == 0 && noemail == 0 && nophone == 0 && fphonesame == true && femailsame == true) {
//            if (is_guest == "1") {
//                guest = 1;
//            } else {
//                guest = 0;
//            }
        //            if ((islogin != "") && (isGuest == 0)) { //old session concept

//        if (islogin != "") {
        $("#guestLoginDiv").hide();
        var card_id = $("#card_id").val();
        var url = "<?php echo BASEURL; ?>send-egreeting-new";
        var frnames = $("input[name='friend_name[]']").map(function () {
            return $(this).val();
        }).get();
        var fremails = $("input[name='friend_email[]']").map(function () {
            return $(this).val();
        }).get();
        var frphones = $("input[name='fphone[]']").map(function () {
            return $(this).val();
        }).get();


        var data = {
            rfrom: 1,
            card_id: $("#card_id").val(),
            cat_id: $("#cat_id").val(),
            message: $("#message").val(),
            friend_name: JSON.stringify(frnames),
            friend_email: JSON.stringify(fremails),
            title: $("#gtitle").val(),
            schedule: $("#greetingSchedule").val(),
            greeting_img: $("#greeting_img").val(),
            gettype: $("#checktype").val(),
            imgcaptureuploaded: $("#imgcaptureuploaded").val(),
            uploaded_finalimg: $("#uploaded_finalimg").val(),
            fphone: JSON.stringify(frphones),
            sender_name: $("#sender_name").val(),
            sender_email: $("#sender_email").val(),
            sender_phone: $("#sender_phone").val(),
        }
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $("#demo").fadeIn(2000);
            },
            success: function (msg) {
                if (msg.success.toString() == "false") {
                    $("#demo").fadeOut(5000);
                    alert(msg.message.toString());
//                    window.location = "<?php // echo BASEURL;               ?>insidegreeting/" + card_id + "/0/" +<?php // echo $user_id;               ?>;
                    window.location = "<?php echo BASEURL; ?>insidegreeting-details/" + card_id + "/0/";
                } else {
                    colorbox = jQuery.colorbox;
                    colorbox.close();
                    $("#demo").hide();
                    var email_count = msg.email_list.length;
                    var name_count = msg.name_list.length;
                    var schedule = '';
                    var indicator = msg.indicator;
                    if (indicator == "0") {
                        schedule = "on " + msg.schedule;
                    } else {
                        schedule = "on " + msg.schedule;
                    }
                    var data = '<li class="list-group-item"> Your Ecard will be delivered ' + schedule + ' to the following recipient(s)</li>';
                    for (i = 0; i < name_count; i++) {
                        if (msg.email_list[i] != "") {
                            data += '<li class="list-group-item"><strong>' + msg.name_list[i] + '</strong>(' + msg.email_list[i] + ')</li>'
                        } else {
                            data += '<li class="list-group-item"><strong>' + msg.name_list[i] + '</strong>(' + msg.mobile_list[i] + ')</li>'
                        }
                    }
                    $("#emaillist").html(data);
                    $(".alertBox").css("display", "block");
                    $(".alertMessageOuterDiv").css("display", "block");
                    $('.alertMessage').animate({top: '20%'}, 500);

                }
            }
        });
//        }

//        else {
//            $("#guestLoginDiv").show();
//            $(".showloader").removeClass("show_overlay").hide();
//            switch (is_guest) {
//                case "1":
//                    {
//                        $("#login_type").val('2');
//                        doLogin(1);
//                    }
//                    break;
//                case "2":
//                    {
//                        var selectedCategory = '';
//                        $('input[name="gGender"]:checked').each(function () {
//                            selectedCategory = (this.value);
//                        });
//                        //New
//                        doSignup($("#gfirst_name").val(), $("#gsign_password").val(), $("#gmobile_no").val(), $("#gsign_email").val(), $("#gconf_password").val(), $(".gsbdate").val(), $(".gsbmonth").val(), $(".gsbyear").val(), selectedCategory, $("#gCmbCountryOption").val(), 11);
//                    }
//                    break;
//                case "3":
//                    {
//                        doLogin(3);
//                    }
//                    break;
//                case "4":
//                    {
//                        var red_type = $(".vfacebookLogin").attr("red_type");
//                        vset_sessiondata(red_type, 4);
//                    }
//                    break;
//                case "5":
//                    {
//                        var red_type = $(".vgoogleLogin").attr("red_type");
//                        vset_sessiondata(red_type, 5);
//                    }
//                    break;
//            }
//        }
    }
//    }
//    $(document).on("click", "#docheckout", function () {
//        var radioValue = $("input[name='loginradio']:checked").val();
//        performCheckout(1, radioValue);
//    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl_bg").show();
        $(".greeting_counterimg").show();
        $(".slider_free").show();
        runslider();
        getgreetingsbyLimit(0);
<?php if ($this->session->flashdata('fmsg')) { ?>
            alert('<?php echo $this->session->flashdata('fmsg'); ?>');
<?php } ?>

<?php
if ($this->session->flashdata('email_list') || $this->session->flashdata('mobile_list')) {
    $listemails = $this->session->flashdata('email_list');
    $listnames = $this->session->flashdata('name_list');
    ?>
            var email_list = "<?php echo $this->session->flashdata('email_list'); ?>";
            var emails = email_list.split(',');
            var email_count = emails.length;

            var name_list = "<?php echo $this->session->flashdata('name_list'); ?>";
            var names = name_list.split(',');
            var name_count = names.length;
            var mobile_list = "<?php echo $this->session->flashdata('mobile_list'); ?>";
            var mb = mobile_list.split(',');
            var schedule = '';
            var indicator = "<?php echo $this->session->flashdata('indicator') ?>";
            if (indicator == "0") {
                schedule = "<?php echo $this->session->flashdata('schedule') ?>";
            } else {
                schedule = "on " + "<?php echo $this->session->flashdata('schedule') ?>";
            }
            var data = '<li class="list-group-item"> Your Ecard will be delivered ,' + schedule + ' , to the following recipient(s)</li>';
            for (i = 0; i < name_count; i++) {
                if (emails[i] != "") {
                    data += '<li class="list-group-item"><strong>' + names[i] + '</strong>(' + emails[i] + ')</li>'
                } else {
                    data += '<li class="list-group-item"><strong>' + names[i] + '</strong>(' + mb[i] + ')</li>'
                }
            }
            $("#emaillist").html(data);
            $(".alertBox").css("display", "block");
            $(".alertMessageOuterDiv").css("display", "block");
            $('.alertMessage').animate({top: '20%'}, 500);
<?php } ?>
    });
    var myslider;
    var g = 1;
    var gcount = parseInt("<?php echo $gcount; ?>") - 1;
    var nexclick = false;
    $("#prdnext").click(function () {
        if (g > gcount) {
            g = 0;
        }
        console.log("g val next" + g);
        //   runslider();
        getgreetingsbyLimit(g);
        g++;
        nextclick = true;


    });
    $("#prdprv").click(function () {
        var j = g - 1;
        j--;
        if (nexclick == false) {
            g--;
        }
        if (j < 0) {
            j = parseInt(gcount);
            g = parseInt(gcount) + 1;
        }

        console.log("j value" + j + "g val" + g);
        //    runslider();
        getgreetingsbyLimit(j);
    });


    $("#message").focus(function () {
        if (this.value === this.defaultValue) {
            this.value = '';
        }
    }).blur(function () {
        if (this.value === '') {
            this.value = this.defaultValue;
        }
    });

    //when the Add Field button is clicked
    var counter = 2;
    $("#add_more").click(function () {
        if (counter <= 5) {
            //Append a new row of code to the "#items" div
            var data = '';
            data += '<li >'
                    + '<input class="name2" type="text" id="fname' + counter + '" required="required" placeholder="To Name" name="friend_name[]"><div id="fname' + counter + '_error_msg" style="color: red; width:100%; height: 16px;font-size: 12px;"></div></li>'
                    + '<li ><input id="femail' + counter + '" type="email" class="emailerror" required="required" placeholder="To Email" name="friend_email[]"> <div id="femail' + counter + '_error_msg" style="color: red; width:100%; height: 16px;font-size: 12px;"></div></li>'
                    + '<li ><input id="fphone' + counter + '" eid="' + counter + '" type="text" class="fphoneerror" required="required" placeholder="Friend‘s No" name="fphone[]"  onkeypress="return isNumberKey(event)"> <div id="fphone' + counter + '_error_msg" style="color: red; width:100%; height: 16px;font-size: 12px;"></div></li>'
            $("#items").append(data);
            counter++;
        } else {
            alert("Maximum 5 users can be added");
        }
    });
    $("#remove_users").click(function () {
        if ($('#items li').length > 3) {
            counter--;
            $('#items li:nth-last-child(3)').remove();
            $('#items li:nth-last-child(2)').remove();
            $('#items li:nth-last-child(1)').remove();
        }
    });

    $(document).on("keyup", ".name2", function () {
        var input = $(this).attr('id');
        $("#" + input + "_error_msg").html("");
    });
    $(document).on("keyup", ".emailerror", function () {
        var input = $(this).attr('id');
        $("#" + input + "_error_msg").html("");
    });
    $(document).on("keyup", ".fphoneerror", function () {
        var input = "femail" + $(this).attr('eid');
        var inputnew = $(this).attr('id');
        $("#" + input + "_error_msg").html("");
        $("#" + inputnew + "_error_msg").html("");
    });

    $('#send_card').click(function () {
//        var radioValue = $("input[name='loginradio']:checked").val();
        performCheckout();
    });

    function getgreetingsbyLimit(start) {
        var data = {
            start: start,
            type: "<?php echo $type; ?>"
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>greetings-by-limit",
            data: data,
            dataType: "json",
            success: function (r) {

                if (r.success.toString() === "false") {

                } else {
                    $("#mloadgreetings").html("");
                    var sallcarts = "", gimg = "", user_id = "";
                    user_id = "<?php echo $user_id; ?>";
                    var greeting_count = r.card_info.length;
                    for (k = 0; k < greeting_count; k++) {
                        gimg = "<?php echo BASEURL_CROPPED_GREET; ?>" + r.card_info[k]['front_page_image'];
                        sallcarts += '<li><a href="<?php echo BASEURL; ?>insidegreeting/' + r.card_info[k]["card_id"] + '/0/' + user_id + '"><img src="' + gimg + '" /></a></li>'
                    }
                    $("#mloadgreetings").html(sallcarts);
                    myslider.reloadSlider();
                    var $div = $('.slider_free a.bx-prev');
                    var nxt = $('.slider_free a.bx-next');
                    if ($div.length > 1) {
                        $div.not(':last').remove();
                        nxt.not(':last').remove();
                    }
                }
            }
        });
    }
    function runslider() {
        if (myslider) {
            slider = $('.slider_free ul').bxSlider();
            slider.destroySlider();
        }
        myslider = $('.slider_free ul').bxSlider({
            slideWidth: 94,
            minSlides: 4,
            maxSlides: 8,
            moveSlides: 1,
            slideMargin: 10,
            pager: false,
            auto: false,
            nextSelector: '#prdnext',
            prevSelector: '#prdprv',
            nextText: '&#xf105; ',
            prevText: ' &#xf104;',
            speed: 500
        });
    }

    function call_send_greeting() {
        var card_id = $("#card_id").val();
        var url = "<?php echo BASEURL; ?>send-egreeting";
        var frnames = $("input[name='friend_name[]']").map(function () {
            return $(this).val();
        }).get();
        var fremails = $("input[name='friend_email[]']").map(function () {
            return $(this).val();
        }).get();
        var frphones = $("input[name='fphone[]']").map(function () {
            return $(this).val();
        }).get();


        var data = {
            rfrom: 1,
            card_id: $("#card_id").val(),
            cat_id: $("#cat_id").val(),
            message: $("#message").val(),
            friend_name: JSON.stringify(frnames),
            friend_email: JSON.stringify(fremails),
            title: $("#gtitle").val(),
            schedule: $("#greetingSchedule").val(),
            greeting_img: $("#greeting_img").val(),
            gettype: $("#checktype").val(),
            imgcaptureuploaded: $("#imgcaptureuploaded").val(),
            uploaded_finalimg: $("#uploaded_finalimg").val(),
            fphone: JSON.stringify(frphones),
        }
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $("#demo").fadeIn(2000);
            },
            success: function (msg) {
                if (msg.success.toString() == "false") {
                    $("#demo").fadeOut(5000);
                    alert(msg.message.toString());
                    window.location = "<?php echo BASEURL ?>insidegreeting/" + card_id + "/0/" +<?php echo $user_id; ?>;
                } else {
                    colorbox = jQuery.colorbox;
                    colorbox.close();
                    $("#demo").hide();
                    var email_count = msg.email_list.length;
                    var name_count = msg.name_list.length;
                    var schedule = '';
                    var indicator = msg.indicator;
                    if (indicator == "0") {
                        schedule = "on " + msg.schedule;
                    } else {
                        schedule = "on " + msg.schedule;
                    }
                    var data = '<li class="list-group-item"> Your Ecard will be delivered ' + schedule + ' to the following recipient(s)</li>';
                    for (i = 0; i < name_count; i++) {
                        if (msg.email_list[i] != "") {
                            data += '<li class="list-group-item"><strong>' + msg.name_list[i] + '</strong>(' + msg.email_list[i] + ')</li>'
                        } else {
                            data += '<li class="list-group-item"><strong>' + msg.name_list[i] + '</strong>(' + msg.mobile_list[i] + ')</li>'
                        }
                    }
                    $("#emaillist").html(data);
                    $(".alertBox").css("display", "block");
                    $(".alertMessageOuterDiv").css("display", "block");
                    $('.alertMessage').animate({top: '20%'}, 500);

                }
            }
        });
    }
    $("#alertclose").click(function () {
        $('.alertMessage').animate({top: '0'}, 500);
        $(".alertBox").css("display", "none");
        $(".alertMessageOuterDiv").css("display", "none");
        var card_id = $("#card_id").val();
        window.location = "<?php echo BASEURL ?>insidegreeting/" + card_id + "/0/" +<?php echo $user_id; ?>;
    });

    $(".closediv").on("click", function () {
        $(".afterclick").hide();
        $(".videoclick").hide();
    });
    $("#preview").click(function () {
        var emessage = $("#greet_text").text();
        var check_type = $("#checktype").val();
        if (check_type == 1) {
            $(".img_section").show();
            var imgsrc = $("#defaultpic").attr("src");
            $("#edefaultpic").attr("src", imgsrc);
        } else if (check_type == 2) {
            $(".video_section").show();
            var vsrc = $("#videof").attr("src");
            $("#evideof").attr("src", vsrc);
        }
        $("#disp_sms").html(emessage);
        $(".outer_preview_div").show();
        var h = $(window).height() / 2 - 10;
        $('.inner_preview_div').animate({top: '10%'}, 500);
        $(".inner_preview_div").show();
    });
    $("#closepreviewbox").click(function () {
        $(".BackgroundMediaPreview").hide();
        $(".previewAudioVideoImage").hide();
        $('.previewAudioVideoImageInnerDiv').animate({top: '0%'}, 500);
    });
    $(".preview_cancel").click(function (e) {
        $("#playit").removeClass('audioPlayerPause').addClass('audioPlayer').attr("rel", "play");
        $(".playerbox").hide();
        $("#playaudioplayer").attr('src', "");
        $(".maincircle").show();
        $(".titlebox").show();
        $(".sampleimg").show();
        $("#checktype").val("0");
        //$("#audioTimePlayer").text("00:00");
        var audioPlayer = document.getElementById("playaudioplayer");
        audioPlayer.pause();
    });
    $("#close_preview").click(function () {
        $(".outer_preview_div").css("display", "none");
        $('.inner_preview_div').animate({top: '0'}, 500);
        $(".img_section").css("display", "none");
        $("#edefaultpic").attr("src", "");
        $(".video_section").css("display", "none");
        $("#evideof").attr("src", "");
    });
    var message = $("#message").val();
    $("#greet_text").text(message);
    var max = 240;
    $("#message").keyup(function () {
        var message = $(this).val();
        $("#count").text((max - $(this).val().length) + " Characters remaining ");
        $("#greet_text").html(message);
    });
    var el = $('.otext');
    setInterval(function () {
        el.toggleClass('blinking');
    }, 1000);
    $("#preview_circle").click(function () {
        $(".BackgroundMediaPreview").show();
        $(".previewAudioVideoImage").show();
        $('.previewAudioVideoImageInnerDiv').animate({top: '20%'}, 500);
    });
    if ($(window).width() > 768) {
        $(function () {
            var $blah = $("#blahnew"),
                    $window = $(window),
                    offset = $blah.offset();
            $window.scroll(function () {
                if ($window.scrollTop() > offset.top) {
                    $blah.stop().animate({
                        top: 650
                    });
                } else {
                    $blah.stop().animate({
                        top: 0
                    });
                }
            });
        });
    }

    function isNumberKey(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 37 && charCode != 38 && charCode != 39 && charCode != 40)
            return false;
        return true;
    }
    function getDate(d, m) {

        $(".errors7").html("");
        if ((d != '') && (m != '')) {
            var data = {
                bmonth: m,
                bday: d
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>zodiac-data",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#zodiac_id").val(r.message.toString());
                        return false;
                    } else {
                        $("#zodiac_id").val(r.message.toString());
                    }
                }
            });
        }
    }

</script>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>zebra_datepicker.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>core.js"></script>