<?php $this->load->view("new_templates/header"); ?>
<link rel="stylesheet" href="<?php echo BASEURL_DATE; ?>metallic.css" type="text/css"/>
<script src="<?php echo BASEURL_OJS; ?>RecordRTC.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_OJS; ?>webcam.js"></script>
<script src="<?php echo BASEURL_OJS; ?>gumadapter.js"></script>
<link rel="stylesheet" href="<?php echo BASEURL_OCSS; ?>flipclock.css" />
<script src="<?php echo BASEURL_OJS; ?>flipclock.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_OJS ?>mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="<?php echo BASEURL_OCSS; ?>videostyle.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo BASEURL_OCSS ?>pstyles.css"/>
<style>
    .section_card ul li{float:left;width:60%}
</style>
<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            Birthday<span> Greeting Card</span>
        </div>
    </div>
</div>
<form method="post" id="greeting_submit" name='greeting_data'  enctype="multipart/form-data" >
    <div class="wrapper">
        <div class="container_12">
            <div class="inner_midd dashbord">
                <div class="in_f_l">
                    <div class="leftside">
                        <div class="free_left">
                            <img src="<?php echo BASEURL_CROPPED_GREET . $reminder_info["greeting_img"]; ?>" class="selected_greeting"  />
                            <input type="hidden" name="greeting_img" id="greeting_img" value="<?php echo $reminder_info["greeting_img"]; ?>">

                            <!--                            <div class="photoupload">
                                                            <img src="<?php echo BASEURL_OIMG ?>frame.png" class="backcorners"/>
                            
                                                            <div class="photo_img">
                                                                <img src="<?php echo BASEURL_OIMG ?>img_before_upload.png" id="defaultpic" class="sampleimg" >
                                                                <div style="width:80%;margin: auto;">
                                                                    <div id="mediaprogress" class="mediauploadhide">
                                                                        <div id="mediabar"></div>
                                                                        <div id="mediapercent">0%</div >
                                                                    </div>
                                                                </div>
                                                                <div class="mpv">Uploading...</div>
                            
                                                                <div class="playerbox" >
                                                                    <div class="preview_box">
                                                                        <div class="playaudioContainer" >
                                                                            <div class="audioControlPlayer audioPlayer" rel="play" id="playit"></div>
                                                                            <audio id="playaudioplayer"  preload="auto" class="a" >
                                                                                <source src="" >
                                                                                Your browser does not support the audio element.
                                                                            </audio>
                                                                        </div> 
                                                                    </div>
                                                                    <div  class="stopPlayer">Remove Audio</div>
                                                                </div>
                            
                                                                <div class="video_container">
                                                                    <video src="" controls id="videof" class="display_video" type='video/mp4'>
                                                                        Your browser does not support the <code>video</code> element.
                                                                    </video>
                                                                    <div  class="stopVideo" id="stop_video">Remove Video</div>
                                                                </div>
                                                                <div class="remove_pic" id="removePhoto" style="cursor:pointer;">Remove Photo</div>
                            
                                                                <div class="maincircle">
                                                                    <img src="<?php echo BASEURL_OIMG ?>image.png" id="icircle" class="icircle"/>    
                                                                    <img src="<?php echo BASEURL_OIMG ?>video.png" id="ivideo" class="icircle" />
                                                                    <img src="<?php echo BASEURL_OIMG ?>audio.png" id="iaudio" class="icircle" />
                                                                </div>
                                                            </div>
                                                        </div>-->
                        </div>
                    </div>
                </div>
                <div class="in_f_r free_card_con">
                    <div class="owl_bg wrapper" style="display:none;">
                        <div class="outer_preview_div" style="background:url('<?php echo BASEURL_OIMG; ?>overlay.png')" >
                            <div class="inner_preview_div">
                                <div style="position: absolute; right: -1%; top: -1%;width: 35px;cursor: pointer;" id="close_preview">
                                    <img src="<?php echo BASEURL_OIMG; ?>closeh.png"  style=""/>
                                </div>
                                <div class="outer_greeting_div">
                                    <div class="greeting_full">
                                        <div class="g_left">
                                            <img src="<?php // echo BASEURL_CROPPED_GREET . $card_details["front_page_image"];                                                                ?>" />
                                        </div>
                                        <div class="g_right">
                                            <div class="r_con scroll_issue">
                                                <div class="disp_msgarea">
                                                    <div class="display_msg" id="disp_sms">
                                                    </div>
                                                </div>
                                                <div class="media_section">
                                                    <div class="img_section"  style="position: relative; left: 0; top: 0;">
                                                        <img src="<?php echo BASEURL_OIMG; ?>imgframe.png" class="ebackcorners" style="position: relative; top: 0; left: 0;"/>
                                                        <img src="" id="edefaultpic" class="esampleimg"  >
                                                    </div>
                                                    <div class="video_section" >
                                                        <div class="evideo_container">
                                                            <img src="<?php echo BASEURL_OIMG; ?>imgframe.png" class="ebackcorners"/>
                                                            <video src="" controls id="evideof" class="edisplay_video" autoplay="" > </video>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="demo" >
                            <div class="loader_div">
                                <img src="<?php echo BASEURL_OIMG ?>sending_email.jpg" alt="Loading" class="ajax-loader " />
                            </div>
                        </div>
                        <div class="alertMessageOuterDiv">
                            <div class="alertMessage">
                                <div class="alertclose" id="alertclose">X</div>
                                <div style="  border-bottom: 1px solid transparent;  min-height: 16.846px;  padding: 15px;"><h4 class="titlemessage">Your Greeting Card has been sent.</h4></div>
                                <div class="alertInnerMessage">
                                    <ul style=" padding-left: 0" id="emaillist">
                                        <li class="list-group-item">
                                            Your Ecard will be delivered to the following recipient(s)
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--Image capture:start-->
                        <?php
                        $data['from'] = 2;
                        $this->load->view("EGIFT/image_capture", $data);
                        ?>
                        <!--Image capture:End-->
                        <!--audio Recording:start-->
                        <?php $this->load->view("EGIFT/audio_recording"); ?>
                        <!--Recording:End-->
                        <!--video-recording:start-->
                        <?php $this->load->view("EGIFT/video_recording"); ?>
                        <!--video-recording:end-->
                        <div class="section_card wrapper">
                            <h3>
                                MAKE IT PERSONAL : Design your greeting</h3>
                        </div>
                        <div class="section_card wrapper bg_grey">
                            <h4>
                                Recipients</h4>
                            <ul class="mt10 two" id="items">
                                <li>
                                    <input class="name2" type="text"  placeholder="To Name" name="friend_name[]" id="fname1"  value="<?php echo $reminder_info["fname"] ?>"/>
                                    <div id="fname1_error_msg" style=" color: red; width:100%; height: 16px;font-size: 12px;"></div>
                                </li>
                                <li>
                                    <input type="email" class="emailerror"  placeholder="To Email" name="friend_email[]" id="femail1" value="<?php echo $reminder_info["femail"] ?>"/>
                                    <div id="femail1_error_msg" style=" color: red; width: 100%; height: 16px;font-size: 12px;"></div>
                                </li>
                                <li>
                                    <input type="text" class="fphoneerror" placeholder="Friend‘s No"   name="fphone[]" value="<?php echo $reminder_info["fphone"] ?>" id="fphone1" eid="1" onkeypress="return isNumberKey(event)"/>
                                    <div id="fphone1_error_msg" style=" color: red; width: 100%; height: 16px;font-size: 12px;"></div>
                                </li>
                            </ul>
                            <ul class="mt10 two">
                                <li>
                                    <a style="cursor:pointer;" id="add_more" class="add_recipient">+ Add Recipient</a>
                                </li>
                                <li>
                                    <a style="cursor:pointer;" id="remove_users" class="remove_recipient">- Remove Recipient</a>
                                </li>
                            </ul>
                        </div>
                        <?php
                        $curr_date = new DateTime(date("Y-m-d"));

//                        $db_date = $reminder_info["delivery_date"];
//                        $sdate = date("Y-m-d", strtotime($db_date));
//                        $datetime1 = new DateTime($sdate);
//                        $datetime2 = new DateTime(date("Y-m-d"));
//                        $interval = $datetime1->diff($datetime2);
//                        $date_diff = $interval->format('%a');
////                        print_r($date_diff);
////                        exit;
//                        if ($date_diff > 0) {
//                            $db_date = new DateTime(date("Y-m-d"));
//                        } else {
//                            $db_date = date_create($db_date);
//                        }
                        ?>
                        <div class="section_card wrapper">
                            <ul class="info_con">
                                <li>
                                    <h2>
                                        When would you like this delivered (DD/MM/YYYY)?</h2>
                                    <input id="greetingSchedule" type="text" placeholder="<?php echo date('d/m/Y'); ?>" value="<?php echo date_format($curr_date, 'd/m/Y'); ?>" name="schedule" style="cursor:pointer;" >
                                    <div><b>Note:</b>&nbsp;Your Delivery will be done between 10am to 10pm.</div>
                                </li>
                                <li>
                                    <h2>
                                        Subject</h2>
                                    <input type="text"  placeholder="Your card is waiting for your !" name="title" id="gtitle" value="<?php echo $reminder_info["title"] ?>"/>
                                </li>
                                <li>
                                    <h2>
                                        Add your personal message</h2>
                                    <textarea  rows="8" maxlength="240" name="message" id="message"><?php echo $reminder_info["message"] ?></textarea>
                                    <div id="count" class="count"></div>
                                    <div id="greet_text" style="display:none;"></div>
                                </li>
                                <li>
                                    <input type="hidden"  name="gettype" id="checktype" value="0"  />
                                    <input type="hidden" id="uploaded_finalimg" name="uploaded_finalimg"  value="">
                                    <input type="hidden" name="cat_id" value="<?php // echo $card_details["greet_cat_id"];                                                                 ?>">
                                    <input type="hidden"  name="card_id" id="card_id" value="<?php // echo $card_details["card_id"];                                                                 ?>">
                                    <input type="hidden" name="email" id="freeemail"  value="" >
                                    <input type="hidden" name="password" id="freepass" value="" >
                                    <!--                                    <div><b>Note:</b>&nbsp;Only 10 greetings will be free.</div>
                                                                        <div class="greetingsignup"> 
                                                                            <a href="#SignUp" class="inline_colorbox memsignup" id="signuppopup" data-width="30%">  </a>
                                                                            For unlimited sending of greetings,<a id="signupforgreetings" style="cursor: pointer;"> (sign up)</a> now.
                                                                        </div>-->
                                </li>
                                <li class=" wrapper mb20 text-center">
                                    <a href="#openpopup" class="inline_colorbox memlogin" id="login_popup" data-width="30%">  </a>
                                    <div class="arrowbtn" id="send_card" >SUBMIT</div>  
                                    <div class="arrowbtn" id="preview" >PREVIEW</div>
                                    <div style="height:108px;"></div>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--                    <div class="slider_free wrapper" style="display:none" >
                                            <b>You May also like</b> <span id="prdnext"></span><span id="prdprv"></span>
                    
                                            <ul id="mloadgreetings">
                                            </ul>
                                        </div>-->
                </div>
                <div class="in_f_l greeting_counterimg" style="text-align: center;width:15%;display:none;position: relative;" id="blahnew">
                    <div  class="insideowl_text"><div class="otext"><?php echo $greeting_count; ?></div></div>
                </div>
            </div>
        </div>
    </div>
</form>   
<div class="alertBox"></div>
<div class='BackgroundMedia'></div>
<div class='BackgroundMediaPreview'></div>

<?php $this->load->view("new_templates/vouchers_footer"); ?>
<link href="<?php echo BASEURL_OCSS; ?>jquery.loader.css" rel="stylesheet" />  <!--page loading -->

<script type="text/javascript">

    $(document).ready(function () {
//        window.alert(window.location.href.substr(window.location.href.lastIndexOf('/') + 1));
        $(".owl_bg").show();
        $(".greeting_counterimg").show();
        $(".slider_free").show();
        runslider();
        getgreetingsbyLimit(0);



<?php if ($this->session->flashdata('fmsg')) { ?>
            alert('<?php echo $this->session->flashdata('fmsg'); ?>');
<?php } ?>

<?php
if ($this->session->flashdata('email_list') || $this->session->flashdata('mobile_list')) {
    $listemails = $this->session->flashdata('email_list');
    $listnames = $this->session->flashdata('name_list');
    ?>
            var email_list = "<?php echo $this->session->flashdata('email_list'); ?>";
            var emails = email_list.split(',');
            var email_count = emails.length;

            var name_list = "<?php echo $this->session->flashdata('name_list'); ?>";
            var names = name_list.split(',');
            var name_count = names.length;
            var mobile_list = "<?php echo $this->session->flashdata('mobile_list'); ?>";
            var mb = mobile_list.split(',');
            var schedule = '';
            var indicator = "<?php echo $this->session->flashdata('indicator') ?>";
            if (indicator == "0") {
                schedule = "<?php echo $this->session->flashdata('schedule') ?>";
            } else {
                schedule = "on " + "<?php echo $this->session->flashdata('schedule') ?>";
            }
            var data = '<li class="list-group-item"> Your Ecard will be delivered ,' + schedule + ' , to the following recipient(s)</li>';
            for (i = 0; i < name_count; i++) {
                if (emails[i] != "") {
                    data += '<li class="list-group-item"><strong>' + names[i] + '</strong>(' + emails[i] + ')</li>'
                } else {
                    data += '<li class="list-group-item"><strong>' + names[i] + '</strong>(' + mb[i] + ')</li>'
                }



            }
            $("#emaillist").html(data);
            $(".alertBox").css("display", "block");
            $(".alertMessageOuterDiv").css("display", "block");
            $('.alertMessage').animate({top: '20%'}, 500);
<?php } ?>


//    if ((islogin != "") && (isGuest == 0)) {
//        $(".greetingsignup").css("display", "none");
//    } else {
//        $(".greetingsignup").css("display", "block");
//    }



    });
    var myslider;
    var g = 1;
    var gcount = parseInt("<?php echo $gcount; ?>") - 1;
    var nexclick = false;
    $("#prdnext").click(function () {
        if (g > gcount) {
            g = 0;
        }
        console.log("g val next" + g);
        //   runslider();
        getgreetingsbyLimit(g);
        g++;
        nextclick = true;


    });
    $("#prdprv").click(function () {
        var j = g - 1;
        j--;
        if (nexclick == false) {
            g--;
        }
        if (j < 0) {
            j = parseInt(gcount);
            g = parseInt(gcount) + 1;
        }

        console.log("j value" + j + "g val" + g);
        //    runslider();
        getgreetingsbyLimit(j);
    });


    $("#message").focus(function () {
        if (this.value === this.defaultValue) {
            this.value = '';
        }
    }).blur(function () {
        if (this.value === '') {
            this.value = this.defaultValue;
        }
    });

    //when the Add Field button is clicked
    var counter = 2;
    $("#add_more").click(function () {
        if (counter <= 5) {
            //Append a new row of code to the "#items" div
            var data = '';
            data += '<li >'
                    + '<input class="name2" type="text" id="fname' + counter + '" required="required" placeholder="To Name" name="friend_name[]"><div id="fname' + counter + '_error_msg" style="color: red; width:100%; height: 16px;font-size: 12px;"></div></li>'
                    + '<li ><input id="femail' + counter + '" type="email" class="emailerror" required="required" placeholder="To Email" name="friend_email[]"> <div id="femail' + counter + '_error_msg" style="color: red; width:100%; height: 16px;font-size: 12px;"></div></li>'
                    + '<li ><input id="fphone' + counter + '" eid="' + counter + '" type="text" class="fphoneerror" required="required" placeholder="Friend‘s No" name="fphone[]"  onkeypress="return isNumberKey(event)"> <div id="fphone' + counter + '_error_msg" style="color: red; width:100%; height: 16px;font-size: 12px;"></div></li>'
            $("#items").append(data);
            counter++;
        } else {
            alert("Maximum 5 users can be added");
        }
    });
    $("#remove_users").click(function () {
        if ($('#items li').length > 3) {
            counter--;
            $('#items li:nth-last-child(3)').remove();
            $('#items li:nth-last-child(2)').remove();
            $('#items li:nth-last-child(1)').remove();
        }
    });

    $(document).on("keyup", ".name2", function () {
        var input = $(this).attr('id');
        $("#" + input + "_error_msg").html("");
    });
    $(document).on("keyup", ".emailerror", function () {
        var input = $(this).attr('id');
        $("#" + input + "_error_msg").html("");
    });
    $(document).on("keyup", ".fphoneerror", function () {
        var input = "femail" + $(this).attr('eid');
        var inputnew = $(this).attr('id');
        $("#" + input + "_error_msg").html("");
        $("#" + inputnew + "_error_msg").html("");
    });

    $('#send_card').click(function () {
        send_greetingvalidation(1);
    });

    $("#signupforgreetings").click(function () {
        send_greetingvalidation(2);
    });

    function getgreetingsbyLimit(start) {
        var data = {
            start: start,
            type: "<?php echo $type; ?>"
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>greetings-by-limit",
            data: data,
            dataType: "json",
            success: function (r) {

                if (r.success.toString() === "false") {

                } else {
                    $("#mloadgreetings").html("");
                    var sallcarts = "", gimg = "", user_id = "";
                    user_id = "<?php echo $user_id; ?>";
                    var greeting_count = r.card_info.length;
                    for (k = 0; k < greeting_count; k++) {
                        gimg = "<?php echo BASEURL_CROPPED_GREET; ?>" + r.card_info[k]['front_page_image'];
                        sallcarts += '<li><a href="<?php echo BASEURL; ?>insidegreeting/' + r.card_info[k]["card_id"] + '/0/' + user_id + '"><img src="' + gimg + '" /></a></li>'
                    }
                    $("#mloadgreetings").html(sallcarts);
//                    myslider.reloadSlider();
                    var $div = $('.slider_free a.bx-prev');
                    var nxt = $('.slider_free a.bx-next');
                    if ($div.length > 1) {
                        $div.not(':last').remove();
                        nxt.not(':last').remove();
                    }
                }
            }
        });
    }


    function runslider() {
        if (myslider) {
            slider = $('.slider_free ul').bxSlider();
            slider.destroySlider();
        }
        myslider = $('.slider_free ul').bxSlider({
            slideWidth: 94,
            minSlides: 4,
            maxSlides: 8,
            moveSlides: 1,
            slideMargin: 10,
            pager: false,
            auto: false,
            nextSelector: '#prdnext',
            prevSelector: '#prdprv',
            nextText: '&#xf105; ',
            prevText: ' &#xf104;',
            speed: 500
        });
    }


    function send_greetingvalidation(from) {
        // $("#demo").fadeIn(5000);
        var count = counter - 1;
        var message1 = '';
        var message2 = '';
        var message3 = '';
        var message4 = '';
        var message5 = '';
        var success = false;
        var noname = 0, noemail = 0, nophone = 0;
        var femailsame = true;
        var fphonesame = true;
        for (i = 1; i <= count; i++) {
            var femail = $("#femail" + i).val();
            var fname = $("#fname" + i).val();
            var fphone = $("#fphone" + i).val();

            message1 = "Enter Friend's Name";
            message2 = "Enter Friend's Email or  Mobile No";
            message3 = "Enter Friend's Mobile No";
            message4 = "Invalid Email Address";
            message5 = "Please enter valid Indian Mobile No";
            if (fname == '') {
                noname = 1;
                $("#fname" + i + "_error_msg").html(message1);
            }
            if (femail == "" && fphone == "") {
                noemail = 1;
                $("#femail" + i + "_error_msg").html(message2);
                $('html, body').animate({
                    scrollTop: $(".bg_grey").offset().top
                }, 2000);
            } else if ((!(validateEmail(femail))) && (femail != '')) {
                noemail = 1;
                $("#femail" + i + "_error_msg").html(message4);
                $('html, body').animate({
                    scrollTop: $(".bg_grey").offset().top
                }, 2000);
            } else if ((!(checkMobileValid(fphone))) && (fphone != '')) {
                nophone = 1;
                $("#fphone" + i + "_error_msg").html(message5);
                $('html, body').animate({
                    scrollTop: $(".bg_grey").offset().top
                }, 2000);
            }
            if (femail != '') {
                for (j = i + 1; j <= count; j++) {
                    if ($("#femail" + [i]).val() == $("#femail" + [j]).val()) {
                        $("#femail" + i + "_error_msg").html("Email-address can't be same");
                        $("#femail" + j + "_error_msg").html("Email-address can't be same");
                        femailsame = false;
                        $('html, body').animate({
                            scrollTop: $(".bg_grey").offset().top
                        }, 2000);
                    } else {
                        $("#femail" + i + "_error_msg").html("");
                        $("#femail" + j + "_error_msg").html("");
                        femailsame = true;
                    }
                }
            }
            if (fphone != "") {
                for (j = i + 1; j <= count; j++) {
                    if ($("#fphone" + [i]).val() == $("#fphone" + [j]).val()) {
                        $("#fphone" + i + "_error_msg").html("Mobile numbers can't be same");
                        $("#fphone" + j + "_error_msg").html("Mobile numbers can't be same");
                        fphonesame = false;
                        $('html, body').animate({
                            scrollTop: $(".bg_grey").offset().top
                        }, 2000);
                    } else {
                        $("#fphone" + i + "_error_msg").html("");
                        $("#fphone" + j + "_error_msg").html("");
                        fphonesame = true;
                    }
                }
            }
        }
        if (noname == 0 && noemail == 0 && nophone == 0 && fphonesame == true && femailsame == true) {
            if ((islogin != "") && (isGuest == 0)) {
                call_send_greeting();
//                check_sent_greetings_counter();
            } else {
                if (from == 1) {
                    $("#login_type").val('5');
                    $(".facebookLogin").attr("red_type", "22");
                    $(".googleLogin").attr("red_type", "22");
                    $(".change_id").attr("id", "freegreetinglogin");
                    $("#submit").attr("redirect", "2");
                    $('#login_popup').click(); //triggered href using id of a tag in jqeury
                } else {
                    $("#submit").attr("redirect", "2");
                    $('#signuppopup').click();
                }
            }
        }
    }

    $(document).on("click", "#freegreetinglogin", function () {

        var remail = $("#remail").val();
        var password = $("#rpassword").val();
        var login_type = $("#login_type").val();
        var card_id = $("#card_id").val();
        //checkout-2 ,homepage-3,category-4,free_greeting-5

        if (remail == '') {
            alert("Enter Email Address");
        } else if (!(validateEmail(remail)) && remail != "") {
            alert("Not a Valid Email Address");

        } else if (password == '') {
            alert("Enter Password");
        } else {
            commonfreegretinglogin(remail, password, login_type);
        }
    });


    function commonfreegretinglogin(remail, password, login_type) {
        $("#demo").fadeIn(5000);
        var data = {
            email: remail,
            password: password,
            login_type: login_type,
            is_guest: 0
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>login/1",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {

                    $("#demo").hide();
                    alert(r.message.toString());
                    $('html, body').animate({
                        scrollTop: $("#colorbox").offset().top
                    }, 2000);
                    $('#rpassword').val("");

                } else {
                    $("#cboxOverlay").css("display", "none");
                    $("#colorbox").css("display", "none");
                    $("#freeemail").val(remail);
                    $("#freepass").val(password);
//                    check_sent_greetings_counter();
                    call_send_greeting();

                }
            }
        });


    }

    function call_send_greeting() {
        var card_id = $("#card_id").val();
        var url = "<?php echo BASEURL; ?>send-egreeting";
        var frnames = $("input[name='friend_name[]']").map(function () {
            return $(this).val();
        }).get();
        var fremails = $("input[name='friend_email[]']").map(function () {
            return $(this).val();
        }).get();
        var frphones = $("input[name='fphone[]']").map(function () {
            return $(this).val();
        }).get();
        var fid = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);

        var data = {
            rfrom: 1,
            card_id: $("#card_id").val(),
            cat_id: $("#cat_id").val(),
            message: $("#message").val(),
            friend_name: JSON.stringify(frnames),
            friend_email: JSON.stringify(fremails),
            title: $("#gtitle").val(),
            schedule: $("#greetingSchedule").val(),
            greeting_img: $("#greeting_img").val(),
            gettype: $("#checktype").val(),
            imgcaptureuploaded: $("#imgcaptureuploaded").val(),
            uploaded_finalimg: $("#uploaded_finalimg").val(),
            fphone: JSON.stringify(frphones),
            fid: fid
        }
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $("#demo").fadeIn(2000);
            },
            success: function (msg) {
                if (msg.success.toString() == "false") {
                    $("#demo").fadeOut(5000);
                    alert(msg.message.toString());
                    window.location = "<?php echo BASEURL; ?>insidegreeting/" + card_id + "/0/" + "<?php echo $user_id; ?>";
                    window.location = "<?php echo WEB_ORDER_HISTORY; ?>";

                } else {
                    colorbox = jQuery.colorbox;
                    colorbox.close();
                    $("#demo").hide();
                    var email_count = msg.email_list.length;
                    var name_count = msg.name_list.length;
                    var schedule = '';
                    var indicator = msg.indicator;
                    if (indicator == "0") {
                        schedule = "on " + msg.schedule;
                    } else {
                        schedule = "on " + msg.schedule;
                    }
                    var data = '<li class="list-group-item"> Your Ecard will be delivered ' + schedule + ' to the following recipient(s)</li>';
                    for (i = 0; i < name_count; i++) {
                        if (msg.email_list[i] != "") {
                            data += '<li class="list-group-item"><strong>' + msg.name_list[i] + '</strong>(' + msg.email_list[i] + ')</li>'
                        } else {
                            data += '<li class="list-group-item"><strong>' + msg.name_list[i] + '</strong>(' + msg.mobile_list[i] + ')</li>'
                        }
                    }
                    $("#emaillist").html(data);
                    $(".alertBox").css("display", "block");
                    $(".alertMessageOuterDiv").css("display", "block");
                    $('.alertMessage').animate({top: '20%'}, 500);

                }
            }, });
    }
    $("#alertclose").click(function () {
        $('.alertMessage').animate({top: '0'}, 500);
        $(".alertBox").css("display", "none");
        $(".alertMessageOuterDiv").css("display", "none");
        var card_id = $("#card_id").val();
//        window.location = "<?php // echo BASEURL             ?>insidegreeting/" + card_id + "/0/" + "<?php // echo $user_id;              ?>";
        window.location = "<?php echo WEB_ORDER_HISTORY; ?>";
    });




    $(".closediv").on("click", function () {
        $(".afterclick").hide();
        $(".videoclick").hide();

    });

    $("#preview").click(function () {
        var emessage = $("#greet_text").text();
        var check_type = $("#checktype").val();
        if (check_type == 1) {
            $(".img_section").show();
            var imgsrc = $("#defaultpic").attr("src");
            $("#edefaultpic").attr("src", imgsrc);

        } else if (check_type == 2) {
            $(".video_section").show();
            var vsrc = $("#videof").attr("src");
            $("#evideof").attr("src", vsrc);

        }
        $("#disp_sms").html(emessage);
        $(".outer_preview_div").show();

        var h = $(window).height() / 2 - 10;
        $('.inner_preview_div').animate({top: '10%'}, 500);
        //   $(".inner_preview_div").show();
    });

    $("#closepreviewbox").click(function () {
        $(".BackgroundMediaPreview").hide();
        $(".previewAudioVideoImage").hide();
        $('.previewAudioVideoImageInnerDiv').animate({top: '0%'}, 500);
    });

    $(".preview_cancel").click(function (e) {
        $("#playit").removeClass('audioPlayerPause').addClass('audioPlayer').attr("rel", "play");
        $(".playerbox").hide();
        $("#playaudioplayer").attr('src', "");
        $(".maincircle").show();
        $(".titlebox").show();
        $(".sampleimg").show();
        $("#checktype").val("0");
        //$("#audioTimePlayer").text("00:00");
        var audioPlayer = document.getElementById("playaudioplayer");
        audioPlayer.pause();

    });

    $("#close_preview").click(function () {
        $(".outer_preview_div").css("display", "none");
        $('.inner_preview_div').animate({top: '0'}, 500);
        $(".img_section").css("display", "none");
        $("#edefaultpic").attr("src", "");
        $(".video_section").css("display", "none");
        $("#evideof").attr("src", "");
    });
    var message = $("#message").val();
    $("#greet_text").text(message);
    var max = 240;
    $("#message").keyup(function () {
        var message = $(this).val();
        $("#count").text((max - $(this).val().length) + " Characters remaining ");
        $("#greet_text").html(message);
    });

    var el = $('.otext');
    setInterval(function () {
        el.toggleClass('blinking');
    }, 1000);


    $("#preview_circle").click(function () {
        $(".BackgroundMediaPreview").show();
        $(".previewAudioVideoImage").show();
        $('.previewAudioVideoImageInnerDiv').animate({top: '20%'}, 500);
    });
    if ($(window).width() > 768) {
        $(function () {
            var $blah = $("#blahnew"),
                    $window = $(window),
                    offset = $blah.offset();

            $window.scroll(function () {
                if ($window.scrollTop() > offset.top) {
                    $blah.stop().animate({
                        top: 650
                    });

                } else {
                    $blah.stop().animate({
                        top: 0
                    });

                }
            });
        });
    }

    function isNumberKey(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 37 && charCode != 38 && charCode != 39 && charCode != 40)
            return false;
        return true;
    }
    function getDate(d, m) {

        $(".errors7").html("");
        if ((d != '') && (m != '')) {
            var data = {
                bmonth: m,
                bday: d
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>zodiac-data",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#zodiac_id").val(r.message.toString());
                        return false;
                    } else {
                        $("#zodiac_id").val(r.message.toString());
                    }
                }
            });
        }
    }

</script>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>zebra_datepicker.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>core.js"></script>
