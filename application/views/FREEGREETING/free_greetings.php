<?php $this->load->view("new_templates/header"); ?>

<script src="<?php echo BASEURL_OJS; ?>jPages.js"></script>

<style type="text/css">
 .sortinfo{
        padding: 60px;
        text-align: justify;
        font-family:"gotham-book";
        font-size:15px;
    }
   
    .loader_div{
        border-radius: 11px;
        padding-top:35px;
        background-color: white;
        text-align: center;
        height:150px;
        width: 136px;
        margin: auto;
        left:0; right:0;
        top:0; bottom:0;
        position: absolute;
    }
    .ajax-loader {

        margin: auto;
        left:0; right:0;
        top:0; bottom:0;
        position: absolute;

    }
    @media only screen and (max-width: 767px){

		 .sortinfo
	{
        padding: 10px;
        text-align: justify;
        font-family:"gotham-book";
        font-size:15px;
    }

    }

</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 col-xs-12 no-padd inner-top-slider">
            <img src="<?php echo BASEURL_ONEWIMG ?>free_greetings.jpg" class="img-responsive" alt="free birthday greeting cards">
        </div>
    </div>
</div>
<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            Free <span>Greeting Cards</span>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <div class="inner_midd ">
            <div class="in_f_r" style="background:transparent;border:none;">
                <div class="prd_heading wrapper" style="font-family: gothammedium;">
                    <span style="text-align:center;">Make It Personal : Design Your Greeting</span>
                    <abbr style="text-align:center;">Choose Your “BIRTHDAY” Design Below</abbr>
                </div>
                <ul class="grg_selection wrapper mt20" id="greeting_images">
                    <?php foreach ($greeting_cards as $cards) { ?>
                        <li>
                            <a card_id="<?php echo $cards["card_id"]; ?>"class="greeting_card" style="cursor:pointer;">
                                <img  src="<?php echo BASEURL_CROPPED_GREET . $cards["front_page_image"]; ?>" alt="<?php echo $cards["card_name"];?>" />
                                <span >QUICK LOOK</span>
                            </a>
                        </li>                    
                    <?php } ?>
                </ul>
                <div class="wrapper">
                    <?php
//                    echo '<pre>';
//                    print_r($greeting_cards);
//                    exit;
                    if (count($greeting_cards) > 0) {
                        ?>
<!--                        <div class="wrapper defaults" id="content" >
                            <div class="holder text-center" style="cursor:pointer;"></div>
                        </div>-->
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="container-fluid sortinfo">
    <h1 style="font-size: 22px;text-align: center;font-weight: 600;" >Free Birthday Greeting Cards</h1>
    <h2 style="font-size:22px;">Free Birthday Cards</h2>
    <p>When you are reminded of a birthday and need to send something nice and quick, we have some of the finest <a href="https://www.birthdayowl.com/free-birthday-greeting-cards"><b>Free Greeting Cards</b></a> lined up for you. Saves you time and money for sure!</p>

    <p>Our wide selection of free birthday greeting cards are designed appropriately where you also have the option of inputting your own choice of text to make it even more personal. We deliver your birthday greeting card on your scheduled date and time by email and also send a text notification to your recipient’s mobile phone that your birthday card is waiting in their inbox. And its free!</p>

    <p>We also have great options of branded gift cards which you can purchase and send to your recipient. These are instantly redeemable by your recipient at their own time and convenience. If you are not sure what they may like to receive, then simply gift them a holiday or direct them to a nice restaurant or even to a shopping mall for that matter. This way, they get the ultimate choice to decide whatever they want to do next.</p>

    <p>If you prefer something more colorful and physical, you can choose from our wonderful collection of flowers, cakes and more. It will show on your recipient’s face when they receive our gifts!</p>
    <h2 style="font-size:22px;">Free Greeting Cards</h2>
    <p>We provide you with plenty of options of <b>Free Birthday Cards</b> to make your loved ones' birthday special.</p>

    <p>What better than having the choice to send a great e-greeting card together with audio or video messages which you can either record yourself or simply attach an earlier downloaded file.  We can assure you that your gesture will bring lots of smiles and happiness to your loved ones. And all this for free!</p>

    <p>Our wide selection of birthday greeting cards are designed appropriately where you also have the option of inputting your own choice of text to make it even more personal. We deliver your birthday greeting card on your scheduled date and time by email and also send a text notification to your recipient’s mobile phone that your e-greeting card is waiting in their inbox.</p>

    <p>All our greeting cards are free, so you can use as many as you like and even send all your yearly wishes to all your friends and family in one sitting. Simply input the dates and times when you wish these respective cards to be delivered and we will do the rest. We will even notify you – the sender – on the day the recipient views your card!</p>
    <h2 style="font-size:22px;">Birthday Greeting Cards</h2> 
    <p>Birthdays are important, so we just make sure you will remember all your important birthdays once you have keyed them all once in our  <a href="https://www.birthdayowl.com/birthday-reminder"><b>Birthday Reminder</b></a>. </p>

    <p>What better than having the choice to send a great e-greeting card together with audio or video messages which you can either record yourself or simply attach an earlier downloaded file.  Either ways, you stand to win, as both options are terrific. </p>

    <p>Our wide selection of birthday greeting cards are designed appropriately where you also have the option of inputting your own choice of text to make it even more personal. We deliver your birthday greeting card on your scheduled date and time by email and also send a text notification to your recipient’s mobile phone that your e-greeting card is waiting in their inbox. </p>

    <p>All our greeting cards are free, so you can use as many as you like and even send all your yearly wishes to all your friends and family in one sitting. Simply input the dates and times when you wish these respective cards to be delivered and we will do the rest. We will even notify you – the sender – on the day the recipient views your card! </p>
    <h2 style="font-size:22px;">Online Birthday Cards</h2>
    <p>The beauty of a gift lies in the eyes of the recipient. Why settle for ordinary gifts when you can have the best gift options at your fingertips with Birthdayowl™</p>

    <p>If you prefer to send something for free, then you can choose to select from our awesome collection of <a href="https://www.birthdayowl.com/free-birthday-greeting-cards"><b>Birthday Greeting Cards</b></a> which you can also customize with your own personal audio or video recordings, or attach a song or video which you have downloaded from the internet. It’s all so easy to do.</p>

    <p>Choose from a wide range of gift card brands which are instantly redeemable by your recipient at their own time and convenience. If you are not sure what specifically they may like to receive, then simply gift them a store or restaurant or even a shopping website or holiday voucher – so that you give them the freedom to choose what they want, and whenever they want to avail it.</p>

    <p>If you prefer something more tangible where they can touch and feel, go on and choose from our nice collection of flowers, cakes and more. We aim to please and it will show on your recipient’s face when they receive our gifts!</p>

    

    
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript">

    $(".clickworks").click(function () {
        $("#hpopup").addClass("hwpopup");
        $('.inner_box').show();
        $('.inner_box').animate({top: '20%'}, 500);
        $('html, body').animate({
            scrollTop: $(".inner_box").offset().top
        }, 2000);
    });

    $(".hclose").click(function () {
        $(" #hpopup").removeClass("hwpopup");
        $('.inner_box').css("display", "none");

    });

    $(document).ready(function () {

<?php if ($this->session->flashdata('fmsg')) { ?>
            alert('<?php echo $this->session->flashdata('fmsg'); ?>');
<?php } ?>
    });


    /* pagination*/
    $("div.holder").jPages({
        containerID: "greeting_images",
        perPage: 28,
        startPage: 1,
        startRange: 1,
        midRange: 4,
        endRange: 1
    });
    $(".holder").click(function () {
        $('html, body').animate({
            scrollTop: $(".prd_heading").offset().top
        }, 500);
    });
    $(".greeting_card").click(function () {
        var card_id = $(this).attr("card_id");
        window.location = "<?php echo BASEURL; ?>insidegreeting/" + card_id + "/0/" +<?php echo $fid; ?>;
    });



</script>
<script type="text/javascript">
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-92768886-1', 'auto');
    ga('send', 'pageview');</script>


