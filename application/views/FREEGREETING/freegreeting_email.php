<?php $this->load->view("new_templates/header"); ?>  
<style type="text/css">
    .greeting_full
    {
        margin-top: 0px !important;
    }
    .dashbord_nav1 li:hover:before, .dashbord_nav1 li.active:before
    {
        content: none !important;
    }
    .dashbord_nav1 li a
    {
        font-size: 1.0em !important;
    }

    .esub{
        position: relative;
        top:-148px;
        text-align:center;
    }

    .esubemail{
        width: 68% !important;
        margin-top: 32px; height: 26px;
    }

    .msgarea{
        word-wrap: break-word;
        height: 230px;
        width: 100%;
    }
    .display_msg{
        width: 100%;
    }
    .newletter_owl{
        margin:auto;
    }
    .photoupload{
        /*background: rgba(0, 0, 0, 0) url("../../public/assets/images/imgframe.png") no-repeat scroll 8px 0px;*/
        height: 184px;
        margin-top:159px;
        width: 100%;
    }
    .backcorners{
        width: 100%;
        height: 219px;
    }
    .photo_img{
        margin-top: -186px;
    }
    .sampleimg{
        height: 182px;
        margin-top: -19px;
        /*width: 91%;*/

    }
    .video_container{
        height: 181px;
        position: absolute;
        width: 100%;


    }
    .stopVideo{
        color: white;
        left: 74%;
        position: absolute;
        top: 14px;
        z-index: 30;

    }

    .display_video{
        background: #000 none repeat scroll 0 0;
        bottom: 0;
        height: 106%;
        left: -6%;
        overflow: hidden;
        position: relative;
        top: -11%;
        width: 79%;
    }
    .bredcumb li { display:inline-block;  }
    .bredcumb li:after { content:'/'; margin:0 10px}
    .bredcumb li:last-child:after { display:none;}
    .bredcumb li a { color:#575757;}
    .bredcumb li a.active { color:#D30003;}
    .display_audio{
        bottom: 0;
        height: 106%;
        left: -10%;
        overflow: hidden;
        position: relative;
        top: -80%;
        width: 67%;
    }
    .freegreeting_links1{
        margin-top: -48px; 
        padding:5px;
    }
    .glink{
        width: 98%;
    }
    .glink li a {
        color:black;
    }
    .brown_color{
        background-color: beige;
    }
    .outer{
        width:75%;
    }
    .preview_box{
        background: black none repeat scroll 0 0;
        height: 189px;
        margin: auto;


        position: relative;
        top: -17px;
        width: 91%;
    }
    .audioPlayer{
        background:url('<?php echo BASEURL_OIMG ?>playvideo.png')  no-repeat scroll -2px 0px;
        background-size: 67px;
        height:269px;
    }

    .audioPlayerPause{
        background:url('<?php echo BASEURL_OIMG ?>pause.png') no-repeat scroll -2px 0px;
        background-size: 67px;
        height:269px;
    }
    .playaudioContainer{
        cursor: pointer;
        position:relative;
        top:52px;
        left:38%;
        height: 46px;


    }


    .leftside span {
        left: 0 !important;
        right: 0;
        margin: auto;
        display: block;
        text-align: center;
        margin-bottom: 15px;
    }

    .leftside br{
        display: none;
    }

    @media only screen and (max-width: 1280px) {

        .newletter_owl{
            left:2%;
        }

    }
    @media only screen and (max-width:768px)
    {
        .r_con{
            height: 424px;
            margin-left: 22px;

        }
        .msgarea{
            height: 186px;
        }

        .backcorners{
            height: 188px;
        }
        .sampleimg{
            /*width: 72%;*/
            height: 148px;
            margin-top:17px;
        }
        .photoupload{
            margin-top: 176px;
        }
        .display_video{
            height: 90%;
            left: -8%;
            top: 5%;
            width: 77%;
        }
        .newletter_owl{
            position:static;
        }
        .esubemail{
            width: 34% !important;
            margin-top:17px;
        }

    }   

    @media only screen and (max-width:600px)
    {
        .display_video{

            left: -10%;

            width: 74%;
        }

        .esubemail{
            width: 44% !important;
            margin-top:17px;
        }

    }

    @media only screen and (max-width:480px)
    {
        .display_video{

            left: -11%;

            width: 71%;
        }
        .esubemail{
            width: 58% !important;

        }


    }
    @media only screen and (max-width:350px)
    {
        .display_video{
            left: -17%;
            width: 59%;
        }
    }

    @media only screen and (max-width: 991px){

        .greeting_full .g_left img {
            width: 50%;
            max-width: 100%;
            margin: auto;
            padding-bottom: 10px;
            display: block;
        }

        .subscriber-cont {
            border: 0px solid #000;
            max-width: 315px;
            margin: auto;
        }

        .greeting_full .r_con{
            height: 300px;
            margin-left: 0;
        }

        .msgarea{
            font-size: 18px;
        }

        a.pdfdownload {
            padding: 0 !important;
        }

        a.pdfdownload img {
            top: 0 !important;
            margin-left: 10px;
        }

    }

    @media only screen and (max-width: 767px){



        .subscriber-img {
            width: 100%;
            padding: 0 15px;
        }

        .greeting_full .r_con{
            height: 200px;
            margin-left: 0;
        }

        .msgarea{
            font-size: 15px;
            margin-top: 0 !important;
        }

        a.pdfdownload {
            padding: 0 !important;
        }

        a.pdfdownload img {
            top: 0 !important;
            margin-left: 10px;
        }

    }

</style>
<?php if ($display == 1) { ?>
    <!--Banner -->
    <div class="wrapper">
        <div class="container_12 text-center">
            <div class="heading_2">
                It`s Your <span>Birthday Greeting Card</span>
            </div>
        </div>
    </div>
    <div class="wrapper">
        <div class="container_12">
            <div class="inner_midd">
                <div class="in_f_l">
                    <img src="<?php echo BASEURL_OIMG; ?>add_bday_reminder.png" id="blah" style="padding-left: 0px;margin: 0px" />
                </div>
                <div class="in_f_r">
                    <div class="wrapper">
                        <div class="greeting_full">
                            <div class="g_left">
                                <img src="<?php echo BASEURL_CROPPED_GREET . $greeting_data['greeting_img']; ?>" />
                            </div>
                            <div class="g_right">
                                <div class="r_con scroll_issue">
                                    <div class="msgarea">
                                        <div class="display_msg">
                                            <?php echo $message; ?>
                                        </div>
                                    </div>
                                    <?php if ($greeting_data['type'] == 1 || $greeting_data['type'] == 2 || $greeting_data['type'] == 3) { ?>
                                        <div class="photo_img">
                                            <?php if ($greeting_data['type'] == 1) { ?>
                                                <div class="photoupload" >
                                                    <img src="<?php echo BASEURL_OIMG ?>imgframe.png" class="backcorners"/>
                                                    <div class="photo_img">
                                                        <img src="<?php echo BASEURL_BVMG . $greeting_data['uploded_img'] ?>" id="defaultpic" class="sampleimg" >
                                                    </div>
                                                </div>
                                            <?php } else if ($greeting_data['type'] == 2) { ?>
                                                <div class="photoupload" >
                                                    <img src="<?php echo BASEURL_OIMG ?>imgframe.png" class="backcorners"/>
                                                    <div class="photo_img">
                                                        <div class="video_container">
                                                            <video   id="videof" class="display_video" controls preload="none">
                                                                <source src="<?php echo BASEURL_BVMG . $greeting_data['uploded_img'] ?>" type="video/mp4">

                                                            </video>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else if ($greeting_data['type'] == 3) { ?>
                                                <div class="photoupload"  style="margin-top:0px;">
                                                    <img src="<?php echo BASEURL_OIMG ?>imgframe.png" class="backcorners"/>
                                                    <div class="photo_img">
                                                        <div class="preview_box">
                                                            <div class="playaudioContainer" >
                                                                <div class="audioControlPlayer audioPlayer" rel="play" id="playit"></div>
                                                                <audio id="playaudioplayer"  preload="auto" class="a" >
                                                                    <source src="<?php echo BASEURL_BVMG . $greeting_data['uploded_img'] ?>" >
                                                                    Your browser does not support the audio element.
                                                                </audio>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div style="color: #000; margin-top: 40px; font-size: 18px; margin-left: 5px;">
                            From: <span> <?php echo $greeting_data['first_name']; ?>(<?php echo $greeting_data['email']; ?>)</span>
                        </div>
                        <div style="margin-top: 20px; font-size: 18px; margin-left: 5px;">
                            <a href="<?php echo BASEURL . "Home_web/download_pdf/" . $fid . "/1" ?>" style="color: #000;">Download e-card as a pdf
                                <img src='<?php echo BASEURL_OIMG; ?>pdf.png' style="top: 20px; position: relative;"/>
                            </a> 
                        </div>
                    </div>
                </div>

                <div class="in_f_l">
                    <div>
                        <div style="margin:auto;text-align: center;">
                            <img src="<?php echo BASEURL_OIMG . "sub2.png" ?>"  />
                        </div>
                        <div class='esub'>
                            <input type='email' class='esubemail' id="nsemail" name="email_id" style='border-radius: 9px;'/>
                            <div class="error_n3" style="color: red; width: 105px; margin: auto;"></div>
                            <button id='esubscribe' class="esubscribe" style="right:0%"></button>
                            <div style="font-size:12px;color: white">OR</div>
                            <div style="font-size:12px;color: white">like and follow us on <a  style="font-family:FontAwesome;color: white" href="https://www.facebook.com/birthdayowl/" target="_blank">&#xf09a;</a></div>
                        </div>

                    </div> 

                </div>
            </div>
        </div>
    </div>


<?php } else { ?>

    <div style="display: block; text-align: center; font-size: 20px; font-weight: bold;">Your e-greeting card has been expired</div>

<?php } ?>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () {
            change_val();
        }, 2000);
    });

    function change_val() {
        var data = {
            fid: "<?php echo $fid; ?>",
            check: "<?php echo $check; ?>",
            mail_open_status: "<?php echo $mail_open_status; ?>"
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>Home_web/change_status",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() == "false") {
                } else {
                }
            }
        });


    }
<?php if ($greeting_data['type'] != 1 && $greeting_data['type'] != 2) { ?>
        $(".msgarea").css("margin-top", "95px");
<?php } ?>
    $("#esubscribe").click(function () {

        var nsemail = $("#nsemail").val();
        if (nsemail == '') {
            $(".error_n3").html("Enter Email");
        } else if ((nsemail != '') && (validateEmail(nsemail) == false)) {
            $(".error_n3").show();
            alert("Not a Valid Email Address");
        } else if ((nsemail != '') && (validateEmail(nsemail) == true)) {
            var data = {
                email_id: nsemail
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>newsletter",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() == "false") {

                        $("#message").html(r.message.toString());
                        return false;
                    } else {

                        $("#nsemail").val('');
                        alert(r.message.toString());
                    }
                }
            });
        }
    });

    $("#nsemail").keyup(function () {
        $(".error_n3").html("");
    })


    $("body").on("click", "#playit", function (e)
    {
        var ID = $(this).attr("id");
        var progressArea = $("#playaudioProgress");
        var audioTimer = $("#audioTimePlayer");
        var audio = $("#playaudioplayer");
        var audioCtrl = $(this);
        e.preventDefault();
        var R = $(this).attr('rel');
        if (R == 'play')
        {
            $(this).removeClass('audioPlayer').addClass('audioPlayerPause').attr("rel", "pause");
            audio.trigger('play');
        } else {
            $(this).removeClass('audioPlayerPause').addClass('audioPlayer').attr("rel", "play");
            audio.trigger('pause');
        }

        audio.bind("timeupdate", function (e) {
            var audioDOM = audio.get(0);
            audioTimer.text(getAudioTimeByDec(audioDOM.currentTime, audioDOM.duration));
            var audioPos = (audioDOM.currentTime / audioDOM.duration) * 100;

            progressArea.css('width', audioPos + "%");
            if (audioPos == "100")
            {
                $("#" + ID).removeClass('audioPlayerPause').addClass('audioPlayer').attr("rel", "play");
                audio.trigger('pause');
            }
        });

    });
    getAudioTimeByDec = function (cTime, duration) {
        var duration = parseInt(duration),
                currentTime = parseInt(cTime),
                left = duration - currentTime,
                second, minute;
        second = (left % 60);
        minute = Math.floor(left / 60) % 60;
        second = second < 10 ? "0" + second : second;
        minute = minute < 10 ? "0" + minute : minute;

        return minute + ":" + second;
    };
    if ($(window).width() < 768) {
        $("#blah").css({"top": "11px", "padding": "0px"});
    }
    if ($(window).width() > 768) {
        $(function () {

            var $blah = $("#blah"),
                    $window = $(window),
                    offset = $blah.offset();

            $window.scroll(function () {
                if ($window.scrollTop() > offset.top) {
                    $blah.stop().animate({
                        top: 450
                    });
                } else {
                    $blah.stop().animate({
                        top: 0
                    });
                }
            });
        });
    }


</script>

