<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from aqvatarius.com/themes/atlant_v1_4/html/table-export.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Dec 2014 12:26:16 GMT -->
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->

        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo BASEURL_AMN_CSS; ?>theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                                       
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">

            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="index-2.html">ATLANT</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
                        </a>
                    </li>
                    <li class="xn-title">Navigation</li>                    
                    <li>
                        <a href="index-2.html"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
                    </li>
                    <li>
                        <a href="<?php echo BASEURL; ?>Admin_panel/users"><span class="fa fa-download"></span>Users</a>
                    </li>
                    <li>
                        <a href="company.html"><span class="fa fa-download"></span>Company Register</a>
                    </li>
                    <li>
                        <a href="<?php echo BASEURL; ?>Admin_panel/newsletter"><span class="fa fa-download"></span>Newsletter</a>
                    </li>
                    <li>
                        <a href="<?php echo BASEURL; ?>Admin_panel/friends"><span class="fa fa-download"></span>Refer Friend List</a>
                    </li>
                    <li>
                        <a href="<?php echo BASEURL; ?>Admin_panel/marquee"><span class="fa fa-download"></span>Marquee</a>
                    </li>
                    <li>
                        <a href="<?php echo BASEURL; ?>Admin_panel/add_sign_data"><span class="fa fa-align-justify"></span> Add Sign Data</a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Menu</span></a>
                        <ul>
                            <li  class="active"><a href="<?php echo BASEURL; ?>Admin_panel/add_menu"><span class="glyphicon glyphicon-plus-sign"></span> Add Main Menu</a></li>
                        </ul>
                        <ul>
                            <li class="xn-openable">
                                <a href="#"><span class="glyphicon glyphicon-folder-close"></span>Categories</a>
                                <ul>
                                    <li><a href="pages-timeline.html"><span class="glyphicon glyphicon-plus-sign"></span> Add Category</a></li>
                                    <li><a href="pages-timeline-simple.html"><span class="fa fa-list"></span> View Categories</a></li>
                                </ul>
                            </li>
                            <li class="xn-openable">
                                <a href="#"><span class="glyphicon glyphicon-folder-close"></span> Sub-Categories</a>
                                <ul>
                                    <li><a href="pages-timeline.html"><span class="glyphicon glyphicon-plus-sign"></span> Add  Sub-Category</a></li>
                                    <li><a href="pages-timeline-simple.html"><span class="fa fa-list"></span> View Sub-Categories</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->

            <!-- PAGE CONTENT -->
            <div class="page-content">

                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- POWER OFF -->
                    <li class="xn-icon-button pull-right last">
                        <a href="#"><span class="fa fa-power-off"></span></a>
                        <ul class="xn-drop-left animated zoomIn">
                            <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                        </ul>                        
                    </li> 
                    <!-- END POWER OFF -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span>ADD CATEGORY</h2>
                </div>
                <!-- END PAGE TITLE -->                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="panel panel-default">
                                    <div class="panel-body">    
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Main Menu</label>
                                            <div class="col-md-3 col-xs-12">    
                                                <select id="days" name="bday" class="form-control select">
                                                    <option selected="selected" value="" disabled="true">Select</option>
                                                    <?php foreach($menu as $menu){?>
                                                    <option value="<?php echo $menu["menu_id"];?>"  ><?php echo $menu["menu_name"];?></option>
                                               
                                                    <?php }?>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Category Name</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="category_name" id="category_name"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">                                                                        
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Sub-Category Name</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="menu_name" id="menu_name"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <button class="btn btn-primary pull-right" style=" margin-left: 0px; margin-right:889px;" id="submit_category">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                <div class="panel-heading">

                                    <h3 class="panel-title">Manage Menu</h3>
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                        </ul>
                                    </div>                                    
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="customers2" class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>MAIN MENU</th>
                                                    <th>CATEGORY NAME</th>
                                                    <th>CATEGORY DESCRIPTION</th>
                                                    <th>SUB-CATEGORY NAME</th>
                                                    <th>SUB-CATEGORY DESCRIPTION</th>
                                                    <th>Edit</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php //foreach ($categories as $category) { ?>
                                                    <tr>
                                                        <td><?php //echo $category["menu_id"];  ?></td>
                                                        <td><?php //echo $category["menu_name"];  ?></td>
                                                        <td><?php //echo $category["menu_id"];  ?></td>
                                                        <td><?php //echo $category["menu_name"];  ?></td>
                                                        <td><?php //echo $category["menu_id"];  ?></td>
                                                        <td> <button class="btn btn-default btn-rounded btn-condensed btn-sm edit_menu" menu_id="<?php //echo $menu["menu_id"];  ?>"><span class="fa fa-pencil"></span></button></td>
                                                    </tr>
                                                <?php //} ?>
                                            </tbody>
                                        </table>                                    
                                    </div>
                                </div>
                            </div>
                            <!-- END DATATABLE EXPORT -->                            
                        </div>
                    </div>
                </div>         
                <!-- END PAGE CONTENT WRAPPER -->
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->          
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->
        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='<?php echo BASEURL_AMN_JS; ?>plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/tableExport.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/jquery.base64.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/html2canvas.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/jspdf/jspdf.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/jspdf/libs/base64.js"></script>        
        <!-- END THIS PAGE PLUGINS-->  
        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>settings.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins.js"></script>        
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>actions.js"></script>        
        <!-- END TEMPLATE -->
        <!-- END SCRIPTS -->   
        <script>
                                                $("#submit_category").click(function () {
                                                    var data = {
                                                        	category_name: $("#category_name").val()
                                                    }
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "<?php echo BASEURL; ?>Admin_panel/add_menu1",
                                                        data: data,
                                                        dataType: "json",
                                                        success: function (r) {
                                                            if (r.success.toString() === "false") {
                                                                $("#message").html(r.message.toString());
                                                                return false;
                                                            } else {
                                                                alert(r.message.toString());
                                                                window.location = "<?php echo BASEURL; ?>Admin_panel/menu";
                                                            }
                                                        }
                                                    });
                                                });
                                              
        </script>
    </body>

    <!-- Mirrored from aqvatarius.com/themes/atlant_v1_4/html/table-export.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Dec 2014 12:26:24 GMT -->
</html>






