<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>

<body>
    <script>
        $(document).ready(function () {
            $("#reminderopen").addClass("active");
            $(".rusers li").addClass("active");
        });


    </script>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <!-- START PAGE SIDEBAR -->
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <!-- END PAGE SIDEBAR -->

        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">

                <!-- END SEARCH -->
                <!-- POWER OFF -->
                <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                </li> 
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->

            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     
            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span class="fa fa-arrow-circle-o-left"></span> Registered Users</h2>
            </div>
            <!-- END PAGE TITLE -->                

            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="delete_user()" ><span class="fa fa-times"> DELETE</span></button>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    

                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="customers2" class="table datatable" >
                                        <thead>
                                            <tr>
                                                <th>Select</th>
                                                <!--<th>User ID</th>-->
                                                <th>Real Name</th>
                                                <th>Email</th>
                                                <th>Birth date</th>
                                                <th>Gender</th>
                                                <th>Phone No</th>
                                                <th>Registration date</th>
                                                <th>Reminder count</th>
                                                <th>Country Code</th>
<!--                                                <th>Occupation</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($user_info as $users) {
                                                if ($users["gender"] == 1) {
                                                    $gender_name = "Male";
                                                } else {
                                                    $gender_name = "Female";
                                                }
//                                                if ($users["occup_id"] == 1) {
//                                                    $occupation = "Student";
//                                                } else {
//                                                    $occupation = "Employee";
//                                                }
                                                if ($users["bmonth"] == 1) {
                                                    $month = "January";
                                                } else if ($users["bmonth"] == 2) {
                                                    $month = "February";
                                                } else if ($users["bmonth"] == 3) {
                                                    $month = "March";
                                                } else if ($users["bmonth"] == 4) {
                                                    $month = "April";
                                                } else if ($users["bmonth"] == 5) {
                                                    $month = "May";
                                                } else if ($users["bmonth"] == 6) {
                                                    $month = "June";
                                                } else if ($users["bmonth"] == 7) {
                                                    $month = "July";
                                                } else if ($users["bmonth"] == 8) {
                                                    $month = "August";
                                                } else if ($users["bmonth"] == 9) {
                                                    $month = "September";
                                                } else if ($users["bmonth"] == 10) {
                                                    $month = "October";
                                                } else if ($users["bmonth"] == 11) {
                                                    $month = "November";
                                                } else if ($users["bmonth"] == 12) {
                                                    $month = "December";
                                                }
                                                ?>
                                                <tr>
                                                    <td><input type="checkbox" class="checkbox" value="<?php echo $users["user_id"]; ?>" /></td>
                                                    <!--<td><?php // echo $users["user_id"];          ?></td>-->
                                                    <td><a href="<?php echo BASEURL . "Admin_panel/getReminders/" . $users["user_id"] ?>" style="text-decoration: underline;cursor: pointer;"><?php echo $users["first_name"]; ?></a></td>
                                                    <td><?php echo $users["email"]; ?></td>
                                                    <?php if ($users["bdate"] != "") { ?>
                                                        <td><?php echo $month . " " . $users["bdate"] . "," . $users["byear"]; ?></td>
                                                    <?php } else { ?>
                                                        <td></td>
                                                    <?php } ?>
                                                    <td><?php echo $gender_name; ?></td>
                                                    <td><?php echo $users["mobile_no"]; ?></td>
                                                    <td><?php echo date("d/m/Y", strtotime($users["created_date"])); ?></td>
                                                    <td><?php echo $users["rem_count"]; ?></td>                                         
                                                    <td><?php echo $users["country_name"];  ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>
            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->    
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- START PRELOADS -->
    <!--    <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>-->
    <!-- END PRELOADS -->  
    <script type="text/javascript">
        $('#customers2').dataTable({
            "pageLength": 50
        });
        function delete_user() {
            var checkboxArray = new Array();
            var checkedValues = $('.checkbox:checked').map(function () {
                checkboxArray.push(this.value);
            });
            if (checkboxArray == '') {
                alert("Select at least one User");
            } else {
                var ask = window.confirm("Are you sure you want to delete?");
                if (ask) {
                    var data = {
                        id: checkboxArray,
                        from: 1
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo BASEURL; ?>Admin_panel/CommonDelete",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.success.toString() === "false") {
                                $("#message").html(r.message.toString());
                                return false;
                            } else {
                                alert(r.message.toString());
                                window.location = "<?php echo BASEURL; ?>Admin_panel/users";
                            }
                        }
                    });

                } else {
                    window.location = "<?php echo BASEURL; ?>Admin_panel/users";
                }
            }
        }
    </script>
    <!-- END TEMPLATE -->
