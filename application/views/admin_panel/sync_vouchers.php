<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>

<body>
    <style type="text/css">
        .show_overlay{background:rgba(0,0,0,0.6);display:none;width:100%;height:100%;top:0;right:0;position:fixed;z-index:11111}
        .loader_div{border-radius:11px;padding-top:35px;background-color:#fff;text-align:center;height:150px;width:136px;margin:auto;left:0;right:0;top:0;bottom:0;position:absolute}
    </style>
    <script>
        $(document).ready(function () {
            $("#vouchers").addClass("active");
            $(".sync_woohoo li").addClass("active");
        });
    </script>
    <div class="page-container">
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <div class="page-content">

            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li>
            </ul>

            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">SYNC VOUCHERS</h3>                                  
                            </div>
                            <div class="panel-body">
                                <button class="btn btn-primary text-center"  id="sync_vouchers" >SYNC</button>
                            </div>
                        </div>                 
                    </div>
                </div>
            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->
    <div class="showloader" style="display:none">
        <div class="loader_div">
            <img src="<?php echo BASEURL_OIMG ?>89.gif" alt="Loading" class="ajax-loader " />
        </div>
    </div>

    <script>
        $("#sync_vouchers").click(function () {
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Home_web/callWoohooProductsApi",
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    $(".showloader").removeClass("show_overlay").hide();
                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        return false;
                    } else {
                        alert(r.message.toString());
                        return true;
                    }
                }
            });
        });

    </script>
