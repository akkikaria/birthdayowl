<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <script>
        $(document).ready(function () {
            $("#reminderopen").addClass("active");
            $(".news li").addClass("active");
        });
    </script>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <!-- END PAGE SIDEBAR -->

        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">

                <!-- POWER OFF -->
                <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                </li> 
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->

            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     
            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span class="fa fa-arrow-circle-o-left"></span>Manage Subscription</h2>
            </div>
            <!-- END PAGE TITLE -->                
            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <form class="form-group" method="post" id="news_form" name="news_data" enctype="multipart/form-data">
                                <div>
                                    <label class="col-md-1 col-xs-12 control-label">Title</label>
                                    <div class="col-md-3 col-xs-12">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="title" id="title"/>
                                        </div> 
                                    </div>
                                    <div class="error_r1" style="color: red;margin-left:245px;"></div>
                                    <br/>
                                </div>
                                <br/>
                                <div>
                                    <label class="col-md-1 col-xs-12 control-label">link</label>
                                    <div class="col-md-3 col-xs-12">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="link" id="link"/>
                                        </div> 
                                    </div>
                                    <br/>
                                </div>
                                <div style="margin-top: 23px;">
                                    <label class="col-md-1 col-xs-12 control-label" style="margin-top: 25px;">Description:</label>
                                    <div class="col-md-3 col-xs-12">                                            
                                        <textarea class="form-control" rows="0" name="description" id="news_desc"></textarea>
                                    </div>
                                    <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                </div>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <div class="form-group">
                                    <label class="col-md-1 control-label">Image</label>
                                    <div class="col-md-6 col-xs-12">      
                                        <input type="file" name="userfile" id="userfile1" title="Browse file"/>
                                    </div>
                                </div>
                                <br/>
                                <div class="col-sm-2 col-sm-offset-0" style="margin-left: 93px;">
                                    <div class="btn btn-primary pull-right"   onclick="send_newsletter()">Submit</div>
                                </div>

                                <div class="panel-heading">

                                    <div class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user"><input type="checkbox"   id="checkAll" value="0" /> Select All</div>
                                    <div class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="get()" news_id="<?php //echo $news["id"];                ?>"><span class="fa fa-times"> DELETE</span></div>
                                    <div class="btn-group pull-right">

                                        <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                        </ul>
                                    </div>                                    

                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="customers2" class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Email</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($news_data as $news) { ?>
                                                    <tr>
                                                        <td><input type="checkbox" class="checkbox" name="id[]" value="<?php echo $news["id"]; ?>" /></td>
                                                        <td><?php echo $news["first_name"]; ?></td>
                                                        <td><?php echo $news["last_name"]; ?></td>
                                                        <td><?php echo $news["email_id"]; ?></td>

                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>                                    
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>

            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->    

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-times"></span> Remove <strong>Data</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to remove this row?</p>                    
                    <p>Press Yes if you sure.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <button class="btn btn-success btn-lg mb-control-yes">Yes</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->        
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('#customers2').dataTable({
            "pageLength": 25
        });

        $("#checkAll").change(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });

        function send_newsletter() {
            var checkboxArray = new Array();
            var checkedValues = $('.checkbox:checked').map(function () {
                checkboxArray.push(this.value);
            });
            var title = $("#title").val();

            if (title == '') {
                $(".error_r1").html("Title is required");
            }
            $('#title').keyup(function (e) {
                $(".error_r1").hide();
            });
            if ((checkboxArray == '') && (title != '')) {
                alert("Select at least one user ")
            }
            if ((title != '') && (checkboxArray != '')) {
                var formData = new FormData($("form[name='news_data']")[0]);
                var url = "<?php echo BASEURL; ?>Admin_panel/send_newsletter";
                $.ajax({
                    url: url,
                    type: "POST",
                    data: formData,
                    async: true,
                    dataType: "json",

                    success: function (msg) {

                        if (msg.success.toString() == "false") {
                            alert(msg.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/newsletter";
                        } else {
                            alert(msg.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/newsletter";

                        }
                    }, cache: false, contentType: false, processData: false});


            }

        }

        function get() {
            var checkboxArray = new Array();
            var checkedValues = $('.checkbox:checked').map(function () {
                checkboxArray.push(this.value);
            })

            if (checkboxArray == '') {
                alert("Select at least one User");
            } else {
                var data = {
                    id: checkboxArray,
                    from: 2
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/CommonDelete",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#message").html(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/newsletter";
                        }
                    }
                });
            }
        }
    </script>
</body>
</html>






