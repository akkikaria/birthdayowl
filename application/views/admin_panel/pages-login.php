<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <?php
    header("Access-Control-Allow-Origin: *");
    ?>
    <!-- Mirrored from aqvatarius.com/themes/atlant_v1_4/html/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Dec 2014 12:25:49 GMT -->
    <head>        
        <!-- META SECTION -->
        <title>BirthdayOwl</title>    
        <link rel="shortcut icon" type="image/png" href="<?php echo BASEURL_OIMG; ?>main_icn.png"/>        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo BASEURL_AMN_CSS; ?>theme-default.css"/>
        <script type="text/javascript" src="<?php echo BASEURL_FANCY; ?>jquery-1.4.3.min.js"></script>
        <!-- EOF CSS INCLUDE -->                                       
    </head>
    <body>
        <div class="login-container">
            <div class="login-box animated fadeInDown">
                <div  style="color:white;font-size:18px;text-align:center"><img src="<?php echo BASEURL_OIMG; ?>logo.png"></div>
                <div class="login-body">
                    <div class="login-title"><strong>Welcome</strong>, Please login</div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" class="form-control" placeholder="Username" id="username" name="username"/>
                            </div>
                        </div>
                        <div class="err1" style="color:red; margin-top: -14px; margin-left: 17px;"></div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="password" class="form-control" placeholder="Password" id="password" name="password"/>
                            </div>
                        </div>
                        <div class="err2" style="color:red; margin-top: -14px; margin-left: 17px;"></div>
                        <div class="form-group">
                            <!--<div class="col-md-6">
                                <a href="#" class="btn btn-link btn-block">Forgot your password?</a>
                            </div>-->
                            <div class="col-md-6 col-md-offset-3" id="admin_login" style="margin-top: 5px;">
                                <button class="btn btn-info btn-block">Log In</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <script>
            $("#admin_login").click(function () {
                admin_enter();
            });
            $("input").keypress(function (e) {
                if (e.which == 13) {
                    admin_enter();
                }
            });
            function admin_enter() {
                var username = $("#username").val();
                var password = $("#password").val();
                if (username == '') {
                    $(".err1").html("Enter Username");
                }
                $('#username').focus(function () {
                    $(".err1").hide();
                });
                if (password == '') {
                    $(".err2").html("Enter Password");
                }
                $('#password').keyup(function () {
                    $(".err2").hide();
                });
                if ((username != '') && (password != '')) {
                    var data = {
                        username: username,
                        password: password
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo BASEURL; ?>Admin_panel/admin_success",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.success.toString() === "true") {
                                window.location = "<?php echo BASEURL; ?>Admin_panel/admin_dashboard";
                                return true;
                            } else {
                                alert(r.message.toString());
                                $("#password").val("");
                                return false;
                            }
                        }
                    });
                }
            }

        </script>
    </body>

    <!-- Mirrored from aqvatarius.com/themes/atlant_v1_4/html/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Dec 2014 12:25:49 GMT -->
</html>






