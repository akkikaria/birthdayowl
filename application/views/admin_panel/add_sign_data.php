<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from aqvatarius.com/themes/atlant_v1_4/html/form-editors.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Dec 2014 12:26:07 GMT -->
    <head>        
        <!-- META SECTION -->
        <title>BirthdayOwl</title>    
        <link rel="shortcut icon" type="image/png" href="<?php echo BASEURL_OIMG; ?>main_icn.png"/>               
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo BASEURL_AMN_CSS; ?>theme-default.css"/>
        <link rel="stylesheet" type="text/css"  href="<?php echo BASEURL_AMN_CSS; ?>admin.css"/>
        <!-- EOF CSS INCLUDE -->                   
    </head>
    <body>

        <div class="page-container">
            <div class="page-sidebar">
                <ul class="x-navigation">
                    <li>
                        <div style="height:100px;background: white;"> <a href="<?php echo BASEURL; ?>Admin_panel/admin_dashboard"><img src="<?php echo BASEURL_OIMG; ?>logo.png"  style=" width: 82%; margin-top: 17px; margin-left: 10px;"></a></div>
                    </li>
                    <li class="xn-title"></li>
                    <li class="xn-openable active"  id="reminderopen">
                        <a href="#"><span class="glyphicon glyphicon-list"></span><span class="xn-text">Birthday Reminder</span></a>
                        <ul class="rusers" >
                            <li > <a href="<?php echo BASEURL; ?>Admin_panel/users"><span class="glyphicon glyphicon-plus-sign"></span>Users</a></li>
                        </ul>
                        <ul class="rgusers" >
                            <li > <a href="<?php echo BASEURL; ?>Admin_panel/guest_users"><span class="glyphicon glyphicon-plus-sign"></span>Guest Users</a></li>
                        </ul>
                         <ul class="treminders" >
                            <li > <a href="<?php echo BASEURL; ?>Admin_panel/add_reminder"><span class="glyphicon glyphicon-plus-sign"></span>Total Reminders Added</a></li>
                        </ul>
                        <ul class="news">
                            <li>  <a href="<?php echo BASEURL; ?>Admin_panel/newsletter"><span class="glyphicon glyphicon-plus-sign"></span>Newsletter</a></li>
                        </ul>
                        <ul class="marquee">
                            <li>  <a href="<?php echo BASEURL; ?>Admin_panel/marquee"><span class="glyphicon glyphicon-plus-sign"></span>Marquee</a></li>
                        </ul>
                        <ul class="signdata active">
                            <li class="active">   <a href="<?php echo BASEURL; ?>Admin_panel/add_sign_data"><span class="glyphicon glyphicon-plus-sign"></span> Add Sign Data</a></li>
                        </ul>
                        <!--                        <ul class="relation">
                                                    <li> <a href="<?php // echo BASEURL;   ?>Admin_panel/relationship"><span class="glyphicon glyphicon-plus-sign"></span> Add Relationship</a></li>
                                                </ul>-->
                    </li>

                    <!--START  GREETING CARDS-->
                    <li class="xn-openable" id="freegreetings">
                        <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Free-Greetings</span></a>
                        <ul class="uploadfreegreetings">
                            <li><a href="<?php echo BASEURL; ?>Greeting_card/greeting/1"><span class="glyphicon glyphicon-plus-sign"></span>Upload Greetings</a></li>
                        </ul>
                        <ul class="sent_greetings">
                            <li><a href="<?php echo BASEURL; ?>Greeting_card/sent_greetings"><span class="glyphicon glyphicon-plus-sign"></span>Sent Greetings</a></li>
                        </ul>
                        <ul class="thankyou">
                            <li><a href="<?php echo BASEURL; ?>Greeting_card/greeting/3"><span class="glyphicon glyphicon-plus-sign"></span>ThankYou Cards</a></li>
                        </ul>
                    </li>
                    <!--END  GREETING CARDS-->

                    <!-- Flowers-->
                    <li class="xn-openable" id="flowers">
                        <a href="#"><span class="glyphicon glyphicon-list"></span>Flowers</a>
                        <ul class="add_mainmenu">
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/menu"><span class="glyphicon glyphicon-plus-sign"></span> Add Main Menu</a></li>
                        </ul>
                        <ul class="categories">
                            <li class="xn-openable">
                                <a href="#"><span class="glyphicon glyphicon-folder-close"></span>Categories</a>
                                <ul class="add_categories">
                                    <li><a href="<?php echo BASEURL; ?>Admin_panel/category"><span class="glyphicon glyphicon-plus-sign"></span> Add Category</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="product_folder">
                            <li class="xn-openable">
                                <a href="#"><span class="glyphicon glyphicon-folder-close"></span>Products</a>
                                <ul class="add_product">
                                    <li><a href="<?php echo BASEURL; ?>Admin_panel/product"><span class="glyphicon glyphicon-plus-sign"></span> Add Product</a></li>
                                </ul>
                                <ul class="view_product">
                                    <li><a href="<?php echo BASEURL; ?>Admin_panel/view_product/0"><span class="fa fa-list"></span> View Products</a></li>
                                </ul>
                            </li>
                        </ul>

                    </li>
                    <!-- End Flowers-->

                    <!--START  Product Voucher-->
                    <li class="xn-openable" id="vouchers">
                        <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Product Voucher</span></a>
                        <ul class="sync_woohoo" >
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/sync_vouchers"><span class="glyphicon glyphicon-plus-sign"></span>Sync vouchers</a></li>
                        </ul>
                        <ul class="upload_vouchersgreeting">
                            <li><a href="<?php echo BASEURL; ?>Greeting_card/greeting/2"><span class="glyphicon glyphicon-plus-sign"></span>Upload Greetings</a></li>
                        </ul>
                    </li>
                    <!--END  Product Voucher-->

                    <!-- Banners-->
                    <li class="xn-openable" id="banners">
                        <a href="#"><span class="glyphicon glyphicon-list"></span>Banners</a>
                        <ul class="add_banners">
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/banner"><span class="glyphicon glyphicon-plus-sign"></span>Add Banners</a></li>
                        </ul>
                        <!--                        <ul class="homepage">
                                                    <li><a href="<?php // echo BASEURL;      ?>Admin_panel/change_homepage"><span class="glyphicon glyphicon-plus-sign"></span>Change Home Screen Banners</a></li>
                                                </ul>-->
                    </li>
                    <!-- End Banners-->
                    <!-- sales-->
                    <li class="xn-openable" id="sales">
                        <a href="#"><span class="glyphicon glyphicon-list"></span>Orders</a>
                        <ul class="orders">
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/orders"><span class="glyphicon glyphicon-plus-sign"></span>Orders</a></li>
                        </ul>
                        <ul class="flower_orders">
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/flower_orders"><span class="glyphicon glyphicon-plus-sign"></span>Flower Orders</a></li>
                        </ul>
                        <!--                        <ul class="wishList">
                                                    <li><a href="<?php // echo BASEURL;   ?>Admin_panel/mycartorders"><span class="glyphicon glyphicon-plus-sign"></span>Wish List Orders</a></li>
                                                </ul>-->
                        <ul class="refund">
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/show_refunded_orders"><span class="glyphicon glyphicon-plus-sign"></span>Refunded Orders</a></li>
                        </ul>
                    </li>

                    <li class="xn-openable" id="footermenu">
                        <a href="#"><span class="glyphicon glyphicon-list"></span>Footer</a>
                        <ul class="aboutus">
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/add_aboutus"><span class="glyphicon glyphicon-plus-sign"></span>About us</a></li>
                        </ul>
                        <ul class="privacy">
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/add_privacy"><span class="glyphicon glyphicon-plus-sign"></span>Privacy Policy</a></li>
                        </ul>
                        <ul class="terms_egift">
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/add_terms_egift"><span class="glyphicon glyphicon-plus-sign"></span>Terms & conditions - e-Gift Cards</a></li>
                        </ul>
                        <ul class="terms_other">
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/add_terms_other"><span class="glyphicon glyphicon-plus-sign"></span>Terms & conditions - Flowers & Other Physical Gifts</a></li>
                        </ul>
                        <ul class="faq">
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/add_faq"><span class="glyphicon glyphicon-plus-sign"></span>FAQ</a></li>
                        </ul>
                        <ul class="contactus">
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/contactUs"><span class="glyphicon glyphicon-plus-sign"></span>Contact us</a></li>
                        </ul>
                        <ul class="testimonial">
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/testimonial"><span class="glyphicon glyphicon-plus-sign"></span>Testimonial</a></li>
                        </ul>
                    </li>

                    <li class="xn-openable" id="seo">
                        <a href="#"><span class="glyphicon glyphicon-list"></span>SEO</a>
                        <ul class="custom">
                            <li><a href="<?php echo BASEURL; ?>Admin_panel/seo"><span class="glyphicon glyphicon-plus-sign"></span>customization</a></li>
                        </ul>
                    </li>

                    <ul class="change_password">
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/change_password_view"><span class="glyphicon glyphicon-list"></span>Change Password</a></li>
                    </ul>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <div class="page-content">
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- POWER OFF -->
                    <li>
                        <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                    </li> 
                    <li class="xn-icon-button pull-right last">
                        <a href="#"><span class="fa fa-power-off"></span></a>
                        <ul class="xn-drop-left animated zoomIn">
                            <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                        </ul>                        
                    </li> 
                </ul>
                <div class="page-content-wrap">   
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3><strong>ADD ZODIAC SIGN</strong></h3>
                                        <div class="panel-body" id="ids">                                                                        
                                            <div class="row">
                                                <label class="col-md-1 control-label">Days</label>
                                                <div class="col-md-2 col-xs-12">    
                                                    <select id="days"  name="bday" style="width:89px;">
                                                        <option value="0" selected="selected" disabled="true">Day</option>
                                                        <option value="1">1&nbsp;</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                        <option value="13">13</option>
                                                        <option value="14">14</option>
                                                        <option value="15">15</option>
                                                        <option value="16">16</option>
                                                        <option value="17">17</option>
                                                        <option value="18">18</option>
                                                        <option value="19">19</option>
                                                        <option value="20">20</option>
                                                        <option value="21">21</option>
                                                        <option value="22">22</option>
                                                        <option value="23">23</option>
                                                        <option value="24">24</option>
                                                        <option value="25">25</option>
                                                        <option value="26">26</option>
                                                        <option value="27">27</option>
                                                        <option value="28">28</option>
                                                        <option value="29">29</option>
                                                        <option value="30">30</option>
                                                        <option value="31">31</option>
                                                    </select>

                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-md-1  control-label">Month</label>
                                                <div class="col-md-2 col-xs-12">  
                                                    <select id="month" name="bmonth">
                                                        <option selected="selected" value="0" disabled="true">Month</option>
                                                        <option value="1">January</option>
                                                        <option value="2">February</option>
                                                        <option value="3">March</option>
                                                        <option value="4">April</option>
                                                        <option value="5">May</option>
                                                        <option value="6">June</option>
                                                        <option value="7">July</option>
                                                        <option value="8">August</option>
                                                        <option value="9">September</option>
                                                        <option value="10">October</option>
                                                        <option value="11">November</option>
                                                        <option value="12">December</option>
                                                    </select>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-left: -14px;">
                                                <div class="col-md-6 col-md-offset-1">

                                                    <div class="block">
                                                        <textarea class="summernote"></h4>
                                                        </textarea>
                                                    </div>
                                                </div>                   
                                            </div>
                                            <div class="  col-md-2 col-md-offset-1" >

                                                <button class="btn btn-primary pull-right" id="zodiac_submit" style="margin-right: 37px;" >Submit</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <!-- END PAGE CONTENT WRAPPER -->                                

        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/jquery/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.1.1.js"></script>
    <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/bootstrap/bootstrap-datepicker.js"></script>                
    <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/bootstrap/bootstrap-file-input.js"></script>
    <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/bootstrap/bootstrap-select.js"></script>
    <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tagsinput/jquery.tagsinput.min.js"></script>
    <!-- END THIS PAGE PLUGINS -->       
    <!-- THIS PAGE PLUGINS -->        
    <script type='text/javascript' src='<?php echo BASEURL_AMN_JS; ?>plugins/icheck/icheck.min.js'></script>
    <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>        

    <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/codemirror.js"></script>        
    <script type='text/javascript' src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <script type='text/javascript' src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/mode/xml/xml.js"></script>
    <script type='text/javascript' src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/mode/javascript/javascript.js"></script>
    <script type='text/javascript' src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/mode/css/css.js"></script>
    <script type='text/javascript' src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/mode/clike/clike.js"></script>
    <script type='text/javascript' src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/mode/php/php.js"></script>    

    <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/summernote/summernote.js"></script>
    <!-- END PAGE PLUGINS -->

    <!-- START TEMPLATE -->
   <!-- <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>settings.js"></script>-->
    <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins.js"></script>        
    <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>actions.js"></script>        
    <!-- END TEMPLATE -->
    <script type="text/javascript">
        $(document).ready(function () {
            $("#days").change(function () {
                display_data();
            });
            $("#month").change(function () {
                display_data();
            });
            $("#zodiac_submit").click(function () {
                var sign_data = $(".note-editable").html();
                var days = $("#days").val();
                var month = $("#month").val();
                var data = {
                    sign_day: days,
                    sign_month: month,
                    sign_data: sign_data
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/update_zodiac_data",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#message").html(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                        }
                    }
                });

            });
        });
        function display_data() {
            var days = $("#days").val();
            var month = $("#month").val();
            var data = {
                sign_day: days,
                sign_month: month
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/display_zodiac_data",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        var zodiac_data = r.zodiac_data[0]["sign_data"];

                        $(".note-editable").html(zodiac_data);
                    }
                }
            });
        }

    </script>                 
    <!-- END SCRIPTS -->
</body>

<!-- Mirrored from aqvatarius.com/themes/atlant_v1_4/html/form-editors.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Dec 2014 12:26:12 GMT -->
</html>












