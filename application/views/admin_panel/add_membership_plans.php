<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <script>
            $(document).ready(function () {
                $("#membership").addClass("active");
                $(".add_membership li").addClass("active");
            });
        </script>

        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                </li> 
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     

            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span  class="fa fa-plus-circle"></span><?php //echo $heading;          ?></h2>
            </div>
            <!-- END PAGE TITLE -->                

            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-body">                                                                        
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Plan Name</label>
                                        <div class="col-md-3 col-xs-12">                                        
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="plan_name" id="plan_name" value="<?php echo $plan_data["plan_name"]; ?>"/>
                                            </div> 
                                        </div>
                                        <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Amount</label>
                                        <div class="col-md-3 col-xs-12">                                        
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="amount" id="amount" value="<?php echo $plan_data["amount"]; ?>"/>
                                            </div> 
                                        </div>
                                        <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Duration</label>
                                        <div class="col-md-3 col-xs-12">                                        
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="duration" id="duration" value="<?php echo $plan_data["duration"]; ?>"/>
                                            </div> 
                                        </div>
                                        <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="btn btn-primary pull-right"mid="<?php echo $plan_data["id"]; ?>"  id="<?php echo $btn_click; ?>">Submit</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="delete_voucher_category()" style="margin-left: 1100px;"><span class="fa fa-times"> DELETE</span></button>
                                <h3 class="panel-title">Manage Product Voucher Category</h3>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    

                            </div>

                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>SELECT PLAN</th>
                                                <th>PLAN NAME</th>
                                                <th>PLAN AMOUNT</th>
                                                <th>DURATION</th>
                                                <th>Edit</th>
                                                <th>Status</th>
                                                <th>Change Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php
                                            foreach ($membership_plans as $plans) {

                                                if ($plans["status"] == 1) {
                                                    $status = "Active";
                                                } else {
                                                    $status = "Inactive";
                                                }
                                                ?>
                                                <tr>
                                                    <td><input type="checkbox" class="checkbox" value="<?php echo $plans["id"]; ?>" /></td>

                                                    <td><?php echo $plans["plan_name"]; ?></td>
                                                    <td><?php echo $plans["amount"]; ?></td>
                                                    <td><?php echo $plans["duration"]; ?></td>
                                                    <td> <button class="btn btn-default btn-rounded btn-condensed btn-sm edit_voucher_category" voucher_cat_id="<?php echo $plans["id"]; ?>" onclick="edit_voucher_category('<?php echo $plans["id"]; ?>')"><span class="fa fa-pencil"></span></button></td>

                                                    <td><?php echo $status; ?></td>
                                                    <td>
                                                        <?php if ($plans["status"] == 1) { ?>
                                                            <a class="popular" pid="<?php echo $plans["id"]; ?>" status="0" style="cursor: pointer;text-decoration: underline;">Inactive</a>
                                                        <?php } else { ?>
                                                            <a class="popular" pid="<?php echo $plans["id"]; ?>" status="1" style="cursor: pointer;text-decoration: underline;">Active</a>

                                                        <?php } ?>

                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>

            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->
    <script>
        $(".popular").click(function () {
            var pid = $(this).attr("pid");
            var status = $(this).attr("status");

            var data = {
                id: pid,
                status: status

            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/Change_plan_status",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        window.location = "<?php echo BASEURL; ?>Admin_panel/membership_plan";
                    }
                }
            });
        });
        $("#submit_voucher_category").click(function () {
            var plan_name = $("#plan_name").val();
            var amount = $("#amount").val();
            var duration = $("#duration").val();

            if (plan_name == '') {
                alert("Plan name is required");
            } else if (amount == " ") {
                alert("Amount is required");

            } else if (duration == " ") {
                alert("Duration is required");

            } else {
                var data = {
                    plan_name: plan_name,
                    amount: amount,
                    duration: duration
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/addUpdateMembership",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#message").html(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/membership_plan";
                        }
                    }
                });
            }
        });
        function edit_voucher_category(id) {
            window.location = "<?php echo BASEURL; ?>Admin_panel/membership_plan/" + id;
        }
        $("#edit_membership").click(function () {

            var plan_name = $("#plan_name").val();
            var amount = $("#amount").val();
            var duration = $("#duration").val();
            var id = $(this).attr("mid");

            if (plan_name == '') {
                alert("Plan name is required");
            } else if (amount == " ") {
                alert("Amount is required");

            } else if (duration == " ") {
                alert("Duration is required");

            } else {
                var data = {
                    plan_name: plan_name,
                    amount: amount,
                    duration: duration,
                    id: id
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/addUpdateMembership",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#message").html(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/membership_plan";
                        }
                    }
                });
            }
        });


        function delete_voucher_category() {
            var checkboxArray = new Array();
            var checkedValues = $('.checkbox:checked').map(function () {
                checkboxArray.push(this.value);
            });
            if (checkboxArray == '') {
                alert("Select at least one plan");
            } else {
                var ask = window.confirm("Are you sure you want to delete?");
                if (ask) {
                    var data = {
                        id: checkboxArray,
                        from: 5
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo BASEURL; ?>Admin_panel/CommonDelete",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.success.toString() === "false") {
                                $("#message").html(r.message.toString());
                                return false;
                            } else {
                                alert(r.message.toString());
                                window.location = "<?php echo BASEURL; ?>Admin_panel/membership_plan";
                            }
                        }
                    });

                } else {
                    window.location = "<?php echo BASEURL; ?>Admin_panel/membership_plan";
                }
            }
        }


    </script>