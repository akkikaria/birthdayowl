<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <script>
        $(document).ready(function () {
//            $("#footermenu").addClass("active");
//            $(".faq li").addClass("active");
        });
    </script>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     
            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span class="fa fa-arrow-circle-o-left"></span><?php echo $heading; ?></h2>
            </div>
            <!-- END PAGE TITLE -->                

            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" id="banner_submit" action="<?php echo BASEURL . $action; ?>" enctype="multipart/form-data" >
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-1 col-xs-12 control-label">Festival Name</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="festival_name" id="<?php echo $id; ?>" value="<?php echo $banner_data["festival_name"]; ?>" festival_id="<?php echo $banner_data["festival_id"]; ?>"/>
                                                <input type="hidden" class="form-control" name="banner_type" id="banner_type" value="2"/>
                                            </div>                                            
                                        </div>
                                        <div class="error_r1" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-1 col-xs-12 control-label">URL</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="url" id="url" value="<?php echo $banner_data["url"]; ?>"/>
                                            </div>                                            
                                        </div>
                                        <div class="error_r1" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <table>
                                        <tr class="col-md-1 col-xs-12">
                                            <td class="col-md-4 col-xs-12">
                                                <label class="control-label" style="width:117px"> Image Upload</label>
                                            </td>
                                            <?php $j = 1; ?>
                                            <?php foreach ($banner_festival as $festivals) { ?>

                                                <td>
                                                    <?php //if ($banner_data["banner_image"] != '') { ?>
                                                    <!-- <div style="height: 20px; width: 238px; position: absolute; margin-left: 78px; margin-top: 3px; background-color: rgb(252, 248, 227);"><span id="change_img"><?php //echo $banner_data["banner_image"];                                    ?></span></div>-->
                                                    <?php // } ?>
                                                    <input type="hidden" id="userfile_old<?php echo $j; ?>"name="userfile_old[]" value="<?php echo $festivals["banner_image"]; ?>" />
                                                    <?php
                                                    $default_pic = "no_image.jpg";
                                                    $img1 = BASEURL_BPIC . $festivals["banner_image"];
                                                    if ($festivals["banner_image"] == FALSE) {//It will upload default image
                                                        $img1 = BASEURL_BPIC . $default_pic;
                                                    }
                                                    ?>

                                                    <img id="uploadPreview<?php echo $j; ?>" src="<?php echo $img1; ?>" style="width:100px;height: 100px" /><br />
                                                    <input type="file" name="userfile[]" id="uploadImage<?php echo $j; ?>"  onclick="PreviewImage(<?php echo $j; ?>);" />

                                                </td>
                                                <?php
                                                $j++;
                                            }
                                            ?>
                                            <?php for ($i = $j; $i <= 4; $i++) { ?>
                                                <td> 
                                                    <input type="hidden" id="userfile_old<?php echo $j; ?>"name="userfile_old[]" value="" />
                                                    <?php
                                                    $default_pic = "no_image.jpg";
                                                    $img2 = BASEURL_BPIC . $default_pic;
                                                    ?>
                                                    <img id="uploadPreview<?php echo $i; ?>" src="<?php echo $img2; ?>" style="width:100px;height: 100px;"/><br />
                                                    <input id="uploadImage<?php echo $i; ?>" type="file" name="userfile[]" onclick="PreviewImage(<?php echo $i; ?>);" />
                                                </td>
                                            <?php } ?>

                                        </tr>

                                    </table>
                                    <br/>
                                    <span class="col-md-6 col-md-offset-1">Note:Image size should  be more than 300px X 300px</span>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <div>                                        
                                        <label class="col-md-1 control-label">Product Vouchers</label>
                                        &nbsp;&nbsp;&nbsp;

                                        <select multiple="multiple" placeholder="Select Product Vouchers" class="SlectBox" id="select" name="voucher_pro_id[]" value="<?php echo $voucher_id; ?>">
                                            <?php foreach ($product_voucher as $products) { ?>
                                                <option value="<?php echo $products["voucher_pro_id"] ?>" ><?php echo $products["voucher_pro_name"]; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <br/>
                                    <div class="col-md-2">
                                        <div class="btn btn-primary pull-right" id="<?php echo $btn_click; ?>">Submit</div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">

                                <h3 class="panel-title">Manage Home Banner</h3>
                                <div style=""><button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="delete_banner()" style="margin-left: 1100px;"><span class="fa fa-times"> DELETE</span></button></div>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    

                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>BANNER ID</th>
                                                <th>BANNER NAME</th>
                                                <th>Edit</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($banner as $banner) { ?>
                                                <tr>
                                                    <td><input type="checkbox" class="checkbox" value="<?php echo $banner["festival_id"]; ?>" /></td>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $banner["festival_name"]; ?></td>
                                                    <td> <button class="btn btn-default btn-rounded btn-condensed btn-sm edit_banner" festival_id="<?php echo $banner["festival_id"]; ?>"><span class="fa fa-pencil"></span></button></td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>

            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

    <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->   





    <script type="text/javascript" >
        var array = [];
        var voucher_id = '<?php echo $voucher_id; ?>';
        array = voucher_id.split(",");
        var count = array.length;
        for (var i = 0; i < count; i++) {
            $("#select").find('option[value="' + array[i] + '"]').prop('selected', 'selected');
        }
        window.asd = $('.SlectBox').SumoSelect({csvDispCount: 3});
        function PreviewImage(no) {
            $("#uploadImage" + no).change(function () {
                //Get reference of FileUpload.
                var fileUpload = $(this)[0];
                var file = this.files[0];
                //Check whether the file is valid Image.
                var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");
                if (regex.test(fileUpload.value.toLowerCase())) {
                    //Check whether HTML5 is supported.
                    if (typeof (fileUpload.files) != "undefined") {
                        //Initiate the FileReader object.
                        var reader = new FileReader();
                        //Read the contents of Image File.
                        reader.readAsDataURL(fileUpload.files[0]);
                        reader.onload = function (e) {
                            //Initiate the JavaScript Image object.
                            var image = new Image();
                            //Set the Base64 string return from FileReader as source.
                            image.src = e.target.result;
                            image.name = file.name;
                            image.onload = function () {
                                //Determine the Height and Width.
                                var height = this.height;
                                var width = this.width;
                                if (height < 300 || width < 300) {
                                    $("#uploadImage" + no).val("");
                                    // $('#uploadPreview' + no).html(file.name);
                                    $('#uploadPreview' + no).attr('src', e.target.result);
                                    alert("Height and Width  must be more than 300px X 300px.");
                                    return false;
                                } else {
                                    //   $("#change_img").html(image.name);
                                    $('#uploadPreview' + no).attr('src', e.target.result);
                                    alert("Image Uploaded Successfully");
                                    return true;
                                }
                            }
                            ;
                        }
                    } else {
                        alert("This browser does not support HTML5.");
                        return false;
                    }
                } else {
                    alert("Please select a valid Image file.");
                    return false;
                }
            });
        }
        var timer2 = null;
        $('#festival_name').keydown(function () {
            clearTimeout(timer2);
            timer2 = setTimeout(check_festival_name, 1000);
        });
        var timer1 = null;
        $('#festival_name_edit').keydown(function () {
            clearTimeout(timer1);
            timer1 = setTimeout(edit_check_festival_name, 1000);
        });
        $("#submit_banner").click(function () {
            var festival_name = $("#festival_name").val();
            var url = $("#url").val();

            var result1 = check_festival_name();
            var uploadImage1 = $("#uploadImage1").val();
            var uploadImage2 = $("#uploadImage2").val();
            var uploadImage3 = $("#uploadImage3").val();
            var uploadImage4 = $("#uploadImage4").val();
            if (festival_name == '') {
                alert("Festival  name is required.");
            } else if (url == '') {
                alert("Festival  url is required.");
            } else if ((uploadImage1 == '') && (uploadImage2 == '') && (uploadImage3 == '') && (uploadImage4 == '')) {
                alert("Upload at least one image.");
            } else if ((festival_name != '') && (url != '') && (uploadImage1 != '' || uploadImage2 != '' || uploadImage3 != '' || uploadImage4 != '') && (result1 == true)) {
                pageLoadingFrame("show");
                setTimeout(function () {
                    pageLoadingFrame("hide");
                }, 2000, alert("Festival Banner added successfully"));
                $("#banner_submit").submit();
            }

        });
        $("#edit_banner").click(function () {
            var festival_name = $("#festival_name_edit").val();
            var url = $("#url").val();
            var result1 = edit_check_festival_name();
//            var uploadImage1 = $("#uploadImage1").val();
//            var uploadImage2 = $("#uploadImage2").val();
//            var uploadImage3 = $("#uploadImage3").val();
//            var uploadImage4 = $("#uploadImage4").val();
            if (festival_name == '') {
                alert("Festival name is required.");
//            } else if ((uploadImage1 == '') && (uploadImage2 == '') && (uploadImage3 == '') && (uploadImage4 == '')) {
//                alert("Upload at least one image.");
            } else if (url == '') {
                alert("Festival  url is required.");
            } else if ((festival_name != '') && (url != '') && (result1 == true)) {
                pageLoadingFrame("show");
                setTimeout(function () {
                    pageLoadingFrame("hide");
                }, 2000, alert("Festival Banner Updated successfully"));
                $("#banner_submit").submit();
            }
        });
        $(".edit_banner").click(function () {
            var festival_id = $(this).attr("festival_id");
            window.location = "<?php echo BASEURL; ?>Admin_panel/edit__festival_banner/" + festival_id;
        });
        function delete_banner() {
            var checkboxArray = new Array();
            var checkedValues = $('.checkbox:checked').map(function () {
                checkboxArray.push(this.value);
            });
            if (checkboxArray == '') {
                alert("Select at least one banner");
            } else {
                var ask = window.confirm("Are you sure you want to delete?");
                if (ask) {
                    var data = {
                        id: checkboxArray,
                        from: 14
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo BASEURL; ?>Admin_panel/CommonDelete",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.success.toString() === "false") {
                                $("#message").html(r.message.toString());
                                return false;
                            } else {
                                alert(r.message.toString());
                                window.location = "<?php echo BASEURL; ?>Admin_panel/festivals";
                            }
                        }
                    });

                } else {
                    window.location = "<?php echo BASEURL; ?>Admin_panel/festivals";
                }
            }
        }

        function check_festival_name() {
            var festival_name = $("#festival_name").val();
            var banner_type = '2';   //default=1,festival=2
            var result = 1;
            var data = {
                festival_name: festival_name,
                banner_type: banner_type
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/check_festival_name",
                data: data,
                dataType: "json",
                async: false,
                success: function (r) {
                    //  console.log(r.success.toString());
                    if (r.success.toString() == "false") {
                        alert(r.message.toString());
                        $("#festival_name").val('');

                        result = 0;
                    }
                }
            });
            if (result) {
                return true;
            }
            return false;
        }


        function edit_check_festival_name() {
            var festival_name = $("#festival_name_edit").val();
            var banner_type = '2';
            var festival_id = $("#festival_name_edit").attr("festival_id");
            var result = 1;
            var data = {
                festival_name: festival_name,
                festival_id: festival_id,
                banner_type: banner_type
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/Edit_check_festival_name",
                data: data,
                dataType: "json",
                async: false,
                success: function (r) {
                    //  console.log(r.success.toString());
                    if (r.success.toString() == "false") {
                        alert(r.message.toString());
                        $("#festival_name_edit").val('');
                        result = 0;
                    }
                }
            });
            if (result) {
                return true;
            }
            return false;
        }

    </script>



