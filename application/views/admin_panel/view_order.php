<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <script>
        $(document).ready(function () {
            $("#sales").addClass("active");
            $(".orders li").addClass("active");
        });
    </script>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>

        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     
            <div class="row">
                <div class="col-md-4">
                    <!-- START SALES & EVENTS BLOCK -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title-box" >
                                <h3 style="margin-left:28px;  "><?php echo $payment_detail["unique_oid"]; ?></h3>
                            </div>
                        </div>
                        <div class="panel-body padding-0">
                            <div class="chart-holder" id="dashboard-line-1" style="height: 200px;padding-left: 37px;  padding-top: 38px;font-size: 15px">
                                <?php
                                if ($payment_detail["delivery_status"] == 4) {
                                    $status = "Successful";
                                    $action = "";
                                } else if ($payment_detail["delivery_status"] == 3) {
                                    $status = "payment failed";
                                    $action = "";
                                } else if ($payment_detail["delivery_status"] == 8) {
                                    $status = "failed";
                                    $action = "REFUND";
                                } else if ($payment_detail["delivery_status"] == 10) {
                                    $status = "Refunded";
                                }
                                ?>
<!--                                <div>Order Date:-<?php //echo date('d/m/Y h:i:s A', strtotime($payment_detail["purchased_date"]));       ?></div>-->
                                <div>Order Status:-<?php echo $status; ?></div>
                                <div>Purchased From:-birthdayowl.com</div>

                                <?php if ($payment_detail["delivery_status"] == 3 || $payment_detail["delivery_status"] == 8) { ?>
                                    <div>Reason:-<?php echo $payment_detail["order_message"] ?></div>

                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <!-- END SALES & EVENTS BLOCK -->
                </div>
                <div class="col-md-4">
                    <!-- START SALES & EVENTS BLOCK -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title-box">
                                <h3>ACCOUNT INFORMATION</h3>
                            </div>
                        </div>
                        <div class="panel-body padding-0">
                            <div class="chart-holder" id="dashboard-line-1" style="height: 200px;padding-left: 37px;  padding-top: 38px;font-size: 15px">
                                <div>Customer Name:-<?php echo $payment_detail["uname"]; ?></div>
                                <div>Email:-<?php echo $payment_detail["uemail"]; ?></div>
                            </div>
                        </div>
                    </div>
                    <!-- END SALES & EVENTS BLOCK -->
                </div>
                <div class="panel panel-default" style="width: 1102px;">
                    <div class="panel-heading">
                        <h3 class="panel-title">PRODUCTS ORDERED</h3>
                    </div>
                    <div class="panel-body" >
                        <div class="table-responsive">
                            <table class="table table-bordered" >
                                <thead>
                                    <tr>
                                        <th>Voucher id</th>
                                        <th>Voucher Name</th>
                                        <th>Total</th>
                                        <th>e-Gift Info</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $keyid = 'eGift@birthdayOwl';
                                    $euser_id = $this->Birthday->encryptPassword($payment_detail["user_id"]);
                                    $pay_id = $this->Birthday->encryptPassword($payment_detail["payment_id"]);
                                    $key = $this->Birthday->encryptPassword($keyid);
                                    $opid = $this->Birthday->encryptPassword($payment_detail["order_pro_id"]);
                                    $from = $this->Birthday->encryptPassword(1);
                                    $greeting_id = $this->Birthday->encryptPassword($payment_detail["greeting_id"]);
                                    $link = BASEURL . "Home_web/voucher_email/$euser_id/$pay_id/$greeting_id/$opid/$key";
                                    ?>
                                    <tr>
                                        <td><?php echo $payment_detail["voucher_pro_id"] ?></td>
                                        <td><?php echo $payment_detail["voucher_pro_name"] ?></td>
                                        <td><?php echo $payment_detail["selected_amount"] ?></td>
                                        <td><a href="<?php echo $link ?>" target="_blank">click here</a></td>

                                    </tr>
                                </tbody>
                            </table> 
                        </div>
                    </div>
                </div>
            </div>
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

