<?php /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ ?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title></title>
        <style type="text/css">
            body{
                font-family: verdana, arial, sans-serif;
            }
        </style>
    </head>
    <body>

        <div style="width: 100%; text-align: center;margin:auto;background-color: #efefef;height:1100px;padding-top: 22px;">
            <table   cellspacing="0" cellpadding="0" border="0" align="center"  >
                <tr width="100">
                    <td  align="center">
                        <img src="<?php echo BASEURL_OIMG ?>logo.png" style="width:180px;" />
                        <div style="height:20px"></div>
                    </td>
                </tr>
                <tr valign="center" align="center"  width="100" >
                    <td>
                        <table style="background:#4D9E03;height:53px;width:480px;">
                            <tr>
                                <td valign="center"  align="center"  >
                                    <img src="<?php echo BASEURL_EIMG ?>p.png" style="float:left;width: 50px;"/> 
                                </td>
                                <td align='left'>
                                    <div style="line-height: 60px;text-align:left;color: white;font-size: 18px;">Payment Received Rs.<?php echo $total_amount ?></div>    
                                </td>
                            </tr>
                        </table>
                    </td>


                </tr>

                <tr  align="center" width="100">
                    <td style="width:450px;" >
                        <img src='<?php echo BASEURL_EIMG ?>payment_body.png' />
                    </td> 
                </tr>

                <tr style="background:white;" height="200" width="100">

                    <td >
                        <table style="height:53px;width:480px;">
                            <tr>
                                <td style="height:50px; padding-left: 10px;"width="300">
                                    <img src="<?php echo $voucher_img; ?>" title="birthdayowl" alt="birthdayowl" border="0" style=""  /><br/>
                                </td>
                                <td width="50%" align="center">
                                    <span style="font-size:12px;"><b><?php echo $voucher_name; ?> to <?php echo $fname; ?></b></span><br/>
                                    <span style="font-size:12px;"><b>Rs.<?php echo $selected_amount ?> Gift Amount-Sent</b></span><br/>
                                    <p style="border-top:2px solid #BFEDAC;width:150px;"></p>
                                    <span style="width:150px;font-size:12px;">
                                        to:<?php echo $femail; ?> </span><br/>                                  
                                    <span style="font-size:12px;">on:Sept 30,2016</span><br/>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="background:white;" height="250" width="100">

                    <td style="border-top:1px solid #DFDFDF" >
                        <table style="height:53px;width:480px;">
                            <tr>
                                <td style="height:50px; padding-left: 40px;"width="300">
                                    <img src="<?php echo BASEURL_CROPPED_GREET . $greeting_img ?>" title="birthdayowl" alt="birthdayowl" border="0" style="width:140px;height:190px;border-radius: 5px"  /><br/>
                                </td>
                                <td width="50%" align="center">
                                    <span style="font-size:12px;">Happy Birthday,I thought you would like this e-Gift</span><br/>
                                    <span style="font-size:12px;">-swati</span><br/>
                                    <p style="border-top:2px solid #BFEDAC;width:150px;"></p>
                                    <span style="font-size:12px;width:150px;">
                                        to:swati@yopmail.com </span><br/>
                                    <span style="font-size:12px;">on:Sept 30,2016</span><br/>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr  align="center" width="100">
                    <td  style='background: #bfedac;width:480px;height:300px ;border-top:1px solid #DFDFDF' >
                        <table style="padding:8px">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <div><b>Payment Details</b></div>
                                            </td>
                                        </tr>


                                    </table>
                                </td>
                                <td align='right'>
                                    <img src="<?php echo BASEURL_EIMG ?>payd.png" />
                                </td>
                            </tr>
                            <tr align='center' >
                                <td colspan="2"  align='center' style="background: white;"  width="400">
                                    <table align="center">
                                        <tr align="left">
                                            <td width="120" align="left">Order number</td>
                                            <td>:</td>
                                            <td>#<?php echo $order_id; ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2"  align='center'>
                                    <table>
                                        <tr>
                                            <td colspan="5">Billed to</td>
                                            <td>:</td>
                                            <td><?php echo $bill_to_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">Gift Amount</td>
                                            <td>:</td>
                                            <td>Rs.<?php echo $total_amount; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">Additional Charges</td>
                                            <td>:</td>
                                            <td>Rs.0</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">Payment method</td>
                                            <td>:</td>
                                            <?php
                                            if ($payment_mode == "CC") {
                                                $mode = "Credit Card";
                                            } else if ($payment_mode == "DC") {

                                                $mode = "Debit Card";
                                            }
                                            ?>
                                            <td><?php echo $mode; ?></td>
                                        </tr>
<!--                                        <tr>
                                            <td colspan="5">Card number</td>
                                            <td>:</td>
                                            <td>Rs.100</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">Card expiry</td>
                                            <td>:</td>
                                            <td>Rs.100</td>
                                        </tr>-->
                                        <tr>
                                            <td colspan="5">Payment date & time</td>
                                            <td>:</td>
                                            <td><?php echo $purchased_date; ?></td>
                                        </tr>
                                    </table>
                                </td> 
                            </tr>
                        </table>
                    </td>
                </tr>


                <tr width="100">
                    <td  valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; font-weight:normal;padding:15px; ">For more information please read our <a href="<?php echo WEB_PRIVACY_AND_POLICY; ?>" title="Privacy Policy" style="text-decoration:none; color:#1155cc;">Privacy Policy</a> and <a href="<?php echo BASEURL; ?>terms-of-use" title="Terms of Use" style="text-decoration:none; color:#1155cc;">Terms of Use</a>.<br />
                        Copyright © 2016 birthdayowl.com. All rights reserved.</td>
                </tr>
            </table>

        </div>
    </body>
</html>