<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<?php
//echo '<pre>';
//print_r($this->session->all_userdata());
//exit;
?>
<body>
    <script>
        $(document).ready(function () {
            $(".change_password li").addClass("active");
        });
    </script>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>
                </li> 
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 


            </ul>
            <div class="page-title">                    
                <h2><span class="fa fa-arrow-circle-o-left"></span>Change Password</h2>
            </div>
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-body">                                                                        
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Current Password</label>
                                        <div class="col-md-5 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="password" class="form-control" name="curr_pass" id="curr_pass"/>
                                            </div>                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2  control-label">New Password</label>
                                        <div class="col-md-5 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="password" class="form-control" name="new_pass" id="new_pass" />
                                            </div>                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2  control-label">Confirm Password</label>
                                        <div class="col-md-5 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="password" class="form-control" name="conf_pass" id="conf_pass" />
                                            </div>                                            
                                        </div>
                                    </div>


                                    <div class="col-md-5">
                                        <button class="btn btn-primary pull-right" style=" margin-top: -5px;" id="submit_password">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

    <script>
        $("#submit_password").click(function () {
            var curr_pass = $("#curr_pass").val();
            var new_pass = $("#new_pass").val();
            var conf_pass = $("#conf_pass").val();

            if (curr_pass == '') {
                alert('Current Password Required');
            } else if (new_pass == '') {
                alert('New Password Required');
            } else if (conf_pass == '') {
                alert('Confirm Password Required');
            } else if (new_pass != conf_pass) {
                alert('New Password & Confirm Password is not same');
            } else {
                var data = {
                    curr_pass: curr_pass,
                    new_pass: new_pass,
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/chnage_password",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            alert(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/admin_dashboard";
                        }
                    }
                });
            }
        });
    </script>
