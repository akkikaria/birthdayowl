<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<style>


    .uploaded_pic {
        background: #59aa01 none repeat scroll 0 0;
        border-radius: 7px;
        color: white;
        line-height: 29px;
        text-align: center;
        position: relative;
        right: 5%;
        top: 27%;
        height: 29px;
        width: 42%;
    }
    .uploaded_pic input.uploaded_pic1 {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        width: 100%;
        filter: alpha(opacity=0);
    }
</style>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">

        <script>
            $(document).ready(function () {
                $("#banners").addClass("active");
                $(".add_banners li").addClass("active");
            });
        </script>
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                </li> 
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     

            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span class="fa fa-arrow-circle-o-left"></span><?php echo $heading; ?></h2>
            </div>
            <!-- END PAGE TITLE -->                

            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" id="banner_submit" action="<?php echo BASEURL . $action; ?>" enctype="multipart/form-data" >
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-5 col-xs-12 control-label">Choose Image:</label>
                                        <div class="col-md-6 col-xs-12" style="  margin-top: 6px;">                                        
                                            <input type="file" name="userfile[]" id="upload-images" multiple >
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="btn btn-primary pull-right" id="<?php echo $btn_click; ?>">Upload</div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">

                                <h3 class="panel-title">Manage Home Banner</h3>
                                <div style=""><button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="delete_banner()" style="float: right; margin-left: 5px;"><span class="fa fa-times"> DELETE</span></button></div>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    

                            </div>

                            <div class="panel-body">
                                <div class="table-responsive">
                                    <form class="form-horizontal" method="POST" id="banner_img" action="" enctype="multipart/form-data">
                                        <table id="customers2" class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>BANNER ID</th>
                                                    <th>POSITION</th>
                                                    <th>IMAGE</th>
                                                    <th>Edit</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 1; ?>

                                                <?php foreach ($banner as $banner) { ?>
                                                    <tr>
                                                        <td><input type="checkbox" class="checkbox" value="<?php echo $banner["fb_id"]; ?>" /></td>
                                                        <td><?php echo $i; ?></td>

                                                        <td><input type="text" style="width:30%" value="<?php echo $banner["position"]; ?>" pid="<?php echo $banner["fb_id"]; ?>"  class="position_value" /></td>
                                                        <td><img src="<?php echo BASEURL_BPIC . $banner["banner_image"]; ?>" style="width:250px;height:150px;" /></td>
                                                        <td> 

                                                            <div class="uploaded_pic " >
                                                                <span>Change Image</span>
                                                                <input type="file" fb_id="<?php echo $banner["fb_id"]; ?>"  onclick="change_img(<?php echo $banner["fb_id"]; ?>)" id="bannerimg<?php echo $banner["fb_id"]; ?>"  name="bannerimg<?php echo $banner["fb_id"]; ?>"  class="uploaded_pic1" style="cursor:pointer;"/>
                                                            </div>


                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                                ?>

                                            </tbody>
                                        </table>  
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>         

        </div>            

    </div>

    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

    <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->   
    <script type="text/javascript">
        $('.position_value').on('input', function () {
            var position_val = $(this).val();
            if (position_val != "") {
                var data = {
                    position: position_val,
                    fb_id: $(this).attr("pid"),
                    type: 1
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/update_position",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#message").html(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/banner";
                        }
                    }
                });
            }
        });
        function change_img(no) {
            $("#bannerimg" + no).change(function () {
                var fileUpload = $(this)[0];
                var file = this.files[0];
                var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");
                if (regex.test(fileUpload.value.toLowerCase())) {
                    if (typeof (fileUpload.files) != "undefined") {
                        var reader = new FileReader();
                        reader.readAsDataURL(fileUpload.files[0]);
                        reader.onload = function (e) {
                            var image = new Image();
                            image.src = e.target.result;
                            image.name = file.name;
                            image.onload = function () {
                                if (e.target.result != "") {
                                    alert("Image Uploaded Successfully");
                                    var actionurl = "<?php echo BASEURL . "Admin_panel/Change_banner_img/"; ?>" + no;
                                    $('#banner_img').attr('action', actionurl).submit();

                                }
                            }
                            ;
                        }
                    } else {
                        alert("This browser does not support HTML5.");
                        return false;
                    }
                } else {
                    alert("Please select a valid Image file.");
                    return false;
                }
            });
        }





        $("#submit_banner").click(function () {
            pageLoadingFrame("show");
            setTimeout(function () {
                pageLoadingFrame("hide");
            }, 2000, alert("Banner added successfully"));
            $("#banner_submit").submit();
            // }

        });

        function delete_banner() {
            var checkboxArray = new Array();
            var checkedValues = $('.checkbox:checked').map(function () {
                checkboxArray.push(this.value);
            });
            if (checkboxArray == '') {
                alert("Select at least one banner");
            } else {
                var ask = window.confirm("Are you sure you want to delete?");
                if (ask) {
                    var data = {
                        id: checkboxArray,
                        from: 14
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo BASEURL; ?>Admin_panel/CommonDelete",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.success.toString() === "false") {
                                $("#message").html(r.message.toString());
                                return false;
                            } else {
                                alert(r.message.toString());
                                window.location = "<?php echo BASEURL; ?>Admin_panel/banner";
                            }
                        }
                    });

                } else {
                    window.location = "<?php echo BASEURL; ?>Admin_panel/banner";
                }
            }
        }







    </script>



