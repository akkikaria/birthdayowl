<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>

<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <!-- START PAGE SIDEBAR -->
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <!-- END PAGE SIDEBAR -->
        <!-- PAGE CONTENT -->
        <div class="page-content">
            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">

                <!-- END SEARCH -->                    
                <!-- POWER OFF -->
                <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                </li> 
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->                    

            </ul>
            <div class="page-content-wrap">

                <!-- START WIDGETS -->                    
                <div class="row" style="margin-top: 24px;">


                    <div class="col-md-3"  id="registeredusers" style="cursor: pointer;">

                        <!-- START WIDGET REGISTRED -->
                        <div class="widget widget-default widget-item-icon" >
                            <div class="widget-item-left">
                                <span class="fa fa-user"></span>
                            </div>
                            <div class="widget-data">
                                <div class="widget-int num-count"><?php echo $count_users; ?></div>
                                <div class="widget-title">Registred users</div>
                                <div class="widget-subtitle">On your website</div>
                            </div>
                            <div class="widget-controls">                                
                                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                            </div>                            
                        </div>                            
                        <!-- END WIDGET REGISTRED -->

                    </div>
                    <div class="col-md-3" id="egiftorders" style="cursor: pointer">

                        <!-- START WIDGET REGISTRED -->
                        <div class="widget widget-default widget-item-icon" >
                            <div style="margin-top: 10px;">                                    
                                <div class="widget-title">e-Gift Orders</div>                                                                        
                                <div class="widget-int"><?php echo $vouchers_count; ?></div>
                            </div>

                            <div class="widget-controls">                                
                                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                            </div>                            
                        </div>                            
                        <!-- END WIDGET REGISTRED -->

                    </div>
                    <div class="col-md-3" id="sentgreetings" style="cursor: pointer">

                        <!-- START WIDGET MESSAGES -->
                        <div class="widget widget-default widget-item-icon" >
                            <div style="margin-top: 10px;">                                    
                                <div class="widget-title">Sent eGreetings</div>                                                                        
                                <div class="widget-int"><?php echo $freegreeting_count; ?></div>
                            </div>

                            <div class="widget-controls">                                
                                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                            </div>
                        </div>                            
                        <!-- END WIDGET MESSAGES -->

                    </div>
                    <div class="col-md-3" >

                        <!-- START WIDGET CLOCK -->
                        <div class="widget widget-danger widget-padding-sm">
                            <div class="widget-big-int plugin-clock">00:00</div>                            
                            <div class="widget-subtitle plugin-date">Loading...</div>
                            <div class="widget-controls">                                
                                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="left" title="Remove Widget"><span class="fa fa-times"></span></a>
                            </div>                            
                            <div class="widget-buttons widget-c3">
                                <div class="col">
                                    <a href="#"><span class="fa fa-clock-o"></span></a>
                                </div>
                                <div class="col">
                                    <a href="#"><span class="fa fa-bell"></span></a>
                                </div>
                                <div class="col">
                                    <a href="#"><span class="fa fa-calendar"></span></a>
                                </div>
                            </div>                            
                        </div>                        
                        <!-- END WIDGET CLOCK -->

                    </div>
                </div>
                <!-- END WIDGETS -->                    

                <div class="row">

                    <div class="col-md-3" id="guestusers" style="cursor: pointer">

                        <!-- START WIDGET REGISTRED -->
                        <div class="widget widget-default widget-item-icon" >

                            <div class="widget-item-left">
                                <span class="fa fa-user"></span>
                            </div>
                            <div class="widget-data">
                                <div class="widget-int num-count"><?php echo $gcount_users; ?></div>
                                <div class="widget-title">Guest users</div>
                                <div class="widget-subtitle">On your website</div>
                            </div>
                            <div class="widget-controls">                                
                                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                            </div>     

                        </div>                            
                        <!-- END WIDGET REGISTRED -->

                    </div>

                    <div class="col-md-3" id="total_added_reminder" style="cursor: pointer">

                        <!-- START WIDGET REGISTRED -->
                        <div class="widget widget-default widget-item-icon" >

                            <div class="widget-item-left">
                                <span class="fa fa-user"></span>
                            </div>
                            <div class="widget-data">
                                <div class="widget-int num-count"><?php echo $added_reminder_count; ?></div>
                                <div class="widget-title">Added Reminder</div>
                                <div class="widget-subtitle">On your website</div>
                            </div>
                            <div class="widget-controls">                                
                                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                            </div>     

                        </div>                            
                        <!-- END WIDGET REGISTRED -->

                    </div>

<!--                    <div class="col-md-3" id="total_free_greeting_sent" style="cursor: pointer">

                         START WIDGET REGISTRED 
                        <div class="widget widget-default widget-item-icon" >

                            <div class="widget-item-left">
                                <span class="fa fa-user"></span>
                            </div>
                            <div class="widget-data">
                                <div class="widget-int num-count"><?php echo $freegreeting_sent_count; ?></div>
                                <div class="widget-title">Free Sent Greetings</div>
                                <div class="widget-subtitle">On your website</div>
                            </div>
                            <div class="widget-controls">                                
                                <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                            </div>     

                        </div>                            
                         END WIDGET REGISTRED 

                    </div>-->

                    <!--                    <div class="col-md-3" id="paidm" style="cursor: pointer">
                    
                    
                                            <div class="widget widget-default widget-item-icon" >
                                                <div style="margin-top: 10px;">                                    
                                                    <div class="widget-title">Paid Members</div>                                                                        
                                                    <div class="widget-int"><?php // echo $paid_members;    ?></div>
                                                </div>
                    
                                                <div class="widget-controls">                                
                                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                                </div>                            
                                            </div>                            
                    
                    
                                        </div>-->


                </div>

                <!-- START DASHBOARD CHART -->
                <div class="block-full-width">
                    <div id="dashboard-chart" style="height: 250px; width: 100%; float: left;"></div>
                    <div class="chart-legend">
                        <div id="dashboard-legend"></div>
                    </div>                                                
                </div>                    
                <!-- END DASHBOARD CHART -->

            </div>

            <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
                <div class="mb-container">
                    <div class="mb-middle">
                        <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                        <div class="mb-content">
                            <p>Are you sure you want to log out?</p>                    
                            <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                        </div>
                        <div class="mb-footer">
                            <div class="pull-right">
                                <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                                <button class="btn btn-default btn-lg mb-control-close">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>

    <script type="text/javascript">
        $("#guestusers").click(function () {
            window.location = "<?php echo BASEURL . "Admin_panel/guest_users" ?>";
        });

        $("#registeredusers").click(function () {
            window.location = "<?php echo BASEURL . "Admin_panel/users" ?>";
        });
        $("#paidm").click(function () {
            window.location = "<?php echo BASEURL . "Admin_panel/paid_members" ?>";
        });
        $("#sentgreetings").click(function () {
            window.location = "<?php echo BASEURL . "Greeting_card/sent_greetings" ?>";
        });
        $("#egiftorders").click(function () {
            window.location = "<?php echo BASEURL . "Admin_panel/orders" ?>";
        });
        $("#total_added_reminder").click(function () {
            window.location = "<?php echo BASEURL . "Admin_panel/add_reminder" ?>";
        });
        $("#total_free_greeting_sent").click(function () {
            window.location = "<?php echo BASEURL . "Greeting_card/sent_free_greetings" ?>";
        });

    </script>

