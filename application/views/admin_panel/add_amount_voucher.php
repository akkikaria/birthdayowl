<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>zebra_datepicker.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>core.js"></script>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>

        <div class="page-content">
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
            </ul>
            <div class="page-title">                    
                <h2><span  class="fa fa-plus-circle"></span><?php echo $heading; ?></h2>
            </div>
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body"> 
                                <div class="col-md-12">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Vouchers</label>
                                            <div class="col-md-3 col-xs-12">    
                                                <select id="voucher_pro_id" name="voucher_pro_id" class="form-control">
                                                    <option selected="selected" value="0" >Select Vouchers</option>
                                                    <?php foreach ($vouchers as $vc) { ?>
                                                        <option value="<?php echo $vc["voucher_pro_id"]; ?>"><?php echo $vc["voucher_pro_name"]; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                            <div class="error_r1" style="color: red;margin-left:245px;"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Amount</label>
                                            <div class="col-md-3 col-xs-12">                                        
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>

                                                    <input type="text" class="form-control" name="amount" id="amount" onkeypress="return isNumberKey(event)" value="<?php echo $voucher_data["pamount"]; ?>"/>



                                                </div> 
                                            </div>
                                            <div class="error_r2" style="color: red;margin-left:245px;"></div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">PIN</label>
                                            <div class="col-md-3 col-xs-12">                                        
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>

                                                    <input type="text" class="form-control" name="pin" id="pin"  value="<?php echo $voucher_data["pin"]; ?>"/>
                                                </div> 
                                            </div>
                                            <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Expiry Date</label>
                                            <div class="col-md-3 col-xs-12">                                        
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>

                                                    <input type="text" class="form-control" name="expiry_date" id="popupDatepicker" type="text" placeholder="02 / 03 / 2016"  style=" color: #555;"   name="schedule" style="cursor:pointer;" value="<?php echo $voucher_data["expiry_date"]; ?>"/>
                                                    <span class="input-group-addon add-on">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>

                                                </div> 

                                            </div>
                                            <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="col-md-4">
                                    <div class="btn btn-primary pull-right"  id="<?php echo $btn_click; ?>" v_id="<?php echo $voucher_data["id"] ?>">Submit</div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div> 

                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="delete_menu()" style="margin-left: 1100px;"><span class="fa fa-times"> DELETE</span></button>
                                <h3 class="panel-title">Manage Menu</h3>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    

                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>SELECT VOUCHER</th>
                                                <th>VOUCHER ID</th>
                                                <th>VOUCHER NAME</th>
                                                <th>AMOUNT</th>
                                                <th>PIN</th>
                                                <th>EXPIRY DATE</th>
                                                <th>Edit</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($voucher_amt as $brand) { ?>
                                                <tr>
                                                    <td><input type="checkbox" class="checkbox" value="<?php echo $brand["voucher_id"]; ?>" /></td>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $brand["voucher_pro_name"]; ?></td>
                                                    <td><?php echo $brand["pamount"]; ?></td>
                                                    <td><?php echo $brand["pin"]; ?></td>
                                                    <td><?php echo $brand["expiry_date"]; ?></td>
                                                    <td> <button class="btn btn-default btn-rounded btn-condensed btn-sm edit_brand" voucher_id="<?php echo $brand["voucher_id"]; ?>"><span class="fa fa-pencil"></span></button></td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>

                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>

            </div>            

        </div>

        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var voucher_pro_id = "<?php echo $voucher_data["voucher_id"]; ?>";
        $("#voucher_pro_id").val(voucher_pro_id);
        $("#submit_amount").click(function () {
            var voucher_pro_id = $("#voucher_pro_id").val();
            var amount = $("#amount").val();
            var pin = $("#pin").val();
            var expiry_date = $("#popupDatepicker").val();
            if (voucher_pro_id == '0') {
                alert("Select Voucher");
            } else if (amount == '') {
                alert("Enter amount");
            } else if (pin == '') {
                alert("Enter PIN");
            } else if (expiry_date == '') {
                alert("Enter expiry date ");
            } else {
                var data = {
                    voucher_pro_id: voucher_pro_id,
                    pamount: amount,
                    pin: pin,
                    expiry_date: expiry_date
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Product_voucher/add_voucher_amount",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            alert(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Product_voucher/amount_voucher";
                        }
                    }
                });

            }
        });
        $(".edit_brand").click(function () {
            var voucher_id = $(this).attr("voucher_id");
            window.location = "<?php echo BASEURL; ?>Product_voucher/edit_voucher_amount/" + voucher_id;
        });
        $("#edit_voucher").click(function () {
            var id = $(this).attr("v_id");
            var voucher_pro_id = $("#voucher_pro_id").val();
            var amount = $("#amount").val();
            var pin = $("#pin").val();
            var expiry_date = $("#popupDatepicker").val();
            if (voucher_pro_id == '0') {
                alert("Select Voucher");
            } else if (amount == '') {
                alert("Enter amount");
            } else if (pin == '') {
                alert("Enter PIN");
            } else if (expiry_date == '') {
                alert("Enter expiry date ");
            } else {
                var data = {
                    voucher_pro_id: voucher_pro_id,
                    pamount: amount,
                    pin: pin,
                    expiry_date: expiry_date,
                    id: id
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Product_voucher/voucher_amount_edit_success",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            alert(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Product_voucher/amount_voucher";
                        }
                    }
                });
            }

        });
        function getDate(date) {
            $(".errors7").hide();
            var parts = date.split('/');
            var month = parts[0];
            var bdate = parts[1];
            if ((bdate != '') && (month != '')) {
                var data = {
                    bmonth: month,
                    bday: bdate
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/get_zodiac",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#zodiac_id").val(r.message.toString());
                            return false;
                        } else {
                            $("#zodiac_id").val(r.message.toString());
                        }
                    }
                });
            }
        }
        function isNumberKey(evt)
        {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

    </script>