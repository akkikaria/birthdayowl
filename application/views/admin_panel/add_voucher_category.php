<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <script>
            $(document).ready(function () {
                $("#vouchers").addClass("active");
                $(".vcategory li").addClass("active");
            });
        </script>

        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                </li> 
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     

            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span  class="fa fa-plus-circle"></span><?php echo $heading; ?></h2>
            </div>
            <!-- END PAGE TITLE -->                

            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" id="voucher_submit" action="<?php echo BASEURL . $action; ?>" enctype="multipart/form-data" >
                            <div class="panel panel-default">
                                <div class="panel-body">                                                                        
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Voucher Category Name</label>
                                        <div class="col-md-3 col-xs-12">                                        
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="voucher_cat_name" id="voucher_cat_name" value="<?php echo $voucher_data["voucher_cat_name"]; ?>"/>
                                            </div> 
                                        </div>
                                        <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Image Upload</label>
                                        <div class="col-md-3 col-xs-12">    
                                            <?php if ($voucher_data["voucher_image_icon"] != '') { ?>
                                                <div style="height: 20px; width: 238px; position: absolute; margin-left: 78px; margin-top:100px; background-color: rgb(252, 248, 227);"><span id="change_img"><?php echo $voucher_data["voucher_image_icon"]; ?></span></div>
                                            <?php } ?>
                                            <input type="hidden" id="userfile_old"name="userfile_old" value="<?php echo $voucher_data["voucher_image_icon"]; ?>"  />
                                            <?php
                                            $default_pic = "dashboard.png";
                                            $img = BASEURL_VCAT . $voucher_data["voucher_image_icon"];
                                            if ($voucher_data["voucher_image_icon"] == FALSE) {//It will upload default image
                                                $img = BASEURL_VCAT . $default_pic;
                                            }
                                            ?>
                                            <image src="<?php echo $img ?>" height="100px" width= "100px" style="margin-left:0px;" id="preview"/>
                                            <input type="file" name="userfile" id="userfile" title="Browse file" />
                                            <span>Note:Image size should not be more than 100 X 100</span>
                                        </div>
                                        <div class="error_r3" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="btn btn-primary pull-right"  id="<?php echo $btn_click; ?>">Submit</div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="delete_voucher_category()" style="margin-left: 1100px;"><span class="fa fa-times"> DELETE</span></button>
                                <h3 class="panel-title">Manage Product Voucher Category</h3>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    

                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>SELECT CATEGORY</th>
                                                <th>VOUCHER CATEGORY ID</th>
                                                <th>VOUCHER CATEGORY NAME</th>
                                                <th>Edit</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($voucher_category as $voucher) { ?>
                                                <tr>
                                                    <td><input type="checkbox" class="checkbox" value="<?php echo $voucher["voucher_cat_id"]; ?>" /></td>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $voucher["voucher_cat_name"]; ?></td>
                                                    <td> <button class="btn btn-default btn-rounded btn-condensed btn-sm edit_voucher_category" voucher_cat_id="<?php echo $voucher["voucher_cat_id"]; ?>" onclick="edit_voucher_category('<?php echo $voucher["voucher_cat_id"]; ?>')"><span class="fa fa-pencil"></span></button></td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>

            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->
    <script>
        $("#submit_voucher_category").click(function () {
            var voucher_cat_name = $("#voucher_cat_name").val();
            var userfile = $("#userfile").val();
            if (voucher_cat_name == '') {
                $(".error_r2").html(" Product Voucher Category Name is required");
            }
            $('#voucher_cat_name').keyup(function (e) {
                $(".error_r2").hide();
            });
            if (userfile == '') {
                $(".error_r3").html("Image upload is required.");
            } else {
                $(".error_r3").hide();
            }
            if ((voucher_cat_name != '') && (userfile != '')) {
                alert("Product Voucher Category added Successfully!.");
                $("#voucher_submit").submit();
            }
        });
        function edit_voucher_category(voucher_cat_id) {
            window.location = "<?php echo BASEURL; ?>Product_voucher/edit_voucher_category/" + voucher_cat_id;
        }
        $("#edit_voucher_category").click(function () {
            var voucher_cat_name = $("#voucher_cat_name").val();
            var userfile_old = $("#userfile_old").val();
            if (voucher_cat_name == '') {
                $(".error_r2").html(" Product Voucher Category Name is required");
            }
            $('#voucher_cat_name').keyup(function (e) {
                $(".error_r2").hide();
            });
            if (userfile_old == '') {
                $(".error_r3").html("Image upload is required.");
            } else {
                $(".error_r3").hide();
            }
            if ((voucher_cat_name != '') && (userfile_old != '')) {
                alert("Product Voucher Category Updated Successfully!.");
                $("#voucher_submit").submit();
            }
        });
//        function imageIsLoaded(e) {
//            $("#userfile").css("color", "green");
//            $('#image_preview').css("display", "block");
//            $('#previewing').attr('src', e.target.result);
//        }
        $("#userfile").change(function () {
            //Get reference of FileUpload.
            var fileUpload = $("#userfile")[0];
            var file = this.files[0];
            //Check whether the file is valid Image.
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
            if (regex.test(fileUpload.value.toLowerCase())) {
                //Check whether HTML5 is supported.
                if (typeof (fileUpload.files) != "undefined") {
                    //Initiate the FileReader object.
                    var reader = new FileReader();
                    //Read the contents of Image File.
                    reader.readAsDataURL(fileUpload.files[0]);
                    reader.onload = function (e) {
                        //Initiate the JavaScript Image object.
                        var image = new Image();
                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;
                        image.name = file.name;
                        image.onload = function () {
                            //Determine the Height and Width.
                            var height = this.height;
                            var width = this.width;
                            if (height > 100 || width > 100) {
                                $("#userfile").val("");
                                alert("Height and Width must not exceed 100px.");
                                return false;
                            } else {
                                $("#change_img").html(image.name);
                                $("#preview").attr('src', e.target.result);
                                alert("Image Uploaded Successfully");
                                return true;
                            }
                        };
                    }
                } else {
                    alert("This browser does not support HTML5.");
                    return false;
                }
            } else {
                alert("Please select a valid Image file.");
                return false;
            }
        });

        function delete_voucher_category() {
            var checkboxArray = new Array();
            var checkedValues = $('.checkbox:checked').map(function () {
                checkboxArray.push(this.value);
            });
            if (checkboxArray == '') {
                alert("Select at least one Voucher Category");
            } else {
                var ask = window.confirm("Are you sure you want to delete?");
                if (ask) {
                    var data = {
                        voucher_cat_id: checkboxArray
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo BASEURL; ?>Product_voucher/delete_voucher_category",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.success.toString() === "false") {
                                $("#message").html(r.message.toString());
                                return false;
                            } else {
                                alert(r.message.toString());
                                window.location = "<?php echo BASEURL; ?>Product_voucher/voucher_category";
                            }
                        }
                    });

                } else {
                    window.location = "<?php echo BASEURL; ?>Product_voucher/voucher_category";
                }
            }
        }


    </script>