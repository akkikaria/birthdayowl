<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <script>
        $(document).ready(function () {
//            $("#footermenu").addClass("active");
//            $(".faq li").addClass("active");
        });
    </script>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     

            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2 id="heading1"><span  class="fa fa-plus-circle" ></span>Add E-Greeting Category</h2>
                <h2 id="heading2"><span  class="fa fa-plus-circle" id="heading2" style="display: none;"></span>Edit E-Greeting Category</h2>
            </div>
            <!-- END PAGE TITLE -->                

            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-body">                                                                        
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Greeting Category Name</label>
                                        <div class="col-md-3 col-xs-12">                                        
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="greet_cat_name" id="greet_cat_name"/>
                                            </div> 
                                        </div>
                                        <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Greeting category Type</label>
                                        <div class="col-md-3 col-xs-12">    
                                            <select id="greeting_type" name="greeting_type" class="form-control">
                                                <option selected="selected" value="0" >Select Greeting Type</option>
                                                <option value="1">Free Greetings</option>
                                                <option value="2">Product Voucher Greetings</option>
                                            </select>
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="error_r1" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="col-md-5">
                                        <button class="btn btn-primary pull-right"  id="submit_greeting_category" category_id="<?php //echo $category["category_id"]               ?>">Submit</button>
                                        <button class="btn btn-primary pull-right"  style="display:none;" id="update_greeting_category" greet_cat_id="">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="delete_greeting_category()" style="margin-left: 1100px;"><span class="fa fa-times"> DELETE</span></button>
                                <h3 class="panel-title">Manage E-Greeting Category</h3>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    

                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>SELECT CATEGORY</th>
                                                <th>GREETING CATEGORY ID</th>
                                                <th>GREETING CATEGORY NAME</th>
                                                <th>Edit</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($greeting_category as $greet_cat) { ?>
                                                <tr>
                                                    <td><input type="checkbox" class="checkbox" value="<?php echo $greet_cat["greet_cat_id"]; ?>" /></td>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $greet_cat["greet_cat_name"]; ?></td>
                                                    <td> <button class="btn btn-default btn-rounded btn-condensed btn-sm edit_greeting_category" greet_cat_id="<?php echo $greet_cat["greet_cat_id"]; ?>"><span class="fa fa-pencil"></span></button></td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>

            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->
    <script>
        $("#heading2").hide();
        $("#submit_greeting_category").click(function () {
            var greet_cat_name = $("#greet_cat_name").val();
            var greeting_type = $("#greeting_type").val();
            if (greet_cat_name == '') {
                alert("Greeting Category Name is required");
                //$(".error_r2").html(" Greeting Category Name is required");
            } else if (greeting_type == "0") {
                alert("Select Greeting type");
            } else {
//            
//            $('#greet_cat_name').keyup(function (e) {
//                $(".error_r2").hide();
//            });
//          
//            if (greet_cat_name != '') {
                var data = {
                    greet_cat_name: greet_cat_name,
                    greeting_type: greeting_type
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Greeting_card/add_greeting_category",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#message").html(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Greeting_card/greeting_category";
                        }
                    }
                });
            }
        });
        //   }
        $(".edit_greeting_category").click(function () {
            var data = {
                greet_cat_id: $(this).attr("greet_cat_id")
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Greeting_card/get_greeting_category_details",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        var greet_cat_id = r.greetingcategory["greet_cat_id"];
                        var greet_cat_name = r.greetingcategory["greet_cat_name"];
                        $("#greet_cat_name").val(greet_cat_name);
                        $("#update_greeting_category").attr("greet_cat_id", greet_cat_id);
                        $("#heading1").hide();
                        $("#heading2").show();
                        $("#submit_greeting_category").hide();
                        $("#update_greeting_category").show();
                    }
                }
            });
        });
        $("#update_greeting_category").click(function () {
            var greet_cat_id = $(this).attr("greet_cat_id");
            var greet_cat_name = $("#greet_cat_name").val();
            if (greet_cat_name == '') {
                $(".error_r2").html("Greeting Category Name is required");
            }
            $('#greet_cat_name').keyup(function (e) {
                $(".error_r2").hide();
            });
            if ((greet_cat_name != '')) {
                var data = {
                    greet_cat_id: greet_cat_id,
                    greet_cat_name: greet_cat_name
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Greeting_card/update_greeting_category",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#message").html(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Greeting_card/greeting_category";
                        }
                    }
                });
            }
        });
        function delete_greeting_category() {
            var checkboxArray = new Array();
            var checkedValues = $('.checkbox:checked').map(function () {
                checkboxArray.push(this.value);
            });
            if (checkboxArray == '') {
                alert("Select at least one Category");
            } else {
                var ask = window.confirm("Are you sure you want to delete?");
                if (ask) {
                    var data = {
                        greet_cat_id: checkboxArray
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo BASEURL; ?>Greeting_card/delete_greeting_category",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.success.toString() === "false") {
                                $("#message").html(r.message.toString());
                                return false;
                            } else {
                                alert(r.message.toString());
                                window.location = "<?php echo BASEURL; ?>Greeting_card/greeting_category";
                            }
                        }
                    });
                } else {
                    window.location = "<?php echo BASEURL; ?>Greeting_card/greeting_category";
                }
            }
        }
    </script>