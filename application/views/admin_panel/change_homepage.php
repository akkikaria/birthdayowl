<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
         <script>
        $(document).ready(function () {
            $("#banners").addClass("active");
            $(".homepage li").addClass("active");
        });
    </script>
 
   <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>

        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                   <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                </li> 
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     

            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span class="fa fa-arrow-circle-o-left"></span><?php echo $heading; ?></h2>
            </div>
            <!-- END PAGE TITLE -->                

            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" id="hapges_submit1" action="<?php echo BASEURL; ?>Admin_panel/add_home_screen/1" enctype="multipart/form-data" >
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <table>
                                        <tr>
                                            <td style=" padding-right: 17px;">
                                                <label class="control-label" style="width:117px"> BANNER 1</label>
                                            </td>
                                            <td style="padding-bottom: 15px;">
<!--                                               <div style=" margin-top: 102px; margin-left: 70px; width: 122px; height: 23px; position: absolute;  background-color: rgb(252, 248, 227);"><span id="change_img"><?php //echo $screens["image"];                        ?></span></div>-->
                                                <?php
                                                $default_pic = "no_image.jpg";
                                                if ($screen_count > 0) {
                                                    $img = BASEURL_HPIC . $screens[0]["image"];
                                                    if ($screens[0]["image"] == FALSE) {
                                                        $img = BASEURL_HPIC . $default_pic;
                                                    }
                                                }else{
                                                      $img = BASEURL_HPIC . $default_pic;
                                                }
                                                ?>
                                                <img id="uploadPreview1" src="<?php echo $img; ?>"  /><br />
                                                <input type="file" name="userfile" id="uploadImage1"  onclick="PreviewImage1(1);"value="" />
                                            </td>
                                            <td>
                                                
                                            </td>
                                        </tr>
                                         
                                        <br/> <br/>
                                    </table>
                                  
                                </div>
                                <div class="col-md-2">
                                                    <div class="btn btn-primary pull-right" id="submit1">Submit</div>
                                                </div>
                            </div>
                        </form>
<!--                        <form class="form-horizontal" method="post" id="hapges_submit2" action="<?php echo BASEURL; ?>Admin_panel/add_home_screen/2" enctype="multipart/form-data" >
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <table>
                                        <tr>
                                            <td style=" padding-right: 17px;">
                                                <label class="control-label" style="width:117px"> BANNER 2</label>
                                            </td>
                                            <td style="padding-bottom: 15px;">
                                                <?php //if ($screens["image"] != '') { ?>
                                                <div style=" margin-top: 102px; margin-left: 70px; width: 122px; height: 23px; position: absolute;  background-color: rgb(252, 248, 227);"><span id="change_img"><?php echo $screens["image"];                        ?></span></div>
                                                <?php // } ?>
                                                 <?php
                                                $default_pic = "no_image.jpg";
                                                if ($screen_count > 1) {

                                                    $img = BASEURL_HPIC . $screens[1]["image"];
                                                    if ($screens[1]["image"] == FALSE) {
                                                        $img = BASEURL_HPIC . $default_pic;
                                                    }
                                                }else{
                                                      $img = BASEURL_HPIC . $default_pic;
                                                }

                                                ?>
                                                <img id="uploadPreview2" src="<?php echo $img;     ?>" style="width:100px;height: 100px" /><br />
                                                <input type="file" name="userfile" id="uploadImage2"  onclick="PreviewImage(2);" value=""/>

                                            </td>
                                            
                                            <td>
                                                <div class="col-md-2">
                                                    <div class="btn btn-primary pull-right" id="submit2">Submit</div>
                                                </div>
                                            </td>
                                        </tr>  
                                           
                                    </table>
                                    <span class="col-md-6 col-md-offset-1">Note:Image size should  be more than 300px X 300px</span>   
                                </div>
                            </div>
                        </form>
                        <form class="form-horizontal" method="post" id="hapges_submit3" action="<?php echo BASEURL; ?>Admin_panel/add_home_screen/3" enctype="multipart/form-data" >
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <table>                                  
                                        <tr>
                                            <td style=" padding-right: 17px;">
                                                <label class="control-label" style="width:117px"> BANNER 3</label>
                                            </td>
                                            <td style="padding-bottom: 15px;">
                                                <?php //if ($screens["image"] != '') { ?>
                                                <div style=" margin-top: 102px; margin-left: 70px; width: 122px; height: 23px; position: absolute;  background-color: rgb(252, 248, 227);"><span id="change_img"><?php //echo $screens["image"];                        ?></span></div>
                                                <?php // } ?>
                                                   <?php
                                                $default_pic = "no_image.jpg";
                                                if ($screen_count > 1 && $screen_count > 2) {
                                                    $img = BASEURL_HPIC . $screens[2]["image"];
                                                    if ($screens[2]["image"] == FALSE) {//It will upload default image
                                                        $img = BASEURL_HPIC . $default_pic;
                                                    }
                                                }else{
                                                      $img = BASEURL_HPIC . $default_pic;
                                                }
                                                ?>
                                                <img id="uploadPreview3" src="<?php echo $img;?>" style="width:100px;height: 100px" /><br />
                                                <input type="file" name="userfile" id="uploadImage3"  onclick="PreviewImage(3);"  value=""/>
                                            </td>
                                            <td>
                                                <div class="col-md-2">
                                                    <div class="btn btn-primary pull-right" id="submit3">Submit</div>
                                                </div>
                                            </td>
                                        </tr>
                                         
                                    </table>
                                     <span class="col-md-6 col-md-offset-1">Note:Image size should  be more than 300px X 300px</span>
                                </div>
                            </div>
                        </form>-->
                    </div>
                </div>
            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->
    <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->   
    <script type="text/javascript">
         function PreviewImage(no) {
            $("#uploadImage" + no).change(function () {
                //Get reference of FileUpload.
                var fileUpload = $(this)[0];
                var file = this.files[0];
                //Check whether the file is valid Image.
                var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
                if (regex.test(fileUpload.value.toLowerCase())) {
                    //Check whether HTML5 is supported.
                    if (typeof (fileUpload.files) != "undefined") {
                        //Initiate the FileReader object.
                        var reader = new FileReader();
                        //Read the contents of Image File.
                        reader.readAsDataURL(fileUpload.files[0]);
                        reader.onload = function (e) {
                            //Initiate the JavaScript Image object.
                            var image = new Image();
                            //Set the Base64 string return from FileReader as source.
                            image.src = e.target.result;
                            image.name = file.name;
                            image.onload = function () {
                                //Determine the Height and Width.
                            var height = this.height;
                            var width = this.width;
//                            if (height <300 || width <300) {
//                              //  $("#userfile").val("");
//                              // $('#uploadPreview' + no).attr('src', e.target.result);
//                                alert("Height and Width should be more than 300px X 300px.");
//                                return false;
//                            }else{
                                $('#uploadPreview' + no).attr('src', e.target.result);
                                alert("Image Uploaded Successfully");
                                return true;
//                               }
                            };
                        }
                    } else {
                        alert("This browser does not support HTML5.");
                        return false;
                    }
                } else {
                    alert("Please select a valid Image file.");
                    return false;
                }
            });
        }
        
        function PreviewImage1(no) {
            $("#uploadImage" + no).change(function () {
                //Get reference of FileUpload.
                var fileUpload = $(this)[0];
                var file = this.files[0];
                //Check whether the file is valid Image.
                var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
                if (regex.test(fileUpload.value.toLowerCase())) {
                    //Check whether HTML5 is supported.
                    if (typeof (fileUpload.files) != "undefined") {
                        //Initiate the FileReader object.
                        var reader = new FileReader();
                        //Read the contents of Image File.
                        reader.readAsDataURL(fileUpload.files[0]);
                        reader.onload = function (e) {
                            //Initiate the JavaScript Image object.
                            var image = new Image();
                            //Set the Base64 string return from FileReader as source.
                            image.src = e.target.result;
                            image.name = file.name;
                            image.onload = function () {
                                //Determine the Height and Width.
                            var height = this.height;
                            var width = this.width;
//                            if (height <350 || width <300) {
//                              //  $("#userfile").val("");
//                              // $('#uploadPreview' + no).attr('src', e.target.result);
//                                alert("Height and Width should be more than 350px X 300px.");
//                                return false;
//                            }else{
                                $('#uploadPreview' + no).attr('src', e.target.result);
                                alert("Image Uploaded Successfully");
                                return true;
//                               }
                            };
                        }
                    } else {
                        alert("This browser does not support HTML5.");
                        return false;
                    }
                } else {
                    alert("Please select a valid Image file.");
                    return false;
                }
            });
        }
        $("#submit1").click(function () {
            var uploadImage1 = $("#uploadImage1").val();
            if (uploadImage1 == '') {
                alert("Upload Images");
            } else if (uploadImage1 != '') {
                pageLoadingFrame("show");
                setTimeout(function () {
                    pageLoadingFrame("hide");
                }, 2000, alert("Home screens added successfully"));
                $("#hapges_submit1").submit();
            }
        });
        $("#submit2").click(function () {
            var uploadImage2 = $("#uploadImage2").val();
            if (uploadImage2 == '') {
                alert("Upload Images");
            } else if (uploadImage2 != '') {
                pageLoadingFrame("show");
                setTimeout(function () {
                    pageLoadingFrame("hide");
                }, 2000, alert("Home screens added successfully"));
                $("#hapges_submit2").submit();
            }
        });
        $("#submit3").click(function () {
            var uploadImage3 = $("#uploadImage3").val();
            if (uploadImage3 == '') {
                alert("Upload Images");
            } else if (uploadImage3 != '') {
                pageLoadingFrame("show");
                setTimeout(function () {
                    pageLoadingFrame("hide");
                }, 2000, alert("Home screens added successfully"));
                $("#hapges_submit3").submit();
            }
        });
        $("#update").click(function () {
            pageLoadingFrame("show");
            setTimeout(function () {
                pageLoadingFrame("hide");
            }, 2000, alert("Home screens Updated successfully"));
            $("#hapges_submit").submit();

        });

    </script>



