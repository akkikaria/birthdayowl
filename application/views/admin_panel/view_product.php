<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <script>
            $(document).ready(function () {
                $("#flowers").addClass("active");
                $(".product_folder li").addClass("active");
                $(".view_product li").addClass("active");
                $(".add_product li").removeClass("active");
            });
        </script>
        <!-- START PAGE SIDEBAR -->
        <div class="page-sidebar">
            <!-- START X-NAVIGATION -->
            <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
            <!-- END X-NAVIGATION -->
        </div>
        <!-- END PAGE SIDEBAR -->

        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     
            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span class="fa fa-arrow-circle-o-left"></span>View Products</h2>
            </div>
            <!-- END PAGE TITLE -->                
            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">

                                <button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="delete_product()" style="margin-left: 1254px;"><span class="fa fa-times"> DELETE</span></button>
                                <div class="form-group">
                                    <div class="col-md-3 col-xs-12">
                                        <label>Select Category:</label>
                                        <select id="change_view" name="change_view" class="form-control">
                                            <!--<option value="0" >View only Sub-Category Items</option>-->
                                            <option value="0">All</option>
                                            <?php foreach ($category_list as $category1) { ?>
                                                <option category_name="<?php echo $category1["category_name"]; ?>" value="<?php echo $category1["category_id"]; ?>"  ><?php echo $category1["category_name"]; ?></option>
                                            <?php } ?>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive" style="overflow-x: scroll;">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Edit</th>
                                                <th>PRODUCT NAME</th>
                                                <th>PRODUCT IMAGE</th>
                                                <th>PRODUCT CONTENT</th>
                                                <th>PRODUCT DESCRIPTION</th>
                                                <th>DELIVERY INFORMATION</th>
                                                <th>CARE INSTRUCTION</th>
                                                <th>CATEGORY</th>
                                                <th>PRICE</th>

                                                <th>HSN CAKES</th>
                                                <th>GST CAKES</th>
                                                <th>HSN FLOWERS</th>
                                                <th>GST FLOWERS</th>
                                                <th>HSN DRY FRUITS</th>
                                                <th>GST DRY FRUITS</th>
                                                <th>HSN SWEETS</th>
                                                <th>GST SWEETS</th>
                                                <th>HSN CHOCOLATES</th>
                                                <th>GST CHOCOLATES</th>
                                                <th>HSN TEDDY</th>
                                                <th>GST TEDDY</th>
                                                <th>STOCK STATUS</th>
<!--<th>Featured</th>-->
<!--<th>Selling</th>-->
<!--<th>Latest</th>-->

                                            </tr>
                                        </thead>
                                        <tbody id="">
                                            <?php $i = 1; ?>
                                            <?php
                                            foreach ($category as $category_data) {
                                                ?>
                                                <tr>
                                                    <td><input type="checkbox" class="checkbox" value="<?php echo $category_data["pro_id"]; ?>" /></td>
                                                    <td> <div class="btn btn-default btn-rounded btn-condensed btn-sm" onclick="edit_category(<?php echo $category_data["pro_id"]; ?>)" ><span class="fa fa-pencil"></span></div></td>
                                                    <td><?php echo $category_data["product_name"]; ?></td>
                                                    <td><img src="<?php echo BASEURL_PRODUCT_IMG . $category_data["product_image"]; ?>" height="100px" width= "125px"></td>
                                                    <td><?php echo $category_data["product_content"]; ?></td>
                                                    <td><?php echo $category_data["product_description"]; ?></td>
                                                    <td><?php echo $category_data["delivery_information"]; ?></td>
                                                    <td><?php echo $category_data["care_instruction"]; ?></td>
                                                    <td><?php echo $category_data["category_name"]; ?></td>
                                                    <td><?php echo $category_data["price"]; ?></td>

                                                    <td><?php echo $category_data["HSN_code_cake"]; ?></td>
                                                    <td><?php echo $category_data["GST_cake"]; ?></td>
                                                    <td><?php echo $category_data["HSN_code_flower"]; ?></td>
                                                    <td><?php echo $category_data["GST_flower"]; ?></td>
                                                    <td><?php echo $category_data["HSN_code_dry_fruits"]; ?></td>
                                                    <td><?php echo $category_data["GST_dry_fruits"]; ?></td>
                                                    <td><?php echo $category_data["HSN_code_sweets"]; ?></td>
                                                    <td><?php echo $category_data["GST_sweets"]; ?></td>
                                                    <td><?php echo $category_data["HSN_code_chocolate"]; ?></td>
                                                    <td><?php echo $category_data["GST_chocolate"]; ?></td>
                                                    <td><?php echo $category_data["HSN_code_teddy"]; ?></td>
                                                    <td><?php echo $category_data["GST_teddy"]; ?></td>
                                                    <?php
                                                    $stock_status = $category_data["stock_status_id"];

                                                    switch ($stock_status) {
                                                        case 1:
                                                            $stock_status_value = 'Deactive';
                                                            break;
                                                        case 2:
                                                            $stock_status_value = 'In Stock';
                                                            break;
                                                        case 3:
                                                            $stock_status_value = 'Out Of Stock';
                                                            break;
                                                        default:
                                                            echo "Your favorite color is neither red, blue, nor green!";
                                                    }
                                                    ?>
                                                    <td><?php echo $stock_status_value; ?></td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>
            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

    <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->
    <script type="text/javascript">
//        $(document).ready(function () {
//            diplay_view(1);
////            //                                                $("#change_view").change(function () {
////            //                                                    var id = $(this).val();
////            //                                                    diplay_view(id);
////            //                                                });
//        });
//        function diplay_view(id) {
//            //            if (id == 1) {
//            var data = {
//                id: id
//            }
//            $.ajax({
//                type: "POST",
//                url: "<?php // echo BASEURL;                                                                                                                        ?>Admin_panel/get_product_data",
//                data: data,
//                dataType: "json",
//                success: function (r) {
//                    if (r.success.toString() === "false") {
//                        $("#message").html(r.message.toString());
//                        return false;
//                    } else {
//                        var image = '';
//                        var cat_count = r.category.length;
//                        for (var i = 1; i < cat_count; i++) {
//                            image += '<tr>'
//                                    + '<td> <input type="checkbox" class="checkbox" value="' + r.category[i]["pro_id"] + '" /> </td>'
//                                    + '<td>' + r.category[i]["product_name"] + '</td>'
//                                    + '<td>' + r.category[i]["product_description"] + '</td>'
//                                    + '<td>' + r.category[i]["delivery_information"] + '</td>'
//                                    + '<td>' + r.category[i]["care_instruction"] + '</td>'
//                                    + '<td>' + r.category[i]["category_name"] + '</td>'
//                                    + '<td> <div class="btn btn-default btn-rounded btn-condensed btn-sm" onclick="edit_category(' + r.category[i]["pro_id"] + ')" ><span class="fa fa-pencil"></span></div></td>'
//                                    + '</tr>';
//                        }
//                        $("#product_data").html(image);
//                    }
//                }
//            });
//            //            }
//        }

        var segments = $(location).attr('href').split('/');
<?php if (SERVER_TYPE == 1) { ?>
            var id = segments[5];//Live
<?php } else { ?>
            var id = segments[6];//Local
<?php } ?>
        $("#change_view").val(id);

        function edit_category(pro_id) {
            localStorage.setItem('category_id', id);
            window.location = "<?php echo BASEURL; ?>Admin_panel/edit_product/" + pro_id;
        }


        function change_category(category_id) {
            window.location = "<?php echo BASEURL; ?>Admin_panel/view_product/" + category_id;
        }

        $("#change_view").change(function () {
            var category_id = $(this).val();
            change_category(category_id);
        });

        function delete_product() {
            var checkboxArray = new Array();
            var checkedValues = $('.checkbox:checked').map(function () {
                checkboxArray.push(this.value);
            });
            if (checkboxArray == '') {
                alert("Select Product");
            } else {
                var ask = window.confirm("Are you sure you want to delete?");
                if (ask) {
                    var data = {
                        id: checkboxArray,
                        from: 13
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo BASEURL; ?>Admin_panel/CommonDelete",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.success.toString() === "false") {
                                $("#message").html(r.message.toString());
                                return false;
                            } else {
                                alert(r.message.toString());
                                window.location = "<?php echo BASEURL; ?>Admin_panel/view_product/" + id;
                            }
                        }
                    });
                } else {
                    window.location = "<?php echo BASEURL; ?>Admin_panel/view_product/" + id;
                }
            }
        }
    </script>





