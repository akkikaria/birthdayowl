<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <script>
            $(document).ready(function () {
                $("#flowers").addClass("active");
                $(".product_folder li").addClass("active");
                $(".add_product li").addClass("active");
                $(".view_product li").removeClass("active");
            });
        </script>
        <!-- START PAGE SIDEBAR -->
        <div class="page-sidebar">
            <!-- START X-NAVIGATION -->
            <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
            <!-- END X-NAVIGATION -->
        </div>
        <!-- END PAGE SIDEBAR -->

        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     
            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span class="fa fa-arrow-circle-o-left"></span>ADD PRODUCT</h2>
            </div>
            <!-- END PAGE TITLE -->                
            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <form  method="post" id="product_form" action="<?php echo BASEURL . $action; ?>" enctype="multipart/form-data">
                            <div class="form-horizontal">
                                <div class="panel panel-default">
                                    <div class="panel-body">    
                                        <div class="form-group">
                                            <label class="col-md-1 col-xs-12 control-label">Item Code</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="product_code" id="product_code" value="<?php echo $product_data[0]["product_code"]; ?>"/>
                                                </div>                                            
                                            </div>
                                            <label class="col-md-2 col-xs-12 control-label">HSN Cakes</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="hsn_cakes" id="hsn_cake" value="<?php echo $product_data[0]["HSN_code_cake"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-1 col-xs-12 control-label">Main Menu</label>
                                            <div class="col-md-3 col-xs-12">
                                                <select id="menu_id" name="menu_id" class="form-control">
                                                    <option selected="selected" value="">Select Menu</option>
                                                    <?php foreach ($menu_list as $menu) { ?>
                                                        <option value="<?php echo $menu["menu_id"]; ?>" ><?php echo $menu["menu_name"]; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="help-block"></span>
                                                <div class="error_r1" style="color: red;margin-left:245px;"></div>
                                            </div>

                                            <label class="col-md-2 col-xs-12 control-label">GST Cakes</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="gst_cakes" id="gst_cake" value="<?php echo $product_data[0]["GST_cake"]; ?>"/>
                                                </div>                                            
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-1 col-xs-12 control-label">Category</label>
                                            <div class="col-md-3 col-xs-12">    
                                                <select id="category_id" name="category_id" class="form-control">
                                                    <option selected="selected" value="">Select Category</option>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                            <label class="col-md-2 col-xs-12 control-label">HSN Flowers</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="hsn_flowers" id="hsn_flowers" value="<?php echo $product_data[0]["HSN_code_flower"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-1 col-xs-12 control-label">Product Name</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="product_name" id="product_name" value="<?php echo $product_data[0]["product_name"]; ?>"/>
                                                </div>                                            
                                            </div>
                                            <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                            <label class="col-md-2 col-xs-12 control-label">GST Flowers</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="gst_flowers" id="gst_flowers" value="<?php echo $product_data[0]["GST_flower"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-1 col-xs-12 control-label">Meta Tags</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="meta_tags" id="meta_tags" value="<?php echo $product_data[0]["meta_tags"]; ?>"/>
                                                </div>                                            
                                            </div>
                                            <label class="col-md-2 col-xs-12 control-label">HSN Dry Fruits</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="hsn_dry_fruits" id="hsn_dry_fruits" value="<?php echo $product_data[0]["HSN_code_dry_fruits"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-1 col-xs-12 control-label">Product Content</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <textarea class="form-control" rows="2" name="product_content" id="product_content"><?php echo $product_data[0]["product_content"]; ?></textarea>
                                            </div>
                                            <label class="col-md-2 col-xs-12 control-label">GST Dry Fruits</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="gst_dry_fruits" id="gst_dry_fruits" value="<?php echo $product_data[0]["GST_dry_fruits"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-1 col-xs-12 control-label">Description</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <textarea class="form-control" rows="2" name="product_description" id="product_description"><?php echo $product_data[0]["product_description"]; ?></textarea>
                                            </div>
                                            <label class="col-md-2 col-xs-12 control-label">HSN Sweets</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="hsn_sweets" id="hsn_sweets" value="<?php echo $product_data[0]["HSN_code_sweets"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-1 col-xs-12 control-label">Delivery Information</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <textarea class="form-control" rows="2" name="delivery_information" id="delivery_information"><?php echo $product_data[0]["delivery_information"]; ?></textarea>
                                            </div>
                                            <label class="col-md-2 col-xs-12 control-label">GST Sweets</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="gst_sweets" id="gst_sweets" value="<?php echo $product_data[0]["GST_sweets"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-1 col-xs-12 control-label">Care Instruction</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <textarea class="form-control" rows="2" name="care_instruction" id="care_instruction"><?php echo $product_data[0]["care_instruction"]; ?></textarea>
                                            </div>
                                            <label class="col-md-2 col-xs-12 control-label">HSN Chocolates</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="hsn_chocolates" id="hsn_chocolates" value="<?php echo $product_data[0]["HSN_code_chocolate"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-1 col-xs-12 control-label">Price</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="price" id="price" value="<?php echo $product_data[0]["price"]; ?>"/>
                                                </div>                                            
                                            </div>
                                            <div class="error_r3" style="color: red;margin-left:245px;"></div>
                                            <label class="col-md-2 col-xs-12 control-label">GST Chocolates</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="gst_chocolates" id="gst_chocolates" value="<?php echo $product_data[0]["GST_chocolate"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-1 col-xs-12 control-label">Out Of Stock Status</label>
                                            <div class="col-md-3 col-xs-12">    
                                                <select id="stock_status_id" name="stock_status_id" class="form-control">
                                                    <option selected="selected" value="" >Select</option>
                                                    <?php foreach ($stock as $stock) { ?>
                                                        <option value="<?php echo $stock["id"]; ?>"><?php echo $stock["status"]; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                            <label class="col-md-2 col-xs-12 control-label">HSN Teddy</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="hsn_teddy" id="hsn_teddy" value="<?php echo $product_data[0]["HSN_code_teddy"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-1 col-xs-12 control-label">Product Images</label>
                                            <div class="col-md-3 col-xs-12">  
                                                <?php if ($product_data[0]["product_image"] != '') { ?>
                                                    <div style="height: 20px; width: 238px; position: absolute; margin-left: 78px; margin-top: 3px; background-color: rgb(252, 248, 227);"><span id="change_img"><?php echo $product_data[0]["product_image"]; ?></span></div>
                                                <?php } ?>
                                                <input type="hidden" id="userfile_old" name="userfile_old1" value="<?php echo $product_data[0]["product_image"]; ?>" />
                                                <input type="file"  name="userfile[]" id="userfile" title="Browse file"/>
                                            </div>
                                            <label class="col-md-2 col-xs-12 control-label">GST Teddy</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="gst_teddy" id="gst_teddy" value="<?php echo $product_data[0]["GST_teddy"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="btn btn-primary pull-right" style=" margin-left: 0px;" id="<?php echo $btn_click; ?>" pro_id="<?php echo $product_data[0]["pro_id"] ?>">Submit</div>
                                            <div class="btn btn-default pull-right" style=" margin-right: 10px; display: none;" id="cancel" pro_id="<?php echo $product_data[0]["pro_id"] ?>">Cancel</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->
    <script type="text/javascript">
        $(document).ready(function () {
            var menu_id = '<?php echo $product_data[0]["menu_id"]; ?>';
            $("#menu_id").val(menu_id);
            var category_id = '<?php echo $product_data[0]["category_id"]; ?>';
            $("#category_id").val(category_id);
            var stock_status_id = '<?php echo $product_data[0]["stock_status_id"]; ?>';
            $("#stock_status_id").val(stock_status_id);
            if (menu_id != '') {
                change_menu(menu_id);
                change_menu_subCategory(category_id);
            }

        });
        var btn_click = '<?php echo $btn_click; ?>';
        if (btn_click == 'edit_product') {
            var segments = $(location).attr('href').split('/');
<?php if (SERVER_TYPE == 1) { ?>
                var edit_id = segments[5];//Live
<?php } else { ?>
                var edit_id = segments[6];//Local
<?php } ?>
            $('#cancel').show();
        }
        var id = localStorage.getItem('category_id');

        $('#cancel').click(function () {
            window.location = "<?php echo BASEURL; ?>Admin_panel/view_product/" + id;
        });
        $("#menu_id").change(function () {
            var menu_id = $(this).val();
            change_menu(menu_id);
            change_menu_subCategory(menu_id);
        });

        $(document).on('click', "#submit_product", function () {
            var menu_id = $("#menu_id").val();
            var product_name = $("#product_name").val();
            var price = $("#price").val();
            if (menu_id == '') {
                $(".error_r1").html("Select Menu");
            }
            $('#menu_id').change(function (e) {
                $(".error_r1").hide();
            });
            if (product_name == '') {
                $(".error_r2").html("Enter Product Name");
            }
            $('#product_name').keyup(function (e) {
                $(".error_r2").hide();
            });
            if (price == '') {
                $(".error_r3").html("Enter Price");
            }
            $('#price').keyup(function (e) {
                $(".error_r3").hide();
            });
            if ((menu_id != '') && (product_name != '') && (price != '')) {
                //                $("#product_form").submit();
                var formData = new FormData($("#product_form")[0]);
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/product_success",
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            alert(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/view_product/0";
                            return true;

                        }
                    }
                });
            }
        });

        $("#edit_product").click(function () {

            var menu_id = $("#menu_id").val();
            var product_name = $("#product_name").val();
            var price = $("#price").val();
            if (menu_id == '') {
                $(".error_r1").html("Select Menu");
            }
            $('#menu_id').change(function (e) {
                $(".error_r1").hide();
            });
            if (product_name == '') {
                $(".error_r2").html("Enter Product Name");
            }
            $('#product_name').keyup(function (e) {
                $(".error_r2").hide();
            });
            if (price == '') {
                $(".error_r3").html("Enter Price");
            }
            $('#price').keyup(function (e) {
                $(".error_r3").hide();
            });
            if ((menu_id != '') && (product_name != '') && (price != '')) {
                //                $("#product_form").submit();
                var formData = new FormData($("#product_form")[0]);
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/productEdit_success/" + edit_id,
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            alert(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/view_product/" + id;
                            return true;

                        }
                    }
                });
            }
        });
        function change_menu(menu_id) {
            var data = {
                menu_id: menu_id
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/get_category_name",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        var image = '';
                        var cat_count = r.subcategory.length;
                        for (i = 0; i < cat_count; i++) {
                            image += '<option value="' + r.subcategory[i]["category_id"] + '">' + r.subcategory[i]["category_name"] + '</option>'
                        }
                        $("#category_id").html(image);
                        var category_id = '<?php echo $product_data[0]["category_id"]; ?>';
                        if (category_id != '') {
                            $("#category_id").val(category_id);
                        }
                    }
                }
            });
        }
        function change_menu_subCategory(menu_id) {
            var data = {
                menu_id: menu_id
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/get_SubCategory_name",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        var image = '';
                        var cat_count = r.subcategory.length;
                        for (i = 0; i < cat_count; i++) {
                            image += '<option value="' + r.subcategory[i]["subcategory_id"] + '">' + r.subcategory[i]["subcategory_name"] + '</option>'
                        }
                        $("#subcategory_id").html(image);
                    }
                }
            });
        }

        function change_category(category_id) {
            var data = {
                is_Subcategory: category_id
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/SubCategory_name",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        var image = '';
                        var cat_count = r.subcategory.length;
                        for (i = 0; i < cat_count; i++) {
                            image += '<option value="' + r.subcategory[i]["category_id"] + '">' + r.subcategory[i]["category_name"] + '</option>'
                        }
                        $("#subcategory_id").html(image);
                    }
                }
            });
        }

        $(function () {
            $("#userfile").change(function () {
                var file = this.files[0];
                var imagefile = file.type;
                var match = ["image/jpeg", "image/png", "image/jpg"];
                if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
                {
                    $("#userfile").val("");
                    alert("Does not support file type");
                } else
                {
                    var reader = new FileReader();
                    reader.readAsDataURL(this.files[0]);
                    $("#change_img").html(file.name);
//                    alert("Image uploaded Successfully");
                }

            });
        });
        $(function () {
            $("#userfile1").change(function () {
                var file = this.files[0];
                var imagefile = file.type;
                var match = ["image/jpeg", "image/png", "image/jpg"];
                if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
                {
                    $("#userfile1").val("");
                    alert("Does not support file type");
                } else
                {
                    var reader = new FileReader();
                    reader.readAsDataURL(this.files[0]);
                    $("#change_img1").html(file.name);
                    alert("Image uploaded Successfully");
                }

            });
        });
    </script>




