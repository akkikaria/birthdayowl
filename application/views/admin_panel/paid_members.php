<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>zebra_datepicker.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>core.js"></script>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <script>
            $(document).ready(function () {
                $("#sales").addClass("active");
                $(".paid_members li").addClass("active");
            });
        </script>

        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <div class="page-content">
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
            </ul>
            <div class="page-title">                    
                <h2><span  class="fa fa-plus-circle"></span>PAID MEMBERS</h2>
            </div>
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-body">                                                                        
                                    <div class="form-group">
                                        <label class="col-md-1 ">Start Date</label>
                                        <div class="col-md-2 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="order_start" id="order_start" style="background: white;color: black;"/>
                                            </div>                                            
                                        </div>
                                        <label class="col-md-1 ">From Date</label>
                                        <div class="col-md-2 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="order_end" id="order_end" style="background: white;color: black;"/>
                                            </div>                                            
                                        </div>
                                        <div class="col-md-1">
                                            <button class="btn btn-primary pull-right"  id="submit_members">SUBMIT</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"></h3>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    
                            </div>
                            <div class="panel-body" >
                                <div class="table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>

                                                <th>ORDER ID</th>
                                                <th>PURCHASED ON</th>
                                                <th>BILL TO NAME</th>
                                                <th>ORDER AMOUNT</th>
                                                <th>MODE</th>
                                                <th>EXPIRY DATE</th>
                                                <th>MEMBERSHIP PLAN TYPE</th>
                                                <th>TRANSACTION STATUS</th>
                                                <th>PlAN STATUS</th>
                                                <th>SEND RENEWALE EMAIL</th>


                                            </tr>
                                        </thead>
                                        <tbody id="order_history">

                                            <?php foreach ($orders as $order) { ?>
                                                <tr>
                                                    <?php
                                                    if ($order["payment_mode"] == "CC") {
                                                        $mode = "Credit Card";
                                                    } else if ($order["payment_mode"] == "DC") {
                                                        $mode = "Debit Card";
                                                    } else {
                                                        $mode = "Net Banking";
                                                    }

                                                    if ($order["plan_purchase_type"] == "1") {
                                                        $plan = "Yearly";
                                                    } else {
                                                        $plan = "2 Years combined";
                                                    }

                                                    if ($order["membership_status"] == 1) {
                                                        $status = "active";
                                                    } else if ($order["membership_status"] == 2) {
                                                        $status = "Expired";
                                                    }
                                                    
                                                    ?>

                                                    <td><?php echo $order["membership_id"]; ?></td>
                                                    <td><?php echo $order["purchased_date"]; ?></td>
                                                    <td><?php echo ucfirst($order["bill_to_name"]); ?></td>
                                                    <td><?php echo $order["total_amount"]; ?> </td>
                                                    <td><?php echo $mode; ?></td>
                                                    <td><?php echo $order["expiry_date"]; ?></td>

                                                    <td><?php echo $plan; ?></td>

                                                    <td><?php echo $order["transaction_status"]; ?></td>
                                                    <td><?php echo $status; ?></td>


                                                    <td>

                                                        <?php if ($order["membership_status"] == 2 || ($order["days_left"] <= 1)) { ?>

                                                            <a style="cursor:pointer " class="sendrenemail" mstatus="<?php echo $order["membership_status"]; ?>" rname="<?php echo $order["bill_to_name"]; ?>" remail="<?php echo $order["email"]; ?>">Send Email</a>
                                                        <?php } ?>

                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>
            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(".sendrenemail").click(function () {
            var mstatus = $(this).attr("mstatus");

            var data = {
                email: $(this).attr("remail"),
                name: $(this).attr("rname")
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/send_membership_renewalEmail",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                    } else {
                        alert(r.message.toString());

                    }
                }
            });
        });

        $("#submit_members").click(function () {
            var start_date = $("#order_start").val();
            var end_date = $("#order_end").val();
            if (start_date == '') {
                alert("Enter From Date");
            } else if (end_date == '') {
                alert("Enter To Date ");
            } else {
                var data = {
                    order_start: start_date,
                    order_end: end_date
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/get_membership_data",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#order_history").html(r.message.toString());
                            return false;
                        } else {
                            var count = r.membership.length;
                            var data = '';
                            var mode = '';
                            var plan = '';

                            if (count > 0) {
                                for (i = 0; i < count; i++) {
                                    if (r.membership[i]["payment_mode"] == "CC") {
                                        mode = "Credit Card";
                                    } else if (r.membership[i]["payment_mode"] == "DC") {
                                        mode = "Debit Card";
                                    } else {
                                        mode = "Net Banking";
                                    }

                                    if (r.membership[i]["plan_purchase_type"] == "1") {
                                        plan = "Yearly";
                                    } else {
                                        plan = "2 Years combined";
                                    }

                                    data += '<tr>'
                                            + '<td>' + r.membership[i]['membership_id'] + '</td>'
                                            + '<td>' + r.membership[i]['purchased_date'] + '</td>'
                                            + '<td>' + r.membership[i]['bill_to_name'] + '</td>'
                                            + '<td>' + r.membership[i]['total_amount'] + '</td>'
                                            + '<td>' + mode + '</td>'
                                            + '<td>' + plan + '</td>'
                                            + '<td>' + r.membership[i]['transaction_status'] + '</td>'

                                            + '</tr>'

                                }
                            } else {
                                data = "No orders"
                            }
                            $("#order_history").html(data);

                        }
                    }
                });
            }
        });




        function getDate(d, m) {

            $(".errors7").hide();
//                                var parts = date.split('/');
//                                var month = parts[0];
//                                var bdate = parts[1];
            if ((d != '') && (m != '')) {
                var data = {
                    bmonth: m,
                    bday: d
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/get_zodiac",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#zodiac_id").val(r.message.toString());
                            return false;
                        } else {
                            $("#zodiac_id").val(r.message.toString());
                        }
                    }
                });
            }
        }

    </script>