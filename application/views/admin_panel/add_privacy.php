<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <script>
        $(document).ready(function () {
            $("#footermenu").addClass("active");
            $(".privacy li").addClass("active");
        });
    </script>
    <div class="page-container">

        <div class="page-sidebar">
            <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        </div>
        <div class="page-content">


            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">

                <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                </li> 
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 


            </ul>
            <div class="page-content-wrap">   
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3><strong>Privacy & Security Policy</strong></h3>
                                    <div class="panel-body" id="ids">                                                                        
                                        <div class="row" style="margin-left: -14px;">
                                            <div class="col-md-6 col-md-offset-1">

                                                <div class="block">
                                                    <textarea class="summernote">
                                                        <?php echo $privacy; ?>
                                                    </textarea>
                                                </div>
                                            </div>                   
                                        </div>
                                        <div class="  col-md-2 col-md-offset-1" >
                                            <button class="btn btn-primary pull-right" id="zodiac_submit" style="margin-right: 37px;" >Submit</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- END PAGE CONTENT WRAPPER -->                                

    </div>            
    <!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
            <div class="mb-content">
                <p>Are you sure you want to log out?</p>                    
                <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->
<!-- THIS PAGE PLUGINS -->        
<script type='text/javascript' src='<?php echo BASEURL_AMN_JS; ?>plugins/icheck/icheck.min.js'></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>        

<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/codemirror.js"></script>        
<script type='text/javascript' src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<script type='text/javascript' src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/mode/xml/xml.js"></script>
<script type='text/javascript' src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/mode/javascript/javascript.js"></script>
<script type='text/javascript' src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/mode/css/css.js"></script>
<script type='text/javascript' src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/mode/clike/clike.js"></script>
<script type='text/javascript' src="<?php echo BASEURL_AMN_JS; ?>plugins/codemirror/mode/php/php.js"></script>    

<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/summernote/summernote.js"></script>
<!-- END PAGE PLUGINS -->

<script type="text/javascript">
        $(document).ready(function () {

            $("#zodiac_submit").click(function () {
                var privacy = $(".note-editable").html();

                var data = {
                    privacy: privacy
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/update_privacy",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#message").html(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/add_privacy";
                        }
                    }
                });

            });
        });
</script>                 












