<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<style type="text/css">
    .show_overlay{background:rgba(0,0,0,0.6);display:none;width:100%;height:100%;top:0;right:0;position:fixed;z-index:11111}
</style>
<body>
    <script>
        $(document).ready(function () {
            $("#vouchers").addClass("active");
            $(".upload_vouchersgreeting li").addClass("active");
        });


    </script>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">

        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>

        <!-- PAGE CONTENT -->
        <div class="page-content">
            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                </li> 
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     

            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span class="fa fa-plus-circle"></span><?php echo $heading; ?></h2>;
            </div>
            <!-- END PAGE TITLE -->                
            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" name="card_submit" id="card_submit"  enctype="multipart/form-data" >
                            <div class="panel panel-default">
                                <div class="panel-body">  


                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Greeting Card Name</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="hidden" class="form-control" name="greeting_type" id='greeting_type'   value="2" />

                                                <input type="text" class="form-control" name="card_name" id="<?php echo $id; ?>"  value="<?php echo $card_data["card_name"]; ?>" card_id="<?php echo $card_data["card_id"]; ?>"/>
                                            </div>                                            
                                        </div>
                                        <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Meta Keywords</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="keywords" id="keywords" value="<?php echo $card_data["keywords"]; ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Meta Description</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="description" id="description" value="<?php echo $card_data["description"]; ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Image Upload</label>
                                        <div class="col-md-3 col-xs-12">    
                                            <?php if ($card_data["front_page_image"] != '') { ?>
                                                <div style="height: 20px; width: 238px; position: absolute; margin-left: 78px; margin-top: 153px; background-color: rgb(252, 248, 227);"><span id="change_img"><?php echo $card_data["front_page_image"]; ?></span></div>
                                            <?php } ?>
                                            <input type="hidden" id="userfile_old"name="userfile_old" value="<?php echo $card_data["front_page_image"]; ?>" />
                                            <?php
                                            $default_pic = "1.jpg";
                                            $img = BASEURL_GREETINGS . $card_data["front_page_image"];
                                            if ($card_data["front_page_image"] == FALSE) {//It will upload default image
                                                $img = BASEURL_GREETINGS . $default_pic;
                                            }
                                            ?>
                                            <image src="<?php echo $img ?>" height="150px" width= "200px" style="margin-left:0px;" id="preview"/>
                                            <input type="file" name="userfile" id="userfile" title="Browse file" />

                                        </div>
                                        <div class="error_r3" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="form-group" >
                                        <label class="col-md-3 col-xs-12 control-label">Image Alt Tag</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="alt_tag" id="alt_tag"  value="<?php echo $card_data["alt_tag"]; ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>

                                    <div class="form-group" >
                                        <label class="col-md-3 col-xs-12 control-label">Image Title Tag</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="title_tag" id="title_tag"  value="<?php echo $card_data["title_tag"]; ?>"/>
                                            </div>                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Card Message</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <textarea class="form-control" rows="2" name="message" id="message"><?php echo $card_data["message"]; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="btn btn-primary pull-right" id="<?php echo $btn_click; ?>">Submit</div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Manage Greeting Cards</h3>
                                <div style=""><button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="delete_greeting_card()" style="margin-left: 10px;float: right;"><span class="fa fa-times"> DELETE</span></button></div>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>CARD ID</th>
                                                <th>GREETING CARD NAME</th>
                                                <th>GREETING CARD IMAGE</th>
                                                <th>POSITION</th>
                                                <th>Edit</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php
                                            foreach ($greeting_card as $card) {
                                                ?>
                                                <tr>
                                                    <td><input type="checkbox" class="checkbox" value="<?php echo $card["card_id"]; ?>" /></td>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $card["card_name"]; ?></td>
                                                    <td><img src="<?php echo BASEURL_GREETINGS . $card["front_page_image"]; ?>" height="150px" width= "200px"></td>
                                                    <td><input type="text" style="width:30%" value="<?php echo $card["position"]; ?>" pid="<?php echo $card["card_id"]; ?>"  class="position_value" /></td>
                                                    <td> <button class="btn btn-default btn-rounded btn-condensed btn-sm edit_banner" card_id="<?php echo $card["card_id"]; ?>" onclick="edit_card('<?php echo $card["card_id"]; ?>')"><span class="fa fa-pencil"></span></button></td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="showloader" style="display:none">
        <div class="loader_div">
            <img src="<?php echo BASEURL_OIMG ?>89.gif" alt="Loading" class="ajax-loader " />
        </div>
    </div>
    <!-- END MESSAGE BOX-->

    <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->   
    <script type="text/javascript">
        $('#customers2').dataTable({
            "pageLength": 25
        });

        var timer2 = null;
        $('#card_name_submit').keyup(function () { //check card name while adding greeting card
            clearTimeout(timer2);
            timer2 = setTimeout(check_name, 1000);
        });
        var timer1 = null;
        $('#card_name_edit').keyup(function () { //check card name while editing greeting card
            clearTimeout(timer1);
            timer1 = setTimeout(check_name_edit, 1000);
        });

        $("#submit_greeting_card").click(function () {
            var greet_cat_id = $("#greet_cat_id").val();
            var greeting_type = $("#greeting_type").val();
            var card_name = $("#card_name_submit").val();
            var result1 = check_name();
            var userfile = $("#userfile").val();
            if (greet_cat_id == '0') {
                $(".error_r1").html("Select Greeting Category");
            }
            $('#greet_cat_id').change(function () {
                $(".error_r1").hide();
            });
            if (greeting_type == '0') {
                $(".error_r4").html("Select Greeting Type");
            }
            $('#greeting_type').change(function () {
                $(".error_r4").hide();
            });

            if (card_name == '') {
                $(".error_r2").html("Greeting Card Name is required.");
            }
            $('#card_name').keyup(function (e) {
                $(".error_r2").hide();
            });
            if (userfile == '') {
                $(".error_r3").html("Image upload is required.");
            } else {
                $(".error_r3").hide();
            }
            if ((card_name != '') && (greet_cat_id != '0') && (userfile != '') && (result1 == true)) {
                $(".error_r2").hide();
                $(".error_r2").hide();
                var formData = new FormData($("form[name='card_submit']")[0]);
                var url = "<?php echo BASEURL . $action; ?>";
                $.ajax({
                    url: url,
                    type: "POST",
                    data: formData,
                    async: true,
                    dataType: "json",
                    beforeSend: function () {
                        $(".showloader").addClass("show_overlay").show();


                    },
                    success: function (msg) {
                        $(".showloader").removeClass("show_overlay").hide();
                        if (msg.success.toString() == "true") {
                            alert(msg.message.toString());
                            window.location = "<?php echo BASEURL; ?>Greeting_card/voucher_greetings";
                        } else {
                            alert(msg.message.toString());
                        }
                    },
                    cache: false, contentType: false, processData: false});
            }

//            if (greet_cat_id == '0') {
//                $(".error_r1").html("Select Greeting Category");
//            } else if (card_name == '') {
//                $(".error_r2").html("Greeting Card Name is required.");
//            } else if (userfile == '') {
//                $(".error_r3").html("Image upload is required.");
//            } else if ((card_name != '') && (greet_cat_id != '') && (userfile != '')&&(result1 == true)) {
//                $("#card_submit").submit();
//
//            }
        });

        function edit_card(card_id) {
            var type = $("#greeting_type").val();
            window.location = "<?php echo BASEURL; ?>Greeting_card/edit_card/" + card_id + "/" + type;
        }
        $("#edit_greeting_card").click(function () {
            var greet_cat_id = $("#greet_cat_id").val();
            var card_name = $("#card_name_edit").val();
            var userfile_old = $("#userfile_old").val();

            if (greet_cat_id == '0') {
                $(".error_r1").html("Select Greeting Category");
            }
            $('#greet_cat_id').change(function () {
                $(".error_r1").hide();
            });
            if (card_name == '') {
                $(".error_r2").html("Greeting Card Name is required.");
            }
            $('#card_name').keyup(function (e) {
                $(".error_r2").hide();
            });
            if (userfile_old == '') {
                $(".error_r3").html("Image upload is required.");
            } else {
                $(".error_r3").hide();
            }
            if ((card_name != '') && (greet_cat_id != '0') && (userfile_old != '')) {
                $("#card_submit").submit();
                alert("Greeting Card Updated Successfully!");
            }
        });
//        function check_card_name() {
//            var card_name = $("#card_name").val();
//            var data = {
//                card_name: card_name
//            }
//            $.ajax({
//                type: "POST",
//                url: "<?php echo BASEURL; ?>Greeting_card/check_card_name",
//                data: data,
//                dataType: "json",
//                success: function (r) {
//                    if (r.success.toString() === "false") {
//
//                        alert(r.message.toString());
//                        $("#card_name").val('');
//
//                        return false;
//                    } else {
//
//                        return true;
//                    }
//                }
//            });
//        }
        function check_name() {
            var card_name = $("#card_name_submit").val();
            var result = 1;
            var data = {
                card_name: card_name
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Greeting_card/check_card_name",
                data: data,
                dataType: "json",
                async: false,
                success: function (r) {
                    //  console.log(r.success.toString());
                    if (r.success.toString() == "false") {
                        alert(r.message.toString());
                        $("#card_name_submit").val('');
                        result = 0;
                    }
                }
            });
            if (result) {
                return true;
            }
            return false;
        }
        function check_name_edit() {
            var card_name = $("#card_name_edit").val();
            var card_id = $("#card_name_edit").attr("card_id");

            var result = 1;
            var data = {
                card_name: card_name,
                card_id: card_id
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Greeting_card/Edit_check_card_name",
                data: data,
                dataType: "json",
                async: false,
                success: function (r) {
                    //  console.log(r.success.toString());
                    if (r.success.toString() == "false") {
                        alert(r.message.toString());
                        $("#card_name_edit").val('');
                        result = 0;
                    }
                }
            });
            if (result) {
                return true;
            }
            return false;
        }
        function delete_greeting_card() {
            var checkboxArray = new Array();
            var checkedValues = $('.checkbox:checked').map(function () {
                checkboxArray.push(this.value);
            });
            if (checkboxArray == '') {
                alert("Select at least one card");
            } else {
                var ask = window.confirm("Are you sure you want to delete?");
                if (ask) {
                    var data = {
                        card_id: checkboxArray
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo BASEURL; ?>Greeting_card/delete_greeting_card",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.success.toString() === "false") {
                                $("#message").html(r.message.toString());
                                return false;
                            } else {
                                alert(r.message.toString());
                                window.location = "<?php echo BASEURL; ?>Greeting_card/greeting";
                            }
                        }
                    });

                } else {
                    window.location = "<?php echo BASEURL; ?>Greeting_card/voucher_greetings";
                }
            }
        }
        $("#userfile").change(function () {
            //Get reference of FileUpload.
            var fileUpload = $("#userfile")[0];
            var file = this.files[0];
            //Check whether the file is valid Image.
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
            if (regex.test(fileUpload.value.toLowerCase())) {
                //Check whether HTML5 is supported.
                if (typeof (fileUpload.files) != "undefined") {
                    //Initiate the FileReader object.
                    var reader = new FileReader();
                    //Read the contents of Image File.
                    reader.readAsDataURL(fileUpload.files[0]);
                    //  
                    reader.onload = function (e) {
                        //Initiate the JavaScript Image object.
                        var image = new Image();
                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;
                        image.name = file.name;
                        image.onload = function () {
                            //Determine the Height and Width.
                            var height = this.height;
                            var width = this.width;
//                            if (height < 245 || width < 182) {
//                                $("#userfile").val("");
//                                alert("Height and Width  must be more than 182X245px.");
//                                return false;
//                            }
                            $("#change_img").html(image.name);
                            $("#preview").attr('src', e.target.result);
                            alert("Image Uploaded Successfully!");
                            return true;
                        };
                    }
                } else {
                    alert("This browser does not support HTML5.");
                    return false;
                }
            } else {
                alert("Please select a valid Image file.");
                return false;
            }
        });
//         $("#userfile_old").change(function () {
//            var file = this.files[0];
//            var imagefile = file.type;
//            var match = ["image/jpeg", "image/png", "image/jpg"];
//            if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
//            {
//                $("#userfile_old").val("");
//                alert("Does not support file type");
//            }
//            else
//            {
//                var reader = new FileReader();
//                reader.readAsDataURL(this.files[0]);
//                $(".error_r3").hide();
//                 $("#change_img").html(file.name);
//                alert("Image uploaded Successfully");
//            }
//
//        });
//        $("#userfile").change(function () {
//            var file = this.files[0];
//            var imagefile = file.type;
//            var match = ["image/jpeg", "image/png", "image/jpg"];
//            if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
//            {
//                $("#userfile").val("");
//                alert("Does not support file type");
//            }
//            else
//            {
//                var reader = new FileReader();
//                reader.readAsDataURL(this.files[0]);
//                $(".error_r3").hide();
//                 $("#change_img").html(file.name);
//                alert("Image uploaded Successfully");
//            }
//
//        });

        $('.position_value').on('input', function () {

            var position_val = $(this).val();
            if (position_val != "") {
                var data = {
                    position: position_val,
                    fb_id: $(this).attr("pid"),
                    type: 2
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/update_position",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#message").html(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Greeting_card/voucher_greetings";
                        }
                    }
                });
            }
        });
    </script>



