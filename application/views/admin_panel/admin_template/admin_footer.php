
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/jquery/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/bootstrap/bootstrap.min.js"></script>        
<!-- END PLUGINS -->

<!-- START THIS PAGE PLUGINS-->        
<script type='text/javascript' src="<?php echo BASEURL_AMN_JS; ?>plugins/icheck/icheck.min.js"></script>        
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/bootstrap/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/scrolltotop/scrolltopcontrol.js"></script>

<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/morris/raphael-min.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/morris/morris.min.js"></script>       
<!--<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/rickshaw/d3.v3.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/rickshaw/rickshaw.min.js"></script>-->
<script type='text/javascript' src='<?php echo BASEURL_AMN_JS; ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
<script type='text/javascript' src='<?php echo BASEURL_AMN_JS; ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
<script type='text/javascript' src='<?php echo BASEURL_AMN_JS; ?>plugins/bootstrap/bootstrap-datepicker.js'></script>                
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/owl/owl.carousel.min.js"></script>                 

<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/moment.min.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- END THIS PAGE PLUGINS-->        
<!-- START TEMPLATE -->
<!--<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>settings.js"></script>-->
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins.js"></script>        
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>actions.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>demo_dashboard.js"></script>
<!-- users.php  -->
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/tableExport.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/jquery.base64.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/html2canvas.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/jspdf/jspdf.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/jspdf/libs/base64.js"></script>  
<!--voucher product-->
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tagsinput/jquery.tagsinput.min.js"></script>

<!--image picker-->
<script type="text/javascript" src="<?php echo BASEURL_IMGPICKER; ?>image-picker.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_IMGPICKER; ?>image-picker.min.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>jquery.sumoselect.js"></script>

<!---Gallery--->
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/icheck/icheck.min.js"></script>

<!--Pagination-->

<script src="<?php echo BASEURL_AMN_JS; ?>jPages.js"></script>

<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/bootstrap/bootstrap-select.js"></script>
</head>
<!-- END TEMPLATE -->
<!-- END SCRIPTS -->         

<?php
header("Access-Control-Allow-Origin: *");
?>  






