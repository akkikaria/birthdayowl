
<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li>
            <div style="height:100px;background: white;"> <a href="<?php echo BASEURL; ?>Admin_panel/admin_dashboard"><img src="<?php echo BASEURL_OIMG; ?>logo.png"  style=" width: 82%; margin-top: 17px; margin-left: 10px;"></a></div>

        </li>
        <li class="xn-title"></li>


        <li class="xn-openable " id="reminderopen">
            <a href="#"><span class="glyphicon glyphicon-list"></span><span class="xn-text">Birthday Reminder</span></a>
            <ul class="rusers" >
                <li > <a href="<?php echo BASEURL; ?>Admin_panel/users"><span class="glyphicon glyphicon-plus-sign"></span>Users</a></li>
            </ul>
            <ul class="rgusers" >
                <li > <a href="<?php echo BASEURL; ?>Admin_panel/guest_users"><span class="glyphicon glyphicon-plus-sign"></span>Guest Users</a></li>
            </ul>

            <ul class="news">
                <li>  <a href="<?php echo BASEURL; ?>Admin_panel/newsletter"><span class="glyphicon glyphicon-plus-sign"></span>Newsletter</a></li>
            </ul>

            <ul class="marquee">
                <li>  <a href="<?php echo BASEURL; ?>Admin_panel/marquee"><span class="glyphicon glyphicon-plus-sign"></span>Marquee</a></li>
            </ul>
            <ul class="signdata">
                <li>   <a href="<?php echo BASEURL; ?>Admin_panel/add_sign_data"><span class="glyphicon glyphicon-plus-sign"></span> Add Sign Data</a></li>
            </ul>
            <ul class="relation">
                <li> <a href="<?php echo BASEURL; ?>Admin_panel/relationship"><span class="glyphicon glyphicon-plus-sign"></span> Add Relationship</a></li>
            </ul>


        </li>

        <!--START  GREETING CARDS-->
        <li class="xn-openable" id="freegreetings">
            <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Free-Greetings</span></a>

            <ul class="uploadfreegreetings">
                <li><a href="<?php echo BASEURL; ?>Greeting_card/greeting/1"><span class="glyphicon glyphicon-plus-sign"></span>Upload Greetings</a></li>
            </ul>
            <ul class="sent_greetings">
                <li><a href="<?php echo BASEURL; ?>Greeting_card/sent_greetings"><span class="glyphicon glyphicon-plus-sign"></span>Sent Greetings</a></li>
            </ul>
            <ul class="thankyou">
                <li><a href="<?php echo BASEURL; ?>Greeting_card/greeting/3"><span class="glyphicon glyphicon-plus-sign"></span>ThankYou Cards</a></li>
            </ul>
        </li>
        <!--END  GREETING CARDS-->
        <!--START  Product Voucher-->
        <li class="xn-openable" id="vouchers">
            <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Product Voucher</span></a>
          
            <ul class="sync_woohoo" style="display: none">
                <li><a href="<?php echo BASEURL; ?>Product_voucher/sync_woohoo"><span class="glyphicon glyphicon-plus-sign"></span>Sync vouchers</a></li>
            </ul>
            <ul class="upload_vouchersgreeting">
                <li><a href="<?php echo BASEURL; ?>Greeting_card/greeting/2"><span class="glyphicon glyphicon-plus-sign"></span>Upload Greetings</a></li>
            </ul>
            <ul class="deno_disc" style="display: none">
                <li><a href="<?php echo BASEURL; ?>Product_voucher/denomination"><span class="glyphicon glyphicon-plus-sign"></span>Custom Denomination</a></li>
            </ul>
            <ul class="discount" style="display: none">
                <li><a href="<?php echo BASEURL; ?>Product_voucher/discount"><span class="glyphicon glyphicon-plus-sign"></span>Discount</a></li>
            </ul>

        </li>
        <!--END  Product Voucher-->

        <!-- Banners-->
        <li class="xn-openable" id="banners">
            <a href="#"><span class="glyphicon glyphicon-list"></span>Banners</a>
            <ul class="add_banners">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/banner"><span class="glyphicon glyphicon-plus-sign"></span>Add Banners</a></li>
            </ul>
            <ul class="homepage">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/change_homepage"><span class="glyphicon glyphicon-plus-sign"></span>Change Home Screen Banners</a></li>
            </ul>
        </li>
        <!-- End Banners-->
        <!-- sales-->
        <li class="xn-openable" id="sales">
            <a href="#"><span class="glyphicon glyphicon-list"></span>Orders</a>
            <ul class="orders">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/orders"><span class="glyphicon glyphicon-plus-sign"></span>Orders</a></li>
            </ul>
            <ul class="wishList">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/mycartorders"><span class="glyphicon glyphicon-plus-sign"></span>Wish List Orders</a></li>
            </ul>
            <ul class="refund">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/show_refunded_orders"><span class="glyphicon glyphicon-plus-sign"></span>Refunded Orders</a></li>
            </ul>
            <ul class="paid_members">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/paid_members"><span class="glyphicon glyphicon-plus-sign"></span>Paid Members</a></li>
            </ul>
        </li>




        <!-- End sales-->
        <li class="xn-openable" id="membership">
            <a href="#"><span class="glyphicon glyphicon-list"></span>Membership</a>
            <ul class="add_membership">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/membership_plan"><span class="glyphicon glyphicon-plus-sign"></span>Add Plan</a></li>
            </ul>
        </li>
        <li class="xn-openable" id="footermenu">
            <a href="#"><span class="glyphicon glyphicon-list"></span>Footer</a>
            <ul class="aboutus">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/add_aboutus"><span class="glyphicon glyphicon-plus-sign"></span>About us</a></li>
            </ul>
            <ul class="privacy">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/add_privacy"><span class="glyphicon glyphicon-plus-sign"></span>Privacy Policy</a></li>
            </ul>
            <ul class="terms">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/add_terms"><span class="glyphicon glyphicon-plus-sign"></span>Terms & conditions</a></li>
            </ul>
            <ul class="faq">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/add_faq"><span class="glyphicon glyphicon-plus-sign"></span>FAQ</a></li>
            </ul>
            <ul class="contactus">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/contactUs"><span class="glyphicon glyphicon-plus-sign"></span>Contact us</a></li>
            </ul>
            <ul class="testimonial">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/testimonial"><span class="glyphicon glyphicon-plus-sign"></span>Testimonial</a></li>
            </ul>
        </li>
        <li class="xn-openable" id="seo">
            <a href="#"><span class="glyphicon glyphicon-list"></span>SEO</a>
            <ul class="custom">
                <li><a href="<?php echo BASEURL; ?>Admin_panel/seo"><span class="glyphicon glyphicon-plus-sign"></span>customization</a></li>
            </ul>
        </li>

    </ul>
</div>