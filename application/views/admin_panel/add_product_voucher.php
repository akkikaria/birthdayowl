<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>zebra_datepicker.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>core.js"></script>
<style>
    ul li{
        list-style: none;
    }
</style>
<script>
    $(document).ready(function () {
        $("#vouchers").addClass("active");
        $(".add_vouchers li").addClass("active");
    });
</script>
<body>
    <div class="page-container">
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <div class="page-content">
            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                </li> 
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     

            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span  class="fa fa-plus-circle"></span><?php echo $heading; ?></h2>
            </div>
            <!-- END PAGE TITLE -->                

            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" id="voucher_submit" action="<?php echo BASEURL . $action; ?>" enctype="multipart/form-data" >
                            <div class="panel panel-default">
                                <div class="panel-body"> 
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Voucher Category</label>
                                            <div class="col-md-6 col-xs-12">    
                                                <select id="voucher_cat_id" name="voucher_cat_id" class="form-control">
                                                    <option selected="selected" value="0" >Select Voucher Category</option>
                                                    <?php foreach ($voucher_category as $vc) { ?>
                                                        <option value="<?php echo $vc["voucher_cat_id"]; ?>"><?php echo $vc["voucher_cat_name"]; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                            <div class="error_r1" style="color: red;margin-left:245px;"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Product Name</label>
                                            <div class="col-md-6 col-xs-12">                                        
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>

                                                    <input type="text" class="form-control" name="voucher_pro_name" id="<?php echo $id; ?>" voucher_pro_id="<?php echo $voucher_data["voucher_pro_id"]; ?>" value="<?php echo $voucher_data["voucher_pro_name"]; ?>"/>
                                                </div> 
                                            </div>
                                            <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Product Tag Line</label>
                                            <div class="col-md-6 col-xs-12">                                        
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="pro_tagline" id="pro_tagline" value="<?php echo $voucher_data["pro_tagline"]; ?>"/>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Meta keywords</label>
                                            <div class="col-md-6 col-xs-12">                                        
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="keywords" id="keywords" value="<?php echo $voucher_data["keywords"]; ?>"/>
                                                </div> 
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Meta Description</label>
                                            <div class="col-md-6 col-xs-12">                                            
                                                <textarea class="form-control" rows="2" name="description" id="description"><?php echo $voucher_data["description"]; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Image Upload</label>
                                            <div class="col-md-6 col-xs-12">    
                                                <?php if ($voucher_data["product_image"] != '') { ?>
                                                    <div style="height: 20px; width: 174px; position: absolute; margin-left: 78px; margin-top:149px; background-color: rgb(252, 248, 227);"><span id="change_img"><?php echo $voucher_data["product_image"]; ?></span></div>
                                                <?php } ?>
                                                <input type="hidden" id="userfile_old"name="userfile_old" value="<?php echo $voucher_data["product_image"]; ?>"  />
                                                <?php
                                                $default_pic = "dashboard.png";
                                                $img = BASEURL_VPRO . $voucher_data["product_image"];
                                                if ($voucher_data["product_image"] == FALSE) {//It will upload default image
                                                    $img = BASEURL_VPRO . $default_pic;
                                                }
                                                ?>
                                                <image src="<?php echo $img ?>" height="148px" width= "274px" style="margin-left:0px;" id="preview"/>
                                                <input type="file" name="userfile" id="userfile" title="Browse file" />

                                            </div>
                                            <div class="error_r3" style="color: red;margin-left:245px;"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Image Alt Tag</label>
                                            <div class="col-md-6 col-xs-12">                                        
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="alt_tag" id="alt_tag" value="<?php echo $voucher_data["alt_tag"]; ?>"/>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Image Title Tag</label>
                                            <div class="col-md-6 col-xs-12">                                        
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="title_tag" id="title_tag" value="<?php echo $voucher_data["title_tag"]; ?>"/>
                                                </div> 
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Terms & Conditions</label>
                                            <div class="col-md-6 col-xs-12">                                            
                                                <textarea  rows="2" name="terms_conditions" class="form-control summernote" id="terms_conditions"><?php echo $voucher_data["terms_conditions"]; ?></textarea>
                                            </div>
                                            <div class="error_r4" style="color: red;margin-left:245px;"></div><br/>

                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-10 col-xs-12 col-md-offset-3">   
                                                <span><b>Note:Use bullet points(•) for adding Terms & Conditions.</b></span>
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Amount</label>
                                            <ul class="col-md-8 col-xs-12" id="items" > 
                                                <?php if ($checkview == "1") { ?>
                                                    <li class="col-md-3 col-xs-12" style="margin-top:5px;">                                        
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                            <input class="amount2 form-control" type="text"  placeholder="AMOUNT" name="amount[]" id="amount1"  value=""onkeypress="return isNumberKey(event)" />
                                                        </div> 
                                                        <div id="amount1_error_msg" style=" color: red; width: 64%; height: 16px;"></div>
                                                    </li>

                                                    <li class="col-md-3 col-xs-12" style="margin-top:5px;">
                                                        <div class="input-group">
                                                            <span class="input-group-addon "><span class="fa fa-pencil"></span></span>
                                                            <input type="text"   placeholder="PIN" name="pin[]" id="pin1" value="" class="form-control pin_error"/>
                                                        </div>
                                                        <div id="pin1_error_msg" style=" color: red; width: 64%; height: 16px;"></div>
                                                    </li>


                                                    <li class="col-md-3 col-xs-12" style="margin-top:5px;">
                                                        <div class="input-group">
                                                            <span class="input-group-addon "><span class="fa fa-pencil"></span></span>
                                                            <input type="text"   placeholder="CODE" name="code[]" id="code1" value="" class="form-control code_error"/>
                                                        </div>
                                                        <div id="code1_error_msg" style=" color: red; width: 64%; height: 16px;"></div>
                                                    </li>
                                                    <li class="col-md-3 col-xs-12" style="margin-top:5px;">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                            <input type="text"   placeholder="EXPIRY DATE" name="expiry[]" id="expiry1"    class="form-control expiry_error"/>
                                                        </div>
                                                        <div id="expiry1_error_msg" style=" color: red; width: 64%; height: 16px;"></div>
                                                    </li>
                                                    <?php
                                                } else {
                                                    $i = 1;
                                                    foreach ($voucher_amount as $vamount) {
                                                        ?>
                                                        <li class="col-md-3 col-xs-12" style="margin-top:5px;">                                        
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                                <input class="amount2 form-control" type="text"  placeholder="AMOUNT" name="amount[]" id="amount<?php echo $i; ?>"  value="<?php echo $vamount["pamount"] ?>"onkeypress="return isNumberKey(event)" />
                                                            </div> 
                                                            <div id="amount1_error_msg" style=" color: red; width: 64%; height: 16px;"></div>
                                                        </li>

                                                        <li class="col-md-3 col-xs-12" style="margin-top:5px;">
                                                            <div class="input-group">
                                                                <span class="input-group-addon "><span class="fa fa-pencil"></span></span>
                                                                <input type="text"   placeholder="PIN" name="pin[]" id="pin<?php echo $i; ?>" value="<?php echo $vamount["pin"] ?>" class="form-control pin_error"/>
                                                            </div>
                                                            <div id="pin1_error_msg" style=" color: red; width: 64%; height: 16px;"></div>
                                                        </li>

                                                        <li class="col-md-3 col-xs-12" style="margin-top:5px;">
                                                            <div class="input-group">
                                                                <span class="input-group-addon "><span class="fa fa-pencil"></span></span>
                                                                <input type="text"   placeholder="CODE" name="code[]" id="code<?php echo $i; ?>" value="<?php echo $vamount["code"] ?>" class="form-control code_error"/>
                                                            </div>
                                                            <div id="code1_error_msg" style=" color: red; width: 64%; height: 16px;"></div>
                                                        </li>

                                                        <li class="col-md-3 col-xs-12" style="margin-top:5px;">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                                <input type="text" value="<?php echo $vamount["expiry_date"] ?>"  placeholder="EXPIRY DATE" name="expiry[]" id="expiry<?php echo $i; ?>"    class="form-control expiry_error"/>
                                                            </div>
                                                            <div id="expiry1_error_msg" style=" color: red; width: 64%; height: 16px;"></div>
                                                        </li>

                                                        <?php
                                                        $i++;
                                                    }
                                                }
                                                ?>

                                            </ul>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label"></label>
                                            <div class="col-md-6 col-xs-12">  
                                                <div class="col-md-6 col-xs-12">                                        
                                                    <div  id="add_more"  style="background: #59aa01 none repeat scroll 0 0;  border-radius: 10px;    color: #fff;   display: block; cursor: pointer;  margin-top: 10px;  padding: 12px;    text-align: center;  width: 100%;">+ Add Amount</div>
                                                </div>
                                                <div class="col-md-6 col-xs-12">
                                                    <div id="remove_users"style="cursor:pointer;background:#e6991f none repeat scroll 0 0;  border-radius: 10px;    color: #fff;   display: block;   margin-top: 10px;  padding: 12px;    text-align: center;  width: 100%;"  class="add_recipient">- Remove Amount</div>
                                                </div>
                                            </div>
                                        </div>
                                        <script>
                                            $(document).ready(function () {

                                                var acount = "<?php echo count($voucher_amount); ?>";
                                                for (i = 1; i <= acount; i++) {
                                                    $('#expiry' + i).Zebra_DatePicker({
                                                        default_position: 'below'


                                                    });
                                                }
                                            });

                                            var counter = 4;
                                            $("#add_more").click(function () {

                                                var data = '';
                                                data += '<li class="col-md-3 col-xs-12" style="margin-top:5px;"><div class="input-group"> <span class="input-group-addon"><span class="fa fa-pencil"></span></span> <input class="amount2 form-control" type="text"  placeholder="AMOUNT" name="amount[]" id="amount' + counter + '"  value="" onkeypress="return isNumberKey(event)"/> </div>  <div id="amount' + counter + '_error_msg" style=" color: red; width: 64%; height: 16px;"></div>  </li>'
                                                        + '<li class="col-md-3 col-xs-12" style="margin-top:5px;">  <div class="input-group">   <span class="input-group-addon"><span class="fa fa-pencil"></span></span> <input type="text"   placeholder="PIN" name="pin[]" id="pin' + counter + '" value="" class="form-control pin_error"/>   </div>  <div id="pin' + counter + '_error_msg" style=" color: red; width: 64%; height: 16px;"></div>    </li>'
                                                        + '<li class="col-md-3 col-xs-12" style="margin-top:5px;">  <div class="input-group">   <span class="input-group-addon"><span class="fa fa-pencil"></span></span> <input type="text"   placeholder="CODE" name="code[]" id="code' + counter + '" value="" class="form-control code_error"/>   </div>  <div id="code' + counter + '_error_msg" style=" color: red; width: 64%; height: 16px;"></div>    </li>'
                                                        + '<li class="col-md-3 col-xs-12" style="margin-top:5px;">  <div class="input-group">   <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span> <input type="text"   placeholder="EXPIRY DATE" name="expiry[]" id="expiry' + counter + '" value="" class="form-control expiry_error pdatepicker"/>     </div><div id="expiry' + counter + '_error_msg" style=" color: red; width: 64%; height: 16px;"></div> </li>'
                                                $("#items").append(data);
                                                $('#expiry' + counter).Zebra_DatePicker({
                                                    default_position: 'below'


                                                });
                                                counter++;
                                            });
                                            $("#remove_users").click(function () {
                                                if ($('#items li').length > 4) {
                                                    counter--;
                                                    $('#items li:nth-last-child(4)').remove();
                                                    $('#items li:nth-last-child(3)').remove();
                                                    $('#items li:nth-last-child(2)').remove();
                                                    $('#items li:nth-last-child(1)').remove();
                                                }
                                            });
                                        </script>

                                        <div class="col-md-6">
                                            <div class="btn btn-primary pull-right"  id="<?php echo $btn_click; ?>">Submit</div>
                                        </div>

                                    </div>

                                </div>
                        </form>
                    </div>
                </div>



            </div>         

        </div>            

    </div>

    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/summernote/summernote.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_BJS; ?>tiny/tinymce.min.js"></script>
<script type="text/javascript">
//                                            tinyMCE.init({
//                                                mode: "textareas",
//                                                editor_selector: "mceEditor",
//                                                // Style formats
//
//                                                style_formats: [
//                                                    {title: 'Bold text', inline: 'b'},
//                                                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
//                                                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
//                                                    {title: 'Example 1', inline: 'span', classes: 'example1'},
//                                                    {title: 'Example 2', inline: 'span', classes: 'example2'},
//                                                    {title: 'Table styles'},
//                                                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
//                                                ],
//                                            });
                                        $("#voucher_cat_id").change(function () {
                                            var voucher_cat_id = $("#voucher_cat_id").val();
                                            if (voucher_cat_id != "0") {
                                                $(".error_r1").html("");
                                            } else {
                                                $(".error_r1").html("Select Voucher Category");
                                            }
                                        });
                                        $(document).ready(function () {
                                            var voucher_cat_id = "<?php echo $voucher_data["voucher_cat_id"]; ?>";

                                            $("#voucher_cat_id").val(voucher_cat_id);
                                        });
                                        var timer2 = null;
                                        $('#voucher_pro_name_submit').keydown(function () {
                                            clearTimeout(timer2);
                                            timer2 = setTimeout(check_voucher_product_name, 1000);
                                        });
                                        var timer1 = null;
                                        $('#voucher_pro_name_edit').keydown(function () {
                                            clearTimeout(timer1);
                                            timer1 = setTimeout(edit_check_voucher_product_name, 1000);
                                        });
                                        $(document).on("keyup", ".note-editable", function () {

                                        });



                                        $(document).on("keyup", "#voucher_pro_name_submit", function () {
                                            $(".error_r2").html("");
                                        });

                                        $(document).on("keyup", ".amount2", function () {
                                            var input = $(this).attr('id');
                                            $("#" + input + "_error_msg").html("");
                                        });
                                        $(document).on("keyup", ".pin_error", function () {
                                            var input = $(this).attr('id');
                                            $("#" + input + "_error_msg").html("");
                                        });
                                        $(document).on("keyup", ".code_error", function () {
                                            var input = $(this).attr('id');
                                            $("#" + input + "_error_msg").html("");
                                        });

                                        $(document).on("keyup", ".expiry_error", function () {
                                            var input = $(this).attr('id');
                                            $("#" + input + "_error_msg").html("");
                                        });
                                        $("#edit_voucher_product").click(function () {

                                            var voucher_pro_name = $("#voucher_pro_name_edit").val();
                                            var result1 = edit_check_voucher_product_name();
                                            var userfile_old = $("#userfile_old").val();

                                            var card_id = $("#card_id").val();
                                            var voucher_cat_id = $("#voucher_cat_id").val();
                                            // var redemption_details = tinymce.get('redemption_details').getContent();
                                            // var locations = tinymce.get('locations').getContent();
                                            var terms_conditions = $(".note-editable").html();

                                            if (voucher_cat_id == '0') {
                                                $(".error_r1").html("Select Voucher Category");
                                            }
                                            if (voucher_pro_name == '') {
                                                $(".error_r2").html("Product Voucher Name is required");
                                            }

                                            if (userfile_old == '') {
                                                $(".error_r3").html("Image upload is required.");
                                            }
                                            if (terms_conditions == '') {
                                                $(".error_r4").html("Enter terms_conditions");

                                            }

                                            var count = counter - 1;

                                            var message1 = '';
                                            var message2 = '';
                                            var message3 = '';
                                            var message4 = '';
                                            var success = false;

                                            var noamt = 0, nopin = 0, noexpiry = 0, nocode = 0;
                                            for (i = 1; i <= count; i++) {
                                                var amount = $("#amount" + i).val();
//                                                    var pin = $("#pin" + i).val();
                                                var expiry = $("#expiry" + i).val();
                                                var code = $("#code" + i).val();


                                                message1 = "Enter Amount ";
//                                                    message2 = "Enter PIN ";
                                                message3 = "Enter Expiry date ";
                                                message4 = "Enter code";

                                                if (amount == '') {
                                                    noamt = 1;
                                                    $("#amount" + i + "_error_msg").html(message1);
                                                }
//                                                    if (pin == "") {
//                                                        nopin = 1;
//                                                        $("#pin" + i + "_error_msg").html(message2);
//                                                    }
                                                if (expiry == "") {
                                                    noexpiry = 1;
                                                    $("#expiry" + i + "_error_msg").html(message3);
                                                }
                                                if (code == "") {
                                                    nocode = 1;
                                                    $("#code" + i + "_error_msg").html(message4);
                                                }

                                            }

                                            if ((voucher_pro_name != '') && ((voucher_cat_id != '0')) && (userfile_old != '') && (result1 == true) && noamt == 0 && noexpiry == 0 && nocode == 0) {
                                                alert("Product Voucher Product Updated Successfully!.");
                                                $("#voucher_submit").submit();
                                            }

                                        });

                                        $("#submit_product_voucher").click(function () {
                                            var voucher_pro_name = $("#voucher_pro_name_submit").val();
                                            var result1 = check_voucher_product_name();
                                            var voucher_cat_id = $("#voucher_cat_id").val();
                                            var userfile = $("#userfile").val();
                                            //  var terms_conditions = tinymce.get('terms_conditions').getContent();
                                            var terms_conditions = $(".note-editable").html();



                                            if (voucher_cat_id == '0') {
                                                $(".error_r1").html("Select Voucher Category");
                                            }

                                            if (voucher_pro_name == '') {
                                                $(".error_r2").html("Product Voucher Name is required");
                                            }

                                            if (userfile == '') {
                                                $(".error_r3").html("Image upload is required.");
                                            }
                                            if (terms_conditions == "") {
                                                alert("dsfd");
                                                $(".error_r4").html("Enter terms_conditions");

                                            }
                                            var count = counter - 1;
                                            var message1 = '';
                                            var message2 = '';
                                            var message3 = '';
                                            var message4 = '';
                                            var success = false;
                                            var noamt = 0, nopin = 0, noexpiry = 0, nocode = 0;
                                            ;
                                            for (i = 1; i <= count; i++) {
                                                var amount = $("#amount" + i).val();
                                                //var pin = $("#pin" + i).val();
                                                var expiry = $("#expiry" + i).val();
                                                var code = $("#code" + i).val();
                                                message1 = "Enter Amount ";
                                                // message2 = "Enter PIN ";
                                                message3 = "Enter Expiry date ";
                                                message4 = "Enter code";
                                                if (amount == '') {
                                                    noamt = 1;
                                                    $("#amount" + i + "_error_msg").html(message1);
                                                }
//                                                    if (pin == "") {
//                                                        nopin = 1;
//                                                        $("#pin" + i + "_error_msg").html(message2);
//                                                    }
                                                if (expiry == "") {
                                                    noexpiry = 1;
                                                    $("#expiry" + i + "_error_msg").html(message3);
                                                }
                                                if (code == "") {
                                                    nocode = 1;
                                                    $("#code" + i + "_error_msg").html(message4);
                                                }

                                            }



                                            if ((voucher_pro_name != '') && ((voucher_cat_id != '0')) && (userfile != '') && (result1 == true) && noamt == 0 && noexpiry == 0 && nocode == 0) {
                                                alert("Product Voucher Product added Successfully!.");
                                                $("#voucher_submit").submit();
                                            }
                                        });

                                        function check_voucher_product_name() {
                                            var voucher_pro_name = $("#voucher_pro_name_submit").val();
                                            var result = 1;
                                            var data = {
                                                voucher_pro_name: voucher_pro_name
                                            }
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo BASEURL; ?>Product_voucher/check_voucher_product_name",
                                                data: data,
                                                dataType: "json",
                                                async: false,
                                                success: function (r) {
                                                    if (r.success.toString() == "false") {
                                                        alert(r.message.toString());
                                                        $("#voucher_pro_name_submit").val('');
                                                        result = 0;
                                                    }
                                                }
                                            });
                                            if (result) {
                                                return true;
                                            }
                                            return false;
                                        }
                                        function edit_check_voucher_product_name() {
                                            var voucher_pro_name = $("#voucher_pro_name_edit").val();
                                            var voucher_pro_id = $("#voucher_pro_name_edit").attr("voucher_pro_id");
                                            var result = 1;
                                            var data = {
                                                voucher_pro_name: voucher_pro_name,
                                                voucher_pro_id: voucher_pro_id
                                            }
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo BASEURL; ?>Product_voucher/Edit_check_voucher_product_name",
                                                data: data,
                                                dataType: "json",
                                                async: false,
                                                success: function (r) {
                                                    if (r.success.toString() == "false") {
                                                        alert(r.message.toString());
                                                        $("#voucher_pro_name_edit").val('');
                                                        result = 0;
                                                    }
                                                }
                                            });
                                            if (result) {
                                                return true;
                                            }
                                            return false;
                                        }

                                        function edit_voucher_pro(voucher_pro_id) {
                                            window.location = "<?php echo BASEURL; ?>Product_voucher/edit_voucher_product/" + voucher_pro_id;
                                        }
                                        $("#userfile").change(function () {
                                            var fileUpload = $("#userfile")[0];
                                            var file = this.files[0];
                                            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
                                            if (regex.test(fileUpload.value.toLowerCase())) {
                                                if (typeof (fileUpload.files) != "undefined") {
                                                    var reader = new FileReader();
                                                    reader.readAsDataURL(fileUpload.files[0]);
                                                    reader.onload = function (e) {
                                                        var image = new Image();
                                                        image.src = e.target.result;
                                                        image.name = file.name;
                                                        image.onload = function () {
                                                            var height = this.height;
                                                            var width = this.width;
                                                            $(".error_r3").hide();
                                                            $("#change_img").html(image.name);
                                                            $("#preview").attr('src', e.target.result);
                                                            alert("Image Uploaded Successfully.");
                                                            return true;
                                                        };
                                                    }
                                                } else {
                                                    alert("This browser does not support HTML5.");
                                                    return false;
                                                }
                                            } else {
                                                alert("Please select a valid Image file.");
                                                return false;
                                            }
                                        });
                                        function getDate(d, m) {

                                            $(".errors7").hide();
//                                var parts = date.split('/');
//                                var month = parts[0];
//                                var bdate = parts[1];
                                            if ((d != '') && (m != '')) {
                                                var data = {
                                                    bmonth: m,
                                                    bday: d
                                                }
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo BASEURL; ?>Admin_panel/get_zodiac",
                                                    data: data,
                                                    dataType: "json",
                                                    success: function (r) {
                                                        if (r.success.toString() === "false") {
                                                            $("#zodiac_id").val(r.message.toString());
                                                            return false;
                                                        } else {
                                                            $("#zodiac_id").val(r.message.toString());
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                        function isNumberKey(evt)
                                        {
                                            evt = (evt) ? evt : window.event;
                                            var charCode = (evt.which) ? evt.which : evt.keyCode;
                                            if (charCode > 31 && (charCode < 48 || charCode > 57))
                                                return false;
                                            return true;
                                        }
</script>