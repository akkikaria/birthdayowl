<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <script>
        $(document).ready(function () {
            $("#seo").addClass("active");
            $(".custom li").addClass("active");
        });


    </script>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">

        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>

        <!-- PAGE CONTENT -->
        <div class="page-content">
            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                </li> 
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     
            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span class="fa fa-arrow-circle-o-left"></span>SEO</h2>
            </div>
            <!-- END PAGE TITLE -->                
            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-body">                                                                        
                                    <div class="form-group">
                                        <label class="col-md-1 ">Custom Menu</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <select id="menu_id" name="menu_id" class="form-control">
                                                <option selected="selected" value="0" >Select</option>
                                                <?php foreach ($menus as $menu_list) { ?>
                                                    <option value="<?php echo $menu_list["id"]; ?>"  ><?php echo $menu_list["menu_name"]; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="help-block"></span>                                           
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-1 ">SEO TITLE</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <!--<div class="input-group">-->
                                                <!--<span class="input-group-addon"><span class="fa fa-pencil"></span></span>-->
                                            <textarea class="form-control" rows="2" name="stitle" id="stitle"></textarea>
                                            <!--</div>-->                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-1 ">SEO DESCRIPTION </label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <textarea class="form-control" rows="3" name="description" id="description"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-1 ">SEO KEYWORDS</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <!--<div class="input-group">-->
                                                <!--<span class="input-group-addon"><span class="fa fa-pencil"></span></span>-->
                                            <textarea class="form-control" rows="3" name="keywords" id="keywords"></textarea>
                                            <!--</div>-->                                            
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-primary pull-right"  id="done">DONE</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->


    <script>
        $("#done").click(function () {

            var title = $("#stitle").val();
            if (title == "") {
                alert("title is required");
            } else {
                var data = {
                    title: title,
                    description: $("#description").val(),
                    keywords: $("#keywords").val(),
                    menu_id: $("#menu_id").val()

                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/add_seo",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#message").html(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/seo";
                        }
                    }
                });
            }

        });
        $("#menu_id").change(function () {
            var menu_id = $(this).val();
            change_menu(menu_id);
        });
        function change_menu(menu_id) {
            var data = {
                menu_id: menu_id
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/get_seo_data",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        return false;
                    } else {
                        var stitle = r.seo_data['title'];
                        var sdecription = r.seo_data['description'];
                        var skeywords = r.seo_data['keywords'];

                        $('#stitle').val(stitle);
                        $('#description').val(sdecription);
                        $('#keywords').val(skeywords);


                    }
                }
            });
        }

    </script>






