<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <!-- START PAGE SIDEBAR -->
        <div class="page-sidebar">
            <!-- START X-NAVIGATION -->
            <ul class="x-navigation">
                <li>
                    <div style="height:100px;background: white;"> <a href="<?php echo BASEURL; ?>Admin_panel/admin_dashboard"><img src="<?php echo BASEURL_OIMG; ?>logo.png"  style=" width: 82%; margin-left: 15px; margin-top: 5px;"></a></div>
                </li>
                <li class="xn-title"></li>

                <li class="xn-openable">
                    <a href="<?php echo BASEURL; ?>Admin_panel/admin_dashboard"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Birthday Reminder</span></a>
                    <ul>
                        <li> <a href="<?php echo BASEURL; ?>Admin_panel/users"><span class="glyphicon glyphicon-plus-sign"></span>Users</a></li>
                    </ul>
                    <ul>
                        <li>  <a href="<?php echo BASEURL; ?>Admin_panel/company"><span class="glyphicon glyphicon-plus-sign"></span>Company Register</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/newsletter"><span class="glyphicon glyphicon-plus-sign"></span>Newsletter</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/friends"><span class="glyphicon glyphicon-plus-sign"></span>Refer Friend List</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/marquee"><span class="glyphicon glyphicon-plus-sign"></span>Marquee</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/add_sign_data"><span class="glyphicon glyphicon-plus-sign"></span> Add Sign Data</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/relationship"><span class="glyphicon glyphicon-plus-sign"></span> Add Relationship</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/occupation"><span class="glyphicon glyphicon-plus-sign"></span> Add Occupation</a></li>
                    </ul>

                </li>
                <li class="xn-openable active">
                    <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Menu</span></a>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/banner"><span class="glyphicon glyphicon-plus-sign"></span> Add Banner</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/menu"><span class="glyphicon glyphicon-plus-sign"></span> Add Main Menu</a></li>
                    </ul>
                    <ul>
                        <li class="xn-openable">
                            <a href="#"><span class="glyphicon glyphicon-folder-close"></span>Categories</a>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/category"><span class="glyphicon glyphicon-plus-sign"></span> Add Category</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable ">
                            <a href="#"><span class="glyphicon glyphicon-folder-close"></span> Sub-Categories</a>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/sub_category"><span class="glyphicon glyphicon-plus-sign"></span> Add  Sub-Category</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable active">
                            <a href="#"><span class="glyphicon glyphicon-folder-close"></span>Products</a>
                            <ul>
                                <li class="active"><a href="<?php echo BASEURL; ?>Admin_panel/product"><span class="glyphicon glyphicon-plus-sign"></span> Add Product</a></li>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/view_product"><span class="fa fa-list"></span> View Products</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!--START  GREETING CARDS-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">E-Greetings</span></a>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Greeting_card/greeting_category"><span class="glyphicon glyphicon-plus-sign"></span> Add Greeting Category </a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Greeting_card/greeting"><span class="glyphicon glyphicon-plus-sign"></span>Upload Greeting Card Image</a></li>
                    </ul>
                </li>
                <!--END  GREETING CARDS-->
                <!--START  Product Voucher-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Product Voucher</span></a>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Product_voucher/voucher_category"><span class="glyphicon glyphicon-plus-sign"></span> Add Voucher Category </a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Product_voucher/voucher_product"><span class="glyphicon glyphicon-plus-sign"></span>Add Product Voucher</a></li>
                    </ul>
                    <ul>
                        <li ><a href="<?php echo BASEURL; ?>Product_voucher/amount_voucher"><span class="glyphicon glyphicon-plus-sign"></span>Add amount</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Product_voucher/display_products"><span class="glyphicon glyphicon-plus-sign"></span>Display Product Vouchers</a></li>
                    </ul>
                      <ul>
                        <li ><a href="<?php echo BASEURL; ?>Product_voucher/stock"><span class="glyphicon glyphicon-plus-sign"></span>Check Vouchers stock</a></li>
                    </ul>
                </li>
                <!--END  Product Voucher-->
                <!-- Brands-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span>Brands</a>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/brands"><span class="glyphicon glyphicon-plus-sign"></span>Add Brand</a></li>
                    </ul>
                </li>
                <!-- End Brands-->
                <!-- Banners-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span>Banners</a>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/banner"><span class="glyphicon glyphicon-plus-sign"></span>Add Banners</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/festivals"><span class="glyphicon glyphicon-plus-sign"></span> Add Festival Banners</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/activate_banners"><span class="glyphicon glyphicon-plus-sign"></span>Activate Banners</a></li>
                    </ul>
                </li>
                <!-- End Banners-->
                <!-- sales-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span>Sales</a>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/orders"><span class="glyphicon glyphicon-plus-sign"></span>Orders</a></li>
                    </ul>

                </li>
                <!-- End sales-->
                <!-- Reports-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span>Reports</a>
                    <ul>
                        <li>

                            <a href=""><span class="fa fa-th-list"></span>Products</a>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/best_sell"><span class="fa fa-angle-right"></span>Best Seller</a></li>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/most_view"><span class="fa fa-angle-right"></span>Most Viewed</a></li>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/ordered_product"><span class="fa fa-angle-right"></span>Products Ordered</a></li>
                            </ul>


                        </li>
                    </ul>

                </li>
                <!-- End reports-->

                <!-- orders-->
                <li class="xn-openable">
                    <a href="#"><span class="glyphicon glyphicon-list"></span>Orders</a>
                    <ul>
                        <li><a href="<?php echo BASEURL; ?>Admin_panel/invoice"><span class="glyphicon glyphicon-plus-sign"></span>Invoice</a></li>
                    </ul>

                </li>
                <!-- End orders-->
            </ul>
            <!-- END X-NAVIGATION -->
        </div>
        <!-- END PAGE SIDEBAR -->

        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     
            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span class="fa fa-arrow-circle-o-left"></span>ADD PRODUCT</h2>
            </div>
            <!-- END PAGE TITLE -->                
            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <form  method="post" id="product_form" action="<?php echo BASEURL . $action; ?>" enctype="multipart/form-data">
                            <div class="form-horizontal">
                                <div class="panel panel-default">
                                    <div class="panel-body">    
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Item Code</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="product_code" id="product_code" value="<?php echo $product_data[0]["product_code"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Main Menu</label>
                                            <div class="col-md-3 col-xs-12">    
                                                <select id="menu_id" name="menu_id" class="form-control" >
                                                    <option selected="selected" value="" >Select Menu</option>
                                                    <?php foreach ($menu_list as $menu) { ?>
                                                        <option value="<?php echo $menu["menu_id"]; ?>"  ><?php echo $menu["menu_name"]; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                            <div class="error_r1" style="color: red;margin-left:245px;"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Category</label>
                                            <div class="col-md-3 col-xs-12">    
                                                <select id="category_id" name="category_id"class="form-control">
                                                    <option selected="selected" value="0" >Select Category</option>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Sub-Category</label>
                                            <div class="col-md-3 col-xs-12">    
                                                <select id="subcategory_id" name="subcategory_id" class="form-control">
                                                    <option selected="selected" value="0" >Select Sub-Category</option>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Product Name</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="product_name" id="product_name" value="<?php echo $product_data[0]["product_name"]; ?>"/>
                                                </div>                                            
                                            </div>
                                            <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Meta Tags</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="meta_tags" id="meta_tags" value="<?php echo $product_data[0]["meta_tags"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Description</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <textarea class="form-control" rows="2" name="product_desc" id="product_desc"><?php echo $product_data[0]["product_desc"]; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Price</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="price" id="price" value="<?php echo $product_data[0]["price"]; ?>"/>
                                                </div>                                            
                                            </div>
                                            <div class="error_r3" style="color: red;margin-left:245px;"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Discount</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="discount" id="discount" value="<?php echo $product_data[0]["discount"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Out Of Stock Status</label>
                                            <div class="col-md-3 col-xs-12">    
                                                <select id="stock_status_id" name="stock_status_id" class="form-control">
                                                    <option selected="selected" value="0" >Select</option>
                                                    <?php foreach ($stock as $stock) { ?>
                                                        <option value="<?php echo $stock["id"]; ?>"><?php echo $stock["status"]; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Tax</label>
                                            <div class="col-md-3 col-xs-12">    
                                                <select id="tax" name="tax" class="form-control">
                                                    <option selected="selected" value="Tax Inc" >Tax Inc</option>
                                                    <?php //foreach ($menu as $menu) { ?>
                                                    <option value="<?php // echo $menu["menu_id"];                     ?>"  ><?php //echo $menu["menu_name"];              ?></option>
                                                    <?php //} ?>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Additional Charges</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="additional_charges" id="additional_charges" value="<?php echo $product_data[0]["additional_charges"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Brand</label>
                                            <div class="col-md-3 col-xs-12">                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                    <input type="text" class="form-control" name="brand" id="brand" value="<?php echo $product_data[0]["brand"]; ?>"/>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Featured Products</label>
                                            <div class="col-md-6 col-xs-12">                                                                                                                                        
                                                <label class="check"><input type="checkbox" class="checkbox" id="is_feature_product"  value="1" name="is_feature_product"/></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Special Products</label>
                                            <div class="col-md-6 col-xs-12">                                                                                                                                        
                                                <label class="check"><input type="checkbox" class="checkbox" id="is_special_product" value="1" name="is_special_product"/></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Best Selling</label>
                                            <div class="col-md-6 col-xs-12">                                                                                                                                        
                                                <label class="check"><input type="checkbox" class="checkbox" id="is_best_selling" value="1" name="is_best_selling"/></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Latest</label>
                                            <div class="col-md-6 col-xs-12">                                                                                                                                        
                                                <label class="check"><input type="checkbox" class="checkbox" value="1" name="is_latest"/></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Product Images</label>
                                            <div class="col-md-6 col-xs-12">  
                                                <?php if ($product_data[0]["product_image"] != '') { ?>
                                                    <div style="height: 20px; width: 238px; position: absolute; margin-left: 78px; margin-top: 3px; background-color: rgb(252, 248, 227);"><span id="change_img"><?php echo $product_data[0]["product_image"]; ?></span></div>
                                                <?php } ?>
                                                <input type="hidden" id="userfile_old"name="userfile_old1" value="<?php echo $product_data[0]["product_image"]; ?>" />
                                                <input type="file"  name="userfile[]" id="userfile" title="Browse file"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Additional Images</label>
                                            <div class="col-md-6 col-xs-12">     
                                                <?php if ($product_data[0]["additional_image"] != '') { ?>
                                                    <div style="height: 20px; width: 238px; position: absolute; margin-left: 78px; margin-top: 3px; background-color: rgb(252, 248, 227);"><span id="change_img1"><?php echo $product_data[0]["additional_image"]; ?></span></div>
                                                <?php } ?>
                                                <input type="hidden" id="userfile_old"name="userfile_old2" value="<?php echo $product_data[0]["additional_image"]; ?>" />
                                                <input type="file" name="userfile[]" id="userfile1" title="Browse file"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="btn btn-primary pull-right" style=" margin-left: 0px;" id="<?php echo $btn_click; ?>" pro_id="<?php echo $product_data[0]["pro_id"] ?>">Submit</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->
    <script>
        $(document).ready(function () {
            var menu_id = '<?php echo $product_data[0]["menu_id"]; ?>';
            $("#menu_id").val(menu_id);
            if (menu_id != '') {
                change_menu(menu_id);
                change_menu_subCategory(menu_id);
            }
            var is_feature_product = '<?php echo $product_data[0]["is_feature_product"]; ?>';
            if (is_feature_product == 1) {
                $('#is_feature_product').attr('checked', true);
            }
            var is_special_product = '<?php echo $product_data[0]["is_special_product"]; ?>';
            if (is_special_product == 1) {
                $('#is_special_product').attr('checked', true);
            }
            var is_best_selling = '<?php echo $product_data[0]["is_best_selling"]; ?>';
            if (is_best_selling == 1) {
                $('#is_best_selling').attr('checked', true);
            }
            var is_latest = '<?php echo $product_data[0]["is_latest"]; ?>';
            if (is_latest == 1) {
                $('#is_latest').attr('checked', true);
            }
            var stock_id = '<?php echo $product_data[0]["stock_status_id"]; ?>';
            $("#stock_status_id").val(stock_id);
        });
        $("#menu_id").change(function () {
            var menu_id = $(this).val();
            change_menu(menu_id);
            change_menu_subCategory(menu_id);
        });
        $("#category_id").change(function () {
            var category_id = $(this).val();
            change_category(category_id);
        });
        $("#submit_product").click(function () {
            var menu_id = $("#menu_id").val();
            var product_name = $("#product_name").val();
            var price = $("#price").val();
            if (menu_id == '') {
                $(".error_r1").html("Select Menu");
            }
            $('#menu_id').change(function (e) {
                $(".error_r1").hide();
            });
            if (product_name == '') {
                $(".error_r2").html("Enter Product Name");
            }
            $('#product_name').keyup(function (e) {
                $(".error_r2").hide();
            });
            if (price == '') {
                $(".error_r3").html("Enter Price");
            }
            $('#price').keyup(function (e) {
                $(".error_r3").hide();
            });
            if ((menu_id != '') && (product_name != '') && (price != '')) {
                $("#product_form").submit();
                alert("Product added Successfully!");
            }
        });
        $("#edit_product").click(function () {
            var menu_id = $("#menu_id").val();
            var product_name = $("#product_name").val();
            var price = $("#price").val();
            if (menu_id == '') {
                $(".error_r1").html("Select Menu");
            }
            $('#menu_id').change(function (e) {
                $(".error_r1").hide();
            });
            if (product_name == '') {
                $(".error_r2").html("Enter Product Name");
            }
            $('#product_name').keyup(function (e) {
                $(".error_r2").hide();
            });
            if (price == '') {
                $(".error_r3").html("Enter Price");
            }
            $('#price').keyup(function (e) {
                $(".error_r3").hide();
            });
            if ((menu_id != '') && (product_name != '') && (price != '')) {
                $("#product_form").submit();
                alert("Product edited Successfully!");
            }
        });
        function change_menu(menu_id) {
            var data = {
                menu_id: menu_id
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/get_category_name",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        var image = '';
                        var cat_count = r.subcategory.length;
                        for (i = 0; i < cat_count; i++) {
                            image += '<option value="' + r.subcategory[i]["category_id"] + '">' + r.subcategory[i]["category_name"] + '</option>'
                        }
                        $("#category_id").html(image);
                    }
                }
            });
        }
        function change_menu_subCategory(menu_id) {
            var data = {
                menu_id: menu_id
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/get_SubCategory_name",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        var image = '';
                        var cat_count = r.subcategory.length;
                        for (i = 0; i < cat_count; i++) {
                            image += '<option value="' + r.subcategory[i]["subcategory_id"] + '">' + r.subcategory[i]["subcategory_name"] + '</option>'
                        }
                        $("#subcategory_id").html(image);
                    }
                }
            });
        }

        function change_category(category_id) {
            var data = {
                is_Subcategory: category_id
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/SubCategory_name",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        var image = '';
                        var cat_count = r.subcategory.length;
                        for (i = 0; i < cat_count; i++) {
                            image += '<option value="' + r.subcategory[i]["category_id"] + '">' + r.subcategory[i]["category_name"] + '</option>'
                        }
                        $("#subcategory_id").html(image);
                    }
                }
            });
        }

        $(function () {
            $("#userfile").change(function () {
                var file = this.files[0];
                var imagefile = file.type;
                var match = ["image/jpeg", "image/png", "image/jpg"];
                if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
                {
                    $("#userfile").val("");
                    alert("Does not support file type");
                } else
                {
                    var reader = new FileReader();
                    reader.readAsDataURL(this.files[0]);
                    $("#change_img").html(file.name);
                    alert("Image uploaded Successfully");
                }

            });
        });
        $(function () {
            $("#userfile1").change(function () {
                var file = this.files[0];
                var imagefile = file.type;
                var match = ["image/jpeg", "image/png", "image/jpg"];
                if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
                {
                    $("#userfile1").val("");
                    alert("Does not support file type");
                } else
                {
                    var reader = new FileReader();
                    reader.readAsDataURL(this.files[0]);
                    $("#change_img1").html(file.name);
                    alert("Image uploaded Successfully");
                }

            });
        });
    </script>




