<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <script>
        $(document).ready(function () {
//            $("#sales").addClass("active");
//            $(".flower_orders li").addClass("active");
        });
    </script>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">

                <!-- END SEARCH -->
                <!-- POWER OFF -->
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     
            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span class="fa fa-arrow-circle-o-left"></span>Manage Friend List</h2>
            </div>
            <!-- END PAGE TITLE -->                
            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">

                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    

                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>User Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($user_info as $users) { ?>
                                                <tr>
                                                    <td><?php echo $users["username"]; ?></td>
                                                    <td><a class="get_refer_friend" user_id="<?php echo $users["user_id"]; ?>">View</a></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>

            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->    

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-times"></span> Remove <strong>Data</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to remove this row?</p>                    
                    <p>Press Yes if you sure.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <button class="btn btn-success btn-lg mb-control-yes">Yes</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->        

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->
    <!-- START PRELOADS -->
    <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>

    <script>
        $(".get_refer_friend").click(function () {
            var user_id = $(this).attr("user_id");
            window.location = "<?php echo BASEURL; ?>Admin_panel/get_refer_friend/" + user_id;

        });

    </script>
