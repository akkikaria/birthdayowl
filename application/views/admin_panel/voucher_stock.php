<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>zebra_datepicker.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>core.js"></script>
<body>
    <script>
        $(document).ready(function () {
//            $("#sales").addClass("active");
//            $(".orders li").addClass("active");
        });
    </script>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>

        <div class="page-content">
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
            </ul>
            <div class="page-title">                    
                <h2><span  class="fa fa-plus-circle"></span><?php echo $heading; ?></h2>
            </div>
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body"> 
                                <div class="col-md-12">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Vouchers</label>
                                            <div class="col-md-3 col-xs-12">    
                                                <select id="voucher_pro_id" name="voucher_pro_id" class="form-control">
                                                    <option selected="selected" value="0" >Select Vouchers</option>
                                                    <?php foreach ($vouchers as $vc) { ?>
                                                        <option value="<?php echo $vc["voucher_pro_id"]; ?>"><?php echo $vc["voucher_pro_name"]; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                            <div class="error_r1" style="color: red;margin-left:245px;"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 control-label">Amount</label>
                                            <div class="col-md-3 col-xs-12">    
                                                <select id="pamount" name="pamount" class="form-control">
                                                    <option selected="selected" value="0" >Select Amount</option>

                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                            <div class="error_r1" style="color: red;margin-left:245px;"></div>
                                        </div>




                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="col-md-4">
                                    <div class="btn btn-primary pull-right"  id="vsubmit" >Submit</div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div> 

                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">

                                <div class="btn-group pull-left">
                                    <div style="font-weight: bold;font-size: 14px;">Voucher Name:Product</div>
                                    <table>
                                        <tr>
                                            <td id="vcount" style="padding: 18px;"></td>
                                            <td id="available" style="padding: 18px;"></td>
                                            <td id="purchased" style="padding: 18px;"></td>
                                            <td id="expired" style="padding: 18px;"></td>
                                        </tr>

                                    </table>

                                </div>
                                <div class="btn-group pull-right">

                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    

                            </div>
                            <div class="panel-body">
                                <!--                               
                                
                                
                                <!-- START BORDERED TABLE SAMPLE -->
                                <div class="panel panel-default">

                                    <!--                                <div class="panel-heading">
                                                                        <h3 class="panel-title">Bordered table</h3>
                                                                    </div>-->
                                    <div class="panel-body">

                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>VOUCHER ID</th>
                                                        <th>VOUCHER NAME</th>
                                                        <th>AMOUNT</th>
                                                        <th>STATUS</th>
                                                        <th>EXPIRY DATE</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="stock_details">


                                                </tbody>
                                            </table> 
                                        </div>
                                    </div>
                                </div>
                                <!-- END BORDERED TABLE SAMPLE -->
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>

            </div>            

        </div>

        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $("#voucher_pro_id").change(function () {
            var voucher_pro_id = $(this).val();
            var data = {
                voucher_pro_id: voucher_pro_id
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Product_voucher/get_voucher_amount",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        var image = '';
                        var vcount = r.voucher_amt.length;
                        for (i = 0; i < vcount; i++) {
                            image += '<option value="' + r.voucher_amt[i]["id"] + '">' + r.voucher_amt[i]["pamount"] + '</option>'
                        }
                        $("#pamount").html(image);
                    }
                }
            });
        });



        $("#vsubmit").click(function () {
            var pamount = $("#pamount").val();
            var voucher_pro_id = $("#voucher_pro_id").val();
            if (voucher_pro_id == "0") {
                alert("Select voucher");
            } else if (pamount == '0') {
                alert("Select Amount");
            } else {
                var data = {
                    pamount: pamount,
                    voucher_pro_id: voucher_pro_id
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Product_voucher/check_stock",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            alert(r.message.toString());
                            return false;
                        } else {
                            var data = '';
                            var status_val = '';
                            var pro_count = r.vstock.length;

                            $("#available").html("<b>Available: </b> " + r.vstock[0]["availble_count"]);
                            $("#purchased").html("<b>Purchased: </b>" + r.vstock[0]["purchased_count"]);
                            $("#expired").html("<b>Expired: </b>" + r.vstock[0]["expired_count"]);
                            $("#vcount").html("<b>Count: </b>" + pro_count);


                            for (i = 0; i < pro_count; i++) {

                                var status = r.vstock[i]["status"];
                                if (status == "1") {
                                    status_val = "Available";
                                } else if (status == "0") {
                                    status_val = "Purchased";
                                } else {
                                    status_val = "Expired";
                                }
                                data += '<tr>'
                                        + '<td>' + r.vstock[i]["voucher_id"] + '</td>'
                                        + '<td>' + r.vstock[i]["voucher_pro_name"] + '</td>'
                                        + '<td>' + r.vstock[i]["pamount"] + '</td>'
                                        + '<td>' + status_val + '</td>'
                                        + '<td>' + r.vstock[i]["expiry_date"] + '</td>'
                                        + '</tr>'
                            }

                            $("#stock_details").html(data);
                        }
                    }
                });
            }
        });

    </script>