<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <script>
        $(document).ready(function () {
            $("#vouchers").addClass("active");
            $(".display_vouchers li").addClass("active");
        });
    </script>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>

        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                </li> 
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     

            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2><span  class="fa fa-plus-circle"></span><?php echo $heading; ?></h2>
            </div>
            <!-- END PAGE TITLE -->                

            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="delete_voucher_product()" style="margin-left: 10px;float: right;"><span class="fa fa-times"> DELETE</span></button>
                                <h3 class="panel-title"></h3>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>SELECT PRODUCT</th>
                                                <th>PRODUCT ID</th>
                                                <th>PRODUCT NAME</th>
                                                <th>AMOUNT</th>
                                                <th>CATEGORY NAME</th>

                                                <th>Edit</th>
                                                <th>Popularity_status</th>
                                                <th>Change Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            foreach ($voucher_product as $voucher) {
                                                if ($voucher["click_counter"] > 5) {
                                                    $status = "Popular";
                                                } else {
                                                    $status = "Unpopular";
                                                }
                                                ?>
                                                <tr>
                                                    <td><input type="checkbox" class="checkbox" value="<?php echo $voucher["voucher_pro_id"]; ?>" /></td>
                                                    <td><?php echo $voucher["voucher_pro_id"]; ?></td>
                                                    <td><?php echo $voucher["voucher_pro_name"]; ?></td>
                                                    <td><?php echo $voucher["amount"]; ?></td>
                                                    <td><?php echo $voucher["voucher_cat_name"]; ?></td>

                                                    <td> <button class="btn btn-default btn-rounded btn-condensed btn-sm edit_voucher_category" voucher_pro_id="<?php echo $voucher["voucher_pro_id"]; ?>" onclick="edit_voucher_pro('<?php echo $voucher["voucher_pro_id"]; ?>')"><span class="fa fa-pencil"></span></button></td>
                                                    <td><?php echo $status; ?></td>
                                                    <td>
                                                        <?php if ($voucher["click_counter"] > 5) { ?>
                                                            <a class="popular" voucher_pro_id="<?php echo $voucher["voucher_pro_id"]; ?>" click_counter="0" style="cursor: pointer;text-decoration: underline;">Unpopular</a>
                                                        <?php } else { ?>
                                                            <a class="popular" voucher_pro_id="<?php echo $voucher["voucher_pro_id"]; ?>" click_counter="10" style="cursor: pointer;text-decoration: underline;">Popular</a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>
            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function check_voucher_product_name() {
            var voucher_pro_name = $("#voucher_pro_name_submit").val();
            var result = 1;
            var data = {
                voucher_pro_name: voucher_pro_name
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Product_voucher/check_voucher_product_name",
                data: data,
                dataType: "json",
                async: false,
                success: function (r) {
                    //  console.log(r.success.toString());
                    if (r.success.toString() == "false") {
                        alert(r.message.toString());
                        $("#voucher_pro_name_submit").val('');
                        result = 0;
                    }
                }
            });
            if (result) {
                return true;
            }
            return false;
        }
        function edit_check_voucher_product_name() {
            var voucher_pro_name = $("#voucher_pro_name_edit").val();
            var voucher_pro_id = $("#voucher_pro_name_edit").attr("voucher_pro_id");
            var result = 1;
            var data = {
                voucher_pro_name: voucher_pro_name,
                voucher_pro_id: voucher_pro_id
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Product_voucher/Edit_check_voucher_product_name",
                data: data,
                dataType: "json",
                async: false,
                success: function (r) {
                    //  console.log(r.success.toString());
                    if (r.success.toString() == "false") {
                        alert(r.message.toString());
                        $("#voucher_pro_name_edit").val('');
                        result = 0;
                    }
                }
            });
            if (result) {
                return true;
            }
            return false;
        }

        function edit_voucher_pro(voucher_pro_id) {
            window.location = "<?php echo BASEURL; ?>Product_voucher/edit_voucher_product/" + voucher_pro_id;
        }
        $("#edit_voucher_product").click(function () {
            var voucher_pro_name = $("#voucher_pro_name_edit").val();
            // var result1 = check_voucher_product_name_edit();
            var userfile_old = $("#userfile_old").val();
            var amount = $("#amount").val();
            var card_id = $("#card_id").val();
            //  var select_greeting = $("#select_greeting").val();
            if (voucher_pro_name == '') {
                $(".error_r2").html("Product Voucher Name is required");
            }
            $('#voucher_pro_name_edit').keyup(function (e) {
                $(".error_r2").hide();
            });
            if (userfile_old == '') {
                $(".error_r3").html("Image upload is required.");
            } else {
                $(".error_r3").hide();
            }
            if (amount == '') {
                $(".error_r5").html("Enter Amount");
            }
            $('#amount').keyup(function () {
                $(".error_r5").hide();
            });
            if (card_id == '') {
                $(".error_r6").html("Select Greeting cards");
            }
            if ((voucher_pro_name != '') && (userfile_old != '') && (amount != '') && (card_id != '')) {
                alert("Product Voucher Product Updated Successfully!.");
                $("#voucher_submit").submit();
            }
        });

        $(".popular").click(function () {
            var voucher_pro_id = $(this).attr("voucher_pro_id");
            var click_counter = $(this).attr("click_counter");
            var data = {
                voucher_pro_id: voucher_pro_id,
                click_counter: click_counter
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Product_voucher/change_popularity",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        window.location = "<?php echo BASEURL; ?>Product_voucher/display_products";
                    }
                }
            });
        });

        function delete_voucher_product() {
            var checkboxArray = new Array();
            var checkedValues = $('.checkbox:checked').map(function () {
                checkboxArray.push(this.value);
            });
            if (checkboxArray == '') {
                alert("Select at least one Voucher Product");
            } else {
                var ask = window.confirm("Are you sure you want to delete?");
                if (ask) {
                    var data = {
                        voucher_pro_id: checkboxArray
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo BASEURL; ?>Product_voucher/delete_voucher_product",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.success.toString() === "false") {
                                $("#message").html(r.message.toString());
                                return false;
                            } else {
                                alert(r.message.toString());
                                window.location = "<?php echo BASEURL; ?>Product_voucher/display_products";
                            }
                        }
                    });

                } else {
                    window.location = "<?php echo BASEURL; ?>Product_voucher/display_products";
                }
            }
        }
    </script>