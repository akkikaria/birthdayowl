<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <script>
            $(document).ready(function () {
                $("#flowers").addClass("active");
                $(".categories li").addClass("active");
                $(".add_categories li").addClass("active");
            });
        </script>
        <!-- START PAGE SIDEBAR -->
        <div class="page-sidebar">
            <!-- START X-NAVIGATION -->
            <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
            <!-- END X-NAVIGATION -->
        </div>
        <!-- END PAGE SIDEBAR -->

        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- POWER OFF -->
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
                <!-- END POWER OFF -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->                     
            <!-- PAGE TITLE -->
            <div class="page-title">                    
                <h2 id="heading1" ><span class="fa fa-arrow-circle-o-left"></span>ADD CATEGORY</h2>
                <h2 id="heading2" style="display:none;"><span class="fa fa-arrow-circle-o-left"></span>EDIT CATEGORY</h2>
            </div>
            <!-- END PAGE TITLE -->                
            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-body">    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Main Menu</label>
                                        <div class="col-md-3 col-xs-12">    
                                            <select id="menu_id" name="menu_id" class="form-control">
                                                <option selected="selected" value="0" >Select Menu</option>
                                                <?php foreach ($menu as $menu) { ?>
                                                    <option value="<?php echo $menu["menu_id"]; ?>"  ><?php echo $menu["menu_name"]; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="error_r1" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Category Name</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="category_name" id="category_name" value="<?php echo $category["category_name"]; ?>"/>
                                            </div>                                            
                                        </div>
                                        <div class="error_r2" style="color: red;margin-left:245px;"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Description</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <textarea class="form-control" rows="2" name="description" id="description"><?php echo $category["description"]; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <button class="btn btn-primary pull-right"  id="submit_category" category_id="<?php echo $category["category_id"] ?>">Submit</button>
                                        <button class="btn btn-primary pull-right"  style="display:none;" id="update_category" category_id="">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- START DATATABLE EXPORT -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="delete_category()" style="margin-left: 1100px;"><span class="fa fa-times"> DELETE</span></button>
                                <h3 class="panel-title">Manage Categories</h3>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>SELECT CATEGORY</th>
                                                <th>MAIN MENU</th>
                                                <th>CATEGORY NAME</th>
                                                <th>CATEGORY DESCRIPTION</th>
                                                <th>Edit</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($categories as $categories) { ?>
                                                <tr>
                                                    <td><input type="checkbox" class="checkbox" value="<?php echo $categories["category_id"]; ?>" /></td>
                                                    <td><?php echo $categories["menu_name"]; ?></td>
                                                    <td><?php echo $categories["category_name"]; ?></td>
                                                    <td><?php echo $categories["description"]; ?></td>
                                                    <td> <button class="btn btn-default btn-rounded btn-condensed btn-sm edit_category" category_id="<?php echo $categories["category_id"]; ?>"><span class="fa fa-pencil"></span></button></td>

                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>
            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->
    <script>
        $(document).ready(function () {
            //$("#menu_id").val("<?php //echo $category["category_id"];              ?>");
        });
        $("#menu_id").change(function () {
            var menu_id = $(this).val();
            change_menu(menu_id);
        });
        $("#submit_category").click(function () {
            var menu_id = $("#menu_id").val();
            var category_name = $("#category_name").val();
            if (menu_id == '0') {
                $(".error_r1").html("Select Menu");
            }
            $('#menu_id').change(function (e) {
                $(".error_r1").hide();
            });
            if (category_name == '') {
                $(".error_r2").html("Category Name is Required");
            }
            $('#category_name').keyup(function (e) {
                $(".error_r2").hide();
            });
            if ((menu_id != '0') && (category_name != '')) {
                var data = {
                    menu_id: menu_id,
                    category_name: category_name,
                    description: $("#description").val()
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/add_category",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#message").html(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/category";
                        }
                    }
                });
            }
        });
        $("#update_category").click(function () {
            var menu_id = $("#menu_id").val();
            var category_id = $(this).attr("category_id");

            var category_name = $("#category_name").val();
            if (menu_id == '0') {
                $(".error_r1").html("Select Menu");
            }
            $('#menu_id').change(function (e) {
                $(".error_r1").hide();
            });
            if (category_name == '') {
                $(".error_r2").html("Category Name is Required");
            }
            $('#category_name').keyup(function (e) {
                $(".error_r2").hide();
            });
            if ((menu_id != '0') && (category_name != '')) {
                var data = {
                    menu_id: menu_id,
                    category_id: category_id,
                    category_name: category_name,
                    description: $("#description").val()
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/update_category",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#message").html(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());
                            window.location = "<?php echo BASEURL; ?>Admin_panel/category";
                        }
                    }
                });
            }
        });
        $(".edit_category").click(function () {
            var data = {
                category_id: $(this).attr("category_id")
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/get_sub_catDetails",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        var image = '';
                        var sub_cat = r.subcategory["category_id"];
                        var menu_id = r.subcategory["menu_id"];
                        var cat_name = r.subcategory["category_name"];
                        var cat_desc = r.subcategory["description"];
                        $("#menu_id").val(menu_id);
                        change_menu(menu_id);
                        $("#category_name").val(cat_name);
                        $("#description").val(cat_desc);
                        $("#heading1").hide();
                        $("#heading2").show();
                        $("#update_category").attr("category_id", sub_cat);
                        $("#submit_category").hide();
                        $("#update_category").show();

                    }
                }
            });
        });
        function change_menu(menu_id) {
            var data = {
                menu_id: menu_id
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Admin_panel/get_category_name",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        var image = '';
                        var cat_count = r.subcategory.length;
                        for (i = 0; i < cat_count; i++) {
                            image += '<option value="' + r.subcategory[i]["category_id"] + '">' + r.subcategory[i]["category_name"] + '</option>'
                        }
                        $("#category_id").html(image);
                    }
                }
            });
        }

        function delete_category() {
            var checkboxArray = new Array();

            var checkedValues = $('.checkbox:checked').map(function () {
                checkboxArray.push(this.value);
            });
            if (checkboxArray == '') {
                alert("Select Category");
            } else {
                var ask = window.confirm("Are you sure you want to delete?");
                if (ask) {
                    var data = {
                        id: checkboxArray,
                        from: 6
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo BASEURL; ?>Admin_panel/CommonDelete",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.success.toString() === "false") {
                                $("#message").html(r.message.toString());
                                return false;
                            } else {
                                alert(r.message.toString());
                                window.location = "<?php echo BASEURL; ?>Admin_panel/category";
                            }
                        }
                    });

                } else {
                    window.location = "<?php echo BASEURL; ?>Admin_panel/category";
                }
            }
        }
    </script>
