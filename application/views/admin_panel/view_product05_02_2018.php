<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from aqvatarius.com/themes/atlant_v1_4/html/table-export.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Dec 2014 12:26:16 GMT -->
    <head>        
        <!-- META SECTION -->
        <title><?php echo $title; ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->

        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo BASEURL_AMN_CSS; ?>theme-default.css"/> 
        <link rel="stylesheet" type="text/css"  href="<?php echo BASEURL_AMN_CSS; ?>admin.css"/>

        <!-- EOF CSS INCLUDE -->                                       
    </head>
    <?php //$this->load->view("admin_panel/admin_template/admin_footer"); ?>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">

            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <ul class="x-navigation">
                    <ul class="x-navigation">
                        <li>
                            <div style="height:100px;background: white;"> <a href="<?php echo BASEURL; ?>Admin_panel/admin_dashboard"><img src="<?php echo BASEURL_OIMG; ?>logo.png"  style=" width: 82%; margin-left: 15px; margin-top: 5px;"></a></div>
                        </li>
                        <li class="xn-title"></li>
                        <li class="active">
                         <!--   <a href="<?php echo BASEURL; ?>Admin_panel/admin_dashboard"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        -->
                        </li>

                        <li class="xn-openable">
                            <a href="<?php echo BASEURL; ?>Admin_panel/admin_dashboard"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Birthday Reminder</span></a>
                            <ul>
                                <li> <a href="<?php echo BASEURL; ?>Admin_panel/users"><span class="glyphicon glyphicon-plus-sign"></span>Users</a></li>
                            </ul>
                            <ul>
                                <li>  <a href="<?php echo BASEURL; ?>Admin_panel/company"><span class="glyphicon glyphicon-plus-sign"></span>Company Register</a></li>
                            </ul>
                            <ul>
                                <li>  <a href="<?php echo BASEURL; ?>Admin_panel/newsletter"><span class="glyphicon glyphicon-plus-sign"></span>Newsletter</a></li>
                            </ul>
                            <ul>
                                <li>  <a href="<?php echo BASEURL; ?>Admin_panel/friends"><span class="glyphicon glyphicon-plus-sign"></span>Refer Friend List</a></li>
                            </ul>
                            <ul>
                                <li>  <a href="<?php echo BASEURL; ?>Admin_panel/marquee"><span class="glyphicon glyphicon-plus-sign"></span>Marquee</a></li>
                            </ul>
                            <ul>
                                <li>   <a href="<?php echo BASEURL; ?>Admin_panel/add_sign_data"><span class="glyphicon glyphicon-plus-sign"></span> Add Sign Data</a></li>
                            </ul>
                            <ul>
                                <li> <a href="<?php echo BASEURL; ?>Admin_panel/relationship"><span class="glyphicon glyphicon-plus-sign"></span> Add Relationship</a></li>
                            </ul>
                            <ul>
                                <li>   <a href="<?php echo BASEURL; ?>Admin_panel/occupation"><span class="glyphicon glyphicon-plus-sign"></span> Add Occupation</a></li>
                            </ul>

                        </li>

                        <li class="xn-openable active">
                            <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Menu</span></a>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/banner"><span class="glyphicon glyphicon-plus-sign"></span> Add Banner</a></li>
                            </ul>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/menu"><span class="glyphicon glyphicon-plus-sign"></span> Add Main Menu</a></li>
                            </ul>
                            <ul>
                                <li class="xn-openable">
                                    <a href="#"><span class="glyphicon glyphicon-folder-close"></span>Categories</a>
                                    <ul>
                                        <li><a href="<?php echo BASEURL; ?>Admin_panel/category"><span class="glyphicon glyphicon-plus-sign"></span> Add Category</a></li>
                                    </ul>
                                </li>
                                <li class="xn-openable ">
                                    <a href="#"><span class="glyphicon glyphicon-folder-close"></span> Sub-Categories</a>
                                    <ul>
                                        <li class="active"><a href="<?php echo BASEURL; ?>Admin_panel/sub_category"><span class="glyphicon glyphicon-plus-sign"></span> Add  Sub-Category</a></li>
                                    </ul>
                                </li>
                                <li class="xn-openable active">
                                    <a href="#"><span class="glyphicon glyphicon-folder-close"></span>Products</a>
                                    <ul>
                                        <li><a href="<?php echo BASEURL; ?>Admin_panel/product"><span class="glyphicon glyphicon-plus-sign"></span> Add Product</a></li>
                                        <li class="active"><a href="<?php echo BASEURL; ?>Admin_panel/view_product"><span class="fa fa-list"></span> View Products</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!--START  GREETING CARDS-->
                        <li class="xn-openable">
                            <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">E-Greetings</span></a>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Greeting_card/greeting_category"><span class="glyphicon glyphicon-plus-sign"></span> Add Greeting Category </a></li>
                            </ul>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Greeting_card/greeting"><span class="glyphicon glyphicon-plus-sign"></span>Upload Greeting Card Image</a></li>
                            </ul>
                        </li>
                        <!--END  GREETING CARDS-->
                        <!--START  Product Voucher-->
                        <li class="xn-openable">
                            <a href="#"><span class="glyphicon glyphicon-list"></span> <span class="xn-text">Product Voucher</span></a>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Product_voucher/voucher_category"><span class="glyphicon glyphicon-plus-sign"></span> Add Voucher Category </a></li>
                            </ul>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Product_voucher/voucher_product"><span class="glyphicon glyphicon-plus-sign"></span>Add Product Voucher</a></li>
                            </ul>
                            <ul>
                                <li ><a href="<?php echo BASEURL; ?>Product_voucher/amount_voucher"><span class="glyphicon glyphicon-plus-sign"></span>Add amount</a></li>
                            </ul>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Product_voucher/display_products"><span class="glyphicon glyphicon-plus-sign"></span>Display Product Vouchers</a></li>
                            </ul>
                            <ul>
                                <li ><a href="<?php echo BASEURL; ?>Product_voucher/stock"><span class="glyphicon glyphicon-plus-sign"></span>Check Vouchers stock</a></li>
                            </ul>
                        </li>
                        <!--END  Product Voucher-->
                        <!-- Brands-->
                        <li class="xn-openable">
                            <a href="#"><span class="glyphicon glyphicon-list"></span>Brands</a>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/brands"><span class="glyphicon glyphicon-plus-sign"></span>Add Brand</a></li>
                            </ul>
                        </li>
                        <!-- End Brands-->
                        <!-- Banners-->
                        <li class="xn-openable">
                            <a href="#"><span class="glyphicon glyphicon-list"></span>Banners</a>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/banner"><span class="glyphicon glyphicon-plus-sign"></span>Add Banners</a></li>
                            </ul>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/festivals"><span class="glyphicon glyphicon-plus-sign"></span> Add Festival Banners</a></li>
                            </ul>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/activate_banners"><span class="glyphicon glyphicon-plus-sign"></span>Activate Banners</a></li>
                            </ul>
                        </li>
                        <!-- End Banners-->
                        <!-- sales-->
                        <li class="xn-openable">
                            <a href="#"><span class="glyphicon glyphicon-list"></span>Sales</a>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/orders"><span class="glyphicon glyphicon-plus-sign"></span>Orders</a></li>
                            </ul>

                        </li>
                        <!-- End sales-->
                        <!-- Reports-->
                        <li class="xn-openable">
                            <a href="#"><span class="glyphicon glyphicon-list"></span>Reports</a>
                            <ul>
                                <li>

                                    <a href=""><span class="fa fa-th-list"></span>Products</a>
                                    <ul>
                                        <li><a href="<?php echo BASEURL; ?>Admin_panel/best_sell"><span class="fa fa-angle-right"></span>Best Seller</a></li>
                                        <li><a href="<?php echo BASEURL; ?>Admin_panel/most_view"><span class="fa fa-angle-right"></span>Most Viewed</a></li>
                                        <li><a href="<?php echo BASEURL; ?>Admin_panel/ordered_product"><span class="fa fa-angle-right"></span>Products Ordered</a></li>
                                    </ul>


                                </li>
                            </ul>

                        </li>
                        <!-- End reports-->

                        <!-- orders-->
                        <li class="xn-openable">
                            <a href="#"><span class="glyphicon glyphicon-list"></span>Orders</a>
                            <ul>
                                <li><a href="<?php echo BASEURL; ?>Admin_panel/invoice"><span class="glyphicon glyphicon-plus-sign"></span>Invoice</a></li>
                            </ul>

                        </li>
                        <!-- End orders-->
                    </ul>
                </ul>
            </div>
            <!-- END PAGE SIDEBAR -->

            <!-- PAGE CONTENT -->
            <div class="page-content">

                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- POWER OFF -->
                    <li class="xn-icon-button pull-right last">
                        <a href="#"><span class="fa fa-power-off"></span></a>
                        <ul class="xn-drop-left animated zoomIn">
                            <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                        </ul>                        
                    </li> 
                    <!-- END POWER OFF -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span>View Products</h2>
                </div>
                <!-- END PAGE TITLE -->                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                <div class="panel-heading">

                                    <button class="btn btn-danger btn-rounded btn-condensed btn-sm delete_user" onclick="delete_product()" style="margin-left: 1254px;"><span class="fa fa-times"> DELETE</span></button>
                                    <div class="form-group">
                                        <div class="col-md-3 col-xs-12">    
                                            <select id="change_view" name="change_view" class="form-control">
                                                <option selected="selected" value="0" >View only Sub-Category Items</option>
                                                <option value="1">View Only Category Items</option>
                                            </select>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                        </ul>
                                    </div>                                    
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="customers2" class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>PRODUCT NAME</th>
                                                    <th>PRODUCT DESCRIPTION</th>
                                                    <th>CATEGORY</th>
                                                    <th>SUB-CATEGORY </th>
                                                    <th>Featured</th>
                                                    <th>Selling</th>
                                                    <th>Latest</th>
                                                    <th>Edit</th>
                                                </tr>
                                            </thead>
                                            <tbody id="product_data">
                                               <!-- <tr >
                                                    <td><input type="checkbox" class="checkbox" value="" /></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><input type="checkbox" class="checkbox" value="" /></td>
                                                    <td><input type="checkbox" class="checkbox" value="" /></td>
                                                    <td><input type="checkbox" class="checkbox" value="" /></td>
                                                    <td> <button class="btn btn-default btn-rounded btn-condensed btn-sm edit_subcategory" category_id="<?php //echo $subcategories["subcategory_id"];                  ?>"><span class="fa fa-pencil"></span></button></td>
                                                </tr><!-->
                                            </tbody>
                                        </table>                                    
                                    </div>
                                </div>
                            </div>
                            <!-- END DATATABLE EXPORT -->                            
                        </div>
                    </div>
                </div>         
                <!-- END PAGE CONTENT WRAPPER -->
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->          
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->
        <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->
        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='<?php echo BASEURL_AMN_JS; ?>plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/tableExport.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/jquery.base64.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/html2canvas.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/jspdf/jspdf.js"></script>
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins/tableexport/jspdf/libs/base64.js"></script>        
        <!-- END THIS PAGE PLUGINS-->  
        <!-- START TEMPLATE -->
        <!--<script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>settings.js"></script>-->
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>plugins.js"></script>        
        <script type="text/javascript" src="<?php echo BASEURL_AMN_JS; ?>actions.js"></script>        
        <!-- END TEMPLATE -->
        <!-- END SCRIPTS -->   
        <script>
                                                $(document).ready(function () {
                                                    diplay_view(0);
                                                    $("#change_view").change(function () {
                                                        var id = $(this).val();
                                                        diplay_view(id);
                                                    });
                                                });
                                                function diplay_view(id) {
                                                    if (id == 0) {
                                                        var data = {
                                                            id: id
                                                        }
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "<?php echo BASEURL; ?>Admin_panel/get_product_data",
                                                            data: data,
                                                            dataType: "json",
                                                            success: function (r) {
                                                                if (r.success.toString() === "false") {
                                                                    $("#message").html(r.message.toString());
                                                                    return false;
                                                                } else {
                                                                    var image = '';
                                                                    var cat_count = r.subcategory.length;
                                                                    for (i = 0; i < cat_count; i++) {
                                                                        var feature_product = r.subcategory[i]["is_feature_product"];
                                                                        var selling = r.subcategory[i]["is_best_selling"];
                                                                        var latest = r.subcategory[i]["is_latest"];
                                                                        image += '<tr>'
                                                                                + '<td><input type="checkbox" class="checkbox" value="' + r.subcategory[i]["pro_id"] + '" /></td>'
                                                                                + '<td>' + r.subcategory[i]["product_name"] + '</td>'
                                                                                + '<td>' + r.subcategory[i]["product_desc"] + '</td>'
                                                                                + '<td>' + r.subcategory[i]["category_name"] + '</td>'
                                                                                + '<td>' + r.subcategory[i]["sub_category"] + '</td>'
                                                                        if (feature_product == '1') {
                                                                            image += '<td><input type="checkbox" class="checkbox" checked="true" value=' + feature_product + '  /></td>'
                                                                        } else {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + feature_product + ' /></td>'
                                                                        }
                                                                        if (selling == '1') {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + selling + ' checked="true" /></td>'
                                                                        } else {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + selling + ' /></td>'
                                                                        }
                                                                        if (latest == '1') {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + latest + ' checked="true" /></td>'
                                                                        } else {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + latest + ' /></td>'
                                                                        }

                                                                        image += '<td> <div class="btn btn-default btn-rounded btn-condensed btn-sm  " onclick="edit_subcategory(' + r.subcategory[i]["pro_id"] + ')" ><span class="fa fa-pencil"></span></div></td>'
                                                                                + '</tr>'
                                                                    }
                                                                    $("#product_data").html(image);
                                                                }
                                                            }
                                                        });
                                                    } else if (id == 1) {
                                                        var data = {
                                                            id: id
                                                        }
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "<?php echo BASEURL; ?>Admin_panel/get_product_data",
                                                            data: data,
                                                            dataType: "json",
                                                            success: function (r) {
                                                                if (r.success.toString() === "false") {
                                                                    $("#message").html(r.message.toString());
                                                                    return false;
                                                                } else {
                                                                    var image = '';
                                                                    var cat_count = r.category.length;
                                                                    for (i = 0; i < cat_count; i++) {
                                                                        var feature_product = r.category[i]["is_feature_product"];
                                                                        var selling = r.category[i]["is_best_selling"];
                                                                        var latest = r.category[i]["is_latest"];

                                                                        var subcategory = '';
                                                                        image += '<tr>'
                                                                                + '<td><input type="checkbox" class="checkbox" value="' + r.category[i]["pro_id"] + '" /></td>'
                                                                                + '<td>' + r.category[i]["product_name"] + '</td>'
                                                                                + '<td>' + r.category[i]["product_desc"] + '</td>'
                                                                                + '<td>' + r.category[i]["category_name"] + '</td>'
                                                                                + '<td>' + subcategory + '</td>'
                                                                        if (feature_product == '1') {
                                                                            image += '<td><input type="checkbox" class="checkbox" checked="true" value=' + feature_product + '  /></td>'
                                                                        } else {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + feature_product + ' /></td>'
                                                                        }
                                                                        if (selling == '1') {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + selling + ' checked="true" /></td>'
                                                                        } else {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + selling + ' /></td>'
                                                                        }
                                                                        if (latest == '1') {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + latest + ' checked="true" /></td>'
                                                                        } else {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + latest + ' /></td>'
                                                                        }

                                                                        image += '<td> <div class="btn btn-default btn-rounded btn-condensed btn-sm  " onclick="edit_category(' + r.category[i]["pro_id"] + ')" ><span class="fa fa-pencil"></span></div></td>'
                                                                                + '</tr>'
                                                                    }
                                                                    $("#product_data").html(image);
                                                                }
                                                            }
                                                        });
                                                    } else if (id == 2) {
                                                        var data = {
                                                            id: id
                                                        }
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "<?php echo BASEURL; ?>Admin_panel/get_product_data",
                                                            data: data,
                                                            dataType: "json",
                                                            success: function (r) {
                                                                if (r.success.toString() === "false") {
                                                                    $("#message").html(r.message.toString());
                                                                    return false;
                                                                } else {
                                                                    var image = '';
                                                                    var cat_count = r.category.length;
                                                                    for (i = 0; i < cat_count; i++) {
                                                                        var feature_product = r.category[i]["is_feature_product"];
                                                                        var selling = r.category[i]["is_best_selling"];
                                                                        var latest = r.category[i]["is_latest"];

                                                                        var subcategory = '';
                                                                        image += '<tr>'
                                                                                + '<td><input type="checkbox" class="checkbox" value="" /></td>'
                                                                                + '<td>' + r.category[i]["product_name"] + '</td>'
                                                                                + '<td>' + r.category[i]["product_desc"] + '</td>'
                                                                                + '<td>' + r.category[i]["category_name"] + '</td>'
                                                                                + '<td>' + subcategory + '</td>'
                                                                        if (feature_product == '1') {
                                                                            image += '<td><input type="checkbox" class="checkbox" checked="true" value=' + feature_product + '  /></td>'
                                                                        } else {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + feature_product + ' /></td>'
                                                                        }
                                                                        if (selling == '1') {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + selling + ' checked="true" /></td>'
                                                                        } else {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + selling + ' /></td>'
                                                                        }
                                                                        if (latest == '1') {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + latest + ' checked="true" /></td>'
                                                                        } else {
                                                                            image += '<td><input type="checkbox" class="checkbox" value=' + latest + ' /></td>'
                                                                        }

                                                                        image += '<td> <div class="btn btn-default btn-rounded btn-condensed btn-sm  " onclick="edit_category(' + r.category[i]["pro_id"] + ')" ><span class="fa fa-pencil"></span></div></td>'
                                                                                + '</tr>'
                                                                    }
                                                                    $("#product_data").html(image);
                                                                }
                                                            }
                                                        });
                                                    }
                                                }

                                                function edit_category(pro_id) {
                                                    window.location = "<?php echo BASEURL; ?>Admin_panel/edit_product/" + pro_id;
                                                }
                                                function edit_subcategory(pro_id) {
                                                    window.location = "<?php echo BASEURL; ?>Admin_panel/edit_subcatproduct/" + pro_id;
                                                }
                                                function delete_product() {
                                                    var checkboxArray = new Array();
                                                    var checkedValues = $('.checkbox:checked').map(function () {
                                                        checkboxArray.push(this.value);
                                                    });
                                                    if (checkboxArray == '') {
                                                        alert("Select Product");
                                                    } else {
                                                        var ask = window.confirm("Are you sure you want to delete?");
                                                        if (ask) {
                                                            var data = {
                                                                id: checkboxArray,
                                                                from:13
                                                            }
                                                            $.ajax({
                                                                type: "POST",
                                                                url: "<?php echo BASEURL; ?>Admin_panel/CommonDelete",
                                                                data: data,
                                                                dataType: "json",
                                                                success: function (r) {
                                                                    if (r.success.toString() === "false") {
                                                                        $("#message").html(r.message.toString());
                                                                        return false;
                                                                    } else {
                                                                        alert(r.message.toString());
                                                                        window.location = "<?php echo BASEURL; ?>Admin_panel/view_product";
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            window.location = "<?php echo BASEURL; ?>Admin_panel/view_product";
                                                        }
                                                    }
                                                }
        </script>





