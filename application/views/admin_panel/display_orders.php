<?php $this->load->view("admin_panel/admin_template/admin_header"); ?>
<?php $this->load->view("admin_panel/admin_template/admin_footer"); ?>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>zebra_datepicker.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_DATE; ?>core.js"></script>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        <script>
            $(document).ready(function () {
                $("#sales").addClass("active");
                $(".orders li").addClass("active");
            });
        </script>

        <?php $this->load->view("admin_panel/admin_template/left_side_menu"); ?>
        <div class="page-content">
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <li >
                    <a href="#" style="font-size: 18px;"><span >Welcome Admin</span></a>

                </li> 
                <!-- POWER OFF -->
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
            </ul>
            <div class="page-title">                    
                <h2><span  class="fa fa-plus-circle"></span>ORDER DETAILS</h2>
            </div>
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-body">                                                                        
                                    <div class="form-group">
                                        <label class="col-md-1 " style="text-align: center;line-height: 23px;"> Date From:</label>
                                        <div class="col-md-2 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="order_start" id="order_start" style="background: white;color: black;"/>
                                            </div>                                            
                                        </div>
                                        <label class="col-md-1 " style="text-align: center;line-height: 23px;">Date To:</label>
                                        <div class="col-md-2 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="order_end" id="order_end" style="background: white;color: black;"/>
                                            </div>                                            
                                        </div>
                                        <div class="col-md-1">
                                            <button class="btn btn-primary pull-right"  id="submitorder">SUBMIT</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"></h3>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'false', ignoreColumn: '[2,3]'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'json', escape: 'true'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/json.png' width="24"/> JSON (with Escape)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'xml', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xml.png' width="24"/> XML</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'sql'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/sql.png' width="24"/> SQL</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'csv', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/csv.png' width="24"/> CSV</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'txt', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/txt.png' width="24"/> TXT</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'excel', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/xls.png' width="24"/> XLS</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'doc', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/word.png' width="24"/> Word</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'powerpoint', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/ppt.png' width="24"/> PowerPoint</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'png', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/png.png' width="24"/> PNG</a></li>
                                        <li><a href="#" onClick ="$('#customers2').tableExport({type: 'pdf', escape: 'false'});"><img src='<?php echo BASEURL_AMN_IMG; ?>icons/pdf.png' width="24"/> PDF</a></li>
                                    </ul>
                                </div>                                    
                            </div>
                            <div class="panel-body" >
                                <div class="table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>WOOHOO ORDER ID</th>
                                                <th>BDAYOWL ORDER ID</th>
                                                <th>CARD NUMBER</th>
                                                <th>PIN</th>
                                                <th>VOUCHER NAME</th>
                                                <th>AMOUNT</th>
                                                <th>TOTAL AMOUNT</th>
                                                <th>CUSTOMER NAME</th>
                                                <th>CUSTOMER EMAIL ID</th>
                                                <th>CUSTOMER PHONE</th>

                                                <th>ORDER STATUS</th>
                                                <th>DETAILS</th>
                                                <th>ACTION</th>
                                                <th>PURCHASED ON</th>
                                            </tr>
                                        </thead>
                                        <tbody id="order_history">

                                            <?php
                                            $i = 1;
                                            foreach ($orders as $order) {
                                                ?>
                                                <tr>
                                                    <?php
                                                    if ($order["payment_mode"] == "CC") {
                                                        $mode = "Credit Card";
                                                    } else if ($order["payment_mode"] == "DC") {
                                                        $mode = "Debit Card";
                                                    } else {
                                                        $mode = "Net Banking";
                                                    }

                                                    if ($order["delivery_status"] == 4) {
                                                        $status = "Successful";
                                                        $action = "";
                                                    } else if ($order["delivery_status"] == 3) {
                                                        $status = "payment failed";
                                                        $action = "";
                                                    } else if ($order["delivery_status"] == 8) {
                                                        $status = "failed";
                                                        $action = "REFUND";
                                                    } else if ($order["delivery_status"] == 6) {
                                                        $status = "Processing from whoohoo";
                                                        $action = "";
                                                    } else if ($order["delivery_status"] == 10) {
                                                        $status = "Refunded";
                                                        $action = "";
                                                    } else if ($order["delivery_status"] == 2) {
                                                        $status = "Pending";
                                                        $action = "";
                                                    }
                                                    ?>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $order["woohoo_order_id"]; ?></td>
                                                    <td><?php echo $order["unique_oid"]; ?></td>
                                                    <td><?php echo $order["voucher_code"]; ?></td>  
                                                    <td><?php echo $order["pin"]; ?></td>
                                                    <td><?php echo $order["voucher_pro_name"]; ?></td>
                                                    <td><?php echo $order["selected_amount"]; ?></td>
                                                    <td><?php echo $order["total_amount"]; ?></td>
                                                    <td><?php echo $order["uname"]; ?></td>
                                                    <td><?php echo $order["uemail"]; ?></td>
                                                    <td><?php echo $order["mobile_no"]; ?></td>
                                                    <td><?php echo $status; ?></td>
                                                    <td>
                                                        <?php if ($count > 0) { ?>
                                                            <a href="<?php echo BASEURL ?>Admin_panel/view_order/<?php echo $order["order_pro_id"]; ?>">VIEW</a>
                                                        <?php } ?>
                                                    </td>
                                                    <td> <a href="<?php echo BASEURL ?>Admin_panel/refund_view/voucher/<?php echo $order["order_pro_id"]; ?>"><?php echo $action; ?></a></td>
                                                    <td><?php echo date("d/m/Y h:i:s", strtotime($order["purchased_date"])); ?></td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                        <!-- END DATATABLE EXPORT -->                            
                    </div>
                </div>
            </div>         
            <!-- END PAGE CONTENT WRAPPER -->
        </div>            
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->          
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">

                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?php echo BASEURL; ?>Admin_panel/admin_logout" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('#customers2').dataTable({
            "pageLength": 25
        });
        $("#submitorder").click(function () {
            var start_date = $("#order_start").val();
            var end_date = $("#order_end").val();
            if (start_date == '') {
                alert("Enter From Date");
            } else if (end_date == '') {
                alert("Enter To Date ");
            } else {
                var data = {
                    order_start: start_date,
                    order_end: end_date
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/get_orderData",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#order_history").html(r.message.toString());
                            return false;
                        } else {
                            var count = r.vorders.length;
                            var data = '';
                            var mode = '';
                            var link = '', linkb = '';
                            var status = '', action = "";

                            if (count > 0) {
                                for (i = 0; i < count; i++) {
                                    if (r.vorders[i]["payment_mode"] == "CC") {
                                        mode = "Credit Card";
                                    } else if (r.vorders[i]["payment_mode"] == "DC") {
                                        mode = "Debit Card";
                                    } else {
                                        mode = "Net Banking";
                                    }



                                    if (r.vorders[i]["delivery_status"] == 4) {
                                        status = "Successful";
                                        action = '';
                                    } else if (r.vorders[i]["delivery_status"] == 3) {
                                        status = "payment failed";
                                        action = '';
                                    } else if (r.vorders[i]["delivery_status"] == 8) {
                                        status = "failed";
                                        action = 'REFUND';
                                    } else if (r.vorders[i]["delivery_status"] == 6) {
                                        status = "Processing from whoohoo";
                                        action = '';
                                    }

                                    link = "<?php echo BASEURL ?>Admin_panel/view_order/" + r.vorders[i]['payment_id'];
                                    linkb = "<?php echo BASEURL ?>Admin_panel/refund_view/" + r.vorders[i]["order_pro_id"];
                                    data += '<tr>'
                                            + '<td>' + (i + 1) + '</td>'
                                            + '<td>' + r.vorders[i]['woohoo_order_id'] + '</td>'
                                            + '<td>' + r.vorders[i]['unique_oid'] + '</td>'
                                            + '<td>' + r.vorders[i]['voucher_code'] + '</td>'
                                            + '<td>' + r.vorders[i]['pin'] + '</td>'
                                            + '<td>' + r.vorders[i]['voucher_pro_name'] + '</td>'
                                            + '<td>' + r.vorders[i]['selected_amount'] + '</td>'
                                            + '<td>' + r.vorders[i]['uname'] + '</td>'
                                            + '<td>' + r.vorders[i]['uemail'] + '</td>'
                                            + '<td>' + r.vorders[i]['mobile_no'] + '</td>'
                                            + '<td>' + status + '</td>'

                                            + '<td><a href = "' + link + '"> VIEW </a></td>'

                                            + '<td><a href = "' + linkb + '">' + action + '</a></td>'
                                            + '<td>' + r.vorders[i]['purchased_date'] + '</td>'
                                            + '</tr>'

                                }
                            } else {
                                data = "No orders"
                            }
                            $("#order_history").html(data);

                        }
                    }
                });
            }
        });
        function getDate(d, m) {
            $(".errors7").hide();
//                                var parts = date.split('/');
//                                var month = parts[0];
//                                var bdate = parts[1];
            if ((d != '') && (m != '')) {
                var data = {
                    bmonth: m,
                    bday: d
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>Admin_panel/get_zodiac",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $("#zodiac_id").val(r.message.toString());
                            return false;
                        } else {
                            $("#zodiac_id").val(r.message.toString());
                        }
                    }
                });
            }
        }

    </script>