<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head><title> Birthday Matters </title></head>
    <body>

        <div>
            <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                <tr>
                    <td>
                        <center>

                        </center>
                    </td>
                </tr>
            </table>

            <div style="font: 15px/17px Calibri; color: #000;">
                <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="21%">

                                        </td>
                                        <td width="79%" align="center" class="text18">
                                            <strong>Invoice</strong><br />
                                            <span class="padl70">Order No -
                                                <span id="lblorderno"><?php echo $payment_detail["order_id"] ?></span>
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="">
                                <strong>Thank you for shopping with Birthday Matters</strong>
                            </td>
                        </tr>
                        <tr height="15">
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                We're pleased to send you this order confirmation containing details of your order.
                                <span id="lblMsgOrderNo"></span>. If you have any further
                                queries, you will need to quote this reference number.
                            </td>
                        </tr>
                        <tr height="15">
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr height="15">
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" valign="top" style="padding-right: 25px">
                                <span class="text10"><strong>Shipping Address</strong>: </span>
                                <p>
                                    <span id="lblname">naviti mehra</span><br />
                                    Phone:<strong><span id="lblphone">9910374184</span>
                                    </strong>
                                    <br />
                                    <span id="lbladdress">House No. 2104, Sector-4,
                                        Urban Estate<br/>GURGAON,122001, Haryana, India.</span>
                                    <br />
                                    <span id="lblcountry">India</span>
                                    <br />
                                    <span id="lblemail">naviti.mehra@gmail.com</span>
                                </p>
                            </td>
                        </tr>
                        <tr height="15">
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="" colspan="2">
                                <table class="" cellspacing="1" cellpadding="1" width="100%" border="0">
                                    <tbody>
                                        <tr>
                                            <td width="50%" height="20" align="left">
                                                <strong>Payment Method:</strong>
                                                <span id="lblmethod">Debit Card</span>
                                            </td>
                                            <td width="50%" height="20" align="left">
                                                <strong>Order Date:</strong>
                                                <span id="lblDate"> <?php echo date('d/m/Y  h:i:s A', strtotime($payment_detail["purchased_date"])); ?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" align="left" valign="top">
                                                <strong>Order Status:</strong>
                                                <span id="lblstatus"> <?php echo $payment_detail["transaction_status"] ?>   </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table class="" style="border-right: 1px solid; border-top: 1px solid; border-left: 1px solid;
                                                       border-bottom: 1px solid" cellspacing="1" cellpadding="4" width="100%" border="1">
                                                    <tbody>
                                                        <tr style="font-weight: bold">
                                                            <td>
                                                                Product Id
                                                            </td>
                                                            <td>
                                                                Product Name
                                                            </td>

                                                            <td>
                                                                Total Cost
                                                            </td>

                                                        </tr>

                                                        <tr>
                                                            <td>  <?php echo $payment_detail["voucher_pro_id"] ?></td>
                                                            <td>
                                                                <?php echo $payment_detail["voucher_pro_name"] ?>
                                                            </td>

                                                            <td>
                                                               Rs. <?php echo $payment_detail["total_amount"] ?>
                                                            </td>

                                                        </tr>


                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr height="15">
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="style2" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="" colspan="2">
                                <table class="" id="table1" cellpadding="0" width="100%" border="0">
                                    <tbody>
                                        <tr>
                                            <td style="padding-right: 0.75pt; padding-left: 0.75pt; padding-bottom: 0.75pt; width: 5%;
                                                padding-top: 0.75pt" width="5%">
                                                &nbsp;
                                            </td>
                                            <td style="padding-right: 0.75pt; padding-left: 0.75pt; padding-bottom: 0.75pt; width: 36%;
                                                padding-top: 0.75pt" width="36%">
                                                <p style="text-align: right" align="right">
                                                    &nbsp;</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-right: 7.5pt; padding-bottom: 7.5pt; padding-top: 1.5pt" colspan="2">
                                                <span style="font-size: 11.5pt; color: #000;"><strong>How long will my order take to
                                                        arrive?</strong></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="" id="table2" cellpadding="0" width="100%" border="0">
                                    <tbody>
                                        <tr>
                                            <td valign="top">
                                                <p>
                                                    If you're unhappy with anything you buy from us, please contact our friendly customer
                                                    services team, who will be glad to help with exchanging or refunding your purchase.
                                                    Full details of your right to return are set out in the <a href="#" target="_blank">
                                                        terms and conditions</a> found on our web site.<br />
                                                    <br />
                                                    If you have any concerns with your order then please e-mail <a href="mailto:sales@heerson.com">
                                                        sales@Birthday_matters.com</a> or call us on (999) 999-9999.<br />
                                                    &nbsp;<br />
                                                    We thank you for shopping with Birthday Matters.</p>
                                                <p>
                                                    <br />
                                                    Kind regards,<br />
                                                    <br />
                                                    Birthday Matters
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="style2" style="padding-top: 7.5pt" colspan="2">
                                <!--DETAILS-->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </body>
</html>
