<?php
//echo '<pre>';
//print_r($this->session->userdata());
//exit;
?>
<?php $this->load->view("new_templates/header"); 

require_once(APPPATH.'libraries/Mobile_Detect.php');
$detect = new Mobile_Detect;
?>
<style>
    .grg_selection a:hover span {
        display: block;
    }
    .grg_selection a span {
        display: none;
        padding: 10px;
        color: #FFF !important;
        background: rgba(39,39,39,.67);
        position: absolute;
        width: 100%;
        bottom: 0;
        left: 0;
        text-align: center;
        margin: 0px !important;
    }
    .sortinfo{
        padding: 60px;
        text-align: justify;
        font-family:"gotham-book";
        font-size:15px;
    }
</style>

<style>

    .spacer-80{
        height: 60px;
        display: block;
    }

    .todays-zodiac-cont{
        background: url('<?php echo BASEURL_OIMG; ?>zodiac_home/todays-zodiac-background.png');
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        padding: 1em 0;
        color: #000000;
        margin: 6rem auto;
        font-family:"gotham-book";
    }

    .todays-zodiac-cont p{
        font-size: 20px;
        font-weight: 600;
        text-align: justify;
        line-height: 30px;

    }

    h1.rashi-title {
        margin-bottom: 0;
        font-weight: 700;
        font-size: 45px;
    }

    h3.rashi-date {
        margin-top: 0;
        font-size: 25px;
    }

    a.zodiac-more-btn {
        font-size: 30px;
        font-weight: 300;
        color: #333333;
        margin-top: 20px;
        display: block;
        text-align: center;
    }

    a.zodiac-more-btn:hover {
        text-decoration:  none;
        color: #333333;
    }

    h1.rashi-sec-title {
        font-weight: 700;
        margin-bottom: 25px;
    }

    .display-inlineblock{
        display: inline-block;
    }

    @media only screen and (max-width: 991px) and (min-width: 768px){

        .spacer-80{
            height: 50px;
        }

        h1.rashi-sec-title{
            margin-bottom: 20px;
            font-size: 22px;
        }

        h1.rashi-sec-title{
            font-size: 22px;
        }

        a.zodiac-more-btn{
            font-size: 18px;
            margin-top: 15px;
        }

        .flexbox{
            display: flex;
            flex-wrap: wrap;
            align-items: center;
        }

        .todays-zodiac-cont p{
            font-size: 15px;
        }

    }

    @media only screen and (max-width: 767px){

        .spacer-80{
            height: 30px;
        }
        h1.rashi-sec-title{
            font-size: 22px;
            margin-top: 0;
            text-align: center;
        }
        a.zodiac-more-btn{
            font-size: 20px;
            margin-bottom: 0px;
        }
        .zodiac-owlbox{
            display: none;
        }
        .todays-zodiac-cont p{
            font-size: 14px;
            font-weight: 600;
            text-align: justify;
            line-height: 25px;

        }
        h1.rashi-title {
            margin-bottom: 0;
            font-weight: 700;
            font-size: 25px;
        }
        .resp1{
            width:150px;
        }
        .sortinfo{
            padding: 10px;
            text-align: justify;
            font-family:"gotham-book";
            font-size:15px;
        }
    }
	
	
	
	#mobile-quick-menu-outer{
		margin: 35px 0 0;
	}
	
	#mobile-quick-menu-outer a .col-xs-6{
		padding: 0 10px;
	}
	
	.mobile-quick-menu {
		border: 0px solid #000;
		text-align: center;
		padding: 15px 10px;
		margin: 10px 0;
		box-shadow: 0px 5px 20px #ddd;
		border-radius: 10px;
		background: #ffffff;
	}

	.quick-menu-icon img {
		width: 60%;
		max-width: 50px;
		display: inline-block;
		margin-bottom: 10px;
	}

	.quick-menu-text p {
		margin-bottom: 0;
		text-transform: capitalize;
		font-size: 10px;
		font-family:gotham-bold;
	}
	
	.quick-menu-icon.flowers-cake img{
		width: 100%;
		max-width: 72%;
		max-width: 70px;
		display: inline-block;
	}
	
	
</style>

<?php 

// Any mobile device (phones or tablets).
if ( $detect->isMobile() ) {
	//echo "Mobile Device";
	$this->load->view("HOME/home_mobile_layout"); 
}else{
	//echo "Desktop Device";
	$this->load->view("HOME/home_desktop_layout"); 
}

?>

