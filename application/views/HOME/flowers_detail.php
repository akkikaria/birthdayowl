<?php $this->load->view("new_templates/header_inside"); ?>
<?php
//echo '<pre>';
//print_r($this->session->all_userdata());
//print_r($edit_flower_data);
//exit;
$flower_id = $flower_detail[0]['pro_id'];
$flower_code = $flower_detail[0]['product_code'];
$flower_image = $flower_detail[0]['product_image'];
$flower_name = $flower_detail[0]['product_name'];
$flower_price = $flower_detail[0]['price'];
$flower_content = $flower_detail[0]['product_content'];
$flower_description = $flower_detail[0]['product_description'];
$delivery_information = $flower_detail[0]['delivery_information'];
$care_instruction = $flower_detail[0]['care_instruction'];

//if ($delivery_information == "") {
//    $delivery_information = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
//}
//if ($care_instruction == "") {
//    $care_instruction = "labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ulla cupidatat";
//}
?>
<!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css"/>-->
<link href="<?PHP echo BASEURL_OCSS ?>slick.min.css" rel="stylesheet" type="text/css"/>
<!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css"/>-->
<link href="<?PHP echo BASEURL_OCSS ?>slick-theme-min.css" rel="stylesheet" type="text/css"/>
<link href="<?PHP echo BASEURL_OCSS ?>jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="<?PHP echo BASEURL_OCSS ?>calendar.css" rel="stylesheet" type="text/css"/>
<style>
    body {font-family: Arial, Helvetica, sans-serif;}

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 99999; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: lightgray; /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fff;
        margin: auto;
        /*padding: 20px;*/
        /*border: 1px solid #888;*/
        width: 500px;

    }

    /* The Close Button */
    .close_css {
        color: #fff;
        font-size: 28px;
        font-weight: bold;
        position: relative;
        top: 7px;
        right: 9px;
        z-index: 99999;
        text-align: right;
        opacity: .7;
        float: right;
        text-shadow: 0 1px 0 #fff;
        line-height: 35px;
    }

    .close_css:hover,
    .close_css:focus {
        color: #fff;
        text-decoration: none;
        cursor: pointer;
        opacity:1;
    }

    .main {
        font-family:Arial;

        display:block;
        margin:0 auto;
    }
    .slider div{
        /*height: 450px;*/
        background: #fff;
        /*color: #4caf50;*/
        font-size: 28px;
        /*position: relative;*/
        text-align: center;
    }
    .action{
        display:block;
        margin:100px auto;
        width:100%;
        text-align:center;
    }
    .action a {
        display:inline-block;
        padding:5px 10px; 
        background:#f30;
        color:#fff;
        text-decoration:none;
    }
    .action a:hover{
        /*background:lightgrey;*/
    }


    .ui-datepicker,
    .ui-datepicker table,
    .ui-datepicker tr,
    .ui-datepicker td,
    .ui-datepicker th {
        margin: 0;
        padding: 0;
        border: none;
        border-spacing: 0;
    }
    .ui-datepicker {
        display: none;
        width: 100%;
        /*padding: 0 25px;*/
        cursor: default;

        text-transform: uppercase;
        font-family: Tahoma;
        font-size: 12px;

        background: #141517;

        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;

        -webkit-box-shadow: 0px 1px 1px rgba(255,255,255, .1), inset 0px 1px 1px rgb(0,0,0);
        -moz-box-shadow: 0px 1px 1px rgba(255,255,255, .1), inset 0px 1px 1px rgb(0,0,0);
        box-shadow: 0px 1px 1px rgba(255,255,255, .1), inset 0px 1px 1px rgb(0,0,0);
    }
    .ui-datepicker-header {
        position: relative;
        padding-bottom: 10px;
        border-bottom: 1px solid #43a047;
    }

    .ui-datepicker-title { text-align: center; }

    .ui-datepicker-month {
        position: relative; 
        color: #424242;
        font-size:20px;
        font-weight:normal;
        text-transform:capitalize;

    }

    .ui-datepicker-year {
        padding-left: 8px;
        color: #9e9e9e;
        font-weight:normal;
        font-size:18px;
    }
    .ui-datepicker .ui-datepicker-title {line-height:30px;}
    .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover {background:none;}
    .ui-state-disabled, .ui-widget-content .ui-state-disabled {opacity:.7;}
    /* .ui-datepicker-month:before {
         display: block;
         position: absolute;
         top: 5px;
         right: 0;
         width: 5px;
         height: 5px;
         content: '';

         background: #a5cd4e;
         background: -moz-linear-gradient(top, #a5cd4e 0%, #6b8f1a 100%);
         background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#a5cd4e), color-stop(100%,#6b8f1a));
         background: -webkit-linear-gradient(top, #a5cd4e 0%,#6b8f1a 100%);
         background: -o-linear-gradient(top, #a5cd4e 0%,#6b8f1a 100%);
         background: -ms-linear-gradient(top, #a5cd4e 0%,#6b8f1a 100%);
         background: linear-gradient(top, #a5cd4e 0%,#6b8f1a 100%);

         -webkit-border-radius: 5px;
         -moz-border-radius: 5px;
         border-radius: 5px;
     }*/
    .ui-datepicker-prev,
    .ui-datepicker-next {
        position: absolute;
        top: -2px;
        padding: 5px;
        cursor: pointer;
    }

    .ui-datepicker-prev {
        left: 0;
        padding-left: 0;
    }

    .ui-datepicker-next {
        right: 0;
        padding-right: 0;
    }

    .ui-datepicker-prev span,
    .ui-datepicker-next span{
        display: block;
        width: 5px;
        height: 10px;
        text-indent: -9999px;

        background-image: url('<?php echo BASEURL_ONEWIMG; ?>arrows.png');
    }

    .ui-datepicker-prev span { background-position: 0px 0px; }

    .ui-datepicker-next span { background-position: -5px 0px; }

    /*.ui-datepicker-prev-hover span { background-position: 0px -10px; }*/

    /*.ui-datepicker-next-hover span { background-position: -5px -10px; }*/
    .ui-datepicker-calendar th {
        padding: 10px 0;
        /*padding-bottom: 10px;*/ 
        text-align: center;
        font-weight: normal;
        color: #424242;
        font-size:15px;
        text-transform:capitalize;
        /*padding: 15px 15px 15px 5px;*/
    }
    .ui-datepicker-calendar td {
        padding: 0 10px;
        text-align: center;
        line-height: 26px;
        font-size:15px;
    }

    .ui-datepicker-calendar .ui-state-default {
        display: block;
        width: 50px;
        outline: none; 
        text-decoration: none; 
        text-align:center; 
        border: 1px solid transparent;
        background:#e8f5e9;
    }
    .ui-datepicker-calendar .ui-state-active {
        color: #fff;
        background:#4caf50;
    }

    .ui-datepicker-other-month .ui-state-default { color: #565656; }

    #datetimeshipping {
        text-align: left;
        padding-left: 10px;
        /*border-bottom: 1px solid #2272b1;*/
        color: #444;
        position: relative;
        display: none;
        cursor: pointer;
    }
    #deliverydateofmonth {
        font-size: 3em;
        line-height: 100%;
        margin-right: 5px;
    }
    #deliverymonth, #deliveryweekday {
        text-transform: uppercase;
        position: absolute;
        font-size: 0.9em;
    }
    #timeslot {
        bottom: 10%;
    }
    #shippingmethod, #timeslot {
        position: absolute;
        left: 8.5em;
        white-space: nowrap;
        overflow: hidden;
        font-size: 0.9em;
    }
    #deliveryweekday {
        bottom: 10%;
    }
    #deliverymonth {
        top: 10%;
    }
    #shippingmethod {
        top: 10%;
    }
    #datetimeshipping #shippingcost {
        display: inline-block;
    }
    #shippingcost {
        position: absolute;
        text-transform: uppercase;
        color: #7ec9e8;
    }
    .ui-datepicker .ui-datepicker-next{
        right: 0px;
        /*top:5px;*/
    }
    .ui-datepicker .ui-datepicker-prev{
        left: 10px;
        /*top:5px;*/
    }
    @media screen and (max-width: 600px) {
        .modal-content {
            width: 275px;
        }
        .ui-datepicker-calendar td{
            padding: 0 2.5px;
        }
        .ui-datepicker-calendar .ui-state-default {
            width: 30px;
        }
        .slider div{
            font-size: 18px;
        }
        .ui-datepicker .ui-datepicker-next{
            right: 0px;
            top:5px;
        }
        .ui-datepicker .ui-datepicker-prev{
            left: 10px;
            top:5px;
        }
    }

</style>

<div style="clear: both;"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3">
            <div id="imgholder1" class="imgholder">
                <a><img src="<?php echo BASEURL_PRODUCT_IMG . $flower_image ?>" id="product_image"></a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-5">
            <div class="">
                <div>  
                    <p id="product_name" style="font-weight: bold; font-size: 20px;"><?php echo $flower_name; ?></p>
                </div>
                <ul class="nav nav-tabs" role="tablist" style=" ">
                    <li role="presentation" class="active"><a aria-controls="home" role="tab" data-toggle="tab" style="font-family: 'gotham-bold'; text-transform: uppercase;">Description</a></li>
                    <!--<li role="presentation"><a href="#delivery_information" aria-controls="delivery_information" role="tab" data-toggle="tab" style="font-family: 'gotham-bold'; text-transform: uppercase;">Delivery Information</a></li>-->
                    <!--<li role="presentation"><a href="#care_instruction" aria-controls="care_instruction" role="tab" data-toggle="tab" style="font-family: 'gotham-bold'; text-transform: uppercase;">Care Instructions</a></li>-->
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <p><b>Your Gift Contains: </b><?php echo $flower_content; ?></p>
                        <p><?php echo $flower_description; ?></p>
<!--                        <p>A bountiful bouquet of 30 red roses in a basket with a white big bow.</p>
                        <p>Occasions : Sympathy, Cheer Up</p>
                        <p>Includes : Orchid, White, Glass Vase arrangements, For Him, For The Elderly</p>-->

<!--                        <p id="product_name"><h3>A Proposal</h3></p>
                        <p><b>Your Gift Contains:</b></p>
                        <p id="product_content">Occasions : Sympathy, Cheer Up</p>
                        <p id="product_description">Includes : Orchid, White, Glass Vase arrangements, For Him, For The Elderly</p>                   -->
                    </div>
                    <!--                    <div role="tabpanel" class="tab-pane" id="delivery_information">
                    <?php echo $delivery_information; ?>
                                            <p></p>
                                        </div>-->
                    <!--                    <div role="tabpanel" class="tab-pane" id="care_instruction">
                    <?php echo $care_instruction; ?>
                                            <p>labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
                                                exercitation ulla cupidatat</p>
                                        </div>-->
                </div>
                <!--                <div id="imgholder1" class="imgholder">
                                    <a href="javascript:void(0);"><img src="<?php BASEURL_ONEWIMG ?>flower-2.jpg" id="i2"></a>
                                    <a href="javascript:void(0);"><img src="<?php BASEURL_ONEWIMG ?>flower-1.jpg" id="i3"></a>
                                </div>-->
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-offset-0 col-md-4 pro-content">
            <div class="location-div">
                <h3 id="product_price">₹. <?php echo $flower_price; ?>/-<i style="color: red; font-size: 25px;">*</i></h3>
                <!--<form>-->

                <div class="city-drop" style="margin: auto; width: 90%;">
                    <span>Area &amp; City (or) PIN</span>
                    <input class="location-input" type="text" placeholder="* Area &amp; City (or) PIN" name="country" id="country" autocomplete="off">
                    <span class="selectedlocality"></span>
                </div>
                <div class="when-drop" style="margin: auto; width: 90%;">
                    <span>When?</span>
                    <!-- <input class="date-input datepicker datepicker-input"  data-date-format="mm/dd/yyyy"> -->
                    <div id="datetimeshipping" style="display: block; border: 1px solid #ccc; background-color: #fff; height: 45px;"><span id="deliverydateofmonth"></span>
                        <span id="deliverymonth"></span>
                        <span id="timeslot"></span><span id="deliveryweekday"></span>
                        <span id="shippingmethod">
                            <span id="shippingcost">
                            </span>
                        </span>
                    </div>
                    <input type="hidden" id="delivery_date" />
                    <input type="hidden" id="delivery_time" />
                    <input type="hidden" id="delivery_shipping_method" />
            <!--                        <input id="input_01" class="datepicker date-input" name="date" type="text" autofocuss placeholder="* When" data-valuee="2014-08-08">
            
                                    <div id="container"></div>-->
                    <!-- The Modal -->
                    <div id="myModal" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content">
                            <div class="main" style="">

                                <div class="slider slider-for" style="margin-bottom: 15px;">
                                    <div class="slide1" style="">
                                        <a class="close_css">&times;</a>
                                        <div style="width: 100%;  margin: auto; text-align: center; border-bottom: 2px solid #43a047; background:#4caf50;"> 
                                            <span style="font: 300 21px / 46.001px Roboto; color:#fafafa;">Select Delivery Date</span>
                                        </div>  
                                        <div id="calendar" style=" border-bottom: 1px solid #4caf50;">
                                        </div>
                                        <!--                            <div style="height: 50px; text-align: left; border-bottom: 1px solid;">
                                        
                                                                    </div>-->
                                    </div>
                                    <div class="slide2">
                                        <a class="close_css">&times;</a>
                                        <div style="width: 100%;  margin: auto; text-align: center; border-bottom: 2px solid #43a047; background:#4caf50;"> 
                                            <span style="font: 300 21px / 46.001px Roboto; color:#fafafa;">Select Shipping Method</span>
                                        </div> 
                                        <div style="height: 275px; border-bottom: 1px solid; padding: 20px 25px;">
                                            <div style="height: 65px; border: 1px solid;">
                                                <table style="height: 100%; width: 100%;">
                                                    <tr class="standard_delivery" delivery="Standard Delivery" style="cursor: pointer;" data-slide="3">
                                                        <td style="width: 10%;">
                                                            <div id="shipping_method" style="margin: 10px; font-size: 0px;">
                                                                <input name="shippingtime" id="EXPRESS_DELIVERY" type="radio">
                                                            </div>
                                                        </td>
                                                        <td style="width: 70%; color:#424242;">
                                                            <span class="timesloter">Standard Delivery</span>
                                                        </td>
                                                        <td style="width: 20%; background: #4caf50; color: white;">
                                                            <span class="delcost">Free</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div style="height: 50px; text-align: center;">
                                            <a style="font-size: 15px; color:#424242;" onclick="backToCalendar()" data-slide="1" href="">Back to Calendar</a>
                                        </div>
                                    </div>
                                    <div class="slide3">
                                        <a class="close_css">&times;</a>
                                        <div style="width: 100%;  margin: auto; text-align: center; border-bottom: 2px solid #43a047; background:#4caf50;"> 
                                            <span style="font: 300 21px / 46.001px Roboto; color:#fafafa;">Select Time Slot</span>
                                        </div>  
                                        <div style="height: 275px; border-bottom: 1px solid; padding: 20px 25px;">
                                            <br/>
                                            <table style="margin: auto;">
                                                <tr class="timeslottable" style="cursor: pointer; height: 65px; border: 1px solid; margin: 10px 0;" time_slot="1" time="09:00 - 15:00HRS" data-slide="1" >
                                                    <td style="">
                                                        <div id="" style="margin: 10px; font-size: 0px;">
                                                            <input class="selectTimeSlot" name="selectTimeSlot" id="time1"  type="radio">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <span class="timesloter" style="margin: 0 10px; color:#424242;">09:00 - 15:00 hrs</span>
                                                    </td>
                                                </tr>
                                                <tr style="height: 30px;"></tr>
                                                <tr class="timeslottable" style="cursor: pointer; height: 65px; border: 1px solid; margin: 10px 0; color:#424242;" time_slot="2" time="15:00 - 21:00HRS" data-slide="1" >
                                                    <td style="">
                                                        <div id="" style="margin: 10px; font-size: 0px;">
                                                            <input class="selectTimeSlot" name="selectTimeSlot" id="time2"  type="radio">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <span class="timesloter" style="margin: 0 10px;">15:00 - 21:00 hrs</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="height: 50px; text-align: center; ">
                                            <a style="font-size: 15px; color:#424242;" onclick="backToCalendar()" data-slide="1" href="">Back to Calendar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="text-align: center; color: red;">
                    <span id="error_message"></span>
                </div>
                <div class="btn-div">
                    <!--<a class="btn btn-primary btn-atc" href="#">Add To Cart</a><br>-->
                    <a class="btn btn-primary btn-atc" style="font-family: gotham-book;" id="docheckout" is_edit="<?php echo $is_edit; ?>">Buy Now</a>
                    <!--href="<?php // echo BASEURL;                                                                                                                                                                                                                                              ?>Home_web/delivery_address"-->
                </div>
                <div class="btn-div"><span style="color:red;">*Processing charges 3.5%.</span></div>
                <!--</form>-->
            </div>
        </div>
        <!-- <div class="col-md-7 pro-content">
            <h1>A Proposal</h1>
            <p>A bountiful bouquet of 30 red roses in a basket with a white big bow.</p>
            <h3>₹. 1450/-</h3>
            <h4>Item is available at city: <span> Mumbai - change</span></h4>
            <h4>Select delivery date & time: <span><img src="tools/img/date-picker.png" data-date-format="mm/dd/yyyy"class="img-responsive datepicker"> <input class="datepicker datepicker-input" placeholder="mm/dd/yyyy" data-date-format="mm/dd/yyyy"></span></h4>
            <span><a class="btn btn-primary btn-atc" href="#">Add To Cart</a></span>
            <h5>Occasions : Sympathy, Cheer Up</h5>
            <h5>Includes : Orchid, White, Glass Vase arrangements, For Him, For The Elderly</h5>
        </div> -->
    </div>
</div>
<?php if ($is_edit == "0") { ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center you-may-title">
                <h1>You May Also Like...</h1>
            </div>
        </div>

        <div class="row">
            <div class="owl-carousel owl-theme related-product">
                <?php
                $flower_count = count($flower_product_random);
                if ($flower_count != 0) {
                    for ($i = 0; $i < $flower_count; $i++) {
                        $product_id = $flower_product_random[$i]['pro_id'];
                        $product_name = $flower_product_random[$i]['product_name'];
                        $image_name = $flower_product_random[$i]['product_image'];
                        $price = $flower_product_random[$i]['price'];
                        ?>
                        <div class="item pro"  product_id="<?php echo $product_id; ?>" style="cursor: pointer" >
                            <img src="<?php echo BASEURL_PRODUCT_IMG . $image_name ?>" class="img-responsive">
                            <div class="content_height">
                                <h4><?php echo $product_name; ?></h4>
                                <h4>₹. <?php echo $price; ?></h4>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>

    </div>
<?php } ?>

<!--<div id="modaldatetimepicker" data-ga-category="SelectDate &amp; timeslot" class="reveal-modal slideable-dialog slick-initialized slick-slider open" data-reveal="" aria-hidden="false" role="dialog" data-options="close_on_background_click:false; close_on_esc:false" style="display: block; opacity: 1; visibility: visible; top: 70px;" tabindex="0"><div id="prevArrow" class="slick-prev slick-arrow slick-disabled" aria-disabled="true" style="display: block;"></div>
    <a class="close-reveal-modal roboX product-reveal-close" href="javascript:void(0);" aria-label="Close">×</a>
    <div aria-live="polite" class="slick-list"><div class="slick-track" style="opacity: 1; width: 15000px; transform: translate3d(0px, 0px, 0px);" role="listbox"><div class="slider-item slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="0" role="option" aria-describedby="slick-slide10">
                <div class="datepickerhead">
                    <span class="calendartitle">Select Delivery Date</span>
                </div>
                <div data-ga-category="SelectDate &amp; timeslot" id="deliverydatepicker" class="hasDatepicker"><div class="ui-datepicker-inline ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="display: block;"><div class="ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all"><a class="ui-datepicker-prev ui-corner-all ui-state-disabled" title="Prev" tabindex="0"><span class="ui-icon ui-icon-circle-triangle-w">Prev</span></a><a class="ui-datepicker-next ui-corner-all" data-handler="next" data-event="click" title="Next" tabindex="0"><span class="ui-icon ui-icon-circle-triangle-e">Next</span></a><div class="ui-datepicker-title"><span class="ui-datepicker-month">February</span>&nbsp;<span class="ui-datepicker-year">2018</span></div></div><table class="ui-datepicker-calendar"><thead><tr><th scope="col" class="ui-datepicker-week-end"><span title="Sunday">S</span></th><th scope="col"><span title="Monday">M</span></th><th scope="col"><span title="Tuesday">T</span></th><th scope="col"><span title="Wednesday">W</span></th><th scope="col"><span title="Thursday">T</span></th><th scope="col"><span title="Friday">F</span></th><th scope="col" class="ui-datepicker-week-end"><span title="Saturday">S</span></th></tr></thead><tbody><tr><td class=" ui-datepicker-week-end ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">28</span></td><td class=" ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">29</span></td><td class=" ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">30</span></td><td class=" ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">31</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">1</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">2</span></td><td class=" ui-datepicker-week-end ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">3</span></td></tr><tr><td class=" ui-datepicker-week-end ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">4</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">5</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">6</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">7</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">8</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">9</span></td><td class=" ui-datepicker-week-end ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">10</span></td></tr><tr><td class=" ui-datepicker-week-end ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">11</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">12</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">13</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">14</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">15</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">16</span></td><td class=" ui-datepicker-week-end ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">17</span></td></tr><tr><td class=" ui-datepicker-week-end ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">18</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">19</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">20</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">21</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">22</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">23</span></td><td class=" ui-datepicker-week-end ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">24</span></td></tr><tr><td class=" ui-datepicker-week-end ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">25</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">26</span></td><td class=" ui-datepicker-unselectable ui-state-disabled  holiday"><span class="ui-state-default">27</span></td><td class=" ui-datepicker-days-cell-over  availableDate ui-datepicker-current-day ui-datepicker-today" data-handler="selectDay" data-event="click" data-month="1" data-year="2018"><a class="ui-state-default ui-state-highlight ui-state-active ui-state-hover" href="#" onclick="gaElement(this);" data-ga-title="2018-1-28" tabindex="0">28</a></td><td class=" ui-datepicker-other-month  availableDate otherMonthAvailDate" data-handler="selectDay" data-event="click" data-month="2" data-year="2018"><a class="ui-state-default ui-priority-secondary" href="#" onclick="gaElement(this);" data-ga-title="2018-2-1" tabindex="0">1</a></td><td class=" ui-datepicker-other-month  availableDate otherMonthAvailDate" data-handler="selectDay" data-event="click" data-month="2" data-year="2018"><a class="ui-state-default ui-priority-secondary" href="#" onclick="gaElement(this);" data-ga-title="2018-2-2" tabindex="0">2</a></td><td class=" ui-datepicker-week-end ui-datepicker-other-month  availableDate otherMonthAvailDate" data-handler="selectDay" data-event="click" data-month="2" data-year="2018"><a class="ui-state-default ui-priority-secondary" href="#" onclick="gaElement(this);" data-ga-title="2018-2-3" tabindex="0">3</a></td></tr></tbody></table></div></div>
                <div class="price-info-legend-message"><span></span><span class="legend-message priceinfo-icon-display"><span>Price surge applicable for these delivery dates due to Valentine's Day season</span></span></div>
                <div class="calendarfooter">
                    <a class="close-reveal-modal roboX" href="javascript:void(0);" aria-label="Close" tabindex="0">×</a>
                </div>
            </div><div id="modaltimeslot" class="slider-item slick-slide" data-ga-category="SelectDate &amp; timeslot" data-slick-index="1" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide11">
                <div class="timeslothead">
                    <h1 class="dialogueheadtitle">Select Time Slot</h1>
                </div>
                <div id="shippingmethoddiv"></div>
                <div class="timeslotfooter">
                    <span class="b2c" onclick="goToCalendar()" style="cursor: pointer;">Back to Calendar</span>
                </div>
            </div><div id="modalshiping" class="slider-item slick-slide" data-slick-index="2" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide12">
                <div class="timeslothead">
                    <a href="javascript:void(0);" class="backtocalendar" onclick="goToShippingMethod()" tabindex="-1">
                        <img src="/assets/images/back-arrow-icon.png" alt="back to calendar">
                    </a>
                    <h1 class="dialogueheadtitle">Select Time Slot</h1>
                </div>
                <div id="timeslotDiv"></div>
                <div class="shippingmodepanel"></div>
                <div class="timeslotfooter shipmodefooter">
                    <span class="b2c" onclick="goToCalendar()" style="cursor: pointer;">Back to Calendar</span>
                </div>
                <a class="close-reveal-modal roboX" href="javascript:void(0);" aria-label="Close" tabindex="-1">×</a>
            </div></div></div>


    <div id="nextArrow" class="slick-next slick-arrow" style="display: block;" aria-disabled="false"></div></div>-->
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js"></script>
<script src="<?PHP echo BASEURL_OJS ?>slick.js" type="text/javascript"></script>
<script src="<?PHP echo BASEURL_OJS ?>jquery-dateFormat.js" type="text/javascript"></script>
<script src="<?PHP echo BASEURL_OJS ?>jquery-ui.js" type="text/javascript"></script>
<script src="<?PHP echo BASEURL_OJS ?>jquery-ui-datepicker.min.js"></script>
<script>
                                                $(document).ready(function () {
                                                    var maxHeight = Math.max.apply(null, $(".content_height").map(function ()
                                                    {
                                                        return $(this).height();
                                                    }).get());
                                                    $('.content_height').css('height', maxHeight + 'px');
                                                });

                                                $('#country').focus(function () {
                                                    $('#error_message').html('');
                                                });
                                                var delivery_time = "";
                                                var delivery_date = "";
                                                var delivery_shipping_method = "";
                                                var delivery_date_date = "";
                                                var delivery_date_month = "";
                                                var delivery_date_year = "";
                                                var delivery_date_day = "";
                                                var date = new Date();
<?php if (count($edit_flower_data) > 0) { ?>
                                                    var dateAsObject = '<?php echo $edit_flower_data['delivery_date']; ?>';
                                                    date = new Date(dateAsObject);
                                                    delivery_date = $.format.date(date, "yyyy-MM-dd");
                                                    delivery_date_date = $.format.date(date, "dd");
                                                    delivery_date_month = $.format.date(date, "MMM");
                                                    delivery_date_year = $.format.date(date, "yyyy");
                                                    delivery_date_day = $.format.date(date, "E");
                                                    $('#deliverydateofmonth').html(delivery_date_date);
                                                    $('#deliverymonth').html(delivery_date_month);
                                                    $('#timeslot').html("<?php echo $edit_flower_data['delivery_time_slot']; ?>");
                                                    $('#deliveryweekday').html(delivery_date_day);
                                                    $('#shippingmethod').html("<?php echo $edit_flower_data['delivery_shipping_method']; ?>" + " : FREE");
                                                    $('#delivery_date').val("<?php echo $edit_flower_data['delivery_date']; ?>");
                                                    $('#delivery_time').val("<?php echo $edit_flower_data['delivery_time_slot']; ?>");
                                                    delivery_time = "<?php echo $edit_flower_data['delivery_time_slot']; ?>";
                                                    $('#delivery_shipping_method').val("<?php echo $edit_flower_data['delivery_shipping_method']; ?>");
                                                    delivery_shipping_method = "<?php echo $edit_flower_data['delivery_shipping_method']; ?>";
                                                    $('#country').val("<?php echo $edit_flower_data['delivery_pincode']; ?>" + ", " + "<?php echo $edit_flower_data['delivery_city']; ?>");
<?php } ?>





                                                $('#combine_value').val('');
                                                //                                    $('#shippingcost').hide();
                                                $(document).on('click', '.timeslottable', function () {
                                                    var time_slot = $(this).attr('time_slot');
                                                    delivery_time = $(this).attr('time');
                                                    $('#delivery_time').val(delivery_time);
                                                    if (time_slot == "1") {
                                                        $('#time2').attr('checked', false);
                                                        $('#time1').attr('checked', true);
                                                    } else {
                                                        $('#time1').attr('checked', false);
                                                        $('#time2').attr('checked', true);
                                                    }
                                                    modal.style.display = "none";
                                                    $("body").css("overflow", "auto");
                                                    var combine_value = delivery_date + ', ' + delivery_shipping_method + ':Free, ' + delivery_time;
                                                    $('#combine_value').val(combine_value);
                                                    $('#deliverydateofmonth').html(delivery_date_date);
                                                    $('#deliverymonth').html(delivery_date_month);
                                                    $('#timeslot').html(delivery_time);
                                                    $('#deliveryweekday').html(delivery_date_day);
                                                    $('#shippingmethod').html(delivery_shipping_method + " : FREE");
                                                    //                                        $('#deliveryweekday').html(delivery_date_day);
                                                    //                                        $('#shippingcost').html();


                                                });
                                                $("#calendar").datepicker("setDate", date);
                                                //                                                var today = new Date($.now());
                                                var today_date_time = '<?php echo Date('Y-m-d H:i:s'); ?>';
                                                var today_date = '<?php echo Date('Y-m-d'); ?>';
                                                var today_time = '<?php echo Date('H:i:s'); ?>';
                                                //                                                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                                                var compare_time = today_time.toString("hh:mm:ss");
                                                if (compare_time > "15:59:59" && compare_time < "23:59:59") {
                                                    $('#calendar').datepicker({
                                                        minDate: +2,
                                                        inline: true,
                                                        firstDay: 1,
                                                        showOtherMonths: true,
                                                        dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                                                        setDate: date

                                                    });
                                                    $("#calendar").datepicker("setDate", date);
                                                } else {
                                                    $('#calendar').datepicker({
                                                        minDate: +1,
                                                        inline: true,
                                                        firstDay: 1,
                                                        showOtherMonths: true,
                                                        dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                                                        setDate: date

                                                    });
                                                    $("#calendar").datepicker("setDate", date);
                                                }

                                                $(document).on('click', '.ui-state-default', function () {
                                                    var slideno = $(this).data('slide');
                                                    $('.slider-for').slick('slickGoTo', slideno - 1);
                                                    $('#EXPRESS_DELIVERY').attr("checked", false);
                                                    $('#time2').attr('checked', false);
                                                    $('#time1').attr('checked', false);
                                                    var dateAsObject = $("#calendar").datepicker('getDate'); //the getDate method
                                                    var date = new Date(dateAsObject);
                                                    delivery_date = $.format.date(date, "yyyy-MM-dd");
                                                    delivery_date_date = $.format.date(date, "dd");
                                                    delivery_date_month = $.format.date(date, "MMM");
                                                    delivery_date_year = $.format.date(date, "yyyy");
                                                    delivery_date_day = $.format.date(date, "E");
                                                    //                                        $('#weekDay').val(e.date.format('dddd'));
                                                    //                                        alert(delivery_date_date);
                                                    //                                        alert(delivery_date_month);
                                                    //                                        alert(delivery_date_year);
                                                    //                                        alert(delivery_date_day);
                                                    $('#delivery_date').val(delivery_date);
                                                });
                                                $(document).on('click', '.standard_delivery', function () {
                                                    var slideno = $(this).data('slide');
                                                    $('.slider-for').slick('slickGoTo', slideno - 1);
                                                    $('#EXPRESS_DELIVERY').attr("checked", "checked");
                                                    delivery_shipping_method = $(this).attr('delivery');
                                                    $('#delivery_shipping_method').val(delivery_shipping_method);
                                                });</script>
<script>

    // Get the modal
    var modal = document.getElementById('myModal');
    // Get the button that opens the modal
    var btn = document.getElementById("datetimeshipping");
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    // When the user clicks the button, open the modal 
    btn.onclick = function () {
        //        alert(date);
        $('#error_message').html('');
        var country = $('#country').val();
        var array = $('#country').val().split(",");
        var flower_city = $.trim(array[1]);
        var flower_pincode = $.trim(array[0]);
        if (country == '') {
            $('#error_message').html('Valid Area / City / PIN required.');
        } else if (flower_pincode.length != '6') {
            $('#error_message').html('Valid Area / City / PIN required.');
        } else {
            modal.style.display = "block";
            $("body").css("overflow", "hidden");
            $('.slider-for').slick('slickGoTo', 0);
            $(window).trigger('resize');
            $('#EXPRESS_DELIVERY').attr("checked", false);
            $('#time2').attr('checked', false);
            $('#time1').attr('checked', false);
            var date = new Date();
        }
    }

    // When the user clicks on <span> (x), close the modal
    //            span.onclick = function () {
    $(document).on('click', '.close_css', function () {
        $("body").css("overflow", "auto");
        modal.style.display = "none";
    });
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        //                if (event.target == modal) {
        //                    modal.style.display = "none";
        //                }
    }

    $('.slider-for').slick({
        arrows: false,
        fade: false,
        speed: 300,
        infinite: false,
        draggable: false
    });
    $('a[data-slide]').click(function (e) {
        e.preventDefault();
        var slideno = $(this).data('slide');
        $('.slider-for').slick('slickGoTo', slideno - 1);
    });
    $('a[data-slide]').click(function (e) {
        e.preventDefault();
        var slideno = $(this).data('slide');
        $('.slider-for').slick('slickGoTo', slideno - 1);
    });
    function backToCalendar() {
        var slideno = $(this).data('slide');
        $('.slider-for').slick('slickGoTo', slideno - 1);
    }
</script>
<script type="text/javascript">

    function flower_data_next_page(value) {

        var flower_id = '<?php echo $flower_id; ?>';
        localStorage.setItem("flower_id", flower_id);
        var flower_code = '<?php echo $flower_code; ?>';
        localStorage.setItem("flower_code", flower_code);
        var flower_name = '<?php echo $flower_name; ?>';
        localStorage.setItem("flower_name", flower_name);
        var flower_image = '<?php echo $flower_image; ?>';
        localStorage.setItem("flower_image", flower_image);
        var flower_price = '<?php echo $flower_price; ?>';
        localStorage.setItem("flower_price", flower_price);
        var array = $('#country').val().split(",");
        var flower_city = $.trim(array[1]);
        localStorage.setItem("city", flower_city);
        var flower_pincode = $.trim(array[0]);
        localStorage.setItem("pincode", flower_pincode);
        var flower_delivery_date = delivery_date;
        localStorage.setItem("delivery_date", flower_delivery_date);
        var flower_shipping_method = delivery_shipping_method;
        localStorage.setItem("shipping_method", flower_shipping_method);
        var flower_time_slot = delivery_time;
        localStorage.setItem("time_slot", flower_time_slot);
        var flower_delivery_date_date = delivery_date_date;
        localStorage.setItem("delivery_date_date", flower_delivery_date_date);
        var flower_delivery_date_month = delivery_date_month;
        localStorage.setItem("delivery_date_month", flower_delivery_date_month);
        var flower_delivery_date_day = delivery_date_day;
        localStorage.setItem("delivery_date_day", flower_delivery_date_day);
        //        alert(flower_pincode);
        if (flower_pincode.length != '6') {
            $('#error_message').html('Valid Area / City / PIN required.');
        } else {
            var data = {
                pin_code: flower_pincode,
                city: flower_city
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>verify-zip",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $('#error_message').html(r.message.toString());
                        return false;
                    } else {
                        if (value == 0) {
                            window.location = '<?php echo BASEURL; ?>delivery_address/' + value;
                        } else {
                            window.location = '<?php echo BASEURL; ?>delivery_address/' + value;
                        }
                        return true;
                    }
                }
            });
        }
    }

    $(document).on("click", "#docheckout", function () {
        //        $('#error_message').html('');
        var is_edit_value = $(this).attr('is_edit');
        var country = $('#country').val();
        var date = $('#deliverydateofmonth').html();
        //        alert(date);
        if (country == "") {
            $('#error_message').html('Valid Area / City / PIN required.');
            return false;
        }
        if (date == "") {
            $('#error_message').html('Date is Required.');
            return false;
        }
        if (country != "" && date != "") {
            flower_data_next_page(is_edit_value);
        }
    });</script>

<script type="text/javascript">
    $(document).on('click', '.pro', function () {
        var product_id = $(this).attr('product_id');
        localStorage.setItem('product_id', product_id);
        window.location = '<?php echo BASEURL; ?>flowers_detail/' + product_id + "/0";
    });
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        navText: ["<img src='<?php echo BASEURL_ONEWIMG ?>prev.png'>", "<img src='<?php echo BASEURL_ONEWIMG ?>next.png'>"],
        slideBy: 2,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items: 6
            }
        }
    });
    var today = new Date($.now());
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var compare_time = time.toString("hh:mm:ss");
    if (compare_time > "15:59:59" && compare_time < "23:59:59") {
        $('#input_01').click(function () {
            var $input = $('.datepicker').pickadate({
                formatSubmit: 'yyyy/mm/dd',
                min: +2,
                container: '#container',
                closeOnSelect: true,
                closeOnClear: false,
            });
            var picker = $input.pickadate('picker');
        });
    } else {
        $('#input_01').click(function () {
            var $input = $('.datepicker').pickadate({
                formatSubmit: 'yyyy/mm/dd',
                min: +1,
                container: '#container',
                closeOnSelect: true,
                closeOnClear: false,
            });
            var picker = $input.pickadate('picker');
        });
    }


    // picker.set('select', '14 October, 2014')
    // picker.open()

    // $('button').on('click', function() {
    //     picker.set('disable', true);
    // });

    //  $('.datepicker').datepicker({
    //     startDate: '-3d'
    // })

    $(".imgholder img").click(function (e) {
        temp1 = "#" + ($(this).parent().parent().attr('id'));
        tempvar = $(temp1).find('img:first').attr('src');
        $(temp1).find('img:first').attr("src", this.src);
        this.src = tempvar;
    });</script>
<script src="<?PHP echo BASEURL_OJS ?>jquery.autosuggest.js" type="text/javascript"></script>
<script>
    var countries = [];
<?php
$zip_count = count($zip_data);
for ($i = 0; $i < $zip_count; $i++) {
    $pin_code = $zip_data[$i]['pin_code'];
    $city_name = $zip_data[$i]['city_name'];
    $pin_name = $pin_code . ', ' . $city_name;
    ?>
        countries.push('<?php echo $pin_name; ?>');
    <?php
}
?>
    //    countries = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];
    $("#country").autosuggest({
        sugggestionsArray: countries,
    });
</script>
