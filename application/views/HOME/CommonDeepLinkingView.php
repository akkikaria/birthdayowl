<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <script type="text/javascript" src="<?php echo BASEURL_OJS ?>redirect.js"></script>
        <script type="text/javascript" src="<?PHP echo BASEURL_OJS ?>jquery-1.10.2.min.js"></script>

        <style type="text/css">
            .innerTwoDiv{

                margin-left:20px;
                margin-right: 20px;

            }
            .MainDiv{
                box-shadow: 4px 4px 30px #130507;
                transition: top 800ms ease 0s;
                background: rgb(81, 163, 3) none repeat scroll 0% 0%;
                margin: auto; border-radius: 10px; 
                text-align: center;
                height: 350px; 
                padding: 145px 0px 0px; 
                width: 23%; 

            }
            @media only screen and (max-width: 1000px) {
                .MainDiv{
                    width: 80%;
                    height:593px; 
                }

                .innerTwoDiv{
                    margin-top: 15%;
                    margin-left:40px;
                    margin-right: 40px;

                }
            }
            @media only screen and (max-width:480px){
                .MainDiv{
                    width: 90%;
                }

            }

        </style>

    </head>


    <body style="background: rgba(0, 0, 0, 0) url('../public/owl_assets/images/body-bg.png') repeat scroll 0 0 ">
        <div style="margin:5% auto auto;text-align: center;">
            <a href="<?php echo WEB_HOME; ?>" ><img src="<?PHP echo BASEURL_OIMG ?>logo.png" class="logo " style="width:50%" /></a>
        </div>
        <div class="MainDiv" >
            <div  class="innerTwoDiv">
                <div id="web"style="background-color: white; height: 100px; text-align: center; line-height: 100px; font-weight: bold; border-radius: 8px;cursor: pointer;font-size:30px">Open with web browser</div>
                <div id="app"style="background-color: white; height: 100px; text-align: center; line-height: 100px; font-weight: bold; border-radius: 8px;font-size: 30px;margin-top: 25px;cursor: pointer;">Open with App</div>  
            </div>
        </div>  
        <script type="text/javascript">
            $("#web").click(function () {
                window.location = '<?php echo BASEURL . $url . '/1'; ?>';
            });
            $("#app").click(function () {
                window.location = '<?php echo BASEURL . $url . '/2'; ?>';
            });
        </script>
    </body>
</html>
