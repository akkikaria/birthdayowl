<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <script type="text/javascript" src="<?php echo BASEURL_OJS ?>redirect.js"></script>
        <title></title>
    </head>
    <body>
        <p>Redirecting...</p>
        <script type="text/javascript">
            var qs = AppRedirect.queryString;
<?php if ($from == 1) { ?>
                AppRedirect.redirect({
                    iosApp: "birthdayowl://changepassword?<?php echo $user_id; ?>",
                    iosAppStore: "https://itunes.apple.com/in/app/gatha/id740524911?mt=8",
                    android: {
                        'host': "ChangePassword/<?php echo $user_id; ?>",
                        'scheme': 'birthdayowl', // Scheme part in a custom scheme URL
                        'package': 'com.tech.birthdayowl', // Package name in Play store
                        'fallback': 'https://play.google.com/store/apps/details?id=com.tech.birthdayowl&hl=en'
                    }

                });

<?php } ?>
<?php if ($from == 2) { ?>
                AppRedirect.redirect({
                    iosApp: "birthdayowl://FreeGreetingReceived?<?php echo $fid; ?>/<?php echo $user_id; ?>/<?php echo $display; ?>",
                    iosAppStore: "https://itunes.apple.com/in/app/gatha/id740524911?mt=8",
                    ////echo BASEURL; dashboard
                    android: {
                        'host': "FreeGreetingReceived/<?php echo $fid; ?>/<?php echo $user_id; ?>/<?php echo $display; ?>",
                        'scheme': 'birthdayowl', // Scheme part in a custom scheme URL
                        'package': 'com.tech.birthdayowl', // Package name in Play store
                        'fallback': 'https://play.google.com/store/apps/details?id=com.tech.birthdayowl&hl=en'
                    }
                });

<?php } ?>
<?php if ($from == 3) { ?>

                AppRedirect.redirect({
                    iosApp: "birthdayowl://EgiftReceived?<?php echo $order_pro_id; ?>/<?php echo $user_id; ?>",
                    iosAppStore: "https://itunes.apple.com/in/app/gatha/id740524911?mt=8",
                    // For this, your app need to have category filter: android.intent.category.BROWSABLE
                    android: {
                        'host': "EgiftReceived/<?php echo $order_pro_id; ?>/<?php echo $user_id; ?>",
                        'scheme': 'birthdayowl', // Scheme part in a custom scheme URL
                        'package': 'com.tech.birthdayowl', // Package name in Play store
                        'fallback': 'https://play.google.com/store/apps/details?id=com.tech.birthdayowl&hl=en'
                    }

                });
<?php } ?>

<?php if ($from == 4) { ?>
                AppRedirect.redirect({
                    iosApp: "birthdayowl://ThankYou?<?php echo $fname . "/" . $femail . "/" . $fphone . "/" . $user_id; ?>",
                    iosAppStore: "https://itunes.apple.com/in/app/gatha/id740524911?mt=8",
                    ////echo BASEURL."thankyou/".$user_id."/1"; 
                    android: {
                        'host': 'ThankYou/<?php echo $fname . "/" . $femail . "/" . $fphone . "/" . $user_id; ?>',
                        'scheme': 'birthdayowl', // Scheme part in a custom scheme URL
                        'package': 'com.tech.birthdayowl', // Package name in Play store
                        'fallback': 'https://play.google.com/store/apps/details?id=com.tech.birthdayowl&hl=en'
                    }
                });
<?php } ?>

<?php if ($from == 5) {//for birthday reminder page   ?>
                AppRedirect.redirect({
                    iosApp: "birthdayowl://BirthdayReminder?",
                    iosAppStore: "https://itunes.apple.com/in/app/gatha/id740524911?mt=8",
                    ////echo BASEURL."thankyou/".$user_id."/1"; 
                    android: {
                        'host': 'BirthdayReminder/',
                        'scheme': 'birthdayowl', // Scheme part in a custom scheme URL
                        'package': 'com.tech.birthdayowl', // Package name in Play store
                        'fallback': 'https://play.google.com/store/apps/details?id=com.tech.birthdayowl&hl=en'
                    }
                });
<?php } ?>


        </script>
    </body>
</body>
</html>





