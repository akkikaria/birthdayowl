<?php $this->load->view("new_templates/header"); ?>
<style>
    .commoncss{
        margin-top: 18px; text-align: justify; font-size: 17px; line-height: 25px;font-family:Gotham;
    }
</style>

<div class="wrapper">
    <div class="container_12">
        <div class="inner_midd">
            <div class="col-xs-12 col-sm-8 col-md-9" style="">
                <div class="grid_12">

                    <div class="zodiac-content1">
                        <div class="zodiac-img">
                            <div class="heading_2" >
                                Refund <span>Policy</span>
                            </div>
                        </div>
                        <div style="background-color: white;padding: 5px;font-family:Gotham;font-size: 17px; line-height: 25px;">
                            <?php echo $refund_policy; ?>

                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-4 col-md-3" style="text-align:center;">
                <img src="<?php echo BASEURL_OIMG; ?>add_bday_reminder.png" id="blah" style="padding-left: 0px;" />
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript">

    if ($(window).width() < 768) {
        $("#blah").css({"top": "11px", "padding": "0px"});
    }
    if ($(window).width() > 768) {
        $(function () {

            var $blah = $("#blah"),
                    $window = $(window),
                    offset = $blah.offset();

            $window.scroll(function () {
                if ($window.scrollTop() > offset.top) {
                    $blah.stop().animate({
                        top: 490
                    });
                } else {
                    $blah.stop().animate({
                        top: 0
                    });
                }
            });
        });
    }


</script>
