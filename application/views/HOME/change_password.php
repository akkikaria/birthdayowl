<?php $this->load->view("new_templates/header"); ?>
<style type="text/css">
    @media only screen and (max-width: 768px) {
        .dashbord_nav li a{
            height:50px;
        }   
    }

</style>
<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            Reset <span>Password</span>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <div class="inner_midd dashbord">
            <div class="in_f_l">
                <ul class="leftside dashbord_nav" >
                    <li class="active"><img src="<?php echo BASEURL_OIMG ?>greet_history.png"  class='dash'/><a >RESET PASSWORD</a></li>
                </ul>
            </div>
            <div class="in_f_r">
                <ul class="add_bday_input wrapper">
                    <li>New Password</li>
                    <li>
                        <input type="hidden"  name="user_id" id='user_id' value="<?php echo $user_id ?>" style="width:100%"/>
                        <input type="password"  name="new_password" id='cnew_password' style="width:100%"/>
                        <div class="erroro2" style="color: red;width: 158px; margin-top: 5px;width: 581px" ></div>
                    </li>
                    <li>Confirm Password</li>
                    <li>
                        <input type="password"  id='re_pass' style="width:100%"/>
                        <div class="erroro3" style="color: red;width: 158px; margin-top: 5px;"></div>
                    </li>
                    <li class="add_bday_last">
                        <input type="submit" value="Submit" class="" id="change_password">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#change_password").click(function () {
            var user_id = $("#user_id").val();
            var new_password = $("#cnew_password").val();
            var re_pass = $("#re_pass").val();
            if (new_password == '') {
                $(".erroro2").html("Enter new Password");
            }
            $('#cnew_password').keyup(function (e) {
                $(".erroro2").hide();
            });
            if (re_pass == '') {
                $(".erroro3").html("Confirm Password");
            }
            $('#re_pass').keyup(function (e) {
                $(".erroro3").hide();
            });
            if (new_password != re_pass) {
                $(".erroro3").html("Passwords mismatch");
            } else {
                $(".erroro3").hide();
            }
            if ((new_password != '') && (re_pass != '') && (new_password == re_pass)) {
                var data = {
                    user_id: user_id,
                    new_password: new_password
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>change-password",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {

                            $("#cnew_password").val('');
                            $("#re_pass").val('');
                            alert(r.message.toString());
                            return false;
                        } else {
                            alert(r.message.toString());

                            window.location = "<?php echo WEB_DASHBOARD; ?>";
                        }
                    }
                });
            }
        });
    });


</script>

