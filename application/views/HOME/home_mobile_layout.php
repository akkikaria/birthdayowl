<!-- New Mobile Layout -->

<!-- Mobile Slider -->
<div class="container-fluid">
    <div class="row">
        <img src="<?php echo BASEURL_BPIC ?>mobile-layout-slider.jpg" class="img-responsive" alt="birthday owl">
    </div>
</div>
<!-- Mobile Slider End -->

<!-- Mobile Quick Menu -->
<div class="container-fluid" id="mobile-quick-menu-outer">

    <div class="container">

        <div class="row">
            <a href="<?php echo BASEURL; ?>birthday-reminder">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="mobile-quick-menu">
                        <div class="quick-menu-icon">
                            <img src="<?php echo BASEURL_ONEWIMG ?>input-reminder-icon-mobile.png" alt="Input Reminder" class="img-responsive">
                        </div>
                        <div class="quick-menu-text">
                            <!--<p>Free<br>Birthday Reminders</p>-->
                            <p>Birthday Reminders</p>
                        </div>
                    </div>
                </div></a>

            <a href="<?php echo BASEURL; ?>free-birthday-greeting-cards">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="mobile-quick-menu">
                        <div class="quick-menu-icon">
                            <img src="<?php echo BASEURL_ONEWIMG ?>greeting-card-icon-mobile.png" alt="Input Reminder" class="img-responsive">
                        </div>
                        <div class="quick-menu-text">
                        <!--<p>Free<br>Greeting Cards</p>-->
                            <p>e-Greeting Cards</p>
                        </div>
                    </div>
                </div></a>

            <a href="<?php echo BASEURL; ?>zodiac-signs">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="mobile-quick-menu">
                        <div class="quick-menu-icon">
                            <img src="<?php echo BASEURL_ONEWIMG ?>zodiac-sign-icon-mobile.png" alt="Input Reminder" class="img-responsive">
                        </div>
                        <div class="quick-menu-text">
                            <p>Zodiac Signs</p>
                        </div>
                    </div>
                </div></a>

            <a href="<?php echo BASEURL; ?>egiftcards-vouchers">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="mobile-quick-menu">
                        <div class="quick-menu-icon">
                            <img src="<?php echo BASEURL_ONEWIMG ?>gift-voucher-icon-mobile.png" alt="Input Reminder" class="img-responsive">
                        </div>
                        <div class="quick-menu-text">
                        <!--<p>Paid Greeting Cards</p>-->
			<p>Printed Cards</p>
                        </div>
                    </div>
                </div></a>

            <?php if ($this->session->userdata('userdata') == NULL) { ?>
                <a href="<?php echo BASEURL; ?>birthday-reminder">
                <?php } else { ?>
                    <a href="<?php echo BASEURL; ?>view-birthday-reminder">
                    <?php } ?>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="mobile-quick-menu">
                            <div class="quick-menu-icon">
                                <img src="<?php echo BASEURL_ONEWIMG ?>upcoming-birthday-icon-mobile.png" alt="Input Reminder" class="img-responsive">
                            </div>
                            <div class="quick-menu-text">
                                <p>Upcoming Birthdays</p>
                            </div>
                        </div>
                    </div></a>


                <a href="<?php echo BASEURL; ?>happy-birthday-beautiful-flower-cake">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="mobile-quick-menu">
                            <div class="quick-menu-icon flowers-cake">
                                <img src="<?php echo BASEURL_ONEWIMG ?>flowers-cake-icon-mobile.png" alt="Input Reminder" class="img-responsive">
                            </div>
                            <div class="quick-menu-text">
                                <p>Flowers &amp; Cakes</p>
                            </div>
                        </div>
                    </div></a>

        </div>
    </div>
</div>
<!-- Mobile Quick Menu End -->

<!-- New Mobile Layout End -->

<?php $this->load->view("new_templates/vouchers_footer"); ?>