
<?php $this->load->view("new_templates/header"); ?>
<style>
    .products li i{background-size:100%;padding-bottom:17%;}
</style>
<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            <?php echo $keyword; ?>

        </div>
    </div>
</div>

<div class="wrapper">
    <div class="container_12">
        <div class="inner_midd dashbord">

            <div class="in_f_r" style="background:none;border: 0">
                <?php if ($count > 0) { ?>
                    <div class="prd_heading" style="margin-bottom: 20px;">
                        <span>Buy Most Popular e-Gift Cards...</span>
                        <abbr>Personalized gift vouchers delivered online. </abbr>
                    </div>

                    <ul class="products" id="six_products">     
                        <?php
                        foreach ($voucher_info as $vouchers) {
                            ?>
                            <li style="cursor:pointer; border-right: 0;">
                                <a class="showPopup" voucher_pro_id="<?php echo $vouchers["voucher_pro_id"]; ?>" data-width="60%" >
                                    <span><?php echo $vouchers["voucher_pro_name"]; ?></span>
                                    <i >
                                        <?php
                                        if ($vouchers["product_image"] != '')
                                            $img = $vouchers["product_image"];
                                        else {
                                            $img = BASEURL_OIMG . "unnamed.png";
                                        }
                                        ?>
                                        <img src="<?php echo $img; ?>" alt="" />
                                    </i>
                                    <abbr></abbr>
                                </a>
                            </li>
                            <?php
                        }
                        ?>


                        <?php // foreach ($voucher_info as $vouchers) { ?>

                                        <!--                            <li style="padding: 0px 5px;margin-bottom:4%; border-right: 0; margin-right: 2%;" class="col-md-3 col-xs-6 text-center card showPopup" voucher_pro_id="<?php // echo $vouchers['voucher_pro_id'];           ?>" >
                                                                        <div class="tray">
                        <?php
//                                if ($vouchers["product_image"] != '')
//                                    $img = $vouchers["product_image"];
//                                else {
//                                    $img = BASEURL_OIMG . "unnamed.png";
//                                }
                        ?>
                                                                        <img src="<?php // echo $img;           ?>" class="img-responsive img-mid" width="220" height="150" style="border: 1px solid; border-radius: 5px;">
                                                                        <div style="height: 50px; background-image: url('<?php // echo BASEURL_OIMG . "tray.png";           ?>');background-position: center bottom -5px;background-repeat: no-repeat;">
                                                                             <img src="<?php // echo BASEURL_OIMG . "tray.png";                                                                                                                                                                                                                                                ?>" class="img-responsive img-mid" >
                                                                        </div>
                                                                        </div>
                                                                        <h5><?php // echo $vouchers["voucher_pro_name"];          ?></h5>

                                                                    </li>-->
                        <?php
//                        }
                    } else {
                        ?>
                        <p style="font-weight:bold;font-size: 20px; text-align: center">No Search found </p>
                    <?php } ?>

                </ul>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script>
    $(".clickworks").click(function () {
        $("#hpopup").addClass("hwpopup");
        $('.inner_box').show();
        $('.inner_box').animate({top: '20%'}, 500);
        $('html, body').animate({
            scrollTop: $(".inner_box").offset().top
        }, 2000);
    });
    $(".hwpopup").click(function () {
        $(this).hide();

    });
    $(".hclose").click(function () {
        $(" #hpopup").removeClass("hwpopup");
        $('.inner_box').css("display", "none");
        // $('.inner_box').animate({top: '0'}, 500);

    });



    var islogin = "<?php echo $this->session->userdata('userdata')['user_id'] ?>";

    $("#reminder_banner").click(function () {
        if ((islogin != '') && (isGuest == 0)) {
            window.location = "<?php echo WEB_DASHBOARD; ?>";
        } else {
            $('#reminderpopup').click(); //triggered href using id of a tag in jqeury
        }
    });


    $("#searchval").keypress(function (e) {
        if (e.which == 13) {

            if ($("#searchval").val() != '') {
                window.location = "<?php echo BASEURL; ?>search/" + $("#searchval").val();
            } else {
                alert("Enter search keyword");
            }
        }
    });
    $("#searchanything").click(function () {
        var searchval = $("#searchval").val();
        if (searchval == '') {
            alert("Enter search keyword");
        } else {
            var data = {
                search_name: searchval
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>Home_web/search_by_post",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        var data = '';
                        var voucher_count = r.voucher_info.length;
                        for (i = 0; i < voucher_count; i++) {
                            var proimg = '<?php echo BASEURL_VPRO; ?>' + r.voucher_info[i]['product_image'];
                            data += '<li>'
                                    + '<a class="inline_colorbox showPopup"  style="cursor:pointer"  voucher_pro_id="' + r.voucher_info[i]['voucher_pro_id'] + '" data-width="60%" is_active="">'
                                    + '<span>' + r.voucher_info[i]['voucher_pro_name'] + '</span>'
                                    + '<i><img src =" ' + proimg + '" / ></i>'
                            '<abbr>' + r.voucher_info[i]["pro_tagline"] + '</abbr>'
                                    + '</a>'
                                    + '</li>'

                        }
                        $("#six_products").html(data);
                    }
                }
            });
        }



    });
    $(document).on("click", ".showPopup", function () {
        $("body").css("overflow", "hidden");
        var voucher_pro_id = $(this).attr("voucher_pro_id");

        var data = {
            voucher_pro_id: voucher_pro_id
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>voucher-data",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() == "false") {
                    alert(r.message.toString());
                    return false;
                } else {
                    var voucher_url = r.product_data["voucher_url"];
                    var voucher_pro_name = r.product_data["voucher_pro_name"];
                    var redemption_details = r.product_data["terms_conditions"];
                    var locations = r.product_data["locations"];
                    var vdescription = r.product_data["vdescription"];

                    var product_image;
                    if (r.product_data["product_image"] != "") {
                        product_image = r.product_data["product_image"];
                    } else {
                        product_image = "<?php echo BASEURL_OIMG . "unnamed.png"; ?>";
                    }

                    $(".pro_name").text(voucher_pro_name);
                    $(".minamt").text(" " + r.product_data["min_custom_price"] + " onwards");


                    $("#pro_img").attr("src", product_image);
                    $(".selected_card").attr("voucher_url", voucher_url);
                    var data1 = '';
                    data1 += '<div>' + redemption_details + '</div>';
                    $("#redemption_data").html(data1);
                    var data2 = '';
                    data2 += '<div>' + vdescription + '</div>';
                    $("#location_data").html(data2);

                    $.colorbox({
                        width: "50%",
                        height: "550px",
                        inline: true,
                        href: "#pickcard"
                    });
                    if ($(window).width() < 1280) {

                        $.colorbox({
                            width: "60%",
                            height: "500px",
                            inline: true,
                            href: "#pickcard"
                        });
                    }

                    if ($(window).width() < 480) {
                        $.colorbox({
                            width: "90%",
                            height: "90%",
                            inline: true,
                            href: "#pickcard"
                        });
                    }
                }
            }
        });
    });
    $(".selected_card").click(function () {
        var voucher_url = $(this).attr("voucher_url");
        window.location = "<?php echo BASEURL; ?>voucher/" + voucher_url;
    });



</script>
