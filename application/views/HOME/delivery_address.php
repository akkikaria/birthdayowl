<?php $this->load->view("new_templates/header"); ?>
<?php
//echo '<pre>';
//print_r($this->session->all_userdata());
//print_r($edit_flower_data);
//exit;
?>
<style>
    .owl_image{
        float:right;
        height: 120px;
    }
    .container1{
        width: 80%;
    }
    .pro{
        padding: 0px 5px;
        margin-bottom:4%;
        cursor: pointer;
    }
    .back_tray_img{
        height: 50px;
        background-image: url('<?php echo BASEURL_OIMG . "tray.png"; ?>');
        background-position: center bottom -5px;
        background-repeat: no-repeat;
    }
    .voucher_name{
        /*margin: 0;*/
    }
    .img-mid{
        width: 220px;
        border: 1px solid;
        border-radius: 5px;
        height: 132.8px !important;
    }

    .products li i{background-size:100%;padding-bottom:17%;}
    .lbcon li{
        margin-top: 10px;
    }
    .top-section {
        float: left;
        width: 100%;
        background: #5CAD01;
        border-radius: 5px;
        padding: 10px;
    }
    .pro-name {
        color: #fff;
        font-size: 24px;
        padding-top: 3px;
        text-align: left;
    }
    .delivery-details {
        text-align: center;
        font-size: 20px;
        padding-top: 5px;
        color: #1f586b;
        display: inline-block;
    }
    .delivery-details span {
        color: #fff;
        margin: 0 3px;
    }
    .show-hide {
        border: 1px solid #fff;
        border-radius: 5px;
        color: #fff;
        padding: 8px 5px;
        cursor: pointer;
        transition: all ease-in-out 0.3s;
        width: 110px;
        margin: auto;
    }
    .product-section {
        float: left;
        width: 100%;
        padding: 10px 20px;
    }
    .pro-details1 {
        /*float: left;*/
        /*width: 40%;*/
    }
    .sender-details ul, .pro-details1 ul {
        margin: 0;
        width: 100%;
    }
    .pro-details1 li, .edit-address form li {
        /*float: left;*/
        /*width: 100%;*/
        /*margin-bottom: 20px;*/
        /*position: relative;*/
    }
    .pro-details1 .thumbnails {
        /*float: left;*/
        /*width: 24%;*/
    }
    .pro-details1 {
        /*float: right;*/
        /*width: 72%;*/
        /*text-align: left;*/
        /*line-height: 1;*/
        /*color: #989898;*/
    }
    .pro-price.delivery-detail {
        float: left;
    }
    .pro-price {
        float: right;
        width: 15%;
        text-align: right;
        color: #333;
    }
    /*for removing select arrow in dropdown of phone number*/
    select {
        /* for Firefox */
        -moz-appearance: none;
        /* for Chrome */
        -webkit-appearance: none;
    }
    @media screen and (max-width: 600px) {
        .delivery-details{
            font-size: 15px;
        }
        .product-name{
            font-size: 17px;
        }
        .price_div{
            font-size: 17px;
        }
    }
</style>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo BASEURL_OCSS ?>pstyles.css"/>
<div class="container-fluid product-title">

    <div class="row">
        <div class="col-md-12 col-xs-12 pro-page-title text-center">
            <div class="owl-text">
                <h1>Delivery <span>Details</span></h1>
                <!--<p>Buy Most Popular eGift Cards...</p>-->
            </div>
        </div>
        <!--        <div class="col-md-4 text-right no-padd hidden-xs hidden-sm">
                    <img src="<?php echo BASEURL_ONEWIMG ?>owl.png" class="img-responsive owl_image">
                </div>-->
    </div>
</div>
<div class="container1 container">
    <div class="row">
        <!--<div class="pro-content">-->
        <!--<div class="">-->

        <form>
            <div id="" class="">
                <!--<div style=" min-height: 60px; ">-->
                <div class="top-section col-md-12" style="text-align: center; margin-bottom: 5px;">
                    <!--<div class="pro-name col-md-3 col-xs-12">Gift</div>-->
                    <div class="delivery-details col-md-10 col-xs-12" style="text-align: center;">
                        <span>GIFT: To be delivered between <span id="flower_time_slot"></span></span>on <span id="delivery_day_month_date"></span>
                    </div>
                    <div class="col-md-2 col-xs-12" style="text-align: right;">
                        <div class="show-hide" data-ga-element="ga-element" data-ga-title="View Details" show_hide="0" style="text-align: center; font-weight: bold;">Show Details</div>
                    </div>

                </div>
                <div class="product-section" data-ga-category="MainContent_Gift-1" style="display: none; border: 1px solid #5cad01; margin-bottom: 5px;">
                    <div class="pro-details1 col-md-12">
                        <div>
                            <div class="col-sm-4" style="text-align: center;">
                                <img id="flower_image"  alt="" src="" width="120" height="100">
                            </div>
                            <div class="col-sm-4" style="text-align: center">
                                <h2 class="product-name"></h2>
                            </div>
                            <div class="col-sm-4" style="color:#333; text-align: center;">
                                <h2 class="price_div">
                                    <span class="WebRupee">₹</span>
                                    <span class="product-price"> </span>
                                </h2>
                            </div>
                        </div>
                        <!--                        <ul>
                                                    <li class="remove1" data-productid="EXP3965" data-productprice="499">
                                                        <div class="thumbnails">
                                                            <img id="flower_image"  alt="" src="" width="120" height="100">
                                                        </div>
                                                        <h2 class="product-name" style="margin: 0;">

                                                        </h2>
                                                    </li>
                                                </ul>-->
                    </div>
                    <!--                    <div class="pro-price  delivery-detail">
                                            <ul>
                                                <li class="remove1">
                                                    <h2>
                                                        <span class="WebRupee">₹</span>
                                                        <span class="product-price">499 </span>
                                                    </h2>
                                                    <span>X</span> 1
                                                </li>
                                            </ul>
                                        </div>-->
                    <!--                    <div class="pro-price  delivery-detail addon-action">
                                            <ul>
                                                <li>&nbsp; </li>
                                            </ul>
                                        </div>-->
                    <!--                    <div class="pro-price delete1">
                                            <ul>
                                                <li>
                                                    <a href="javascript:void(0);" tabindex="-1" onclick="deleteItem(0, 'EXP3965')" data-ga-title="Delete">
                                                        <img src="https://i7.fnp.com/assets/images/dustbin-icon.png" alt="delit">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>-->
                </div>
                <!--</div>-->
                <?php // for ($i = 0; $i <= 1; $i++) {   ?>
                <ul class="lbcon wrapper location-div" style="padding: 10px 0;">
                    <h2 id="rec_details" style="margin-left: 10px;">Recipient's Details</h2>
                    <li>
                        <label for="rec_first_name" >Name</label>
                        <input type="text" placeholder="Name *" id="rec_first_name" name="rec_first_name" size="2" />
                        <div class="signerror_rec_first_name" style="color: red; font-weight: bold; height: 10px;"></div>
                    </li>
                    <li>
                        <label for="rec_email" >Email</label>
                        <input type="email" placeholder="Email address *" id="rec_email" name="rec_email" />
                        <div class="signerror_rec_email" style="color: red; font-weight: bold; height: 10px;"></div>
                    </li>
                    <li>
                        <label for="rec_mobile" >Mobile</label>
                        <input type="text" placeholder="Mobile *" id="rec_mobile" name="rec_mobile" />
                        <div class="signerror_rec_mobile" style="color: red; font-weight: bold; height: 10px;"></div>
                    </li>
                    <li>
                        <label for="line1" >Address Line1</label>
                        <input type="text" placeholder="Line1 *" id="line1" name="line1" />
                        <div class="signerror_line1" style="color: red; font-weight: bold; height: 10px;"></div>
                    </li>
                    <li>
                        <label for="line2" >Address Line2</label>
                        <input type="text" placeholder="Line2 " id="line2" name="line2" />
                        <div class="" style="color: red; font-weight: bold; height: 10px;"></div>
                    </li>
                    <li>
                        <label for="city" >City</label>
                        <input type="text" placeholder="City *" id="city" name="city" />
                        <div class="signerror_city" style="color: red; font-weight: bold; height: 10px;"></div>
                    </li>
                    <li>
                        <label for="pin_code" >Pin code</label>
                        <input type="text" placeholder="Pin code *" id="pin_code" name="pin_code" />
                        <div class="signerror_pin_code" style="color: red; font-weight: bold; height: 10px;"></div>
                    </li>
                    <li>
                        <label for="message_on_card" >Message on Card</label>
                        <textarea rows="" name="message_on_card" id="message_on_card"></textarea>
                    </li>


                    <div id="guestLoginDiv" class="wrapper" >
                        <h2 style="margin-left: 10px;">Sender's Details</h2>
                        <div class="guestinnerDiv">
                            <ul class="guestradio">
                                <li>
                                    <input type="radio" id="checkbox3" value="3" name="loginradio" class="gcheckbox" checked=""/>
                                    <label for="checkbox3"  class="glable">Login </label>
                                </li>
                                <li>
                                    <input type="radio" id="guestLogin" value="1" name="loginradio" class="gcheckbox"/>
                                    <label for="guestLogin" class="glable">Continue as Guest</label>
                                </li>
                                <li>
                                    <input type="radio" id="checkbox2" value="2" name="loginradio" class="gcheckbox"/>
                                    <label for="checkbox2"  class="glable">Sign up</label>
                                </li>
                            </ul>

                            <ul class="guestinput"  style="text-align:center;">
                                <li>
                                    <label id="gemail-label" data-icon="&#xf0e0;">
                                        <input type="email" id="gemail"  name="gemail" placeholder="YOUR EMAIL ID" />
                                    </label>
                                </li>
                                <li id="numberBox" data-icon="&#xf10b;" style="display:none;">
                                    <label id="gnumber-label">
                                        <input id="gnumber" type="text" name="gnumber" placeholder="YOUR NUMBER" onkeypress="return isNumberKey(event)" />
                                    </label>
                                </li>
                                <li id="nameBox" style="display:none;">
                                    <label id="gname-label" data-icon="&#xf007;">
                                        <input id="gname" type="text" name="gname" placeholder="YOUR NAME" />
                                    </label>
                                </li>

                                <li id="passwordBox" >
                                    <label id="gname-label" data-icon="&#xf023;">
                                        <input id="gpassword" type="password" name="password" placeholder="PASSWORD" />
                                    </label>
                                </li>
                            </ul>
                            <ul  class="guestinput"  id="socialdiv" >
                                <!--                                <li>
                                                                    <a class="vfacebookLogin" style="cursor: pointer;" red_type="33" >
                                                                        <img src="<?php // echo BASEURL_OIMG . "fb.png"                                                                                                                                                                ?>" />
                                                                    </a>
                                                                </li>-->
                                <li>
                                    <a class="g-signin2 social_signin" onclick="ClickLogin_flowers_cakes()" data-onsuccess="onSignIn" style="cursor: pointer;" red_type="33">
                                    </a>
                                </li>
                            </ul>
                            <ul class="genderBox" style="display:none">
                                <li>Gender :
                                    <label id="gname-label">
                                        <input type="radio" name="gGender" value="1" id="Gender_0 " checked="true" />
                                        Male &nbsp;

                                        <input type="radio" name="gGender" value="2" id="Gender_1 " />
                                        Female
                                    </label>
                                </li>
                            </ul>

                            <ul class="signupinput" style="text-align:center;display: none">
                                <li style="padding-bottom:7px;">
                                    <label id="gname-label" data-icon="&#xf007;"><input id="gfirst_name" type="text" name="gfirst_name" placeholder="YOUR NAME" /></label>
                                </li>
                                <li style="padding-bottom:7px;">
                                    <label id="gname-label" data-icon="&#xf0e0;">
                                        <input type="email" id="gsign_email"  name="gsign_email" placeholder="YOUR EMAIL ID" />
                                    </label>
                                </li>
                                <li style="padding-bottom:7px;" >
                                    <label id="gname-label" data-icon="&#xf023;">
                                        <input id="gsign_password" type="password" name="gsign_password" placeholder="PASSWORD" />
                                    </label>
                                </li>
                                <li style="padding-bottom:7px;" >
                                    <label id="gname-label" data-icon="&#xf023;">
                                        <input id="gconf_password" type="password" name="gconf_password" placeholder="CONFIRM PASSWORD" />
                                    </label>
                                </li>

                                <li style="padding-bottom:7px;">
                                    <div style="width: 100%;display: flex;">
                                        <select id="dobday1" class="gsbdate" name="gsbdate"></select>
                                        <select id="dobmonth1" class="gsbmonth" name="gsbmonth"></select>
                                        <select id="dobyear1" class="gsbyear" name="gsbyear"></select>
                                    </div>
                                    <div style="text-align: left; font-weight: 600;">Note: Enter date of birth above.</div>
                                </li>
                                <li data-icon="&#xf10b;" style="padding-bottom:7px;">
                                    <div style="width: 100%;display: flex;">

                                        <select id="gCmbCountryOption" name="nationality_id" class="countrylist" style="margin-right: 10px;" disabled>
                        			<?php $country = get_country_info(); ?>
                                    <option value="<?php echo $country["country_id"]; ?>"> <?php echo $country['country_name'] . " (+" . $country["nationality_code"] . ")"; ?></option>
                    				</select>
                                        <input type="text" placeholder="Mobile No" id="gmobile_no" name="gmobile_no" onkeypress="return isNumberKey(event)"  class="mbno" />
                                    </div>
                                    <div style="">&nbsp;</div>
                                </li>
                                <li>
                                    <div class="insidesignuperror" style=" height: 20px; font-weight: bold; text-align: center;margin-top: 10px;"></div>
                                </li>

                            </ul>
                            <div id="gEmailValidation" style="font-weight: bold; height: 15px;"></div>
                        </div>
                    </div>
                    <input type="hidden" id="urlaction" value="" redirectval=""/>

                    <input type="hidden" id="localstorage_shipping_method" />
                    <input type="hidden" id="localstorage_flower_name"  />
                    <input type="hidden" id="localstorage_time_slot" />
                    <!--<input type="hidden" id="localstorage_pincode" value="" />-->
                    <input type="hidden" id="localstorage_flower_image"  />
                    <input type="hidden" id="localstorage_flower_code"  />
                    <input type="hidden" id="localstorage_flower_id"  />
                    <!--<input type="hidden" id="localstorage_city" value="" />-->
                    <input type="hidden" id="localstorage_delivery_date"  />
                    <input type="hidden" id="localstorage_flower_price" />
                    <input type="hidden" id="localstorage_delivery_date_date"  />
                    <input type="hidden" id="localstorage_delivery_date_month"  />
                    <input type="hidden" id="localstorage_delivery_date_day"  />

                    <!--                    <li>
                                            <label for="send_first_name" >Sender's Name</label>
                                            <input type="text" placeholder="Name *" id="send_first_name" name="send_first_name" size="2" value=""/>
                                        </li>
                                        <li>
                                            <label for="send_email" >Sender's Email</label>
                                            <input type="text" placeholder="Email address *" id="send_email" name="send_email" value=""/>
                                        </li>
                                        <li>
                                            <label for="send_mobile" >Sender's Mobile</label>
                                            <input type="text" placeholder="Mobile *" id="send_mobile" name="send_mobile" value=""/>
                                        </li>
                    -->
                    <li class="full text-center">

                        <a class="btn btn-primary btn-atc" id="docheckout" style="font-weight: bold; font-size: 18px;">Save & Continue</a>
                        <!--<button id="docheckout" type="button" ></button>-->
                    </li>
                </ul>
                <?php // }   ?>
            </div>
            <!--                    <div class="btn-div">
                                    <a class="btn btn-primary btn-atc" href="#">Add To Cart</a><br>
                                    <a class="btn btn-primary btn-atc" href="<?php // echo BASEURL;                                                                                                                                                                                                                                                       ?>Home_web/delivery_address"  id="docheckout">Buy Now</a>
                                </div>-->
        </form>
        <!--</div>-->

        <!--</div>-->

    </div>

</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js"></script>
<script>
                                            $(document).on('change', '#gCmbCountryOption', function () {
                                                $('#gmobile_no').val('');
                                                var curr_val = $(this).val();
                                                if (curr_val != 102) {
                                                    $('#gmobile_no').attr('readonly', '');
                                                } else {
                                                    $('#gmobile_no').removeAttr('readonly');
                                                }
                                            });
                                            $(document).ready(function () {
                                                $.dobPicker({
                                                    daySelector: '#dobday1', /* Required */
                                                    monthSelector: '#dobmonth1', /* Required */
                                                    yearSelector: '#dobyear1', /* Required */
                                                    //                dayDefault: birth_date, /* Optional */
                                                    //                monthDefault: birth_month, /* Optional */
                                                    //                yearDefault: birth_year, /* Optional */
                                                    minimumAge: 8, /* Optional */
                                                    maximumAge: 60 /* Optional */
                                                });
                                            });
                                            $("#gCmbCountryOption option[value=102]").attr("selected", "selected");
//                                            var today_date_time = '<?php // echo Date('Y-m-d H:i:s');                                                                                                                                                                                                                                                                                                                  ?>';
//                                            var today_date = '<?php // echo Date('Y-m-d');                                                                                                                                                                                                                                                                                                                  ?>';
//                                            var today_time = '<?php // echo Date('H:i:s');                                                                                                                                                                                                                                                                                                                  ?>';
//
//                                            var compare_time = today_time.toString("hh:mm:ss");
//                                            if (compare_time > "15:59:59" && compare_time < "23:59:59") {
//                                                if (new Date(fit_start_time) <= new Date(fit_end_time))
//                                                {//compare end <=, not >=
//                                                    //your code here
//                                                    alert('akash');
//                                                }
//                                            }


//                                            for (var i = 0; i <= localStorage.length; i++) {
////                                                alert(localStorage.key(i) + ": " + localStorage.getItem(localStorage.key(i)));
//                                            }
//                                            $(".showPopup").click(function () {
//                                                $("body").css("overflow", "hidden");
//                                                if (typeof (Storage) != "undefined") {
//                                                    localStorage.setItem("add_another_message", "0");
//                                                }
//                                                var voucher_pro_id = $(this).attr("voucher_pro_id");
//                                                var data = {
//                                                    voucher_pro_id: voucher_pro_id
//                                                }
//                                                $.ajax({
//                                                    type: "POST",
//                                                    url: "<?php // echo BASEURL;                                                                                            ?>voucher-data",
//                                                    data: data,
//                                                    dataType: "json",
//                                                    success: function (r) {
//                                                        if (r.success.toString() == "false") {
//                                                            alert(r.message.toString());
//                                                            return false;
//                                                        } else {
//                                                            var voucher_url = r.product_data["voucher_url"];
//                                                            var voucher_pro_name = r.product_data["voucher_pro_name"];
//                                                            var redemption_details = r.product_data["terms_conditions"];
//                                                            var vdescription = r.product_data["vdescription"];
//                                                            var product_image;
//                                                            if (r.product_data["product_image"] != "") {
//                                                                product_image = r.product_data["product_image"];
//                                                            } else {
//                                                                product_image = "<?php // echo BASEURL_OIMG . "unnamed.png";                                                                                            ?>";
//                                                            }
//                                                            $(".pro_name").text(voucher_pro_name);
//                                                            $(".minamt").text(" " + r.product_data["min_custom_price"] + " onwards");
//                                                            $("#pro_img").attr("src", product_image);
//                                                            $(".selected_card").attr("voucher_url", voucher_url);
//                                                            var data1 = '';
//                                                            data1 += '<div>' + redemption_details + '</div>';
//                                                            $("#redemption_data").html(data1);
//                                                            var data2 = '';
//                                                            data2 += '<div>' + vdescription + '</div>';
//                                                            $("#location_data").html(data2);
//                                                            $.colorbox({
//                                                                width: "50%",
//                                                                height: "550px",
//                                                                inline: true,
//                                                                href: "#pickcard"
//                                                            });
//                                                            if ($(window).width() < 1280) {
//                                                                $.colorbox({
//                                                                    width: "60%",
//                                                                    height: "500px",
//                                                                    inline: true,
//                                                                    href: "#pickcard"
//                                                                });
//                                                            }
//                                                            if ($(window).width() < 480) {
//                                                                $.colorbox({
//                                                                    width: "90%",
//                                                                    height: "90%",
//                                                                    inline: true,
//                                                                    href: "#pickcard"
//                                                                });
//                                                            }
//                                                        }
//                                                    },
//                                                });
//                                            });
</script>
<script type="text/javascript">
    $('input').focus(function () {
        $('.signerror_rec_first_name').html('');
        $('.signerror_rec_email').html('');
        $('.signerror_rec_mobile').html('');
        $('.signerror_line1').html('');
        $('.signerror_city').html('');
        $('.signerror_pin_code').html('');
        $('#gEmailValidation').html('');
        $('.insidesignuperror').html('');
    });

    $('.show-hide').click(function () {
        var show_hide = $(this).attr("show_hide");
        if (show_hide == 0) {
            $(this).attr("show_hide", "1");
        } else {
            $(this).attr("show_hide", "0");
        }

        show_hide = $(this).attr("show_hide");
        if (show_hide == 0) {
            $(this).html('Show Details');
        } else {
            $(this).html('Hide Details');
        }

        $(".product-section").toggle();
    });
    $(document).ready(function () {
//        if ((islogin != "") && (isGuest == 0)) {//Old session concept
        if (islogin != "") {
            $("#guestLoginDiv").hide();
        } else {
            $("#guestLoginDiv").show();
        }

        var shipping_method = localStorage.getItem("shipping_method");
        $('#localstorage_shipping_method').val(shipping_method);

        var flower_name = localStorage.getItem("flower_name");
        $('#localstorage_flower_name').val(flower_name);

        var time_slot = localStorage.getItem("time_slot");
        $('#localstorage_time_slot').val(time_slot);

        var pincode = localStorage.getItem("pincode");
        $('#pin_code').val(pincode);

        var flower_image = localStorage.getItem("flower_image");
        $('#localstorage_flower_image').val(flower_image);

        var flower_code = localStorage.getItem("flower_code");
        $('#localstorage_flower_code').val(flower_code);

//    var product_id = localStorage.getItem("product_id");
        var flower_id = localStorage.getItem("flower_id");
        $('#localstorage_flower_id').val(flower_id);

        var city = localStorage.getItem("city");
        $('#city').val(city);

        var delivery_date = localStorage.getItem("delivery_date");
        $('#localstorage_delivery_date').val(delivery_date);

        var flower_price = localStorage.getItem("flower_price");
        $('#localstorage_flower_price').val(flower_price);

        var delivery_date_date = localStorage.getItem("delivery_date_date");
        $('#localstorage_delivery_date_date').val(delivery_date_date);

        var delivery_date_month = localStorage.getItem("delivery_date_month");
        $('#localstorage_delivery_date_month').val(delivery_date_month);

        var delivery_date_day = localStorage.getItem("delivery_date_day");
        $('#localstorage_delivery_date_day').val(delivery_date_day);

        $('#flower_time_slot').html($('#localstorage_time_slot').val());

        var delivery_day_month_date = $('#localstorage_delivery_date_day').val() + ", " + $('#localstorage_delivery_date_month').val() + " " + $('#localstorage_delivery_date_date').val();
        $('#delivery_day_month_date').html(delivery_day_month_date);

        $('#flower_image').attr('src', '<?php echo BASEURL_PRODUCT_IMG; ?>' + $('#localstorage_flower_image').val());
        $('#flower_image').attr('alt', '<?php echo BASEURL_PRODUCT_IMG; ?>' + $('#localstorage_flower_image').val());

        $('.product-name').html($('#localstorage_flower_name').val());

        $('.product-price').html($('#localstorage_flower_price').val() + '/-<i style="color: red; font-size: 25px;">*</i>');

<?php if (count($edit_flower_data) > 0) { ?>
            var rec_first_name = "<?php echo $edit_flower_data['receiver_name']; ?>";
            var rec_email = "<?php echo $edit_flower_data['receiver_email']; ?>";
            var rec_mobile = "<?php echo $edit_flower_data['receiver_phone']; ?>";
            var line1 = "<?php echo $edit_flower_data['delivery_address_line1']; ?>";
            var line2 = "<?php echo $edit_flower_data['delivery_address_line2']; ?>";
            var message_on_card = "<?php echo $edit_flower_data['message_on_card']; ?>";

            $('#line1').val(line1);
            $('#line2').val(line2);
            $('#rec_first_name').val(rec_first_name);
            $('#rec_email').val(rec_email);
            $('#rec_mobile').val(rec_mobile);
            $('#message_on_card').val(message_on_card);
<?php } ?>
    });

    $("input[name='loginradio']").change(function () {
        $('.ierror').html('');
        $('.showerror').html('');
        var radioValue = $("input[name='loginradio']:checked").val();
        switch (radioValue) {
            case "1": //continue as guest
                {
                    $(".guestinput").show();
                    $("#socialdiv").hide();
                    $("#passwordBox").hide();
                    $("#nameBox").show();
                    $(".signupinput").hide();
                    $(".genderBox").hide();
                    $("#numberBox").show();
                }
                break;
            case "2": //signup
                {
                    $(".guestinput").hide();
                    $(".signupinput").show()
                    $("#socialdiv").hide();
                    $("#passwordBox").hide();
                    $("#nameBox").hide();
                    $(".genderBox").show();
                }
                break;
            case "3": //login
                {
                    $(".guestinput").show();
                    $("#socialdiv").show();
                    $("#passwordBox").show();
                    $("#nameBox").hide();
                    $(".signupinput").hide();
                    $(".genderBox").hide();
                    $("#numberBox").hide();
                }
                break;
        }
    });
    function doLogin(from) {
        var gpassword = $("#gpassword").val();
        var gemail = $("#gemail").val();
        var gname = $("#gname").val();
        var gnumber = $("#gnumber").val();

        if (from == "1") {
            if (gemail == '') {
                $("#gEmailValidation").html("Enter Email Address").addClass("ierror");
            } else if (!(validateEmail(gemail)) && gemail != "") {
                $("#gEmailValidation").html("Invalid Email Address").addClass("ierror");
            } else if (gnumber == '') {
                $("#gEmailValidation").html("Enter Mobile Number").addClass("ierror");
            } else if ((!(checkMobileValid(gnumber))) && (gnumber != '')) {
                $('#gEmailValidation').html("Enter valid Indian Mobile No").addClass("ierror");
            } else if (gname == '') {
                $("#gEmailValidation").html("Enter Name").addClass("ierror");
            } else {
//                update_phone_value_guest = gnumber;
                var data = {
                    email: gemail,
                    first_name: gname,
                    mobile_no: gnumber,
                    login_type: 2, //checkout-2 ,homepage-3,category-4,free_greeting-5
                    is_guest: 1

                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>login/" + from,
                    data: data,
                    dataType: "json", success: function (r) {
                        if (r.success.toString() === "false") {
                            alert(r.message.toString());
                            $('#password').val("");
                            return false;
                        } else {
                            var is_user_exist = r.is_user_exist;
                            var email = r.userdata.email;
                            if (is_user_exist == 1) {
                                $.colorbox({
                                    width: "400px",
                                    height: "320px",
                                    inline: true,
                                    href: $("#Login_birthdayowl_user_flower_cake").show(),
                                    overlayClose: false
                                });
                                $("#lemail_flower_cake").val(email);
//                                send_flower_to_cart(0, 1);
                            } else if (is_user_exist == 2) {
                                if (r.session_guest_id != localStorage.getItem("guest_session"))
                                {
                                    if (typeof (Storage) != "undefined") {
                                        localStorage.setItem("guest_session", r.session_guest_id);
                                    }
                                }
                                send_flower_to_cart(1, r.session_guest_id);
                            } else {
                                is_guest = 1;
                                redirect_value = 10;
                                inserted_user_id = r.userdata['user_id'];
                                is_profile_edit = 0;
//                                if (r.hasOwnProperty('update_guest_phone')) {
//                                    is_update_phone_number_guest = true;
//                                }
                                if (r.session_guest_id != localStorage.getItem("guest_session"))
                                {
                                    if (typeof (Storage) != "undefined") {
                                        localStorage.setItem("guest_session", r.session_guest_id);
                                    }
                                }
                                $('#forgotpopup').click();
                                $("#otpresetDiv").hide();
//                            $("#forgetpassDiv").hide();
//                            $("#verifyMobileDiv").show();
                                $.colorbox({
                                    width: "400px",
                                    height: "320px",
                                    inline: true,
                                    href: $("#verifyMobileDiv").show(),
                                    overlayClose: false
                                });
                                resend_mobile_number = gnumber;
//                            send_flower_to_cart(1, r.session_guest_id);
                            }
                        }
                    }
                });
            }
        } else {
            if (gemail == '') {
                $("#gEmailValidation").html("Enter Email Address").addClass("ierror");
            } else if (gpassword == '') {
                $("#gEmailValidation").html("Enter Password ").addClass("ierror");
            } else if (!(validateEmail(gemail)) && gemail != "") {
                $("#gEmailValidation").html("Invalid Email Address").addClass("ierror");
            } else {
                var data = {
                    email: gemail,
                    password: gpassword,
                    login_type: 2,
                    is_guest: 0
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>login/1",
                    data: data,
                    dataType: "json", success: function (r) {
                        if (r.success.toString() === "false") {
                            alert(r.message.toString());
                            $('#gpassword').val("");
                            return false;
                        } else {
                            $("#flemail").val(gemail);
                            $("#flpass").val(gpassword);
                            send_flower_to_cart(0, 1);
                        }
                    }
                });
            }
        }
    }

    function vset_sessiondata(red_type, from) {
        var tz = jstz.determine(); // Determines the time zone of the browser client
        var timezone = tz.name();
        var data = {
            delivery_shipping_method: $('#localstorage_shipping_method').val(),
            flower_name: $('#localstorage_flower_name').val(),
            delivery_time_slot: $('#localstorage_time_slot').val(),
            delivery_pincode: $('#pin_code').val(),
            flower_image: $('#localstorage_flower_image').val(),
            delivery_date: $('#localstorage_delivery_date').val(),
            flower_code: $('#localstorage_flower_code').val(),
            flower_id: $('#localstorage_flower_id').val(),
            delivery_city: $('#city').val(),
            flower_price: $('#localstorage_flower_price').val(),
            delivery_address_line1: $('#line1').val(),
            delivery_address_line2: $('#line2').val(),
            receiver_name: $('#rec_first_name').val(),
            receiver_email: $('#rec_email').val(),
            receiver_phone: $('#rec_mobile').val(),
            redirect: $("#urlaction").attr("redirectval"),
            timezone: timezone,
            is_social: 1
        }
        var url = "<?php echo BASEURL; ?>Home_web/set_Voucher_sessionData";
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $(".showloader").addClass("show_overlay").show();
            },
            success: function (msg) {
                if (from == "5")
                    window.location = "<?php echo BASEURL; ?>Home_web/social_login_data/3/" + red_type;
                else
                    window.location = "<?php echo BASEURL; ?>Home_web/social_login_data/2/" + red_type;
            },
        });
    }

    function send_flower_to_cart(is_guest, session_id) {

        var tz = jstz.determine(); // Determines the time zone of the browser client
        var timezone = tz.name(); //'Asia/Kolhata' for Indian Time.

//        var delivery_shipping_method = "";
//        var flower_name = "";
//        var delivery_time_slot = "";
//        var delivery_pincode = "";
//        var flower_image = "";
//        var delivery_date = "";
//        var flower_code = "";
//        var flower_id = "";
//        var delivery_city = "";
//        var flower_price = "";
//        var delivery_address_line1 = "";
//        var delivery_address_line2 = "";
//        var receiver_name = "";
//        var receiver_email = "";
//        var receiver_phone = "";
        var data = {
            delivery_shipping_method: $('#localstorage_shipping_method').val(),
            flower_name: $('#localstorage_flower_name').val(),
            delivery_time_slot: $('#localstorage_time_slot').val(),
            message_on_card: $('#message_on_card').val(),
            delivery_pincode: $('#pin_code').val(),
            flower_image: $('#localstorage_flower_image').val(),
            delivery_date: $('#localstorage_delivery_date').val(),
            flower_code: $('#localstorage_flower_code').val(),
            flower_id: $('#localstorage_flower_id').val(),
            delivery_city: $('#city').val(),
            flower_price: $('#localstorage_flower_price').val(),
            delivery_address_line1: $('#line1').val(),
            delivery_address_line2: $('#line2').val(),
            receiver_name: $('#rec_first_name').val(),
            receiver_email: $('#rec_email').val(),
            receiver_phone: $('#rec_mobile').val(),
            redirect: $("#urlaction").attr("redirectval"),
            timezone: timezone,
            is_guest: is_guest,
            guest_session: session_id

        }
        $.ajax({
            url: "<?php echo BASEURL; ?>flower-cart/" + "<?php echo $is_edit; ?>",
            type: "POST",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $(".showloader").addClass("show_overlay").show();
            },
            success: function (msg) {

                $(".showloader").removeClass("show_overlay").hide();
                if (msg.success.toString() == "true") {
                    $(".showloader").removeClass("show_overlay").hide();
                    if (msg.redirect.toString() == "2") {
                        $("#selgreeting").attr("src", $("#largeImage").attr("src"));
                        $(".alertPopupDiv").addClass("show_overlay").show();
                        $('.alertPopupMessage').animate({top: '34%'}, 500);
                        if (typeof (Storage) != "undefined") {
                            localStorage.setItem("add_another_message", "1");
                        }
                        setTimeout(function () {
                            $(".alertPopupDiv").removeClass("show_overlay").hide();
                            $('.alertPopupMessage').animate({top: '0%'}, 500);
                            window.location = "<?php echo WEB_HOME; ?>";
                        }, 2000);
                    } else {
                        window.location = "<?php echo WEB_SHOW_CART; ?>";
                    }
                } else {
                    alert(msg.message.toString());
                }
            },
        });
    }


    function performCheckout(redirect, is_guest) {

        var guest = 0;
//        var delivery_shipping_method = "";
//        var flower_name = "";
//        var delivery_time_slot = "";
        var delivery_pincode = $('#pin_code').val();
//        var flower_image = "";
//        var delivery_date = "";
//        var flower_code = "";
//        var flower_id = "";
        var delivery_city = $('#city').val();
//        var flower_price = "";
        var delivery_address_line1 = $('#line1').val();
//        var delivery_address_line2 = "";
        var receiver_name = $('#rec_first_name').val();
        var receiver_email = $('#rec_email').val();
        var receiver_phone = $('#rec_mobile').val();
        var tz = jstz.determine(); // Determines the time zone of the browser client
        var timezone = tz.name(); //'Asia/Kolhata' for Indian Time.

        if ((receiver_name == '')) {
            $('.signerror_rec_first_name').html("Enter Name").addClass("ierror");
//            $('html, body').animate({
//                scrollTop: $("#rec_details").offset().top
//            }, 500);
        } else if (receiver_phone == '') {
            $('.signerror_rec_mobile').html("Enter Mobile No").addClass("ierror");
//            $('html, body').animate({
//                scrollTop: $("#rec_details").offset().top
//            }, 500);
        } else if ((!(validateEmail(receiver_email))) && (receiver_email != '')) {
            $('.signerror_rec_email').html("Invalid Email Address").addClass("ierror");
//            $('html, body').animate({
//                scrollTop: $("#rec_details").offset().top
//            }, 500);
        } else if ((!(checkMobileValid(receiver_phone))) && (receiver_phone != '')) {
            $('.signerror_rec_mobile').html("Enter valid Indian Mobile No").addClass("ierror");
//            $('html, body').animate({
//                scrollTop: $("#rec_details").offset().top
//            }, 500);
        } else if (delivery_address_line1 == '') {
            $('.signerror_line1').html("Enter Address Line1").addClass("ierror");
//            $('html, body').animate({
//                scrollTop: $("#rec_details").offset().top
//            }, 500);
        } else if (delivery_city == '') {
            $('.signerror_city').html("Enter City").addClass("ierror");
//            $('html, body').animate({
//                scrollTop: $("#rec_details").offset().top
//            }, 500);
        } else if (delivery_pincode == '') {
            $('.signerror_pin_code').html("Enter Pincode").addClass("ierror");
//            $('html, body').animate({
//                scrollTop: $("#rec_details").offset().top
//            }, 500);
        } else {
            if (is_guest == "1") {
                guest = 1;
            } else {
                guest = 0;
            }
            var data = {
                delivery_shipping_method: $('#localstorage_shipping_method').val(),
                flower_name: $('#localstorage_flower_name').val(),
                delivery_time_slot: $('#localstorage_time_slot').val(),
                message_on_card: $('#message_on_card').val(),
                delivery_pincode: $('#pin_code').val(),
                flower_image: $('#localstorage_flower_image').val(),
                delivery_date: $('#localstorage_delivery_date').val(),
                flower_code: $('#localstorage_flower_code').val(),
                flower_id: $('#localstorage_flower_id').val(),
                delivery_city: $('#city').val(),
                flower_price: $('#localstorage_flower_price').val(),
                delivery_address_line1: $('#line1').val(),
                delivery_address_line2: $('#line2').val(),
                receiver_name: $('#rec_first_name').val(),
                receiver_email: $('#rec_email').val(),
                receiver_phone: $('#rec_mobile').val(),
                timezone: timezone,
                redirect: redirect, //Normal checkout
                is_guest: guest
//                                                                order_pro_id: order_pro_id,
//                                                                same_quantity_id: $("#same_quantity_id").val()
            }
            //            if ((islogin != "") && (isGuest == 0)) { //old session concept
            if (islogin != "") {
                $("#guestLoginDiv").hide();
                $.ajax({
                    url: "<?php echo BASEURL; ?>flower-cart/" + "<?php echo $is_edit; ?>",
                    type: "POST",
                    data: data,
                    dataType: "json",
                    beforeSend: function () {
                        $("#inlineDatepicker").hide();
                        $(".showloader").addClass("show_overlay").show();
                    },
                    success: function (msg) {
<?php
//$this->session->set_userdata(array('is_social' => 0));
?>
                        $(".showloader").removeClass("show_overlay").hide();
                        if (msg.success.toString() == "true") {
                            if (msg.redirect.toString() == "1") {
                                window.location = "<?php echo WEB_SHOW_CART; ?>";
                            } else {
                                $("#selgreeting").attr("src", $("#largeImage").attr("src"));
                                $(".alertPopupDiv").addClass("show_overlay").show();
                                $('.alertPopupMessage').animate({top: '34%'}, 500);
                                if (typeof (Storage) != "undefined") {
                                    localStorage.setItem("add_another_message", "1");
                                }
                                setTimeout(function () {
                                    $(".alertPopupDiv").removeClass("show_overlay").hide();
                                    $('.alertPopupMessage').animate({top: '0%'}, 500);
                                    window.location = "<?php echo WEB_HOME; ?>";
                                }, 2000);
                            }
                        } else {
                            alert(msg.message.toString());
                            window.location = "<?php echo WEB_HOME; ?>";
                        }
                    },
                });
            } else {
                $("#guestLoginDiv").show();
                $(".showloader").removeClass("show_overlay").hide();
                $("#urlaction").val("<?php echo BASEURL; ?>flower-cart/" + "<?php echo $is_edit; ?>");
                $("#urlaction").attr("redirectval", redirect);
                switch (is_guest) {
                    case "1":
                        {
                            $("#login_type").val('2');
                            doLogin(1);
                        }
                        break;
                    case "2":
                        {
                            var selectedCategory = '';
                            $('input[name="gGender"]:checked').each(function () {
                                selectedCategory = (this.value);
                            });
                            //Old
                            //                            doSignup($("#gfirst_name").val(), $("#gsign_password").val(), $("#gmobile_no").val(), $("#gsign_email").val(), $("#gconf_password").val(), $("#gsbdate").val(), $("#gsbmonth").val(), $("#gsbyear").val(), selectedCategory, $("#gCmbCountryOption").val(), 10);
                            //New
                            doSignup($("#gfirst_name").val(), $("#gsign_password").val(), $("#gmobile_no").val(), $("#gsign_email").val(), $("#gconf_password").val(), $(".gsbdate").val(), $(".gsbmonth").val(), $(".gsbyear").val(), selectedCategory, $("#gCmbCountryOption").val(), 10);
                        }
                        break;
                    case "3":
                        {
                            doLogin(3);
                        }
                        break;
                    case "4":
                        {
                            var red_type = $(".vfacebookLogin").attr("red_type");
                            vset_sessiondata(red_type, 4);
                        }
                        break;
                    case "5":
                        {
                            var red_type = $(".vgoogleLogin").attr("red_type");
                            vset_sessiondata(red_type, 5);
                        }
                        break;
                }
            }
        }
    }
    $(document).on("click", "#docheckout", function () {
        var radioValue = $("input[name='loginradio']:checked").val();
        performCheckout(1, radioValue);
    });
    $(".vfacebookLogin").click(function () {
        performCheckout(1, "4");
    });
    $(".vgoogleLogin").click(function () {
        performCheckout(1, "5");
    });


    var clicked = false;//Global Variable
    function ClickLogin_flowers_cakes() {
        clicked = true;
    }
    function onSignIn(googleUser) {
        if (clicked) {
            var profile = googleUser.getBasicProfile();
            console.log('ID: ' + profile.getId());
            var user_id = profile.getId();
            console.log('Name: ' + profile.getName());
            var user_name = profile.getName();
            console.log('Image URL: ' + profile.getImageUrl());
            var user_image_url = profile.getImageUrl();
            console.log('Email: ' + profile.getEmail());
            var user_email = profile.getEmail();
            var profile_array = {
                user_id: user_id,
                user_email: user_email,
                user_name: user_name
            }
            update_user_data(profile_array);
        }

    }

    function update_user_data(pro_array)
    {
        $.ajax({
            type: "POST",
            dataType: 'json',
            data: pro_array,
            url: "<?php echo BASEURL; ?>Home_web/jquery_redirect_google_login_flowers_cakes",
            success: function (r) {
                if (r.success.toString() == "false") {
                    alert(r.message.toString());
                    return false;
                } else {
                    islogin = r.islogin;

                    if (r.user_type == '1') {
                        $(".top-left").load(location.href + " .top-left");
                        $("#guestLoginDiv").hide();
//                        window.location = "<?php // echo BASEURL;                                         ?>";
                    }
                    if (r.user_type == '2') {
                        $("#guestLoginDiv").hide();
                    }
                }
            }

        });
    }
</script>
