<?php $this->load->view("new_templates/header"); ?>
<?php
// echo '<pre>';
//print_r($this->session->all_userdata());
?>
<style type="text/css">
    .one_td{
        padding: 10px 10px 5px 0px;
    }
    .sortinfo{
        padding: 60px;
        text-align: justify;
        font-family:"gotham-book";
        font-size:15px;
    }
    .two_td{
        padding: 10px 0px 5px 10px;
        font-weight: bold;
        color: #5cad01;
        font-size: 14px;
        font-family: gotham-book;
    }
    .best_selling,.most_viewed{
        border-bottom: 1px solid; 
        padding: 10px;
    }
    .fix_side tr:last-child {
        border-bottom: 0;
    }
    .product_image{
        float:right; 
        width: 120px;
    }
    .fix_side_headings{
        border-bottom: 1px solid;
    }
    .category_images{
        width: 65px;
    }
    .price_div{
        margin: 10px 0; 
        font-weight: bold; 
        padding: 0 10px;
    }
    .range_display_div{
        margin-right: 10px;
    }
    .val-box{
        border: 0 !important;
    }
    .best_selling_image,.most_viewed_image{
        width: 150px;
    }
    .combo_image{
        width: 275px;
    }
    .fix_div_pro_name{
        padding: 5px 0;
    }
    .right_side_flower_product_image{
        width: 400px;
    }
    .pro,.fix_side_pro{
        cursor: pointer;
    }
    .category{
        cursor: pointer;
    }
    .pro h4{
        font-size: 16px;
        /*margin: 5px 0;*/
    }
    @media only screen and (max-width: 767px){

        .sortinfo
        {
            padding: 10px;
            text-align: justify;
            font-family:"gotham-book";
            font-size:15px;
        }

    }
    @media only screen and (max-width : 1200px) {
        .pro h4{
            font-size: 15px;
        }
        .two_td{
            font-size: 15px;
        }
    }
    @media only screen and (max-width: 480px){
        .two_td{
            font-size: 12.5px;
        }
        .pro h4{
            font-size: 12.5px;
        }
    }
    @media only screen and (max-width: 375px){
        .two_td{
            font-size: 10px;
        }
        .pro h4{
            font-size: 10px;
        }
    }

</style>
<!--<div style="clear: both;"></div>-->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 col-xs-12 no-padd">
            <img src="<?php echo BASEURL_ONEWIMG ?>happy_birthday_flower_cake.jpg" class="img-responsive owl-image-heading" style="width: 100%;" alt="happy birthday flower cake">
        </div>
    </div>
</div>


<div class="container-fluid product-title">
    <div class="row">
        <div class="col-md-12 col-xs-12 pro-page-title text-center">
            <div class="owl-text">    
                <h2 class="owl-text-heading">Flowers &amp; Cakes <span>For Someone Special</span></h2>
            </div>
        </div>
        <!--        <div class="col-md-4 text-right no-padd hidden-xs hidden-sm">
                    <img src="<?php echo BASEURL_ONEWIMG ?>owl.png" class="img-responsive product_image" style="">
                </div>-->
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="col-xs-12 col-sm-4 col-md-3">
        <div class="sidebar-container">

<!--            <table class="fix_side">

                <tr>
                <h3><span class="fix_side_headings widget-title first-child">Categories</span></h3>
                </tr>

                <tr class="flower category" category="Flowers">
                    <td class="one_td">
                        <img src="<?php // echo BASEURL_ONEWIMG           ?>flowers.png" class="img-responsive category_images">
                    </td>
                    <td class="two_td">
                        Flowers
                    </td>
                </tr>
                <tr class="flower_bouquet category" category="Flowers Bouquet">
                    <td class="one_td">
                        <img src="<?php // echo BASEURL_ONEWIMG                                                                                     ?>Flower-Bouquet.png" class="img-responsive category_images">
                    </td>
                    <td class="two_td">
                        Flowers Bouquet
                    </td>
                </tr>
                <tr class="flower_with_cake category" category="Flowers With Cake">
                    <td class="one_td">
                        <img src="<?php // echo BASEURL_ONEWIMG           ?>Flowers-with-cakes.png" class="img-responsive  category_images">
                    </td>
                    <td class="two_td">
                        Flowers with Cakes
                    </td>
                </tr>
            </table>-->
            <table class="fix_side">
                <tr>
                <h3><span class="fix_side_headings widget-title">Price</span></h3>
                </tr>
                <tr>
                <div class="col-xs-12 price_div" style="">
                    <div class="sidebar-widget price-filters rangeslider-widget">
                        <!--                        <div class="sidebar-title">
                                                    <h3>FILTER BY PRICE</h3>
                                                </div>-->
                        <div class="outer-box">
                            <div class="range-slider-price" id="range-slider-price"></div>
                            <div class="form-group clearfix">
                                <div class="pull-left range_display_div">
                                    <span class="left-val">
                                        ₹.<input type="text" class="val-box text-left" id="min-value-rangeslider" readonly></span>
                                    - &nbsp;<span class="right-val">
                                        ₹.<input type="text" class="val-box text-right" id="max-value-rangeslider" readonly></span>
                                </div>
                                <div class="pull-left">
                                    <button type="button" class="theme-btn btn-style-one filter">Filter</button>
                                    <span class="filter_text"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </tr>
            </table>

            <table class="fix_side">
                <tr>
                <h3><span class="fix_side_headings widget-title">Best Selling</span></h3>
                </tr>
                <?php
                if (count($best_selling) > 0) {
                    for ($i = 0; $i < 3; $i++) {
                        $image_name = $best_selling[$i]["product_image"];
                        $product_name = $best_selling[$i]["product_name"];
                        $price = $best_selling[$i]["price"];
                        $product_id = $best_selling[$i]["pro_id"];
                        ?>
                        <tr class="best_selling fix_side_pro" product_id="<?php echo $product_id; ?>">
                            <td class = "one_td">
                                <img alt="<?php echo $product_name; ?>" src = "<?php echo BASEURL_PRODUCT_IMG . $image_name; ?>" class = "img-responsive best_selling_image">
                            </td>
                            <td class= "two_td" style="text-align: center;">
                                <div class="fix_div_pro_name"><?php echo $product_name; ?></div>
                                <div>₹. <?php echo $price; ?></div>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </table>
            <table id="fix_side">
                <tr><h6>&nbsp;</h6></tr>
                <tr>
                <img alt="Beautiful Birthday Bouquets" src="<?php echo BASEURL_ONEWIMG ?>Beautiful_Birthday_Bouquets.png" class="img-responsive combo_image">
                </tr>
            </table>

            <table class="fix_side">
                <tr>
                <h3><span class="fix_side_headings widget-title">Most Viewed</span></h3>
                </tr>
                <?php
                if (count($most_viewed) > 0) {
                    for ($i = 0; $i < 3; $i++) {
                        $image_name = $most_viewed[$i]["product_image"];
                        $product_name = $most_viewed[$i]["product_name"];
                        $price = $most_viewed[$i]["price"];
                        $product_id = $most_viewed[$i]["pro_id"];
                        ?>
                        <tr class="most_viewed fix_side_pro" product_id="<?php echo $product_id; ?>">
                            <td class = "one_td">
                                <img alt="<?php echo $product_name; ?>" src = "<?php echo BASEURL_PRODUCT_IMG . $image_name; ?>" class = "img-responsive most_viewed_image">
                            </td>
                            <td class= "two_td" style="text-align: center;">
                                <div class="fix_div_pro_name"><?php echo $product_name; ?></div>
                                <div>₹. <?php echo $price; ?></div>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </table>
        </div>
    </div>
    <div class="col-xs-12 col-sm-8 col-md-9" style="">
        <div class="row">
            <div class="flower_data" id="flow_data">
            </div>
        </div>
        <!--        <div style="text-align: right;">
                    <a class="btn btn-primary btn-atc" id="load_more" >Load More Products</a>
                </div>-->
    </div>
</div>
<div class="clearfix"></div>
<div class="container-fluid sortinfo">
    <h1 style="font-size: 22px;text-align: center;font-weight: 600;" >Happy Birthday Beautiful Flower Cake</h1>
    <h2 style="font-size: 22px;">Happy Birthday Flower Cake</h2> 
    <p>We all love gifts, don’t we? Sending something thoughtful and useful is what really matters. Imagine receiving a nice <a href="https://www.birthdayowl.com/happy-birthday-beautiful-flower-cake"><b>Happy Birthday Flower And Cake</b></a> on your birthday?</p>

    <p>If you are planning to gift someone, and don’t exactly know how and what to do, then we can show you the way. Firstly, we can remind you of all your birthdays once you input them. Then we provide you with zodiac profile readings of each and every birthday so that you can get an instant insight into their personalities. You would be amazed at what you can learn about someone by simply reading about their zodiac date reading.</p>

    <p>Either ways, we can guarantee you that happy birthday flower and  cake of options would be a good and safe choice. Once you’ve made your choices, simply send them across and have your recipient totally impressed and smiling. All this takes just a few quality minutes and its easy on your wallet too!</p>

    <h2 style="font-size: 22px;">Birthday Flowers & Gifts</h2> 
    <p>Flowers and Gifts and so synonymous with birthdays, aren't they? They make birthdays so special.</p>

    <p>If you are planning to gift someone, and don’t exactly know how and what to do, then we would highly recommend that you send birthday flowers and gifts as this is the most trusted way to make their day special. We also provide you with <a href="https://www.birthdayowl.com/zodiac-signs"><b>Free Zodiac Signs</b></a> profile readings of each and every birthday so that you can get an instant insight into their personalities. You would be amazed at what you can learn about someone by simply reading about their zodiac date reading.</p>

    <p>With all this information, you can choose from a wide range of branded gift cards and other gift options together with some nice options of flowers and cakes too which are the best and safe bets to beat the ambiguity. Once you’ve made your choices, simply send them across and have your recipient totally floored!</p>

    <h2 style="font-size: 22px;">Beautiful Birthday Bouquets</h2> 
    <p>You have been reminded of an upcoming birthday, and are wondering what to do next? Sending something that would lift your recipient's spirit is what really matters. There is nothing better than beautiful birthday bouquets to make a birthday special.</p>

    <p>Choose from our wide range of beautifully crafted floral designs and bouquets of various flower types and color and arrangements. You would be amazed by the choice we have and your choice is guaranteed to have your recipient smiling and happy. After all, who wouldn't love receiving birthday bouquets - be it your friend or mother or sister or cousin or just anyone really. Once you’ve made your choices, simply send them across with a few clicks only!</p>

    <p>We will have your beautiful birthday bouquets delivered to your loved ones at the scheduled time and date.</p>


</div>
<div class="clearfix"></div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript">
//    For price filter
    var min_price_db = '<?php echo $flower_product_min_price['min_price']; ?>';
    var max_price_db = '<?php echo $flower_product_max_price['max_price']; ?>';
    min_price_db = parseInt(min_price_db);
    max_price_db = parseInt(max_price_db);

    var filert_applied = 0;
    var category = "all";
    var offset = 0;
    var limit = 48;
    getFlowerCategory(category, offset);


    $(document).on('click', '.category', function () {
        limit = 48;
        filert_applied = 0;
        offset = 0;
        $('.filter_text').html('');
        category = $(this).attr('category');
        localStorage.setItem('category', category);
        getFlowerCategory(category, offset);

        $('.owl-text-heading').css('text-transform', 'uppercase');
        $('.owl-text-heading').html(category);

        switch (category) {
            case 'Flowers':
                $('.owl-image-heading').attr('src', '<?php echo BASEURL_ONEWIMG; ?>Flowers-banner.png');
                break;
            case 'Flowers Bouquet':
                $('.owl-image-heading').attr('src', '<?php echo BASEURL_ONEWIMG; ?>Flowers-bouquets-banner.png');
                break;
            case 'Flowers With Cake':
                $('.owl-image-heading').attr('src', '<?php echo BASEURL_ONEWIMG; ?>Flowers-with-cakes-banner.png');
                break;
            default:
                $('.owl-image-heading').attr('src', '<?php echo BASEURL_ONEWIMG ?>banner.jpg');
        }



        var priceRange = document.getElementById('range-slider-price');
        priceRange.noUiSlider.set([min_price_db, max_price_db]);
    });

    function getFlowerCategory(category, offset) {
        var data = {
            category_name: category,
            limit: limit,
            offset: offset
        };
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>flower-category",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $("#inlineDatepicker").hide();
                $(".showloader").addClass("show_overlay").show();
            },
            success: function (r) {
                $(".showloader").removeClass("show_overlay").hide();
                if (r.success.toString() === "false") {
                    $("#message").html(r.message.toString());
                    return false;
                } else {
                    if (r.count > 47) {
                        $('#load_more').show();
                    }
                    var flower_data = "";
                    var pages_data = "";
                    var flower_length = r.flower_product.length;
//                    if (flower_length > 0) {
//                        pages_data += '<div class = "wrapper defaults" id= "content" style = "background-color: red;" >'
//                                + '<div class="holder text-center" style="cursor:pointer;"></div>'
//                                + '</div>';
//                    }
                    if (flower_length != 0) {
                        for (var i = 0; i < flower_length; i++) {
                            var product_id = r.flower_product[i]['pro_id'];
                            var product_name = r.flower_product[i]['product_name'];
                            var image_name = r.flower_product[i]['product_image'];
                            var price = r.flower_product[i]['price'];

                            flower_data += '<div id="" category="" class="col-md-3 col-sm-4 col-xs-6 pro" product_id="' + product_id + '">'
                                    + '<div class="flowercake-product">'
                                    + ' <img alt="' + product_name + '" src="<?php echo BASEURL_PRODUCT_IMG; ?>' + image_name + '" class="img-responsive right_side_flower_product_image">'
                                    + '<div class="content_height">'
                                    + '<h4>' + product_name + '</h4>'
                                    + '<h4>₹. ' + price + '</h4>'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>';

                        }

                    } else {
                        flower_data += '<div class="col-md-3 col-xs-6 pro" product_id="' + product_id + '">No Products'
                                + '</div>';
                    }

                    $('.flower_data').html(flower_data);

                }
                var maxHeight = Math.max.apply(null, $(".content_height").map(function ()
                {
                    return $(this).height();
                }).get());
                $('.content_height').css('height', maxHeight + 'px');
                //                alert(maxHeight);
            }
        });
    }

    function getFlowerCategory_append(category, offset) {
        limit = 24;
        var data = {
            category_name: category,
            limit: limit,
            offset: offset
        };
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>flower-category",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {
                    $("#message").html(r.message.toString());
                    $('#load_more').hide();
                    return false;
                } else {
                    if (r.count > 23) {
                        $('#load_more').show();
                    } else {
                        $('#load_more').hide();
                    }
                    var flower_data = "";
                    var flower_length = r.flower_product.length;
//                    alert(flower_length);
                    if (flower_length != 0) {
                        for (var i = 0; i < flower_length; i++) {
                            var product_id = r.flower_product[i]['pro_id'];
                            var product_name = r.flower_product[i]['product_name'];
                            var image_name = r.flower_product[i]['product_image'];
                            var price = r.flower_product[i]['price'];

                            flower_data += '<div class="col-md-3 col-sm-4 col-xs-6 pro" product_id="' + product_id + '">'
                                    + '<div class="flowercake-product">'
                                    + ' <img alt="' + product_name + '" src="<?php echo BASEURL_PRODUCT_IMG; ?>' + image_name + '" class="img-responsive right_side_flower_product_image">'
                                    + '<div class="content_height">'
                                    + ' <h4>' + product_name + '</h4>'
                                    + ' <h4>₹. ' + price + '</h4>'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>';

                        }

                    } else {
                        flower_data += '<div class="col-md-3 col-xs-6 pro" product_id="' + product_id + '">No Products'
                                + '</div>';
                    }

                    $('.flower_data').append(flower_data);

                }
                var maxHeight = Math.max.apply(null, $(".content_height").map(function ()
                {
                    return $(this).height();
                }).get());
                $('.content_height').css('height', maxHeight + 'px');
                //                alert(maxHeight);
            }
        });
    }

    //Price Range Slider
    if ($('.range-slider-price').length) {
        var priceRange = document.getElementById('range-slider-price');
        noUiSlider.create(priceRange, {
            start: [min_price_db, max_price_db],
            limit: max_price_db,
            behaviour: 'drag',
            connect: true,
            range: {
                'min': min_price_db,
                'max': max_price_db
            }
        });
        var limitFieldMin = document.getElementById('min-value-rangeslider');
        var limitFieldMax = document.getElementById('max-value-rangeslider');

        priceRange.noUiSlider.on('update', function (values, handle) {
            (handle ? limitFieldMax : limitFieldMin).value = values[handle];
        });
    }
    function getFlowerPrice(category, offset) {
        var limitFieldMin = $('#min-value-rangeslider').val();
        var limitFieldMax = $('#max-value-rangeslider').val();
        var flower_data = "";
        var data = {
            category_name: category,
            min_value: limitFieldMin,
            max_value: limitFieldMax,
            limit: limit,
            offset: offset
        };
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>flower-price",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $("#inlineDatepicker").hide();
                $(".showloader").addClass("show_overlay").show();
            },
            success: function (r) {
                $(".showloader").removeClass("show_overlay").hide();
                if (r.success.toString() === "false") {
                    flower_data += '<div class="col-md-3 col-xs-6 pro" product_id="">No Products Found</div>';
                    $('.flower_data').html(flower_data);
                    return false;
                } else {
                    if (r.count > 47) {
                        $('#load_more').show();
                    } else {
                        $('#load_more').hide();
                    }
                    var flower_length = r.flower_product.length;
                    if (flower_length != 0) {
                        for (var i = 0; i < flower_length; i++) {
                            var product_id = r.flower_product[i]['pro_id'];
                            var product_name = r.flower_product[i]['product_name'];
                            var image_name = r.flower_product[i]['product_image'];
                            var price = r.flower_product[i]['price'];

                            flower_data += '<div class="col-md-3 col-xs-6 pro" product_id="' + product_id + '">'
                                    + '<div class="flowercake-product">'
                                    + ' <img alt="' + product_name + '" src="<?php echo BASEURL_PRODUCT_IMG; ?>' + image_name + '" class="img-responsive right_side_flower_product_image">'
                                    + '<div class="content_height">'
                                    + ' <h4>' + product_name + '</h4>'
                                    + ' <h4>₹. ' + price + '</h4>'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>';

                        }

                    } else {
                        flower_data += '<div class="col-md-3 col-xs-6 pro" product_id="">No Products'
                                + '</div>';
                    }
                    $('.flower_data').html(flower_data);
                }
                var maxHeight = Math.max.apply(null, $(".content_height").map(function ()
                {
                    return $(this).height();
                }).get());
                $('.content_height').css('height', maxHeight + 'px');
                //                alert(maxHeight);
            }
        });
    }
    function getFlowerPrice_append(category, offset) {
        limit = 24;
        var limitFieldMin = $('#min-value-rangeslider').val();
        var limitFieldMax = $('#max-value-rangeslider').val();
        var flower_data = "";
        var data = {
            category_name: category,
            min_value: limitFieldMin,
            max_value: limitFieldMax,
            limit: limit,
            offset: offset
        };
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>flower-price",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {
                    $('#load_more').hide();
                    return false;
                } else {
                    if (r.count > 23) {
                        $('#load_more').show();
                    } else {
                        $('#load_more').hide();
                    }
                    var flower_length = r.flower_product.length;
                    if (flower_length != 0) {
                        for (var i = 0; i < flower_length; i++) {
                            var product_id = r.flower_product[i]['pro_id'];
                            var product_name = r.flower_product[i]['product_name'];
                            var image_name = r.flower_product[i]['product_image'];
                            var price = r.flower_product[i]['price'];

                            flower_data += '<div class="col-md-3 col-xs-6 pro" product_id="' + product_id + '">'
                                    + '<div class="flowercake-product">'
                                    + ' <img alt="' + product_name + '" src="<?php echo BASEURL_PRODUCT_IMG; ?>' + image_name + '" class="img-responsive right_side_flower_product_image">'
                                    + '<div class="content_height">'
                                    + ' <h4>' + product_name + '</h4>'
                                    + ' <h4>₹. ' + price + '</h4>'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>';

                        }

                    } else {
                        flower_data += '<div class="col-md-3 col-xs-6 pro" product_id="">No Products'
                                + '</div>';
                    }
                    $('.flower_data').append(flower_data);
                }
                var maxHeight = Math.max.apply(null, $(".content_height").map(function ()
                {
                    return $(this).height();
                }).get());
                $('.content_height').css('height', maxHeight + 'px');
                //                alert(maxHeight);

            }
        });
    }
    $(document).on('click', '.filter', function () {
        limit = 48;
        offset = 0;
        filert_applied = 1;
        $('.filter_text').html('Filter Applied');
        getFlowerPrice(category, offset);
    });


//    $(document).ready(function () {
//        var win = $(window);
//        var doc = $(document);
////        alert(doc.height());//3119
////        alert(win.height());//943
////        alert(win.scrollTop());
////        var abcd = win.height() + $('.footer').height();
//        // Each time the user scrolls
//        win.scroll(function () {
//            // Vertical end reached?
//            if ((doc.height() - win.height()) === win.scrollTop()) {
////            if ($(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.7) {
////                alert('akki');
//                offset = offset + limit;
//                if (filert_applied == 1) {
//                    getFlowerPrice_append(category, offset);
//                } else {
//                    getFlowerCategory_append(category, offset);
//                }
//            }
//        });
//    });

    $(document).on('click', '#load_more', function () {
        offset = offset + limit;
        if (filert_applied == 1) {
            getFlowerPrice_append(category, offset);
        } else {
            getFlowerCategory_append(category, offset);
        }
    });
    $(document).on('click', '.pro,.fix_side_pro', function () {
        var product_id = $(this).attr('product_id');
        localStorage.setItem('product_id', product_id);
        window.location = '<?php echo BASEURL; ?>flowers_detail/' + product_id + "/0";
    });
</script>

