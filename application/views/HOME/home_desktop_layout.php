<!-- Desktop Version -->
<!-- Slider Section -->
<div class="container-fluid">
    <!--<div class="slider-section">
        <div class="row">
            <div class="owl-carousel owl-banner owl-theme">
    <?php for ($i = 0; $i < count($banners); $i++) { ?>
                                                <div class="item"><img src="<?php echo BASEURL_BPIC . $banners[$i]["banner_image"]; ?>" class="img-responsive" alt="<?php echo $banners[$i]["banner_image_name"]; ?>"></div>
    <?php } ?>
            </div>
        </div>
    </div>-->
    <div class="container">
        <div class="row">
            <div class="slider-outer-box">
                <div class="row">
                    <div class="col-sm-8 col-md-8 slider-img-box">
                        <img src="<?php echo BASEURL_ONEWIMG ?>desktop-home-slider-image.jpg" style="width:100%;" class="img-responsive owl-image-heading" alt="happy birthday flower cake">
                    </div>
                    <div class="col-sm-4 col-md-4 slider-left-menu">
                        <div id="mobile-quick-menu-outer">

                            <div class="container">

                                <div class="row">
                                    <a href="https://www.birthdayowl.com/birthday-reminder">
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="mobile-quick-menu">
                                                <div class="quick-menu-icon">
                                                    <img src="https://www.birthdayowl.com/public/owl_assets/images/new_assets/input-reminder-icon-mobile.png" alt="Input Reminder" class="img-responsive">
                                                </div>
                                                <div class="quick-menu-text">
                                                    <p>Free<br>Birthday Reminders</p>
                                                </div>
                                            </div>
                                        </div></a>

                                    <a href="https://www.birthdayowl.com/free-birthday-greeting-cards">
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="mobile-quick-menu">
                                                <div class="quick-menu-icon">
                                                    <img src="https://www.birthdayowl.com/public/owl_assets/images/new_assets/greeting-card-icon-mobile.png" alt="Input Reminder" class="img-responsive">
                                                </div>
                                                <div class="quick-menu-text">
                                                    <p>Free<br>Greeting Cards</p>
                                                </div>
                                            </div>
                                        </div></a>

                                    <a href="https://www.birthdayowl.com/zodiac-signs">
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="mobile-quick-menu">
                                                <div class="quick-menu-icon">
                                                    <img src="https://www.birthdayowl.com/public/owl_assets/images/new_assets/zodiac-sign-icon-mobile.png" alt="Input Reminder" class="img-responsive">
                                                </div>
                                                <div class="quick-menu-text">
                                                    <p>Zodiac Signs</p>
                                                </div>
                                            </div>
                                        </div></a>

                                    <a href="https://www.birthdayowl.com/egiftcards-vouchers">
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="mobile-quick-menu">
                                                <div class="quick-menu-icon">
                                                    <img src="https://www.birthdayowl.com/public/owl_assets/images/new_assets/gift-voucher-icon-mobile.png" alt="Input Reminder" class="img-responsive">
                                                </div>
                                                <div class="quick-menu-text">
                                                    <p>Printed Cards</p>
                                                </div>
                                            </div>
                                        </div></a>

                                    <a href="https://www.birthdayowl.com/birthday-reminder">
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="mobile-quick-menu">
                                                <div class="quick-menu-icon">
                                                    <img src="https://www.birthdayowl.com/public/owl_assets/images/new_assets/upcoming-birthday-icon-mobile.png" alt="Input Reminder" class="img-responsive">
                                                </div>
                                                <div class="quick-menu-text">
                                                    <p>Upcoming Birthdays</p>
                                                </div>
                                            </div>
                                        </div></a>


                                    <a href="https://www.birthdayowl.com/happy-birthday-beautiful-flower-cake">
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="mobile-quick-menu">
                                                <div class="quick-menu-icon flowers-cake">
                                                    <img src="https://www.birthdayowl.com/public/owl_assets/images/new_assets/flowers-cake-icon-mobile.png" alt="Input Reminder" class="img-responsive">
                                                </div>
                                                <div class="quick-menu-text">
                                                    <p>Flowers &amp; Cakes</p>
                                                </div>
                                            </div>
                                        </div></a>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Slider Section -->

<!-- Featured Product -->
<div class="container-fluid">

    <div class="container product-display-section">

        <div class="row">
            <div class="product-res-menu"></div>
            <ul class="nav nav-tabs nav-justified tabs-menu" id="myTabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#flowers" role="tab" id="flowers-tab" data-toggle="tab" aria-controls="flowers">
                        <img src="<?php echo BASEURL_ONEWIMG ?>flowers-&-cake-icon.png" class="hidden-sm hidden-md hidden-lg" alt="Flowers &amp; Cakes">Flowers &amp; Cakes
                    </a>
                </li>
                <li role="presentation">
                    <a href="#egift" id="egift-tab" role="tab" data-toggle="tab" aria-controls="egift">
<!--                        <img src="<?php echo BASEURL_ONEWIMG ?>gift-card-icon.png" class="hidden-sm hidden-md hidden-lg" alt="e-Gift Cards">e-Gift Cards-->
                        <img src="<?php echo BASEURL_ONEWIMG ?>gift-card-icon.png" class="hidden-sm hidden-md hidden-lg" alt="e-Gift Cards">Printed Cards
                    </a>
                </li>
                <li role="presentation">
                    <a href="#greetingcard" role="tab" id="greetingcard-tab" data-toggle="tab" aria-controls="greetingcard"  aria-expanded="true">
                        <img src="<?php echo BASEURL_ONEWIMG ?>greeting-card-icon.png" class="hidden-sm hidden-md hidden-lg" alt="Free Greeting Card">Free Greeting Cards
                    </a>
                </li>
                <li role="presentation">
                    <a href="#zodiac" role="tab" id="zodiac-tab" data-toggle="tab" aria-controls="zodiac">
                        <img src="<?php echo BASEURL_ONEWIMG ?>zodiac-icon.png" class="hidden-sm hidden-md hidden-lg" alt="Free Zodiac Signs">Free Zodiac Signs
                    </a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent"> 
                <div class="tab-pane fade" role="tabpanel" id="egift" aria-labelledby="egift-tab"> 
                    <div class="owl-carousel owl-theme our-product egift">
                        <?php
                        if (count($product_voucher) > 0) {
                            for ($i = 0; $i < 12; $i++) {
                                if ($product_voucher[$i]["product_image"] != '')
                                    $img = $product_voucher[$i]["product_image"];
                                else {
                                    $img = BASEURL_OIMG . "unnamed.png";
                                }
                                ?>
                                <div class="item showPopup" voucher_pro_id="<?php echo $product_voucher[$i]["voucher_pro_id"]; ?>">
                                    <a data-width="70%" >
                                        <img src="<?php echo $img; ?>" class="img-responsive product-img" alt="<?php echo $product_voucher[$i]["voucher_pro_name"]; ?>">
                                        <img src="<?php echo BASEURL_ONEWIMG ?>tray.png" class="img-responsive product-tray" alt="tray" />
                                        <span><?php echo $product_voucher[$i]["voucher_pro_name"]; ?></span>
                                        <span class="clearfix"></span>
                                    </a>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <li><div class="item">No Products</div></li>
                            <!--<img src="<?php echo BASEURL; ?>/public/coming_soon/background.jpg" class="img-responsive product-tray" alt="tray" />-->
                        <?php } ?>
                    </div>

                    <div class="btn-right text-right"><a class="btn btn-primary btn-atc-home text-right egiftcards" >View All</a></div>	

                </div> 
                <div  class="tab-pane fade" role="tabpanel" id="greetingcard" aria-labelledby="greetingcard-tab"> 
                    <div class="owl-carousel owl-theme our-product greetingscard">
                        <?php
                        for ($i = 0; $i < 18; $i++) {
                            ?>
                            <div class="item grg_selection">
                                <a card_id="<?php echo $greeting_cards[$i]["card_id"]; ?>" class="greeting_card grg_selection" style="cursor:pointer;">
                                    <img alt="<?php echo $greeting_cards[$i]["card_name"]; ?>" src="<?php echo BASEURL_CROPPED_GREET . $greeting_cards[$i]["front_page_image"]; ?>"  />
                                    <span >QUICK LOOK</span>
                                </a> 
                            </div>
                        <?php } ?>
                    </div>
                    <div class="btn-right text-right"><a class="btn btn-primary btn-atc-home text-right free_greetings" >View All</a></div>	
                </div> 
                <div class="tab-pane fade in active" role="tabpanel" id="flowers" aria-labelledby="flowers-tab"> 
                    <div class="owl-carousel owl-theme our-product flowers">
                        <?php
                        for ($i = 0; $i < count($flower_product); $i++) {
                            $product_id = $flower_product[$i]["pro_id"];
                            $image_name = $flower_product[$i]["product_image"];
                            $product_name = $flower_product[$i]["product_name"];
                            $price = $flower_product[$i]["price"];
                            ?>
                            <div class="item pro" product_id="<?php echo $product_id; ?>" style="cursor: pointer;">
                                <a>
                                    <img alt="<?php echo $product_name; ?>" src="<?php echo BASEURL_PRODUCT_IMG . $image_name; ?>" />
                                    <span><?php echo $product_name; ?></span>
                                    <span>₹. <?php echo $price; ?></span>
                            <!--<span>Cake</span>-->
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="btn-right text-right"><a class="btn btn-primary btn-atc-home text-right flowers_cakes" >View All</a></div>	
                </div> 
                <div class="tab-pane fade" role="tabpanel" id="zodiac" aria-labelledby="zodiac-tab"> 
                    <div class="owl-carousel owl-theme our-product zodiac">

                        <div class="item zodiac_sign_click" zodiac_id="1" style="cursor:pointer;">
                            <a>
                                <img src="<?php echo BASEURL_IMG; ?>zodiac/Aries.png" style="height: 189px;" class="content_height" alt="Aries Zodiac" />
                                <div style="margin:15px;">
                                    <span>Aries</span>
                                    <span>(Mar 21 - Apr 20)</span>
                                </div>
                            </a>
                        </div>
                        <div class="item zodiac_sign_click" zodiac_id="2" style="cursor:pointer;">
                            <a>
                                <img src="<?php echo BASEURL_IMG; ?>zodiac/taurus.png" class="content_height" alt="Taurus Zodiac" />
                                <div style="margin:15px;">
                                    <span>Taurus</span>
                                    <span>(Apr 20 - May 20)</span>
                                </div>
                            </a>
                        </div>
                        <div class="item zodiac_sign_click" zodiac_id="3" style="cursor:pointer;">
                            <a>
                                <img src="<?php echo BASEURL_IMG; ?>zodiac/gemini.png" class="content_height" alt="Gemini"/>
                                <div style="margin:15px;">
                                    <span>Gemini</span>
                                    <span>(May 21 - June 20)</span>
                                </div>
                            </a>
                        </div>
                        <div class="item zodiac_sign_click" zodiac_id="4" style="cursor:pointer;">
                            <a>
                                <img src="<?php echo BASEURL_IMG; ?>zodiac/cancer.png" class="content_height" alt="Zodiac For Cancer"/>
                                <div style="margin:15px;">
                                    <span>Cancer</span>
                                    <span>(June 21 - July 22)</span>
                                </div>
                            </a>
                        </div>
                        <div class="item zodiac_sign_click" zodiac_id="5" style="cursor:pointer;">
                            <a>
                                <img src="<?php echo BASEURL_IMG; ?>zodiac/leo.png" class="content_height" alt="Leo"/>
                                <div style="margin:15px;">
                                    <span>Leo</span>
                                    <span>(July 23 - Aug 22)</span>
                                </div>
                            </a>
                        </div>
                        <div class="item zodiac_sign_click" zodiac_id="6" style="cursor:pointer;">
                            <a>
                                <img src="<?php echo BASEURL_IMG; ?>zodiac/virgo.png" class="content_height" alt="Virgo"/>
                                <div style="margin:15px;">
                                    <span>Virgo</span>
                                    <span>(Aug 23 - Sept 22)</span>
                                </div>
                            </a>
                        </div>
                        <div class="item zodiac_sign_click" zodiac_id="7" style="cursor:pointer;">
                            <a>
                                <img src="<?php echo BASEURL_IMG; ?>zodiac/libra.png" class="content_height" alt="Libra"/>
                                <div style="margin:15px;">
                                    <span>Libra</span>
                                    <span>(Sept 23 - Oct 22)</span>
                                </div>
                            </a>
                        </div>
                        <div class="item zodiac_sign_click" zodiac_id="8" style="cursor:pointer;">
                            <a>
                                <img src="<?php echo BASEURL_IMG; ?>zodiac/scorpio.png" class="content_height" alt="Scorpio"/>
                                <div style="margin:15px;">
                                    <span>Scorpio</span>
                                    <span>(Oct 23 - Nov 21)</span>
                                </div>
                            </a>
                        </div>
                        <div class="item zodiac_sign_click" zodiac_id="9" style="cursor:pointer;">
                            <a>
                                <img src="<?php echo BASEURL_IMG; ?>zodiac/sagittarius.png" class="content_height" alt="Sagittarius"/>
                                <div style="margin:15px;">
                                    <span>Sagittarius</span>
                                    <span>(Nov 22 - Dec 21)</span>
                                </div>
                            </a>
                        </div>
                        <div class="item zodiac_sign_click" zodiac_id="10" style="cursor:pointer;">
                            <a>
                                <img src="<?php echo BASEURL_IMG; ?>zodiac/capricorn.png" class="content_height" alt="Capricorn"  />
                                <div style="margin:15px;">
                                    <span>Capricorn</span>
                                    <span>(Dec 22 - Jan 21)</span>
                                </div>
                            </a>
                        </div>
                        <div class="item zodiac_sign_click" zodiac_id="11" style="cursor:pointer;">
                            <a>
                                <img src="<?php echo BASEURL_IMG; ?>zodiac/aquarius.png" class="content_height" />
                                <div style="margin:15px;" alt="Aquarius">
                                    <span>Aquarius</span>
                                    <span>(Jan 20 - Feb 18)</span>
                                </div>
                            </a>
                        </div>
                        <div class="item zodiac_sign_click" zodiac_id="12" style="cursor:pointer;">
                            <a>
                                <img src="<?php echo BASEURL_IMG; ?>zodiac/pisces.png" class="content_height" alt="Pisces"/>
                                <div style="margin:15px;">
                                    <span>Pisces</span>
                                    <span>(Feb 19 - Mar 20)</span>
                                </div>
                            </a>
                        </div>

                    </div>
                    <div class="btn-right text-right"><a class="btn btn-primary btn-atc-home text-right zodaic_sign" >View All</a></div>	
                </div> 
            </div>
        </div>

    </div>

</div>
<!-- Featured Product -->
<div class="clearfix"></div>

<!-- Birthday Reminder -->
<div class="container-fluid birthday-reminder-sec" style="background: transparent;">

    <div class="container">

        <div class="row">

            <img src="<?PHP echo BASEURL_ONEWIMG ?>/birthday-reminder-new-layout.png" class="img-responsive" width="100%" alt="birthday reminder">

        </div>

    </div>

</div>
<!-- Birthday Reminder End -->


<!-- Home 4 boxes Section -->
<div class="container-fluid">
    <div class="container">
        <div class="row">

            <div class="flex-boxes-container">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="video">
                            <!--<img src="<?php //echo BASEURL_ONEWIMG        ?>video-outlet.png" class="img-responsive" alt="video">-->
                        <video width="650" height="415" controls poster="<?php echo BASEURL_ONEWIMG ?>video-outlet.png" onclick="this.play();">
                            <source src="<?php echo BASEURL_VIDEO ?>BirthdayBoy.mp4" type="video/mp4">
                            Your browser does not support HTML5 video.
                        </video>
                    </div>


                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="testimonial-back">
                        <div class="owl-testimonial square owl-carousel owl-theme">

                            <div class="item testi">
                                <p>Really pleased to discover the website and app! Its so quick and convenient to use with so many cool options. And its free too!</p>
                                <h5>- Macy Smith, New York, USA</h5>
                            </div>
                            <div class="item testi">
                                <p>I received an awesome greeting card from my friend and it included a selfie video of my friend singing happy birthday for me! So very cool.</p>
                                <h5>- Rubina Khan, Dubai, UAE</h5>
                            </div>
                            <div class="item testi">
                                <p>This is a great free app to have to remind you of important birthdays. My friends and family are amazed as to how I remember all their birthdays!</p>
                                <h5>- Rustom Desai, Mumbai, India</h5>
                            </div>
                            <!--                            <div class="item testi">
                                                            <p>Really pleased to discover the site and app! I have started wishing my friends and business associates in India and also sent them birthday gift cards using the Birthday Owl app. Its really so quick, convenient and efficient.</p>
                                                            <h5>- Macy Smith, New York, USA</h5>
                                                        </div>-->
                        </div>
                        <div class="testimonials bottom-arrow"></div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
            <div class="flex-boxes-container">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <img src="<?PHP echo BASEURL_ONEWIMG ?>zodiac-sign-layout.png" class="img-responsive" alt="Zodiac">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="today-zodiac-back">
                        <div class="today-zodiac-sec">
                            <div class="row">
                                <div class="col-sm-9 col-md-9">
                                    <h2>Today's Zodiac Sign</h2>
                                    <div class="zodiac-name-date">
                                        <div class="col-sm-6 col-md-6">
                                            <h2><?php echo trim($zodiac_name); ?></h2>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <h3 class="rashi-date"><?php echo $date . " " . $month; ?></h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3">
                                    <img src="<?php echo $zodiac_img; ?>" class="img-responsive display-inlineblock resp1" alt="<?php echo trim($zodiac_name); ?>">
                                </div>
                            </div>
                            <p class="today-zodiac-content"><?php echo trim($first_part); ?></p>
                            <div class="clearfix"></div>
                            <p><a href="<?php echo WEB_HOROSCOPE_DAILY_WISE; ?>" class="zodiac-more-btn">Click here to know more...</a></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Home 4 boxes Section End -->


<!--Zodiac Sign-->
<!--<div class="container-fluid todays-zodiac-cont">
    <div class="container">
        <div class="row flexbox">

            <div class="col-xs-12 col-sm-10 col-md-9">

                <div class="row flexbox">

                    <div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-6 col-md-push-6">

                        <h2  style="font-weight: 700; margin-bottom: 25px">Today's Zodiac Sign</h2>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <img src="<?php echo $zodiac_img; ?>" class="img-responsive display-inlineblock resp1" alt="<?php echo trim($zodiac_name); ?>">
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">

                                <h2 style="margin-bottom: 0;font-weight: 700;font-size: 45px;"><?php echo trim($zodiac_name); ?></h2>
                                <h3 class="rashi-date"><?php echo $date . " " . $month; ?></h3>

                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-sm-pull-6 col-md-6 col-md-pull-6">
                        <div class="spacer-80"></div>
                        <p><?php echo trim($first_part); ?></p>

                    </div>
                    <div class="clearfix"></div>

                    <div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-6 col-md-push-6 "></div>
                    <div class="col-xs-12 col-sm-6 col-sm-pull-6 col-md-6 col-md-pull-6">
                        <div class="text-left">
                            <p><a href="<?php echo WEB_HOROSCOPE_DAILY_WISE; ?>" class="zodiac-more-btn">Click here to know more...</a></p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-2 col-md-3">

                <div class="text-center zodiac-owlbox">
                    <img src="<?php echo BASEURL_OIMG; ?>zodiac_home/owl-img.png" class="img-responsive display-inlineblock" alt="online gift items for birthday">
                </div>

            </div>

        </div>
    </div>
</div>-->


<div class="clearfix"></div>

<!-- New Testimonials -->
<!--<div class="container-fluid testimonial-back">
    <div class="container">
        <div class="row">
            <div class="flex-content">
                <div class="col-xs-12 col-sm-5 col-md-6 col-lg-6">

                    <img src="<?php //echo BASEURL_ONEWIMG        ?>what-cust-says-people.png" alt="free birthday wishes" class="img-responsive d-inline">

                </div>
                <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6">

                    <div class="owl-testimonial square owl-carousel owl-theme">
<?php //foreach ($testimonials as $testimonial) { ?>
                            <div class="item testi">
                                <p style=""><?php //echo $testimonial["description"];        ?></p> 
                                <h5>- <?php //echo $testimonial["witness_name"];        ?></h5>
                            </div>
<?php //} ?>
                    </div>
                    <div class="testimonials bottom-arrow"></div>

                </div>
            </div>
        </div>
    </div>
</div>-->
<!-- New Testimonials -->
<div class="clearfix"></div>
<!-- Newsletter Sign-up -->
<div class="container-fluid newsletter-sec">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                <div class="newsletter-box">
                    <div class="border-box"></div>
                    <div class="col-xs-12 col-sm-12 col-md-5 text-content-newsletter">
                        <div class="flex-content">
                            <h4>Newsletter SignUp</h4>
                            <p>Add your email address &amp; subscribe<br>
                                to our free newsletters and awesome offers!
                            </p>
                        </div>
                    </div>	
                    <div class="col-xs-12 col-sm-12 col-md-7">
                        <div class="">
                            <form action="">
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <input id="sname" type="text" placeholder="Name" class="newsletter-field">
                                </div>
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <input id="semail" type="text" placeholder="Email address" class="newsletter-field">
                                </div>
                                <div class="col-xs-12 col-sm-2 col-md-2">
                                    <input type="button" value="submit" id="subscribe">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid sortinfo">
    <h1 style="font-size: 22px;text-align: center;font-weight: 600;" >Free Happy Birthday Wishes and Gift Items Birthday</h1>
    <h2 style="font-size: 22px;">Gift Items For Birthday</h2> 
    <p> We all love gifts, don’t we? It’s not about receiving or sending any gift that matter, but sending something thoughtful and useful that really matters. Imagine receiving a nice surprise gift or an awesome <a href="https://www.birthdayowl.com/free-birthday-greeting-cards"><b>Birthday Greeting Card</b></a> with a personalized audio or video message with it? All this and more, is now possible with Birthdayowl™</p>

    <p>If you are planning to gift someone, and don’t exactly know how and what to do, then Birthdayowl™ can show you the way. Firstly, we can remind you of all your birthdays once you input them. Then we provide you with zodiac profile readings of each and every birthday so that you can get an instant insight into their personalities. You would be amazed at what you can learn about someone by simply reading about their zodiac date reading.</p>

    <p>Armed with this info, you can proceed to check out hundreds of options of free e-greeting cards, gifts, flowers, cakes and other cool stuff. Once you’ve made your choices, simply send them across – and trust us, your recipient is sure to impressed and smiling. All this takes just a few quality minutes and its easy on your wallet too, so no sweat really! </p>

    <!--    <h2 style="font-size: 22px;">Online Gift Cards </h2> 
        <p>Everything has now become so convenient online, from shopping to paying bills to movie tickets and airline and hotel bookings. The same applies to gifting as well, with a large variety of options now available at your fingertips.</p>
    
        <p>Choose from a wide range of <a href="https://www.birthdayowl.com/egiftcard-vouchers"><b>EGift Cards</b></a> which are instantly redeemable by your recipient at their own time and convenience. If you are not sure what they may like to receive, then simply gift them a store or restaurant or even a shopping website or holiday voucher – so that you give them the freedom to choose what they want, and whenever they want to avail it.</p>
    
        <p>If you prefer something more physical, go on and choose from our nice collection of <a href="https://www.birthdayowl.com/happy-birthday-beautiful-flower-cake"><b>Happy Birthday Beautiful Flower Cake</b></a> and more. The happiness will show on your recipient’s face when they receive our gifts!</p>
    
    
        <p>Besides, our wide selection of free birthday greeting cards are designed appropriately where you also have the option of inputting your own choice of text to make it even more personal. We deliver your birthday greeting card on your scheduled date and time by email and also send a text notification to your recipient’s mobile phone that your e-greeting card is waiting in their inbox. </p>
    
        <h2 style="font-size: 22px;">Free Birthday Wishes</h2> 
        <p>Birthdays are so important, so we just make sure you remember them. And you can send your free birthday wishes too.</p>
    
        <p>What better than having the choice to send a great greeting card together with audio or video messages which you can either record yourself, or simply attach an earlier downloaded file.  Either ways, you stand to win, as both options are terrific.  And free too!</p>
    
        <p>Our wide selection of birthday greeting cards are designed appropriately where you also have the option of inputting your own choice of text to make it even more personal. We deliver your birthday greeting card by email and also by SMS on the date and time that you may choose. All to your recipient’s email and mobile phone telling them that your <b>e-greeting card</b> is waiting in their inbox.</p>-->


    <p>All our greeting cards are free, so you can use as many as you like and even send all your yearly wishes to all your friends and family in one sitting. Simply input the dates and times when you wish these respective cards to be delivered and we will do the rest. We will even notify you – the sender – on the day the recipient views your card!</p>


</div>

<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script>
    $('.add_rem_banner_click').click(function () {
<?php if (($this->session->userdata('userdata') != NULL) && ($this->session->userdata['userdata']['is_guest'] == 0)) { ?>
            window.location = "<?php echo BASEURL; ?>addReminder/0";
<?php } else { ?>
            window.location = "<?php echo WEB_BIRTHDAY_REMINDER_WITHOUT_LOGIN; ?>";
<?php } ?>
    });
    $(document).ready(function () {
        var maxHeight = Math.max.apply(null, $(".content_height").map(function ()
        {
            return $(this).height();
        }).get());
        $('.content_height').css('height', maxHeight + 'px');
    });
    $(".greeting_card").click(function () {
        var card_id = $(this).attr("card_id");
        window.location = "<?php echo BASEURL; ?>insidegreeting/" + card_id + "/0/" +<?php echo $fid; ?>;
    });

    $(".zodiac_sign_click").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });

    $('.free_greetings').click(function () {
        window.location = "<?php echo WEB_FREE_GREETING_CARDS; ?>";
    });
    $('.zodaic_sign').click(function () {
        window.location = "<?php echo WEB_ZODIAC_SIGNS; ?>";
    });

    $('.flowers_cakes').click(function () {
        window.location = "<?php echo WEB_FLOWERS_AND_CAKES; ?>";
    });
    $('.zodaic_sign_day_wise').click(function () {
        window.location = "<?php echo WEB_HOROSCOPE_DAILY_WISE; ?>";
    });


    $(document).ready(function () {
        if (typeof (Storage) != "undefined") {
            var add_another_message = localStorage.getItem("add_another_message");
        }
        if (add_another_message == 1) {
            $(".banner").css("display", "none");
            $("#cart_success").show();
            if (typeof (Storage) != "undefined") {
                localStorage.setItem("add_another_message", "0");
            }
        } else {
            $(".banner").show();
            $("#cart_success").hide();
        }
    });

    var islogin = "<?php echo $this->session->userdata('userdata')['user_id'] ?>";
    $("#checkreg").click(function () {
        if ((islogin != '') && (isGuest == 0)) {
            window.location = "<?php echo BASEURL; ?>addReminder/0";
        } else {
            $('#remreg').click(); //triggered href using id of a tag in jqeury
        }
    });
    $("#reminder_banner").click(function () {
        if ((islogin != '') && (isGuest == 0)) {
            window.location = "<?php echo WEB_DASHBOARD; ?>";
        } else {
            $('#reminderpopup').click(); //triggered href using id of a tag in jqeury
        }
    });
    $(".showPopup").click(function () {
        $("body").css("overflow", "hidden");
        if (typeof (Storage) != "undefined") {
            localStorage.setItem("add_another_message", "0");
        }
        var voucher_pro_id = $(this).attr("voucher_pro_id");
        var data = {
            voucher_pro_id: voucher_pro_id
        }

        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>voucher-data",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() == "false") {
                    alert(r.message.toString());
                    return false;
                } else {
                    var voucher_url = r.product_data["voucher_url"];
                    var voucher_pro_name = r.product_data["voucher_pro_name"];
                    var redemption_details = r.product_data["terms_conditions"];
                    var vdescription = r.product_data["vdescription"];
                    var product_image;
                    if (r.product_data["product_image"] != "") {
                        product_image = r.product_data["product_image"];
                    } else {
                        product_image = "<?php echo BASEURL_OIMG . "unnamed.png"; ?>";
                    }

                    $(".pro_name").text(voucher_pro_name);
                    $(".minamt").text(" " + r.product_data["min_custom_price"] + " onwards");
                    $("#pro_img").attr("src", product_image);
                    $(".selected_card").attr("voucher_url", voucher_url);
                    var data1 = '';
                    data1 += '<div>' + redemption_details + '</div>';
                    $("#redemption_data").html(data1);
                    var data2 = '';
                    data2 += '<div>' + vdescription + '</div>';
                    $("#location_data").html(data2);
                    $.colorbox({
                        width: "50%",
                        height: "550px",
                        inline: true,
                        href: "#pickcard"
                    });
                    if ($(window).width() < 1280) {
                        $.colorbox({
                            width: "60%",
                            height: "500px",
                            inline: true,
                            href: "#pickcard"
                        });
                    }

                    if ($(window).width() < 480) {
                        $.colorbox({
                            width: "90%",
                            height: "90%",
                            inline: true,
                            href: "#pickcard"
                        });
                    }

                }
            },
        });
    });

    // Not in use
    $(".product_category").click(function () {
        window.location = "<?php echo BASEURL; ?>Home_web/show_vouchers/" + $(this).attr("id");
    });
    function check_confirm() {
        var sign_password = $("#sign_password").val();
        var conf_password = $("#conf_password").val();
        if (sign_password != conf_password) {
            $(".errors3").show();
            $(".errors3").html("Passwords do not match");
        } else {
            $(".errors3").html();
        }
        if (conf_password == '') {
            $(".errors3").html("");
        }
    }

    $('#semail').keyup(function (e) {
        $(".error_n3").hide();
    });
    $("#subscribe").click(function () {
        var semail = $("#semail").val();
        var sname = $("#sname").val();
        if (semail == '') {
            alert("Enter Email");
        } else if ((semail != '') && (validateEmail(semail) == false)) {
            $(".error_n3").show();
            alert("Invalid Email Address");
        } else if ((semail != '') && (validateEmail(semail) == true)) {
            var data = {
                email_id: semail,
                first_name: sname
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>newsletter",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    $(".showloader").removeClass("show_overlay").hide();
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        alert(r.message.toString());
                        window.location = "<?php echo WEB_HOME; ?>";
                    }
                }
            });
        }
    });
    $(document).on('click', '.pro', function () {
        var product_id = $(this).attr('product_id');
        localStorage.setItem('product_id', product_id);
        window.location = '<?php echo BASEURL; ?>flowers_detail/' + product_id + "/0";
    });
</script>
<script>
    $(document).ready(function () {

        $('.owl-banner').owlCarousel({
            loop: true,
            margin: 0,
            autoplay: true,
            nav: false,
            dots: true,
            // navText: ["<img src='tools/img/prev.png'>","<img src='tools/img/next.png'>"],
            slideBy: 1,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });


        $('.egift').owlCarousel({
            items: 4,
            loop: true,
            autoplay: false,
            nav: true,
            dots: false,
            navText: ["<img src='<?php echo BASEURL_ONEWIMG; ?>prev.png'>", "<img src='<?php echo BASEURL_ONEWIMG; ?>next.png'>"],
            slideBy: 1,
            responsive: {
                0: {
                    items: 2,
                    margin: 10
                },
                480: {
                    items: 2,
                    margin: 10
                },
                600: {
                    items: 3,
                    margin: 20
                },
                1000: {
                    items: 4,
                    margin: 40
                }
            }
        });

        $('.greetingscard').owlCarousel({
            items: 6,
            loop: true,
            autoplay: false,
            nav: true,
            dots: false,
            navText: ["<img src='<?php echo BASEURL_ONEWIMG; ?>prev.png'>", "<img src='<?php echo BASEURL_ONEWIMG; ?>next.png'>"],
            slideBy: 1,
            responsive: {
                0: {
                    items: 2,
                    margin: 10
                },
                480: {
                    items: 2,
                    margin: 10
                },
                600: {
                    items: 3,
                    margin: 20
                },
                1000: {
                    items: 6,
                    margin: 50
                }
            }
        });

        $('.flowers').owlCarousel({
            items: 6,
            loop: true,
            autoplay: false,
            nav: true,
            dots: false,
            navText: ["<img src='<?php echo BASEURL_ONEWIMG; ?>prev.png'>", "<img src='<?php echo BASEURL_ONEWIMG; ?>next.png'>"],
            slideBy: 1,
            responsive: {
                0: {
                    items: 2,
                    margin: 10
                },
                480: {
                    items: 2,
                    margin: 10
                },
                600: {
                    items: 3,
                    margin: 20
                },
                1000: {
                    items: 6,
                    margin: 50
                }
            }
        });

        $('.zodiac').owlCarousel({
            items: 6,
            loop: true,
            autoplay: false,
            nav: true,
            dots: false,
            navText: ["<img src='<?php echo BASEURL_ONEWIMG; ?>prev.png'>", "<img src='<?php echo BASEURL_ONEWIMG; ?>next.png'>"],
            slideBy: 1,
            responsive: {
                0: {
                    items: 2,
                    margin: 10
                },
                480: {
                    items: 2,
                    margin: 10
                },
                600: {
                    items: 3,
                    margin: 20
                },
                1000: {
                    items: 6,
                    margin: 50
                }
            }
        });


        $(".our-product.egift .owl-nav").insertBefore(".our-product.egift .owl-stage-outer");

        $(".our-product.greetingscard .owl-nav").insertBefore(".our-product.greetingscard .owl-stage-outer");

        $(".our-product.flowers .owl-nav").insertBefore(".our-product.flowers .owl-stage-outer");

        $(".our-product.zodiac .owl-nav").insertBefore(".our-product.zodiac .owl-stage-outer");

        $('.owl-testimonial').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            dots: false,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            navText: ["<img src='<?php echo BASEURL_ONEWIMG ?>test-prev.png'>", "<img src='<?php echo BASEURL_ONEWIMG ?>test-next.png'>"],
            slideBy: 1,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });

        $(".owl-testimonial.square .owl-nav").insertAfter(".owl-testimonial.square .owl-stage-outer");
        $(".testimonials.bottom-arrow").insertAfter(".owl-testimonial.square .item");

        // Create the dropdown base
        $("<select />").appendTo(".product-res-menu");

        // Create default option "Go to..."
        $("<option />", {
            "selected": "selected",
            "value": "",
            "text": "Select..."
        }).appendTo(".tabs-menu select");

        $(".tabs-menu a").each(function () {
            var value = $(this).attr("href");
            value = value.replace("#", "");
            var el = $(this);
            $("<option />", {
                "value": value,
                "text": el.text()
            }).appendTo(".product-res-menu select");
        });

        $(".product-res-menu select").change(function () {
            var option = $(this).find("option:selected").val();
            $('.tab-pane').removeClass('in active');
            $('#' + option).addClass('in active');

        });

        /*window.onresize = function(event)
         {
         document.location.reload(true);
         }*/

    });
</script>