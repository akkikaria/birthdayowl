<?php // $this->load->view("new_templates/header");  ?>
<link rel="shortcut icon" href="<?php echo BASEURL_OIMG; ?>main_icn.png" type="image/png" />

<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>style.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>grid.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>jquery-ui.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo BASEURL_OCSS; ?>my_style.css"  type="text/css"/>

<!-- Bootstrap -->
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>bootstrap.min.css" type="text/css"/>
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>font-awesome.min.css" type="text/css"/>
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>buttons.css" type="text/css"/>
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>owl.carousel.min.css" type="text/css"/>
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>owl.theme.default.css" type="text/css"/>

<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>nouislider.css" type="text/css"/>
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>nouislider.pips.css" type="text/css"/>

<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>bootstrap-datepicker.min.css" type="text/css"/>
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>default.css" type="text/css"/>
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>default.date.css" type="text/css"/>

<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>easy-autocomplete.min.css" type="text/css"/>
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>colorbox.css" type="text/css"/>
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>home-style.css" type="text/css"/>
<!--=======================================NEW============================================-->

<link href="<?PHP echo BASEURL_OCSS; ?>style_1.css" rel="stylesheet" />
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>default.css" />
<link rel="stylesheet" href="<?PHP echo BASEURL_OCSS ?>default.date.css" />

<link href="<?PHP echo BASEURL_OCSS; ?>style_popup.css" rel="stylesheet" type="text/css"/>
<style>
    .commoncss{
        margin-top: 18px; text-align: justify; font-size: 17px; line-height: 25px;font-family:"Gotham-book";
    }
</style>

<div class="wrapper">
    <div class="container_12">
        <div class="inner_midd">
            <div class="col-xs-12 col-sm-8 col-md-9" style="">
                <div class="grid_12">

                    <div class="zodiac-content1">
                        <div class="zodiac-img">
                            <div class="heading_2" >
                                Terms & Conditions - <span>Flowers & Other Physical Gifts</span>
                            </div>
                        </div>
                        <div style="background-color: white;padding: 5px;font-family:Gotham-book;font-size: 17px; line-height: 25px;">
                            <?php echo $terms; ?>

                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-4 col-md-3" style="text-align:center;">
                <img src="<?php echo BASEURL_OIMG; ?>add_bday_reminder.png" id="blah" style="padding-left: 0px;" />
            </div>
        </div>
    </div>
</div>
<?php // $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript">

    if ($(window).width() < 768) {
        $("#blah").css({"top": "11px", "padding": "0px"});
    }
    if ($(window).width() > 768) {
        $(function () {

            var $blah = $("#blah"),
                    $window = $(window),
                    offset = $blah.offset();

            $window.scroll(function () {
                if ($window.scrollTop() > offset.top) {
                    $blah.stop().animate({
                        top: 1700
                    });
                } else {
                    $blah.stop().animate({
                        top: 0
                    });
                }
            });
        });
    }


</script>
