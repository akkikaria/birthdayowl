<?php $this->load->view("new_templates/header"); ?>

<style>
    .nl-accordion{
        margin:0 auto;	
        background-color:#fff;
    }
    .nl-accordion label.nl-label{
        display:block;
        font-size:14px;
        line-height:120%;
        background-color:#52a303;
        border:1px solid #ccc;
        color:black;    
        font-weight:600;
        cursor:pointer;
        text-transform:uppercase;
    }
    .nl-accordion > ul{
        list-style:none;
        margin:0;
        padding:0;    
    }
    .nl-accordion > ul li {
        overflow:hidden;
        margin:0 0 1px;
    }
    .nl-accordion > ul li label.nl-label {
        padding:10px;
    }
    .nl-accordion > ul li label.nl-label:hover {
        border:1px solid #542437;
    }
    .nl-accordion [type="radio"].nl-radio, .nl-accordion [type="checkbox"].nl-checkbox {
        display:none;
    }
    .nl-accordion > ul li label.nl-label:hover, .nl-accordion [type="radio"].nl-radio:checked ~ label.nl-label, .nl-accordion [type="checkbox"].nl-checkbox:checked ~ label.nl-label {
        background:black;
        color:#FFF;
        -webkit-text-shadow:1px 1px 1px rgba(0,0,0,0.5);
        -moz-text-shadow:1px 1px 1px rgba(0,0,0,0.5);
        text-shadow:1px 1px 1px rgba(0,0,0,0.5);
    }
    .nl-accordion [type="radio"].nl-radio:checked ~ label.nl-label, .nl-accordion [type="checkbox"].nl-checkbox:checked ~ label.nl-label {
        border-bottom:0;
    }
    .nl-accordion > ul li .nl-content {
        height:0px;
        border-top:0;
        padding:0 10px;    
        border:1px solid #fff;
        -webkit-transition: all .3s ease-out;
        -moz-transition: all .3s ease-out;
        transition: all .3s ease-out;
    }
    .nl-accordion [type="checkbox"].nl-checkbox:checked ~ label.nl-label ~ .nl-content, .nl-accordion [type="radio"].nl-radio:checked ~ label.nl-label ~ .nl-content{
        min-height:100px;
        background: #f0f0f0 none repeat scroll 0 0;
        /*border:1px solid #542437;*/
        overflow:auto;
    }
    .blahimg{
        width: 78%;
    }

    #divstart{
        margin-top: 10px; text-align: justify; font-size: 17px; line-height: 25px;font-family:'gotham-book'
    }
    @media only screen and (max-width:480px)
    {
        .nl-accordion label.nl-label{
            font-size: 11px; 
        }
        #divstart{
            font-size: 15px;
        }




    }
</style>

<div class="wrapper">
    <div class="container_12">
        <div class="inner_midd dashbord">
            <div class="col-xs-12 col-sm-8 col-md-9" >
                <div class="zodiac-content1">
                    <div class="zodiac-img">
                        <div class="heading_2" >
                            <span>FAQs</span>
                        </div>
                    </div>
                    <div style="background-color: white;padding: 5px;">
                        <div id="divstart">

                            <div id="myAccordion" class="nl-accordion">
                                <ul>
                                    <?php
                                    $i = 1;
                                    foreach ($faq as $faqs) {
                                        ?>
                                        <li>
                                            <input type="radio" id="nl-radio-<?php echo $i ?>" name="nl-radio" class="nl-radio" />
                                            <label class="nl-label" for="nl-radio-<?php echo $i ?>"><?php echo $faqs["title"] ?></label>
                                            <div class="nl-content">
                                                <p><?php echo $faqs["description"] ?></p>
                                            </div>
                                        </li>

                                        <?php
                                        $i++;
                                    }
                                    ?>

                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3" style="text-align:center;">
                <img alt="birthdayowl" src="<?php echo BASEURL_OIMG; ?>add_bday_reminder.png" id="blah" class="blahimg" style="padding-left: 0px;" />
            </div>



        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script> if ($(window).width() < 768) {
        $("#blah").css({"top": "11px", "padding": "0px"});
    }
    if ($(window).width() > 768) {
        $(function () {

            var $blah = $("#blah"),
                    $window = $(window),
                    offset = $blah.offset();

            $window.scroll(function () {
                if ($window.scrollTop() > offset.top) {
                    $blah.stop().animate({
                        top: 240
                    });
                } else {
                    $blah.stop().animate({
                        top: 0
                    });
                }
            });
        });
    }


</script>
