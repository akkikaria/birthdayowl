<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Emailer</title>
    </head>
    <body>
        <table  width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="color:#000; font-family:Arial, Helvetica, sans-serif; font-size:13px;">
            <tr>
                <td valign="top" ><table  width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tr>
                            <td valign="top"><img src="<?php echo BASEURL_EIMG; ?>banner-confirmed.png" height="328" width="600" title="birthdayowl" alt="birthdayowl" border="0" style="display:block;"  /> </td>
                        </tr> 
                        <tr>
                            <td valign="top" align="left">
                                <table  width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                                    <tr>
                                        <td valign="top" align="left" style="background-color:#fee6f0;"><img src="<?php echo BASEURL_EIMG; ?>emailer-left.png" height="619" width="43" title="birthdayowl" alt="birthdayowl" border="0" style="display:block;"  /></td> 
                                        <td  valign="top" align="left" width="434" style="padding:0 40px;">
                                            <table  width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                <tr>
                                                    <td align="center" style="font-size:22px; color:#000; padding-bottom:20px;">
                                                        Good News <?php echo $sender_name; ?>
                                                        <span style="display:block; font-size:12px; padding-top:5px;">Product is received.</span>
                                                    </td>
                                                </tr> 
                                                <tr>
                                                    <td valign="top"><img src="<?php echo BASEURL_EIMG; ?>progress-bar-confirmed.png" height="17" width="434" title="birthdayowl" alt="birthdayowl" border="0" style="display:block;"  /></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"  style="padding-bottom:30px;">
                                                        <table  width="100%" cellspacing="0" cellpadding="0" border="0" >
                                                            <tr>
                                                                <td width="25%" valign="top" align="left" style="font-size:12px;">
                                                                    <strong>Confirmed
                                                                        <br/>
                                                                        <?php
                                                                        $date = date_create($purchased_date);
                                                                        echo date_format($date, 'jS F Y g:i A');
                                                                        ?>
                                                                    </strong>
                                                                </td>
                                                                <td width="25%" valign="top" align="center" style="font-size:12px;"><strong>Processing</strong></td>
                                                                <td width="25%" valign="top" align="center" style="font-size:12px;"><strong>On the way</strong></td>
                                                                <td width="25%" valign="top" align="right" style="font-size:12px;"> 
                                                                    <strong>Order Delivered<br/>
                                                                        <?php
                                                                        $date = date_create($flower_delivery_date);
                                                                        echo date_format($date, 'jS F Y');
                                                                        ?>
                                                                    </strong>
                                                                </td> 
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
<!--                                                <tr>
                                                    <td valign="top" align="center" style="padding-bottom:30px;"><a href="#" title="Track Order" style="text-decoration:none;"><img src="track-btn.png" height="24" width="89" title="Track Order" alt="Track Order" border="0" style="display:block;"  /></a></td>
                                                </tr>-->
                                                <tr>
                                                    <td valign="top" style="border-bottom:solid 1px #4a8a02; padding-bottom:10px; font-size:14px; color:#000">
                                                        <table  width="100%" cellspacing="0" cellpadding="0" border="0" >
                                                            <tr>

                                                                <?php
                                                                $date = date_create($flower_delivery_date);
                                                                $flower_delivery_date_new = date_format($date, "d-m-Y");
                                                                ?>
                                                                <td align="left"><strong>Delivery Date: </strong><?php echo $flower_delivery_date_new; ?></td>
                                                                <td align="right"><strong>Invoice: </strong>#<?php echo $order_id; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left"><strong>Delivery Time Slot: </strong><?php echo $flower_delivery_time_slot; ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="border-bottom:solid 1px #4a8a02; padding-bottom:5px; font-size:14px; color:#000">
                                                        <table  width="100%" cellspacing="0" cellpadding="0" border="0" >
                                                            <tr>
                                                                <td align="center" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>ITEM</strong></td>
                                                                <td align="center" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>MODEL</strong></td>
                                                                <td align="center" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>QTY</strong></td>
                                                                <td align="center" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>COST</strong></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" valign="top" style="font-size:12px; padding:7px;"><?php echo $flower_name; ?>
                                                                    <!--<br><img src="flower-img.png" height="110" width="134" title="birthdayowl" alt="birthdayowl" border="0" style="display:block; margin-top:10px;"  />-->
                                                                </td>
                                                                <td align="center" valign="top" style="font-size:12px; padding:7px;"><?php echo $flower_code; ?></td>
                                                                <td align="center" valign="top" style="font-size:12px; padding:7px;"><?php echo $flower_quantity; ?></td>
                                                                <td align="right" valign="top" style="font-size:12px; padding:7px;">&#8377;<?php echo $flower_price; ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="border-bottom:solid 1px #4a8a02; padding-bottom:5px; font-size:16px; color:#000">
                                                        <table  width="100%" cellspacing="0" cellpadding="0" border="0" >
                                                            <tr>
                                                                <?php
                                                                $sub_total = $flower_price * $flower_quantity;
                                                                ?>
                                                                <td align="right" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>SUB-TOTAL</strong></td>
                                                                <td align="right" valign="top" style="font-size:12px;  padding:7px;">&#8377;<?php echo $sub_total; ?></td>
                                                            </tr>
                                                            <tr> 
                                                                <td align="right" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>processing fee (3.5%)</strong></td>
                                                                <td align="right" valign="top" style="font-size:12px;  padding:7px;">&#8377;<?php echo $processing_fee1; ?></td>
                                                            </tr>
                                                            <tr> 
                                                                <td align="right" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>Free- Shipping</strong></td>
                                                                <td align="right" valign="top" style="font-size:12px;  padding:7px;">&#8377;0</td>
                                                            </tr>
                                                            <tr> 
                                                                <td align="right" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>TOTAL</strong></td>
                                                                <td align="right" valign="top" style="font-size:12px;  padding:7px;">&#8377;<?php echo $total_amount; ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="font-size:22px; color:#000; padding-bottom:10px; padding-top:10px;">
                                                        Order Details
                                                    </td>
                                                </tr> 
<!--                                                <tr>
                                                    <td valign="top" style="border-bottom:solid 1px #4a8a02; padding:7px 10px; font-size:16px; color:#000">
                                                        <table  width="100%" cellspacing="0" cellpadding="0" border="0" >
                                                            <tr> 
                                                                <td width="50%" align="left" valign="top" style="font-size:12px; padding:2px;"><strong>Telephone:</strong> 1234567890</td>
                                                                <td width="50%" align="left" valign="top" style="font-size:12px;  padding:2px;"><strong>Date:</strong> 2018-04-13 17:45:12</td>
                                                            </tr>
                                                            <tr> 
                                                                <td align="left" valign="top" style="font-size:12px; padding:2px;"><strong>Email:</strong> <a href="mailto:info@birthdayowl.com" title="info@birthdayowl.com" style="color:#000; text-decoration:none;">info@birthdayowl.com</a></td>
                                                                <td align="left" valign="top" style="font-size:12px;  padding:2px;"><strong>Order ID:</strong>  780791</td>
                                                            </tr>
                                                            <tr> 
                                                                <td align="left" valign="top" style="font-size:12px; padding:2px;"><strong>Website:</strong> <a href="https://www.birthdayowl.com" title="https://www.birthdayowl.com" target="_blank" style="color:#000; text-decoration:none;">https://www.birthdayowl.com</a></td>
                                                                <td align="left" valign="top" style="font-size:12px;  padding:2px;"><strong>Payement Method: </strong>  Credit Card /
                                                                    <br>Debit card</td>
                                                            </tr>
                                                            <tr> 
                                                                <td align="left" valign="top" style="font-size:12px; padding:2px;"> </td>
                                                                <td align="left" valign="top" style="font-size:12px;  padding:2px;"><strong>Shipping Method: </strong>   
                                                                    Free Shipping</td>
                                                            </tr> 
                                                        </table>
                                                    </td>
                                                </tr>-->
                                                <tr>
                                                    <td valign="top" style="  padding:7px 10px; font-size:16px; color:#000">
                                                        <table  width="100%" cellspacing="0" cellpadding="0" border="0" >
                                                            <tr> 
                                                                <td width="50%" align="left" valign="top" style="font-size:12px; padding:2px;">
                                                                    <strong>Payment Address:</strong><br/>
                                                                    <span><?php echo $sender_name; ?></span></td>
                                                                <td  width="50%" align="left" valign="top" style="font-size:12px;  padding:2px;">
                                                                    <strong>Shipping Address:</strong><br/>
                                                                    <span><?php echo $receiver_name; ?></span>
                                                                </td>
                                                            </tr>
                                                            <tr> 
                                                                <td width="50%" align="left" valign="top" style="font-size:12px; padding:0 2px;"><strong>Email: </strong> <a href="mailto:info@birthdayowl.com" title="info@birthdayowl.com" style="color:#000; text-decoration:none;"><?php echo $sender_email; ?></a></td>
                                                                <td  width="50%" align="left" valign="top" style="font-size:12px;  padding:0 2px;">
                                                                    <span><?php echo $flower_receiver_address1; ?></span><!--Address Line 1-->
                                                                </td>
                                                            </tr>
                                                            <tr> 
                                                                <td width="50%" align="left" valign="top" style="font-size:12px; padding:0 2px;"><strong>Mobile:</strong><?php echo $sender_phone; ?></td>
                                                                <td  width="50%" align="left" valign="top" style="font-size:12px;  padding:0 2px;">
                                                                    <span><?php echo $flower_receiver_address2; ?><!--Address Line 2-->
                                                                </td>
                                                            </tr>
                                                            <tr> 
                                                                <td width="50%" align="left" valign="top" style="font-size:12px; padding:0 2px;"></td>
                                                                <td  width="50%" align="left" valign="top" style="font-size:12px;  padding:0 2px;">
                                                                    <span><?php echo $flower_delivery_city; ?>-<?php echo $flower_delivery_pincode; ?>, India</span><!--City Country-->
                                                                </td>
                                                            </tr>
                                                            <tr> 
                                                                <td width="50%" align="left" valign="top" style="font-size:12px; padding:2px;"></td>
                                                                <td  width="50%" align="left" valign="top" style="font-size:12px;  padding:2px;">
                                                                    <strong>Mobile: </strong><?php echo $receiver_phone; ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td> 
                                        <td valign="top" align="left" style="background-color:#fee6f0;"><img src="<?php echo BASEURL_EIMG; ?>emailer-rt.png" height="619" width="43" title="birthdayowl" alt="birthdayowl" border="0" style="display:block;"  /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr> 
                        <tr>
                            <td valign="top"><img src="<?php echo BASEURL_EIMG; ?>emailer-btm.png" height="114" width="600" title="birthdayowl" alt="birthdayowl" border="0" style="display:block;"  /> </td>
                        </tr>
                        <tr width="100">
                            <td  valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; font-weight:normal;padding:15px; ">For more information please read our <a href="<?php echo WEB_PRIVACY_AND_POLICY; ?>" title="Privacy Policy" style="text-decoration:none; color:#1155cc;">Privacy Policy</a> and <a href="<?php echo WEB_TERMS_OF_USE_OTHER; ?>" title="Terms of Use" style="text-decoration:none; color:#1155cc;">Terms of Use</a>.<br />
                                Copyright © 2016 birthdayowl.com. All rights reserved.</td>
                        </tr>
                        <tr>
                            <td>
                                <table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody class="mcnFollowBlockOuter">
                                        <tr>
                                            <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                                                <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding-left:9px;padding-right:9px;" align="center">
                                                                <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                                                                                <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td valign="top" align="center">
                                                                                                <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                    <tbody><tr>
                                                                                                            <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                                <a href="https://www.instagram.com/birthday_owl_reminder/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/instagram-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                    <tbody><tr>
                                                                                                            <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                                <a href="https://www.facebook.com/birthdayowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/facebook-ico.png" alt="Facebook" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                    <tbody><tr>
                                                                                                            <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                                <a href="https://twitter.com/birthday_owl" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/twitter-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                    <tbody><tr>
                                                                                                            <td class="mcnFollowIconContent" style="padding-right:0; padding-bottom:9px;" valign="top" align="center">
                                                                                                                <a href="https://www.pinterest.com/birthdyowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/pinterest-ico.png" alt="Pinterest" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>
    </body>
</html>
