<?php /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ ?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title></title>
        <style type="text/css">


            body{
                font-family: verdana, arial, sans-serif;
            }
        </style>
    </head>
    <body>

        <div style="width: 100%; text-align: center;margin:auto;background-color: #efefef;height:auto;">
            <table   cellspacing="0" cellpadding="0" border="0" align="center"  >
                <tr>
                    <td  align="center">
                        <img src="<?php echo BASEURL_OIMG ?>logo.png" />
                    </td>
                </tr>
                <tr valign="center" align="center"   >
                    <td>
                        <table style="background:#4D9E03;height:53px;width:526px;">

                            <tr >
                                <td valign="center"  align="center" width='50' >
                                    <img src="<?php echo BASEURL_EIMG ?>p.png" style="float:left;width: 50px;"/> 
                                </td>
                                <td align='left'>
                                    <div style="font-size: 18px; text-align: left; color: white; line-height: 56px; height: 50px;">Payment Recieved (INR <?php echo $amount; ?>)</div>    
                                </td>
                            </tr>

                        </table>
                    </td>


                </tr>
                <tr  align="center">
                    <td style='width: 450px; height: 100px; background: transparent url("<?php echo BASEURL_EIMG . 'membership_body.png' ?>") no-repeat scroll 0% 0%; font-weight: bold; font-size: 22px; padding-top: 16px; font-family: "Apple Chancery"';>
                        <div style="text-align:left;padding-left: 58px;">Enjoy  Membership Plan</div>
                        <div style="text-align:left;padding-left: 58px;"> For  Next <?php echo $plan_purchase_type; ?> Year!!!</div>
                    </td> 
                </tr>


                <tr  align="center">
                    <td  style='background: #bfedac;width:480px;height:300px ;border-top:1px solid #DFDFDF' >
                        <table style="padding:8px">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <div><b>Payment Details</b></div>
                                            </td>
                                        </tr>


                                    </table>
                                </td>
                                <td align='right'>
                                    <img src="<?php echo BASEURL_EIMG ?>payd.png" />
                                </td>
                            </tr>
                            <tr align='center' >
                                <td colspan="2"  align='center' style="padding-left:4px">
                                    <table align="center">
                                        <tr align="center">
                                            <td>Order number</td>
                                            <td>:</td>
                                            <td>#<?php echo $order_id; ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2"  align='center'>
                                    <table>
                                        <tr>
                                            <td colspan="5">Billed to</td>
                                            <td>:</td>
                                            <td><?php echo ucfirst($bill_name); ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">Membership Amount</td>
                                            <td>:</td>
                                            <td>Rs.<?php echo $amount; ?></td>
                                        </tr>

                                        <tr>
                                            <td colspan="5">Payment method</td>
                                            <td>:</td>
                                            <?php
                                            if ($payment_mode == "CC") {
                                                $mode = "Credit Card";
                                            } else if ($payment_mode == "DC") {

                                                $mode = "Debit Card";
                                            }
//                                            
                                            ?>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">Payment date & time</td>
                                            <td>:</td>
                                            <td><?php echo $purchased_date; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">Expiry date & time</td>
                                            <td>:</td>
                                            <td><?php echo $expiry_date; ?></td>
                                        </tr>
                                    </table>
                                </td> 
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr  align="center" >
                    <td  style='background: white;width:480px;min-height: 850px;border-top:1px solid #DFDFDF'  >
                        <table>
                            <tr>
                                <td valign="center"  align="center"  >
                                    <div style="padding-left:10px;padding-right: 10px;width: 418px;">
                                        <div style="font-size: 12px;float:left;padding-top: 5px;">
                                            <b>Terms & Conditions</b><br/>
                                        </div><br/>
                                        <div style="text-align: left;font-size: 13px;padding-top: 5px;line-height: 18px;">
                                            Thank you for choosing to be a registered member with BirthdayOwl<sup>®</sup>
                                            Welcome onboard, and we hope you will enjoy the benefits and the services offered by us. Please take a minute to read the below Terms and Conditions. These Terms and Conditions (“TC”) will explain how we provide
                                            our services to you, and what obligations you have as a Member user of BirthdayOwl<sup>®</sup>
                                            <p><b>BirthdayOwl<sup>®</sup>’s Obligations to You.</b></p>
                                            We will do the following:
                                            <ul>
                                                <li>Supply you with access to a mobile and online service to remind you of friends' and relatives' birthdays and provide a choice of e-greeting cards and e-Gift Cards to send them (the “Service”).</li>
                                                <li> Answer any questions you have regarding the TC, the Service and the operation of your account.
                                                </li>
                                            </ul>

                                            <b>What We Ask of BirthdayOwl<sup>®</sup> Users.</b><br/>
                                            We ask our users to agree to the following:
                                            <ul>
                                                <li>Read and understand the TC and let us know if you have any questions.</li>
                                                <li>Use the Service responsibly.</li>
                                                <li>Do not allow anyone other than yourself and BirthdayOwl<sup>®</sup> to access your account.</li>
                                                <li>Use the Service solely for your own benefit and do not sell, lease, assign, lend, hire or gift any part of the service to any other person without BirthdayOwl<sup>®</sup>prior written approval.</li>
                                                <li>Use the Service only for lawful purposes and without infringing the rights of any Member, BirthdayOwl<sup>®</sup>or any third party.</li>
                                                <li>Comply with all terms of the TC.</li>
                                            </ul>
                                            <p><b>Security and Member Privacy</b></p>
                                            Please maintain the confidentiality of your password and account. You are responsible for all Member accessible activities that occur under your account. Please notify us immediately of any unauthorized use of your password or account or any other breach of security.
                                            You agree that BirthdayOwl<sup>®</sup> and its authorized employees can access your account, including its contents as stated above, or to respond to support queries or technical issues or to fulfill the obligations under this Agreement. To learn more about how the Service deals with privacy then please see the FAQs in the Privacy & Security Policy.<br/>
                                            <p><b>Member Account Registration</b> </p>
                                            To register as a BirthdayOwl<sup>®</sup>Member, you agree to:
                                            <ul>
                                                <li>Provide us with current, complete and accurate information about yourself (the registration data).</li>
                                                <li>Maintain and promptly update the registration data to keep it current, complete and accurate.</li>
                                                <li>To represent only yourself on the Service, and no other persons, living, dead, or imagined.</li>
                                                <li>If BirthdayOwl<sup>®</sup> has reasonable grounds to believe your information is not current, or is incomplete or inaccurate, we may suspend or terminate your membership. However, we have no obligations to check the accuracy or truthfulness of your registration data.</li>
                                            </ul>

                                            <b>Member Conduct</b> <br/>
                                            As a condition of your use of the Service, you will not use the Service for any purpose that is unlawful or prohibited by the TC. By way of example, and not as a limitation, you agree NOT to:
                                            <ul>
                                                <li>Defame, abuse, harass, stalk, threaten or otherwise violate the rights (such as rights of privacy and publicity) of others.</li>
                                                <li>Publish, distribute and/ or disseminate any harmful, obscene, indecent, unlawful, libelous, profane, defamatory, infringing, inappropriate, hateful, or racially, ethnically or otherwise objectionable material or information.</li>
                                                <li>Create a false or misleading identity of, including, but not limited to, a BirthdayOwl<sup>®</sup>employee, or falsely state or otherwise misrepresent your affiliation with a person or entity, for the purpose of misleading others as to the identity of the sender or the origin of a message or to harvest or otherwise collect information about others.</li>
                                                <li>Transmit, email or post any material that contains in any form software viruses or such programs as including but not limited to, Trojan horses, worms, time bombs, cancel-bots, computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment.</li>
                                                <li>Attempt to gain unauthorized access to the Service, other accounts, computer systems or networks connected to the Service, through password mining or any other means.</li>
                                                <li>Disseminate content that infringes any patent, trademark, trade secret, copyright, rights of privacy or publicity, or other proprietary right ('Rights') of any party or infringes any intellectual property law or any other applicable law.</li>
                                                <li>Disseminate any unsolicited or unauthorized advertising, promotional materials, 'junk mail', 'spam', 'chain letters', 'pyramid schemes', or any other form of such solicitation.</li>
                                                <li>Interfere with another member's use and enjoyment of the Service.</li>
                                                <li>BirthdayOwl<sup>®</sup>has no obligation to monitor the Service or any member's use thereof.</li>
                                            </ul>

                                            <b>e-Gift Cards</b><br/>
                                            BirthdayOwl<sup>®</sup> makes it very convenient to send birthday gifts electronically to your recipient and we have partnered with woohoo.in for this. All e-Gift Cards we display on our website are issued by woohoo.in and these e-Gift Cards or e-Gift vouchers can be preloaded with a desired amount of money and gifted to your loved ones who can then use it to purchase products or services of his/her choice. It can also be used to make purchases online or in physical stores by using the card number and pin available on the card. Given the flexibility and practicality e-Gift Cards offer, they make the perfect presents for birthdays, weddings, anniversaries, baby showers, holidays, house warming, employee rewards, any special occasion, or a simple gesture of appreciation.
                                            <p><b>Limitation of liability</b></p> 
                                            You agree that BirthdayOwl<sup>®</sup> shall not be responsible or liable for:

                                            <ul>
                                                <li>Unauthorized access to or alteration of your transmissions or data, any material or data sent or received or not sent or received, or any transactions entered into through the service.</li>
                                                <li>Any threatening, defamatory, obscene, offensive or illegal content, save that BirthdayOwl<sup>®</sup> shall be entitled to remove it in all practicable speed after BirthdayOwl<sup>®</sup> has been notified in writing of its presence on the site.</li>
                                                <li>Conduct of any other member or any infringement of another's rights, including intellectual property rights.</li>
                                                <li>Any indirect, punitive, incidental, special, consequential or exemplary damages or any damages whatsoever including, without limitation:</li>
                                                <li>Damages for loss of use</li>
                                                <li>Data goodwill</li>
                                                <li>Profits, or other intangible losses</li>
                                            </ul>

                                            <p><b>Indemnification</b></p>
                                            You agree to indemnify, defend and hold harmless BirthdayOwl<sup>®</sup>, its parents, subsidiaries, affiliates, officers and employees from any claim, demand, or damage, including reasonable legal fees, asserted by any third party due to or arising out of your use of or conduct on the Service.
                                            <br/>
                                            <p><b>Modification and Termination of the Service</b></p>
                                            You agree that BirthdayOwl<sup>®</sup>, in its sole discretion, may terminate your password, account or use of the Service, and remove and discard any content within the Service, for any reason, including without limitation, if BirthdayOwl<sup>®</sup>believes that you have violated or acted inconsistently with the letter or spirit of the TC. 
                                            BirthdayOwl<sup>®</sup>may also, in its sole discretion and at any time, discontinue temporarily or permanently providing the Service, or any part thereof, with or without notice.

                                            <p><b>Modification to the TC</b></p>
                                            BirthdayOwl<sup>®</sup> reserves the right to change the TC or policies regarding the use of the Service at any time.
                                            BirthdayOwl<sup>®</sup> reserves the right at any time to modify or discontinue, temporarily or permanently, the Service (or any part thereof) with or without notice. You agree that BirthdayOwl<sup>®</sup> shall not be liable to you or any third party for any modification to or discontinuance of the Service.
                                            <p><b>TC Violations</b></p>

                                            Please report any violations of the TC to us at support@birthdayowl.com
                                            <p><b>Entire Agreement</b></p>
                                            These TC constitute the entire agreement between BirthdayOwl<sup>®</sup>and the Member and supersede any previous agreement or understanding between BirthdayOwl<sup>®</sup>and the Member.


                                            <br/>
                                        </div>
                                    </div>
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td  valign="center" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; font-weight:normal;padding:15px; ">For more information please visit our website and read our <a href="<?php echo WEB_PRIVACY_AND_POLICY; ?>" title="Privacy Policy" style="text-decoration:none; color:#1155cc;">Privacy Policy</a> and <a href="<?php echo BASEURL; ?>Home_web/terms_n_conditions" title="Terms of Use" style="text-decoration:none; color:#1155cc;">Terms of Use</a>.<br />
                        Copyright © 2016 birthdayowl.com. All rights reserved.</td>
                </tr>
                <tr>
                    <td>
                        <table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody class="mcnFollowBlockOuter">
                                <tr>
                                    <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                                        <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding-left:9px;padding-right:9px;" align="center">
                                                        <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                                                                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="top" align="center">
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.instagram.com/birthday_owl_reminder/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/instagram-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.facebook.com/birthdayowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/facebook-ico.png" alt="Facebook" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://twitter.com/birthday_owl" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/twitter-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:0; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.pinterest.com/birthdyowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/pinterest-ico.png" alt="Pinterest" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>

        </div>
    </body>
</html>