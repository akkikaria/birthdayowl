<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <!-- NAME: 1:2:1 COLUMN -->
        <!--[if gte mso 15]>
<xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
</xml>
<![endif]-->
        <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=1024">
                    <title>*|MC:SUBJECT|*</title>

                    <style type="text/css">
                        p{
                            margin:10px 0;
                            padding:0;
                        }
                        table{
                            border-collapse:collapse;
                        }
                        h1,h2,h3,h4,h5,h6{
                            display:block;
                            margin:0;
                            padding:0;
                        }
                        img,a img{
                            border:0;
                            height:auto;
                            outline:none;
                            text-decoration:none;
                        }
                        body,#bodyTable,#bodyCell{
                            height:100%;
                            margin:0;
                            padding:0;
                            width:100%;
                        }
                        .mcnPreviewText{
                            display:none !important;
                        }
                        #outlook a{
                            padding:0;
                        }
                        img{
                            -ms-interpolation-mode:bicubic;
                        }
                        table{
                            mso-table-lspace:0pt;
                            mso-table-rspace:0pt;
                        }
                        .ReadMsgBody{
                            width:100%;
                        }
                        .ExternalClass{
                            width:100%;
                        }
                        p,a,li,td,blockquote{
                            mso-line-height-rule:exactly;
                        }
                        a[href^=tel],a[href^=sms]{
                            color:inherit;
                            cursor:default;
                            text-decoration:none;
                        }
                        p,a,li,td,body,table,blockquote{
                            -ms-text-size-adjust:100%;
                            -webkit-text-size-adjust:100%;
                        }
                        .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
                            line-height:100%;
                        }
                        a[x-apple-data-detectors]{
                            color:inherit !important;
                            text-decoration:none !important;
                            font-size:inherit !important;
                            font-family:inherit !important;
                            font-weight:inherit !important;
                            line-height:inherit !important;
                        }
                        #bodyCell{
                            padding:10px;
                        }
                        .templateContainer{
                            max-width:600px !important;
                        }
                        a.mcnButton{
                            display:block;
                        }
                        .mcnImage,.mcnRetinaImage{
                            vertical-align:bottom;
                        }
                        .mcnTextContent{
                            word-break:break-word;
                        }
                        .mcnTextContent img{
                            height:auto !important;
                        }
                        .mcnDividerBlock{
                            table-layout:fixed !important;
                        }
                        /*
                        @tab Page
                        @section Background Style
                        @tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
                        */
                        body,#bodyTable{
                            /*@editable*/background-color:#FAFAFA;
                        }
                        /*
                        @tab Page
                        @section Background Style
                        @tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
                        */
                        #bodyCell{
                            /*@editable*/border-top:0;
                        }
                        /*
                        @tab Page
                        @section Email Border
                        @tip Set the border for your email.
                        */
                        .templateContainer{
                            /*@editable*/border:0;
                        }
                        /*
                        @tab Page
                        @section Heading 1
                        @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
                        @style heading 1
                        */
                        h1{
                            /*@editable*/color:#202020;
                            /*@editable*/font-family:Helvetica;
                            /*@editable*/font-size:26px;
                            /*@editable*/font-style:normal;
                            /*@editable*/font-weight:bold;
                            /*@editable*/line-height:125%;
                            /*@editable*/letter-spacing:normal;
                            /*@editable*/text-align:left;
                        }
                        /*
                        @tab Page
                        @section Heading 2
                        @tip Set the styling for all second-level headings in your emails.
                        @style heading 2
                        */
                        h2{
                            /*@editable*/color:#202020;
                            /*@editable*/font-family:Helvetica;
                            /*@editable*/font-size:22px;
                            /*@editable*/font-style:normal;
                            /*@editable*/font-weight:bold;
                            /*@editable*/line-height:125%;
                            /*@editable*/letter-spacing:normal;
                            /*@editable*/text-align:left;
                        }
                        /*
                        @tab Page
                        @section Heading 3
                        @tip Set the styling for all third-level headings in your emails.
                        @style heading 3
                        */
                        h3{
                            /*@editable*/color:#202020;
                            /*@editable*/font-family:Helvetica;
                            /*@editable*/font-size:20px;
                            /*@editable*/font-style:normal;
                            /*@editable*/font-weight:bold;
                            /*@editable*/line-height:125%;
                            /*@editable*/letter-spacing:normal;
                            /*@editable*/text-align:left;
                        }
                        /*
                        @tab Page
                        @section Heading 4
                        @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
                        @style heading 4
                        */
                        h4{
                            /*@editable*/color:#202020;
                            /*@editable*/font-family:Helvetica;
                            /*@editable*/font-size:18px;
                            /*@editable*/font-style:normal;
                            /*@editable*/font-weight:bold;
                            /*@editable*/line-height:125%;
                            /*@editable*/letter-spacing:normal;
                            /*@editable*/text-align:left;
                        }
                        /*
                        @tab Preheader
                        @section Preheader Style
                        @tip Set the background color and borders for your email's preheader area.
                        */
                        #templatePreheader{
                            /*@editable*/background-color:#fafafa;
                            /*@editable*/background-image:none;
                            /*@editable*/background-repeat:no-repeat;
                            /*@editable*/background-position:center;
                            /*@editable*/background-size:cover;
                            /*@editable*/border-top:0;
                            /*@editable*/border-bottom:0;
                            /*@editable*/padding-top:0px;
                            /*@editable*/padding-bottom:0px;
                        }
                        /*
                        @tab Preheader
                        @section Preheader Text
                        @tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
                        */
                        #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
                            /*@editable*/color:#656565;
                            /*@editable*/font-family:Helvetica;
                            /*@editable*/font-size:12px;
                            /*@editable*/line-height:150%;
                            /*@editable*/text-align:left;
                        }
                        /*
                        @tab Preheader
                        @section Preheader Link
                        @tip Set the styling for your email's preheader links. Choose a color that helps them stand out from your text.
                        */
                        #templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
                            /*@editable*/color:#656565;
                            /*@editable*/font-weight:normal;
                            /*@editable*/text-decoration:underline;
                        }
                        /*
                        @tab Header
                        @section Header Style
                        @tip Set the background color and borders for your email's header area.
                        */
                        #templateHeader{
                            /*@editable*/background-color:#efefef;
                            /*@editable*/background-image:none;
                            /*@editable*/background-repeat:no-repeat;
                            /*@editable*/background-position:center;
                            /*@editable*/background-size:cover;
                            /*@editable*/border-top:0;
                            /*@editable*/border-bottom:0;
                            /*@editable*/padding-top:9px;
                            /*@editable*/padding-bottom:0;
                        }
                        /*
                        @tab Header
                        @section Header Text
                        @tip Set the styling for your email's header text. Choose a size and color that is easy to read.
                        */
                        #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
                            /*@editable*/color:#202020;
                            /*@editable*/font-family:Helvetica;
                            /*@editable*/font-size:16px;
                            /*@editable*/line-height:150%;
                            /*@editable*/text-align:left;
                        }
                        /*
                        @tab Header
                        @section Header Link
                        @tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
                        */
                        #templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
                            /*@editable*/color:#2BAADF;
                            /*@editable*/font-weight:normal;
                            /*@editable*/text-decoration:underline;
                        }
                        /*
                        @tab Upper Body
                        @section Upper Body Style
                        @tip Set the background color and borders for your email's upper body area.
                        */
                        #templateUpperBody{
                            /*@editable*/background-color:#FFFFFF;
                            /*@editable*/background-image:none;
                            /*@editable*/background-repeat:no-repeat;
                            /*@editable*/background-position:center;
                            /*@editable*/background-size:cover;
                            /*@editable*/border-top:0;
                            /*@editable*/border-bottom:0;
                            /*@editable*/padding-top:0;
                            /*@editable*/padding-bottom:0;
                        }
                        /*
                        @tab Upper Body
                        @section Upper Body Text
                        @tip Set the styling for your email's upper body text. Choose a size and color that is easy to read.
                        */
                        #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{
                            /*@editable*/color:#202020;
                            /*@editable*/font-family:Helvetica;
                            /*@editable*/font-size:16px;
                            /*@editable*/line-height:150%;
                            /*@editable*/text-align:left;
                        }
                        /*
                        @tab Upper Body
                        @section Upper Body Link
                        @tip Set the styling for your email's upper body links. Choose a color that helps them stand out from your text.
                        */
                        #templateUpperBody .mcnTextContent a,#templateUpperBody .mcnTextContent p a{
                            /*@editable*/color:#2BAADF;
                            /*@editable*/font-weight:normal;
                            /*@editable*/text-decoration:underline;
                        }
                        /*
                        @tab Columns
                        @section Column Style
                        @tip Set the background color and borders for your email's columns.
                        */
                        #templateColumns{
                            /*@editable*/background-color:#f7da96;
                            /*@editable*/background-image:none;
                            /*@editable*/background-repeat:no-repeat;
                            /*@editable*/background-position:center;
                            /*@editable*/background-size:cover;
                            /*@editable*/border-top:0;
                            /*@editable*/border-bottom:0;
                            /*@editable*/padding-top:10px;
                            /*@editable*/padding-bottom:10px;
                        }
                        /*
                        @tab Columns
                        @section Column Text
                        @tip Set the styling for your email's column text. Choose a size and color that is easy to read.
                        */
                        #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
                            /*@editable*/color:#202020;
                            /*@editable*/font-family:Helvetica;
                            /*@editable*/font-size:16px;
                            /*@editable*/line-height:150%;
                            /*@editable*/text-align:left;
                        }
                        /*
                        @tab Columns
                        @section Column Link
                        @tip Set the styling for your email's column links. Choose a color that helps them stand out from your text.
                        */
                        #templateColumns .columnContainer .mcnTextContent a,#templateColumns .columnContainer .mcnTextContent p a{
                            /*@editable*/color:#2BAADF;
                            /*@editable*/font-weight:normal;
                            /*@editable*/text-decoration:underline;
                        }
                        /*
                        @tab Lower Body
                        @section Lower Body Style
                        @tip Set the background color and borders for your email's lower body area.
                        */
                        #templateLowerBody{
                            /*@editable*/background-color:#f7da96;
                            /*@editable*/background-image:none;
                            /*@editable*/background-repeat:no-repeat;
                            /*@editable*/background-position:center;
                            /*@editable*/background-size:cover;
                            /*@editable*/border-top:0;
                            /*@editable*/border-bottom:2px solid #EAEAEA;
                            /*@editable*/padding-top:10px;
                            /*@editable*/padding-bottom:10px;
                        }
                        /*
                        @tab Lower Body
                        @section Lower Body Text
                        @tip Set the styling for your email's lower body text. Choose a size and color that is easy to read.
                        */
                        #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{
                            /*@editable*/color:#202020;
                            /*@editable*/font-family:Helvetica;
                            /*@editable*/font-size:16px;
                            /*@editable*/line-height:150%;
                            /*@editable*/text-align:left;
                        }
                        /*
                        @tab Lower Body
                        @section Lower Body Link
                        @tip Set the styling for your email's lower body links. Choose a color that helps them stand out from your text.
                        */
                        #templateLowerBody .mcnTextContent a,#templateLowerBody .mcnTextContent p a{
                            /*@editable*/color:#2BAADF;
                            /*@editable*/font-weight:normal;
                            /*@editable*/text-decoration:underline;
                        }
                        /*
                        @tab Footer
                        @section Footer Style
                        @tip Set the background color and borders for your email's footer area.
                        */
                        #templateFooter{
                            /*@editable*/background-color:#5cad01;
                            /*@editable*/background-image:none;
                            /*@editable*/background-repeat:no-repeat;
                            /*@editable*/background-position:center;
                            /*@editable*/background-size:cover;
                            /*@editable*/border-top:0;
                            /*@editable*/border-bottom:0;
                            /*@editable*/padding-top:9px;
                            /*@editable*/padding-bottom:9px;
                        }
                        /*
                        @tab Footer
                        @section Footer Text
                        @tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
                        */
                        #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
                            /*@editable*/color:#656565;
                            /*@editable*/font-family:Helvetica;
                            /*@editable*/font-size:12px;
                            /*@editable*/line-height:150%;
                            /*@editable*/text-align:center;
                        }
                        /*
                        @tab Footer
                        @section Footer Link
                        @tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
                        */
                        #templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
                            /*@editable*/color:#656565;
                            /*@editable*/font-weight:normal;
                            /*@editable*/text-decoration:underline;
                        }

                    </style></head>
                    <body>
                        <!--*|IF:MC_PREVIEW_TEXT|*-->
                        <!--[if !gte mso 9]><!---->
                        <!--<span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">*|MC_PREVIEW_TEXT|*</span><![endif]-->
                        <!--*|END:IF|*-->
                        <center>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                                <tr>
                                    <td align="center" valign="top" id="bodyCell">
                                        <!-- BEGIN TEMPLATE // -->
                                        <!--[if (gte mso 9)|(IE)]>
                                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                        <tr>
                                        <td align="center" valign="top" width="600" style="width:600px;">
                                        <![endif]-->
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                            <tr>
                                                <td valign="top" id="templatePreheader"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                                                        <tbody class="mcnTextBlockOuter">
                                                            <tr>
                                                                <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                                                    <!--[if mso]>
                                                                                    <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                                                                    <tr>
                                                                                    <![endif]-->

                                                                    <!--[if mso]>
                                                                    <td valign="top" width="600" style="width:600px;">
                                                                    <![endif]-->
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                                                        <tbody><tr>

                                                                                <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px; text-align: center;">

                                                                                    <!--<a href="*|ARCHIVE|*" target="_blank">View this email in your browser</a>-->
                                                                                </td>
                                                                            </tr>
                                                                        </tbody></table>
                                                                    <!--[if mso]>
                                                                    </td>
                                                                    <![endif]-->

                                                                    <!--[if mso]>
                                                                    </tr>
                                                                    </table>
                                                                    <![endif]-->
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                            </tr>
                                            <tr>
                                                <td valign="top" id="templateHeader"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
                                                        <tbody class="mcnImageBlockOuter">
                                                            <tr>
                                                                <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                                                                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                                                                        <tbody><tr>
                                                                                <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">


                                                                                    <img align="center" alt="" src="<?php echo BASEURL_EIMG ?>2790ce73-586e-4dde-9446-dcf8460ceeba.png" width="256" style="max-width:256px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">


                                                                                </td>
                                                                            </tr>
                                                                        </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
                                                        <tbody class="mcnImageBlockOuter">
                                                            <tr>
                                                                <td valign="top" style="padding:0px" class="mcnImageBlockInner">
                                                                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                                                                        <tbody><tr>
                                                                                <td class="mcnImageContent" valign="top" style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;">


                                                                                    <img align="center" alt="" src="<?php echo BASEURL_EIMG ?>dfd635a4-6fee-4f4b-81bb-5942f18943cc.png" width="600" style="max-width:1000px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">


                                                                                </td>
                                                                            </tr>
                                                                        </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                            </tr>
                                            <tr>
                                                <td valign="top" id="templateUpperBody"></td>
                                            </tr>
                                            <tr>
                                                <td valign="top" id="templateColumns">
                                                    <!--[if (gte mso 9)|(IE)]>
                                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                                    <tr>
                                                    <td align="center" valign="top" width="300" style="width:300px;">
                                                    <![endif]-->
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                                                        <tr>
                                                            <td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                                                                    <tbody class="mcnTextBlockOuter">
                                                                        <tr>
                                                                            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                                                                <!--[if mso]>
                                                                                                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                                                                                <tr>
                                                                                                <![endif]-->

                                                                                <!--[if mso]>
                                                                                <td valign="top" width="300" style="width:300px;">
                                                                                <![endif]-->
                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                                                                    <tbody><tr>

                                                                                            <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 100%; text-align: center;">
                                                                                                <br><br>
                                                                                                        <span style="background-position:center right; background-size:cover; display:block; font-size:16px; font-weight:500; line-height:15px; margin-left:0px; margin-right:0px;">We wish to remind you that</span><br> <span style="display:block; font-size:18px; font-weight:600; line-height:15px"><?php echo trim($message); ?></span>
                                                                                                            <span style="display:block; font-size:18px; font-weight:600; line-height:25px;margin-bottom: 5px;"><?php echo trim($suffix); ?></span><span style="display:block; font-size:20px; font-weight:600; line-height:25px"><?php echo trim($bdate); ?></span>
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            </tbody></table>
                                                                                                            <!--[if mso]>
                                                                                                            </td>
                                                                                                            <![endif]-->

                                                                                                            <!--[if mso]>
                                                                                                            </tr>
                                                                                                            </table>
                                                                                                            <![endif]-->
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            </tbody>
                                                                                                            </table></td>
                                                                                                            </tr>
                                                                                                            </table>
                                                                                                            <!--[if (gte mso 9)|(IE)]>
                                                                                                            </td>
                                                                                                            <td align="center" valign="top" width="300" style="width:300px;">
                                                                                                            <![endif]-->
                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                                                                                                                <tr>
                                                                                                                    <td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
                                                                                                                            <tbody class="mcnImageBlockOuter">
                                                                                                                                <tr>
                                                                                                                                    <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                                                                                                                                        <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                                                                                                                                            <tbody><tr>
                                                                                                                                                    <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">


                                                                                                                                                        <img align="center" alt="" src="<?php echo BASEURL_EIMG ?>ff48849a-31de-486f-9276-9f1dfb69919c.png" width="233" style="max-width:233px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">


                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                            </tbody></table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table></td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                            <!--[if (gte mso 9)|(IE)]>
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            </table>
                                                                                                            <![endif]-->
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td valign="top" id="templateLowerBody"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
                                                                                                                        <tbody class="mcnButtonBlockOuter">
                                                                                                                            <tr>
                                                                                                                                <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="center" class="mcnButtonBlockInner">
                                                                                                                                    <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 8px;background-color: #5CAD01;">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 14px; padding: 12px;">
                                                                                                                                                    <a class="mcnButton " title="Click Here" href="https://www.birthdayowl.com/birthday-reminder" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Click Here</a>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                                                                                                                        <tbody class="mcnTextBlockOuter">
                                                                                                                            <tr>
                                                                                                                                <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                                                                                                                    <!--[if mso]>
                                                                                                                                                    <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                                                                                                                                    <tr>
                                                                                                                                                    <![endif]-->

                                                                                                                                    <!--[if mso]>
                                                                                                                                    <td valign="top" width="600" style="width:600px;">
                                                                                                                                    <![endif]-->
                                                                                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                                                                                                                        <tbody><tr>

                                                                                                                                                <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px; font-style: normal; font-weight: bold; line-height: 100%; text-align: center;">

                                                                                                                                                    <span style="font-size:16px"><strong><a href="www.birthdayowl.com/birthday-reminder" target="_blank" style="text-decoration: none"><span style="color:#000000;font-weight:600;">www.birthdayowl.com/birthday-reminder</span></a></strong></span>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody></table>
                                                                                                                                    <!--[if mso]>
                                                                                                                                    </td>
                                                                                                                                    <![endif]-->

                                                                                                                                    <!--[if mso]>
                                                                                                                                    </tr>
                                                                                                                                    </table>
                                                                                                                                    <![endif]-->
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                                                                                                                        <tbody class="mcnTextBlockOuter">
                                                                                                                            <tr>
                                                                                                                                <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                                                                                                                    <!--[if mso]>
                                                                                                                                                    <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                                                                                                                                    <tr>
                                                                                                                                                    <![endif]-->

                                                                                                                                    <!--[if mso]>
                                                                                                                                    <td valign="top" width="600" style="width:600px;">
                                                                                                                                    <![endif]-->
                                                                                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                                                                                                                        <tbody><tr>

                                                                                                                                                <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                                                                                                                                    <div style="text-align: center;"><img align="center" alt="" src="<?php echo BASEURL_EIMG ?>button.png" width="600" style="max-width:600px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" ></span></div>

                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody></table>
                                                                                                                                    <!--[if mso]>
                                                                                                                                    </td>
                                                                                                                                    <![endif]-->

                                                                                                                                    <!--[if mso]>
                                                                                                                                    </tr>
                                                                                                                                    </table>
                                                                                                                                    <![endif]-->
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td valign="top" id="templateFooter"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
                                                                                                                        <tbody class="mcnImageBlockOuter">
                                                                                                                            <tr>
                                                                                                                                <td valign="top" style="padding:0px" class="mcnImageBlockInner">
                                                                                                                                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                                                                                                                                        <tbody><tr>
                                                                                                                                                <td class="mcnImageContent" valign="top" style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;">


                                                                                                                                                    <img align="center" alt="" src="<?php echo BASEURL_EIMG ?>72afea6d-a34b-4e7a-a520-a61959eb43a5.png" width="600" style="max-width:600px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">


                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody></table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table></td>
                                                                                                            </tr>
                                                                                                            </table>
                                                                                                            <!--[if (gte mso 9)|(IE)]>
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            </table>
                                                                                                            <![endif]-->
                                                                                                            <!-- // END TEMPLATE -->
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                                    <tbody class="mcnFollowBlockOuter">
                                                                                                                        <tr>
                                                                                                                            <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                                                                                                                                <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                                                    <tbody>
                                                                                                                                        <tr>
                                                                                                                                            <td style="padding-left:9px;padding-right:9px;" align="center">
                                                                                                                                                <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                                                                    <tbody>
                                                                                                                                                        <tr>
                                                                                                                                                            <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                                                                                                                                                                <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                                                                                    <tbody>
                                                                                                                                                                        <tr>
                                                                                                                                                                            <td valign="top" align="center">
                                                                                                                                                                                <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                                                                    <tbody><tr>
                                                                                                                                                                                            <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                                                                                                                <a href="https://www.instagram.com/birthday_owl_reminder/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/instagram-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                                                                                                            </td>
                                                                                                                                                                                        </tr>
                                                                                                                                                                                    </tbody>
                                                                                                                                                                                </table>
                                                                                                                                                                                <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                                                                    <tbody><tr>
                                                                                                                                                                                            <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                                                                                                                <a href="https://www.facebook.com/birthdayowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/facebook-ico.png" alt="Facebook" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                                                                                                            </td>
                                                                                                                                                                                        </tr>
                                                                                                                                                                                    </tbody>
                                                                                                                                                                                </table>
                                                                                                                                                                                <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                                                                    <tbody><tr>
                                                                                                                                                                                            <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                                                                                                                <a href="https://twitter.com/birthday_owl" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/twitter-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                                                                                                            </td>
                                                                                                                                                                                        </tr>
                                                                                                                                                                                    </tbody>
                                                                                                                                                                                </table>
                                                                                                                                                                                <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                                                                    <tbody><tr>
                                                                                                                                                                                            <td class="mcnFollowIconContent" style="padding-right:0; padding-bottom:9px;" valign="top" align="center">
                                                                                                                                                                                                <a href="https://www.pinterest.com/birthdyowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/pinterest-ico.png" alt="Pinterest" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                                                                                                            </td>
                                                                                                                                                                                        </tr>
                                                                                                                                                                                    </tbody>
                                                                                                                                                                                </table>
                                                                                                                                                                            </td>
                                                                                                                                                                        </tr>
                                                                                                                                                                    </tbody>
                                                                                                                                                                </table>
                                                                                                                                                            </td>
                                                                                                                                                        </tr>
                                                                                                                                                    </tbody>
                                                                                                                                                </table>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </tbody>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            </tr>
                                                                                                            </table>
                                                                                                            </center>
                                                                                                            </body>
                                                                                                            </html>
