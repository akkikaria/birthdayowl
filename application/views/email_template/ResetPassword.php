<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>BirthdayOwl</title>
        <link rel="shortcut icon" type="image/png" href="<?php echo BASEURL_OIMG; ?>main_icn.png"/>
    </head>
    <body>
        <table  width="600" cellspacing="0" cellpadding="0" border="0" align="center">
            <tr>
                <td valign="top" style="border:solid 1px #969696;"><table  width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tr>
                            <td valign="top"><img src="<?php echo BASEURL_EIMG ?>forgt-bnr.png" height="227" width="600" title="birthdayowl" alt="birthdayowl" border="0" style="display:block;"  /> </td>
                        </tr> 
                        <tr>
                            <td valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000; font-weight:normal;padding:0 15px 15px;">You requested to reset your password on <a href="<?php echo WEB_HOME; ?>">birthdayowl.com</a> , to proceed with resetting your password click on the following link: 
                            </td>
                        </tr>  
                        <tr>
                            <td valign="top" align="center" style="padding:0 20px 20px;"><a href="<?php echo $link; ?>" title="Reset Password" style="text-decoration:none;"><img src="<?php echo BASEURL_EIMG ?>reset-btn.png" height="44" width="198" title="Reset Password" alt="Reset Password" border="0" style="display:block;"  /></a> </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000; font-weight:normal;padding:0 15px 15px;">This link is valid only for 24 hours.
                            </td>
                        </tr> 
                        <tr>
                            <td valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000; font-weight:normal;padding:0 15px 15px;">If you didn't request to reset your password then please ignore this email and we apologize for the mistake.
                            </td>
                        </tr> 
                        <tr>
                            <td valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000; font-weight:normal;padding:0 15px 15px;">Thanks <br />
                                The Team<br />
                                <a href="<?php echo WEB_HOME; ?>">birthdayowl.com</a>

                            </td>
                        </tr> 
                    </table></td>
            </tr>
            <tr>
                <td  valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000; font-weight:normal;padding:15px; ">For more information please read our <a href="<?php echo WEB_PRIVACY_AND_POLICY; ?>" title="Privacy Policy" style="text-decoration:none; color:#1155cc;">Privacy & Security Policy</a>.<br />
                    Copyright © 2016 birthdayowl.com. All rights reserved.</td>
            </tr>
            <tr>
                <td>
                    <table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody class="mcnFollowBlockOuter">
                            <tr>
                                <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                                    <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="padding-left:9px;padding-right:9px;" align="center">
                                                    <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                                                                    <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td valign="top" align="center">
                                                                                    <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                        <tbody><tr>
                                                                                                <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                    <a href="https://www.instagram.com/birthday_owl_reminder/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/instagram-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                        <tbody><tr>
                                                                                                <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                    <a href="https://www.facebook.com/birthdayowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/facebook-ico.png" alt="Facebook" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                        <tbody><tr>
                                                                                                <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                    <a href="https://twitter.com/birthday_owl" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/twitter-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                        <tbody><tr>
                                                                                                <td class="mcnFollowIconContent" style="padding-right:0; padding-bottom:9px;" valign="top" align="center">
                                                                                                    <a href="https://www.pinterest.com/birthdyowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/pinterest-ico.png" alt="Pinterest" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
