<?php /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ ?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title></title>
        <style type="text/css">
            body{
                font-family: verdana, arial, sans-serif;
            }
        </style>
    </head>
    <body>
        <div style="width: 100%; text-align: center;margin:auto;background-color: #efefef;height:auto;padding-top: 22px;">
            <table   cellspacing="0" cellpadding="0" border="0" align="center"  >
                <tr>
                    <td  align="center">
                        <img src="<?php echo BASEURL_OIMG ?>logo.png" />
                    </td>
                </tr>
                <tr >
                    <td  align="center" style="background-color:#52A303;">
                        <img src="<?php echo BASEURL_EIMG ?>reminder_header_new.png" style="width:540px;" />

                    </td>
                </tr>
                <tr align="center" style="background-color:white;">
                    <td>
                        <img src="<?php echo BASEURL_EIMG ?>remowl.png" style="width:539px;" /> 
                    </td>

                </tr>
                <tr style="background-color:white;" >
                    <td align='center'>

                        <span style="font-size:15px;">We wish to remind you that your first 10 e-greeting cards are </span><br/>
                        <span style="font-size:15px;">absolutely free,and you now have only 1 remaining  </span><br/>
                        <span style="font-size:15px;">free e-greeting card</span><br/>

                        <p style="border-top:2px solid #57A801;width:440px;"></p>
                        <span style="font-size:15px;line-height: 25px">We highly recommend that you register with us a member as it</span><br/>
                        <span style="font-size:15px;line-height: 25px">takes only a minute and the yearly membership fee</span><br/>
                        <span style="font-size:15px;">is less than the cost of a couple of snacks at a restaurant.</span><br/><br/>
                        <p style="border-top:2px solid #57A801;width:440px;"></p>
                    </td>
                </tr>
                <tr align="center">
                    <td>
                        <img src="<?php echo BASEURL_EIMG ?>remowlfooter.png" style="width:539px;" /> 
                    </td>

                </tr>
                <tr>
                    <td  valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; font-weight:normal;padding:15px; ">For more information please read our <a href="<?php echo WEB_PRIVACY_AND_POLICY; ?>" title="Privacy Policy" style="text-decoration:none; color:#1155cc;">Privacy Policy</a>.<br />
                        Copyright © 2016 birthdayowl.com. All rights reserved.</td>
                </tr>
                <tr>
                    <td>
                        <table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody class="mcnFollowBlockOuter">
                                <tr>
                                    <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                                        <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding-left:9px;padding-right:9px;" align="center">
                                                        <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                                                                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="top" align="center">
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.instagram.com/birthday_owl_reminder/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/instagram-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.facebook.com/birthdayowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/facebook-ico.png" alt="Facebook" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://twitter.com/birthday_owl" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/twitter-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:0; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.pinterest.com/birthdyowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/pinterest-ico.png" alt="Pinterest" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>

        </div>
    </body>
</html>