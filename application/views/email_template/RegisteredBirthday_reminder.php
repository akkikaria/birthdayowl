<?php /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ ?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title></title>
        <style type="text/css">
            body{
                font-family: verdana, arial, sans-serif;
            }
        </style>
    </head>
    <body>
        <div style="width: 100%; text-align: center;margin:auto;background-color: #efefef;height:auto;padding-top: 22px;">
            <table   cellspacing="0" cellpadding="0" border="0" align="center"  >
                <tr>
                    <td  align="center">
                        <img src="<?php echo BASEURL_OIMG ?>logo.png" />
                    </td>
                </tr>
                <tr>
                    <td  align="center" style="background-color:#51A203;">

                        <div style="background-color:#51A203;height:100px;color:white;padding: 10px;font-family: Tahoma;line-height: 29px">
                            <span style="font-size:22px;">Hi <?php echo $first_name ?></span><br/>
                            <span style="font-size:20px;">Thanks for being registered with</span><br/>
                            <span style="font-size:20px;">birthdayowl.com</span>

                        </div>

                    </td>
                </tr>
                <tr>
                    <td align='center' style="background-color:white;">
                        <div>  
                            <br/>
                            <span style="font-size:17px;font-family: Tahoma">We know today is your big day, and we never forget!</span><br/>

                            <span style="font-size:16px;font-family: Tahoma"><b>Happy Birthday "<?php echo strtoupper($first_name) ?>"</b></span><br/>
                            <span style="font-size:17px;font-family: Tahoma">have a great day ahead</span><br/>
                            <img src="<?php echo BASEURL_EIMG ?>birthday_wish.png"  /> 
                            <span style="font-size:14.5px;line-height: 25px;font-family: Tahoma;">  </span><br/>
                            <span style="font-size:15px;">We hope you also enjoy using our other features like <br/>
                                <b>Zodiac Signs and e-Greeting cards with audio and video options.</b><br/>
                                And you can purchase and send e-Gift Cards to your loved ones<br/>
                                anytime; they get delivered instantly<br/>

                            </span><br/><br/>

                        </div>


                    </td>
                </tr>
                <tr align="center">
                    <td>
                        <img src="<?php echo BASEURL_EIMG ?>remowlfooter.png" style="width:539px;" /> 
                    </td>

                </tr>
                <tr>
                    <td  valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; font-weight:normal;padding:15px; ">For more information please read our <a href="<?php echo WEB_PRIVACY_AND_POLICY; ?>" title="Privacy Policy" style="text-decoration:none; color:#1155cc;">Privacy Policy</a> and <a href="<?php echo BASEURL; ?>terms-of-use" title="Terms of Use" style="text-decoration:none; color:#1155cc;">Terms of Use</a>.<br />
                        Copyright © 2016 birthdayowl.com. All rights reserved.<br/>

                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody class="mcnFollowBlockOuter">
                                <tr>
                                    <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                                        <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding-left:9px;padding-right:9px;" align="center">
                                                        <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                                                                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="top" align="center">
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.instagram.com/birthday_owl_reminder/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/instagram-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.facebook.com/birthdayowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/facebook-ico.png" alt="Facebook" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://twitter.com/birthday_owl" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/twitter-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:0; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.pinterest.com/birthdyowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/pinterest-ico.png" alt="Pinterest" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>

        </div>
    </body>
</html>