<?php /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ ?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title></title>
        <style type="text/css">
            body{
                font-family: verdana, arial, sans-serif;
            }
        </style>
    </head>
    <body>
        <div style="width: 100%; text-align: center;margin:auto;background-color: #efefef;height:auto;padding-top: 22px;">
            <table   cellspacing="0" cellpadding="0" border="0" align="center"  >
                <tr>
                    <td  align="center">
                        <img src="<?php echo BASEURL_OIMG ?>logo.png" />
                    </td>
                </tr>
                <tr >
                    <td  align="center" style="background-color:#52A303;">
                        <img src="<?php echo BASEURL_EIMG ?>reminder_header_new.png" style="width:540px;" />

                    </td>
                </tr>
                <tr align="center" style="background-color:white;">
                    <td>
                        <img src="<?php echo BASEURL_EIMG ?>remowl.png" style="width:539px;" /> 
                    </td>

                </tr>
                <tr>
                    <td align='center' style="background-color:white;">
                        <div>                           
                            <span style="font-size:15px;font-family: Tahoma">We wish to remind you that <?php echo trim($message); ?></span><br/>
                            <span><?php echo trim($suffix); ?></span><br/>
                            <span style="font-size:16px;"><b><?php echo trim($bdate); ?></b></span><br/>
                            <p style="border-top:2px solid #57A801;width:440px;"></p>
                            <span style="width:150px;font-size:15px;"><b>Click here</b></span><br/> <br/>  
                            <a href="<?php echo WEB_HOME; ?>" style="cursor:pointer;"><img src="<?php echo BASEURL_EIMG ?>bday_reminder.png" /></a><br/><br/>  
                            <span style="font-size:15px;line-height: 25px;font-family: Tahoma;">to send a greeting card or e-Gift Cards of your choice on the  </span><br/>
                            <span style="font-size:15px;">  date you choose to have it delivered, and we will do the rest</span><br/><br/>
                            <p style="border-top:2px solid #57A801;width:440px;height: 1px;"></p>
                        </div>


                    </td>
                </tr>
                <tr align="center">
                    <td>
                        <img src="<?php echo BASEURL_EIMG ?>remowlfooter.png" style="width:539px;" /> 
                    </td>

                </tr>
                <tr>
                    <td valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; font-weight:normal;padding:15px; ">For more information please read our <a href="<?php echo WEB_PRIVACY_AND_POLICY; ?>" title="Privacy Policy" style="text-decoration:none; color:#1155cc;">Privacy Policy</a> and <a href="<?php echo BASEURL; ?>Home_web/terms_n_conditions" title="Terms of Use" style="text-decoration:none; color:#1155cc;">Terms of Use</a>.<br />
                        Copyright © 2016 birthdayowl.com. All rights reserved.<br/>
                    </td>
                </tr>
            </table>

        </div>
    </body>
</html>