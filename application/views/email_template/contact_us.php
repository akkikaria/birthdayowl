<?php /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title></title>
        <style type="text/css">
            a{
                text-decoration: none;
            }
            img{
                margin: 5px;
            }
            h3{
                margin: 0;
                padding: 0;
            }
            body{
                font-family: verdana, arial, sans-serif;
            }
        </style>
    </head>
    <body>
        <div style="width: 100%; text-align: center;">

            <div style="text-align: center; margin: auto;  padding: 10px;">

                <div style="width: 380px;  text-align: left;  border-radius: 10px; border: none;">
                    <table style="width: 100%;  table-layout: fixed;" cellpadding="0" cellspacing="2">
                        <tr style="border-radius: 10px 10px 0 0;" align="left"> <!-- header -->
                          
                        </tr>
                        <tr style="text-align: left; vertical-align: top; height: 100px;">  <!-- message -->
                            <td >
                                <p style="margin: 0; padding: 5px;"><b>Contact Name:<b><?php echo $contact_name; ?></b></p>
                                <p style="margin: 0; padding: 5px;">Contact No: <b><?php echo $contact_no; ?></b></p><br/>
                                 <p style="margin: 0; padding: 5px;">Contact Email: <b><?php echo $contact_email; ?></b></p><br/>
                                   <p style="margin: 0; padding: 5px;">Message: <b><?php echo $enquiry; ?></b></p><br/>
                                <p style="margin: 0; padding: 5px;">Helping you never forget again,The Birthday-Matters Team</p>
                                <p style="margin: 0; padding: 5px;">Birthday-Matters.com - Never Forget Again</p>

                            </td>
                        </tr>                        
                    </table>
                </div>
            </div>            
        </div>
    </body>
</html>