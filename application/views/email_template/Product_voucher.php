<?php /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ ?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title></title>
        <style type="text/css">


            body{
                font-family: verdana, arial, sans-serif;
            }
        </style>
        <script type="text/javascript" src="<?PHP echo BASEURL_OJS ?>jquery-1.10.2.min.js"></script>
    </head>
    <body>
        <input type="hidden"  id="order_pro_id" value="<?php echo $order_pro_id; ?>" />
        <div style="width: 100%; text-align: center;margin:auto;background-color: #efefef;height:auto;padding-top: 20px">
            <table   cellspacing="0" cellpadding="0" border="0" align="center" style="width:500px;">
                <tr valign="center" align="center"   >
                    <td>
                        <table style="background:url(<?php echo BASEURL_EIMG ?>email_voucher.png);height:525px;width:515px;">
                            <tr>
                                <td valign="center"  align="center" colspan="5">
                                    <table align="center">
                                        <tr align="center" >
                                            <td align="center"  colspan="3">
                                                <br/><br/><br/><br/><br/><br/>
                                                <div style="height:123px;">
                                                    <img src="<?php echo $product_image; ?>" title="birthdayowl" alt="birthdayowl" border="0" style=""  /> 
                                                </div>
                                                <br/>
                                                <span style="font-size:15px;"> <?php echo $voucher_pro_name; ?>  to</span><br/>
                                                <span style="font-size:22px;"><b><?php echo ucfirst($fname) ?></b></span><br/><br/>
                                            </td>
                                        </tr>
                                        <tr align="center" valign="center">
                                            <td valign="bottom" align="center" width="200" style="padding-left:50px;">

                                                <div style="text-align:left;font-size: 28px;height:20px;"><b>Rs.&nbsp;<?php echo $total; ?></b></div><br/>
                                                <div style="text-align:left;font-size: 12px;">
                                                    <?php $expiry_daten = date("d F, Y", strtotime($expiry_date)); ?>
                                                    valid until <?php echo $expiry_daten; ?>
                                                </div>
                                            </td> 
                                            <td rowspan="1" align="center" style="border-left:1px solid gray;" ></td>
                                            <td valign="bottom" align="center" width="250" style="padding-left:5px;">
                                                <div style="font-size: 12px;text-align: left;">e-Gift Code:<?php echo $code; ?></div> <br/>
                                                <div style="font-size: 12px;text-align: left;">PIN:<?php echo $pin; ?></div>

                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td colspan="3">
                                                <br/>
                                                <?php $link = BASEURL . "Home_web/voucher_email/$euser_id/$pay_id/$greeting_id/$opid/$key"; ?>
                                                <a href="<?php echo $link; ?>" style="text-decoration:none;"><img src="<?php echo BASEURL_EIMG ?>get_my_card.png" /></a>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>


                </tr>
                <tr style="width:120px;">
                    <td valign="center"  align="center" >
                        <div style="background-color: white;padding-left:10px;padding-right: 10px;width: 418px;height:180px;">
                            <div style="font-size: 14px; height: 15px; padding-top: 15px;"><span style="font-family:Apple Chancery;">from</span><span style="font-family: Gotham"><b> <?php echo ucfirst($uname) ?></b></span></div> <br/>
                            <div style="text-align: center;font-size: 12px;padding-top: 5px;height: 50px;word-spacing: 2px;">
                                <span>Present this e-Gift Cards at store for redemption.</span> <br/>
                                <span> You may also print the attached foldable version.</span>

                            </div>

                            <span style="text-align: left;font-size: 12px;padding-top: 5px;height: 250px;word-spacing: 2px;">Alternatively, download birthdayowl.com App and have your e-Gift Code automatically stored in the Gift Wallet feature for quick and easy access.</span>
                        </div>
                    </td>
                </tr>
                <tr  align="center">
                    <td>
                        <img src="<?php echo BASEURL_EIMG ?>google_play.png" style="width:435px;" />  
                    </td> 
                </tr>

                <tr style="width:120px;">
                    <td valign="center"  align="center" >
                        <div style="background-color: white;padding-left:10px;padding-right: 10px;width: 418px;">
                            <div style="font-size: 12px;float:left;padding-top: 5px;">
                                <b>Terms & Conditions</b><br/>
                            </div><br/>
                            <div style="text-align: left;font-size: 12px;padding-top: 5px;height: auto;word-spacing: 4px;">
                                <?php echo $terms; ?>
                                <br/>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr  align="center">
                    <td>
                        <div style="background:white;width:438px;">
                            <img src="<?php echo BASEURL_EIMG ?>bottom_border_1.png" style="width:435px;" />  
                        </div>
                    </td> 
                </tr>
                <tr>
                    <td  valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; font-weight:normal;padding:15px; ">For more information please read our <a href="<?php echo WEB_PRIVACY_AND_POLICY; ?>" title="Privacy Policy" style="text-decoration:none; color:#1155cc;">Privacy Policy</a> and <a href="<?php echo WEB_TERMS_OF_USE_EGIFT; ?>" title="Terms of Use" style="text-decoration:none; color:#1155cc;">Terms of Use</a>.<br />
                        Copyright © 2016 birthdayowl.com. All rights reserved.</td>
                </tr>
                <tr>
                    <td>
                        <table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody class="mcnFollowBlockOuter">
                                <tr>
                                    <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                                        <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding-left:9px;padding-right:9px;" align="center">
                                                        <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                                                                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="top" align="center">
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.instagram.com/birthday_owl_reminder/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/instagram-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.facebook.com/birthdayowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/facebook-ico.png" alt="Facebook" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://twitter.com/birthday_owl" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/twitter-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:0; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.pinterest.com/birthdyowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/pinterest-ico.png" alt="Pinterest" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>

        </div>

              <!--<img border='0' src='<?php // echo BASEURL."Home_web/sendegiftOpenedMail/".$order_pro_id;    ?>'  alt='image for email' ></img>-->

    </body>
</html>