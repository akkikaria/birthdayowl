<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Emailer</title>
    </head>
    <body>
        <table  width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="color:#000; font-family:Arial, Helvetica, sans-serif; font-size:13px;">
            <tr>
                <td valign="top" ><table  width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tr>
                            <td valign="top"><img src="<?php echo BASEURL_EIMG; ?>emailer-banner.png" height="328" width="600" title="birthdayowl" alt="birthdayowl" border="0" style="display:block;"  /> </td>
                        </tr> 
                        <tr>
                            <td valign="top" align="left">
                                <table  width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                                    <tr>
                                        <td valign="top" align="left" style="background-color:#fee6f0;"><img src="<?php echo BASEURL_EIMG; ?>emailer-left.png" height="619" width="43" title="birthdayowl" alt="birthdayowl" border="0" style="display:block;"  /></td> 
                                        <td  valign="top" align="left" width="434" style="padding:0 40px;">
                                            <table  width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                <tr>
                                                    <td align="center" style="font-size:22px; color:#000; padding-bottom:30px;">
                                                        Here's what you ordered
                                                    </td>
                                                </tr> 
                                                <tr>
                                                    <td valign="top" style="border-bottom:solid 1px #4a8a02; padding-bottom:10px; font-size:14px; color:#000">
                                                        <table  width="100%" cellspacing="0" cellpadding="0" border="0" >
                                                            <tr>
                                                                <?php
                                                                $date = date_create($flower_delivery_date);
                                                                $flower_delivery_date_new = date_format($date, "d-m-Y");
                                                                ?>
                                                                <td align="left"><strong>Delivery Date: </strong><?php echo $flower_delivery_date_new; ?></td>
                                                                <td align="right"><strong>Invoice: </strong> #<?php echo $order_id; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left"><strong>Delivery Time Slot: </strong><?php echo $flower_delivery_time_slot; ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="border-bottom:solid 1px #4a8a02; padding-bottom:5px; font-size:14px; color:#000">
                                                        <table  width="100%" cellspacing="0" cellpadding="0" border="0" >
                                                            <tr>
                                                                <!--<td align="center" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"></td>-->
                                                                <td align="center" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>ITEM</strong></td>
                                                                <td align="center" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>MODEL</strong></td>
                                                                <td align="center" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>QTY</strong></td>
                                                                <td align="center" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>COST</strong></td>
                                                            </tr>
                                                            <tr>
                                                                <!--<td align="center" valign="top" style="font-size:12px; padding:7px;"> <img src="<?php // BASEURL_PRODUCT_IMG . $flower_image;                                                                      ?>" height="50" width="50" title="birthdayowl" alt="birthdayowl" border="0" style="display:block; margin-top:10px;"  /></td>-->
                                                                <td align="center" valign="top" style="font-size:12px; padding:7px;"><?php echo $flower_name; ?></td>
                                                                <td align="center" valign="top" style="font-size:12px; padding:7px;"><?php echo $flower_code; ?></td>
                                                                <td align="center" valign="top" style="font-size:12px; padding:7px;"><?php echo $flower_quantity; ?></td>
                                                                <td align="right" valign="top" style="font-size:12px; padding:7px;">&#8377;<?php echo $flower_price; ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="border-bottom:solid 1px #4a8a02; padding-bottom:5px; font-size:16px; color:#000">
                                                        <table  width="100%" cellspacing="0" cellpadding="0" border="0" >
                                                            <tr>
                                                                <?php
                                                                $sub_total = $flower_price * $flower_quantity;
                                                                ?>
                                                                <td align="right" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>SUB-TOTAL</strong></td>
                                                                <td align="right" valign="top" style="font-size:12px;  padding:7px;">&#8377;<?php echo $sub_total; ?></td>
                                                            </tr>
                                                            <tr> 
                                                                <td align="right" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>processing fee (3.5%)</strong></td>
                                                                <td align="right" valign="top" style="font-size:12px;  padding:7px;">&#8377;<?php echo $processing_fee1; ?></td>
                                                            </tr>
                                                            <tr> 
                                                                <td align="right" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>Free- Shipping</strong></td>
                                                                <td align="right" valign="top" style="font-size:12px;  padding:7px;">&#8377;0</td>
                                                            </tr>
                                                            <tr> 
                                                                <td align="right" valign="top" style="font-size:11px; text-transform:uppercase; padding:7px;"><strong>TOTAL</strong></td>
                                                                <td align="right" valign="top" style="font-size:12px;  padding:7px;">&#8377;<?php echo $total_amount; ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="font-size:22px; color:#000; padding-bottom:10px; padding-top:10px;">
                                                        Order Details
                                                    </td>
                                                </tr> 
                                                <tr>
                                                    <td valign="top" style="border-bottom:solid 1px #4a8a02; padding:7px 10px; font-size:16px; color:#000">
                                                        <table  width="100%" cellspacing="0" cellpadding="0" border="0" >
                                                            <tr> 
                                                                <td width="50%" align="left" valign="top" style="font-size:12px; padding:2px;"><strong>Mobile: </strong><?php echo $receiver_phone; ?></td>
                                                                <?php
                                                                $date = date_create($purchased_date);
                                                                $purchased_date_new = date_format($date, "d-m-Y H:i:s");
                                                                ?>
                                                                <td width="50%" align="left" valign="top" style="font-size:12px;  padding:2px;"><strong>Date: </strong><?php echo $purchased_date_new; ?></td>
                                                            </tr>
                                                            <tr> 
                                                                <td align="left" valign="top" style="font-size:12px; padding:2px;"><strong>Email: </strong> <a href="mailto:info@birthdayowl.com" title="info@birthdayowl.com" style="color:#000; text-decoration:none;"><?php echo $receiver_email; ?></a></td>
                                                                <td align="left" valign="top" style="font-size:12px;  padding:2px;"><strong>Order ID: </strong><?php echo $order_id; ?></td>
                                                            </tr>
                                                            <tr> 
                                                                <td align="left" valign="top" style="font-size:12px; padding:2px;"><strong>Website: </strong> <a href="https://www.birthdayowl.com" title="https://www.birthdayowl.com" target="_blank" style="color:#000; text-decoration:none;">https://www.birthdayowl.com</a></td>
                                                                <?php
                                                                if ($payment_mode == "CC") {
                                                                    $mode = "Credit Card";
                                                                } else if ($payment_mode == "DC") {
                                                                    $mode = "Debit Card";
                                                                } else if ($payment_mode == "NB") {
                                                                    $mode = "Net Banking";
                                                                }
                                                                if ($payment_mode != "") {
                                                                    ?>
                                                                    <td align="left" valign="top" style="font-size:12px;  padding:2px;"><strong>Payment Method: </strong><?php echo $mode; ?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr> 
                                                                <td align="left" valign="top" style="font-size:12px; padding:2px;"> </td>
                                                                <td align="left" valign="top" style="font-size:12px;  padding:2px;"><strong>Shipping Method: </strong>Free Shipping</td>
                                                            </tr> 
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="border-bottom:solid 1px #4a8a02; padding:7px 10px; font-size:16px; color:#000">
                                                        <table  width="100%" cellspacing="0" cellpadding="0" border="0" >
                                                            <tr> 
                                                                <td width="50%" align="left" valign="top" style="font-size:12px; padding:2px;"><strong>Payment Address:</strong> <br><?php echo $sender_name; ?></td>
                                                                <td  width="50%" align="left" valign="top" style="font-size:12px;  padding:2px;"><strong>Shipping Address:</strong><br>
                                                                    <span><?php echo $receiver_name; ?></span><br>
                                                                    <span><?php echo $flower_receiver_address1; ?></span><br>
                                                                    <span><?php echo $flower_receiver_address2; ?></span><br>
                                                                    <span><?php echo $flower_delivery_city; ?>-<?php echo $flower_delivery_pincode; ?>, India</span></td>
                                                            </tr>  
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="font-size:22px; color:#000; padding-bottom:10px; padding-top:10px;">
                                                        Message on Card
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="color:#000; padding-bottom:10px; padding-top:10px;">
                                                        <?php echo $message_on_card; ?>
                                                    </td>
                                                </tr> 
                                            </table>
                                        </td> 
                                        <td valign="top" align="left" style="background-color:#fee6f0;"><img src="<?php echo BASEURL_EIMG; ?>emailer-rt.png" height="619" width="43" title="birthdayowl" alt="birthdayowl" border="0" style="display:block;"  /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr> 
                        <tr>
                            <td valign="top"><img src="<?php echo BASEURL_EIMG; ?>emailer-btm.png" height="114" width="600" title="birthdayowl" alt="birthdayowl" border="0" style="display:block;"  /> </td>
                        </tr>
                        <tr width="100">
                            <td  valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; font-weight:normal;padding:15px; ">For more information please read our <a href="<?php echo WEB_PRIVACY_AND_POLICY; ?>" title="Privacy Policy" style="text-decoration:none; color:#1155cc;">Privacy Policy</a> and <a href="<?php echo WEB_TERMS_OF_USE_OTHER; ?>" title="Terms of Use" style="text-decoration:none; color:#1155cc;">Terms of Use</a>.<br />
                                Copyright © 2016 birthdayowl.com. All rights reserved.</td>
                        </tr>
                        <tr>
                            <td>
                                <table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody class="mcnFollowBlockOuter">
                                        <tr>
                                            <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                                                <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding-left:9px;padding-right:9px;" align="center">
                                                                <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                                                                                <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td valign="top" align="center">
                                                                                                <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                    <tbody><tr>
                                                                                                            <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                                <a href="https://www.instagram.com/birthday_owl_reminder/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/instagram-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                    <tbody><tr>
                                                                                                            <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                                <a href="https://www.facebook.com/birthdayowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/facebook-ico.png" alt="Facebook" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                    <tbody><tr>
                                                                                                            <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                                <a href="https://twitter.com/birthday_owl" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/twitter-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                    <tbody><tr>
                                                                                                            <td class="mcnFollowIconContent" style="padding-right:0; padding-bottom:9px;" valign="top" align="center">
                                                                                                                <a href="https://www.pinterest.com/birthdyowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/pinterest-ico.png" alt="Pinterest" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table></td>
            </tr>

        </table>
    </body>
</html>
