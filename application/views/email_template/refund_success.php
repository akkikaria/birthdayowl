<?php /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ ?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title></title>
        <style type="text/css">


            body{
                font-family: verdana, arial, sans-serif;
            }
        </style>
    </head>
    <body>

        <div style="width: 100%; text-align: center;margin:auto;background-color: #efefef;height:auto;padding-top: 22px;">
            <table   cellspacing="0" cellpadding="0" border="0" align="center"  >
                <tr width="100">
                    <td  align="center">
                        <img src="<?php echo BASEURL_OIMG ?>logo.png" style="width:180px;" />

                    </td>
                </tr>
                <tr>
                    <td  style='background:white;;width:542px;border-top:1px solid #DFDFDF' >
                        <table style="padding:8px;width: 100%;"  align="center" >
                            <tr> 
                                <td>Hi there,</td>
                            </tr>
                            <tr> 
                                <td style="font-size:13px;">Refund of your Payment ID: <?php echo $paymentId; ?> done on Birthday Owl is successful.</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    if ($payment_mode == "CC") {
                                        $mode = "Credit Card";
                                    } else if ($payment_mode == "DC") {

                                        $mode = "Debit Card";
                                    } else if ($payment_mode == "NB") {

                                        $mode = "Net Banking";
                                    } else {

                                        $mode = "";
                                    }
                                    ?>
                                    <table align="center" style="border-spacing:0;width: 98%;border:2px solid #DFDFDF " cellSpacing="0" cellPadding="0" border="0">
                                        <thead style="background-color:#52A303;color: white;">
                                            <td style="padding: 4px;width: 49%;border-right: solid 1px white;">Mode</td>
                                            <td style="padding: 4px;width: 49%;">Refund Summary</td>
                                        </thead>
                                        <tbody style="font-size:12px;">
                                            <tr>
                                                <td style="padding: 5px; border: solid 1px #DFDFDF;"><?php echo $mode ?></td>
                                                <td style="padding: 5px; border: solid 1px #DFDFDF;">
                                                    <div>Refund of Rs. <?php echo $refundAmount; ?></div>
                                                    <div>Initiated successfully for your <?php echo $mode ?>*</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 5px; border: solid 1px #DFDFDF;">Order Amount: Rs. <?php echo $refundAmount; ?></td>
                                                <td style="padding: 5px; border: solid 1px #DFDFDF;">
                                                    <div>Refunded Amount: Rs.<?php echo $refundAmount; ?></div>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr align="right">
                                <td style="font-size:13px;padding-right: 14px;"> 
                                    *This should reflect in your account in 3-5 working days
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size:13px;">
                                    Please mention your PayUMoney ID: <?php echo $paymentId; ?> for reference.
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size:13px;">
                                    <div><b>Happy buying and more, </b></div>
                                    <div>Team BirthdayOwl</div>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td  valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; font-weight:normal;padding:15px; ">For more information please read our <a href="<?php echo WEB_PRIVACY_AND_POLICY; ?>" title="Privacy Policy" style="text-decoration:none; color:#1155cc;">Privacy Policy</a> and <a href="<?php echo WEB_TERMS_OF_USE_EGIFT; ?>" title="Terms of Use" style="text-decoration:none; color:#1155cc;">Terms of Use</a>.<br />
                        Copyright © 2016 birthdayowl.com. All rights reserved.
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody class="mcnFollowBlockOuter">
                                <tr>
                                    <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                                        <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding-left:9px;padding-right:9px;" align="center">
                                                        <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                                                                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="top" align="center">
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.instagram.com/birthday_owl_reminder/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/instagram-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.facebook.com/birthdayowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/facebook-ico.png" alt="Facebook" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://twitter.com/birthday_owl" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/twitter-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:0; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.pinterest.com/birthdyowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/pinterest-ico.png" alt="Pinterest" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>

        </div>
    </body>
</html>