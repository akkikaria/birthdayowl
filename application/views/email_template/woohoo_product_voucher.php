<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>BirthdayOWl</title>
        <link rel="shortcut icon" type="image/png" href="<?php echo BASEURL_OIMG; ?>main_icn.png"/>
    </head>
    <body>
        <table  width="600" cellspacing="0" cellpadding="0" border="0" align="center">
            <tr>
                <td valign="top" style="border:solid 1px #969696;"><table  width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tr>
                            <td valign="top"><img src="https://s27.postimg.org/88c7ptplf/greet-bnr.png" height="217" width="600" title="birthdayowl" alt="birthdayowl" border="0" style="display:block;"  /> </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 29px; color: #2c2a2a; font-weight:bold;padding-bottom:10px; ">  <?php echo ucfirst($uname); ?> sent you an ecard! </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000; font-weight:normal;padding-bottom:20px; "> There is an ecard waiting for you at birthdayowl.com </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" style="padding-bottom:20px;"><a href="<?php echo $linkclick; ?>" title="ecard" style="text-decoration:none;"><img src="https://s27.postimg.org/8ijq9647n/ecard-btn.png" height="44" width="190" title="ecard" alt="ecard" border="0" style="display:block;"  /></a> </td>
                        </tr>
                        <tr> <td valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000; font-weight:normal;padding-bottom:5px; ">Button not working? No problem, just copy below link and paste it into a browser. 
                            </td>
                        </tr>
                        <tr>  <td valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000; font-weight:normal;padding-bottom:15px; "><a href="<?php echo $linkclick; ?>" title="birthdayowl" style="text-decoration:none; color:#1155cc;"><?php echo $linkclick; ?></a> </td>  </tr>
                        <tr>    <td valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000; font-weight:normal;padding-bottom:20px; ">Your ecard will be available for 30 days. Enjoy and God bless!
                            </td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td  valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000; font-weight:normal;padding:15px; ">For more information please read our <a href="#" title="Privacy Policy" style="text-decoration:none; color:#1155cc;">Privacy Policy</a> and <a href="#" title="Terms of Use" style="text-decoration:none; color:#1155cc;">Terms of Use</a>.<br />
                    Copyright © 2016 birthdayowl.com. All rights reserved.</td>
            </tr>
            <tr>
                <td>
                    <table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody class="mcnFollowBlockOuter">
                            <tr>
                                <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                                    <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="padding-left:9px;padding-right:9px;" align="center">
                                                    <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                                                                    <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td valign="top" align="center">
                                                                                    <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                        <tbody><tr>
                                                                                                <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                    <a href="https://www.instagram.com/birthday_owl_reminder/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/instagram-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                        <tbody><tr>
                                                                                                <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                    <a href="https://www.facebook.com/birthdayowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/facebook-ico.png" alt="Facebook" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                        <tbody><tr>
                                                                                                <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                    <a href="https://twitter.com/birthday_owl" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/twitter-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                        <tbody><tr>
                                                                                                <td class="mcnFollowIconContent" style="padding-right:0; padding-bottom:9px;" valign="top" align="center">
                                                                                                    <a href="https://www.pinterest.com/birthdyowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/pinterest-ico.png" alt="Pinterest" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
