<?php /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ ?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title></title>
        <style type="text/css">


            body{
                font-family: verdana, arial, sans-serif;
            }
        </style>
    </head>
    <body>

        <div style="width: 100%; text-align: center;margin:auto;background-color: #efefef;height:auto;padding-top: 22px;">
            <table   cellspacing="0" cellpadding="0" border="0" align="center"  >
                <tr width="100">
                    <td  align="center">
                        <img src="<?php echo BASEURL_OIMG ?>logo.png" style="width:180px;" />
                        <div style="height:20px"></div>
                    </td>
                </tr>
                <tr valign="center" align="center"  width="100" >
                    <td>
                        <table style="background:#4D9E03;height:53px;width:480px;">
                            <tr>
                                <td valign="center"  align="center"  >
                                    <img src="<?php echo BASEURL_EIMG ?>p.png" style="float:left;width: 50px;"/> 
                                </td>
                                <td align='left'>
                                    <div style="line-height: 60px;text-align:left;color: white;font-size: 18px;">Payment Received Rs.<?php echo $total_amount ?></div>    
                                </td>
                            </tr>
                        </table>
                    </td>


                </tr>

                <tr  align="center" width="100">
                    <td style="width:450px;" >
                        <img src='<?php echo BASEURL_EIMG ?>payment_body.png' />
                    </td> 
                </tr>

                <tr style="background:white;" height="200" width="100">

                    <td >
                        <table style="height:53px;width:480px;">
                            <tr>
                                <td style="height:50px; padding-left: 10px;"width="300">
                                    <img src="<?php echo $voucher_img; ?>" title="birthdayowl" alt="birthdayowl" border="0" style=""  /><br/>
                                </td>
                                <td width="50%" align="center">
                                    <span style="font-size:12px;"><b><?php echo $voucher_name; ?> to <?php echo $fname; ?></b></span><br/>
                                    <span style="font-size:12px;"><b>Rs.<?php echo $selected_amount ?> Gift Amount-Sent</b></span><br/>
                                    <p style="border-top:2px solid #BFEDAC;width:150px;"></p>
                                    <?php if ($femail != '') { ?>
                                        <span style="width:150px;font-size:12px;">
                                            to:<?php echo $femail; ?> </span>
                                        <?php
                                    } else {
                                        if ($fphone != '') {
                                            ?>
                                            <span style="width:150px;font-size:12px;">
                                                to:<?php echo $fphone; ?> </span>
                                            <?php
                                        }
                                    }
                                    ?>    
                                    <br/>
                                    <?php
                                    $date = date_create($purchased_date);
                                    $purchased_date_new = date_format($date, "d-m-Y H:i:s");
                                    ?>
                                    <span style="font-size:12px;">on:<?php echo $purchased_date_new; ?></span><br/>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="background:white;" height="250" width="100">

                    <td style="border-top:1px solid #DFDFDF" >
                        <table style="height:53px;width:480px;">
                            <tr>
                                <td style="height:50px; padding-left: 40px;"width="300">
                                    <img src="<?php echo BASEURL_CROPPED_GREET . $greeting_img ?>" title="birthdayowl" alt="birthdayowl" border="0" style="width:140px;height:190px;border-radius: 5px"  /><br/>
                                </td>
                                <td width="50%" align="center">
                                    <span style="font-size:12px;color: black;"><?php echo $greetmessage; ?></span><br/>

                                    <p style="border-top:2px solid #BFEDAC;width:150px;"></p>
                                    <span style="font-size:12px;width:150px;"> 
                                        <?php if ($type == 1) { ?>
                                            <a href="<?php echo BASEURL_BVMG . $media; ?>" style="cursor:pointer;"><img src="<?php echo BASEURL_EIMG ?>eimage_order.png" /></a>  
                                        <?php } else if ($type == 2) { ?>
                                            <a href="<?php echo BASEURL_BVMG . $media; ?>" style="cursor:pointer;"> <img src="<?php echo BASEURL_EIMG ?>evideo_order.png" /> </a>
                                        <?php } else if ($type == 3) { ?>
                                            <a href="<?php echo BASEURL_BVMG . $media; ?>" style="cursor:pointer;">  <img src="<?php echo BASEURL_EIMG ?>eaudio_order.png" /> </a>
                                        <?php } ?>
                                    </span><br/>
                                    <span style="font-size:12px;"><b>-from <?php echo $sender_name ?></b></span><br/>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr  align="center" width="100">
                    <td  style='background: #bfedac;width:480px;height:300px ;border-top:1px solid #DFDFDF' >
                        <table style="padding:8px">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <div><b>Payment Details</b></div>
                                            </td>
                                        </tr>


                                    </table>
                                </td>
                                <td align='right'>
                                    <img src="<?php echo BASEURL_EIMG ?>payd.png" />
                                </td>
                            </tr>
                            <tr align='center' >
                                <td colspan="2"  align='center' style="background: white;"  width="400">
                                    <table align="center">
                                        <tr align="left">
                                            <td width="120" align="left">Order number</td>
                                            <td>:</td>
                                            <td>#<?php echo $order_id; ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2"  align='center'>
                                    <table>
                                        <tr>
                                            <td colspan="5">Sent to</td>
                                            <td>:</td>
                                            <td><?php echo $bill_to_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">Gift Amount</td>
                                            <td>:</td>
                                            <td>Rs.<?php echo $selected_amount; ?></td>
                                        </tr>
<!--                                        <tr>
                                            <td colspan="5">Quantity</td>
                                            <td>:</td>
                                            <td><?php // echo $quantity;       ?></td>
                                        </tr>-->
<!--                                        <tr>
                                            <td colspan="5">Sub-Total</td>
                                            <td>:</td>
                                        <?php
//                                            $sub_total = $selected_amount * $quantity;
                                        ?>
                                            <td>Rs.<?php // echo $sub_total;       ?></td>
                                        </tr>-->
                                        <tr>
                                            <td colspan="5">Processing fee</td>
                                            <td>:</td>
                                            <td>Rs.<?php echo $processing_fee1; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">Total Amount</td>
                                            <td>:</td>
                                            <td>Rs.<?php echo $total_amount; ?></td>
                                        </tr>
                                        <tr>

                                            <?php
                                            if ($payment_mode == "CC") {
                                                $mode = "Credit Card";
                                            } else if ($payment_mode == "DC") {

                                                $mode = "Debit Card";
                                            } else if ($payment_mode == "NB") {

                                                $mode = "Net Banking";
                                            }
                                            if ($payment_mode != "") {
                                                ?>

                                                <td colspan="5">Payment method</td>
                                                <td>:</td>
                                                <td><?php echo $mode; ?></td>
                                            <?php } ?>
                                        </tr>
<!--                                        <tr>
                                            <td colspan="5">Card number</td>
                                            <td>:</td>
                                            <td>Rs.100</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">Card expiry</td>
                                            <td>:</td>
                                            <td>Rs.100</td>
                                        </tr>-->
                                        <tr>
                                            <td colspan="5">Payment date & time</td>
                                            <td>:</td>
                                            <?php
                                            $date = date_create($purchased_date);
                                            $purchased_date_new = date_format($date, "d-m-Y H:i:s");
                                            ?>
                                            <td><?php echo $purchased_date_new; ?></td>
                                        </tr>
                                    </table>
                                </td> 
                            </tr>
                        </table>
                    </td>
                </tr>


                <tr width="100">
                    <td  valign="top" align="center"  style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; font-weight:normal;padding:15px; ">For more information please read our <a href="<?php echo WEB_PRIVACY_AND_POLICY; ?>" title="Privacy Policy" style="text-decoration:none; color:#1155cc;">Privacy Policy</a> and <a href="<?php echo WEB_TERMS_OF_USE_EGIFT; ?>" title="Terms of Use" style="text-decoration:none; color:#1155cc;">Terms of Use</a>.<br />
                        Copyright © 2016 birthdayowl.com. All rights reserved.</td>
                </tr>
                <tr>
                    <td>
                        <table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody class="mcnFollowBlockOuter">
                                <tr>
                                    <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                                        <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding-left:9px;padding-right:9px;" align="center">
                                                        <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                                                                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="top" align="center">
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.instagram.com/birthday_owl_reminder/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/instagram-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.facebook.com/birthdayowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/facebook-ico.png" alt="Facebook" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://twitter.com/birthday_owl" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/twitter-ico.png" alt="Twitter" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <table class="mcnFollowStacked" style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                            <tbody><tr>
                                                                                                    <td class="mcnFollowIconContent" style="padding-right:0; padding-bottom:9px;" valign="top" align="center">
                                                                                                        <a href="https://www.pinterest.com/birthdyowl/" target="_blank"><img src="<?php echo BASEURL_OIMG; ?>follow_icon/pinterest-ico.png" alt="Pinterest" class="mcnFollowBlockIcon" style="width:48px; max-width:48px; display:block;" width="48"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>

        </div>
    </body>
</html>