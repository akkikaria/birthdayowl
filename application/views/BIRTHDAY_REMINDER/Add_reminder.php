<?php $this->load->view("new_templates/header"); ?>
<!--Banner -->
<link href="<?PHP echo BASEURL_OCSS; ?>style_popup.css" rel="stylesheet" type="text/css"/>
<script src="<?PHP echo BASEURL_OJS ?>main_popup.js" type="text/javascript"></script>
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            Add <span>Birthday Reminders</span>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <!--        <div class="alertPopupDivnew" >
                    <div class="alertPopupMessagenew">
                        <div class="closepopup" id="close_popupnew"> <img src="<?php // echo BASEURL_OIMG;                                                       ?>closer.png" /> </div>
                        <br/>
                        <br/>
                        <div style="text-align: center; margin: auto auto; width: 100%; height:120px;line-height: 29px;">
                            <div style="font-size: 20px;" id='cmessage'>You're unsubscribed.</div>
                            <div style="text-decoration: underline;cursor: pointer;" id='resubscribe'>Re-subscribed?</div>
                            <br/>
                        </div>
                    </div>
                </div>-->
        <div class="inner_midd dashbord">
            <div class="in_f_l">
               	<div class="dashboard-res-menu">

               	</div>
                <ul class="leftside dashbord_nav">
                    <li><img alt="DASHBOARD" src="<?php echo BASEURL_OIMG ?>dash.png" class='dash'/><a redirect_val ="dashboard" href="<?php echo WEB_DASHBOARD; ?>">DASHBOARD</a></li>
                    <li class="active"><img alt="ADD BIRTHDAY REMINDERS"  src="<?php echo BASEURL_OIMG ?>rem.png"  class='dash'/><a redirect_val ="add_rem" href="<?php echo BASEURL . "addReminder/0" ?>">ADD BIRTHDAY REMINDERS</a></li>
                    <li><img alt="VIEW BIRTHDAY REMINDERS" src="<?php echo BASEURL_OIMG ?>bday.png" class='dash'/><a redirect_val ="view_rem" href="<?php echo WEB_VIEW_BIRTHDAY_REMINDER; ?>">VIEW BIRTHDAY REMINDERS</a></li>
                    <li><img alt="KNOW YOUR ZODIAC" src="<?php echo BASEURL_OIMG ?>horo.png" class='dash'/><a redirect_val ="horoscope" href="<?php echo WEB_KNOW_YOUR_HOROSCOPE; ?>" style="cursor:pointer;" id="know_horo">KNOW YOUR ZODIAC</a></li>
                    <li><img alt="ORDER HISTORY" src="<?php echo BASEURL_OIMG ?>order.png" class='dash'/><a redirect_val ="order" href="<?php echo WEB_ORDER_HISTORY; ?>">ORDER HISTORY</a></li>
                    <li><img alt="GIFT WALLET" src="<?php echo BASEURL_OIMG ?>wallet.png" class='dash'/><a redirect_val ="gift_wallet" href="<?php echo WEB_GIFT_WALLET; ?>">GIFT WALLET</a></li>
                    <li><img alt="UPDATE PROFILE" src="<?php echo BASEURL_OIMG ?>update.png" class='dash'/><a redirect_val ="update_profile" href="<?php echo WEB_UPDATE_PROFILE; ?>">UPDATE PROFILE</a></li>
                    <li><img alt="UNSUBSCRIBE" src="<?php echo BASEURL_OIMG ?>greet_history.png" class='dash'/><a id="unsubscribe" style="cursor: pointer;" >UNSUBSCRIBE</a></li>
                </ul>
            </div>
            <div class="in_f_r add_bday_reminder">
                <div class="in_heading google_sync" data-count="1" style="cursor: pointer;display:none;">
                    <b><span>SYNC WITH GOOOGLE </span>
                        <abbr>synchronize with your google Account </abbr>
                    </b><a id="googlesync">
                        <img src="<?PHP echo BASEURL_OIMG ?>google_syn.png" /></a>
                </div>
                <div class="in_heading google_sync" data-count="1">
                    <span>ADD BIRTHDAYS</span>
                    <abbr>It is quite simple to add birthday reminders; it takes only a few seconds.</abbr>
                </div>
                <ul class="add_bday_input wrapper">
                    <li>Gender</li>
                    <li class="gender">
                        <input type="radio" id="Gender_0" name="gender" value="1" />
                        <label for="male"><span></span>Male</label>
                        <input type="radio" id="Gender_1" name="gender" value="2" />
                        <label for="female"><span></span>Female</label>
                    </li>
                    <li>Full Name</li>
                    <li>
                        <input type="text" name="first_name"  id="first_name" value="<?php echo $rem_data["first_name"]; ?>">
                        <input type="hidden" name="reminder_id"  id="reminder_id" value="<?php echo $rem_data["reminder_id"]; ?>">
                    </li>
                    <li>Mobile Number</li>
                    <li>
                        <div style="width: 63%;display: flex;">
                            <select id="CmbCountry" name="nationality_id" class="countrylist">
                                <?php $countryList = get_countryinfo(); ?>

                                <?php foreach ($countryList as $country) { ?>
                                    <option value="<?php echo $country["country_id"]; ?>"><?php echo $country['country_name'] . " (+" . $country["nationality_code"] . ")"; ?></option>
                                <?php } ?>
                            </select>
                            <input type="text" placeholder="Mobile Number *"  name="mobile_no"  onkeypress="return isNumberKey(event)" id="rmobile_no"  class="mbno" value="<?php echo $rem_data["mobile_no"]; ?>" />

                        </div>
                        <!--<input type="text" name="mobile_no" id="rmobile_no" onkeypress="return isNumberKey(event)" value="<?php echo $rem_data["mobile_no"]; ?>">-->
                    </li>
                    <li>Email</li>
                    <li>
                        <input type="text" name="email" id="emailRem" value="<?php echo $rem_data["email"]; ?>">
                    </li>
                    <li>Date of Birth</li>
                    <li>
                        <div style="display: flex;width: 100%;">

                            <select id="dobday1" class="bdate" name="bdate" style="float: left; margin-right: 10px; width: 20%;"></select>
                            <select id="dobmonth1" class="bmonth" name="bmonth" style="height: 41px; margin-right: 11px; width: 20%;"></select>
                            <select id="dobyear1" class="byear" name="byear" style="width: 19%; visibility: hidden;"></select>

                            <!--<input type="text" placeholder="Date"id="bdate" style="float: left; margin-right: 10px; width: 20%;" name="bdate"  maxlength="2" onkeypress="return isNumberKey(event)" value="<?php // echo $rem_data["bdate"];                                                                                                                                                ?>" />-->
<!--                            <select id="CmbMonth" name="bmonth" style="height: 41px; margin-right: 11px; width: 20%;">
                                <option selected="selected" value="0">Month</option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>-->
                            <!--<input type="text" placeholder="Year" style="width: 19%;" maxlength="4" name="byear" id="byear"  onkeypress="return isNumberKey(event)" />-->

                            <input type="hidden" id="edit_rem_date" />
                            <input type="hidden" id="edit_rem_month" />
                            <input type="hidden" id="edit_rem_year" />
                        </div>
                        <span style="color: red; display: none;" id="dob_note">Note: In case of any changes in "Date of Birth" please change date first & then month.</span>

  <!--<input type="text" id="apopupDatepicker" name="birth_date" value="<?php //echo $bdate . " " . $monthname;                                                                                                                                                                                                                                                                   ?>" >-->
                    </li>
                    <li>Zodiac Sign</li>
                    <li>
                        <input type="text" id="zodiac_id" name="zodiac_id" value="<?php echo $rem_data["zodiac_sign"]; ?>">
                    </li>
                    <li class="Priority">Remind me before</li>
                    <li>
                        <select id="priority" name="priority">
                            <option selected="selected" value="1">Select</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                        </select>
                        <div class="daysalign">Day</div>
                    </li>
                    <li class="Priority">&nbsp;</li>
                    <li class="add_bday_last">
                        <input type="submit" value="Submit" class="" id="add_birthday">
                        <input type="reset" value="Cancel" class="" id="cancel_update">
                    </li>
                </ul>
            </div>

            <div class="alertPopupDiv1 alertPopupDiv" style="display: none;">
                <div class="alertPopupMessage1 alertPopupMessage">
                    <div class="closepopup"> <img alt="close" src="<?php echo BASEURL_OIMG; ?>closer.png" /> </div>
                    <div class="cowl">
                        <img src="<?php echo BASEURL_OIMG; ?>cloud.png" id="cowlimg" alt="add birthday reminder"/>
                    </div>
                    <div style="  border-bottom: 1px solid transparent;  min-height: 16.846px;  padding: 15px;"> </div>
                    <div class="owlAlert"><img src="<?php echo BASEURL_OIMG; ?>owlalert.png" alt="birthday calendar" /></div>
                    <!--                    <div class="alertclose" id="alertclose">OK</div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="alertBox"></div>
<div class="errorDiv"></div>
<div class="cd-popup cd-popup-add" role="alert">
    <div class="cd-popup-container" style="color: black;">
        <p>Reminder already exists with current mobile number.
            <br/>
            Do you really want to <span id="add_update_text">add</span>?</p>
        <ul class="cd-buttons">
            <li><a id="yes" style="cursor:pointer;" >Yes</a></li>
            <li><a id="no" style="cursor:pointer;" >No</a></li>
        </ul>
        <!--<a href="#0" class="cd-popup-close img-replace">Close</a>-->
    </div> <!-- cd-popup-container -->
</div> <!-- cd-popup -->
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript">

    $(document).ready(function () {
        if (window.location.href.indexOf('#_=_') > 0) {
            window.location = window.location.href.replace(/#.*/, '');
        }
    });
<?php if ($rem_data["country_id"] != "") { ?>
        $("#CmbCountry").val("<?php echo $rem_data["country_id"]; ?>");
<?php } else { ?>
        $("#CmbCountry").val("<?php echo $rem_data["country_id"]; ?>");
<?php } ?>


    $('#no').click(function (event) {
        $("body").css("overflow", "auto");
        event.preventDefault();
        $('.cd-popup').removeClass('is-visible');
    });

    $('#yes').click(function () {
        $("body").css("overflow", "auto");
        $('.cd-popup').removeClass('is-visible');

        var first_name = $("#first_name").val();
        var mobile_no = $("#rmobile_no").val();
        var email = $("#emailRem").val();

        if (gid != 0) {
            var birthdate = birth_date;
            var bmonth = birth_month;
            var byear = birth_year;
        } else {
            var birthdate = $(".bdate").val();
            var bmonth = $(".bmonth").val();
            var byear = $(".byear").val();
        }



        var zodiac_id = $("#zodiac_id").val();
        var relation_id = $("#relation_id").val();
        var priority = $("#priority").val();
        var rem_id = $("#reminder_id").val();
        var selectedCategory = '';
        var position = '';
        $('input[name="gender"]:checked').each(function () {
            selectedCategory = (this.value);
        });
        if (selectedCategory == '') {
            alert("Select Gender");
            $('html, body').animate({
                scrollTop: $(".gender").offset().top
            }, 2000);
        } else if (first_name == '') {
            alert("Enter Full Name");
            $('html, body').animate({
                scrollTop: $("#first_name").offset().top
            }, 2000);
        } else if (email == '' && mobile_no == '') {
            alert("Enter Email-address or Mobile Number");
            $('html, body').animate({
                scrollTop: $("#rmobile_no").offset().top
            }, 2000);
        } else if ((!(checkMobileValid(mobile_no))) && (mobile_no != '')) {
            alert("Enter valid Indian Mobile No");
            $('html, body').animate({
                scrollTop: $("#rmobile_no").offset().top
            }, 2000);
        } else if ((!(validateEmail(email))) && (email != '')) {
            alert("Not a Valid Email Address");
            $('html, body').animate({
                scrollTop: $("#rmobile_no").offset().top
            }, 2000);
        } else if (birthdate == '') {
            alert("Enter birth date");
            $('html, body').animate({
                scrollTop: $(".bdate").offset().top
            }, 2000);
        } else if (bmonth == '') {
            alert("Select birth month");
            $('html, body').animate({
                scrollTop: $(".bmonth").offset().top
            }, 2000);
        } else {
            //   $(".error_r6").hide();
            var data = {
                first_name: first_name,
                bdate: birthdate,
                bmonth: bmonth,
                byear: byear,
                mobile_no: mobile_no,
                email: email,
                zodiac_sign: zodiac_id,
                gender: selectedCategory,
                priority: priority,
                reminder_id: rem_id,
                country_id: $("#CmbCountry").val(),
                reminder_already_added: 1

            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>add-reminder",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $('.cd-popup').addClass('is-visible');
                        $('.cd-popup-ck').hide();
                        $('.cd-popup-un').hide();

                        //                            alert(r.message.toString());
                        return false;
                    } else {
                        $(".dash").css("z-index", "1");
                        $(".alertBox").css("display", "block");
                        $(".alertPopupDiv1").css("display", "block");
                        if (rem_id == "")
                            $("#cowlimg").attr("src", "<?php echo BASEURL_OIMG; ?>ncloud.png");
                        else
                            $("#cowlimg").attr("src", "<?php echo BASEURL_OIMG; ?>cloudup.png");
                        $('.alertPopupMessage1').animate({top: '34%'}, 500);
                    }
                }
            });
        }
    });
    //    $(document).on("click", "#unsubscribe", function () {
    //        $("#resubscribe").show();
    //        $("#cmessage").html("You're unsubscribed!");
    //        $(".alertPopupDivnew").addClass("show_overlay").show();
    //        $('.alertPopupMessagenew').animate({top: '34%'}, 500);
    //        var data = {
    //            mail_status: 1
    //
    //        }
    //        $.ajax({
    //            type: "POST",
    //            url: "<?php // echo BASEURL;                                                       ?>unsubscribe",
    //            data: data,
    //            dataType: "json",
    //            success: function (r) {
    //                if (r.success.toString() === "false") {
    //                    alert(r.message.toString());
    //                }
    //            }
    //        });
    //    });
    //    $(document).on("click", "#resubscribe", function () {
    //        $(this).hide();
    //        $("#cmessage").html("You're subscribed!");
    //        var data = {
    //            mail_status: 0
    //        }
    //        $.ajax({
    //            type: "POST",
    //            url: "<?php // echo BASEURL;                                                       ?>unsubscribe",
    //            data: data,
    //            dataType: "json",
    //            success: function (r) {
    //                if (r.success.toString() === "false") {
    //                    alert(r.message.toString());
    //                }
    //            }
    //        });
    //    });
    //    $("#close_popupnew").click(function () {
    //        $('.alertPopupMessagenew').animate({top: '0'}, 500);
    //        $(".alertPopupDivnew").removeClass("show_overlay").css("display", "none");
    //    });
    var gid = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
    var birth_date = "";
    var birth_month = "";
    var birth_year = "";
    if (gid != 0) {
        $('#add_update_text').html('update');
        $('#dob_note').css('display', 'block');
        var edit_rem_date = "<?php echo $rem_data["bdate"]; ?>";
        var edit_rem_month = "<?php echo $rem_data["bmonth"]; ?>";
        var edit_rem_year = "<?php echo $rem_data["byear"]; ?>";
        $('#edit_rem_date').val(edit_rem_date);
        $('#edit_rem_month').val(edit_rem_month);
        $('#edit_rem_year').val(edit_rem_year);
        birth_date = $('#edit_rem_date').val();
        birth_month = $('#edit_rem_month').val();
        birth_year = $('#edit_rem_year').val();
        $('.bdate').val(birth_date);
        $('.bmonth').val(birth_month);
        $('.byear').val(birth_year);
        $('.bdate').change(function () {
            birth_date = $('.bdate').val();
        });
        $('.bmonth').change(function () {
            birth_month = $('.bmonth').val();
        });
        $('.byear').change(function () {
            birth_year = $('.byear').val();
        });
        var birth_month_value = "";
        switch (birth_month) {
            case "1":
            case "01":
                birth_month_value = "January";
                break;
            case "2":
            case "02":
                birth_month_value = "February";
                break;
            case "3":
            case "03":
                birth_month_value = "March";
                break;
            case "4":
            case "04":
                birth_month_value = "April";
                break;
            case "5":
            case "05":
                birth_month_value = "May";
                break;
            case "6":
            case "06":
                birth_month_value = "June";
                break;
            case "7":
            case "07":
                birth_month_value = "July";
                break;
            case "8":
            case "08":
                birth_month_value = "August";
                break;
            case "9":
            case "09":
                birth_month_value = "September";
                break;
            case "10":
                birth_month_value = "October";
                break;
            case "11":
                birth_month_value = "November";
                break;
            case "12":
                birth_month_value = "December";
                break;
            default:
                birth_month_value = "Month";
        }

        var birth_year_value = "";
        if (birth_year == "0") {
            birth_year_value = "Year";
        } else {
            birth_year_value = birth_year;
        }
        $(document).ready(function () {
            $.dobPicker({
                daySelector: '#dobday1', /* Required */
                monthSelector: '#dobmonth1', /* Required */
                yearSelector: '#dobyear1', /* Required */
                dayDefault: birth_date, /* Optional */
                monthDefault: birth_month_value, /* Optional */
                yearDefault: birth_year_value, /* Optional */
                minimumAge: 8, /* Optional */
                maximumAge: 60 /* Optional */
            });
        });
    } else {
        $('#add_update_text').html('add');
        $(document).ready(function () {
            $.dobPicker({
                daySelector: '#dobday1', /* Required */
                monthSelector: '#dobmonth1', /* Required */
                yearSelector: '#dobyear1', /* Required */
                //                dayDefault: birth_date, /* Optional */
                //                monthDefault: birth_month, /* Optional */
                //                yearDefault: birth_year, /* Optional */
                minimumAge: 8, /* Optional */
                maximumAge: 60 /* Optional */
            });
        });
        birth_date = $('.bdate').val();
        birth_month = $('.bmonth').val();
        birth_year = $('.byear').val();
    }




    //    $("#CmbMonth").val("<?php echo $rem_data["bmonth"]; ?>");
    $("#priority").val("<?php echo $rem_data["priority"]; ?>");
    //    var birth_year = "<?php echo $rem_data["byear"]; ?>";
    //    if (birth_year == "0") {
    //        $("#byear").val("");
    //    } else {
    //        $("#byear").val(birth_year);
    //    }
    $("#know_horo").click(function () {
        var data = {
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>Home_web/check_birthdate",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {
                    alert(r.message.toString());
                } else {
                    window.location = "<?php echo BASEURL . "know_horoscope" ?>";
                }
            }
        });
    });
    $(document).ready(function () {
        var gen_val = "<?php echo $rem_data["gender"]; ?>";
        if (gen_val == 1) {
            $('input[name=gender][value=' + gen_val + ']').prop('checked', true);
        } else {
            $('input[name=gender][value=' + gen_val + ']').prop('checked', true);
        }


<?php if ($this->session->userdata('syncmsg') != "") { ?>
            $(".alertBox").css("display", "block");
            $(".alertPopupDiv1").css("display", "block");
            $(".titlemessage").html("<?php echo $this->session->userdata('syncmsg') ?>");
            $('.alertPopupMessage1').animate({top: '34%'}, 500);
<?php } ?>
    });
    if (gid != 0) {
        $('#add_birthday').click(function () {
            $("body").css("overflow", "none");
            var first_name = $("#first_name").val();
            var mobile_no = $("#rmobile_no").val();
            var email = $("#emailRem").val();
            var birthdate = birth_date;
            var bmonth = birth_month;
            var byear = birth_year;
            var zodiac_id = $("#zodiac_id").val();
            var relation_id = $("#relation_id").val();
            var priority = $("#priority").val();
            var rem_id = $("#reminder_id").val();
            var selectedCategory = '';
            var position = '';
            $('input[name="gender"]:checked').each(function () {
                selectedCategory = (this.value);
            });
            if (selectedCategory == '') {
                alert("Select Gender");
                $('html, body').animate({
                    scrollTop: $(".gender").offset().top
                }, 2000);
            } else if (first_name == '') {
                alert("Enter Full Name");
                $('html, body').animate({
                    scrollTop: $("#first_name").offset().top
                }, 2000);
            } else if (email == '' && mobile_no == '') {
                alert("Enter Email-address or Mobile Number");
                $('html, body').animate({
                    scrollTop: $("#rmobile_no").offset().top
                }, 2000);
            } else if ((!(checkMobileValid(mobile_no))) && (mobile_no != '')) {
                alert("Enter valid Indian Mobile No");
                $('html, body').animate({
                    scrollTop: $("#rmobile_no").offset().top
                }, 2000);
            } else if ((!(validateEmail(email))) && (email != '')) {
                alert("Not a Valid Email Address");
                $('html, body').animate({
                    scrollTop: $("#rmobile_no").offset().top
                }, 2000);
            } else {
                //   $(".error_r6").hide();
                var data = {
                    first_name: first_name,
                    bdate: birthdate,
                    bmonth: bmonth,
                    byear: byear,
                    mobile_no: mobile_no,
                    email: email,
                    zodiac_sign: zodiac_id,
                    gender: selectedCategory,
                    priority: priority,
                    reminder_id: rem_id,
                    country_id: $("#CmbCountry").val()

                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>add-reminder",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $('.cd-popup').addClass('is-visible');
                            //                            alert(r.message.toString());
                            return false;
                        } else {
                            $(".dash").css("z-index", "1");
                            $(".alertBox").css("display", "block");
                            $(".alertPopupDiv1").css("display", "block");
                            if (rem_id == "")
                                $("#cowlimg").attr("src", "<?php echo BASEURL_OIMG; ?>ncloud.png");
                            else
                                $("#cowlimg").attr("src", "<?php echo BASEURL_OIMG; ?>cloudup.png");
                            $('.alertPopupMessage1').animate({top: '34%'}, 500);
                        }
                    }
                });
            }
        });
    } else {
        $('#add_birthday').click(function () {
            $("body").css("overflow", "none");
            var first_name = $("#first_name").val();
            var mobile_no = $("#rmobile_no").val();
            var email = $("#emailRem").val();
            var birthdate = $(".bdate").val();
            var bmonth = $(".bmonth").val();
            var byear = $(".byear").val();
            var zodiac_id = $("#zodiac_id").val();
            var relation_id = $("#relation_id").val();
            var priority = $("#priority").val();
            var rem_id = $("#reminder_id").val();
            var selectedCategory = '';
            var position = '';
            $('input[name="gender"]:checked').each(function () {
                selectedCategory = (this.value);
            });
            if (selectedCategory == '') {
                alert("Select Gender");
                $('html, body').animate({
                    scrollTop: $(".gender").offset().top
                }, 2000);
            } else if (first_name == '') {
                alert("Enter Full Name");
                $('html, body').animate({
                    scrollTop: $("#first_name").offset().top
                }, 2000);
            } else if (email == '' && mobile_no == '') {
                alert("Enter Email-address or Mobile Number");
                $('html, body').animate({
                    scrollTop: $("#rmobile_no").offset().top
                }, 2000);
            } else if ((!(checkMobileValid(mobile_no))) && (mobile_no != '')) {
                alert("Enter valid Indian Mobile No");
                $('html, body').animate({
                    scrollTop: $("#rmobile_no").offset().top
                }, 2000);
            } else if ((!(validateEmail(email))) && (email != '')) {
                alert("Not a Valid Email Address");
                $('html, body').animate({
                    scrollTop: $("#rmobile_no").offset().top
                }, 2000);
            } else if (birthdate == '') {
                alert("Enter birth date");
                $('html, body').animate({
                    scrollTop: $(".bdate").offset().top
                }, 2000);
            } else if (bmonth == '') {
                alert("Select birth month");
                $('html, body').animate({
                    scrollTop: $(".bmonth").offset().top
                }, 2000);
            }
            //            else if (byear == "") {
            //                alert("Select birth year");
            //                $('html, body').animate({
            //                    scrollTop: $(".byear").offset().top
            //                }, 2000);
            //            } 
            else {
                //   $(".error_r6").hide();
                var data = {
                    first_name: first_name,
                    bdate: birthdate,
                    bmonth: bmonth,
                    byear: byear,
                    mobile_no: mobile_no,
                    email: email,
                    zodiac_sign: zodiac_id,
                    gender: selectedCategory,
                    priority: priority,
                    reminder_id: rem_id,
                    country_id: $("#CmbCountry").val()

                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>add-reminder",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            $('.cd-popup').addClass('is-visible');
                            //                            alert(r.message.toString());
                            return false;
                        } else {
                            $(".dash").css("z-index", "1");
                            $(".alertBox").css("display", "block");
                            $(".alertPopupDiv1").css("display", "block");
                            if (rem_id == "")
                                $("#cowlimg").attr("src", "<?php echo BASEURL_OIMG; ?>ncloud.png");
                            else
                                $("#cowlimg").attr("src", "<?php echo BASEURL_OIMG; ?>cloudup.png");
                            $('.alertPopupMessage1').animate({top: '34%'}, 500);
                        }
                    }
                });
            }
        });
    }


    $(".closepopup").click(function () {
        $('.alertPopupMessage1').animate({top: '0'}, 500);
        $(".alertBox").css("display", "none");
        $(".alertPopupDiv1").css("display", "none");
<?php $this->session->unset_userdata("syncmsg"); ?>
        window.location = "<?php echo BASEURL; ?>addReminder/0";
    });
    $("#googlesync").click(function () {
        $(".showloader").addClass("show_overlay").show();
        var seconds = 5;
        setInterval(function () {
            seconds--;
            if (seconds == 0) {
                $(".showloader").removeClass("show_overlay").hide();
                window.location = "1";
            }
        }, 1000);
    });
    $("#cancel_update").click(function () {
        window.location = "<?php echo WEB_DASHBOARD; ?>";
    });
    //Old
    //    $("#CmbMonth").change(function () {//it get invoke when input value changes.
    //
    //
    //        var bdate = $(".bdate").val();
    //        var bmonth = $("#CmbMonth").val();
    //        if ((bdate != '') && (bmonth != '0')) {
    //            var data = {
    //                bmonth: bmonth,
    //                bday: bdate
    //            }
    //            $.ajax({
    //                type: "POST",
    //                url: "<?php // echo BASEURL;                                                                                                                ?>Home_web/get_zodiac",
    //                data: data,
    //                dataType: "json",
    //                success: function (r) {
    //                    if (r.success.toString() === "false") {
    //                        $("#zodiac_id").val(r.message.toString());
    //                        return false;
    //                    } else {
    //                        $("#zodiac_id").val(r.message.toString());
    //                    }
    //                }
    //            });
    //        }
    //    });

    $(".bmonth").change(function () {//it get invoke when input value changes.
        if (gid != 0) {
            var bdate = birth_date;
        } else {
            var bdate = $(".bdate").val();
        }
        var bmonth = $(".bmonth").val();
        if ((bdate != '') && (bmonth != '0')) {
            var data = {
                bmonth: bmonth,
                bday: bdate
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>zodiac-data",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#zodiac_id").val(r.message.toString());
                        return false;
                    } else {
                        $("#zodiac_id").val(r.message.toString());
                    }
                }
            });
        }
    });
    //Old
    //    $("#bdate").keyup(function () {//it get invoke when input value changes.
    //
    //
    //        var bdate = $("#bdate").val();
    //        var bmonth = $("#CmbMonth").val();
    //        if ((bdate != '') && (bmonth != '0')) {
    //            var data = {
    //                bmonth: bmonth,
    //                bday: bdate
    //            }
    //            $.ajax({
    //                type: "POST",
    //                url: "<?php // echo BASEURL;                                                                                                ?>Home_web/get_zodiac",
    //                data: data,
    //                dataType: "json",
    //                success: function (r) {
    //                    if (r.success.toString() === "false") {
    //                        $("#zodiac_id").val(r.message.toString());
    //                        return false;
    //                    } else {
    //                        $("#zodiac_id").val(r.message.toString());
    //                    }
    //                }
    //            });
    //        }
    //    });

    //New
    $(".bdate").change(function () {//it get invoke when input value changes.
        var bdate = $(".bdate").val();
        if (gid != 0) {
            var bmonth = birth_month;
        } else {
            var bmonth = $("#CmbMonth").val();
        }

        if ((bdate != '') && (bmonth != '0')) {
            var data = {
                bmonth: bmonth,
                bday: bdate
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>zodiac-data",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#zodiac_id").val(r.message.toString());
                        return false;
                    } else {
                        $("#zodiac_id").val(r.message.toString());
                    }
                }
            });
        }
    });
    function getDate(d, m) {

        $(".errors7").hide();
        //                                var parts = date.split('/');
        //                                var month = parts[0];
        //                                var bdate = parts[1];
        if ((d != '') && (m != '')) {
            var data = {
                bmonth: m,
                bday: d
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>zodiac-data",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#zodiac_id").val(r.message.toString());
                        return false;
                    } else {
                        $("#zodiac_id").val(r.message.toString());
                    }
                }
            });
        }
    }



</script>
