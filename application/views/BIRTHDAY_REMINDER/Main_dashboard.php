<?php $this->load->view("new_templates/header"); ?>
<?php
//echo '<pre>';
//print_r($this->session->all_userdata());
//exit;
?>
<link rel="stylesheet" href="<?php echo BASEURL_OCSS; ?>reminder.css"  rel="stylesheet" type="text/css"/>  

<style type="text/css">
    .closepop{
        float: right;
        height: 27px;
        margin-top: -4px;
        width: 27px;
    }
    .closepopupn{
        cursor: pointer;
        float: right;
        left: -1px;
        margin-top: 3px;
        position: relative;
        width: 22px;
    }

</style>


<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            <span>Dashboard</span>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <!--        <div class="alertPopupDiv" >
                    <div class="alertPopupMessage">
                        <div class="closepopup" id="close_popup"> <img src="<?php // echo BASEURL_OIMG;            ?>closer.png" /> </div>
                        <br/>
                        <br/>
                        <div style="text-align: center; margin: auto auto; width: 100%; height:120px;line-height: 29px;">
                            <div style="font-size: 20px;" id='cmessage'>You're unsubscribed.</div>
                            <div style="text-decoration: underline;cursor: pointer;" id='resubscribe'>Re-subscribed?</div>
                            <br/>
                        </div>
                    </div>
                </div>-->
        <div class="inner_midd dashbord">
            <div class="in_f_l">
               	<div class="dashboard-res-menu">

               	</div>
                <ul class="leftside dashbord_nav">
                    <li class="active"><img src="<?php echo BASEURL_OIMG ?>dash.png" alt="DASHBOARD"  class='dash'/><a href="<?php echo WEB_DASHBOARD; ?>">DASHBOARD</a></li>
                    <li><img alt="ADD BIRTHDAY REMINDERS" src="<?php echo BASEURL_OIMG ?>rem.png"  class='dash'/><a href="<?php echo BASEURL . "addReminder/0" ?>">ADD BIRTHDAY REMINDERS</a></li>
                    <li><img alt="VIEW BIRTHDAY REMINDERS" src="<?php echo BASEURL_OIMG ?>bday.png"  class='dash'/><a href="<?php echo WEB_VIEW_BIRTHDAY_REMINDER; ?>">VIEW BIRTHDAY REMINDERS</a></li>
                    <li><img alt="KNOW YOUR ZODIAC" src="<?php echo BASEURL_OIMG ?>horo.png"  class='dash'/><a href="<?php echo WEB_KNOW_YOUR_HOROSCOPE; ?>"style="cursor:pointer;" id="know_horo">KNOW YOUR ZODIAC</a></li>
                    <li><img alt="ORDER HISTORY" src="<?php echo BASEURL_OIMG ?>order.png"  class='dash'/><a href="<?php echo WEB_ORDER_HISTORY; ?>">ORDER HISTORY</a></li>
                    <!--<li><img alt="GIFT WALLET" src="<?php echo BASEURL_OIMG ?>wallet.png"  class='dash'/><a href="<?php echo WEB_GIFT_WALLET; ?>">GIFT WALLET</a></li>-->
                    <li><img alt="UPDATE PROFILE" src="<?php echo BASEURL_OIMG ?>update.png"  class='dash'/><a href="<?php echo WEB_UPDATE_PROFILE; ?>">UPDATE PROFILE</a></li>
                    <li><img alt="UNSUBSCRIBE" src="<?php echo BASEURL_OIMG ?>greet_history.png" class='dash'/><a id="unsubscribe" style="cursor: pointer;" >UNSUBSCRIBE</a></li>

                </ul>
            </div>
            <div class="in_f_r dashbord_bg">
                <?php
                $default_pic = "customer_pic.png";
                $img = BASEURL_PROFILE . $profile_pic;
                if ($profile_pic == FALSE) {
                    $img = BASEURL_PROFILE . $default_pic;
                }
                ?>
                <abbr>
                    <img alt="User Image" src="<?PHP echo $img ?>" class="classimg112 changedimg"  style="  cursor: pointer;" title="Click To change Image"/>
                    <span class="changedimg" style="font-size: 16px; padding-top: 4px;font-family: gotham-book;cursor: pointer">Click to change image</span>

                </abbr>
                <span>Welcome <i> <?php echo ucfirst($first_name); ?></i></span>
                <?php if ($cdate != '') { ?>
                    <p>
                        Date of Birth : <?php echo $cdate . " " . $month . " " . $byear ?>
                    </p>
                <?php } ?>
                <?php if ($mobile_no != '') { ?>
                    <p>
                        Phone Number : <?php echo $mobile_no; ?></p>
                    <p>
                    <?php } ?>
                    <span style="font-size: 20px;color: #5cad01;font-weight: 400;">Email Id : <?php echo $email; ?></span></p>

                <?php // if ($membership_status == 1) { ?>
                <p style="color: #5CAD01;font-size: 20px;">
                    <!--Enjoy Membership Plan For Next 1 Year!!!-->
                    <?php // echo $change_status; ?>
                </p>
                <?php // } ?>

                <?php if (count($reminders) > 0) { ?>

                    <!--                    <div class="heading_3">-->
                    <img src="<?php echo BASEURL_OIMG; ?>rem_heading.png" alt="Free Birthday Reminder" />
    <!--                        <span>YOUR REMINDER THIS MONTH</span>-->
                    <!--</div>-->

                    <div class="reminder_month">
                        <ul>
                            <?php foreach ($reminders as $reminder) { ?>
                                <li class="reminderforList">
                                    <a class="js__p_start"  id="reminder" reminder_id="<?php echo $reminder["reminder_id"]; ?>" gender="<?php echo $reminder["gender"]; ?>" zodiac_id="<?php echo $reminder["zodiac_id"]; ?>" bdate="<?php echo $reminder["bdate"]; ?>" bmonth="<?php echo $reminder["bmonth"]; ?>" zimage="<?php echo $reminder["zimage"]; ?>" fname="<?php echo $reminder["first_name"]; ?>"><?php echo ucfirst($reminder["first_name"]) . " " . $reminder["last_name"] ?> - <?php echo $reminder["bdate"]; ?> th</a></li>
                            <?php } ?>

                        </ul>
                    </div>
                <?php } ?>
            </div>
            <div id="demo" >
                <div class="showimg"id="disply_img">
                    <div class="closepopupn" id="close_popup" style="color:green;font-weight: bold">X</div>
                    <div  class="picupload">
                        <form  name='dashboard_form' method="post" id="img_upload"  enctype="multipart/form-data" >
                            <input id="userfile" type="file" name="userfile"  style="display:none" />
                        </form>
                        <div style="position:relative;top:31px;left:37%;height: 98px;cursor: pointer" id="imgupload">
                            <img src="<?php echo BASEURL_OIMG; ?>technology.png"  style="width:50px; height: 50px;" alt="image upload button"/>
                        </div>
                        <div style="border-bottom: 1px solid black; position: relative; "></div>
                        <div  class="picdelete">
                            <div class="pdelete"> <img alt="delete profile image" src="<?php echo BASEURL_OIMG ?>delete.png"  style="width:50px;height: 50px; cursor: pointer   " class="removeimg"/></div>
                            <br/> <div class="removeimg" style="cursor: pointer;margin: auto; text-align: center; background: rgb(82, 163, 3) none repeat scroll 0% 0%; color: white; font-size: 15px; width: 32%; border-radius: 4px;">DELETE</div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="popupbg"></div>
                <div class="my_reminder  "> <a  class=" js__p_close" title="Close"  style="cursor:pointer"><img alt="close" src="<?php echo BASEURL_OIMG; ?>closen.png" class="closepop"></a>
                    <div class="tmessage" id="title_message"> </div>
                    <div style="padding:20px;margin-top: 39px;">
                        <div class="giftdiv HoroDiv"  horolink="" style="cursor:pointer">
                            <div id="horoscope" class="popup_horo" >
                            </div>
                            <div id="horo_text" style=" font-size: 12px;float: left;  font-family: cursive; text-align: center;padding: 5px;">
                            </div>
                        </div>
                        <div class="ordiv">OR</div>
                        <div class="giftdiv voucherDiv" voucher_link=""style="cursor:pointer" >
                            <div id="products" class="popup_product" >   </div>
                            <div id="pro_text" style="font-size: 12px;  font-family: cursive; text-align: center; float: left;padding: 5px;"> </div>
                        </div>
                        <div class="ordiv">OR</div>

                        <div class="giftdiv greetingDiv" geeting_link="" style="cursor:pointer">
                            <div id="freegreeting" class="popup_greeting" > </div>
                            <div id="greeting_text" style="font-size: 12px;  font-family: cursive; text-align: center; float: left;padding: 5px;"></div>

                        </div>
                    </div>
                </div> 

            </div>
        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript">
    $(document).ready(function () {
        if (window.location.href.indexOf('#_=_') > 0) {
            window.location = window.location.href.replace(/#.*/, '');
        }
    });
//    $(document).on("click", "#unsubscribe", function () {
//        var data = {
//            mail_status: 1
//        }
//        $.ajax({
//            type: "POST",
//            url: "<?php // echo BASEURL;            ?>unsubscribe",
//            data: data,
//            dataType: "json",
//            success: function (r) {
//                if (r.success.toString() == "false") {
//                    $("#cmessage").html(r.message.toString());
//                    $(".alertPopupDiv").addClass("show_overlay").show();
//                    $('.alertPopupMessage').animate({top: '34%'}, 500);
//                } else {
//                    $("#resubscribe").show();
//                    $("#cmessage").html("You're unsubscribed!");
//                    $(".alertPopupDiv").addClass("show_overlay").show();
//                    $('.alertPopupMessage').animate({top: '34%'}, 500);
//                }
//            }
//        });
//    });
//
//    $(document).on("click", "#resubscribe", function () {
//        var data = {
//            mail_status: 0
//        }
//        $.ajax({
//            type: "POST",
//            url: "<?php // echo BASEURL;            ?>unsubscribe",
//            data: data,
//            dataType: "json",
//            success: function (r) {
//                if (r.success.toString() == "false") {
//                    alert(r.message.toString());
//                } else {
//                    $("#resubscribe").hide();
//                    $("#cmessage").html("You're subscribed!");
//                }
//            }
//        });
//
//
//    });


    $("#imgupload").click(function () {
        $("#userfile").click();
    });
//    $("#close_popup").click(function () {
//        $("#demo").hide();
//        $('.alertPopupMessage').animate({top: '0'}, 500);
//        $(".alertPopupDiv").removeClass("show_overlay").css("display", "none");
//    });

    $(".changedimg").click(function () {
        $(".active").css("z-index", "0");
        $(".dash").css("z-index", "0");
        $("#demo").show();
        $("#disply_img").show();
    });

    $("#know_horo").click(function () {

        var data = {
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>Home_web/check_birthdate",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {
                    alert(r.message.toString());
                } else {
                    window.location = "<?php echo WEB_KNOW_YOUR_HOROSCOPE; ?>";
                }
            }
        });

    });
    $(function () {
        $("#userfile").change(function () {
            $("#demo").hide();
            var fileUpload = $(this)[0];
            var file = this.files[0];
            var filesize = this.files[0].size;
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");
            if (regex.test(fileUpload.value.toLowerCase())) {
                if (typeof (fileUpload.files) != "undefined") {
                    var reader = new FileReader();
                    reader.readAsDataURL(fileUpload.files[0]);
                    reader.onload = function (e) {
                        var image = new Image();
                        image.src = e.target.result;
                        image.name = file.name;
                        image.onload = function () {
                            if (e.target.result != '') {
                                upload_profile();

                            }
                        }
                    }
                } else {
                    alert("This browser does not support HTML5.");
                    return false;
                }
            } else {
                alert("Please select a valid Image file.");
                return false;
            }
        });
    });

    $(".removeimg").click(function () {
        $("#demo").hide();
        var iname = $(".changedimg").attr('src');
        var default_src = "<?php echo BASEURL_PROFILE . 'customer_pic.png' ?>";
        if (iname != default_src) {

            var imgname = 'customer_pic.png';
            var data = {
                user_profile_pic: imgname
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>remove-pic",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();


                },
                success: function (r) {
                    $(".showloader").removeClass("show_overlay").hide();

                    if (r.success.toString() === "true") {

                        $(".changedimg").attr('src', default_src);
                        alert(r.message.toString());
                        window.location = "<?php echo WEB_DASHBOARD; ?>";



                    } else {
                        alert(r.message.toString());

                    }
                }
            });
        }
    });


    function upload_profile() {
        var formData = new FormData($("form[name='dashboard_form']")[0]);
        var url = "<?php echo BASEURL . "upload" ?>";
        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            async: true,
            dataType: "json",
            beforeSend: function () {
                $(".showloader").addClass("show_overlay").show();
            },
            success: function (msg) {
                $(".showloader").removeClass("show_overlay").hide();

                if (msg.success.toString() == "true") {
                    $('.changedimg').attr('src', "<?php echo BASEURL_PROFILE; ?>" + msg.filename.toString());
                    alert("Profile image has been upload successfully");

                } else {
                    $(".arrowbtn").css("opacity", "1");
                    $(".arrowbtn").css("cursor", "pointer");
                    alert("upload failed");

                }
            },
            cache: false, contentType: false, processData: false});
    }



    $("#demo").click(function () {
        $(".dash").css("z-index", "111111");

        $(".active").css("z-index", "1");
        $("#disply_img").hide();
        $(this).hide();
    });


    $(".js__p_start").click(function () {
        $(".active").css("z-index", "0");
        $(".dash").css("z-index", "0");
        $('.popupbg').show();

        $('.my_reminder').css("display", "block");
//        


        $('.my_reminder').animate({top: '20%'}, 1000);
        $('html, body').animate({
            scrollTop: $(".my_reminder").offset().top
        }, 2000);
        var zodiac_id = $(this).attr("zodiac_id");
        var bdate = $(this).attr("bdate");
        var bmonth = $(this).attr("bmonth");
        var fname = $(this).attr("fname").toUpperCase();
        var zimage = $(this).attr("zimage");
        var gender = $(this).attr("gender");
        var reminder_id = $(this).attr("reminder_id");

        var gen = "";
        if (gender == 1) {
            gen = "his";
        } else {
            gen = "her";
        }
        var img_path = "<?php echo BASEURL_IMG . "zodiac/"; ?>" + zimage;
        $("#title_message").html("It’s that time of the year again for " + fname + ".We’ve got some great products to choose from to make " + gen + " birthday even special");
        $("#horoscope").css("background", 'white url("' + img_path + '") no-repeat scroll right center / 100% auto');


        $("#horo_text").html("Read " + gen + " zodiac to find out what’s the Perfect Gift for " + fname);
        $("#pro_text").html("Surprise " + fname + " with the perfect gift this birthday");

        $(".HoroDiv").attr("horolink", "know_horoscope_reminder/" + zodiac_id + "/" + bdate + "/" + bmonth);
        $("#greeting_text").html("Send " + fname + " amazing Greeting cards!")
        var data = {
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>random-product",
            data: data,
            dataType: "json",
            success: function (r) {

                if (r.success.toString() == "true") {
                    var link = "<?php echo BASEURL . "voucher/"; ?>" + r.product["voucher_url"];
                    var voucher_id = r.product["voucher_pro_id"];
                    var image;
                    if (r.product["product_image"] != "") {
                        image = r.product["product_image"];
                    } else {
                        image = "<?php echo BASEURL_OIMG . "unnamed.png"; ?>";
                    }
                    $("#products").css("background", 'white url("' + image + '") no-repeat scroll right center / 100% auto');
                    $(".voucherDiv").attr("voucher_link", "" + link);
                    $(".voucherDiv").attr("voucher_id", "" + voucher_id);

                    var greeting_data = '';
                    var greeting_img = "<?php echo BASEURL_CROPPED_GREET; ?>" + r.gcards["front_page_image"];
                    $("#freegreeting").css("background", 'white url("' + greeting_img + '") no-repeat scroll right center / 100% auto');

                    var glink = "<?php echo BASEURL . "insidegreeting/"; ?>" + r.gcards["card_id"] + "/" + reminder_id + "/0";
                    $(".greetingDiv").attr("geeting_link", "" + glink);


                    // $("#freegreeting").html(greeting_data);
                }
            }
        });
    });


    $(".js__p_close").click(function () {
        $(".active").css("z-index", "1");
        $(".dash").css("z-index", "1");
        $('.my_reminder').css("display", "none");
        $('.my_reminder').animate({top: '0'}, 1000);
        $('.popupbg').hide();

    });



    $(".HoroDiv").click(function () {
        window.location = "<?php echo BASEURL . "Home_web/" ?>" + $(this).attr("horolink");

    });

    $(".voucherDiv").click(function () {
        var value_id = $(this).attr("voucher_id");
        var value_link = $(this).attr("voucher_link")
//        alert($(this).attr("voucher_link"));
        var data = {
            voucher_pro_id: value_id
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>voucher-data",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() == "false") {
                    alert(r.message.toString());
                    return false;
                } else {
                    window.location = value_link;
                }
            }
        });
    });

    $(".greetingDiv").click(function () {
        window.location = "" + $(this).attr("geeting_link");

    });
</script>
