<?php $this->load->view("new_templates/header"); ?>
<style>
    .commoncss{
        margin-top: 18px; text-align: justify; font-size: 17px; line-height: 25px;font-family:Gotham;
        .btn-primary:focus,.btn-primary.focus{
            border-color: #5cad01 !important;
            background-color: #5cad01 !important;
            color:#fff !important;
        }
        .sortinfo{
        padding: 60px;
        text-align: justify;
        font-family:"gotham-book";
        font-size:15px;
    }
    }
    .sortinfo{
        padding: 60px;
        text-align: justify;
        font-family:"gotham-book";
        font-size:15px;
    }

    #add_birthday1 {

        border: none;
        padding: 10px 20px;
        font-size: 18px;
        font-family: MyriadPro-Regular;
        color: #FFF;
        cursor: pointer;
        border-radius: 0;
        -moz-border-radius: 0;
        -ms-border-radius: 0;
        -o-border-radius: 0;
        -webkit-border-radius: 0;
        background: #5cad01;
        font-weight: bold;
    }
     @media only screen and (max-width: 767px){

        
		 .sortinfo{
        padding: 10px;
        text-align: justify;
        font-family:"gotham-book";
        font-size:15px;
    }

    }
</style>

<div class="wrapper">
    <div class="container_12">
        <div class="inner_midd">
            <div class="col-xs-12 col-sm-8 col-md-9" style="">
                <div class="grid_12">

                    <div class="zodiac-content1">
                        <div class="zodiac-img">
                            <div class="heading_2" >
                                Birthday <span>Reminders</span>
                            </div>
                        </div>
                        <div style="background-color: white;padding: 5px;font-family:Gotham;font-size: 17px; line-height: 25px;">
                            <?php // echo $about_us; ?>
                            <img style="cursor: pointer;" class="inline_colorbox cboxElement" href="#Login_Reminder" src="<?php echo BASEURL_ONEWIMG; ?>Online_Birthday_Reminder.png" alt="online birthday reminder"/>
                        </div>
                        <div style="text-align: center;">
                            <input href="#Login_Reminder" class="inline_colorbox cboxElement" type="button" value="Click to Proceed" id="add_birthday1">
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-4 col-md-3" style="text-align:center;">
                <img src="<?php echo BASEURL_OIMG; ?>Birthday_Calendar_Reminder.png" id="blah" alt="Birthday Calendar Reminder" style="padding-left: 0px;" alt="1" />
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container-fluid sortinfo">
    <h1 style="font-size: 22px;text-align: center;font-weight: 600;" >Birthday Reminder</h1>
       <h2 style="font-size: 22px;">Birthday Reminder</h2> 
    <p> Never forget another birthday, ever! With Birthdayowl™ by your side, you just need to input all your important birthdays and other dates once and that’s all – we ensure that you are reminded well in advance of these upcoming important dates. </p>

    <p>We understand that it can be embarrassing and quite a lousy feeling to have forgotten an important birthday or event, and while we know how busy your lives are with work or college or home, it is still not enough of an excuse to forget such dates – especially when you have easy tools like Birthdayowl™ to help you remember and also send <a href="https://www.birthdayowl.com/free-birthday-greeting-cards"><b>Free Birthday Greeting Cards</b></a> or purchase and send across gifts with ease.</p>

    <p>Our <a href="https://www.birthdayowl.com/birthday-reminder"><b>Free Birthday Calendar Reminder</b></a> is super easy to input and takes only a few minutes if you have the birthdates ready with you. Alternatively if you do not know everyone’s birthdates, you can simply input all the names of people that matter to you the most, and thereafter gradually figure out their birthdates and then only input the dates corresponding to their names, and it's all done your end -  leave the rest to us really. </p>
    
    <p>Remembering birthdays and other important events is a wonderful and empowering feeling. You would definitely feel in control of things when you have the information and knowledge of something which other people tend to forget in their busy lives. That makes all the difference. </p>
    
    <h2 style="font-size: 22px;">Online Birthday Calendar Reminder</h2> 
    <p>Never forget another birthday, ever! With our online birthday reminder services by your side, we will ensure that you are reminded well in advance of all your important important dates.</p>

    <p>We understand that it can be embarrassing and quite a lousy feeling to have forgotten an important birthday or event, and while we know how busy your lives are with work or college or home, it is still not enough of an excuse to forget such dates – especially when you have easy tools like online birthday reminder.</p>

    <p>Our calendar is super easy to input and takes only a few minutes if you have the birth dates ready with you. Alternatively if you do not know everyone’s birth dates, you can simply input all the names of people that matter to you the most, and thereafter gradually figure out their birth dates and then only input the dates corresponding to their names, and it's all done your end -  leave the rest to us really.</p>
    
    <p>Remembering birthdays and other important events is a wonderful and empowering feeling. You would definitely feel in control of things when you have the information and knowledge of something which other people tend to forget in their busy lives. That makes all the difference.</p>
    </div>
        
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript">

    if ($(window).width() < 768) {
        $("#blah").css({"top": "11px", "padding": "0px"});
    }
    if ($(window).width() > 768) {
        $(function () {

            var $blah = $("#blah"),
                    $window = $(window),
                    offset = $blah.offset();

            $window.scroll(function () {
                if ($window.scrollTop() > offset.top) {
                    $blah.stop().animate({
                        top: 650
                    });
                } else {
                    $blah.stop().animate({
                        top: 0
                    });
                }
            });
        });
    }


</script>
