<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title><?php echo $title; ?></title>
        <link rel="shortcut icon" type="image/png" href="<?php echo BASEURL_OIMG; ?>main_icn.png"/>
        <style type="text/css">
            a{
                text-decoration: none;
            }
            img{
                margin: 5px;
            }
            h3{
                margin: 0;
                padding: 0;
            }
            body{
                font-family: verdana, arial, sans-serif;
            }
        </style>

    </head>
    <body>

        <div style="text-align: center; margin: 293px auto auto; line-height: 24px;">

            <div style="font-size: 20px;"><?php echo $message; ?></div>
            <?php if($clink!=""){ ?>
            <div style="cursor: pointer;text-decoration: underline;"><a href='<?php echo $clink; ?>' ><?php echo $link; ?></a></div>
            <?php } ?>

        </div>

    </body>
</html>