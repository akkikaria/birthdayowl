<?php $this->load->view("new_templates/header"); ?>

<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            Today's <span>Zodiac Sign</span></div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <!--        <div class="alertPopupDiv" >
                    <div class="alertPopupMessage">
                        <div class="closepopup" id="close_popup"> <img src="<?php // echo BASEURL_OIMG;                        ?>closer.png" /> </div>
                        <br/>
                        <br/>
                        <div style="text-align: center; margin: auto auto; width: 100%; height:120px;line-height: 29px;">
                            <div style="font-size: 20px;" id='cmessage'>You're unsubscribed.</div>
                            <div style="text-decoration: underline;cursor: pointer;" id='resubscribe'>Re-subscribed?</div>
                            <br/>
                        </div>
                    </div>
                </div>-->
        <div class="inner_midd dashbord">
            <div class="in_f_r">
                <?php if ($zodiac_id == '1') { ?>
                    <div class="horoscope_image">
                        <img alt="Aries" src="<?php echo BASEURL_IMG; ?>zodiac/Aries.png" />
                        <abbr style="font-family: gotham-book;">Aries <span>(<?php echo $date; ?>&nbsp;<?php echo $month; ?>)</span></abbr>
                    </div>
                <?php } else if ($zodiac_id == '2') { ?>
                    <div class="horoscope_image">
                        <img alt="Taurus" src="<?php echo BASEURL_IMG; ?>zodiac/taurus.png" />
                        <abbr style="font-family: gotham-book;">Taurus <span>(<?php echo $date; ?>&nbsp;<?php echo $month; ?>)</span></abbr>
                    </div>
                <?php } else if ($zodiac_id == '3') { ?>
                    <div class="horoscope_image">
                        <img alt="Gemini" src="<?php echo BASEURL_IMG; ?>zodiac/gemini.png" />
                        <abbr style="font-family: gotham-book;">Gemini <span>(<?php echo $date; ?>&nbsp;<?php echo $month; ?>)</span></abbr>
                    </div>
                <?php } else if ($zodiac_id == '4') { ?>
                    <div class="horoscope_image">
                        <img alt="Cancer" src="<?php echo BASEURL_IMG; ?>zodiac/cancer.png" />
                        <abbr style="font-family: gotham-book;">Cancer <span>(<?php echo $date; ?>&nbsp;<?php echo $month; ?>)</span></abbr>
                    </div>
                <?php } else if ($zodiac_id == '5') { ?>
                    <div class="horoscope_image">
                        <img alt="Leo" src="<?php echo BASEURL_IMG; ?>zodiac/leo.png" />
                        <abbr style="font-family: gotham-book;">Leo <span>(<?php echo $date; ?>&nbsp;<?php echo $month; ?>)</span></abbr>
                    </div>
                <?php } else if ($zodiac_id == '6') { ?>
                    <div class="horoscope_image">
                        <img alt="Virgo" src="<?php echo BASEURL_IMG; ?>zodiac/virgo.png" />
                        <abbr style="font-family: gotham-book;">Virgo <span>(<?php echo $date; ?>&nbsp;<?php echo $month; ?>)</span></abbr>
                    </div>
                <?php } else if ($zodiac_id == '7') { ?>
                    <div class="horoscope_image">
                        <img alt="Libra" src="<?php echo BASEURL_IMG; ?>zodiac/libra.png" />
                        <abbr style="font-family: gotham-book;">Libra <span>(<?php echo $date; ?>&nbsp;<?php echo $month; ?>)</span></abbr>
                    </div>
                <?php } else if ($zodiac_id == '8') { ?>
                    <div class="horoscope_image">
                        <img alt="Scorpio" src="<?php echo BASEURL_IMG; ?>zodiac/scorpio.png" />
                        <abbr style="font-family: gotham-book;">Scorpio <span>(<?php echo $date; ?>&nbsp;<?php echo $month; ?>)</span></abbr>
                    </div>
                <?php } else if ($zodiac_id == '9') { ?>
                    <div class="horoscope_image">
                        <img alt="Sagittarius" src="<?php echo BASEURL_IMG; ?>zodiac/sagittarius.png" />
                        <abbr style="font-family: gotham-book;">Sagittarius <span>(<?php echo $date; ?>&nbsp;<?php echo $month; ?>)</span></abbr>
                    </div>
                <?php } else if ($zodiac_id == '10') { ?>
                    <div class="horoscope_image">
                        <img alt="Capricorn" src="<?php echo BASEURL_IMG; ?>zodiac/capricorn.png" />
                        <abbr style="font-family: gotham-book;">Capricorn <span>(<?php echo $date; ?>&nbsp;<?php echo $month; ?>)</span></abbr>
                    </div>
                <?php } else if ($zodiac_id == '11') { ?>
                    <div class="horoscope_image">
                        <img alt="Aquarius" src="<?php echo BASEURL_IMG; ?>zodiac/aquarius.png" />
                        <abbr style="font-family: gotham-book;">Aquarius <span>(<?php echo $date; ?>&nbsp;<?php echo $month; ?>)</span></abbr>
                    </div>
                <?php } else if ($zodiac_id == '12') { ?>
                    <div class="horoscope_image">
                        <img alt="Pisces" src="<?php echo BASEURL_IMG; ?>zodiac/pisces.png" />
                        <abbr style="font-family: gotham-book;">Pisces <span>(<?php echo $date; ?>&nbsp;<?php echo $month; ?>)</span></abbr>
                    </div>
                <?php } ?>

                <div id="fpart"  style="display: none;"> <?php echo $first_part; ?></div>
                <div id="spart"  style="display: none;"> <?php echo $second_part; ?></div>

                <div class="wrapper res100_768">
                    <div class="grid_8" >
                        <div class="horoscope_content" id="first_part"  style="font-size: 16px; font-family: gotham-book;">

                        </div>
                    </div>
                    <div class="grid_4 horoscope_owl" id="owlimg" >

                    </div>
                </div>

                <div class="horoscope_content wrapper" id="second_part" style="font-size: 16px; font-family: gotham-book;">





                </div>

                <a href="<?php echo WEB_ZODIAC_SIGNS; ?>" style="background-color: #5cad01 ;margin: auto;text-align: center;border: medium none; display: table; border-radius: 6px;padding: 10px;color: #fff; cursor: pointer;font-family: gotham-book !important;;font-size: 18px;">View other Zodiac Signs</a> 

            </div>
        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script>
    $(document).ready(function () {
        var fcontent = $("#fpart").html();
        var scontent = $("#spart").html();
        var oimg = "<?php echo BASEURL_OIMG . 'horoscope_owl.png' ?>";
        var data = '';
        data = '<img src="' + oimg + '"> ';
        $('#first_part').html(fcontent);
        $("#second_part").html(scontent);
        $("#owlimg").html(data);
    });

</script>
