<?php $this->load->view("new_templates/header"); ?>

<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            Know <span>Your Zodiac</span></div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <!--        <div class="alertPopupDiv" >
                    <div class="alertPopupMessage">
                        <div class="closepopup" id="close_popup"> <img src="<?php // echo BASEURL_OIMG;                         ?>closer.png" /> </div>
                        <br/>
                        <br/>
                        <div style="text-align: center; margin: auto auto; width: 100%; height:120px;line-height: 29px;">
                            <div style="font-size: 20px;" id='cmessage'>You're unsubscribed.</div>
                            <div style="text-decoration: underline;cursor: pointer;" id='resubscribe'>Re-subscribed?</div>
                            <br/>
                        </div>
                    </div>
                </div>-->
        <div class="inner_midd dashbord">
            <div class="in_f_l">
               	<div class="dashboard-res-menu">

               	</div>
                <ul class="leftside dashbord_nav">
                    <li><img alt="DASHBOARD" src="<?php echo BASEURL_OIMG ?>dash.png"  class='dash'/><a href="<?php echo WEB_DASHBOARD; ?>">DASHBOARD</a></li>
                    <li ><img alt="ADD BIRTHDAY REMINDERS" src="<?php echo BASEURL_OIMG ?>rem.png"  class='dash'/><a href="<?php echo BASEURL . "addReminder/0" ?>">ADD BIRTHDAY REMINDERS</a></li>
                    <li><img alt="VIEW BIRTHDAY REMINDERS" src="<?php echo BASEURL_OIMG ?>bday.png"  class='dash'/><a href="<?php echo WEB_VIEW_BIRTHDAY_REMINDER; ?>">VIEW BIRTHDAY REMINDERS</a></li>
                    <li class="active"><img alt="KNOW YOUR ZODIAC" src="<?php echo BASEURL_OIMG ?>horo.png"  class='dash'/><a href="<?php echo WEB_KNOW_YOUR_HOROSCOPE; ?>">KNOW YOUR ZODIAC</a></li>
                    <li><img alt="ORDER HISTORY" src="<?php echo BASEURL_OIMG ?>order.png"  class='dash'/><a href="<?php echo WEB_ORDER_HISTORY; ?>">ORDER HISTORY</a></li>
                    <li><img alt="GIFT WALLET" src="<?php echo BASEURL_OIMG ?>wallet.png"  class='dash'/><a href="<?php echo WEB_GIFT_WALLET; ?>">GIFT WALLET</a></li>
                    <li><img alt="UPDATE PROFILE" src="<?php echo BASEURL_OIMG ?>update.png"  class='dash'/><a href="<?php echo WEB_UPDATE_PROFILE; ?>">UPDATE PROFILE</a></li>
                    <li><img alt="UNSUBSCRIBE" src="<?php echo BASEURL_OIMG ?>greet_history.png" class='dash'/><a id="unsubscribe" style="cursor: pointer;" >UNSUBSCRIBE</a></li>
                </ul>
            </div>

            <?php if ($sign_data && $zodiac_id != " ") { ?>
                <div class="in_f_r">

                    <?php if ($zodiac_id == '1') { ?>
                        <div class="horoscope_image">
                            <img alt="Aries" src="<?php echo BASEURL_IMG; ?>zodiac/Aries.png" />
                            <abbr>Aries <span>(March 21 - April 19)</span></abbr>
                        </div>

                    <?php } else if ($zodiac_id == '2') { ?>

                        <div class="horoscope_image">
                            <img alt="Taurus" src="<?php echo BASEURL_IMG; ?>zodiac/taurus.png" />
                            <abbr>Taurus <span>(April 20 - May 20)</span></abbr>
                        </div>

                    <?php } else if ($zodiac_id == '3') { ?>
                        <div class="horoscope_image">
                            <img alt="Gemini" src="<?php echo BASEURL_IMG; ?>zodiac/gemini.png" />
                            <abbr>Gemini <span>(May 21 - June 20)</span></abbr>
                        </div>

                    <?php } else if ($zodiac_id == '4') { ?>
                        <div class="horoscope_image">
                            <img alt="Cancer" src="<?php echo BASEURL_IMG; ?>zodiac/cancer.png" />
                            <abbr>Cancer <span>(June 21 - July 22)</span></abbr>
                        </div>


                    <?php } else if ($zodiac_id == '5') { ?>

                        <div class="horoscope_image">
                            <img alt="Leo" src="<?php echo BASEURL_IMG; ?>zodiac/leo.png" />
                            <abbr>Leo <span>(July 23 - August 22)</span></abbr>
                        </div>


                    <?php } else if ($zodiac_id == '6') { ?>

                        <div class="horoscope_image">
                            <img alt="Virgo" src="<?php echo BASEURL_IMG; ?>zodiac/virgo.png" />
                            <abbr>Virgo <span>(August 23 - Sept. 22)</span></abbr>
                        </div>


                    <?php } else if ($zodiac_id == '7') { ?>
                        <div class="horoscope_image">
                            <img alt="Libra" src="<?php echo BASEURL_IMG; ?>zodiac/libra.png" />
                            <abbr>Libra <span>(Sept. 23 - October 22)</span></abbr>
                        </div>

                    <?php } else if ($zodiac_id == '8') { ?>
                        <div class="horoscope_image">
                            <img alt="Scorpio" src="<?php echo BASEURL_IMG; ?>zodiac/scorpio.png" />
                            <abbr>Scorpio <span>(October 23 - Nov.  21)</span></abbr>
                        </div>

                    <?php } else if ($zodiac_id == '9') { ?>
                        <div class="horoscope_image">
                            <img alt="Sagittarius" src="<?php echo BASEURL_IMG; ?>zodiac/sagittarius.png" />
                            <abbr>Sagittarius <span>(November 22  - December  21)</span></abbr>
                        </div>


                    <?php } else if ($zodiac_id == '10') { ?>
                        <div class="horoscope_image">
                            <img alt="Capricorn" src="<?php echo BASEURL_IMG; ?>zodiac/capricorn.png" />
                            <abbr>Capricorn <span>(December 22  - January  19)</span></abbr>
                        </div>

                    <?php } else if ($zodiac_id == '11') { ?>
                        <div class="horoscope_image">
                            <img alt="Aquarius" src="<?php echo BASEURL_IMG; ?>zodiac/aquarius.png" />
                            <abbr>Aquarius <span>(January 20  - February  18)</span></abbr>
                        </div>


                    <?php } else if ($zodiac_id == '12') { ?>
                        <div class="horoscope_image">
                            <img alt="Pisces" src="<?php echo BASEURL_IMG; ?>zodiac/pisces.png" />
                            <abbr>Pisces <span>(February 19  - March  20)</span></abbr>
                        </div>


                    <?php } ?>
                    <div id="fpart"  style="display: none;"> <?php echo $first_part; ?></div>
                    <div id="spart"  style="display: none;"> <?php echo $second_part; ?></div>

                    <div class="wrapper res100_768">
                        <div class="grid_8" >
                            <div class="horoscope_content" id="first_part"  style="font-size: 17px;">

                            </div>
                        </div>
                        <div class="grid_4 horoscope_owl" id="owlimg" >

                        </div>
                    </div>

                    <div class="horoscope_content wrapper" id="second_part" style="font-size: 15px;font-family:gotham-book;">





                    </div>

                    <a href="<?php echo WEB_ZODIAC_SIGNS; ?>" style="background-color: #5cad01 ;margin: auto;text-align: center;border: medium none; display: table; border-radius: 6px;padding: 10px;color: #fff; cursor: pointer;font-family: MyriadPro-Regular;font-size: 18px;">View other Zodiac Signs</a> 

                </div>
            <?php } else { ?>
                <!--Div for guest login to update their profile-->
                <div class="in_f_r" style="text-align: center; font-size: 20px;">
                    <p style="margin-top: 15px;">Hi there - you need to first input and save your birth date in Update Profile tab. Once you've done that, your Zodiac details will be displayed automatically here.</p>
                    <a href="<?php echo WEB_UPDATE_PROFILE; ?>">Click Here...</a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script>
    //    $("#close_popup").click(function () {
    //        $('.alertPopupMessage').animate({top: '0'}, 500);
    //        $(".alertPopupDiv").removeClass("show_overlay").css("display", "none");
    //
    //    });
    //    $(document).on("click", "#unsubscribe", function () {
    //        $("#resubscribe").show();
    //        $("#cmessage").html("You're unsubscribed!");
    //        $(".alertPopupDiv").addClass("show_overlay").show();
    //        $('.alertPopupMessage').animate({top: '34%'}, 500);
    //        var data = {
    //            mail_status: 1
    //        }
    //        $.ajax({
    //            type: "POST",
    //            url: "<?php // echo BASEURL;                         ?>unsubscribe",
    //            data: data,
    //            dataType: "json",
    //            success: function (r) {
    //
    //            }
    //        });
    //    });
    //    $(document).on("click", "#resubscribe", function () {
    //        $(this).hide();
    //        $("#cmessage").html("You're subscribed!");
    //        var data = {
    //            mail_status: 0
    //        }
    //        $.ajax({
    //            type: "POST",
    //            url: "<?php // echo BASEURL;                         ?>unsubscribe",
    //            data: data,
    //            dataType: "json",
    //            success: function (r) {
    //
    //            }
    //        });
    //
    //
    //    });
    $(document).ready(function () {
        var fcontent = $("#fpart").html();
        var scontent = $("#spart").html();
        var oimg = "<?php echo BASEURL_OIMG . 'horoscope_owl.png' ?>";
        var data = '';
        data = '<img src="' + oimg + '"> ';
        $('#first_part').html(fcontent);
        $("#second_part").html(scontent);
        $("#owlimg").html(data);
    });

</script>
