<?php $this->load->view("new_templates/header"); ?>
<link rel="stylesheet" href="<?php echo BASEURL_OCSS ?>animate.css">
<link rel="stylesheet" href="<?php echo BASEURL_OCSS ?>tipso.css">
<script src="<?php echo BASEURL_OJS ?>tipso.min.js"></script>
<link rel="stylesheet" href="<?php echo BASEURL_OCSS ?>reminder.css">

<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">

        <div class="heading_2">
            Order <span>History</span>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <!--        <div class="alertPopupDivnew" >
                    <div class="alertPopupMessagenew">
                        <div class="closepopup" id="close_popupnew"> <img src="<?php // echo BASEURL_OIMG;    ?>closer.png" /> </div>
                        <br/>
                        <br/>
                        <div style="text-align: center; margin: auto auto; width: 100%; height:120px;line-height: 29px;">
                            <div style="font-size: 20px;" id='cmessage'>You're unsubscribed.</div>
                            <div style="text-decoration: underline;cursor: pointer;" id='resubscribe'>Re-subscribed?</div>
                            <br/>
                        </div>
                    </div>
                </div>-->
        <div class="inner_midd dashbord">
            <div class="in_f_l">
               	<div class="dashboard-res-menu">

               	</div>
                <ul class="leftside dashbord_nav">
                    <li><img alt="DASHBOARD" src="<?php echo BASEURL_OIMG ?>dash.png"  class='dash'/><a href="<?php echo WEB_DASHBOARD; ?>">DASHBOARD</a></li>
                    <li ><img alt="ADD BIRTHDAY REMINDERS" src="<?php echo BASEURL_OIMG ?>rem.png"  class='dash'/><a href="<?php echo BASEURL . "addReminder/0" ?>">ADD BIRTHDAY REMINDERS</a></li>
                    <li><img alt="VIEW BIRTHDAY REMINDERS" src="<?php echo BASEURL_OIMG ?>bday.png"  class='dash'/><a href="<?php echo WEB_VIEW_BIRTHDAY_REMINDER; ?>">VIEW BIRTHDAY REMINDERS</a></li>
                    <li><img alt="KNOW YOUR ZODIAC" src="<?php echo BASEURL_OIMG ?>horo.png"  class='dash'/><a href="<?php echo WEB_KNOW_YOUR_HOROSCOPE; ?>" style="cursor:pointer;" id="know_horo">KNOW YOUR ZODIAC</a></li>
                    <li class="active"><img alt="ORDER HISTORY" src="<?php echo BASEURL_OIMG ?>order.png"  class='dash'/><a href="<?php echo WEB_ORDER_HISTORY; ?>">ORDER HISTORY</a></li>
                    <li><img alt="GIFT WALLET" src="<?php echo BASEURL_OIMG ?>wallet.png"  class='dash'/><a href="<?php echo WEB_GIFT_WALLET; ?>">GIFT WALLET</a></li>

                    <li><img alt="UPDATE PROFILE" src="<?php echo BASEURL_OIMG ?>update.png"  class='dash'/><a href="<?php echo WEB_UPDATE_PROFILE; ?>">UPDATE PROFILE</a></li>
                    <li><img alt="UNSUBSCRIBE" src="<?php echo BASEURL_OIMG ?>greet_history.png" class='dash'/><a id="unsubscribe" style="cursor: pointer;" >UNSUBSCRIBE</a></li>

                </ul>
                <!--                <div  id="greetingmov" style="margin-top:10px;display:none;">
                                    <div class="leftside" style="border:0;" >
                                        <ul class="leftside dashbord_nav1" >
                                            <li class="greetfree">Free Greetings:<?php // echo $gcount;       ?></li>
                                            <li class="greetfreemiddle"><?php // echo $sent_count;       ?></li>
                                            <li class="greetfree" ><?php // echo $left_count;       ?></li>
                                            <li class="greetfreeend">Membership Plan added: <?php // echo $membership_status;       ?></li>
                                        </ul>
                
                                    </div>
                                </div>-->
            </div>
            <div class="alertPopupDiv">
                <div class="giftalertPopupMessage" >
                    <div class="bclose" id="alertclose">X</div>
                    <br/>
                    <br/>
                    <div style="text-align: center; margin: auto auto; width: 100%; height:105px;">
                        <div style="font-weight:bold;font-size: 18px;">Hi,<?php echo ucfirst($user_name); ?></div>
                        <div class="mess"></div>
                        <div class="planm" >Would you like to buy our Membership plan?</div>
                    </div>

                    <div style="display:inline-flex">
                        <div class="alertclose" id="yesm"style="margin:2px;">Yes</div>
                        <div class="alertclose" id="alertclosep">No</div>
                    </div>



                </div>
            </div>
            <div class="alertBox"></div>
            <div class="in_f_r" style="text-align: center;">

                <ul class="tabs  primary-nav">
<!--                    <li class="tabs__item" id="egift_orders" style="cursor:pointer;">
                        <a  class="tabs__link">e-Gift Cards</a>
                    </li>-->
                    <li class="tabs__item" id="greetingorders" style="cursor:pointer;">
                        <a  class="tabs__link">Free Greeting Cards</a>
                    </li>
                    <li class="tabs__item" id="flowerorders" style="cursor:pointer;">
                        <a  class="tabs__link">Flowers & Cakes</a>
                    </li>

                </ul>
                <div style="border:none;display:none"  id="greeting_images">

                    <?php if (count($history) > 0) { ?>

                        <?php foreach ($history as $history) { ?>
                            <div  class="o_history">

                                <?php
                                $status = $history["is_sent"];
                                if ($status == 1) {

                                    $statusval = "Sent Successfully";
                                    $ind_img = "successr.png";
                                } else if ($status == 0) {

                                    $statusval = "Failed";
                                    $ind_img = "fail.png";
                                } else if ($status == 2) {

                                    $statusval = "Pending";
                                    $ind_img = "info_pending.png";
                                }
                                ?>
                                <div class="outer_order_history">
                                    <div class="outerorderone">
                                        <div>
                                            <img src="<?php echo BASEURL_CROPPED_GREET . $history["greeting_img"]; ?>" class="selected_greeting" alt="greeting card" />
                                        </div>
                                        <div class="vmessage" style="cursor:pointer;" img_vid_audio_upload="<?php echo BASEURL_BVMG . $history['uploded_img']; ?>" mtype="<?php echo $history['type'] ?>" >
                                            <?php if ($history["type"] == 1) { ?>
                                                <img src="<?php echo BASEURL_OIMG ?>image_order.png" alt="order" />
                                            <?php } else if ($history["type"] == 2) { ?>
                                                <img src="<?php echo BASEURL_OIMG ?>video_order.png" />
                                            <?php } else if ($history["type"] == 3) { ?>
                                                <img src="<?php echo BASEURL_OIMG ?>audio_order.png" alt="audio message for you"  />

                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="outerordertwogreeting">
                                        <div class="orderinfo">
                                            <ul style="padding-top: 32px; line-height:25px;font-family: gotham-book;">
                                                <li><b>Order Id: </b>#<span><?php echo $history['fid']; ?></span></li>
                                                <li><b>Receiver Name: </b><?php echo $history["fname"]; ?></li>
                                                <?php if ($history["femail"] != "") { ?>
                                                    <li><b>Email: </b><?php echo $history["femail"]; ?></li>
                                                <?php } ?>
                                                <?php if ($history["fphone"] != "") { ?>
                                                    <li><b>Mobile Number: </b><?php echo $history["fphone"]; ?></li>
                                                <?php } ?>
                                                <li><b>Sent: </b><?php echo date("d/m/Y", strtotime($history["delivery_date"])); ?></li>


                                            </ul>
                                        </div>
                                        <div class="resendDiv">
                                            <div class="statustext"><b>Status: </b><span style="width: 75px;padding-right:10px;"><?php echo $statusval; ?></span><div style="vertical-align: middle;display: inline-block"><img alt="birthdayowl" src='<?php echo BASEURL_OIMG . $ind_img; ?>' statusvalue="<?php echo $status; ?>" width="22" height="22" style="cursor: pointer;"class="top-right" data-tipso="<?php echo $statusval; ?>" /></div></div>
                                            <?php if ($status == "1") { ?>
                                                <div class="resend_greeting"  fid="<?php echo $history["fid"]; ?>" >Resend Greeting</div>
                                            <?php }if ($status == "0") { ?>
                                                <div class="try_again_greeting" fid="<?php echo $history["fid"]; ?>" >Try Again</div>

                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        ?>
                        <div style="margin: auto;text-align: center;padding:104px;font-size: 20px;">No Order History</div>
                    <?php } ?>

                </div>
                <div style="border:none;display:none"  id="flowers">

                    <?php if (count($flower_history) > 0) { ?>

                        <?php foreach ($flower_history as $fhistory) { ?>
                            <div  class="o_history">

                                <?php
                                $status = $fhistory["flower_is_delivered_status"];

                                if ($status == 1) {
                                    $statusval = "Order Received";
                                    $ind_img = "pending.png";
                                } else if ($status == 2) {
                                    $statusval = "Shipped";
                                    $ind_img = "pending.png";
                                } else if ($status == 3) {
                                    $statusval = "Refund";
                                    $ind_img = "pending.png";
                                } else if ($status == 4) {
                                    $statusval = "Delivered";
                                    $ind_img = "successr.png";
                                } else if ($status == 5) {
                                    $statusval = "Refunded";
                                    $ind_img = "successr.png";
                                } else {
                                    $statusval = "Action Not defined";
                                    $ind_img = "info_pending.png";
                                }


//                                if ($status == 1) {
//                                    
//                                } else if ($status == 0) {
//                                    $statusval = "Failed";
//                                    $ind_img = "fail.png";
//                                } else if ($status == 2) {
//                                    $statusval = "Successful";
//                                    $ind_img = "successr.png";
//                                }
                                ?>
                                <div class="outer_order_history">
                                    <div class="outerorderone">
                                        <div>
                                            <img src="<?php echo BASEURL_PRODUCT_IMG . $fhistory["flower_purchased_image"]; ?>" class="selected_greeting" alt="flowers and cake purchase" />
                                        </div>
                                        <div class="vmessage" style="cursor:pointer;" img_vid_audio_upload="<?php // echo BASEURL_BVMG . $history['uploded_img'];                                                                                   ?>" mtype="<?php // echo $history['type']                                                                                   ?>" >
                                            <?php // if ($history["type"] == 1) {   ?>
                                                <!--<img src="<?php echo BASEURL_OIMG ?>image_order.png" />-->
                                            <?php // } else if ($history["type"] == 2) {   ?>
                                                <!--<img src="<?php echo BASEURL_OIMG ?>video_order.png" />-->
                                            <?php // } else if ($history["type"] == 3) {   ?>
                                                <!--<img src="<?php echo BASEURL_OIMG ?>audio_order.png" />-->

                                            <?php // }   ?>
                                        </div>
                                    </div>
                                    <div class="outerordertwogreeting">
                                        <div class="orderinfo">
                                            <ul style="padding-top: 32px; line-height:25px;">
                                                <li><b>Order Id: </b>#<span><?php echo $fhistory['flower_unique_oid']; ?></span></li>
                                                <li><b>Receiver Name: </b><?php echo $fhistory["flower_receiver_name"]; ?></li>
                                                <?php if ($fhistory["flower_receiver_email"] != "") { ?>
                                                    <li><b>Email: </b><?php echo $fhistory["flower_receiver_email"]; ?></li>
                                                <?php } ?>
                                                <?php if ($fhistory["flower_receiver_phone"] != "") { ?>
                                                    <li><b>Mobile Number: </b><?php echo $fhistory["flower_receiver_phone"]; ?></li>
                                                <?php } ?>


                                            </ul>
                                        </div>
                                        <div class="resendDiv">
                                            <div class="statustext"><b>Status: </b><span style="width: 75px;padding-right:10px;"><?php echo $statusval; ?></span><div style="vertical-align: middle;display: inline-block"><img alt="owl" src='<?php echo BASEURL_OIMG . $ind_img; ?>' statusvalue="<?php echo $status; ?>" width="22" height="22" style="cursor: pointer;"class="top-right" data-tipso="<?php echo $statusval; ?>" /></div></div>
                                            <?php // if ($status == "3") { ?>
                                                <!--<div class=""  fid="<?php // echo $fhistory["flower_order_id"];                                ?>" >Re-buy Product</div>-->
                                            <?php // }if ($status == "0") { ?>
                                                <!--<div class="try_again_greeting" fid="<?php // echo $fhistory["flower_order_id"];                                ?>" >Try Again</div>-->

                                            <?php //    } ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        ?>
                        <div style="margin: auto;text-align: center;padding:104px;font-size: 20px;">No Order History</div>
                    <?php } ?>

                </div>

                <div  style="border:none;"  id="vouchersection">

                    <?php
                    if (count($ehistory) > 0) {
                        $i = 0;
                        ?>

                        <?php foreach ($ehistory as $history) { ?>
                            <div  class="o_history">
                                <?php
                                $status = $history["delivery_status"];
                                switch ($status) {
                                    case 0:
                                    case 3: {
                                            $statusval = "Failed";
                                            $ind_img = "fail.png";
                                        }
                                        break;
                                    case 1: {
                                            $statusval = "Sending e-Gift Cards..";
                                            $ind_img = "pending.png";
                                        }
                                        break;
                                    case 2: {
                                            $statusval = "Pending";
                                            $ind_img = "info_pending.png";
                                        }
                                        break;
                                    case 4: {
                                            $statusval = " Successful";
                                            $ind_img = "successr.png";
                                        }
                                        break;
                                    case 6: {
                                            $statusval = "Processing";
                                            $ind_img = "pending.png";
                                        }
                                        break;
                                    case 8: {
                                            $statusval = "Failed";
                                            $ind_img = "info_error.png";
                                        }
                                        break;
                                    case 10: {
                                            $statusval = "Refunded";
                                            $ind_img = "successr.png";
                                        }
                                        break;
                                }
                                ?>


                                <div class="outer_order_history">
                                    <div class="outerorderone">
                                        <div>
                                            <img alt="greeting card" src="<?php echo BASEURL_CROPPED_GREET . $history["front_page_image"]; ?>" class="selected_greeting" />
                                        </div>

                                        <div class="vmessage" style="cursor:pointer;" img_vid_audio_upload="<?php echo BASEURL_BVMG . $history['uploded_img_video']; ?>" mtype="<?php echo $history['type'] ?>" >
                                            <?php if ($history["type"] == 1) { ?>
                                                <img alt="order icon" src="<?php echo BASEURL_OIMG ?>image_order.png" />
                                            <?php } else if ($history["type"] == 2) { ?>
                                                <img alt="order video" src="<?php echo BASEURL_OIMG ?>video_order.png" />
                                            <?php } else if ($history["type"] == 3) { ?>
                                                <img alt="audio order" src="<?php echo BASEURL_OIMG ?>audio_order.png" />

                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="outerordertwo">
                                        <div class="orderinfo">
                                            <ul style="padding-top: 32px; line-height:25px;">
                                                <li><b>Order Id: </b>#<span><?php echo $history['unique_oid']; ?></span></li>
                                                <li><b>Receiver Name: </b><?php echo $history["fname"]; ?></li>
                                                <?php if ($history["femail"] != "") { ?> <li><b>Email: </b><?php echo $history["femail"]; ?></li> <?php } ?>
                                                <?php if ($history["fphone"] != "") { ?> <li><b>Mobile Number: </b><?php echo $history["fphone"]; ?></li> <?php } ?>
                                                <?php if ($history["delivery_date_time"] == "0000-00-00 00:00:00") { ?>
                                                    <li><b>Sent: </b>Immediately</li>
                                                <?php } else { ?>
                                                    <li><b>Sent: </b><?php echo date("d/m/Y", strtotime($history["delivery_date_time"])); ?></li>
                                                <?php } ?>
                                                <li><b>Quantity: </b><?php echo $history["quantity"]; ?></li>

                                            </ul>
                                        </div>


                                        <div class="resendDiv">
                                            <div  class="statustext"><b>Status: </b><span style="width: 75px;padding-right:10px;" class="sval val_<?php echo $i; ?>"><?php echo $statusval; ?></span><div style="vertical-align: middle;display: inline-block"><img alt="birthdayowl" src='<?php echo BASEURL_OIMG . $ind_img; ?>' statusvalue="<?php echo $status; ?>" width="22" height="22" style="cursor: pointer;"class="top-right" data-tipso="<?php echo $history["order_message"] ?>" /></div></div>
                                            <?php if ($status == "4") { ?>
                                                <div class="resend_voucher" id="resend_voucher" order_pro_id="<?php echo $history["order_pro_id"]; ?>" >Resend Voucher</div>
                                            <?php }if ($status == "3") { ?>
                                                <?php
                                                if ($history["delivery_date_time"] == "0000-00-00 00:00:00") {
                                                    $ddate = date("d/m/Y");
                                                } else {
                                                    $ddate = date("d/m/Y", strtotime($history["delivery_date_time"]));
                                                }
                                                ?>


                                                <div class="try_again" ddate="<?php echo $ddate; ?>"  order_pro_id="<?php echo $history["order_pro_id"]; ?>" >Try Again</div>
                                            <?php } ?>
                                        </div>


                                    </div>
                                    <div class="outerorderthree">
                                        <div style="margin-left: 15px; margin-right: 15px; width: 90%;border:1px solid #cdcdcd;border-radius: 5px;padding: 8px; margin-top: 16px">
                                            <img alt="voucher product" src='<?php echo $history["voucher_img"]; ?>' />
                                        </div>
                                        <div style="line-height: 34px;font-family: gotham-book;">
                                            <span class="vlabel"><?php echo $history["voucher_pro_name"]; ?></span><br/>
                                            <span style="font-weight: bold;font-size: 25px;">Rs.<?php echo $history["selected_amount"]; ?></span><br/>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <?php
                            $i++;
                        }
                    } else {
                        ?>
                        <div style="margin: auto;text-align: center;padding:104px;font-size: 20px;">No Order History</div>
                    <?php } ?>

                </div>

                <div style="margin:auto">
                    <?php if (count($ehistory) > 0 || count($history) > 0 || (count($flower_history) > 0)) { ?>
                        <div class="wrapper defaults" id="content" >
                            <div class="holder text-center" style="cursor:pointer;"></div>
                        </div>
                    <?php } ?>  
                </div>
            </div>
        </div>
        <div class="wrapper">


        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script src="<?php echo BASEURL_OJS; ?>jPages.js"></script>
<script type="text/javascript">

    setTimeout(function () {
        startRefresh();
    }, 50000);

    function startRefresh() {
        // alert("DSF");
        $.ajax({
            type: "POST",
            url: '<?php echo BASEURL . "Home_web/getStatusByOrders"; ?>',
            data: "",
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {
                    alert(r.message.toString());
                } else {
                    for (var i = 0; i < r.count.toLocaleString(); i++) {

                        var status = r.history[i]["delivery_status"];
                        var statusval, ind_img;
                        if (status == "4") {
                            statusval = " Successful";
                            ind_img = "successr.png";
                        } else if (status == 0 || status == 3) {
                            statusval = "Failed";
                            ind_img = "fail.png";
                        } else if (status == 2) {
                            statusval = "Pending";
                            ind_img = "info_pending.png";
                        } else if (status == 6) {
                            statusval = "Processing";
                            ind_img = "pending.png";
                        } else if (status == 1) {
                            statusval = "Sending e-Gift Cards..";
                            ind_img = "pending.png";
                        } else if (status == 8) {
                            statusval = "Failed";
                            ind_img = "info_error.png";
                        } else if (status == 10) {
                            statusval = "Refunded";
                            ind_img = "successr.png";
                        }
                        $('.val_' + i).html(statusval);
                    }
                }
            }
        });
    }


    jQuery('.top-right').tipso({
        position: 'top-right',
        background: 'rgba(0,0,0,0.8)',
        titleBackground: 'tomato',
        titleContent: 'Some title',
        useTitle: false,
        tooltipHover: true
    });
    $(".vmessage").click(function () {
        var type = $(this).attr("mtype");
        var iva = $(this).attr("img_vid_audio_upload");
        window.open('<?php BASEURL . "public/imageVideo/" ?>' + iva, '_blank');

    });

    $("#alertclose").click(function () {

        $('.giftalertPopupMessage').animate({top: '0'}, 500);
        $(".alertBox").css("display", "none");
        $(".alertPopupDiv").css("display", "none");
        $(".imgview").hide();
        $(".videoview").hide();
        $(".audioview").hide();
    });

    $(".top-right").hover(function () {
        var statusvalue = $(this).attr("statusvalue");
        if (statusvalue == 4) {
            jQuery(this).tipso('update', 'titleBackground', '#4FA003');
            jQuery(this).tipso('update', 'titleContent', 'Successful!');
            //e.preventDefault();

        }
        if (statusvalue == 8 || statusvalue == 3) {
            jQuery(this).tipso('update', 'titleBackground', '#FA1F36');
            jQuery(this).tipso('update', 'titleContent', 'Failed');
        }

        if (statusvalue == 2) {
            jQuery(this).tipso('update', 'titleBackground', '#FFD63A');
            jQuery(this).tipso('update', 'titleContent', 'Pending');
        }

        if (statusvalue == 6) {
            jQuery(this).tipso('update', 'titleBackground', '#FFD63A');
            jQuery(this).tipso('update', 'titleContent', 'Processing');
        }
        if (statusvalue == 1) {
            jQuery(this).tipso('update', 'titleBackground', '#FFD63A');
            jQuery(this).tipso('update', 'titleContent', 'Sending e-Gift Cards');
        }
        if (statusvalue == 10) {
            jQuery(this).tipso('update', 'titleBackground', '#4FA003');
            jQuery(this).tipso('update', 'titleContent', 'Refunded Successfully!');
            //e.preventDefault();

        }

    });



    $(".resend_voucher").click(function () {
        var data = {
            order_pro_id: $(this).attr("order_pro_id")
        }
        $(".resend_voucher").css("cursor", "none");
        $(this).html("Resending voucher...");
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>resend-voucher",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {
                    $(".resend_voucher").css("cursor", "pointer");

                    $(".rsend").html("Resend Voucher");
                    alert(r.message.toString());
                } else {
                    $(".resend_voucher").css("cursor", "pointer");

                    $(".rsend").html("Resend Voucher");

                    alert(r.message.toString());
                    window.location = "<?php echo WEB_ORDER_HISTORY; ?>";
                }
            }
        })
    });
    $("#yesm").click(function () {
    });


    $("#alertclosep").click(function () {
        $(".rsend").html("Resend Greeting");

        $('.giftalertPopupMessage').animate({top: '0'}, 500);
        $(".alertBox").css("display", "none");
        $(".alertPopupDiv").css("display", "none");

    });
    $(".resend_greeting").click(function () {
        var fid = $(this).attr("fid");
        window.location = '<?php echo BASEURL; ?>editegreeting/' + fid;
//        var data = {
//            fid: $(this).attr("fid")
//        }
//        $(".resend_greeting").css("cursor", "none");
//        $(this).html("Resending greeting...");
//
//        $.ajax({
//            type: "POST",
//            url: "<?php echo BASEURL; ?>Home_web/resend_greeting",
//            data: data,
//            dataType: "json",
//            success: function (r) {
//                if (r.success.toString() == "true") {
//                    $(".rsend").html("Resend Greeting");
//                    $(".resend_greeting").css("cursor", "pointer");
//                    alert(r.message.toString());
//                    window.location = "<?php echo BASEURL . "order-history" ?>";
//                } else {
//                    $(".rsend").html("Resend Greeting");
//                    $(".resend_greeting").css("cursor", "pointer");
//                    if (r.message.toString() == "1") {
//                        $(".alertBox").css("display", "block");
//                        $(".alertPopupDiv").css("display", "block");
//                        $('.giftalertPopupMessage').animate({top: '34%'}, 500);
//                        $(".mess").html("You have used your 10 free greeting cards.");
//                    } else if (r.message.toString() == "2") {
//                        $(".alertBox").css("display", "block");
//                        $(".alertPopupDiv").css("display", "block");
//                        $('.giftalertPopupMessage').animate({top: '34%'}, 500);
//                        $(".mess").html("Your membership plan has been expired.");
//                    } else if (r.message.toString() == "3") {
//                        alert("Sorry your session has been expired.Please login to send e-greeting");
//                        window.location = "<?php echo WEB_HOME; ?>";
//                    } else {
//                        alert(r.message.toString());
//                    }
//
//                }
//            }
//        });
    });


    $(".try_again").click(function () {
        var dateEntered = $(this).attr("ddate");
        var date = dateEntered.substring(0, 2);
        var month = dateEntered.substring(3, 5);
        var year = dateEntered.substring(6, 10);
        var dateToCompare = new Date(year, month, date);
        var currentDate = new Date();
        var today = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        var order_pro_id = $(this).attr("order_pro_id");
        if ((dateToCompare.getTime() === today.getTime()) || (dateToCompare.getTime() > today.getTime())) {
            window.location = "<?php echo WEB_SHOW_CART; ?>";
        } else if (dateToCompare.getTime() < today.getTime()) {
            alert("Delivery date has been past.Please select proper date.");
            window.location = "<?php echo BASEURL . "edit_voucher/" ?>" + order_pro_id;
        }

    });
    $("#know_horo").click(function () {
        var data = {
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>Home_web/check_birthdate",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {
                    alert(r.message.toString());
                } else {
                    window.location = "<?php echo WEB_KNOW_YOUR_HOROSCOPE; ?>";
                }
            }
        });

    });

    $("#egift_orders").click(function () {
        $("#egift_orders").css("background-color", "black");
        $("#greetingorders").css("background-color", "#4fa003");
        $("#flowerorders").css("background-color", "#4fa003");
        $("#flowers").hide();
        $("#greeting_images").hide();
        $("#vouchersection").show();
        $("#greetingmov").hide();

    });
    $("#greetingorders").click(function () {
        $("#greetingorders").css("background-color", "#00000");
        $("#egift_orders").css("background-color", "#4fa003");
        $("#flowerorders").css("background-color", "#4fa003");
        $("#flowers").hide();
        $("#greeting_images").show();
        $("#vouchersection").hide();
        $("#greetingmov").show();
    });

    $("#flowerorders").click(function () {
        $("#flowerorders").css("background-color", "#00000");
        $("#egift_orders").css("background-color", "#4fa003");
        $("#greetingorders").css("background-color", "#4fa003");
        $("#greeting_images").hide();
        $("#vouchersection").hide();
        $("#greetingmov").hide();
        $("#flowers").show();
    });

    /* pagination*/
    $("div.holder").jPages({
        containerID: "greeting_images",
        perPage: 10,
        startPage: 1,
        startRange: 1,
        midRange: 4,
        endRange: 1
    });
    $("div.holder").jPages({
        containerID: "vouchersection",
        perPage: 10,
        startPage: 1,
        startRange: 1,
        midRange: 4,
        endRange: 1
    });
    $("div.holder").jPages({
        containerID: "flowers",
        perPage: 10,
        startPage: 1,
        startRange: 1,
        midRange: 4,
        endRange: 1
    });
//    setTimeout(function () {
//        window.location.reload(1);
//    }, 30000);
    jQuery('.top-right').tipso({
        position: 'top-right',
        background: 'rgba(0,0,0,0.8)',
        titleBackground: 'tomato',
        titleContent: 'Some title',
        useTitle: false,
        tooltipHover: true
    });


    $("#alertclose").click(function () {
        $('.giftalertPopupMessage').animate({top: '0'}, 500);
        $(".alertBox").css("display", "none");
        $(".alertPopupDiv").css("display", "none");
        $(".imgview").hide();
        $(".videoview").hide();
        $(".audioview").hide();
    });

    $(".top-right").hover(function () {
        var statusvalue = $(this).attr("statusvalue");
        if (statusvalue == 1) {
            jQuery(this).tipso('update', 'titleBackground', '#4FA003');
            jQuery(this).tipso('update', 'titleContent', 'Successful!');
        }
        if (statusvalue == 0) {
            jQuery(this).tipso('update', 'titleBackground', '#FA1F36');
            jQuery(this).tipso('update', 'titleContent', 'Failed');
        }
        if (statusvalue == 2) {
            jQuery(this).tipso('update', 'titleBackground', '#FFD63A');
            jQuery(this).tipso('update', 'titleContent', 'Pending');
        }
    });


//    $(document).on("click", "#unsubscribe", function () {
//        $("#resubscribe").show();
//        $("#cmessage").html("You're unsubscribed!");
//        $(".alertPopupDivnew").addClass("show_overlay").show();
//        $('.alertPopupMessagenew').animate({top: '34%'}, 500);
//        var data = {
//            mail_status: 1
//        }
//        $.ajax({
//            type: "POST",
//            url: "<?php // echo BASEURL;    ?>unsubscribe",
//            data: data,
//            dataType: "json",
//            success: function (r) {
//
//            }
//        });
//    });

//    $(document).on("click", "#resubscribe", function () {
//        $(this).hide();
//        $("#cmessage").html("You're subscribed!");
//        var data = {
//            mail_status: 0
//        }
//        $.ajax({
//            type: "POST",
//            url: "<?php // echo BASEURL;    ?>unsubscribe",
//            data: data,
//            dataType: "json",
//            success: function (r) {
//
//            }
//        });
//
//
//    });
//    $("#close_popupnew").click(function () {
//        $('.alertPopupMessagenew').animate({top: '0'}, 500);
//        $(".alertPopupDivnew").removeClass("show_overlay").css("display", "none");
//
//    });
</script>
