<?php $this->load->view("new_templates/header"); ?>
<link rel="stylesheet" href="<?php echo BASEURL_OCSS; ?>calstyle.css">
<link rel="stylesheet" href="<?php echo BASEURL_OCSS; ?>reminder.css">
<script src="<?php echo BASEURL_OJS; ?>simplecalendar.js" type="text/javascript"></script>

<style type="text/css">


    .arrowimg{
        margin-top: 5px;
    }



    .closepop{
        float: right;
        height: 27px;
        margin-top: -4px;
        width: 27px;
    }




    .before_display_clk{
        display:none;
    }





</style>
<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            View <span>Birthday Reminders</span></div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <!--        <div class="alertPopupDiv" >
                    <div class="alertPopupMessage">
                        <div class="closepopup" id="close_popup"> <img src="<?php // echo BASEURL_OIMG;                    ?>closer.png" /> </div>
                        <br/>
                        <br/>
                        <div style="text-align: center; margin: auto auto; width: 100%; height:120px;line-height: 29px;">
                            <div style="font-size: 20px;" id='cmessage'>You're unsubscribed.</div>
                            <div style="text-decoration: underline;cursor: pointer;" id='resubscribe'>Re-subscribed?</div>
                            <br/>
                        </div>
                    </div>
                </div>-->
        <div class="inner_midd dashbord">
            <div class="in_f_l">
               	<div class="dashboard-res-menu">

               	</div>
                <ul class="leftside dashbord_nav">
                    <li><img alt="DASHBOARD" src="<?php echo BASEURL_OIMG ?>dash.png"  class='dash'/><a href="<?php echo WEB_DASHBOARD; ?>">DASHBOARD</a></li>
                    <li><img alt="ADD BIRTHDAY REMINDERS" src="<?php echo BASEURL_OIMG ?>rem.png"  class='dash'/><a href="<?php echo BASEURL . "addReminder/0" ?>">ADD BIRTHDAY REMINDERS</a></li>
                    <li class="active"><img alt="VIEW BIRTHDAY REMINDERS" src="<?php echo BASEURL_OIMG ?>bday.png"  class='dash'/><a href="<?php echo WEB_VIEW_BIRTHDAY_REMINDER; ?>">VIEW BIRTHDAY REMINDERS</a></li>
                    <li><img alt="KNOW YOUR ZODIAC" src="<?php echo BASEURL_OIMG ?>horo.png"  class='dash'/><a href="<?php echo WEB_KNOW_YOUR_HOROSCOPE; ?>" style="cursor:pointer;" id="know_horo">KNOW YOUR ZODIAC</a></li>
                    <li><img alt="ORDER HISTORY" src="<?php echo BASEURL_OIMG ?>order.png"  class='dash'/><a href="<?php echo WEB_ORDER_HISTORY; ?>">ORDER HISTORY</a></li>
                    <li><img alt="GIFT WALLET" src="<?php echo BASEURL_OIMG ?>wallet.png"  class='dash'/><a href="<?php echo WEB_GIFT_WALLET; ?>">GIFT WALLET</a></li>
                    <li><img alt="UPDATE PROFILE" src="<?php echo BASEURL_OIMG ?>update.png"  class='dash'/><a href="<?php echo WEB_UPDATE_PROFILE; ?>">UPDATE PROFILE</a></li>
                    <li><img alt="UNSUBSCRIBE" src="<?php echo BASEURL_OIMG ?>greet_history.png" class='dash'/><a id="unsubscribe" style="cursor: pointer;" >UNSUBSCRIBE</a></li>



                </ul>
            </div>
            <div class="in_f_r view_birthday_reminders ">
                <div class="bday_calender res100_768">
                    <div class="grid_8">
                        <ul class="calender_arrow">
                            <li>
                                <div class="calendar">
                                    <header>
                                        <a  class="btn-prev fontawesome-angle-left"><div class="arrowimg"><img src="<?PHP echo BASEURL_OIMG ?>calender-arrow-left.png" alt="left calendar arrow"  /> </div> </a> 
                                        <a  class="btn-next fontawesome-angle-right"> <div class="arrowimg"> <img src="<?PHP echo BASEURL_OIMG ?>calender-arrow-right.png" alt="right calendar arrow" /></div> </a>
                                    </header>
                                </div>
                            </li>
                            <li><div class="month" ></div></li>
                            <li>
                                <a href="<?php echo BASEURL . "addReminder/0" ?>"> <img src="<?PHP echo BASEURL_OIMG ?>add_reminder_btn.png" /></a>
                            </li>
                        </ul>
                        <div class="calendar-container">
                            <div class="calendar">
                                <table class="cal_table">
                                    <thead class="event-days">
                                        <tr></tr>
                                    </thead>
                                    <tbody class="event-calendar">
                                        <tr class="a1"></tr>
                                        <tr class="a2"></tr>
                                        <tr class="a3"></tr>
                                        <tr class="a4"></tr>
                                        <tr class="a5"></tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="grid_4 cal_img">
                        <img src="<?PHP echo BASEURL_OIMG ?>add_bday_reminder.png" alt="birthday owl" style="padding-top: 80%" />
                    </div>
                </div>
                <div class="cal_list">
                    <?php foreach ($reminder_info as $reminder) { ?>
                        <table class="day-event" date-day="<?php echo $reminder["bdate"] ?>" reminder_id="<?php echo $reminder["reminder_id"]; ?>"  date-month="<?php echo $reminder["bmonth"] ?>" date-year="<?php echo $reminder["byear"] ?>"  data-number="1" style="font-family: cursive;" gender="<?php echo $reminder["gender"]; ?>" zodiac_id="<?php echo $reminder["zodiac_id"]; ?>" bdate="<?php echo $reminder["bdate"]; ?>" bmonth="<?php echo $reminder["bmonth"]; ?>" zimage="<?php echo $reminder["zimage"]; ?>"  fname="<?php echo ucfirst($reminder["first_name"]); ?>" lname="<?php echo $reminder["last_name"]; ?>"  zodiac_id="<?php echo $reminder["zodiac_id"]; ?>" count="<?php echo $count ?>">
                        </table>
                    <?php } ?>
                </div>
                <img style="margin-top:30px;" src="<?php echo BASEURL_OIMG; ?>rem_heading.png" alt="Free birthday reminder" id="rempic" />
                <!--                <div class="heading_3 wrapper">
                                    <span>YOUR REMINDER THIS MONTH</span>
                                </div>-->
                <div class="reminder_details event_list">
                    <?php //$month=date('n');?> <!--//????????????????????????????????????????????????-->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr id="tableheadingdiv">
                                <th scope="col">Name</th>
                                <th scope="col">Date of birth </th>
                                <th scope="col">Gender</th>
                                <th scope="col">Zodiac</th>
                                <th scope="col"> Action</th>
                            </tr>
                            <?php $i = 0; ?>
                            <?php foreach ($reminder_info as $reminder) { ?>

                                <tr  class="before_display_clk current_month<?php echo $reminder["bmonth"]; ?>">
                                    <?php $monthName = date("F", mktime(0, 0, 0, $reminder["bmonth"], 10)); ?>
                                    <?php
                                    $genval;
                                    if ($reminder["gender"] == "1") {
                                        $genval = "Male";
                                    } else {
                                        $genval = "Female";
                                    }
                                    ?>
                                    <td><?php echo ucfirst($reminder["first_name"]) . " " . $reminder["last_name"] ?></td>
                                    <td><?php echo $monthName . " " . $reminder["bdate"]; ?></td>
                                    <td><?php echo $genval; ?></td>
                                    <td><?php echo $reminder["zodiac_sign"]; ?></td>
                                    <td>
                                        <a rem_id="<?php echo $reminder["reminder_id"]; ?>" style="cursor:pointer;" class="delete_reminder"><img src="<?PHP echo BASEURL_OIMG ?>delete-action.png" alt="delete icon"/></a> 
                                        <a rem_id="<?php echo $reminder["reminder_id"]; ?>" style="cursor:pointer;" class="edit_reminder"><img src="<?PHP echo BASEURL_OIMG ?>edit-action.png" alt="edit icon"/></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- Pop up-start -->
            <div>
                <div class="popupbg"></div>
                <div class="my_reminder"> <a  class=" js__p_close" title="Close"  style="cursor:pointer"><img src="<?php echo BASEURL_OIMG; ?>closen.png" alt="close button" class="closepop"></a>
                    <div class="tmessage" id="title_message"> </div>
                    <div style="padding:20px;margin-top: 39px;">
                        <div class="giftdiv HoroDiv"  horolink="" style="cursor:pointer">
                            <div id="horoscope" class="popup_horo" >
                            </div>
                            <div id="horo_text" style=" font-size: 12px;float: left;  font-family: cursive; text-align: center;padding: 5px;">
                            </div>
                        </div>
                        <div class="ordiv">OR</div>
                        <div class="giftdiv voucherDiv" voucher_link=""style="cursor:pointer" >
                            <div id="products" class="popup_product" >   </div>
                            <div id="pro_text" style="font-size: 12px;  font-family: cursive; text-align: center; float: left;padding: 5px;"> </div>
                        </div>
                        <div class="ordiv">OR</div>

                        <div class="giftdiv greetingDiv" geeting_link="" style="cursor:pointer">
                            <div id="freegreeting" class="popup_greeting" > </div>
                            <div id="greeting_text" style="font-size: 12px;  font-family: cursive; text-align: center; float: left;padding: 5px;"></div>

                        </div>
                    </div>
                </div> 

            </div>
            <!-- Popup -end -->


        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript">
    $("#know_horo").click(function () {
        var data = {
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>Home_web/check_birthdate",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {
                    alert(r.message.toString());
                } else {
                    window.location = "<?php echo WEB_KNOW_YOUR_HOROSCOPE; ?>";
                }
            }
        });

    });



    var d = new Date();
    var monthNumber = d.getMonth() + 1;
    $('.current_month' + monthNumber).show();
    $(".delete_reminder").click(function () {
        var reminder_id = $(this).attr("rem_id");
        var ask = window.confirm("Are you sure you want to delete?");
        if (ask) {
            var data = {
                reminder_id: reminder_id
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>delete-reminder",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        return false;
                    } else {
                        alert(r.message.toString());
                        window.location = "<?php echo WEB_VIEW_BIRTHDAY_REMINDER; ?>";
                    }
                }
            });

        } else {
            window.location = "<?php echo WEB_VIEW_BIRTHDAY_REMINDER; ?>";
        }
    });
    $(".edit_reminder").click(function () {
        var reminder_id = $(this).attr("rem_id");
        window.location = "<?php echo BASEURL; ?>addReminder/" + reminder_id;
    });


    $(document).on("click", ".js__p_start", function () {
        $(".active").css("z-index", "0");
        $(".dash").css("z-index", "0");
        $('.popupbg').show();
        $('.my_reminder').css("display", "block");
        $('.my_reminder').animate({top: '20%'}, 1000);
        $('html, body').animate({
            scrollTop: $(".my_reminder").offset().top
        }, 2000);
        var zodiac_id = $(this).attr("zodiac_id");
        var bdate = $(this).attr("bdate");
        var bmonth = $(this).attr("bmonth");
        var fname = $(this).attr("fname");
        var zimage = $(this).attr("zimage");
        var gender = $(this).attr("gender");
        var reminder_id = $(this).attr("reminder_id");
        var gen = "";
        if (gender == 1) {
            gen = "his";
        } else {
            gen = "her";
        }
        var img_path = "<?php echo BASEURL_IMG . "zodiac/"; ?>" + zimage;
        $("#title_message").html("It’s that time of the year again for " + fname + ".We’ve got some great products to choose from to make " + gen + " birthday even special");
        $("#horoscope").css("background", 'white url("' + img_path + '") no-repeat scroll right center / 100% auto');
        $("#horo_text").html("Read " + gen + " zodiac to find out what’s the Perfect Gift for " + fname);
        $("#pro_text").html("Surprise " + fname + " with the perfect gift this birthday");
        $(".HoroDiv").attr("horolink", "know_horoscope_reminder/" + zodiac_id + "/" + bdate + "/" + bmonth);
        $("#greeting_text").html("Send " + fname + " amazing Greeting cards!");
        var data = {
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>random-product-new",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() == "true") {
                    var link = "<?php echo BASEURL . "flowers_detail/"; ?>" + r.product["pro_id"] + "/0";
                    var image;
                    if (r.product["product_image"] != "") {
                        image = "<?php echo BASEURL_PRODUCT_IMG; ?>" + r.product["product_image"];
                    } else {
                        image = "<?php echo BASEURL_OIMG . "unnamed.png"; ?>";
                    }
                    $("#products").css("background", 'white url("' + image + '") no-repeat scroll right center / 100% auto');
                    $(".voucherDiv").attr("voucher_link", "" + link);

                    var greeting_data = '';
                    var greeting_img = "<?php echo BASEURL_CROPPED_GREET; ?>" + r.gcards["front_page_image"];
                    $("#freegreeting").css("background", 'white url("' + greeting_img + '") no-repeat scroll right center / 100% auto');

                    var glink = "<?php echo BASEURL . "insidegreeting/"; ?>" + r.gcards["card_id"] + "/" + reminder_id + "/0";
                    greeting_data += '<a href="' + glink + '"><div style="margin:15px auto auto; "><img src="' + greeting_img + '" class="greetinppopimg" ></div></a>';
                    $(".greetingDiv").attr("geeting_link", "" + glink);


                    // $("#freegreeting").html(greeting_data);
                }
            }
        });







    });



//    $(function () {
//        $(document).on("click", ".js__p_start", function () {
//            var zodiac_id = $(this).attr("zodiac_id");
//            var bdate = $(this).attr("bdate");
//            var bmonth = $(this).attr("bmonth");
//            var fname = $(this).html().toUpperCase();
//            var reminder_id = $(this).attr("reminder_id");
//
//            var zimage = $(this).attr("zimage");
//            var gender = $(this).attr("gender");
//            var gen = "";
//            if (gender == 1) {
//                gen = "his";
//            } else {
//                gen = "her";
//            }
//            var img_path = "<?php echo BASEURL_IMG . "zodiac/"; ?>" + zimage;
//            $("#title_message").html("It’s that time of the year again for " + fname + ".We’ve got some great products to choose from to make " + gen + " birthday even special");
//            $("#horoscope").css("background", 'white url("' + img_path + '") no-repeat scroll right center / 100% auto');
//            $("#horo_text").html("Read " + gen + " zodiac to find out what’s the Perfect Gift for " + fname);
//            $("#pro_text").html("Surprise " + fname + " with the perfect gift this birthday");
//            $(".HoroDiv").attr("horolink", "know_horoscope_reminder/" + zodiac_id + "/" + bdate + "/" + bmonth);
//
//            $("#greeting_text").html("Send " + fname + " amazing Greeting cards!");
//            var data = {
//            }
//            $.ajax({
//                type: "POST",
//                url: "<?php echo BASEURL; ?>random-product",
//                data: data,
//                dataType: "json",
//                success: function (r) {
//
//                    if (r.success.toString() == "true") {
//                        if (r.success.toString() == "true") {
//                            var link = "<?php echo BASEURL . "voucher/"; ?>" + r.product["voucher_url"];
//                            var image;
//                            if (r.product["product_image"] != "") {
//                                image = r.product["product_image"];
//                            } else {
//                                image = "<?php echo BASEURL_OIMG . "unnamed.png"; ?>";
//                            }
//                            $("#products").css("background", 'white url("' + image + '") no-repeat scroll right center / 100% auto');
//                            $(".voucherDiv").attr("voucher_link", "" + link);
//
//
//                            var greeting_img = "<?php echo BASEURL_CROPPED_GREET; ?>" + r.gcards["front_page_image"];
//                            $("#freegreeting").css("background", 'white url("' + greeting_img + '") no-repeat scroll right center / 100% auto');
//
//                            var glink = "<?php echo BASEURL . "insidegreeting/"; ?>" + r.gcards["card_id"] + "/" + reminder_id;
//                            $(".greetingDiv").attr("geeting_link", "" + glink);
//
//
//
//                            // $("#freegreeting").html(greeting_data);
//                        }
//                    }
//                }
//            });
//            $(".active").css("z-index", "0");
//            $(".dash").css("z-index", "0");
//            $(this).simplePopup();
//            $('html, body').animate({
//                scrollTop: $(".my_reminder").offset().top
//            }, 2000);
//        });
//
//
//
//
//
//
//    });

    $(".js__p_close").click(function () {
        $(".active").css("z-index", "1");
        $(".dash").css("z-index", "1");
        $('.my_reminder').css("display", "none");
        $('.my_reminder').animate({top: '0'}, 1000);
        $('.popupbg').hide();

    });
    $(document).ready(function () {
        if ($('tbody.event-calendar tr td').is(".event")) {
            $("#rempic").show();
            $("#tableheadingdiv").show();

        } else {
            $("#rempic").hide();
            $("#tableheadingdiv").hide();

        }

    });

    $(".HoroDiv").click(function () {
        window.location = "<?php echo BASEURL . "Home_web/" ?>" + $(this).attr("horolink");

    });

    $(".voucherDiv").click(function () {
        window.location = "" + $(this).attr("voucher_link");

    });

    $(".greetingDiv").click(function () {
        window.location = "" + $(this).attr("geeting_link");

    });
//    $(document).on("click", "#unsubscribe", function () {
//        $("#resubscribe").show();
//        $("#cmessage").html("You're unsubscribed!");
//        $(".alertPopupDiv").addClass("show_overlay").show();
//        $('.alertPopupMessage').animate({top: '34%'}, 500);
//        var data = {
//            mail_status: 1
//        }
//        $.ajax({
//            type: "POST",
//            url: "<?php // echo BASEURL;                    ?>unsubscribe",
//            data: data,
//            dataType: "json",
//            success: function (r) {
//
//            }
//        });
//    });

//    $(document).on("click", "#resubscribe", function () {
//        $(this).hide();
//        $("#cmessage").html("You're subscribed!");
//        var data = {
//            mail_status: 0
//        }
//        $.ajax({
//            type: "POST",
//            url: "<?php // echo BASEURL;                    ?>unsubscribe",
//            data: data,
//            dataType: "json",
//            success: function (r) {
//
//            }
//        });
//
//
//    });
//    $("#close_popup").click(function () {
//        $('.alertPopupMessage').animate({top: '0'}, 500);
//        $(".alertPopupDiv").removeClass("show_overlay").css("display", "none");
//    });
</script>
