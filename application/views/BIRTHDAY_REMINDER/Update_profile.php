<?php $this->load->view("new_templates/header"); ?>

<style>
    .pass{
        width:63%;
    }
</style>
<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">

        <div class="heading_2">
            Update <span>Profile</span>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <!--        <div class="alertPopupDiv" >
                    <div class="alertPopupMessage">
                        <div class="closepopup" id="close_popup"> <img src="<?php // echo BASEURL_OIMG;                        ?>closer.png" /> </div>
                        <br/>
                        <br/>
                        <div style="text-align: center; margin: auto auto; width: 100%; height:120px;line-height: 29px;">
                            <div style="font-size: 20px;" id='cmessage'>You're unsubscribed.</div>
                            <div style="text-decoration: underline;cursor: pointer;" id='resubscribe'>Re-subscribed?</div>
                            <br/>
                        </div>
                    </div>
                </div>-->
        <div class="inner_midd dashbord">
            <div class="in_f_l">
               	<div class="dashboard-res-menu">

               	</div>
                <ul class="leftside dashbord_nav">
                    <li><img alt="DASHBOARD" src="<?php echo BASEURL_OIMG ?>dash.png"  class='dash'/><a href="<?php echo WEB_DASHBOARD; ?>">DASHBOARD</a></li>
                    <li ><img alt="ADD BIRTHDAY REMINDERS" src="<?php echo BASEURL_OIMG ?>rem.png"  class='dash'/><a href="<?php echo BASEURL . "addReminder/0" ?>">ADD BIRTHDAY REMINDERS</a></li>
                    <li><img alt="VIEW BIRTHDAY REMINDERS" src="<?php echo BASEURL_OIMG ?>bday.png"  class='dash'/><a href="<?php echo WEB_VIEW_BIRTHDAY_REMINDER; ?>">VIEW BIRTHDAY REMINDERS</a></li>
                    <li><img alt="KNOW YOUR ZODIAC" src="<?php echo BASEURL_OIMG ?>horo.png"  class='dash'/><a href="<?php echo WEB_KNOW_YOUR_HOROSCOPE; ?>" style="cursor:pointer;" id="know_horo">KNOW YOUR ZODIAC</a></li>
                    <li ><img alt="ORDER HISTORY" src="<?php echo BASEURL_OIMG ?>order.png"  class='dash'/><a href="<?php echo WEB_ORDER_HISTORY; ?>">ORDER HISTORY</a></li>
                    <li><img alt="GIFT WALLET" src="<?php echo BASEURL_OIMG ?>wallet.png"  class='dash'/><a href="<?php echo WEB_GIFT_WALLET; ?>">GIFT WALLET</a></li>
                    <li class="active"><img alt="UPDATE PROFILE" src="<?php echo BASEURL_OIMG ?>update.png"  class='dash'/><a href="<?php echo WEB_UPDATE_PROFILE; ?>">UPDATE PROFILE</a></li>
                    <li><img alt="UNSUBSCRIBE" src="<?php echo BASEURL_OIMG ?>greet_history.png" class='dash'/><a id="unsubscribe" style="cursor: pointer;" >UNSUBSCRIBE</a></li>
                </ul>
            </div>
            <div class="in_f_r add_bday_reminder1">
                <!--<form  method="post" id="update_form" action="<?php //echo BASEURL;                                                                                    ?>Home_web/update_profile_success1" enctype="multipart/form-data">-->
                <ul class="add_bday_input wrapper">
                    <li>Gender</li>
                    <li class="gen">
                        <label>
                            <input type="radio" id="gender_0" value="1" name="gender"  >
                            Male</label>
                        <label>
                            <input type="radio" id="gender_1" value="2" name="gender" checked="true">
                            Female</label>
                    </li>
                    <li>Your Name</li>
                    <li data-icon="&#xf10b;">
                        <input type="text" value=" <?php echo trim($userprofile["first_name"]); ?> " name="first_name" id="ufirstname">
                    </li>
                    <li>Email address</li>
                    <li>
                        <input type="email" value="<?php echo $userprofile["email"]; ?>" name="email" id="uemail" >
                    </li>
                    <li>Password</li>
                    <li>
                        <input type="password" value="<?php echo $password; ?>" class="pass" name="password" id="upassword" >
                    </li>
                    <li>Confirm Password</li>
                    <li>
                        <input type="password"  value="<?php echo $password; ?>" class="pass" name="con_password" id="cpassword" >
                    </li>
                    <li>Date of Birth</li>
                    <li>
                        <select id="dobday1" class="ubdate" name="ubdate" style="float: left; margin-right: 10px; width: 20%;"></select>
                        <select id="dobmonth1" class="umonth" name="umonth" style="height: 41px; margin-right: 11px; width: 20%;"></select>
                        <select id="dobyear1" class="uyear" name="uyear" style="width: 19%;"></select>

                        <input type="hidden" id="edit_rem_date" />
                        <input type="hidden" id="edit_rem_month" />
                        <input type="hidden" id="edit_rem_year" />
                    </li>
                    <li>Mobile Number</li>
                    <li>
                        <div  style="width: 66%;display: flex;">
                            <select id="CountryOption" name="nationality_id" style="width:40%">
                                <?php $countryList = get_countryinfo(); ?>
                                <?php foreach ($countryList as $country) { ?>
                                    <option value="<?php echo $country["country_id"]; ?>"><?php echo $country['country_name'] . " (+" . $country["nationality_code"] . ")"; ?></option>
                                <?php } ?>
                            </select>
                            <input type="text" placeholder="Mobile Number *"  value="<?php echo $userprofile["mobile_no"]; ?>" class="mbno" name="mobile_no" id="mb_no" onkeypress="return isNumberKey(event)"  />
                        </div>
                    </li>
                    <li class="Priority">&nbsp;</li>
                    <li class="add_bday_last">
                        <input type="submit" value="Submit" class="" id="update_profile">
                        <input type="reset" value="Cancel" class="" id="cancel_update">
                    </li>
                </ul>
                <!--                </form>-->
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript">
    $("#CountryOption").val("<?php echo $userprofile["country_id"]; ?>");
    var country_id_value = $('#CountryOption').val();
    if (country_id_value != 102) {
        $('#mb_no').attr('readonly', '');
    } else {
        $('#mb_no').removeAttr('readonly');
    }

    $(document).on('change', '#CountryOption', function () {
        $('#mb_no').val('');
        var curr_val = $(this).val();
        if (curr_val != 102) {
            $('#mb_no').attr('readonly', '');
        } else {
            $('#mb_no').removeAttr('readonly');
        }
    });

//    $(document).on("click", "#unsubscribe", function () {
//        $("#resubscribe").show();
//        $("#resubscribe").show();
//        $("#cmessage").html("You're unsubscribed!");
//        $(".alertPopupDiv").addClass("show_overlay").show();
//        $('.alertPopupMessage').animate({top: '34%'}, 500);
//        var data = {
//            mail_status: 1
//        }
//        $.ajax({
//            type: "POST",
//            url: "<?php echo BASEURL; ?>unsubscribe",
//            data: data,
//            dataType: "json",
//            success: function (r) {
//
//            }
//        });
//    });
//    $("#close_popup").click(function () {
//        $('.alertPopupMessage').animate({top: '0'}, 500);
//        $(".alertPopupDiv").removeClass("show_overlay").css("display", "none");
//
//    });
//    $(document).on("click", "#resubscribe", function () {
//        $(this).hide();
//        $("#cmessage").html("You're subscribed!");
//        var data = {
//            mail_status: 0
//        }
//        $.ajax({
//            type: "POST",
//            url: "<?php echo BASEURL; ?>unsubscribe",
//            data: data,
//            dataType: "json",
//            success: function (r) {
//
//            }
//        });
//
//
//    });


    $('#dob_note').css('display', 'block');
    var edit_rem_date = "<?php echo $userprofile["bdate"]; ?>";
    var edit_rem_month = "<?php echo $userprofile["bmonth"]; ?>";
    var edit_rem_year = "<?php echo $userprofile["byear"]; ?>";

    edit_rem_date = Number(edit_rem_date).toString();

    $('#edit_rem_date').val(edit_rem_date);
    $('#edit_rem_month').val(edit_rem_month);
    $('#edit_rem_year').val(edit_rem_year);

    birth_date = $('#edit_rem_date').val();
    birth_month = $('#edit_rem_month').val();
    birth_year = $('#edit_rem_year').val();

    $('.ubdate').val(birth_date);
    $('.umonth').val(birth_month);
    $('.uyear').val(birth_year);

    $('.ubdate').change(function () {
        birth_date = $('.ubdate').val();
    });
    $('.umonth').change(function () {
        birth_month = $('.umonth').val();
    });
    $('.uyear').change(function () {
        birth_year = $('.uyear').val();
    });

    var birth_month_value = "";
    switch (birth_month) {
        case "1":
        case "01":
            birth_month_value = "January";
            break;
        case "2":
        case "02":
            birth_month_value = "February";
            break;
        case "3":
        case "03":
            birth_month_value = "March";
            break;
        case "4":
        case "04":
            birth_month_value = "April";
            break;
        case "5":
        case "05":
            birth_month_value = "May";
            break;
        case "6":
        case "06":
            birth_month_value = "June";
            break;
        case "7":
        case "07":
            birth_month_value = "July";
            break;
        case "8":
        case "08":
            birth_month_value = "August";
            break;
        case "9":
        case "09":
            birth_month_value = "September";
            break;
        case "10":
            birth_month_value = "October";
            break;
        case "11":
            birth_month_value = "November";
            break;
        case "12":
            birth_month_value = "December";
            break;
        default:
            birth_month_value = "Month";
    }

    var birth_year_value = "";
    if (birth_year == "0") {
        birth_year_value = "Year";
    } else {
        birth_year_value = birth_year;
    }
    $(document).ready(function () {
        $.dobPicker({
            daySelector: '#dobday1', /* Required */
            monthSelector: '#dobmonth1', /* Required */
            yearSelector: '#dobyear1', /* Required */
            dayDefault: birth_date, /* Optional */
            monthDefault: birth_month_value, /* Optional */
            yearDefault: birth_year_value, /* Optional */
            minimumAge: 8, /* Optional */
            maximumAge: 60 /* Optional */
        });
    });









//    $("#umonth").val("<?php // echo $userprofile["bmonth"];                                                 ?>");

//    var birth_year = "<?php // echo $userprofile["byear"];                                               ?>";
//    if (birth_year == "0") {
//        $("#uyear").val("");
//    } else {
//        $("#uyear").val(birth_year);
//    }
    $(document).ready(function () {
        var gen_val = "<?php echo $userprofile["gender"]; ?>";
        if (gen_val == 1) {
            $('input[name=gender][value=' + gen_val + ']').prop('checked', true)
        } else {
            $('input[name=gender][value=' + gen_val + ']').prop('checked', true)
        }
    });


    $("#know_horo").click(function () {

        var data = {
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>Home_web/check_birthdate",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {
                    alert(r.message.toString());
                } else {
                    window.location = "<?php echo WEB_KNOW_YOUR_HOROSCOPE; ?>";
                }
            }
        });

    });
    $(document).ready(function () {
        $("#update_profile").click(function () {
            var name = $("#ufirstname").val();
            var upassword = $("#upassword").val();
            var conf_password = $("#cpassword").val();
            var mb_no = $("#mb_no").val();
            var uemail = $("#uemail").val();
            var ubdate = $(".ubdate").val();
            var umonth = $(".umonth").val();
            var uyear = $(".uyear").val();
            var country_id = $("#CountryOption").val();
//            var birth_date = $("#popupDatepicker").val();
            var selected_gender = '';
            $('input[name="gender"]:checked').each(function () {
                selected_gender = (this.value);
            });
            if (country_id == 102) {
                if (mb_no == "") {
                    alert("Enter Mobile Number");
                    return false;
                } else if ((!(checkMobileValid(mb_no))) && (mb_no != '')) {
                    alert("Enter valid Indian Mobile No");
                    return false;
                }

            }
            if (name == "") {
                alert("Enter Name");
            } else if (uemail == '') {
                alert("Enter Email");
            } else if (!(validateEmail(uemail))) {
                alert("Not a valid Email address");
            } else if (upassword == "") {
                alert("Enter Password");
            } else if (conf_password == "") {
                alert("Confirm Password");
            } else if (conf_password != upassword) {
                alert("Passwords mismatch");
            }
//            else if (ubdate == '') {
//                alert("Enter birth date");
//                $('html, body').animate({
//                    scrollTop: $("#ubdate").offset().top
//                }, 2000);
//            } else if (ubdate != '' && (ubdate < 1 || ubdate > 31)) {
//                alert("Enter valid birth date");
//                $('html, body').animate({
//                    scrollTop: $("#ubdate").offset().top
//                }, 2000);
//            } else if (umonth == '0') {
//                alert("Select birth month");
//                $('html, body').animate({
//                    scrollTop: $("#umonth").offset().top
//                }, 2000);
//            } else if (uyear != "" && uyear.length < 4) {
//                alert("Birth year should contain 4 digits");
//                $('html, body').animate({
//                    scrollTop: $("#uyear").offset().top
//                }, 2000);
//            }
            else {
                first_name_profile = name;
                email_profile = uemail;
                password_profile = upassword;
                mobile_no_profile = mb_no;
                bdate_profile = birth_date;
                bmonth_profile = birth_month;
                byear_profile = birth_year;
                gender_profile = selected_gender;
                country_id_profile = $("#CountryOption").val();

                var data = {
                    first_name: name,
                    email: uemail,
                    password: upassword,
                    mobile_no: mb_no,
                    bdate: birth_date,
                    bmonth: birth_month,
                    byear: birth_year,
                    gender: selected_gender,
                    country_id: $("#CountryOption").val()


                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>update_profile",
                    data: data,
                    dataType: "json",
                    success: function (r) {
                        if (r.success.toString() === "false") {
                            alert(r.message.toString());
                            return false;
                        } else {
//                            inserted_user_id = r.userdata['user_id'];
                            is_profile_edit = 1;
                            if (r.is_mobile_changed.toString() == '1') {
                                $('#forgotpopup').click();
                                $("#otpresetDiv").hide();

                                $.colorbox({
                                    width: "400px",
                                    height: "320px",
                                    inline: true,
                                    href: $("#verifyMobileDiv").show(),
                                    overlayClose: false
                                });
                                resend_mobile_number = mb_no;
                            } else {
                                alert(r.message.toString());
                                window.location = "<?php echo WEB_DASHBOARD; ?>";
                            }


                        }
                    }
                });
                // $("#update_form").submit();
                // alert("Profile Updated Successfully");
            }
        });
    });

    $("#cancel_update").click(function () {
        window.location = "<?php echo WEB_DASHBOARD; ?>";
    });

    function validateEmail(sEmail) {
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if (filter.test(sEmail)) {
            return true;
        } else {
            return false;
        }
    }
</script>
