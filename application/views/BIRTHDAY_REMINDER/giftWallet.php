<?php $this->load->view("new_templates/header"); ?>
<link rel="stylesheet" href="<?php echo BASEURL_OCSS ?>animate.css">
<script src="<?php echo BASEURL_OJS ?>tipso.min.js"></script>
<link rel="stylesheet" href="<?php echo BASEURL_OCSS ?>reminder.css">

<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            Gift <span>Wallet</span>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <!--        <div class="alertPopupDivnew" >
                    <div class="alertPopupMessagenew">
                        <div class="closepopup" id="close_popupnew"> <img src="<?php // echo BASEURL_OIMG;     ?>closer.png" /> </div>
                        <br/>
                        <br/>
                        <div style="text-align: center; margin: auto auto; width: 100%; height:120px;line-height: 29px;">
                            <div style="font-size: 20px;" id='cmessage'>You're unsubscribed.</div>
                            <div style="text-decoration: underline;cursor: pointer;" id='resubscribe'>Re-subscribed?</div>
                            <br/>
                        </div>
                    </div>
                </div>-->
        <div class="inner_midd dashbord">
            <div class="in_f_l">
               	<div class="dashboard-res-menu">

               	</div>
                <ul class="leftside dashbord_nav">
                    <li><img alt="DASHBOARD" src="<?php echo BASEURL_OIMG ?>dash.png"  class='dash'/><a href="<?php echo WEB_DASHBOARD; ?>">DASHBOARD</a></li>
                    <li ><img alt="ADD BIRTHDAY REMINDERS" src="<?php echo BASEURL_OIMG ?>rem.png"  class='dash'/><a href="<?php echo BASEURL . "addReminder/0" ?>">ADD BIRTHDAY REMINDERS</a></li>
                    <li><img alt="VIEW BIRTHDAY REMINDERS" src="<?php echo BASEURL_OIMG ?>bday.png"  class='dash'/><a href="<?php echo WEB_VIEW_BIRTHDAY_REMINDER; ?>">VIEW BIRTHDAY REMINDERS</a></li>
                    <li><img alt="KNOW YOUR ZODIAC" src="<?php echo BASEURL_OIMG ?>horo.png"  class='dash'/><a href="<?php echo WEB_KNOW_YOUR_HOROSCOPE; ?>" style="cursor:pointer;" id="know_horo">KNOW YOUR ZODIAC</a></li>
                    <li><img alt="ORDER HISTORY" src="<?php echo BASEURL_OIMG ?>order.png"  class='dash'/><a href="<?php echo WEB_ORDER_HISTORY; ?>">ORDER HISTORY</a></li>
                    <li class="active"><img alt="GIFT WALLET" src="<?php echo BASEURL_OIMG ?>wallet.png"  class='dash'/><a href="<?php echo WEB_GIFT_WALLET; ?>">GIFT WALLET</a></li>
                    <li><img alt="UPDATE PROFILE" src="<?php echo BASEURL_OIMG ?>update.png"  class='dash'/><a href="<?php echo WEB_UPDATE_PROFILE; ?>">UPDATE PROFILE</a></li>
                    <li><img alt="UNSUBSCRIBE" src="<?php echo BASEURL_OIMG ?>greet_history.png" class='dash'/><a id="unsubscribe" style="cursor: pointer;" >UNSUBSCRIBE</a></li>

                </ul>

            </div>

            <div class="alertBox"></div>
            <div class="in_f_r" style="text-align: center;">

                <ul class="tabs  primary-nav">
<!--                    <li class="tabs__item" id="egift_orders" style="cursor:pointer;">
                        <a  class="tabs__link">e-Gift Cards</a>
                    </li>-->
                    <li class="tabs__item" id="greetingorders" style="cursor:pointer;">
                        <a  class="tabs__link">Free Greeting Cards</a>
                    </li>

                </ul>
                <div style="border:none;display:none"  id="greeting_images">

                    <?php if (count($history) > 0) { ?>

                        <?php foreach ($history as $history) { ?>
                            <div  class="o_history">
                                <div class="outer_order_history">
                                    <div class="outerorderone">
                                        <div>
                                            <img src="<?php echo BASEURL_CROPPED_GREET . $history["greeting_img"]; ?>" class="selected_greeting" alt="greeting card" />
                                        </div>
                                        <div class="vmessage" style="cursor:pointer;" img_vid_audio_upload="<?php echo BASEURL_BVMG . $history['uploded_img']; ?>" mtype="<?php echo $history['type'] ?>" >
                                            <?php if ($history["type"] == 1) { ?>
                                                <img alt="order image" src="<?php echo BASEURL_OIMG ?>image_order.png" />
                                            <?php } else if ($history["type"] == 2) { ?>
                                                <img alt="video message for you" src="<?php echo BASEURL_OIMG ?>video_order.png" />
                                            <?php } else if ($history["type"] == 3) { ?>
                                                <img alt="audio message for you" src="<?php echo BASEURL_OIMG ?>audio_order.png" />

                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="outerordertwogreeting">
                                        <div class="orderinfo">
                                            <ul style="padding-top: 32px; line-height:25px;font-family:gotham-book;">
                                                <li><b>Order Id: </b>#<span><?php echo $history['fid']; ?></span></li>
                                                <li><b>Sender Name: </b><?php echo $history["fname"]; ?></li>
                                                <?php if ($history["femail"] != "") { ?>
                                                    <li><b>Email: </b><?php echo $history["femail"]; ?></li>
                                                <?php } ?>
                                                <?php if ($history["fphone"] != "") { ?>
                                                    <li><b>Mobile Number: </b><?php echo $history["fphone"]; ?></li>
                                                <?php } ?>
                                                <li><b>Sent: </b><?php echo date("d/m/Y", strtotime($history["delivery_date"])); ?></li>

                                            </ul>
                                        </div>
                                        <!--                                        <div class="resendDiv">
                                                                                    <div  class="thankyougreeting" fid="<?php echo $history["fid"]; ?>" style="width: 50%;padding-top: 19px;cursor: pointer"><img src="<?php echo BASEURL_OIMG . "thankyoucard.png"; ?>" /></div>
                                                                                </div>-->
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        ?>
                        <div style="margin: auto;text-align: center;padding:104px;font-size: 20px;">No Order History</div>
                    <?php } ?>

                </div>


                <div  style="border:none;"  id="vouchersection">

                    <?php if (count($ehistory) > 0) { ?>

                        <?php foreach ($ehistory as $history) { ?>
                            <div  class="o_history">
                                <div class="outer_order_history">
                                    <div class="outerorderone">
                                        <div>
                                            <img alt="Free Greeting Cards" src="<?php echo BASEURL_CROPPED_GREET . $history["front_page_image"]; ?>" class="selected_greeting" />
                                        </div>

                                        <div class="vmessage" style="cursor:pointer;" img_vid_audio_upload="<?php echo BASEURL_BVMG . $history['uploded_img_video']; ?>" mtype="<?php echo $history['type'] ?>" >
                                            <?php if ($history["type"] == 1) { ?>
                                                <img alt="image greeting for you" src="<?php echo BASEURL_OIMG ?>image_order.png" />
                                            <?php } else if ($history["type"] == 2) { ?>
                                                <img alt="video message for you" src="<?php echo BASEURL_OIMG ?>video_order.png" />
                                            <?php } else if ($history["type"] == 3) { ?>
                                                <img alt="audio message for you" src="<?php echo BASEURL_OIMG ?>audio_order.png" />

                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="outerordertwo">
                                        <div class="orderinfo">
                                            <ul style="padding-top: 32px; line-height:25px;">
                                                <li><b>Order Id: </b>#<span><?php echo $history['unique_oid']; ?></span></li>
                                                <li><b>Receiver Name: </b><?php echo $history["fname"]; ?></li>
                                                <?php if ($history["femail"] != "") { ?> <li><b>Email: </b><?php echo $history["femail"]; ?></li> <?php } ?>
                                                <?php if ($history["fphone"] != "") { ?> <li><b>Mobile Number: </b><?php echo $history["fphone"]; ?></li> <?php } ?>
                                                <li><b>Quantity: </b><?php echo $history["quantity"]; ?></li>

                                            </ul>
                                        </div>

                                    </div>
                                    <div class="outerorderthree">
                                        <div style="margin-left: 15px; margin-right: 15px; width: 90%;border:1px solid #cdcdcd;border-radius: 5px;padding: 8px; margin-top: 16px">
                                            <img alt="voucher card" src='<?php echo $history["voucher_img"]; ?>' />
                                        </div>
                                        <div style="line-height: 34px;font-family: gotham-book;">
                                            <span class="vlabel"><?php echo $history["voucher_pro_name"]; ?></span><br/>
                                            <span style="font-weight: bold;font-size: 25px;">Rs.<?php echo $history["selected_amount"]; ?></span><br/>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <?php
                        }
                    } else {
                        ?>
                        <div style="margin: auto;text-align: center;padding:104px;font-size: 20px;">All e-Gift Cards you receive are saved here for your easy reference</div>
                    <?php } ?>

                </div>

                <div style="margin:auto">
                    <?php if (count($ehistory) > 0 || count($history) > 0) { ?>
                        <div class="wrapper defaults" id="content" >
                            <div class="holder text-center" style="cursor:pointer;"></div>
                        </div>
                    <?php } ?>  
                </div>
            </div>
        </div>
        <div class="wrapper">


        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script src="<?php echo BASEURL_OJS; ?>jPages.js"></script>
<script type="text/javascript">

    $(".vmessage").click(function () {
        var type = $(this).attr("mtype");
        var iva = $(this).attr("img_vid_audio_upload");
        window.open('<?php BASEURL . "public/imageVideo/" ?>' + iva, '_blank');

    });


    $(".thankyougreeting").click(function () {
        window.location = "<?php echo BASEURL . "checkThankyou/" ?>" + $(this).attr("fid");
    });

    $("#egift_orders").click(function () {
        $("#egift_orders").css("background-color", "black");
        $("#greetingorders").css("background-color", "#4fa003");
        $("#greeting_images").hide();
        $("#vouchersection").show();
        $("#greetingmov").hide();

    });
    $("#greetingorders").click(function () {
        $("#greetingorders").css("background-color", "#00000");
        $("#egift_orders").css("background-color", "#4fa003");

        $("#greeting_images").show();
        $("#vouchersection").hide();
        $("#greetingmov").show();


    });
    $("#know_horo").click(function () {
        var data = {
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>Home_web/check_birthdate",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {
                    alert(r.message.toString());
                } else {
                    window.location = "<?php echo WEB_KNOW_YOUR_HOROSCOPE; ?>";
                }
            }
        });

    });
    /* pagination*/
    $("div.holder").jPages({
        containerID: "greeting_images",
        perPage: 10,
        startPage: 1,
        startRange: 1,
        midRange: 4,
        endRange: 1
    });
    $("div.holder").jPages({
        containerID: "vouchersection",
        perPage: 10,
        startPage: 1,
        startRange: 1,
        midRange: 4,
        endRange: 1
    });


//    $(document).on("click", "#unsubscribe", function () {
//        $("#resubscribe").show();
//        $("#cmessage").html("You're unsubscribed!");
//        $(".alertPopupDivnew").addClass("show_overlay").show();
//        $('.alertPopupMessagenew').animate({top: '34%'}, 500);
//        var data = {
//            mail_status: 1
//        }
//        $.ajax({
//            type: "POST",
//            url: "<?php // echo BASEURL;     ?>unsubscribe",
//            data: data,
//            dataType: "json",
//            success: function (r) {
//
//            }
//        });
//    });

//    $(document).on("click", "#resubscribe", function () {
//        $(this).hide();
//        $("#cmessage").html("You're subscribed!");
//        var data = {
//            mail_status: 0
//        }
//        $.ajax({
//            type: "POST",
//            url: "<?php // echo BASEURL;     ?>unsubscribe",
//            data: data,
//            dataType: "json",
//            success: function (r) {
//
//            }
//        });
//
//
//    });
//    $("#close_popupnew").click(function () {
//        $('.alertPopupMessagenew').animate({top: '0'}, 500);
//        $(".alertPopupDivnew").removeClass("show_overlay").css("display", "none");
//
//    });
</script>
