<html>
    <head>
        <script type="text/javascript" src="<?PHP echo BASEURL_OJS ?>jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="<?PHP echo BASEURL_OJS ?>jquery-ui.min.js"></script>

        <style>
            .loader_div{
                border-radius: 11px;
                padding-top:35px;
                background-color: white;
                text-align: center;
                height:150px;
                width: 136px;
                margin: auto;
                left:0; right:0;
                top:0; bottom:0;
                position: absolute;
            }
            .ajax-loader {

                margin: auto;
                left:0; right:0;
                top:0; bottom:0;
                position: absolute;

            }
        </style>
        <script type="text/javascript">
            var hash = '<?php echo $hash ?>';
            function submitPayuForm() {
                if (hash == '') {
                    return;
                }
                var payuForm = document.forms.payuForm;
                payuForm.submit();
            }
        </script>
    </head>
    <body onload="submitPayuForm()" >
        <div id="demo" style="display:none">
            <div class="loader_div">
                <img src="<?php echo BASEURL_BIMG ?>89.gif" alt="Loading" class="ajax-loader " />
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post" name="payuForm" id="payuForm">
            <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY; ?>"  />
            <input type="hidden" name="hash" value="<?php echo $hash; ?>"  />
            <input type="hidden" name="txnid" value="<?php echo $txnid; ?>" />
            <input name="amount" value="<?php echo $amount; ?>" input type="hidden"/>
            <input name="firstname" id="firstname" value="<?php echo $firstname; ?>" input type="hidden"/>
            <input name="email" id="email" value="<?php echo $email; ?>" input type="hidden" />
            <input name="phone" value="<?php echo $phone; ?>" input type="hidden"/>
            <input name="productinfo" value="<?php echo $productinfo; ?>" input type="hidden"/>
            <input name="surl" value="<?php echo $surl; ?>" size="64" input type="hidden" />
            <input name="furl" value="<?php echo $furl; ?>" size="64" input type="hidden"/>
             <input name="udf2" value="<?php echo $udf2; ?>" size="64" input type="hidden"/>
            <input type="hidden" name="service_provider" value="payu_paisa" size="64" input type="hidden"/>
            <?php if (!$hash) { ?>
            <input type="submit"  value="submit" size="64" input type="hidden"/>
            <?php } ?>

        </form>

    </body>
</html>
