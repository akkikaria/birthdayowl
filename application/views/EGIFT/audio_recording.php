<!--Recording:start-->
<input type="file" id="selectaudiofile" style="display:none">
<div class='recordingOptions'>
    <div class="recordingInnerDiv">
        <div class="closerecording" id="closerecording">CLOSE</div>
        <div  class="closeaudio" id="closeaudio">CLOSE</div>
        <div id="loaderaudio" >
            <img src="<?php echo BASEURL_OIMG . "ring.gif" ?>"    />
            <div  class="uploadingtag">Uploading Audio...</div>
        </div>
        <div style="margin: auto;width: 100%;display: none;"  id="playbox">

            <div class="audioContainer_new"  >
                <div class="audio_player_preview">
                    <img class="cover" src="<?php echo BASEURL_OIMG; ?>bday_player.png" alt=""style="width:90px;height:82px; float: left">
                    <audio id="playaudio" style="display:none;" class="paudioplayer" src="<?php echo BASEURL_BVMG; ?>demo.mp3" ></audio>
                </div>

                <div class="twoaudiobuttons">
                    <div id="uploadfile"  class="uploadfilenew">UPLOAD</div>
                    <div id="removefile" class="removefilenew" >CANCEL</div>
                </div>
            </div>

        </div>

        <div style="margin: auto" id="moptions">
            <div class="recordchoices">
                <div id="select_audio" style="cursor:pointer">
                    <img src="<?php echo BASEURL_OIMG; ?>upload_file.png"  style="width:70px"/>
                    <img src="<?php echo BASEURL_OIMG; ?>recordselect.png" style="width:165px;" />
                </div>
                <div class="vertical_line">
                    <img src="<?php echo BASEURL_OIMG; ?>vertical_line.png" style="height:213px;" />
                </div>
                <div  id="record_audio" style="cursor:pointer"> 

                    <img src="<?php echo BASEURL_OIMG; ?>voice.png" style="width:70px" />
                    <img src="<?php echo BASEURL_OIMG; ?>recordaudio.png" style="width:165px;" />

                </div>
            </div>
        </div>


        <div style="margin: auto; width: 100%;" id="dorecording">


            <div class="audioclock" ></div>
            <div  class="defaultheight"></div>
            <div id="recording_section">

                <select class="media-container-format" style="display:none;">
                    <option disabled>WAV</option>
                    <option disabled>Ogg</option>

                </select>
                <div id="recordingstartcontainer"> 
                    <div id="recordlable">
                        <img src="<?php echo BASEURL_OIMG; ?>arecord_welcome.png" />
                    </div>

                    <div class="audiorecordingplayer">
                        <img  src="<?php echo BASEURL_OIMG; ?>bday_player.png" alt=""style="width:90px;height:82px;float:left;" id="musicalowl">

                        <div id="equi">
                            <div class="bar" style="width: 0%;"></div>
                            <div class="equalizer"> 
                                <span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
                            </div>
                        </div>

                    </div>
                    <div id="audioc">
                    </div>
                </div>


                <div id="previewaudiocontainer"> 
                    <div class="audio-player">
                        <img class="cover" src="<?php echo BASEURL_OIMG; ?>bday_player.png" alt=""style="width:90px;height:82px;">
                        <audio id="audio-player" style="display:none;" class="paudioplayer" src="<?php echo BASEURL_BVMG; ?>demo.mp3" type="audio/mp3" controls="controls"></audio>
                    </div>
                </div>

            </div>


        </div>

        <div class="arecording">
            <div>
                <div id="startaudiorecording" >START</div>
                <div id="stopaudiorecording" >STOP</div>
                <div id="saveaudiorecording" >UPLOAD</div>
                <div id="cancelaudiorecording" >CANCEL</div>
            </div>

        </div>
        <div style="width:90%;margin: auto;">

            <div id="audiouploadprogress" class="audiouploadhide">
                <div id="audiobar"></div>
                <div id="audiopercent">0%</div >

            </div>
        </div>
        <div class="apv">Uploading Audio...</div>
        <div class="videosize_div" id="audiosize">Max audio size 3 MB.</div>
    </div>

</div>

<script>
    //audio recording:Start

    //fluctuate


    var overallAnimationDuration = 60000; // <---- CHANGE IF YOU LIKE
    // jQuery animate
    $('.bar').animate({
        width: '100%'
    }, {
        duration: overallAnimationDuration,
        easing: "linear"
    });
    function fluctuate(bar) {
        var hgt = Math.random() * 35;
        hgt += 1;
        var t = 15 * hgt;

        if (t < 40) {
            t = 40;
        } else if (t > 400) {
            t = 400;
        }

        bar.animate({
            height: hgt
        }, t, "swing", function () {
            fluctuate($(this));
        });

    }

    $('#audio-player,#playaudio').mediaelementplayer({
        alwaysShowControls: true,
        features: ['playpause', 'progress', 'volume'],
        audioVolume: 'horizontal',
        audioWidth: 450,
        audioHeight: 70,
        iPadUseNativeControls: true,
        iPhoneUseNativeControls: true,
        AndroidUseNativeControls: true
    });
    (function () {
        var params = {},
                r = /([^&=]+)=?([^&]*)/g;

        function d(s) {
            return decodeURIComponent(s.replace(/\+/g, ' '));
        }

        var match, search = window.location.search;
        while (match = r.exec(search.substring(1))) {
            params[d(match[1])] = d(match[2]);

            if (d(match[2]) === 'true' || d(match[2]) === 'false') {
                params[d(match[1])] = d(match[2]) === 'true' ? true : false;
            }
        }

        window.params = params;
    })();

    var audioajaxstop, auploadingstart = 0;

    var audioclock, acountup, audiocancel = 0;
    audioclock = $('.audioclock').FlipClock(0, {
        clockFace: 'MinuteCounter',
        countdown: false,
        autoStart: false,
        callbacks: {
            start: function () {
                auploadingstart = 0;
                $("#audio_size").html("Max audio size 3 MB.");
            },
            stop: function () {
                if (audiocancel == 0) {
                    stopStream();
//                    alert('cancel 0');
//                    $(".clock").hide();
//                    $(".videoOptions").show();
//                    $("#voptions").show();
//                    $(".closerecording").show();
//                    $(".vrecording").hide();
//                    $("#videos-container").hide();
//                    $(".recordingvideo").hide();
//                    $("#video_new_player").hide().remove();
//                    $("#videopreviewContainer").hide();
//                    $(".capturevideosection").hide();
//                    $('.videoInnerDiv').animate({top: '20%'}, 500);
//                    $("#vinnerDiv").removeClass("videoInnerDivnew").addClass("videoInnerDiv");
//                    $(".dorecordimg").hide();
//                    $(".videorecordingcontainer").hide();
                }
                if (audiocancel == 2) {
//                    alert('cancel 2');
                    stopStream();
                    audiocancel = 0;
                    alert("Recording limit is exceeded.");

//                    $("#audiorecorder").hide();
//                    $("#previewaudiocontainer").show();
//                    $("#recordingstartcontainer").hide();
//                    $("#previewaudioplayer").css("display", "block");
//                    $("#startaudiorecording").css("color", "#52A303");
//                    document.getElementById('stopaudiorecording').style.pointerEvents = 'none';
//                    document.getElementById('startaudiorecording').style.pointerEvents = 'auto';
//                    document.getElementById('saveaudiorecording').style.pointerEvents = 'auto';
//                    $("#stopaudiorecording").css("color", "#000000");
//                    $("#saveaudiorecording").css("color", "#52A303");

                }



            },
        },
    });



    acountup = setInterval(function () {

        if (audioclock.getTime().time > 20) {
            if (stopaudiobutton.recordRTC) {
                if (stopaudiobutton.recordRTC.length) {

                    stopaudiobutton.recordRTC[0].stopRecording(function (url) {
                        if (!stopaudiobutton.recordRTC[1]) {
                            stopaudiobutton.recordingEndedCallback(url);
                            stopStream();
                            return;
                        }

                        stopaudiobutton.recordRTC[1].stopRecording(function (url) {
                            stopaudiobutton.recordingEndedCallback(url);
                            stopStream();
                        });
                    });
                } else {
                    stopaudiobutton.recordRTC.stopRecording(function (url) {
                        console.log("ssss");
                        stopaudiobutton.recordingEndedCallback(url);
                        stopStream();
                        audiocancel = 2;
                        audioclock.stop();
                        audioclock.reset();
//                        audioclock.stop();
//                        audiocancel = 2;
                        $("#audiorecorder").hide();
                        $("#previewaudiocontainer").show();
                        $("#recordingstartcontainer").hide();
                        $("#previewaudioplayer").css("display", "block");
                        $("#startaudiorecording").css("color", "#52A303");
                        document.getElementById('stopaudiorecording').style.pointerEvents = 'none';
                        document.getElementById('startaudiorecording').style.pointerEvents = 'auto';
                        document.getElementById('saveaudiorecording').style.pointerEvents = 'auto';
                        $("#stopaudiorecording").css("color", "#000000");
                        $("#saveaudiorecording").css("color", "#52A303");
                    });
                }
            }
        }
        else{
//            alert('akki');
        }
    }, 500);



    $("#iaudio").click(function () {
        $(".BackgroundMedia").show();
        $(".recordingOptions").show();
        $("#moptions").show();
        $(".closerecording").show();
        $('.recordingInnerDiv').animate({top: '20%'}, 500);
    });

    $("#select_audio").click(function () {
        $("#selectaudiofile").click();
    });

    $("#selectaudiofile").change(function (event) {
        var fileUpload = $(this)[0];
        var tmppath = URL.createObjectURL(event.target.files[0]);
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.mp3|.ogg|.wma|.wav|.mid)$");
        if (regex.test(fileUpload.value.toLowerCase())) {
            var videosize = event.target.files[0].size;
            //file size in  byte (convert it into  MB)
            if (videosize > 3000000) {
                alert("Please upload a smaller Audio file, max size is 3MB");
                return false;
            } else {
                $(".BackgroundMedia").css("display", "none");
                $(".recordingOptions").css("display", "none");
                $('.recordingInnerDiv').animate({top: '0%'}, 500);
                $(".uploaded_pic1").attr("name", "userfile1");
                $(".upload_video").attr("name", "userfile2");
                $("#selectaudiofile").attr("name", "userfile");
                $("#checktype").val("3"); // audio:3

                $(".maincircle").hide();

                mediaupload(3);



            }
        } else {

            alert("Please select valid Audio format");
            return false;
        }
    });



    $("body").on("click", "#playit", function (e)
    {

        var ID = $(this).attr("id");
        var progressArea = $("#playaudioProgress");
        var audioTimer = $("#audioTimePlayer");
        var audio = $("#playaudioplayer");
        var audioCtrl = $(this);
        e.preventDefault();
        var R = $(this).attr('rel');
        if (R == 'play')
        {
            $(this).removeClass('audioPlayer').addClass('audioPlayerPause').attr("rel", "pause");
            audio.trigger('play');
        } else {
            $(this).removeClass('audioPlayerPause').addClass('audioPlayer').attr("rel", "play");
            audio.trigger('pause');
        }

        audio.bind("timeupdate", function (e) {
            var audioDOM = audio.get(0);
            audioTimer.text(getAudioTimeByDec(audioDOM.currentTime, audioDOM.duration));
            var audioPos = (audioDOM.currentTime / audioDOM.duration) * 100;

            progressArea.css('width', audioPos + "%");
            if (audioPos == "100")
            {
                $("#" + ID).removeClass('audioPlayerPause').addClass('audioPlayer').attr("rel", "play");
                audio.trigger('pause');
            }
        });

    });

    $("#record_audio").click(function () {
        document.getElementById('startaudiorecording').style.pointerEvents = 'auto';
        document.getElementById('stopaudiorecording').style.pointerEvents = 'none';
        document.getElementById('saveaudiorecording').style.pointerEvents = 'none';
        $("#stopaudiorecording").css("color", "#000000");
        $("#saveaudiorecording").css("color", "#000000");
        $("#moptions").hide();
        $("#playbox").hide();
        $("#dorecording").show();
        $(".closerecording").hide();
        $(".arecording").show();
        $("#previewaudioplayer").hide();
        $("#audio_size").html("Max audio size 3 MB.");
        $("#demorecorder").show();
        $("#startaudiorecording").css("color", "#52A303");
        $(".defaultheight").css("height", "50px");
        $("#previewaudiocontainer").hide();
        $("#recordlable").show();
        $("#musicalowl").hide();
        $(".audiorecordingplayer").hide();
        $("#recordingstartcontainer").show();

    });

    var recordingPlayer = document.getElementById('audiorecorder');
    var stopaudiobutton = document.getElementById('stopaudiorecording');
    var startaudiobutton = document.getElementById('startaudiorecording');
    var mediaContainerFormat = document.getElementById('.media-container-format');

    var previewaudioplayer = document.getElementById("audio-player");
    $("#startaudiorecording").click(function () {
        var commonConfig = {
            onMediaCaptured: function (stream) {
                stopaudiobutton.stream = stream;
                if (stopaudiobutton.mediaCapturedCallback) {
                    stopaudiobutton.mediaCapturedCallback();
                }

            },
            onMediaStopped: function () {
                if (!startaudiobutton.disableStateWaiting) {
                    startaudiobutton.disabled = false;
                }
            },
            onMediaCapturingFailed: function (error) {
                if (error.name === 'PermissionDeniedError' && !!navigator.mozGetUserMedia) {
                    InstallTrigger.install({
                        'Foo': {
                            // https://addons.mozilla.org/firefox/downloads/latest/655146/addon-655146-latest.xpi?src=dp-btn-primary
                            URL: 'https://addons.mozilla.org/en-US/firefox/addon/enable-screen-capturing/',
                            toString: function () {
                                return this.URL;
                            }
                        }
                    });
                }
                commonConfig.onMediaStopped();
            }
        };


//                                if (recordingMedia.value === 'record-audio') {
        captureAudio(commonConfig);
        startaudiobutton.mediaCapturedCallback = function () {
            startaudiobutton.recordRTC = RecordRTC(startaudiobutton.stream, {
                type: 'audio',
                bufferSize: typeof params.bufferSize == 'undefined' ? 0 : parseInt(params.bufferSize),
                sampleRate: typeof params.sampleRate == 'undefined' ? 44100 : parseInt(params.sampleRate),
                leftChannel: params.leftChannel || false,
                disableLogs: params.disableLogs || false,
                recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null
            });

            startaudiobutton.recordingEndedCallback = function (url) {

//                                        var audio = new Audio();
//                                        audio.src = url;
//                                        audio.controls = true;
//                                        recordingPlayer.parentNode.appendChild(document.createElement('hr'));
//                                        recordingPlayer.parentNode.appendChild(audio);
//                                        if (audio.paused)
//                                            audio.play();
//                                        audio.onended = function () {
//                                            audio.pause();
//                                            audio.src = URL.createObjectURL(startaudiobutton.recordRTC.blob);
//                                        };
            };
            startaudiobutton.recordRTC.startRecording();
        };
        stopaudiobutton.mediaCapturedCallback = function () {
            stopaudiobutton.recordRTC = RecordRTC(stopaudiobutton.stream, {
                type: 'audio',
                bufferSize: typeof params.bufferSize == 'undefined' ? 0 : parseInt(params.bufferSize),
                sampleRate: typeof params.sampleRate == 'undefined' ? 44100 : parseInt(params.sampleRate),
                leftChannel: params.leftChannel || false,
                disableLogs: params.disableLogs || false,
                recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null
            });

            stopaudiobutton.recordingEndedCallback = function (url) {
                console.log("ss" + url);

                previewaudioplayer.src = url;
                previewaudioplayer.controls = false;
                if (previewaudioplayer.paused)
                    //  previewaudioplayer.play();
                    previewaudioplayer.onended = function () {
                        previewaudioplayer.pause();
                        previewaudioplayer.src = URL.createObjectURL(stopaudiobutton.recordRTC.blob);

                    };
            };
            stopaudiobutton.recordRTC.startRecording();
        };
//                                }
    });

    function captureAudio(config) {
        captureUserMedia({audio: true}, function (audioStream) {

            var audiop = document.createElement('audio');
            audiop.controls = false;
            audiop.muted = true;
            audiop.id = "audiorecorder";
            $("#audioc").html(audiop);
            audiop.srcObject = audioStream;
            audiop.play();
            $(".audioclock").show();
            $("#videopreviewContainer").hide();
            $("#videos-container").show();
            $(".defaultheight").css("height", "3px");
            $("#audio_size").html("Max audio size 3 MB.");
            audioclock.start();
            $("#recordlable").hide();
            $(".audiorecordingplayer").show();

            $("#demorecorder").hide();
            $("#previewaudioplayer").hide();
            $("#previewaudiocontainer").hide();
            $("#recordingstartcontainer").show();
            $("#musicalowl").show();
            $("#recordlable").hide();
            $("#equi").show();
            $(".equalizer span").each(function (i) {
                fluctuate($(this));
            });
            $("#startaudiorecording").css("color", "#000000");
            document.getElementById('stopaudiorecording').style.pointerEvents = 'auto';
            document.getElementById('startaudiorecording').style.pointerEvents = 'none';
            document.getElementById('saveaudiorecording').style.pointerEvents = 'none';
            $("#stopaudiorecording").css("color", "#52A303");
            $("#saveaudiorecording").css("color", "#000000");
            config.onMediaCaptured(audioStream);

            audioStream.onended = function () {
                config.onMediaStopped();
            };
        }, function (error) {

            alert("To use this recording feature, please connect your external microphone to your computer and try again");
            config.onMediaCapturingFailed(error);
        });
    }
    getAudioTimeByDec = function (cTime, duration) {
        var duration = parseInt(duration),
                currentTime = parseInt(cTime),
                left = duration - currentTime,
                second, minute;
        second = (left % 60);
        minute = Math.floor(left / 60) % 60;
        second = second < 10 ? "0" + second : second;
        minute = minute < 10 ? "0" + minute : minute;

        return minute + ":" + second;
    };

    $(document).on("click", "#stopaudiorecording", function () {
        if (stopaudiobutton.recordRTC) {
            if (stopaudiobutton.recordRTC.length) {

                stopaudiobutton.recordRTC[0].stopRecording(function (url) {
                    if (!stopaudiobutton.recordRTC[1]) {
                        stopaudiobutton.recordingEndedCallback(url);
                        stopStream();
                        return;
                    }

                    stopaudiobutton.recordRTC[1].stopRecording(function (url) {
                        stopaudiobutton.recordingEndedCallback(url);
                        stopStream();
                    });
                });
            } else {
                stopaudiobutton.recordRTC.stopRecording(function (url) {
                    console.log("sss");
                    stopaudiobutton.recordingEndedCallback(url);
                    stopStream();
                    audioclock.stop();
                    audiocancel = 1;
                    audioclock.reset();
                    $("#audiorecorder").hide();
                    $("#previewaudiocontainer").show();
                    $("#recordingstartcontainer").hide();
                    $("#previewaudioplayer").css("display", "block");
//                    $("#startaudiorecording").css("color", "#52A303");
                    document.getElementById('stopaudiorecording').style.pointerEvents = 'none';
                    document.getElementById('startaudiorecording').style.pointerEvents = 'none';
                    document.getElementById('saveaudiorecording').style.pointerEvents = 'auto';
                    $("#stopaudiorecording").css("color", "#000000");
                    $("#saveaudiorecording").css("color", "#52A303");
                });
            }
        }

    });


    function stopStream() {
        if (stopaudiobutton.stream && stopaudiobutton.stream.stop) {
            stopaudiobutton.stream.stop();
            //stopaudiobutton.stream.getVideoTracks()[0].stop();
            stopaudiobutton.stream = null;
        }
    }
    $("#removefile").click(function () {
        $("#playbox").css("display", "none");
        $("#audioTime").text("00:00");
        $(".closerecording").show();
        $("#moptions").show();
        $("#dorecording").hide();
        $("#playaudio").attr('src', "");
        var audioPlayer = document.getElementById("recordingplay");
        audioPlayer.pause();
    });

    $(document).on("click", "#uploadfile", function () {

//    $("#uploadfile").click(function (e) {

        $(".BackgroundMedia").css("display", "none");
        $(".recordingOptions").css("display", "none");
        $('.recordingInnerDiv').animate({top: '0%'}, 500);
        $(".uploaded_pic1").attr("name", "userfile1");
        $(".upload_video").attr("name", "userfile2");
        $("#selectaudiofile").attr("name", "userfile");
        $("#playaudioplayer").attr('src', $("#playaudio").attr("src"));
        $("#checktype").val("3"); // audio:3
        $("#audioTime").text("00:00");
        $("#audioTimePlayer").text("00:00");
        $(".imgVideoContainer").hide();
        $(".afterclick").hide();
        $(".maincircle").hide();
        $(".titlebox").hide();
        $(".playerbox").show();
        $("#playbox").hide();
        $("#dorecording").hide();
        $(".closerecording").show();
        $(".recordchoices").css("width", "100%");
        $("#select_audio").css("width", "40%");
        $(".vertical_line").css("width", "20%");
        $("#record_audio").css("width", "40%");

        $(".sampleimg").hide();

//        var audioPlayer = document.getElementById("recordingplay");
//        audioPlayer.pause();
    });

    $(".stopPlayer").click(function (e) {
        $("#playit").removeClass('audioPlayerPause').addClass('audioPlayer').attr("rel", "play");
        $(".playerbox").hide();
        $("#playaudioplayer").attr('src', "");
        $(".maincircle").show();
        $(".sampleimg").show();
        $(".titlebox").show();
        $("#checktype").val("0");
        var audioPlayer = document.getElementById("playaudioplayer");
        audioPlayer.pause();

    });

    $(document).on("click", "#cancelaudiorecording", function () {
        $("#moptions").show();
        $("#dorecording").hide();
        $("#audio_size").html("");

        $("#audio_size").html("Max audio size 3 MB.");
        $(".closerecording").show();
        $(".arecording").hide();
        $(".audioclock").hide();

        $("#audiouploadprogress").hide();
        $(".apv").hide();


        $("#audiorecorder").hide().remove();
        $("#previewaudiocontainer").hide();
        $("#recordingstartcontainer").show();
        $("#musicalowl").show();
        $("#recordlable").hide();
        $("#equi").hide();
        audioclock.stop();
        audioclock.reset();
        if (auploadingstart == 1) {
            audioajaxstop.abort();
            auploadingstart = 0;
        }
    });

    $(document).on("click", ".closerecording", function () {
        $(".BackgroundMedia").css("display", "none");
        $(".recordingOptions").css("display", "none");
        $(".imageOptions").css("display", "none");
        $(".imageInnerDiv").animate({top: '0%'}, 500);
        $("#moptions").css("display", "none");
        $("#ioptions").css("display", "none");
        $('.recordingInnerDiv').animate({top: '0%'}, 500);
        $(".videoOptions").css("display", "none");
        $(".videoInnerDiv").animate({top: '0%'}, 500);
        $("#voptions").css("display", "none");

    });
    $("#closeaudio").click(function () {
        $("#moptions").show();
        $("#dorecording").hide();
        $(".closeaudio").hide();
        $(".closerecording").show();

    });

    function setMediaContainerFormat(arrayOfOptionsSupported) {
        var options = Array.prototype.slice.call(
                mediaContainerFormat.querySelectorAll('option')
                );

        var selectedItem;
        options.forEach(function (option) {
            option.disabled = true;

            if (arrayOfOptionsSupported.indexOf(option.value) !== -1) {
                option.disabled = false;

                if (!selectedItem) {
                    option.selected = true;
                    selectedItem = option;
                }
            }
        });
    }

    if (webrtcDetectedBrowser === 'edge') {
        // webp isn't supported in Microsoft Edge
        // neither MediaRecorder API
        // so lets disable both video/screen recording options

        console.warn('Neither MediaRecorder API nor webp is supported in Microsoft Edge. You cam merely record audio.');

        // recordingMedia.innerHTML = '<option value="record-audio">Audio</option>';
        setMediaContainerFormat(['WAV']);
    }
    $("#saveaudiorecording").click(function () {

        $(".audioclock").hide();
        $(".defaultheight").css("height", "50px");
//        $(".dorecordimg").hide();
        $("#saveaudiorecording").css("color", "#52A303");
        document.getElementById('stopaudiorecording').style.pointerEvents = 'none';
        document.getElementById('startaudiorecording').style.pointerEvents = 'none';
        document.getElementById('saveaudiorecording').style.pointerEvents = 'none';
        $("#startaudiorecording").css("color", "#000000");
        $("#stopaudiorecording").css("color", "#000000");
        var formData = new FormData();
        var fileExtension = ('audio/ogg').split('/')[1];
        var fileFullName = ((Math.round(Math.random() * 9999999999) + 888888888)) + '.' + fileExtension;
        formData.append('userfile', fileFullName);
        formData.append('videofile', stopaudiobutton.recordRTC.blob);
        var url = "<?php echo BASEURL . 'Home_web/upload_video'; ?>";
        audioajaxstop = $.ajax({
            url: url,
            type: "POST",
            data: formData,
            async: true,
            dataType: "json",
            beforeSend: function () {
//                $("#loaderaudio").css("top", "24%");
//                $("#loaderaudio").show();
//                $(".uploadingtag").css("color", "black");

                $(".audiouploadhide").show();
                $("#audiouploadprogress").show();
                //clear everything
                $("#audiobar").width('0%');

                $("#savevideorecording").css("color", "#000000");
//                $("#message").html("");
                $("#audiopercent").html("0%");

                auploadingstart = 1;

            },
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $("#audiobar").width(percentComplete + '%');
                        $("#audiopercent").html(percentComplete + '%');
                        $(".apv").show();
                        if (percentComplete == 100) {
                            $(".apv").html("Audio Uploaded Successfully!");
                        }

                    }
                }, false);

                return xhr;
            },
            success: function (msg) {

                if (msg.success.toString() == "true") {

                    $("#uploaded_finalimg").val(msg.filename.toString());
                    $("#imgcaptureuploaded").val(msg.filename.toString());

                    $("#audiobar").width('100%');
                    $("#audiopercent").html('100%');
                    $(".apv").html("Uploading Audio...");
                    $(".apv").hide();
                    $(".audiouploadhide").hide();
                    $("#audio_size").html("Max audio size 3 MB.");

                    var audiofile = "<?php echo BASEURL_BVMG; ?>" + msg.filename.toString();
                    $(".BackgroundMedia").css("display", "none");
                    $(".recordingOptions").css("display", "none");
                    $('.recordingInnerDiv').animate({top: '0%'}, 500);
                    $(".uploaded_pic1").attr("name", "userfile1");
                    $(".upload_video").attr("name", "userfile2");
                    $("#selectaudiofile").attr("name", "userfile");
                    $("#playaudioplayer").attr('src', audiofile);


                    $(".recordingOptions").hide();
                    $(".sampleimg").hide();
                    $("#checktype").val("3"); // audio:3
                    $("#audioTime").text("00:00");
                    // $("#audioTimePlayer").text("00:00");
                    $(".imgVideoContainer").hide();
                    $(".afterclick").hide();
                    $(".maincircle").hide();
                    $(".titlebox").hide();
                    $(".playerbox").show();
                    $("#playbox").hide();
                    $("#dorecording").hide();
                    $("#loaderaudio").hide();
                    $(".arecording").hide();
                    $("#loaderaudio").css("top", "0%");
                    $(".closerecording").show();


                } else {
                }
            }, cache: false, contentType: false, processData: false});
    });
</script>