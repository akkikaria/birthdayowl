<?php $this->load->view("new_templates/header"); ?>
<?php
//echo '<pre>';
//print_r($orderdata);
//exit;
?>
<style>
    .pro_img{
        left: 37%;
        position: absolute;
        top: 19%;
    }
    .recname{
        color: white;font-size: 14px;  text-align: left;     margin-left: 26px;  width: 100%;    height: 13px;
    }

    .recemail{
        color: white; width: 100%;   text-align: left;    margin-left: 26px; height: 13px;font-size: 14px;
    }
    .recschedule{
        text-align: left;    margin-left: 26px;  width: 100%;    height: 13px; color: white;font-size: 14px;  
    }

    .scart{
        background-color: gray;height:229px; width:100%;   border-radius: 12px;
    }
    .namelist{
        padding-left: 58px;
        padding-top: 5px;

    }
    .namelist li{
        list-style: disc;
        height: 30px;

    }
    .upperBlock{
        width: 100%;  height: 150px;
    }
    .ulcontent{
        font-size:18px;height: 30px;padding-top: 10px;
    }
    @media only screen and (max-width: 768px) {
        .scart{
            margin: auto;
            width: 50%;
        }

        .upperBlock{
            height: 413px;
        }

    }

    @media only screen and (max-width:480px)
    {


        .scart{

            width: 100%;
        }
        .ulcontent{
            font-size: 12px;
        }

    }




</style>
<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            Payment <span>Successful! </span>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <div class="inner_midd">
            <div class="in_f_l">
                <ul class="leftside leftside_nav">
                    <li class="psuccess"><a href="#pick_amount">Pick Amount</a></li>
                    <li class="psuccess"><a href="#make_it_personal">Make It Personal</a></li>
                    <li class="psuccess"><a href="#choose_delivery">Choose Delivery</a></li>
                    <li class="psuccess"><a href="#checkout">CHECKOUT</a></li>
                </ul>
            </div>
            <?php if ($what_was_transaction == 1) { ?>
                <div class="in_f_r add_bday_reminder">
                    <div class="wrapper ">
                        <div class="upperBlock">
                            <?php
                            $tamount = 0;
//                            $receiver_names = array();
                            for ($i = 0; $i < count($orderdata); $i++) {
                                $tamount += $orderdata[$i]["selected_amount"];
//                                $receiver_names[$i] = $orderdata[$i]["fname"];
                            }
//                            $names = implode(",", $receiver_names);
                            ?>
                            <div style="font-size:22px;height: 50px;line-height: 44px;">You're Done!</div>
                            <div style="border-bottom: 1px solid #bdbdbd;text-align: center;"></div>
                            <ul>
                                <li class="ulcontent"><img src="<?php echo BASEURL_OIMG; ?>checkmark.png"  style="width:20px;"/><span style="height:20px;padding-left: 10px"> Your payment of Rs. <?php echo $total; ?> was <b>successfully received.</b></span></li>
                                <li class="ulcontent"><img src="<?php echo BASEURL_OIMG; ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px"> We <b>emailed you a payment receipt</b> for your records.</span></li>
                                <?php foreach ($orderdata as $orders) { ?>
                                    <li class="ulcontent" style="margin-top:20px;"><img src="<?php echo BASEURL_OIMG; ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px"> We will update you on your email <b><?php echo $orders["uemail"]; ?></b> when <b><?php echo $orders["fname"]; ?></b> opens your Gift Card <b><?php echo $orders["voucher_pro_name"]; ?></b>.<br></span></li>
                                <?php } ?>
        <!--<li class="ulcontent"><img src="<?php // echo BASEURL_OIMG;      ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px"> We will update you by email when <b><?php echo $names; ?></b> opens your Gift Card.</span></li>-->
                                <!--                                <li class="ulcontent">
                                                                    <img src="<?php echo BASEURL_OIMG; ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px"> This e-Gift Cards has been delivered to the following email id(s):</span>
                                                                </li>-->
                                <!--                                <li class="ulcontent">
                                                                    <ul class="namelist">
                                <?php
//                                        foreach ($orderdata as $orders) {
                                ?>
                                                                            <li class="ulcontent"><b><?php // echo $orders["femail"];       ?></b></li>
                                <?php // } ?>
                                                                    </ul>
                                                                </li>-->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="in_f_l">
                    <div class="leftside">
                        <div class="right_v">
                            <h4>
                                YOUR ORDER
                            </h4>
                            <ul>
                                <?php
                                $sum = 0;
                                foreach ($orderdata as $voucher) {
                                    $sum += $voucher["selected_amount"];
                                    ?>
                                    <li style="width:100%">
                                        <div style="font-weight: 700; text-align: center;"><?php echo $voucher["voucher_pro_name"]; ?></div>
                                        <!--<abbr><?php // echo $voucher["voucher_pro_name"];                                   ?></abbr>-->
                                        <div class='scart'>
                                            <div style="overflow: hidden; transform: rotate(-8deg); padding-top: 18px; margin-left: 24px; width: 39%;" >
                                                <img src="<?php echo BASEURL_CROPPED_GREET . $voucher["front_page_image"]; ?>" style=" height:128px;"/> 
                                            </div>
                                            <div class="pro_img">
                                                <?php
                                                if ($voucher["voucher_img"] != '') {
                                                    $img = $voucher["voucher_img"];
                                                } else {
                                                    $img = BASEURL_OIMG . "unnamed.png";
                                                }
                                                ?>
                                                <img src="<?php echo $img; ?>"  style=" width: 93%; height: 68px;" />
                                            </div>
                                            <div >
                                                <?php if ($voucher['femail'] != NULL) { ?>
                                                    <p style="text-align: center; font-weight: bold; font-size: 11px;color: white;">Delivery By Email</p> 
                                                <?php } else { ?>
                                                    <p style="text-align: center; font-weight: bold; font-size: 11px;color: white;">Delivery By SMS</p> 
                                                <?php } ?>
                                                <div class='recname' style="background: transparent url('<?php echo BASEURL_OIMG; ?>user.png') no-repeat scroll 0px 0px / 13px 14px; "><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["fname"]; ?></span></div>
                                                <?php if ($voucher["femail"] == "") { ?>
                                                    <div class='recemail'style=" background: transparent url('<?php echo BASEURL_OIMG; ?>smm_greyb.png') no-repeat scroll 0px 0px  / 13px 14px;"><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["fphone"]; ?></span></div>
                                                <?php } else { ?>
                                                    <div class='recemail'style=" background: transparent url('<?php echo BASEURL_OIMG; ?>email.png') no-repeat scroll 0px 0px  / 13px 14px;"><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["femail"]; ?></span></div>
                                                <?php } ?>
                                                <?php
                                                $schedule = $voucher["delievery_schedule"];
                                                $status = "";
                                                if ($schedule == "0") {
                                                    $status = "Immediately";
                                                } else if ($schedule == "1") {
                                                    $status = "Today";
                                                } else if ($schedule == "2") {
                                                    $status = "Tomorrow";
                                                } else if ($schedule == "3") {
//                                                    $status = "Day after tomorrow";
                                                    $status = date("M d,Y", strtotime($voucher["delivery_date_time"]));
                                                } else if ($schedule == "4") {
                                                    $status = date("M d,Y", strtotime($voucher["delivery_date_time"]));
                                                }
                                                ?>
                                                <div  class='recschedule' style=" background: transparent url('<?php echo BASEURL_OIMG; ?>clk.png') no-repeat scroll 0px 0px / 13px 14px;"><span style=" height: 13px; line-height: 10px; margin-left: 27px;   "><?php echo $status; ?></span></div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <!--                    <div class="leftside" style="margin-top: 10px; padding: 10px; line-height: 25px;display: none;">
                                                <div class="daysalign1">
                                                    <span>Delivery by Email</span>
                                                    <br />
                                                    <span>abdul</span>
                                                    <br />
                                                    <span>abd@gmail.com</span>
                                                    <br />
                                                    <span>Feb 24, 2016 3:44 PM</span>
                                                </div>
                                            </div>-->
                </div>
            <?php } else if ($what_was_transaction == 2) { ?>
                <div class="in_f_r add_bday_reminder">
                    <div class="wrapper ">
                        <div class="upperBlock">
                            <?php
                            $tamount = 0;
//                            $receiver_names = array();
                            for ($i = 0; $i < count($orderdata); $i++) {
                                $tamount += $orderdata[$i]["flower_purchased_price"];
//                                $receiver_names[$i] = $orderdata[$i]["flower_receiver_name"];
                            }
//                            $names = implode(",", $receiver_names);
                            ?>
                            <div style="font-size:22px;height: 50px;line-height: 44px;">You're Done!</div>
                            <div style="border-bottom: 1px solid #bdbdbd;text-align: center;"></div>
                            <ul>
                                <li class="ulcontent"><img src="<?php echo BASEURL_OIMG; ?>checkmark.png"  style="width:20px;"/><span style="height:20px;padding-left: 10px"> Your payment of Rs. <?php echo $total; ?> was <b>successfully received.</b></span></li>
                                <li class="ulcontent"><img src="<?php echo BASEURL_OIMG; ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px"> We <b>emailed you a payment receipt</b> for your records.</span></li>
                                <?php foreach ($orderdata as $orders) { ?>
                                    <li class="ulcontent" style="margin-top:20px;"><img src="<?php echo BASEURL_OIMG; ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px"> We will update you on your email <b><?php echo $orders["flower_sender_email"]; ?></b> when <b><?php echo $orders["flower_receiver_name"]; ?></b> receives your gift <b><?php echo $orders["flower_purchased_name"]; ?></b>.</span></li>
                                <?php } ?>
                                <!--                                <li class="ulcontent">
                                                                        <img src="<?php // echo BASEURL_OIMG;                         ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px"> This Flower Product has been delivered to the following email id(s):</span>
                                                                    </li>-->
                                <!--                                <li class="ulcontent">
                                                                    <ul class="namelist">
                                <?php
//                                        foreach ($orderdata as $orders) {
                                ?>
                                                                            <li class="ulcontent"><b><?php // echo $orders["flower_receiver_email"];                     ?></b></li>
                                <?php // } ?>
                                                                    </ul>
                                                                </li>-->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="in_f_l">
                    <div class="leftside">
                        <div class="right_v">
                            <h4>
                                YOUR ORDER</h4>
                            <ul>
                                <?php
                                $sum = 0;
                                foreach ($orderdata as $voucher) {

                                    $sum += $voucher["flower_purchased_price"];
                                    ?>
                                    <li style="width:100%">
                                        <div style="font-weight: 700; text-align: center;"><?php echo $voucher["flower_purchased_name"]; ?></div>
                                        <!--<abbr><?php // echo $voucher["flower_purchased_name"];                                   ?></abbr>-->
                                        <div class='scart'>
                                            <div style="overflow: hidden; padding: 10px; margin: auto; width: 65%;" >
                                                <img src="<?php echo BASEURL_PRODUCT_IMG . $voucher["flower_purchased_image"]; ?>" style=" height:128px;"/> 
                                            </div>
                                            <!--                                            <div class="pro_img">
                                            <?php
//                                                if ($voucher["flower_purchased_image"] != '') {
//                                                    $img = BASEURL_PRODUCT_IMG . $voucher["flower_purchased_image"];
//                                                } else {
//                                                    $img = BASEURL_OIMG . "unnamed.png";
//                                                }
                                            ?>
                                                                                            <img src="<?php // echo $img;                                 ?>"  style=" width: 93%; height: 68px;" />
                                                                                        </div>-->
                                            <div >
                                                <?php if ($voucher['flower_receiver_email'] != NULL) { ?>
                                                    <p style="text-align: center; font-weight: bold; font-size: 11px;color: white;">&nbsp;</p>                                                                                                                                     
                                                <?php } else { ?>
                                                    <p style="text-align: center; font-weight: bold; font-size: 11px;color: white;">&nbsp;</p>                                                                                                                                         
                                                <?php } ?>
                                                <div class='recname' style="background: transparent url('<?php echo BASEURL_OIMG; ?>user.png') no-repeat scroll 0px 0px / 13px 14px; "><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["flower_receiver_name"]; ?></span></div>
                                                <?php if ($voucher["flower_receiver_email"] == "") { ?>
                                                    <div class='recemail'style=" background: transparent url('<?php echo BASEURL_OIMG; ?>smm_greyb.png') no-repeat scroll 0px 0px  / 13px 14px;"><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["flower_receiver_phone"]; ?></span></div>
                                                <?php } else { ?>
                                                    <div class='recemail'style=" background: transparent url('<?php echo BASEURL_OIMG; ?>email.png') no-repeat scroll 0px 0px  / 13px 14px;"><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["flower_receiver_email"]; ?></span></div>
                                                <?php } ?>
                                                <?php
                                                $status = date("M d, Y", strtotime($voucher["flower_delivery_date"]));
                                                ?>
                                                <div  class='recschedule' style=" background: transparent url('<?php echo BASEURL_OIMG; ?>clk.png') no-repeat scroll 0px 0px / 13px 14px;"><span style=" height: 13px; line-height: 10px; margin-left: 27px;   "><?php echo $status; ?></span></div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } else if ($what_was_transaction == 3) { ?>
                <div class="in_f_r add_bday_reminder">
                    <div class="wrapper ">
                        <div class="upperBlock">
                            <?php
                            $tamount = 0;
//                            $receiver_names = array();
                            for ($i = 0; $i < count($orderdata); $i++) {
                                if ($orderdata[$i]['is_voucher'] == 1 && $orderdata[$i]['is_flower'] == 0) {
                                    $tamount += $orderdata[$i]["selected_amount"];
//                                    $receiver_names[$i] = $orderdata[$i]["fname"];
                                } else if ($orderdata[$i]['is_voucher'] == 0 && $orderdata[$i]['is_flower'] == 1) {
                                    $tamount += $orderdata[$i]["flower_purchased_price"];
//                                    $receiver_names[$i] = $orderdata[$i]["flower_receiver_name"];
                                }
                            }
//                            $names = implode(" & ", $receiver_names);
                            ?>
                            <div style="font-size:22px;height: 50px;line-height: 44px;">You're Done!</div>
                            <div style="border-bottom: 1px solid #bdbdbd;text-align: center;"></div>
                            <ul>
                                <li class="ulcontent"><img src="<?php echo BASEURL_OIMG; ?>checkmark.png"  style="width:20px;"/><span style="height:20px;padding-left: 10px"> Your payment of Rs. <?php echo $total; ?> was <b>successfully received.</b></span></li>
                                <li class="ulcontent"><img src="<?php echo BASEURL_OIMG; ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px"> We <b>emailed you a payment receipt</b> for your records.</span></li>
                            </ul>
                            <ul>
    <!--                                <li class="ulcontent"><img src="<?php // echo BASEURL_OIMG;   ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px"> We will update you by email when <b><?php // echo $names;   ?></b> will opens your Gift Card or Received Flower Product respectively.</span></li><br/>
                                <li class="ulcontent">
                                    <img src="<?php // echo BASEURL_OIMG;   ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px"> This e-Gift Cards has been delivered to the following email id(s):</span>
                                </li>-->
                                <?php
                                foreach ($orderdata as $orders) {
                                    if ($orders['is_voucher'] == 1 && $orders['is_flower'] == 0) {
                                        ?>
                                        <li class="ulcontent" style="margin-top:20px;"><img src="<?php echo BASEURL_OIMG; ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px"> We will update you on your email <b><?php echo $orders["uemail"]; ?></b> when <b><?php echo $orders["fname"]; ?></b> opens your Gift Card <b><?php echo $orders["voucher_pro_name"]; ?></b>.</span></li>
                                        <!--                                        <li class="ulcontent">
                                                                                    <ul class="namelist">
                                                                                        <li class="ulcontent"><b><?php // echo $orders["femail"];   ?></b></li>
                                                                                    </ul>
                                                                                </li>-->
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                            <br/>
                            <ul>
                                <?php
                                foreach ($orderdata as $orders) {
                                    if ($orders['is_voucher'] == 0 && $orders['is_flower'] == 1) {
                                        ?>
                                        <li class="ulcontent" style="margin-top:20px;"><img src="<?php echo BASEURL_OIMG; ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px"> We will update you on your email <b><?php echo $orders["flower_sender_email"]; ?></b> when <b><?php echo $orders["flower_receiver_name"]; ?></b> receives your gift <b><?php echo $orders["flower_purchased_name"]; ?></b>.</span></li>
                                        <?php
                                    }
                                }
                                ?>
                                <!--                                <li class="ulcontent">
                                                                    <img src="<?php // echo BASEURL_OIMG;          ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px"> This Flower Product has been delivered to the following email id(s):</span>
                                                                </li>-->
                                <?php
//                                foreach ($orderdata as $orders) {
//                                    if ($orders['is_voucher'] == 0 && $orders['is_flower'] == 1) {
                                ?>
                                <!--                                        <li class="ulcontent">
                                                                            <ul class="namelist">
                                                                                <li class="ulcontent"><b><?php // echo $orders["flower_receiver_email"];          ?></b></li>
                                                                            </ul>
                                                                        </li>-->
                                <?php
//                                    }
//                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="in_f_l">
                    <div class="leftside">
                        <div class="right_v">
                            <h4>
                                YOUR ORDER</h4>
                            <ul>
                                <?php
                                $sum = 0;
                                foreach ($orderdata as $voucher) {
                                    if ($voucher['is_voucher'] == 1 && $voucher['is_flower'] == 0) {
                                        $sum += $voucher["selected_amount"];
                                        ?>
                                        <li style="width:100%">
                                            <div style="font-weight: 700; text-align: center;"><?php echo $voucher["voucher_pro_name"]; ?></div>
                                            <!--<abbr><?php // echo $voucher["voucher_pro_name"];                                   ?></abbr>-->
                                            <div class='scart'>
                                                <div style="overflow: hidden; transform: rotate(-8deg); padding-top: 18px; margin-left: 24px; width: 39%;" >
                                                    <img src="<?php echo BASEURL_CROPPED_GREET . $voucher["front_page_image"]; ?>" style=" height:128px;"/> 
                                                </div>
                                                <div class="pro_img">
                                                    <?php
                                                    if ($voucher["voucher_img"] != '') {
                                                        $img = $voucher["voucher_img"];
                                                    } else {
                                                        $img = BASEURL_OIMG . "unnamed.png";
                                                    }
                                                    ?>
                                                    <img src="<?php echo $img; ?>"  style=" width: 93%; height: 68px;" />
                                                </div>
                                                <div >
                                                    <?php if ($voucher['femail'] != NULL) { ?>
                                                        <p style="text-align: center; font-weight: bold; font-size: 11px;color: white;">Delivery By Email</p> 
                                                    <?php } else { ?>
                                                        <p style="text-align: center; font-weight: bold; font-size: 11px;color: white;">Delivery By SMS</p> 
                                                    <?php } ?>
                                                    <div class='recname' style="background: transparent url('<?php echo BASEURL_OIMG; ?>user.png') no-repeat scroll 0px 0px / 13px 14px; "><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["fname"]; ?></span></div>
                                                    <?php if ($voucher["femail"] == "") { ?>
                                                        <div class='recemail'style=" background: transparent url('<?php echo BASEURL_OIMG; ?>smm_greyb.png') no-repeat scroll 0px 0px  / 13px 14px;"><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["fphone"]; ?></span></div>
                                                    <?php } else { ?>
                                                        <div class='recemail'style=" background: transparent url('<?php echo BASEURL_OIMG; ?>email.png') no-repeat scroll 0px 0px  / 13px 14px;"><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["femail"]; ?></span></div>
                                                    <?php } ?>
                                                    <?php
                                                    $schedule = $voucher["delievery_schedule"];
                                                    $status = "";
                                                    if ($schedule == "0") {
                                                        $status = "Immediately";
                                                    } else if ($schedule == "1") {
                                                        $status = "Today";
                                                    } else if ($schedule == "2") {
                                                        $status = "Tomorrow";
                                                    } else if ($schedule == "3") {
//                                                        $status = "Day after tomorrow";
                                                        $status = date("M d,Y", strtotime($voucher["delivery_date_time"]));
                                                    } else if ($schedule == "4") {
                                                        $status = date("M d,Y", strtotime($voucher["delivery_date_time"]));
                                                    }
                                                    ?>
                                                    <div  class='recschedule' style=" background: transparent url('<?php echo BASEURL_OIMG; ?>clk.png') no-repeat scroll 0px 0px / 13px 14px;"><span style=" height: 13px; line-height: 10px; margin-left: 27px;   "><?php echo $status; ?></span></div>
                                                </div>
                                            </div>
                                        </li>
                                        <?php
                                    } else if ($voucher['is_voucher'] == 0 && $voucher['is_flower'] == 1) {
                                        $sum += $voucher["flower_purchased_price"];
                                        ?>
                                        <li style="width:100%">
                                            <div style="font-weight: 700; text-align: center;"><?php echo $voucher["flower_purchased_name"]; ?></div>
                                            <!--<abbr><?php // echo $voucher["flower_purchased_name"];                                   ?></abbr>-->
                                            <div class='scart'>
                                                <div style="overflow: hidden; padding: 10px; margin: auto; width: 65%;" >
                                                    <img src="<?php echo BASEURL_PRODUCT_IMG . $voucher["flower_purchased_image"]; ?>" style=" height:128px;"/> 
                                                </div>
                                                <!--<div class="pro_img">-->
                                                <?php
//                                                    if ($voucher["flower_purchased_image"] != '') {
//                                                        $img = BASEURL_PRODUCT_IMG . $voucher["flower_purchased_image"];
//                                                    } else {
//                                                        $img = BASEURL_OIMG . "unnamed.png";
//                                                    }
                                                ?>
                                                    <!--<img src="<?php // echo $img;                              ?>"  style=" width: 93%; height: 68px;" />-->
                                                <!--</div>-->
                                                <div >
                                                    <?php if ($voucher['flower_receiver_email'] != NULL) { ?>
                                                        <p style="text-align: center; font-weight: bold; font-size: 11px;color: white;">&nbsp;</p>                                                                                                                                   
                                                    <?php } else { ?>
                                                        <p style="text-align: center; font-weight: bold; font-size: 11px;color: white;">&nbsp;</p>                                                                                                                                           
                                                    <?php } ?>
                                                    <div class='recname' style="background: transparent url('<?php echo BASEURL_OIMG; ?>user.png') no-repeat scroll 0px 0px / 13px 14px; "><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["flower_receiver_name"]; ?></span></div>
                                                    <?php if ($voucher["flower_receiver_email"] == "") { ?>
                                                        <div class='recemail'style=" background: transparent url('<?php echo BASEURL_OIMG; ?>smm_greyb.png') no-repeat scroll 0px 0px  / 13px 14px;"><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["flower_receiver_phone"]; ?></span></div>
                                                    <?php } else { ?>
                                                        <div class='recemail'style=" background: transparent url('<?php echo BASEURL_OIMG; ?>email.png') no-repeat scroll 0px 0px  / 13px 14px;"><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["flower_receiver_email"]; ?></span></div>
                                                    <?php } ?>
                                                    <?php
                                                    $status = date("M d, Y", strtotime($voucher["flower_delivery_date"]));
                                                    ?>
                                                    <div  class='recschedule' style=" background: transparent url('<?php echo BASEURL_OIMG; ?>clk.png') no-repeat scroll 0px 0px / 13px 14px;"><span style=" height: 13px; line-height: 10px; margin-left: 27px;   "><?php echo $status; ?></span></div>
                                                </div>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<link href="<?php echo BASEURL_OCSS; ?>jquery.loader.css" rel="stylesheet" />  <!--page loading -->
<script src="<?php echo BASEURL_BJS; ?>my.loader.js"></script>
<script>
    $(document).ready(function () {
        if (typeof (Storage) != "undefined") {
            localStorage.setItem("add_another_message", "0");
        }
    });




    var count = "<?php echo $gift_count; ?>";

    if (count == "1") {

        $('.right_v ul').bxSlider({
            nextText: '',
            prevText: '',
            pager: true,
            auto: false,
            speed: 1000,
            mode: 'fade'
        });
        $(".delete").click(function () {
            var order_pro_id = $(this).attr("order_pro_id");
            $.loader({
                className: "myloader",
                content: ''
            });
            var data = {
                order_pro_id: order_pro_id,
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>voucher-delete",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        return false;
                    } else {
                        var seconds = 5;
                        setInterval(function () {
                            seconds--;
                            if (seconds == 0) {
                                $.loader('close');
                                alert(r.message.toString());
                                window.location = "<?php echo WEB_HOME; ?>";
                            }
                        }, 1000);

                    }
                }
            });
        });
    } else {
        $('.right_v ul').bxSlider({
            nextText: '',
            prevText: '',
            pager: true,
            auto: true,
            speed: 1000,
            mode: 'fade'
        });
        $(".delete").click(function () {
            var order_pro_id = $(this).attr("order_pro_id");
            $.loader({
                className: "myloader",
                content: ''
            });
            var data = {
                order_pro_id: order_pro_id,
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>voucher-delete",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        return false;
                    } else {
                        var seconds = 5;
                        setInterval(function () {
                            seconds--;
                            if (seconds == 0) {
                                $.loader('close');

                                alert(r.message.toString());

                                window.location = "<?php echo WEB_SHOW_CART; ?>";

                            }
                        }, 1000);


                    }
                }
            });
        });
    }

</script> 
<?php // $this->load->view("new_templates/freegreeting_footer");          ?>

