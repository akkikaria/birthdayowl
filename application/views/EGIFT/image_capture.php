<!--Image Capture:Start-->
<img src="" id="uploadedimg" style="display:none;" />
<input type="file" id="userfile" style="display:none" value="">

<input type="hidden" id="imgcaptureuploaded" name="imgcaptureuploaded" value="" />
<div class='imageOptions'>
    <div class="imageInnerDiv">
        <div class="closerecording">CLOSE</div>
        <div class="closeaudio">CLOSE</div>
        <div id="loader" >
            <img src="<?php echo BASEURL_OIMG . "ring.gif" ?>"    />
            <div  class="uploadingtag">Uploading Image..</div>
        </div>

        <table style="margin: auto;width: 100%;display: none"  id="previewBox">
            <tr>
                <td colspan="2" style="width:90%" align="center">
                    <div class="ImageContainer" >

                        <img src="<?php echo BASEURL_OIMG; ?>imgframe.png"  class="cornerimage" />
                        <img src="" id="previewpic"  class="previewimg">
                    </div> 
                </td>
                <td style="width:10%" align="right" >
                    <table>
                        <tr id="removeimage" style="cursor:pointer">
                            <td>
                                <img src="<?php echo BASEURL_OIMG; ?>uncheck.png" style="width:20px;"  />
                            </td>
                        </tr>
                        <tr id="uploadfinalimg" style="cursor:pointer;">
                            <td>
                                <img src="<?php echo BASEURL_OIMG; ?>check.png" style="width:20px;" />
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>
        <div class="show_camera_focus" style="height:10px;">
            <div id="webcam"></div>
        </div>
        <table style="margin: auto" id="ioptions">
            <tr class="recordchoices">
                <td colspan="2" id="select_img" style="cursor:pointer">
                    <table>
                        <tr><td><img src="<?php echo BASEURL_OIMG; ?>upload_file.png"  style="width:70px"/></td></tr>
                        <tr><td><img src="<?php echo BASEURL_OIMG; ?>upload_img.png" style="width:165px;" /></td></tr>

                    </table>
                </td>
                <td colspan="2" id="verticalLine">
                    <img src="<?php echo BASEURL_OIMG; ?>vertical_line.png" />
                </td>
                <td colspan="2" id="capture_img" style="cursor:pointer" capturepic="1"> 
                    <table>
                        <tr><td><img src="<?php echo BASEURL_OIMG; ?>photo_camera.png" style="width:70px" /></td></tr>
                        <tr><td><img src="<?php echo BASEURL_OIMG; ?>camera.png" style="width:165px;" /></td></tr>

                    </table>
                </td>
            </tr>
        </table>
        <table style="margin: auto" id="capturepicsection">
            <tr>
                <td>
                    <div id="camera_wrapper">
                        <div id="cameracurrent">
                        </div>
                        <div id="capturedimg" >
                            <img src="" id="preview_img" />
                        </div>
                        <br />
                    </div>
                </td>
            </tr>
        </table>

        <div class="btntwo">
            <div class="btnsnapshot1" id="btn1">Take Snapshot</div>
            <div class="btnsnapshot2" id="btn3">Cancel</div>
        </div>

        <div class="size_div">Max image size 5 MB.</div>
    </div>
</div>

<!--Image Capture-End-->

<script type="text/javascript">
    //image upload
    $("#icircle").click(function () {
        $(".BackgroundMedia").show();
        $(".imageOptions").show();
        $("#ioptions").show();
        $(".closerecording").show();

        $('.imageInnerDiv').animate({top: '20%'}, 500);
    });
    $("#select_img").click(function () {
        $("#userfile").click();
    });

    $(function () {
        $("#userfile").change(function () {
            var fileUpload = $(this)[0];
            var file = this.files[0];
            var filesize = this.files[0].size;
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");
            if (regex.test(fileUpload.value.toLowerCase())) {
                if (typeof (fileUpload.files) != "undefined") {
                    var reader = new FileReader();
                    reader.readAsDataURL(fileUpload.files[0]);
                    reader.onload = function (e) {
                        var image = new Image();
                        image.src = e.target.result;
                        image.name = file.name;
                        image.onload = function () {
                            if (filesize > 5000000) {
                                $("#userfile").val("");
                                alert("Please upload a smaller image, max size is 5MB");
                                return false;
                            } else {
                                $("#userfile").attr("name", "userfile");
                                $(".upload_video").attr("name", "userfile1");
                                $("#selectaudiofile").attr("name", "userfile2");
                                $("#checktype").val("1"); // image-1

                                $("#userfile_old").val("");
                                $("#previewBox").hide();
                                $(".BackgroundMedia").hide();
                                $(".imageOptions").hide();
                                $("#ioptions").hide();
                                $(".closerecording").hide();
                                $(".maincircle").hide();
                                mediaupload(1);
                            }
                        }
                    }
                } else {
                    alert("This browser does not support HTML5.");
                    return false;
                }
            } else {
                alert("Please select a valid Image file.");
                return false;
            }
        });
    });
    var i = 0;

    $("#removeimage").click(function () {
        i = 0;
        $("#previewBox").hide();
        $(".closerecording").show();
    });
    function mediaupload(type) {

<?php if ($from == 1) { ?>
            var formData = new FormData($("form[name='voucher_data']")[0]);

<?php } else { ?>
            var formData = new FormData($("form[name='greeting_data']")[0]);
<?php } ?>
        var url = "<?php echo BASEURL . 'Home_web/upload_video_new'; ?>";
        ajaxstop = $.ajax({
            url: url,
            type: "POST",
            data: formData,
            async: true,
            dataType: "json",
            beforeSend: function () {
                $(".arrowbtn").css("opacity", "0.5");
                $(".arrowbtn").css("cursor", "none");

                $(".mediauploadhide").show();
                $("#mediaprogress").show();
                $("#mediabar").width('0%');
                $("#savevideorecording").css("color", "#000000");
                $("#mediapercent").html("0%");
                uploadingstart = 1;
            },
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $("#mediabar").width(percentComplete + '%');
                        $("#mediapercent").html(percentComplete + '%');
                        $(".mpv").show();
                        if (percentComplete == 100) {
                            //  $(".mpv").html("Uploaded Successfully!");
                        }

                    }
                }, false);

                return xhr;
            },
            success: function (msg) {
                if (msg.success.toString() == "true") {
                    $(".arrowbtn").css("opacity", "1");
                    $(".arrowbtn").css("cursor", "pointer");

                    if (type == 1) {
                        $("#defaultpic").attr("src", "<?php echo BASEURL_BVMG; ?>" + msg.filename.toString());
                        $('.imageInnerDiv').animate({top: '0%'}, 500);
                        $(".remove_pic").css("display", "block");
                        $("#mediabar").width('100%');
                        $("#mediapercent").html('100%');
                        $("#mediabar").html();
                        $(".mpv").hide();
                        $(".mediauploadhide").hide();
                        $("#checktype").val("1"); // image-1
                    } else if (type == 2) {
                        $(".sampleimg").css("display", "none");
                        $(".display_video").css("display", "block");
                        $(".titlebox").hide();
                        $(".video_container").show();
                        $(".uploadingtag").css("color", "black");
                        $(".closerecording").hide();
                        $(".vdisplay_video").hide();
                        $("#videof").attr("src", "<?php echo BASEURL_BVMG; ?>" + msg.filename.toString());

                        $("#checktype").val("2"); // image-1
                        $(".mediauploadhide").css("display", "none");
                        $("#mediabar").html();

                        $(".mpv").css("display", "none");
                        $('.videoInnerDiv').animate({top: '0%'}, 500);



                    } else if (type == 3) {

                        $("#audioTime").text("00:00");
                        $("#audioTimePlayer").text("00:00");
                        $(".imgVideoContainer").hide();
                        $(".afterclick").hide();
                        $(".titlebox").hide();
                        $(".playerbox").show();
                        $("#playbox").hide();
                        $("#playaudioplayer").attr('src', "<?php echo BASEURL_BVMG; ?>" + msg.filename.toString());
                        $("#dorecording").hide();
                        $(".closerecording").show();
                        $(".recordchoices").css("width", "100%");
                        $("#select_audio").css("width", "40%");
                        $(".vertical_line").css("width", "20%");
                        $("#record_audio").css("width", "40%");
                        $(".sampleimg").hide();

                        $(".mediauploadhide").css("display", "none");
                        $("#mediabar").html();

                        $(".mpv").css("display", "none");
                        $("#checktype").val("3"); // image-1


                    }


                    $("#uploaded_finalimg").val(msg.filename.toString());




                } else {
                    $(".arrowbtn").css("opacity", "1");
                    $(".arrowbtn").css("cursor", "pointer");
                    alert("upload failed");

                }
            },
            cache: false, contentType: false, processData: false});
    }
    function ShowloadingImg() {
        i++;
        if (i == 3) {
            $("#loader").hide();
            $(".previewimg").show();
            $('#previewpic').attr('src', $("#uploadedimg").attr("src"));
            return false;
        }
        window.setTimeout("ShowloadingImg()", 1000);
    }

    $("#uploadfinalimg").click(function () {
        $("#userfile").attr("name", "userfile");
        $(".upload_video").attr("name", "userfile1");
        $("#selectaudiofile").attr("name", "userfile2");
        $("#checktype").val("1"); // image-1
        $("#userfile_old").val("");
        $("#previewBox").hide();
        $(".BackgroundMedia").hide();
        $(".imageOptions").hide();
        $("#ioptions").hide();
        $(".closerecording").hide();
        $(".maincircle").hide();
        $('.imageInnerDiv').animate({top: '0%'}, 500);
        $(".remove_pic").css("display", "block");
        $("#defaultpic").attr("src", $("#uploadedimg").attr("src"))
    });


    $('#removePhoto').on("click", function () {
        $("#uploaded_finalimg").val("");
        var data = {
            deleteimg: $("#defaultpic").attr("name")
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>Home_web/delete_uploadedimg",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {
                } else {
                }
            }
        });

        var changeimg = "<?php echo BASEURL_OIMG ?>img_before_upload.png";
        $("#defaultpic").attr("src", "" + changeimg);
        $(".remove_pic").css("display", "none");
        $("#checktype").val("0"); // no image and video
        $(".maincircle").show();

    });
    //image upload close
    //image capture-start
    //give the php file path
    webcam.set_api_url('<?php echo BASEURL; ?>Home_web/get_data');
    webcam.set_swf_url('<?php echo BASEURL_OJS; ?>webcam.swf');//flash file (SWF) file path
    webcam.set_quality(100); // Image quality (1 - 100)
    webcam.set_shutter_sound(true); // play shutter click sound

    $(document).on("click", "#capture_img", function () {
        navigator.getMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
        navigator.getMedia({video: true}, function () {
            $("#ioptions").hide();
            $("#previewBox").hide();
            $("#capturepicsection").show();
            $(".btntwo").show();
            $(".closerecording").hide();
            $(".size_div").hide();
            $("#camera_wrapper").show();
            $("#capturedimg").hide();
            $("#cameracurrent").show();
            var camera = $('#cameracurrent');
            camera.html(webcam.get_html(312, 312));
            $(".btnsnapshot1").css("color", "#52a303");
            document.getElementById('btn1').style.pointerEvents = 'auto';


        }, function () {
            alert("No Web Camera is  Detected");
        });

    });



    $(document).on("click", ".btnsnapshot1", function () {
        document.getElementById('btn1').style.pointerEvents = 'none';
        $("#capturedimg").hide();
        $("#cameracurrent").show();
        //  webcam.reset();
        webcam.snap();


    });



    webcam.set_hook('onComplete', function (img) {
        $("#loader").show();

        setTimeout(function () {
            $('#defaultpic').attr("src", "<?php echo BASEURL ?>" + img);



            $("#imgcaptureuploaded").val(img.substr(18));
            $("#previewBox").hide();
            $(".BackgroundMedia").hide();
            $(".imageOptions").hide();
            $("#ioptions").hide();
            $(".closerecording").hide();
            $(".btntwo").hide();
            $(".maincircle").hide();
            $('.imageInnerDiv').animate({top: '0%'}, 500);
            $(".remove_pic").css("display", "block");
            $("#camera_wrapper").hide();

            $("#defaultpic").attr("name", img);
            $("#uploaded_finalimg").val(img);
            $("#loader").hide();
            $("#userfile").attr("name", "userfile");
            $(".upload_video").attr("name", "userfile1");
            $("#selectaudiofile").attr("name", "userfile2");
            $("#checktype").val("1"); // image-1
            $("#userfile_old").val("");
//            webcam.reset();
            $("#cameracurrent").hide();
            $("#loader").hide();

        }, 1500);

    });


    $(".btnsnapshot2").click(function () {
        $("#ioptions").show();
        $("#capturepicsection").hide();
        $(".btntwo").hide();
        $(".closerecording").show();
        $(".size_div").show();
        $("#camera_wrapper").hide();

    });

    //image capture-end
</script>