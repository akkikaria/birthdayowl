
<!--video Recording:Start-->

<video src=""id="uploadedvideo" controls  style="display:none;"> </video>

<input type="file" id="videofile"  class="upload_video"  style="cursor:pointer;display:none">

<div class='videoOptions'>
    <div class="videoInnerDiv" id="vinnerDiv">

        <div class="closerecording">CLOSE</div>
        <div class="closeaudio">CLOSE</div>
        <div id="loadervideo" >
            <img src="<?php echo BASEURL_OIMG . "ring.gif" ?>"    />
            <div  class="uploadingtag">Uploading Video...</div>
        </div>
        <table style="margin: auto;width: 100%;display: none"  id="videopreviewBox">
            <tr>
                <td colspan="2" style="width:90%" align="center">
                    <div class="ImageContainer" >
                        <video src="" controls id="vpreview" class="vdisplay_video" > </video>
                    </div> 
                </td>
                <td style="width:10%" align="right" >
                    <table>
                        <tr id="removevideo" style="cursor:pointer">
                            <td>
                                <img src="<?php echo BASEURL_OIMG; ?>uncheck.png" style="width:20px;"  />
                            </td>
                        </tr>
                        <tr id="uploadfinalvideo" style="cursor:pointer;">
                            <td>
                                <img src="<?php echo BASEURL_OIMG; ?>check.png" style="width:20px;" />
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>
        <div class="show_camera_focus" style="height:10px;">
            <div id="webcam"></div>
        </div>
        <div style="margin: auto" id="voptions">
            <div class="recordchoices">
                <div  id="select_video" style="cursor:pointer">
                    <img src="<?php echo BASEURL_OIMG; ?>upload_file.png"  style="width:70px"/>
                    <img src="<?php echo BASEURL_OIMG; ?>upload_videotag.png" style="width:165px;" />
                </div>

                <div class="vertical_line">
                    <img src="<?php echo BASEURL_OIMG; ?>vertical_line.png" style="height:213px;" />
                </div>

                <div id="record_video" style="cursor:pointer" capturepic="1"> 
                    <img src="<?php echo BASEURL_OIMG; ?>video_camera.png" style="width:70px" />
                    <!--<tr><td><div style="background: #499905 none repeat scroll 0% 0%; height: 23px; width: 106px; border-radius: 4px; line-height: 22px;"><span style="font-size:11px; color: white;">VIDEO RECORDING</span></div></td></tr>-->
                    <img src="<?php echo BASEURL_OIMG; ?>vrecordingtag.png" style="width:165px;" />
                </div>
            </div>
        </div>
        <div style="margin: auto" id="capturevideosection">

            <div class="clock" ></div>
            <div class="videorecordingcontainer"  >
                <div id="Videowrapper">
                    <div class="dorecordimg">
                        <img src="<?php echo BASEURL_OIMG; ?>record_welcome.png"  > 
                    </div>
                    <div id="videoContainerPlayer">
                        <!-- Video -->
                        <div class="recordingvideo" id="videos-container"> 
                            <video id="video_new_player">
                            </video>
                        </div>
                    </div>   
                </div>
            </div>
            <div >
                <div id="videopreviewContainer" >
                    <input type="hidden" name="uploadedvideopreview" id="uploadedvideopreview" value="" >
                    <video id="videopre"  controls >
                        <source src="" >
                    </video>
                </div>
            </div>



        </div>
        <div class="vrecording">
            <div id="startvideorecording" >START</div>
            <div id="stopvideorecording" >STOP</div>
            <div id="savevideorecording" >UPLOAD</div>
            <div id="cancelvideorecording" >CANCEL</div>
        </div>
        <div style="width:90%;margin: auto;">

            <div id="videouploadprogress" class="videouploadhide">
                <div id="videobar"></div>
                <div id="videopercent">0%</div >

            </div>
        </div>
        <div class="upv">Uploading Video...</div>
        <div class="videosize_div" id="video_size">Max video size 60 MB.</div>

    </div>
</div>

<!--video Recording-End-->

<script>
//    alert($(window).width());

    var localStream;
    var cancel = 0;
    //video upload
    $("#ivideo").click(function () {
        $(".BackgroundMedia").show();
        $(".videoOptions").show();
        $("#voptions").show();
        $(".closerecording").show();
        $(".vrecording").hide();
        $('.videoInnerDiv').animate({top: '20%'}, 500);
    });
    $("#select_video").click(function () {
        $("#videofile").click();
    });

    $("#stop_video").on("click", function () {
        var videofile = "public/imageVideo/" + $("#videof").attr("name");

        var data = {
            deleteimg: videofile
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>Home_web/delete_uploadedimg",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {
                } else {
                }
            }
        });

        var changeimg = "<?php echo BASEURL_OIMG ?>img_before_upload.png";
        $(".sampleimg").css("display", "");
        $("#defaultpic").attr("src", "" + changeimg);
        $(".video_container").css("display", "none");
        $(".display_video").css("display", "none");
        $("#videof").attr('src', "");
        $("#checktype").val("0"); // no image and video
        $(".maincircle").show();
        $(".titlebox").show();
        $("#video_new_player").attr("src", "");
        $("#videopre").attr("src", "");
    });
    $(function () {
        $('#videofile').change(function (event) {
            var fileUpload = $(this)[0];
            var tmppath = URL.createObjectURL(event.target.files[0]);
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.mp4|.3gp|.mov|.wmv|.flv|.avi|.mkv)$");
            if (regex.test(fileUpload.value.toLowerCase())) {
                var videosize = event.target.files[0].size;
                if (videosize > 60000000) {
                    alert("Please upload a smaller video, max size is 60MB");
                    return false;
                } else {
                    $("#userfile").attr("name", "userfile1");
                    $(".upload_video").attr("name", "userfile");
                    $("#selectaudiofile").attr("name", "userfile2");
                    $("#checktype").val("2"); // video-2
                    $("#userfile_old").val("");
                    $("#videopreviewBox").hide();
                    $(".BackgroundMedia").hide();
                    $(".videoOptions").hide();
                    $("#voptions").hide();
                    $(".closerecording").hide();
                    $(".maincircle").hide();
                    mediaupload(2);
                }
            } else {
                alert("Please select valid video format");
                return false;
            }
            //                    $("#disp_tmp_path").html("Temporary Path(Copy it and try pasting it in browser address bar) --> <strong>[" + tmppath + "]</strong>");
        });

    });



    var v = 0;

    $("#removevideo").click(function () {
        v = 0;

        $("#videopreviewBox").hide();
        $("#vpreview").attr("src", "");
        $("#uploadedvideo").attr("src", "");
        $(".closerecording").show();

    });

    function ShowloadingImgVideo() {
        v++;
        if (v == 3) {
            $("#loadervideo").hide();
            $(".vdisplay_video").show();
            $('#vpreview').attr('src', $("#uploadedvideo").attr("src"));
            return false;
        }
        window.setTimeout("ShowloadingImgVideo()", 1000);
    }
    $("#uploadfinalvideo").click(function () {
        $("#userfile").attr("name", "userfile1");
        $(".upload_video").attr("name", "userfile");
        $("#selectaudiofile").attr("name", "userfile2");
        $("#checktype").val("2"); // video-2
        $("#userfile_old").val("");
        $("#videopreviewBox").hide();
        $(".BackgroundMedia").hide();
        $(".videoOptions").hide();
        $("#voptions").hide();
        $(".closerecording").hide();
        $(".maincircle").hide();
        $('.videoInnerDiv').animate({top: '0%'}, 500);
        $("#videof").attr("src", $("#vpreview").attr("src"));
        $(".sampleimg").css("display", "none");
        $(".display_video").css("display", "block");
        $(".titlebox").hide();
        $(".video_container").show();
    });
    var vi = 0, au = 0;
    var ajaxstop, uploadingstart = 0;
    var previewElement = document.getElementById('videopre');
    var clock, countup;
    clock = $('.clock').FlipClock(0, {
        clockFace: 'MinuteCounter',
        countdown: false,
        autoStart: false,
        callbacks: {
            start: function () {
                setTimeout(function () {
                    $("#stopvideorecording").css("color", "#52A303");
                    document.getElementById('stopvideorecording').style.pointerEvents = 'auto';
                    document.getElementById('startvideorecording').style.pointerEvents = 'none';
                    document.getElementById('savevideorecording').style.pointerEvents = 'none';
                    $("#startvideorecording").css("color", "#000000");
                    $("#savevideorecording").css("color", "#000000");
                    uploadingstart = 0;
                }, 4000);

            },
            stop: function () {

                console.log("val", cancel);

                $("#videos-container").hide();

                if (cancel == 1) {
                    $("#videopreviewContainer").show();

                    cancel = 0;
                    $(".clock").hide();
                    $("#vinnerDiv").removeClass("videoInnerDivnew").addClass("videoInnerDiv");
                    $(".videorecordingcontainer").hide();
                }

//                if (cancel == 0) {
//                    $("#video_size").html("Max video size 60 MB.");
//                    $(".recordingvideo").css("display", "none");
//                    $(".clock").hide();
//                    $(".videoOptions").show();
//                    $("#voptions").show();
//                    $(".closerecording").show();
//                    $(".vrecording").hide();
//                    $("#videos-container").hide();
//                    $("#videopreviewContainer").css("display", "none");
//
//                    $(".recordingvideo").css("display", "none");
//                    $("#video_new_player").hide().remove();
//                    $("#videopreviewContainer").hide();
//                    $(".capturevideosection").hide();
//                    $('.videoInnerDiv').animate({top: '20%'}, 500);
//                    $("#vinnerDiv").removeClass("videoInnerDivnew").addClass("videoInnerDiv");
//                    $(".dorecordimg").hide();
//                    $(".videorecordingcontainer").hide();
////                    $(".clock").hide();
////                    $(".videoOptions").show();
////                    $("#voptions").show();
////                    $(".closerecording").show();
////                    $(".vrecording").hide();
////                    $("#videos-container").hide();
////                    $(".recordingvideo").hide();
////                    $("#video_new_player").hide().remove();
////                    $("#videopreviewContainer").hide();
////                    $(".capturevideosection").hide();
////                    $('.videoInnerDiv').animate({top: '20%'}, 500);
////                    $("#vinnerDiv").removeClass("videoInnerDivnew").addClass("videoInnerDiv");
////                    $(".dorecordimg").hide();
////                    $(".videorecordingcontainer").hide();
//                }
                if (cancel == 2) {
                    $("#videopreviewContainer").show();
                    cancel = 0;
                    $(".clock").hide();
                    $("#vinnerDiv").removeClass("videoInnerDivnew").addClass("videoInnerDiv");
                    $(".videorecordingcontainer").hide();

                    alert("Recording limit is exceeded.");
                    window.audioVideoRecorder.stopRecording(function (url) {
                        previewElement.src = url;
                        previewElement.muted = false;
                        previewElement.onended = function () {
                            previewElement.pause();
                            // dirty workaround for: "firefox seems unable to playback"
                            previewElement.src = URL.createObjectURL(audioVideoRecorder.getBlob());
                            //  localStream.stop();
                        };
                    });


                }
                $("#stopvideorecording").css("color", "#000000");
                document.getElementById('stopvideorecording').style.pointerEvents = 'none';
                document.getElementById('startvideorecording').style.pointerEvents = 'none';
                document.getElementById('savevideorecording').style.pointerEvents = 'auto';
//                $("#startvideorecording").css("color", "#52A303");
                $("#savevideorecording").css("color", "#52A303");
//              

            },
        },
    });



    countup = setInterval(function () {
        if (clock.getTime().time > 194) {
            cancel = 2;
            clock.stop();
            clock.reset();

        }
    }, 500);
    $(document).on("click", "#stopvideorecording", function () {

        cancel = 1;
        $("#videopreviewContainer").show();
        $("#videos-container").hide();
        clock.stop();
        clock.reset();
        $("#stopvideorecording").css("color", "#000000");
        document.getElementById('stopvideorecording').style.pointerEvents = 'none';
        document.getElementById('startvideorecording').style.pointerEvents = 'none';
        document.getElementById('savevideorecording').style.pointerEvents = 'auto';
//        $("#startvideorecording").css("color", "#52A303");
        $("#savevideorecording").css("color", "#52A303");
        uploadingstart = 0;
        window.audioVideoRecorder.stopRecording(function (url) {

            previewElement.src = url;
            previewElement.muted = false;
            //  previewElement.play();
            previewElement.onended = function () {
                previewElement.pause();
                // dirty workaround for: "firefox seems unable to playback"
                previewElement.src = URL.createObjectURL(audioVideoRecorder.getBlob());
            };
        });
//                                mediaRecorder.stop();
//                                mediaRecorder.stream.getVideoTracks()[0].stop();
//                                mediaRecorder.save();

    });
    $(document).on("click", "#savevideorecording", function () {
        cancel = 1;
        $(".clock").hide();
        $(".dorecordimg").hide();
        $("#savevideorecording").css("color", "#52A303");
        $("#savevideorecording").css("color", "#52A303");
        document.getElementById('stopvideorecording').style.pointerEvents = 'none';
        document.getElementById('startvideorecording').style.pointerEvents = 'none';
        document.getElementById('savevideorecording').style.pointerEvents = 'none';
        $("#startvideorecording").css("color", "#000000");
        $("#stopvideorecording").css("color", "#000000");

        var fileExtension = ('video/mp4').split('/')[1];
        var fileFullName = ((Math.round(Math.random() * 9999999999) + 888888888)) + '.' + fileExtension;
        var formData = new FormData();
        formData.append('userfile', fileFullName);
        formData.append('videofile', audioVideoRecorder.getBlob());
        var url = "<?php echo BASEURL . 'Home_web/upload_video'; ?>";
        ajaxstop = $.ajax({
            url: url,
            type: "POST",
            data: formData,
            async: true,
            dataType: "json",
            beforeSend: function () {
                $(".videouploadhide").show();
                $("#videouploadprogress").show();
                $("#videobar").width('0%');

                $("#savevideorecording").css("color", "#000000");
                $("#videopercent").html("0%");

            },
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $("#videobar").width(percentComplete + '%');
                        $("#videopercent").html(percentComplete + '%');
                        $(".upv").show();
                        if (percentComplete == 100) {
                            $(".upv").html("Video Uploaded Successfully!");
                        }

                    }
                }, false);

                return xhr;
            },
            success: function (msg) {
                if (msg.success.toString() == "true") {
                    $("#uploaded_finalimg").val(msg.filename.toString());
                    $("#videobar").width('100%');
                    $("#videopercent").html('100%');
                    $(".upv").html("Uploading Video...");
                    $(".upv").hide();
                    $(".videouploadhide").hide();
                    $("#video_size").html("Max video size 60 MB.");

                    var videofile = "../../public/imageVideo/" + msg.filename.toString();
                    $("#imgcaptureuploaded").val(msg.filename.toString());

                    $(".BackgroundMedia").hide();
                    $(".videoOptions").hide();
                    $("#voptions").hide();
                    $("#capturevideosection").hide();
                    $(".closerecording").hide();
                    $('.videoInnerDiv').animate({top: '0%'}, 500);
                    $("#userfile").attr("name", "userfile1");
                    $(".upload_video").attr("name", "userfile");
                    $("#selectaudiofile").attr("name", "userfile2");
                    $("#checktype").val("2"); // video-2
                    $("#userfile_old").val("");
                    $(".maincircle").hide();
                    $("#videof").attr("src", videofile);
                    $("#videof").attr("name", msg.filename.toString());
                    $(".sampleimg").css("display", "none");
                    $(".display_video").css("display", "block");
                    $(".titlebox").hide();
                    $(".video_container").show();
                    $("#vinnerDiv").removeClass("videoInnerDivnew").addClass("videoInnerDiv");
                    $(".dorecordimg").hide();
                    localStream.stop();
                } else {
                }
            },
            cache: false, contentType: false, processData: false});



    });


//
//    function xhr(url, data, callback) {
//        var request = new XMLHttpRequest();
//        request.onreadystatechange = function () {
//            if (request.readyState == 4 && request.status == 200) {
//                callback(request.responseText);
//            }
//        };
//        request.open('POST', url);
//        request.send(data);
//    }
    $(document).on("click", "#cancelvideorecording", function () {
        cancel = 1

        $("#video_size").html("Max video size 60 MB.");
        $(".recordingvideo").css("display", "none");
        $(".clock").hide();
        $(".videoOptions").show();
        $("#voptions").show();
        $("#videopreviewContainer").css("display", "none");
        $(".closerecording").show();
        $(".vrecording").hide();
        $("#videos-container").hide();
        $(".recordingvideo").css("display", "none");
        $("#video_new_player").hide().remove();
        $("#videopreviewContainer").hide();
        $(".capturevideosection").hide();
        $('.videoInnerDiv').animate({top: '20%'}, 500);
        $("#vinnerDiv").removeClass("videoInnerDivnew").addClass("videoInnerDiv");
        $(".dorecordimg").hide();
        $(".videorecordingcontainer").hide();

        $(".upv").hide();
        $(".videouploadhide").hide();
        if (uploadingstart == 1) {
            ajaxstop.abort();
            uploadingstart = 0;
        }
        localStream.stop();
        clock.stop();
        clock.reset();
//        if (uploadingstart == 1) {
//     
//            var videofile = "public/imageVideo/" + $("#videof").attr("name");
//
//            var data = {
//                deleteimg: videofile
//            }
//            $.ajax({
//                type: "POST",
//                url: "<?php //echo BASEURL;                                     ?>Home_web/delete_uploadedimg",
//                data: data,
//                dataType: "json",
//                success: function (r) {
//                    if (r.success.toString() === "false") {
//                    } else {
//                    }
//                }
//            });
//        }




    });
    $("#record_video").click(function () {
        $("#videopreviewBox").hide();
        $(".closerecording").hide();
        $(".videorecordingcontainer").show();
        $("#vpreview").attr("src", "");
        $("#uploadedvideo").attr("src", "");
        $("#voptions").hide();
        $(".videosize_div").show();
        $("#videos-container").show();
        $("#video_size").html("Max video size 60 MB.");
        $("#vinnerDiv").removeClass("videoInnerDiv").addClass("videoInnerDivnew");
        $(".videoInnerDivnew").show();
        $(".dorecordimg").show();
        var videop = document.createElement('video');
        videop.controls = false;
        videop.id = "video_new_player";
        videop.muted = true;
        videop.controls = false;
        $("#videos-container").html(videop);
        $("#videopreviewContainer").hide();
        $("#capturevideosection").show();
        $(".vrecording").show();
        $("#startvideorecording").css("color", "#52A303");
        $("#stopvideorecording").css("color", "#000000");
        $("#savevideorecording").css("color", "#000000");
        document.getElementById('startvideorecording').style.pointerEvents = 'auto';
        document.getElementById('stopvideorecording').style.pointerEvents = 'none';
        document.getElementById('savevideorecording').style.pointerEvents = 'none';
    });




    $("#startvideorecording").click(function () {
        captureUserMedia00(function (stream) {
            window.audioVideoRecorder = window.RecordRTC(stream, {
                type: 'video'
            });
            window.audioVideoRecorder.startRecording();
        });

    });
    function captureUserMedia00(callback) {
        captureUserMedia({
            audio: true,
            video: true
        }, function (stream) {
            var videoElement = document.getElementById('video_new_player');
            videoElement.src = URL.createObjectURL(stream);
            videoElement.muted = true;
            videoElement.controls = false;
            videoElement.play();
            videoElement.addEventListener("canplay", function () {
                videoElement.currentTime = 3;
            });
            localStream = stream;
            $(".dorecordimg").hide();
            $(".clock").show();
            $("#videopreviewContainer").hide();
            $("#videos-container").show();
            clock.start();
            callback(stream);

        }, function (error) {
            alert("No live input found or Please enable media permissions");
            $("#videopreviewBox").show();
            $(".closerecording").show();
            $("#vpreview").attr("src", "");
            $("#uploadedvideo").attr("src", "");
            $("#voptions").show();
            $(".videosize_div").show();
            $("#videos-container").hide();
            $("#videopreviewContainer").show();
            $("#capturevideosection").hide();
            $("#videopreviewBox").hide();
            $(".vrecording").hide();
            $("#startvideorecording").css("color", "#52A303");
            $("#vinnerDiv").removeClass("videoInnerDivnew").addClass("videoInnerDiv");
            $(".dorecordimg").hide();
        });
    }



    function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
        navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
    }





</script>

