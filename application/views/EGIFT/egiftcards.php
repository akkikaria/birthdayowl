<?php $this->load->view("new_templates/header"); ?>
<style>
    .sortinfo{
        padding: 60px;
        text-align: justify;
        font-family:"gotham-book";
        font-size:15px;
    }
    .owl_image{
        float:right;
        height: 120px;
    }
    .pro{
        padding: 0px 5px;
        margin-bottom:4%;
        cursor: pointer;
    }
    .back_tray_img{
        height: 50px; 
        background-image: url('');
        background-position: center bottom -5px;
        background-repeat: no-repeat;
    }

    .product-img{
        margin-bottom: -10px !important;
        /*        position: relative;
                background: #ffffff;
                z-index: 2;
                display: inline-block;*/
    }

    .product-base{
        display: inline-block;
        margin-top: -15px;
        position: relative;
        z-index: 1;
        border-radius: 0;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        -ms-border-radius: 0px;
        -o-border-radius: 0px;
    }
    .voucher_name{
        /*margin: 0;*/
    }
    .img-mid{
        width: 220px;
        border: 1px solid; 
        border-radius: 5px; 
        height: 132.8px !important;
    }

    .products li i{background-size:100%;padding-bottom:17%;}

    .products li {
        padding: 5px;
        float: none;
        width: 24.33%;
        text-align: center;
        border-right: 0px solid #bababa;
        height: 280px;
        min-height: 280px;
        margin-bottom: 0;
        border: 0px solid #ddd !important;
        vertical-align: middle;
        display: inline-block;
    }

    .products li i{
        background: none;
        padding-bottom: 0px;
    }

    .products li a {
        text-align: center;
        margin: auto;
        margin-top: 20px;
        border: 2px solid rgba(221, 221, 221, 0.5);
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        align-items: center;
        padding: 20px 5px;
    }

    .products li span {
        font-size: 15px;
        min-height: 48px;
        color: #5CAD01;
        font-family: GothamMedium;
    }

    .product-title{
        margin-bottom: 2em;
    }

    .egift-products li img{
        border-radius: 10px;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        -ms-border-radius: 10px;
        -o-border-radius: 10px;
    }

    @media only screen and (max-width: 991px){

        .product-title{
            margin-bottom: 0;
        }

        .products li{
            width: 50%;
            float: left;
            height: 260px;
            min-height: 260px;
        }

        .products li a{
            margin-top: 10px;
            padding: 10px 5px;
        }

        .products li span{
            min-height: 40px;
        }

    }

    @media only screen and (max-width: 767px){

        .products li{
            padding: 0 5px;
            height: auto;
            min-height: auto;
        }

        .products li span{
            font-size: 14px;
            margin-top: -10px;
            margin-bottom: 0;
        }
        .sortinfo{
            padding: 10px;
            text-align: justify;
            font-family:"gotham-book";
            font-size:15px;
        }

    }

</style>
<div class="container-fluid">
    <div class="row">
<!--        <div class="col-md-12 col-xs-12 no-padd inner-top-slider">
            <img src="<?php echo BASEURL_ONEWIMG ?>birthday_gift_cards.jpg" class="img-responsive" alt="birthday gift cards online">
        </div>-->
    </div>
</div>


<div class="container-fluid product-title">
    <div class="row">
        <div class="col-md-12 col-xs-12 pro-page-title text-center">
            <div class="owl-text">    
                <h1>eGift <span>Cards</span></h1>

            </div>
        </div>
        <!--        <div class="col-md-4 text-right no-padd hidden-xs hidden-sm">
                    <img src="<?php echo BASEURL_ONEWIMG ?>owl.png" class="img-responsive owl_image">
                </div>-->
    </div>
</div>
<div class="container">
    <div class="row" >
        <ul class="products egift-products">
            <?php
            if (count($product_voucher) > 0) {
                for ($i = 0; $i < count($product_voucher); $i++) {
                    ?>
                    <li style="cursor:pointer; border-right: 0;" class="col-xs-6 col-sm-4 col-md-3">
                        <a class="showPopup" voucher_pro_id="<?php echo $product_voucher[$i]["voucher_pro_id"]; ?>" data-width="60%" >


                            <?php
                            if ($product_voucher[$i]["product_image"] != '')
                                $img = $product_voucher[$i]["product_image"];
                            else {
                                $img = BASEURL_OIMG . "unnamed.png";
                            }
                            ?>
                            <img src="<?php echo $img; ?>" alt="<?php echo $product_voucher[$i]["voucher_pro_name"]; ?>" class="img-responsive product-img"/>
                            <div class="clearfix"></div>
                            <img src="<?php echo BASEURL_OIMG . "tray.png"; ?>" alt="tray" class="img-responsive product-base">

                            <div class="clearfix"></div>
                            <span><?php echo $product_voucher[$i]["voucher_pro_name"]; ?></span>

                            <div class="clearfix"></div>

                        </a>
                    </li>
                    <?php
                }
            } else {
                ?>
                <!--<li><div>No Products</div></li>-->
                <img src="<?php echo BASEURL . "public/coming_soon/background.jpg"; ?>" alt="tray" class="img-responsive product-base"> 
            <?php } ?>
        </ul>

        <?php
//        if (count($product_voucher) > 0) {
//            for ($i = 0; $i < count($product_voucher); $i++) {
        ?>
<!--                <div style="" class="col-md-3 col-xs-6 text-center showPopup pro" voucher_pro_id="<?php // echo $product_voucher[$i]['voucher_pro_id'];                                                          ?>" >
                    <div class=""  style="height: 100px;">
        <?php
//                    if ($product_voucher[$i]["product_image"] != '') {
//                        $img = $product_voucher[$i]["product_image"];
//                    } else {
//                        $img = BASEURL_OIMG . "unnamed.png";
//                    }
        ?>
                    <img alt="<?php // echo $product_voucher[$i]["voucher_pro_name"];                                                          ?>" src="<?php echo $img; ?>" class="img-responsive img-mid">
                    <div class="back_tray_img">
                         <img src="<?php // echo BASEURL_OIMG . "tray.png";                                                                                                                                                                                                                                                                                                                                                                 ?>" class="img-responsive img-mid" >
                    </div>
                    </div>
                    <h5 class="voucher_name"><?php // echo $product_voucher[$i]["voucher_pro_name"];                                                          ?></h5>
                </div>-->
        <?php
//            }
//        } else {
        ?>
        <!--            <div class="col-md-3 col-xs-6 text-center card">
                        No Products
                    </div>-->
        <?php // } ?>
        <div class="col-md-12" style="text-align: center; margin: 20px auto;">
            <div  class="col-md-4 col-xs-12" style="margin: 5px auto; padding: 5px;">
                <!--<img src="<?php // echo BASEURL_ONEWIMG;                       ?>Zodiac-Sign_1.png" alt=""/>-->
                <img style="cursor: pointer;" id="flower_cake_img" src="<?php echo BASEURL_ONEWIMG; ?>flowers-&-cake.png" alt="birthday gift cards online"/>
            </div>
            <div  class="col-md-4 col-xs-12" style="margin: 5px auto; padding: 5px;">
                <!--<img src="<?php // echo BASEURL_ONEWIMG;                       ?>Zodiac-Sign_1.png" alt=""/>-->
                <img style="cursor: pointer;" id="greeting_card_img"src="<?php echo BASEURL_ONEWIMG; ?>digital_gift_cards.png" alt="digital gift cards"/>
            </div>
            <div  class="col-md-4 col-xs-12" style="margin: 5px auto; padding: 5px;">
                <img style="cursor: pointer;" id="zodiac_sign_img" src="<?php echo BASEURL_ONEWIMG; ?>gift_cards_for_friends.png" alt="gift cards for friends"/>
            </div>

        </div>
    </div>

</div>
<div class="clearfix"></div>
<div class="container-fluid sortinfo">
    <h2 style="font-size: 22px;text-align: center;font-weight: 600;" >Digital Birthday eGift Cards For Friends</h2>
    <h2 style="font-size: 22px;">eGift Voucher</h2> 
    <p> Why settle for ordinary gift options when you can have some of the very best gift options at your fingertips?</p>

    <p>Choose from a wide range of <a href="https://www.birthdayowl.com"><b>Free Birthday Greeting Cards</b></a> which are instantly redeemable by your recipient at their own time and convenience. If you are not sure what they may like to receive, then simply gift them a store or restaurant or even a shopping website or holiday voucher – so that you give them the freedom to choose what they want, and whenever they want to avail it.</p>

    <p>If you prefer something more tangible where they can touch and feel, go on and choose from our nice collection of <a href="https://www.birthdayowl.com/happy-birthday-beautiful-flower-cake"><b>Happy Birthday Beautiful Flower Cake</b></a> and more. We aim to please and it will show on your recipient’s face when they receive our gifts!</p>
    <p>Besides, our wide selection of free birthday greeting cards are designed appropriately where you also have the option of inputting your own choice of text to make it even more personal. We deliver your birthday greeting card on your scheduled date and time by email and also send a text notification to your recipient’s mobile phone that your e-greeting card is waiting in their inbox. </p>

    <h2 style="font-size: 22px;">Digital Gift Cards</h2> 
    <p>The world is turning digital and gifting is no exception. People from around the world now prefer gifting <b>eGift Voucher</b> in the form of gift cards which makes it so much easier for the recipient to choose and redeem as they please. And it also offers a large variety of options now available at your fingertips. Birthdayowl™ offers free reminders and also a choice of digital gift cards for you.</p>

    <p>Choose from a wide range of gift cards which are instantly redeemable by your recipient at their own time and convenience. If you are not sure what they may like to receive, then simply gift them a store or restaurant or even a shopping website or holiday voucher – so that you give them the freedom to choose what they want, and whenever they want to avail it.</p>

    <p>If you prefer something more physical, go on and choose from our nice collection of flowers, cakes and more. The happiness will show on your recipient’s face when they receive our gifts!</p>


    <p>Besides, our wide selection of free birthday greeting cards are designed appropriately where you also have the option of inputting your own choice of text to make it even more personal. We deliver your birthday greeting card on your scheduled date and time by email and also send a text notification to your recipient’s mobile phone that your e-greeting card is waiting in their inbox. </p>

    <h2 style="font-size: 22px;">Gift Cards For Friends</h2> 
    <p>Our friends really matter to us, so do something nice to make their birthday special. Make it personal with all the awesome features we have in store for you.</p>

    <p>Choose from a wide range of <a href="https://www.birthdayowl.com"><b>Online Gift Cards</b></a> which are instantly redeemable by your recipient at their own time and convenience. If you are not sure what specifically they may like to receive, then simply gift them a store or restaurant or even a shopping website or holiday voucher – so that you give them the freedom to choose what they want, and whenever they want to avail it.</p>

    <p>You can thereafter proceed to choose from hundreds of options of free e-greeting cards, gifts, flowers, cakes and other cool stuff. Once you’ve made your choices, simply send them across and your recipient is sure to be impressed and smiling. We take the stress out of gifting as we provide you with instantly deliverable e-gift cards of the best brands, as well as options of actual gifts being delivered to your recipient’s door.</p>


    <p>If you prefer something more tangible where they can touch and feel, go on and choose from our nice collection of flowers, cakes and more. We aim to please and it will show on your recipient’s face when they receive our gifts!</p>


</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script>
    $('#greeting_card_img').click(function () {
        window.location = "<?php echo WEB_FREE_GREETING_CARDS; ?>";
    });
    $('#flower_cake_img').click(function () {
        window.location = "<?php echo WEB_FLOWERS_AND_CAKES; ?>";
    });
    $('#zodiac_sign_img').click(function () {
        window.location = "<?php echo WEB_ZODIAC_SIGNS; ?>";
    });
    $(".showPopup").click(function () {
        $("body").css("overflow", "auto");
        if (typeof (Storage) != "undefined") {
            localStorage.setItem("add_another_message", "0");
        }
        var voucher_pro_id = $(this).attr("voucher_pro_id");
        var data = {
            voucher_pro_id: voucher_pro_id
        }

        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>voucher-data",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() == "false") {
                    alert(r.message.toString());
                    return false;
                } else {
                    var voucher_url = r.product_data["voucher_url"];
                    var voucher_pro_name = r.product_data["voucher_pro_name"];
                    var redemption_details = r.product_data["terms_conditions"];
                    var vdescription = r.product_data["vdescription"];
                    var product_image;
                    if (r.product_data["product_image"] != "") {
                        product_image = r.product_data["product_image"];
                    } else {
                        product_image = "<?php echo BASEURL_OIMG . "unnamed.png"; ?>";
                    }

                    $(".pro_name").text(voucher_pro_name);
                    $(".minamt").text(" " + r.product_data["min_custom_price"] + " onwards");
                    $("#pro_img").attr("src", product_image);
                    $(".selected_card").attr("voucher_url", voucher_url);
                    var data1 = '';
                    data1 += '<div>' + redemption_details + '</div>';
                    $("#redemption_data").html(data1);
                    var data2 = '';
                    data2 += '<div>' + vdescription + '</div>';
                    $("#location_data").html(data2);
                    $.colorbox({
                        width: "50%",
                        height: "550px",
                        inline: true,
                        href: "#pickcard"
                    });
                    if ($(window).width() < 1280) {
                        $.colorbox({
                            width: "60%",
                            height: "500px",
                            inline: true,
                            href: "#pickcard"
                        });
                    }

                    if ($(window).width() < 480) {
                        $.colorbox({
                            width: "90%",
                            height: "90%",
                            inline: true,
                            href: "#pickcard"
                        });
                    }

                }
            },
        });
    });
</script>

