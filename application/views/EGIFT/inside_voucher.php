<?php $this->load->view("new_templates/header_inside"); ?>
<?php
//echo '<pre>';
//print_r($this->session->all_userdata());
//exit;
?>
<link href="<?PHP echo BASEURL_OCSS ?>dcalendar.picker.css" rel="stylesheet" type="text/css"/>
<link href="<?PHP echo BASEURL_OCSS ?>picktim.css" rel="stylesheet" type="text/css"/>

<script src="<?PHP echo BASEURL_OJS ?>dcalendar.picker.js" type="text/javascript"></script>
<script src="<?PHP echo BASEURL_OJS ?>picktim.js" type="text/javascript"></script>
<style type="text/css">
    #show_greetings{
        display: none;
    }

    .ierror{
        color: red; display: block; padding-left: 52px; font-style: italic;
    }
</style>
<div class="showloader" style="display:none">
    <div class="loader_div">
        <img src="<?php echo BASEURL_OIMG ?>89.gif" alt="Loading" class="ajax-loader " />
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#show_greetings').show();
        $('#show_greetings').css("display", "block");
    });</script>
    
<script type="text/javascript" src="<?php echo BASEURL_OJS; ?>webcam.js"></script>
<script src="<?php echo BASEURL_OJS; ?>RecordRTC.js"></script>
<script src="<?php echo BASEURL_OJS; ?>gumadapter.js"></script>
<link rel="stylesheet" href="<?php echo BASEURL_OCSS; ?>flipclock.css" />
<script src="<?php echo BASEURL_OJS; ?>flipclock.js"></script>
<script type="text/javascript" src="<?php echo BASEURL_OJS ?>mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="<?php echo BASEURL_OCSS; ?>videostyle.css" />
<link rel="stylesheet" href="<?php echo BASEURL_DATE; ?>metallic.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo BASEURL_OCSS ?>pstyles.css"/>
<!--Banner -->
<form  method="post" id="voucher_submit" name="voucher_data"  enctype="multipart/form-data" >
    <div class="wrapper">
        <div class="container_12 text-center">
            <div class="heading_2">
                Personalize <span>Gift Card</span>
            </div>
        </div>
    </div>
    <div class="wrapper">
        <div class="container_12">
            <div class="inner_midd">
                <div class="in_f_l">
                    <ul class="leftside leftside_nav">
                        <li class="active" id="one"><a href="#pick_amount">Pick Amount</a></li>
                        <li id="two"><a href="#make_it_personal">Make It Personal</a></li>
                        <li id="threediv"><a href="#choose_delivery">Choose Delivery</a></li>
                        <li id="fourdiv"><a href="#checkout">CHECKOUT</a></li>
                    </ul>
                </div>

                <div class="in_f_r" >
                    <div class="wrapper bor_sep" id="pick_amount">
                        <div class="in_heading" data-count="1" id="cone">
                            <span>PICK A AMOUNT : How much would you like to give?</span>
                            <abbr id="derror">Enter Denomination </abbr>
                        </div>

                        <ul class="priceselection" id="price_selection">
                            <?php $no = 1 ?>
                            <?php foreach ($amounts as $amt) { ?>
                                <li>
                                    <button type="button"  id='amt_id<?php echo $amt; ?>'index="<?php echo $no; ?>" class="av_amount" voucher_id="<?php echo $voucher_id; ?>" value="<?php echo $amt; ?>" >RS.<?php echo $amt; ?></button>
                                </li>

                                <?php
                                $no++;
                            }
                            ?>
                        </ul>
                        <div id="PriceValidation"></div>
                        <input type="hidden" id='quantity_new' name="quantity_new" value="<?php echo $ordered_data["quantity"]; ?>" >
                        <div style=" padding-left: 51px;padding-top:22px;display:none;" >
                            <div style="text-align:center;margin: auto; display: inline-block;      padding-top: 14px;">Quantity:</div>
                            <div style="text-align:center;margin: auto;display: inline-block">
                                <select id="quantity"  name="quantity" >
                                    <option selected="selected" value="1" >1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>  
                            </div>
                        </div>
                    </div>
                    <div class="wrapper " id="make_it_personal">
                        <div class="in_heading" data-count="2" id="ctwo">
                            <span>Make It Personal : Design Your Greeting</span>
                            <abbr>Choose Your “BIRTHDAY” Design Below</abbr>
                        </div>


                        <div class="slider_g wrapper"id="show_greetings" >
                            <span id="prdnext"></span><span id="prdprv"></span>
                            <ul id="greeting_card" >
                                <?php
                                $i = 0;
//                                echo '<pre>';
//                                print_r($greeting_cards);
//                                exit;
                                foreach ($greeting_cards as $cards) {
                                    ?>
                                    <?php if ($i == 0) { ?>
                                        <li data-image="<?php echo $cards["front_page_image"]; ?>"  id="<?php echo "image" . $cards["card_id"]; ?> " class="active currentact" card_id="<?php echo $cards["card_id"]; ?>" >
                                            <div  class="activediv" >
                                                <img src="<?php echo BASEURL_CROPPED_GREET . $cards["front_page_image"]; ?>"  onclick="get_card_id(<?php echo $cards["card_id"]; ?>)" id="greet_id" card_id="<?php echo $cards["card_id"]; ?>" class="change_img" />
                                            </div>
                                        </li>      
                                    <?php } else { ?>
                                        <li data-image="<?php echo $cards["front_page_image"]; ?>" id="<?php echo "image" . $cards["card_id"]; ?> "card_id="<?php echo $cards["card_id"]; ?>" >
                                            <div  class="activediv" >
                                                <img  src="<?php echo BASEURL_CROPPED_GREET . $cards["front_page_image"]; ?>" onclick="get_card_id(<?php echo $cards["card_id"]; ?>)" id="greet_id" card_id="<?php echo $cards["card_id"]; ?>"  class="change_img"   />
                                            </div>
                                        </li>

                                        <?php
                                    }
                                    $i++;
                                }
                                ?>

                            </ul> 

                        </div>
                    </div>
                    <div class="wrapper bor_sep">
                        <div class="greeting_full">
                            <div class="g_left">
                                <img id="largeImage"  greeting_id="<?php echo $greeting_cards[0]["card_id"]; ?>" src=" <?php echo BASEURL_CROPPED_GREET; ?>a.jpg" imgsrc="<?php echo BASEURL_CROPPED_GREET . $greeting_cards[0]["front_page_image"]; ?>" img_path="<?php echo BASEURL_CROPPED_GREET ?>" />
                                <input type="hidden" name="greeting_id" value="" id="set_greeting_id"/>
                                <input type="hidden" name="selected_amount" value=""  id="setamount"/>
                            </div>
                            <div class="g_right">
                                <div class="r_con scroll_issue">
                                    <span>Add a personal message</span>
                                    <abbr>Write your message here which <br />
                                        appear on ecard inside page</abbr>

                                    <textarea rows="5" id="htmlContent" name="greeting_message" maxlength="240" style="text-align:center"><?php echo $ordered_data["greeting_message"]; ?></textarea>
                                    <div id="messgecount" class="message_count"></div>
                                    <a>
                                        <div class="photoupload">
                                            <img src="<?php echo BASEURL_OIMG ?>frame.png" class="backcorners"/>

                                            <div class="photo_img">
                                                <img src="<?php echo BASEURL_OIMG ?>img_before_upload.png" id="defaultpic" class="sampleimg" >
                                                <div style="width:80%;margin: auto;">
                                                    <div id="mediaprogress" class="mediauploadhide">
                                                        <div id="mediabar"></div>
                                                        <div id="mediapercent">0%</div >
                                                    </div>
                                                </div>
                                                <div class="mpv">Uploading...</div>
                                                <div class="playerbox" >
                                                    <div class="preview_box">
                                                        <div class="playaudioContainer" >
                                                            <div class="audioControlPlayer audioPlayer" rel="play" id="playit"></div>
                                                            <audio id="playaudioplayer"  preload="auto" class="a" >
                                                                <source src="" >
                                                                Your browser does not support the audio element.
                                                            </audio>
                                                        </div> 
                                                    </div>
                                                    <div  class="stopPlayer">Remove Audio</div>
                                                </div>

                                                <div class="video_container">
                                                    <!--                                                    <div  class="stopVideo" id="stop_video">X</div>-->
                                                    <video src="" controls id="videof" class="display_video" type="video/*">
                                                        Your browser does not support the <code>video</code> element.
                                                    </video>
                                                    <div  class="stopVideo" id="stop_video">Remove Video</div>
                                                </div>
                                                <div class="remove_pic" id="removePhoto">Remove Photo</div>
                                                <div class="maincircle">
                                                    <img src="<?php echo BASEURL_OIMG ?>image.png" id="icircle" class="icircle"/>    
                                                    <img src="<?php echo BASEURL_OIMG ?>video.png" id="ivideo" class="icircle" />
                                                    <img src="<?php echo BASEURL_OIMG ?>audio.png" id="iaudio" class="icircle" />
                                                </div>

                                            </div>

                                        </div>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Image capture:start-->
                    <?php
                    $data['from'] = 1;
                    $this->load->view("EGIFT/image_capture", $data);
                    ?>
                    <!--Image capture:End-->
                    <!--audio Recording:start-->
                    <?php $this->load->view("EGIFT/audio_recording"); ?>
                    <!--Recording:End-->
                    <!--video-recording:start-->
                    <?php $this->load->view("EGIFT/video_recording"); ?>
                    <div class="wrapper" id="choose_delivery">
                        <div class="in_heading" data-count="3" id="cthree">
                            <span>Choose Delivery: How would you like to deliver this e-Gift Cards?</span>
                        </div>
                        <div id="EmailValidation"></div>

                        <ul class="by_de wrapper">
                            <li style="padding: 0;"><a href="#"><img src="<?PHP echo BASEURL_OIMG ?>envo.png" /></a></li>
                            <li style="padding: 0;"><a href="#"><img src="<?PHP echo BASEURL_OIMG ?>sms.png" /></a></li>
                        </ul>
                        <ul class="by_de_selection wrapper bor_sep li3">
                            <li><img src="<?PHP echo BASEURL_OIMG ?>envo.png" /></li>
                            <li data-icon="&#xf0e0;">
                                <input type="email" placeholder="Friend‘s Email Id" name="femail" id="eemail" value="<?php echo $ordered_data["femail"]; ?>" />
                            </li>
                            <li data-icon="&#xf007;">
                                <input type="text" placeholder="Friend‘s Name"  name="fname" id="ename" value="<?php echo $ordered_data["fname"]; ?>"/>
                            </li>
                            <li><a style="padding: 0 !important;" href="<?PHP echo BASEURL_OIMG ?>email_sample.jpg" class="lbbox">See Sample </a></li>
                        </ul>
                        <ul class="by_de_selection wrapper bor_sep li3">
                            <li>
                                <img src="<?PHP echo BASEURL_OIMG ?>sms.png" /></li>
                            <li data-icon="&#xf10b;">
                                <input type="text" placeholder="Friend‘s No. eg. +91 97712345678"  value="<?php echo $ordered_data["fphone"]; ?>" name="fphone" id="phone" onkeypress="return isNumberKey(event)"/>
                            </li>
                            <li data-icon="&#xf007;">
                                <input type="text" placeholder="Friend‘s Name" name="fname" id="pname" value="<?php echo $ordered_data["fname"]; ?>"/>
                            </li>
                            <li><a style="padding: 0 !important;" href="<?PHP echo BASEURL_OIMG ?>sms_sample.jpg" class="lbbox">See Sample </a></li>
                        </ul>
                        <ul class="wrapper schedule" id="checkout">
                            <span>Schedule Gift Card Delivery</span>
                            <li style="background:url(<?php echo BASEURL_OIMG; ?>calender-icon.png) left center no-repeat">
                                <input type="text" id="min-date" data-mindate="today"  value="" name="" placeholder="Date of Delivery" style="cursor: pointer;"/>
                                <div id="ScheduleValidation1"></div>
                            </li>
                            <li style="background:url(<?php echo BASEURL_OIMG; ?>time-icon.png) left center no-repeat">
                                <div class="min-time" id="min-time"></div>
                                <div id="ScheduleValidation2"></div>
                                                                <!--<input type="text" id="min-time" value="" name="" placeholder="Time of Delivery" style="cursor: pointer;"/>-->
                            </li>

                            <!--                            <li>
                                                            <select id="delivery_schedule" name="delievery_schedule">
                                                                <option value="0">Immediately</option>
                                                                <option value="1">Today</option>
                                                                <option value="2">Tomorrow</option>
                                                                <option value="3">Day After Tomorrow</option>
                                                                <option value="3" id="imagePicker">Specify Date</option>
                                                            </select>
                            
                                                        </li>-->
                            <!--                            <li id="time_selection"  style="">
                                                            <select id="time"  name="delievery_time">
                                                                <option selected="selected" value="1" >Select Time</option>
                                                                <option value="09:00:00">09.00 AM</option>
                                                                <option value="09:30:00">09.30 AM</option>
                                                                <option value="10:00:00">10.00 AM</option>
                                                                <option value="10:30:00">10.30 AM</option>
                                                                <option value="11:00:00">11.00 AM</option>
                                                                <option value="11:30:00">11.30 AM</option>
                                                                <option value="12:00:00">12.00 PM</option>
                                                                <option value="12:30:00">12.30 PM</option>
                                                                <option value="13:00:00">01.00 PM</option>
                                                                <option value="13:30:00">01.30 PM</option>
                                                                <option value="14:00:00">02.00 PM</option>
                                                                <option value="14:30:00">02.30 PM</option>
                                                                <option value="15:00:00">03.00 PM</option>
                                                                <option value="15:30:00">03.30 PM</option>
                                                                <option value="16:00:00">04.00 PM</option>
                                                                <option value="16:30:00">04.30 PM</option>
                                                                <option value="17:00:00">05.00 PM</option>
                                                                <option value="17:30:00">05.30 PM</option>
                                                                <option value="18:00:00">06.00 PM</option>
                                                                <option value="18:30:00">06.30 PM</option>
                                                                <option value="19:00:00">07.00 PM</option>
                                                                <option value="19:30:00">07.30 PM</option>
                                                                <option value="20:00:00">08.00 PM</option>
                                                                <option value="20:30:00">08.30 PM</option>
                                                                <option value="21:00:00">09.00 PM</option>
                                                                <option value="21:30:00">09.30 PM</option>
                                                                <option value="22:00:00">10.00 PM</option>
                                                                <option value="22:30:00">10.30 PM</option>
                                                                <option value="23:00:00">11.00 PM</option>
                                                                <option value="23:30:00">11.30 PM</option>
                                                            </select>
                                                        </li>-->
                        </ul>
                        </script>
                        <div style="display:none; margin: auto 88px;" class="ResultBox">
                            <input type="hidden"  name="type" id="checktype" value="0"  />
                            <input type="hidden" value="" id="result"  style="border:none;font-weight: 600;color: black"/>
                            <input type="hidden" value="" id="ddate" name="delivery_date" />
                            <input type="hidden" name="email" id="flemail" value="" />
                            <input type="hidden" name="password" id="flpass" value="" />
                            <input type="hidden" name="vurl" id="urllink" value="<?php echo $url; ?>"/>
                            <input type="hidden" name="voucher_pro_id"  value="<?php echo $product_data["voucher_pro_id"]; ?>"/>
                            <input type="hidden" name="voucher_pro_img"  value="<?php echo $product_data["product_image"]; ?>"/>
                            <input type="hidden" name="voucher_pro_name"  value="<?php echo $product_data["voucher_pro_name"]; ?>"/>
                            <input type="hidden" id="uploaded_finalimg" name="uploaded_finalimg"  value="" />
                            <input type="hidden" id="urlaction" value="" redirectval=""/>

                        </div>
                        <div style="display: none;opacity: 0;position: relative;padding-left: 91px;z-index: 0;top:-94px" id="caldiv"  class="inlinedatepicker"> 
                            <div id="inlineDatepicker" ></div>
                        </div>

                        <input type="hidden" name="same_quantity_id" id='same_quantity_id' value="<?php echo $ordered_data["same_quantity_id"]; ?>"/>

                        <div id="guestLoginDiv" class="wrapper" >
                            <div class="guestinnerDiv">
                                <ul  class="guestradio">
                                    <li>
                                        <input type="radio" id="checkbox3" value="3" name="loginradio" class="gcheckbox" checked=""/>
                                        <label for="checkbox3"  class="glable">Login </label>
                                    </li>
                                    <li>
                                        <input type="radio" id="guestLogin" value="1" name="loginradio" class="gcheckbox"/>
                                        <label for="guestLogin" class="glable">Continue as Guest</label>
                                    </li>
                                    <li>
                                        <input type="radio" id="checkbox2" value="2" name="loginradio" class="gcheckbox"/>
                                        <label for="checkbox2"  class="glable">Sign up</label>
                                    </li>                                 
                                </ul>

                                <ul class="guestinput"  style="text-align:center;">
                                    <li>
                                        <label id="gemail-label" data-icon="&#xf0e0;">
                                            <input type="email" id="gemail"  name="gemail" placeholder="YOUR EMAIL ID" />
                                        </label>
                                    </li>
                                    <li id="numberBox1" style="display:none;">
                                        <label id="gnumber-label" data-icon="&#xf10b;" style="font-size: 23px;">
                                            <input id="gnumber" type="text" name="gnumber" placeholder="YOUR NUMBER" onkeypress="return isNumberKey(event)" /> 
                                        </label>
                                    </li>
                                    <li id="nameBox" style="display:none;">
                                        <label id="gname-label" data-icon="&#xf007;">
                                            <input id="gname" type="text" name="gname" placeholder="YOUR NAME" /> 
                                        </label>
                                    </li>                               
                                    <li id="passwordBox">
                                        <label id="gname-label" data-icon="&#xf023;">
                                            <input id="gpassword" type="password" name="password" placeholder="PASSWORD" /> 
                                        </label>
                                    </li> 
                                </ul>
                                <ul  class="guestinput"  id="socialdiv" >
                                    <!--                                <li>
                                                                        <a class="vfacebookLogin" style="cursor: pointer;" red_type="33" > 
                                                                            <img src="<?php // echo BASEURL_OIMG . "fb.png"                                    ?>" />
                                                                        </a>
                                                                    </li>-->
                                    <li>
                                        <a class="g-signin2 social_signin" onclick="ClickLogin_egift_vouchers()" data-onsuccess="onSignIn" style="cursor: pointer;" red_type="33">
                                        </a>
                                    </li>
                                </ul>
                                <ul class="genderBox" style="display:none">
                                    <li>Gender :
                                        <label id="gname-label">
                                            <input type="radio" name="gGender" value="1" id="Gender_0 " checked="true" />
                                            Male &nbsp;

                                            <input type="radio" name="gGender" value="2" id="Gender_1 " />
                                            Female
                                        </label>
                                    </li>
                                </ul>

                                <ul class="signupinput" style="text-align:center;display: none">
                                    <li style="padding-bottom:7px;">
                                        <label id="gname-label" data-icon="&#xf007;"><input id="gfirst_name" type="text" name="gfirst_name" placeholder="YOUR NAME" /></label>   
                                    </li>
                                    <li style="padding-bottom:7px;">
                                        <label id="gname-label" data-icon="&#xf0e0;">
                                            <input type="email" id="gsign_email"  name="gsign_email" placeholder="YOUR EMAIL ID" />
                                        </label> 
                                    </li>
                                    <li style="padding-bottom:7px;">
                                        <label id="gname-label" data-icon="&#xf023;">
                                            <input id="gsign_password" type="password" name="gsign_password" placeholder="PASSWORD" /> 
                                        </label>
                                    </li>
                                    <li style="padding-bottom:7px;">
                                        <label id="gname-label" data-icon="&#xf023;">
                                            <input id="gconf_password" type="password" name="gconf_password" placeholder="CONFIRM PASSWORD" /> 
                                        </label>
                                    </li>

                                    <li style="padding-bottom:7px;">
                                        <div style="width: 100%;display: flex;">

                                            <select id="dobday1" class="gsbdate" name="gsbdate"></select>
                                            <select id="dobmonth1" class="gsbmonth" name="gsbmonth"></select>
                                            <select id="dobyear1" class="gsbyear" name="gsbyear"></select>
                                        </div>
                                        <div style="text-align: left; font-weight: 600;">Note: Enter date of birth above.</div>
                                    </li>
                                    <li data-icon="&#xf10b;" style="padding-bottom:7px;">
                                        <div style="width: 100%;display: flex;">
                                        <select id="CmbCountryOption" name="nationality_id" class="countrylist" disabled>
                        				<option value=""><?php echo $country_name . " (+" . $nationality_code . ")"; ?></option>
                    					</select>
                                            <!-- 
<select id="gCmbCountryOption" name="nationality_id" class="countrylist">
                                                <?php $countryList = get_countryinfo(); ?>
                                                <?php foreach ($countryList as $country) { ?>
                                                    <option value="<?php echo $country["country_id"]; ?>"><?php echo $country['country_name'] . " (+" . $country["nationality_code"] . ")"; ?></option>
                                                <?php } ?>
                                            </select>
 -->
                                            <input type="text" placeholder="Mobile No" id="gmobile_no" name="gmobile_no" onkeypress="return isNumberKey(event)"  class="mbno" />
                                        </div>
                                        <div style="">&nbsp;</div>
                                    </li>
                                    <li>
                                        <div class="insidesignuperror" style=" height: 20px; color: red; font-weight: bold;text-align: center;margin-top: 10px;"></div>
                                    </li>

                                </ul>
                                <div id="gEmailValidation"></div>
                            </div>
                        </div>
                        <div class=" wrapper mb20 mt20 text-center">

                            <a href="#openpopup" class="inline_colorbox memlogin" id="login_popup" data-width="30%">  </a>
                            <?php if ($checkout == 1) { ?>
                                <div class="arrowbtn" id="another_gift">Add Another Gift</div>
                            <?php } ?>
                            <div class="arrowbtn" id="<?php echo $checkoutval; ?>" >Checkout</div>
                        </div>
                        <div class="btn-div"><span style="color:red;font-family: gotham-book;font-size: 16px;font-weight: 600;">* Note: Processing charges 3.5%.</span></div>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div class="in_f_l">
                    <div style="clear: both;"></div>
                    <div class="leftside">
                        <div style="clear: both;"></div>
                        <div class="right_v">
                            <span style="display: block; margin-bottom: 20px; margin-top: 20px; position: relative; text-align: center;font-weight: bold">
                                YOUR GIFT VOUCHER
                            </span>
                            <ul class="mslider">
                                <li>
                                    <div style="font-weight: 700; text-align: center;"><?php echo $product_data["voucher_pro_name"]; ?></div>
                                    <!--<abbr><?php // echo $product_data["voucher_pro_name"];                                                                                                                                                                                                                                                            ?></abbr>-->
                                    <?php
                                    if ($product_data["product_image"] != '')
                                        $img = $product_data["product_image"];
                                    else {
                                        $img = BASEURL_OIMG . "unnamed.png";
                                    }
                                    ?>
                                    <div style="border:1px solid #cdcdcd;border-radius: 5px;padding: 8px">
                                        <img src="<?php echo $img; ?>" />
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</form>
<div class="alertPopupDiv4 alertPopupDiv" >
    <div class="alertPopupMessage4 alertPopupMessage" style="background-color:black;color:white;top:74px">
        <div style="text-align: center; margin: auto auto; width: 100%; height:28px;line-height: 29px;display: inline-flex;">
            <div style="width: 10%"><div class="icon_cart"></div></div>

            <div style="font-size: 14px; margin-left: 9px; padding: 2px;width: 70%;" id='cmessage' >New e-Gift Cards added to the cart </div>
            <div style="width:20%;display: flex;">
                <div style="overflow: hidden; transform: rotate(-11deg); border-right-width: 0px; margin-top: -11px; margin-left: 0px;" >

                    <img src="" id="selgreeting" style="width: 29px"/> 
                </div>
                <div style="position: relative;top:24%">
                    <img src="<?php echo $img; ?>" style="width: 29px"  />
                </div>

            </div>
        </div>
    </div>
</div>


<div class="errorDiv"></div>
<div class="alertBox"></div>
<div class='BackgroundMedia'></div>

<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js"></script>
<script type="text/javascript">
                                                $(document).on('change', '#gCmbCountryOption', function () {
                                                    $('#gmobile_no').val('');
                                                    var curr_val = $(this).val();
                                                    if (curr_val != 102) {
                                                        $('#gmobile_no').attr('readonly', '');
                                                    } else {
                                                        $('#gmobile_no').removeAttr('readonly');
                                                    }
                                                });
                                                $(document).ready(function () {
                                                    $.dobPicker({
                                                        daySelector: '#dobday1', /* Required */
                                                        monthSelector: '#dobmonth1', /* Required */
                                                        yearSelector: '#dobyear1', /* Required */
                                                        //                dayDefault: birth_date, /* Optional */
                                                        //                monthDefault: birth_month, /* Optional */
                                                        //                yearDefault: birth_year, /* Optional */
                                                        minimumAge: 8, /* Optional */
                                                        maximumAge: 60 /* Optional */
                                                    });
                                                });
                                                $('#min-date').dcalendarpicker({
                                                    // default: mm/dd/yyyy
                                                    format: 'yyyy-mm-dd',
                                                    theme: 'green'
                                                });
                                                $('.calendar-head-card').hide();

                                                $("#min-time").picktim({
//                                                    mode: 'h12',
                                                    appendTo: 'body'
                                                });

                                                var time = new Date();
                                                var hours = time.getHours();
//                                                var ampm = hours >= 12 ? 'pm' : 'am';
//                                                hours = hours % 12;
//                                                hours = hours ? hours : 12; // the hour '0' should be '12'
                                                $('.picktim-hour').val(hours);
                                                var minutes = time.getMinutes();
                                                $('.picktim-mins').val(minutes);
//                                                $('.picktim-ampm').html(ampm.toUpperCase());

                                                $('#min-date').click(function () {
                                                    $("#ScheduleValidation1").html("").removeClass("ierror");
                                                    $("#ScheduleValidation2").html("").removeClass("ierror");
                                                });
                                                $('.time-input').click(function () {
                                                    $("#ScheduleValidation1").html("").removeClass("ierror");
                                                    $("#ScheduleValidation2").html("").removeClass("ierror");
                                                });
//                                                alert($("#min-time").picktim('value'));

                                                $(document).ready(function () {
                                                    initialize();
                                                    $("#click_welcome").click(function () {
                                                        window.location = "<?php echo WEB_DASHBOARD; ?>";
                                                    });
                                                    $("ul.priceselection li").click(function () {
                                                        $("#PriceValidation").html("").removeClass("ierror");
                                                    });
                                                    $("input[name='loginradio']").change(function () {
                                                        var radioValue = $("input[name='loginradio']:checked").val();
                                                        switch (radioValue) {
                                                            case "1":
                                                                {
                                                                    $(".guestinput").show();
                                                                    $("#socialdiv").hide();
                                                                    $("#passwordBox").hide();
                                                                    $("#nameBox").show();
                                                                    $(".signupinput").hide();
                                                                    $(".genderBox").hide();
                                                                    $("#numberBox1").show();
                                                                }
                                                                break;
                                                            case "2":
                                                                {
                                                                    $(".guestinput").hide();
                                                                    $(".signupinput").show()
                                                                    $("#socialdiv").hide();
                                                                    $("#passwordBox").hide();
                                                                    $("#nameBox").hide();
                                                                    $(".genderBox").show();
                                                                }
                                                                break;
                                                            case "3":
                                                                {
                                                                    $(".guestinput").show();
                                                                    $("#socialdiv").show();
                                                                    $("#passwordBox").show();
                                                                    $("#nameBox").hide();
                                                                    $(".signupinput").hide();
                                                                    $(".genderBox").hide();
                                                                    $("#numberBox1").hide();
                                                                }
                                                                break;
                                                        }

                                                    });
                                                    $(document).on("click", "#docheckout", function () {
                                                        var radioValue = $("input[name='loginradio']:checked").val();
                                                        performCheckout(1, radioValue);
                                                    });
                                                    $(document).on("click", "#docheckoutupdate", function () {
                                                        var greeting_id = $(".active #greet_id").attr("card_id");
                                                        $("#set_greeting_id").val(greeting_id);
                                                        var amount = $("ul.priceselection li button.active").val();
                                                        var greeting = $("#largeImage").attr('src');
                                                        var greeting_msg = $("#htmlContent").val();
                                                        var eemail = $("#eemail").val();
                                                        var ename = $("#ename").val();
                                                        var phone = $("#phone").val();
                                                        var pname = $("#pname").val();
                                                        var text = $("#get_text").text();
                                                        if ((amount == undefined)) {
                                                            $("#PriceValidation").html("Please specify the Gift Amount").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#derror").offset().top
                                                            }, 2000);
                                                        } else if (eemail == '' && phone == '') {
                                                            $("#EmailValidation").html("Enter Email Address or Mobile No").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#choose_delivery").offset().top
                                                            }, 2000);
                                                        } else if ((!(validateEmail(eemail))) && (eemail != '')) {
                                                            $("#EmailValidation").html("Invalid Email Address").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#choose_delivery").offset().top
                                                            }, 2000);
                                                        } else if ((!(checkMobileValid(phone))) && (phone != '')) {
                                                            $("#EmailValidation").html("Enter valid Indian Mobile No").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#choose_delivery").offset().top
                                                            }, 2000);
                                                        } else if (ename == '' && pname == '') {
                                                            $("#EmailValidation").html("Enter Friend's Name").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#choose_delivery").offset().top
                                                            }, 2000);
                                                        } else if ($("#min-date").val() == '') {
                                                            $("#ScheduleValidation1").html("Please Select Delivery Date").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#checkout").offset().top
                                                            }, 2000);
                                                        } else if ($(".time-input").val() == "") {
                                                            $("#ScheduleValidation2").html("Please Select Delivery Time").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#checkout").offset().top
                                                            }, 2000);
                                                        } else {
//                                                            if ($(".time-input").val() == "") {
//                                                                alert('akki');
//                                                            }
//                                                            alert($("#min-date").val());
//                                                            alert($(".time-input").val());
                                                            var delivery_date_time_concat = $("#min-date").val() + " " + $(".time-input").val();
//                                                            alert(delivery_date_time_concat);

                                                            $("#setamount").val(amount);
                                                            var tz = jstz.determine(); // Determines the time zone of the browser client
                                                            var timezone = tz.name(); //'Asia/Kolhata' for Indian Time.
                                                            var order_pro_id = "<?php echo $order_pro_id; ?>";
                                                            var data = {
                                                                quantity_new: $("#quantity_new").val(),
                                                                selected_amount: $("#setamount").val(),
                                                                greeting_message: greeting_msg,
                                                                fname: $("#pname").val(),
                                                                femail: $("#eemail").val(),
                                                                delievery_schedule: $("#delivery_schedule").val(),
                                                                delivery_date_time: delivery_date_time_concat,
                                                                delievery_time: $("#time").val(),
                                                                delivery_date: $("#result").val(),
                                                                type: $("#checktype").val(),
                                                                greeting_id: $("#set_greeting_id").val(),
                                                                uploaded_finalimg: $("#uploaded_finalimg").val(),
                                                                fphone: $("#phone").val(),
                                                                order_pro_id: order_pro_id,
                                                                timezone: timezone,
                                                                same_quantity_id: $("#same_quantity_id").val()
                                                            }

                                                            if (islogin != "") {
                                                                var url = "<?php echo BASEURL; ?>voucher-update-cart";
                                                                $.ajax({
                                                                    url: url,
                                                                    type: "POST",
                                                                    data: data,
                                                                    dataType: "json",
                                                                    beforeSend: function () {
                                                                        $("#inlineDatepicker").hide();
                                                                        $(".showloader").addClass("show_overlay").show();
                                                                    },
                                                                    success: function (msg) {
                                                                        if (msg.success.toString() == "true") {
                                                                            $(".showloader").removeClass("show_overlay").hide();
                                                                            alert(msg.message.toString());
                                                                            window.location = "<?php echo WEB_SHOW_CART; ?>";
                                                                        } else {
                                                                            $(".showloader").removeClass("show_overlay").hide();
                                                                            alert(msg.message.toString());
                                                                            window.location = "<?php echo WEB_HOME; ?>";
                                                                        }
                                                                    }, });
                                                            } else {
                                                                $("#login_type").val('2');
                                                                $("#urlaction").val("<?php echo BASEURL; ?>voucher-cart");
                                                                $("#urlaction").attr("redirectval", "1");
                                                                $(".change_id").attr("id", "voucherlogin");
                                                                $('#login_popup').click();
                                                            }
                                                        }
                                                    });
                                                    $('#semail').keyup(function (e) {
                                                        $(".merrors3").html("");
                                                    });
                                                    $('#phone').keyup(function (e) {
                                                        $(".serrors4").html("");
                                                    });
                                                    $("#ename").keyup(function () {
                                                        var entered_name = $(this).val();
                                                        $("#pname").val(entered_name);
                                                        $("#EmailValidation").html("").removeClass("ierror");
                                                    });
                                                    $("#eemail,#phone").keyup(function () {
                                                        var eemail = $("#eemail").val();
                                                        $("#EmailValidation").html("").removeClass("ierror");
                                                        if (((validateEmail(eemail))) && (eemail != '')) {
                                                            $("#EmailValidation").html("").removeClass("ierror");
                                                        }
                                                    });
                                                    $("#gemail,#gname").keyup(function () {
                                                        var eemail = $("#gemail").val();
                                                        $("#gEmailValidation").html("").removeClass("ierror");
                                                        if (((validateEmail(eemail))) && (eemail != '')) {
                                                            $("#gEmailValidation").html("").removeClass("ierror");
                                                        }
                                                    });
                                                    $("#pname").keyup(function () {
                                                        $("#EmailValidation").html("").removeClass("ierror");
                                                        var entered_name = $(this).val();
                                                        $("#ename").val(entered_name);
                                                    });
                                                    $("#entered_amount").keyup(function () {
                                                        $(".errorDiv").css("display", "none");
                                                    });
                                                    //checkout click-end
                                                    //add-another-gift-start
                                                    $("#another_gift").click(function () {
                                                        var radioValue = $("input[name='loginradio']:checked").val();
                                                        performCheckout(2, radioValue);
                                                    });
                                                });
                                                var max = 240;
                                                $("#htmlContent").keyup(function () {
                                                    var message = $(this).val();
                                                    $("#messgecount").text((max - $(this).val().length) + " Characters remaining ");
                                                    //$("#greet_text").html(message);
                                                });
                                                $('.right_v ul').bxSlider({
                                                    nextText: '',
                                                    prevText: '',
                                                    pager: true,
                                                    auto: false,
                                                    speed: 1000,
                                                    mode: 'fade'
                                                });
                                                $("#htmlContent").focus(function () {
                                                    if (this.value === this.defaultValue) {
                                                        this.value = '';
                                                    }
                                                }).blur(function () {
                                                    if (this.value === '') {
                                                        this.value = this.defaultValue;
                                                    }
                                                });
                                                $(".ResultBox").show();
                                                var today = "<?php echo date("d/m/Y"); ?>";
                                                $("#result").val("Delivery On:" + today);
                                                //delivery selection section:starts
                                                $("#delivery_schedule").change(function () {
                                                    var svalue = $(this).val();
                                                    switch (svalue) {
                                                        case "0":
                                                            {
                                                                $("#time_selection").hide();
                                                                $(".ResultBox").hide();
                                                                $("#result").val("");
                                                                $("#caldiv").css({"display": "none", "opacity": "0", "top": "-91px", "height": "0px"});
                                                            }
                                                            break;
                                                        case "1":
                                                            {
                                                                $("#time_selection").show();
                                                                $(".ResultBox").show();
                                                                var today = "<?php echo date("d/m/Y"); ?>";
                                                                $("#caldiv").css({"display": "none", "opacity": "0", "top": "-91px", "height": "0px"});
                                                                $("#result").val("Delivery On:" + today);
                                                                $('#time').prop('selectedIndex', 0); // select 1th option
                                                            }
                                                            break;
                                                        case "2":
                                                            {
                                                                $("#time_selection").show();
                                                                var tomorw = "<?php echo date("d/m/Y ", strtotime("tomorrow")); ?>";
                                                                $(".ResultBox").show();
                                                                $("#caldiv").css({"display": "none", "opacity": "0", "top": "-91px", "height": "0px"});
                                                                $("#result").val("Delivery On:" + tomorw);
                                                                $('#time').prop('selectedIndex', 0); // select 1th option
                                                            }
                                                            break;
//                                                        case "3":
//                                                            {
//                                                                $("#time_selection").show();
//                                                                var tomorw = "<?php echo date("d/m/Y ", strtotime('tomorrow + 1 day')); ?>";
//                                                                $(".ResultBox").show();
//
//                                                                $("#caldiv").css({"visibility": "hidden", "opacity": "0", "top": "-91px", "height": "0px"});
//                                                                $("#result").val("Delivery On:" + tomorw);
//                                                            }
//                                                            break;
                                                        case "3":
                                                            {
                                                                $(".dp_header").css("width", "100%");
                                                                $(".dp_footer").css("width", "100%");
                                                                $(".inlinedatepicker").css("height", "291px");
                                                                $("#time_selection").show();
                                                                $(".ResultBox").show();
                                                                $("#caldiv").css({"display": "block", "opacity": "1", "top": "0px", "height": "291px"});
                                                                $('#time').prop('selectedIndex', 0); // select 1th option

                                                            }
                                                            break;
                                                    }
                                                });
                                                $(".vfacebookLogin").click(function () {
                                                    performCheckout(1, "4");
                                                });

                                                $(".vgoogleLogin").click(function () {
                                                    performCheckout(1, "5");

                                                });

                                                function vset_sessiondata(red_type, from) {
                                                    var tz = jstz.determine(); // Determines the time zone of the browser client
                                                    var timezone = tz.name();
                                                    var data = {
                                                        quantity_new: $("#quantity_new").val(),
                                                        selected_amount: $("ul.priceselection li button.active").val(),
                                                        greeting_message: $("#htmlContent").val(),
                                                        fname: $("#pname").val(),
                                                        femail: $("#eemail").val(),
                                                        delievery_schedule: $("#delivery_schedule").val(),
                                                        delivery_date_time: $("#min-date").val(),
                                                        delievery_time: $("#time").val(),
                                                        delivery_date: $("#ddate").val(),
                                                        type: $("#checktype").val(),
                                                        greeting_id: $(".active #greet_id").attr("card_id"),
                                                        uploaded_finalimg: $("#uploaded_finalimg").val(),
                                                        fphone: $("#phone").val(),
                                                        redirect: $("#urlaction").attr("redirectval"),
                                                        timezone: timezone,
                                                        is_social: 1
                                                    }
                                                    var url = "<?php echo BASEURL; ?>Home_web/set_Voucher_sessionData";
                                                    $.ajax({
                                                        url: url,
                                                        type: "POST",
                                                        data: data,
                                                        dataType: "json",
                                                        beforeSend: function () {
                                                            $(".showloader").addClass("show_overlay").show();
                                                        },
                                                        success: function (msg) {
                                                            if (from == "5")
                                                                window.location = "<?php echo BASEURL; ?>Home_web/social_login_data/3/" + red_type;
                                                            else
                                                                window.location = "<?php echo BASEURL; ?>Home_web/social_login_data/2/" + red_type;
                                                        }, });

                                                }
                                                function getDate(date, m) {
                                                    $("#result").val();
                                                    $("#ScheduleValidation").html("").removeClass("ierror");
                                                    $(".errors7").hide();
                                                }
                                                function get_card_id(card_id) {
                                                    $("#largeImage").attr("greeting_id", card_id);
                                                }
                                                //add-another-gift-end
                                                function performCheckout(redirect, is_guest) {

                                                    var guest = 0;
                                                    var greeting_id = $(".active #greet_id").attr("card_id");
                                                    $("#set_greeting_id").val(greeting_id);
                                                    var amount = $("ul.priceselection li button.active").val();
                                                    var greeting = $("#largeImage").attr('src');
                                                    var greeting_msg = $("#htmlContent").val();
                                                    var eemail = $("#eemail").val();
                                                    var ename = $("#ename").val();
                                                    var phone = $("#phone").val();
                                                    var pname = $("#pname").val();
                                                    var text = $("#get_text").text();
                                                    if ((amount == undefined)) {
                                                        $("#PriceValidation").html("Please specify the Gift Amount").addClass("ierror");
                                                        $('html, body').animate({
                                                            scrollTop: $("#derror").offset().top
                                                        }, 2000);
                                                    } else if (eemail == '' && phone == '') {
                                                        $("#EmailValidation").html("Enter Email Address or Mobile No").addClass("ierror");
                                                        $('html, body').animate({
                                                            scrollTop: $("#choose_delivery").offset().top
                                                        }, 2000);
                                                    } else if ((!(validateEmail(eemail))) && (eemail != '')) {
                                                        $("#EmailValidation").html("Invalid Email Address").addClass("ierror");

                                                        $('html, body').animate({
                                                            scrollTop: $("#choose_delivery").offset().top
                                                        }, 2000);
                                                    } else if ((!(checkMobileValid(phone))) && (phone != '')) {
                                                        $("#EmailValidation").html("Enter valid Indian Mobile No").addClass("ierror");
                                                        $('html, body').animate({
                                                            scrollTop: $("#choose_delivery").offset().top
                                                        }, 2000);

                                                    } else if (ename == '' && pname == '') {
                                                        $("#EmailValidation").html("Enter Friend's Name").addClass("ierror");
                                                        $('html, body').animate({
                                                            scrollTop: $("#choose_delivery").offset().top
                                                        }, 2000);

                                                    } else if ($("#min-date").val() == '') {
                                                        $("#ScheduleValidation1").html("Please Select Delivery Date").addClass("ierror");
                                                        $('html, body').animate({
                                                            scrollTop: $("#checkout").offset().top
                                                        }, 2000);
                                                    } else if ($(".time-input").val() == "") {
                                                        $("#ScheduleValidation2").html("Please Select Delivery Time").addClass("ierror");
                                                        $('html, body').animate({
                                                            scrollTop: $("#checkout").offset().top
                                                        }, 2000);
                                                    } else {
//                                                        if ($(".time-input").val() == "") {
//                                                            alert('akki');
//                                                        }
//                                                        alert($("#min-date").val());
//                                                        alert($(".time-input").val());
                                                        var delivery_date_time_concat = $("#min-date").val() + " " + $(".time-input").val();

                                                        var tz = jstz.determine(); // Determines the time zone of the browser client
                                                        var timezone = tz.name(); //'Asia/Kolhata' for Indian Time.
                                                        $("#setamount").val(amount);
                                                        if (is_guest == "1") {
                                                            guest = 1;
                                                        } else {
                                                            guest = 0;
                                                        }

                                                        var data = {
                                                            quantity_new: $("#quantity_new").val(),
                                                            selected_amount: $("#setamount").val(),
                                                            greeting_message: greeting_msg,
                                                            fname: $("#pname").val(),
                                                            femail: $("#eemail").val(),
                                                            delievery_schedule: $("#delivery_schedule").val(),
                                                            delivery_date_time: delivery_date_time_concat,
                                                            delievery_time: $("#time").val(),
                                                            delivery_date: $("#result").val(),
                                                            type: $("#checktype").val(),
                                                            greeting_id: $("#set_greeting_id").val(),
                                                            uploaded_finalimg: $("#uploaded_finalimg").val(),
                                                            fphone: $("#phone").val(),
                                                            timezone: timezone,
                                                            redirect: redirect, //Normal checkout
                                                            is_guest: guest
//                                                                order_pro_id: order_pro_id,
//                                                                same_quantity_id: $("#same_quantity_id").val()
                                                        }
//                                                        if ((islogin != "") && (isGuest == 0)) { //old session concept
                                                        if (islogin != "") {
                                                            $("#guestLoginDiv").hide();
                                                            var url = "<?php echo BASEURL; ?>voucher-cart";
                                                            $.ajax({
                                                                url: url,
                                                                type: "POST",
                                                                data: data,
                                                                dataType: "json",
                                                                beforeSend: function () {
                                                                    $("#inlineDatepicker").hide();
                                                                    $(".showloader").addClass("show_overlay").show();
                                                                },
                                                                success: function (msg) {
<?php
//$this->session->set_userdata(array('is_social' => 0));
?>
                                                                    $(".showloader").removeClass("show_overlay").hide();
                                                                    if (msg.success.toString() == "true") {
                                                                        if (msg.redirect.toString() == "1") {
                                                                            window.location = "<?php echo WEB_SHOW_CART; ?>";
                                                                        } else {
                                                                            $("#selgreeting").attr("src", $("#largeImage").attr("src"));
                                                                            $(".alertPopupDiv4").addClass("show_overlay").show();
                                                                            $('.alertPopupMessage4').animate({top: '34%'}, 500);
                                                                            if (typeof (Storage) != "undefined") {
                                                                                localStorage.setItem("add_another_message", "1");
                                                                            }
                                                                            setTimeout(function () {
                                                                                $(".alertPopupDiv4").removeClass("show_overlay").hide();
                                                                                $('.alertPopupMessage4').animate({top: '0%'}, 500);
                                                                                window.location = "<?php echo WEB_HOME; ?>";
                                                                            }, 2000);
                                                                        }
                                                                    } else {
                                                                        alert(msg.message.toString());
                                                                        window.location = "<?php echo WEB_HOME; ?>";
                                                                    }
                                                                }, });
                                                        } else {
                                                            $("#guestLoginDiv").show();
                                                            $(".showloader").removeClass("show_overlay").hide();
                                                            $("#urlaction").val("<?php echo BASEURL; ?>voucher-cart");
                                                            $("#urlaction").attr("redirectval", redirect);
                                                            switch (is_guest) {
                                                                case "1":
                                                                    {
                                                                        $("#login_type").val('2');
                                                                        doLogin(1);
                                                                    }
                                                                    break;
                                                                case "2":
                                                                    {
                                                                        var selectedCategory = '';
                                                                        $('input[name="gGender"]:checked').each(function () {
                                                                            selectedCategory = (this.value);
                                                                        });
                                                                        //Old
//                                                                        doSignup($("#gfirst_name").val(), $("#gsign_password").val(), $("#gmobile_no").val(), $("#gsign_email").val(), $("#gconf_password").val(), $("#gsbdate").val(), $("#gsbmonth").val(), $("#gsbyear").val(), selectedCategory, $("#gCmbCountryOption").val(), 1);
                                                                        //New
                                                                        doSignup($("#gfirst_name").val(), $("#gsign_password").val(), $("#gmobile_no").val(), $("#gsign_email").val(), $("#gconf_password").val(), $(".gsbdate").val(), $(".gsbmonth").val(), $(".gsbyear").val(), selectedCategory, $("#gCmbCountryOption").val(), 1);
                                                                    }
                                                                    break;
                                                                case "3":
                                                                    {
                                                                        doLogin(3);
                                                                    }
                                                                    break;
                                                                case "4":
                                                                    {

                                                                        var red_type = $(".vfacebookLogin").attr("red_type");
                                                                        vset_sessiondata(red_type, 4);
                                                                    }
                                                                    break;
                                                                case "5":
                                                                    {

                                                                        var red_type = $(".vgoogleLogin").attr("red_type");
                                                                        vset_sessiondata(red_type, 5);
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                                if ($(window).width() > 768)
                                                {
                                                    if ($('.leftside').length) {
                                                        $(window).scroll(function (e) {
                                                            $(".leftside").removeClass("fixed");
                                                            var topPos = $(".leftside").offset().top;
                                                            var footPos = $(".schedule").offset().top - 200;
                                                            var winscroll = $(window).scrollTop();
                                                            if (winscroll > topPos)
                                                            {
                                                                $(".leftside").addClass("fixed");
                                                            }
                                                            if (winscroll > footPos)
                                                            {
                                                                $(".leftside").removeClass("fixed");
                                                            }
                                                        });
                                                    }
                                                }
                                                function checkMobileValid(mobile) {

                                                    var filter = /^[(]{0,1}[0-9]{3}[)\.\- ]{0,1}[0-9]{3}[\.\- ]{0,1}[0-9]{4}$/;
                                                    if (filter.test(mobile)) {
                                                        return true;
                                                    } else {
                                                        return false;
                                                    }
                                                }
                                                function initialize() {
<?php //$this->session->set_userdata(array('is_social' => 0));                                                                                                                                                                                                                                                                                                                                                                                    ?>



//if ((islogin != "") && (isGuest == 0)) { //old session concept
                                                    if (islogin != "") {
                                                        $("#guestLoginDiv").hide();
                                                    } else {
                                                        $("#guestLoginDiv").show();
                                                    }
                                                    $("#gCmbCountryOption option[value=102]").attr("selected", "selected");
                                                    var selected_amount = "<?php echo $ordered_data["selected_amount"]; ?>";
                                                    var selected_card = "<?php echo $ordered_data["greeting_id"]; ?>";
<?php if ($ordered_data["delievery_schedule"] != "0") { ?>
                                                        $("#delivery_schedule").val("<?php echo $ordered_data["delievery_schedule"]; ?>");
    <?php
//    $date = date_create($ordered_data["delivery_date_time"]);
    $date = $ordered_data["delivery_date_time"]; //Actual Date Time
    $span = explode(" ", $date);
    $deli_date = $span[0];
    $deli_time = $span[1];
    $date1 = date_create($deli_time);
    $deli_time_new = date_format($date1, "H:i");


    $deli_time_new_str = (string) $deli_time_new;
    $span1 = explode(":", $deli_time_new_str);
    $deli_hour1 = $span1[0];
    $deli_time1 = $span1[1];
    ?>

                                                        $("#min-date").val("<?php echo $deli_date; ?>");
                                                        $(".time-input").val("<?php echo $deli_time_new; ?>");

                                                        var hours = "<?php echo $deli_hour1; ?>";
                                                        //                                                        var ampm = hours >= 12 ? 'pm' : 'am';
                                                        //                                                        hours = hours % 12;
                                                        //                                                        hours = hours ? hours : 12; // the hour '0' should be '12'
                                                        $('.picktim-hour').val(hours);
                                                        var minutes = "<?php echo $deli_time1; ?>";
                                                        $('.picktim-mins').val(minutes);
                                                        //                                                        $('.picktim-ampm').html(ampm.toUpperCase());
<?php } ?>
<?php if ($ordered_data['uploded_img_video'] != "") { ?>
    <?php if ($ordered_data["type"] == 3) { ?>
                                                            $(".BackgroundMedia").css("display", "none");
                                                            $(".recordingOptions").css("display", "none");
                                                            $('.recordingInnerDiv').animate({top: '0%'}, 500);
                                                            $(".uploaded_pic1").attr("name", "userfile1");
                                                            $(".upload_video").attr("name", "userfile2");
                                                            $("#selectaudiofile").attr("name", "userfile");
                                                            $("#playaudioplayer").attr('src', "<?php echo BASEURL_BVMG . $ordered_data['uploded_img_video'] ?>");
                                                            $("#checktype").val("3"); // audio:3
                                                            $("#audioTime").text("00:00");
                                                            $("#audioTimePlayer").text("00:00");
                                                            $(".imgVideoContainer").hide();
                                                            $(".afterclick").hide();
                                                            $(".maincircle").hide();
                                                            $(".titlebox").hide();
                                                            $(".playerbox").show();
                                                            $("#playbox").hide();
                                                            $("#dorecording").hide();
                                                            $(".closerecording").show();
                                                            $(".recordchoices").css("width", "100%");
                                                            $("#select_audio").css("width", "40%");
                                                            $(".vertical_line").css("width", "20%");
                                                            $("#record_audio").css("width", "40%");
                                                            $(".sampleimg").hide();
    <?php } ?>
    <?php if ($ordered_data["type"] == 1) { ?>
                                                            $(".maincircle").hide();
                                                            $(".remove_pic").css("display", "block");
                                                            $("#defaultpic").attr("src", "<?php echo BASEURL_BVMG . $ordered_data['uploded_img_video'] ?>");
    <?php } ?>

    <?php if ($ordered_data["type"] == 2) { ?>
                                                            $("#userfile").attr("name", "userfile1");
                                                            $(".upload_video").attr("name", "userfile");
                                                            $("#selectaudiofile").attr("name", "userfile2");
                                                            $("#checktype").val("2"); // video-2
                                                            $("#userfile_old").val("");
                                                            $("#videopreviewBox").hide();
                                                            $(".BackgroundMedia").hide();
                                                            $(".videoOptions").hide();
                                                            $("#voptions").hide();
                                                            $(".closerecording").hide();
                                                            $(".maincircle").hide();
                                                            $('.videoInnerDiv').animate({top: '0%'}, 500);
                                                            $("#videof").attr("src", "<?php echo BASEURL_BVMG . $ordered_data['uploded_img_video'] ?>");
                                                            $(".sampleimg").css("display", "none");
                                                            $(".display_video").css("display", "block");
                                                            $(".titlebox").hide();
                                                            $(".video_container").show();
    <?php } ?>
<?php } ?>
                                                    //  alert(selected_amount);
                                                    $('ul.priceselection li ').each(function () {
                                                        if ($(this).find('button').val() == selected_amount)
                                                            $(this).find('button').val(selected_amount).addClass('active');
                                                    });
                                                    if (selected_card != "") {
                                                        $('#greeting_card li').each(function () {
                                                            if ($(this).attr("card_id") == selected_card) {
                                                                $(this).addClass('active');
                                                            } else {
                                                                $(this).removeClass('active');
                                                            }
                                                        });
                                                    }
                                                }

                                                function doLogin(from) {
                                                    var gpassword = $("#gpassword").val();
                                                    var gemail = $("#gemail").val();
                                                    var gname = $("#gname").val();
                                                    var gnumber = $("#gnumber").val();
                                                    if (from == "1") {
                                                        if (gname == '') {
                                                            $("#gEmailValidation").html("Enter Email Address and Name ").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#guestLoginDiv").offset().top
                                                            }, 2000);
                                                        } else if (gemail == '') {
                                                            $("#gEmailValidation").html("Enter Email Address and Name").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#guestLoginDiv").offset().top
                                                            }, 2000);
                                                        } else if (!(validateEmail(gemail)) && gemail != "") {
                                                            $("#gEmailValidation").html("Invalid Email Address").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#guestLoginDiv").offset().top
                                                            }, 2000);
                                                        } else if (gnumber == '') {
                                                            $("#gEmailValidation").html("Enter Mobile Number").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#guestLoginDiv").offset().top
                                                            }, 2000);
                                                        } else if ((!(checkMobileValid(gnumber))) && (gnumber != '')) {
                                                            $('#gEmailValidation').html("Enter valid Indian Mobile No").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#guestLoginDiv").offset().top
                                                            }, 2000);
                                                        } else {
//                                                            update_phone_value_guest = gnumber;
                                                            var data = {
                                                                email: gemail,
                                                                first_name: gname,
                                                                mobile_no: gnumber,
                                                                login_type: 2, //checkout-2 ,homepage-3,category-4,free_greeting-5
                                                                is_guest: 1

                                                            }
                                                            $.ajax({
                                                                type: "POST",
                                                                url: "<?php echo BASEURL; ?>login/" + from,
                                                                data: data,
                                                                dataType: "json",
                                                                beforeSend: function () {
                                                                    $(".showloader").addClass("show_overlay").show();
                                                                },
                                                                success: function (r) {
                                                                    $(".showloader").removeClass("show_overlay").hide();
                                                                    if (r.success.toString() === "false") {
                                                                        alert(r.message.toString());
                                                                        $('#password').val("");
                                                                        return false;
                                                                    } else {
                                                                        var is_user_exist = r.is_user_exist;
                                                                        var email = r.userdata.email;
                                                                        if (is_user_exist == 1) {
                                                                            $.colorbox({
                                                                                width: "400px",
                                                                                height: "320px",
                                                                                inline: true,
                                                                                href: $("#Login_birthdayowl_user_voucher").show(),
                                                                                overlayClose: false
                                                                            });
                                                                            $("#lemail_voucher").val(email);
//                                                                            call_send_greeting(0, 1);
                                                                        } else if (is_user_exist == 2) {
                                                                            if (r.session_guest_id != localStorage.getItem("guest_session"))
                                                                            {
                                                                                if (typeof (Storage) != "undefined") {
                                                                                    localStorage.setItem("guest_session", r.session_guest_id);
                                                                                }
                                                                            }
                                                                            call_send_greeting(1, r.session_guest_id);
                                                                        } else {
                                                                            is_guest = 1;
                                                                            redirect_value = 1;
                                                                            inserted_user_id = r.userdata['user_id'];
                                                                            is_profile_edit = 0;
//                                                                            if (r.hasOwnProperty('update_guest_phone')) {
//                                                                                is_update_phone_number_guest = true;
//                                                                            }
                                                                            if (r.session_guest_id != localStorage.getItem("guest_session"))
                                                                            {
                                                                                if (typeof (Storage) != "undefined") {
                                                                                    localStorage.setItem("guest_session", r.session_guest_id);
                                                                                }
                                                                            }
                                                                            $('#forgotpopup').click();
                                                                            $("#otpresetDiv").hide();
//                            $("#forgetpassDiv").hide();
//                                                                        $("#verifyMobileDiv").show();
                                                                            $.colorbox({
                                                                                width: "400px",
                                                                                height: "320px",
                                                                                inline: true,
                                                                                href: $("#verifyMobileDiv").show(),
                                                                                overlayClose: false
                                                                            });
                                                                            resend_mobile_number = gnumber;
//                                                                        call_send_greeting(1, r.session_guest_id);
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    } else {
                                                        if (gemail == '') {
                                                            $("#gEmailValidation").html("Enter Email Address and Password ").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#guestLoginDiv").offset().top
                                                            }, 2000);
                                                        } else if (gpassword == '') {
                                                            $("#gEmailValidation").html("Enter Email Address and Password ").addClass("ierror");
                                                            $('html, body').animate({
                                                                scrollTop: $("#guestLoginDiv").offset().top
                                                            }, 2000);
                                                        } else if (!(validateEmail(gemail)) && gemail != "") {
                                                            $("#gEmailValidation").html("Invalid Email Address").addClass("ierror");
                                                        } else {
                                                            var data = {
                                                                email: gemail,
                                                                password: gpassword,
                                                                login_type: 2,
                                                                is_guest: 0
                                                            }
                                                            $.ajax({
                                                                type: "POST",
                                                                url: "<?php echo BASEURL; ?>login/1",
                                                                data: data,
                                                                dataType: "json",
                                                                beforeSend: function () {
                                                                    $(".showloader").addClass("show_overlay").show();
                                                                },
                                                                success: function (r) {
                                                                    $(".showloader").removeClass("show_overlay").hide();
                                                                    if (r.success.toString() === "false") {
                                                                        alert(r.message.toString());
                                                                        $('#gpassword').val("");
                                                                        return false;
                                                                    } else {
                                                                        $("#flemail").val(gemail);
                                                                        $("#flpass").val(gpassword);
                                                                        call_send_greeting(0, 1);
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                }

                                                function call_send_greeting(is_guest, session_id) {
                                                    var tz = jstz.determine(); // Determines the time zone of the browser client
                                                    var timezone = tz.name(); //'Asia/Kolhata' for Indian Time.
                                                    var action = $("#urlaction").val();
                                                    var vurl = $("#urllink").val();
                                                    var formData = new FormData($("form[name='voucher_data']")[0]);
                                                    var url = "" + action;
                                                    var greeting_msg = $("#htmlContent").val();
                                                    var delivery_date_time_concat = $("#min-date").val() + " " + $(".time-input").val();
                                                    var data = {
                                                        quantity_new: $("#quantity_new").val(),
                                                        selected_amount: $("#setamount").val(),
                                                        greeting_message: greeting_msg,
                                                        fname: $("#pname").val(),
                                                        femail: $("#eemail").val(),
                                                        delievery_schedule: $("#delivery_schedule").val(),
                                                        delivery_date_time: delivery_date_time_concat,
                                                        delievery_time: $("#time").val(),
                                                        delivery_date: $("#ddate").val(),
                                                        type: $("#checktype").val(),
                                                        greeting_id: $("#set_greeting_id").val(),
                                                        uploaded_finalimg: $("#uploaded_finalimg").val(),
                                                        fphone: $("#phone").val(),
                                                        redirect: $("#urlaction").attr("redirectval"),
                                                        timezone: timezone,
                                                        is_guest: is_guest,
                                                        guest_session: session_id

                                                    }
                                                    $.ajax({
                                                        url: url,
                                                        type: "POST",
                                                        data: data,
                                                        dataType: "json",
                                                        success: function (msg) {
                                                            if (msg.success.toString() == "true") {
                                                                $(".showloader").removeClass("show_overlay").hide();
                                                                if (msg.redirect.toString() == "2") {
                                                                    $("#selgreeting").attr("src", $("#largeImage").attr("src"));
                                                                    $(".alertPopupDiv4").addClass("show_overlay").show();
                                                                    $('.alertPopupMessage4').animate({top: '34%'}, 500);
                                                                    if (typeof (Storage) != "undefined") {
                                                                        localStorage.setItem("add_another_message", "1");
                                                                    }
                                                                    setTimeout(function () {
                                                                        $(".alertPopupDiv4").removeClass("show_overlay").hide();
                                                                        $('.alertPopupMessage4').animate({top: '0%'}, 500);
                                                                        window.location = "<?php echo WEB_HOME; ?>";
                                                                    }, 2000);
                                                                } else {
//                                                                    $("#Login_birthdayowl_user").show();
//
//                                                                    $.colorbox({
//                                                                        width: "400px",
//                                                                        height: "320px",
//                                                                        inline: true,
//                                                                        href: $("#Login_birthdayowl_user").show(),
//                                                                        overlayClose: false
//                                                                    });
                                                                    window.location = "<?php echo WEB_SHOW_CART; ?>";
                                                                }
                                                            } else {
                                                                alert(msg.message.toString());
                                                            }
                                                        },
                                                    });
                                                }




                                                //footer functions

                                                //   alert($(window).width());
                                                $(window).scroll(function () {
                                                    var two = $("#make_it_personal").offset().top - 70;
                                                    var one = $("#pick_amount").offset().top - 70;
                                                    var three = $("#choose_delivery").offset().top - 80;
                                                    //                            var four = $("#checkout").offset().top - 100;
                                                    var scrollY = $(window).scrollTop();
                                                    if (scrollY > one) {
                                                        $('.in_heading').removeClass('in_heading2');
                                                        $('#cone').addClass('in_heading2');
                                                        $("#one").addClass("active");
                                                        $("#two").removeClass("active");
                                                        $("#threediv").removeClass("active");
                                                        $("#fourdiv").removeClass("active");
                                                    }
                                                    if (scrollY > two) {
                                                        $('.in_heading').removeClass('in_heading2');
                                                        $('#ctwo').addClass('in_heading2');
                                                        $("#two").addClass("active");
                                                        $("#one").removeClass("active");
                                                        $("#threediv").removeClass("active");
                                                        $("#fourdiv").removeClass("active");
                                                    }
                                                    if (scrollY > three) {
                                                        $('.in_heading').removeClass('in_heading2');
                                                        $('#cthree').addClass('in_heading2');
                                                        $("#threediv").addClass("active");
                                                        $("#one").removeClass("active");
                                                        $("#two").removeClass("active");
                                                        $("#fourdiv").removeClass("active");
                                                    }

                                                });
                                                $(".in_heading::before ").css("background", "black");

//                                                for google login

                                                var clicked = false;//Global Variable
                                                function ClickLogin_egift_vouchers() {
                                                    clicked = true;
                                                }
                                                function onSignIn(googleUser) {
                                                    if (clicked) {
                                                        var profile = googleUser.getBasicProfile();
                                                        console.log('ID: ' + profile.getId());
                                                        var user_id = profile.getId();
                                                        console.log('Name: ' + profile.getName());
                                                        var user_name = profile.getName();
                                                        console.log('Image URL: ' + profile.getImageUrl());
                                                        var user_image_url = profile.getImageUrl();
                                                        console.log('Email: ' + profile.getEmail());
                                                        var user_email = profile.getEmail();
                                                        var profile_array = {
                                                            user_id: user_id,
                                                            user_email: user_email,
                                                            user_name: user_name
                                                        }
                                                        update_user_data(profile_array);
                                                    }

                                                }

                                                function update_user_data(pro_array)
                                                {
                                                    $.ajax({
                                                        type: "POST",
                                                        dataType: 'json',
                                                        data: pro_array,
                                                        url: "<?php echo BASEURL; ?>Home_web/jquery_redirect_google_login_egift_vouchers",
                                                        success: function (r) {
                                                            if (r.success.toString() == "false") {
                                                                alert(r.message.toString());
                                                                return false;
                                                            } else {
                                                                islogin = r.islogin;

                                                                if (r.user_type == '1') {
                                                                    $(".top-left").load(location.href + " .top-left");
                                                                    $("#guestLoginDiv").hide();
//                                                                     window.location = "<?php // echo BASEURL;           ?>";
                                                                }
                                                                if (r.user_type == '2') {
                                                                    $("#guestLoginDiv").hide();
                                                                }
                                                            }
                                                        }

                                                    });
                                                }
</script>
<!--<script type="text/javascript" src="<?php // echo BASEURL_DATE;                                                             ?>zebra_datepicker.js"></script>-->
<!--<script type="text/javascript" src="<?php // echo BASEURL_DATE;                                                             ?>core.js"></script>-->

