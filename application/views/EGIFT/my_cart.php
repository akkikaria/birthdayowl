<?php $this->load->view("new_templates/header"); ?>
<?php
//echo '<pre>';
//print_r($this->session->all_userdata());
//exit;
?>
<style type="text/css">
    .pro_img{
        left: 37%;
        position: absolute;
        top: 19%;
    }
    .recname{
        color: white;font-size: 14px;  text-align: left;     margin-left: 26px;  width: 100%;    height: 13px;
    }

    .recemail{
        color: white; width: 100%;   text-align: left;    margin-left: 26px; height: 13px;font-size: 14px;
    }
    .recschedule{
        text-align: left;    margin-left: 26px;  width: 100%;    height: 13px; color: white;font-size: 14px;  
    }

    .scart{
        background-color: gray;height:229px; width:100%;   border-radius: 12px;
    }

    .pheading{
        font-size: 20px; font-family: 'GothamMedium';
    }
    .savequantity:hover{
        color: blue;
        text-decoration: underline;
    }
    .myquantity{
        margin: auto;  padding-top:20px;display: inline-flex;
    }
    .col-1-4 {
        float: left;
        width: 25%;
    }
    .myaction{
        margin: auto;
        width: 82%;
        padding-top: 17px;
    }
    .incedec{
        cursor: pointer;width: 25px;
        margin: auto;
    }
    .editcart,.delete,.delete1{
        width: 44%;
    }
    .sendinfo{
        float:left;
        padding-left: 18px;
        font-family: gotham-book;
        font-weight: 600;

    }
    @media only screen and (max-width: 768px) {
        .scart{
            margin: auto;
            width: 50%;
        }
        .pheading{
            font-size: 17px;
        }

    }

    @media only screen and (max-width:480px)
    {


        .scart{

            width: 100%;
        }
        .pheading{
            font-size: 11px;
        }
        .fontClass{
            font-size: 25px;
            line-height: 58px;
        }
        .col-1-4 {
            float: left;
            width: 100%;
        }

        .sendinfo{
            float:unset;
            padding-left: 0px;

        }
    }

</style>


<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            Checkout <span>&amp; Deliver </span>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <div class="inner_midd">
            <div class="in_f_l">
                <ul class="leftside leftside_nav">
                    <li class="msuccess"><a href="#pick_amount">Pick Amount</a></li>
                    <li class="msuccess"><a href="#make_it_personal">Make It Personal</a></li>
                    <li class="msuccess"><a href="#choose_delivery">Choose Delivery</a></li>
                    <li class="active"><a href="#checkout">CHECKOUT</a></li>
                </ul>
            </div>
            <div class="in_f_r">
                <div class="wrapper ">
                    <?php
                    $sum = 0;
//                    echo '<pre>';
//                    print_r($vouchers);
//                    exit;
                    foreach ($vouchers as $voucher) {
                        if ($voucher['is_voucher'] == 1 && $voucher['is_flower'] == 0) {

                            $sum += $voucher["selected_amount"];
                            if ($voucher["voucher_img"] != '')
                                $img = $voucher["voucher_img"];
                            else {
                                $img = BASEURL_OIMG . "unnamed.png";
                            }
                            ?>
                            <div class=" clearfix" style="border-bottom: 1px solid #bdbdbd;padding: 5px; ">
                                <div class="col-1-4">
                                    <div style="text-align:center">
                                        <div style="border:1px solid #cdcdcd;border-radius: 5px;padding: 8px">
                                            <img src="<?php echo $img; ?>" />
                                        </div>
                                        <br/>
                                        <span class="sendinfo"><?php echo $voucher["fname"]; ?></span><br/>
                                        <?php if ($voucher["femail"] != "") { ?>
                                            <span class="sendinfo"><?php echo $voucher["femail"]; ?></span><br/>
                                        <?php } ?>
                                        <?php if ($voucher["fphone"] != "") { ?>
                                            <span class="sendinfo"><?php echo $voucher["fphone"]; ?></span><br/>
                                        <?php } ?>

                                    </div>

                                </div>
                                <div class="col-1-4">
                                    <div style="text-align:center;">
                                        <div style="margin:auto;text-align: center;font-weight: bold;font-family: gotham-book;font-size: 18px;">Total amount</div>                                    
                                        <div class="fontClass" style="padding: 5px;"> Rs.<span><?php echo ($voucher["selected_amount"] * $voucher["quantity"]); ?></span></div>
                                    </div>
                                </div>
                                <div class="col-1-4">
                                    <div style="margin:auto;text-align: center;"><div style="margin:auto;text-align: center;font-weight: bold;font-family: gotham-book;font-size: 18px;">Quantity</div>
                                        <div  class="myquantity">
                                            <div same_quantity_id="<?php echo $voucher["unique_id"]; ?>"  class="incedec minus minus_voucher" field="quantity"   order_pro_id="<?php echo $voucher["unique_id"]; ?>"><img alt="icon" src="<?php echo BASEURL_OIMG . "myminus.png" ?>"/></div>
                                            <div style="float: left;padding: 3px; " ><div style="border: 1px solid rgb(214, 214, 214); text-align: center;width: 30px;height: 20px;"  class="qvalue<?php echo $voucher["unique_id"]; ?>" order_pro_id="<?php echo $voucher["unique_id"]; ?>"><?php echo $voucher["quantity"]; ?> </div> 
                                                <!--<span style="font-size:12px;cursor: pointer;display: none;" id="savequantity<?php // echo $voucher["unique_id"];                                                                   ?>" class="savequantity" same_quantity_id="<?php // echo $voucher["unique_id"];                                                                   ?>" order_pro_id ="<?php // echo $voucher["unique_id"];                                                                   ?>" >Save</span>-->
                                            </div>
                                            <div same_quantity_id="<?php echo $voucher["unique_id"]; ?>" class="incedec plus plus_voucher" field="quantity" order_pro_id="<?php echo $voucher["unique_id"]; ?>" ><img alt="icon" src="<?php echo BASEURL_OIMG . "myplus.png" ?>" /></div>  <br/>                    
                                        </div>

                                    </div>
                                </div>
                                <div class="col-1-4">
                                    <div style="margin:auto;text-align: center;font-weight: bold;font-family: gotham-book;font-size: 18px;">Action</div>                                    

                                    <div class="myaction">
                                        <div style="float:left; cursor: pointer;" class="editcart " title="edit" order_pro_id ="<?php echo $voucher["unique_id"]; ?>"><img alt="edit icon" src="<?php echo BASEURL_OIMG . "edit.png" ?>" /></div>
                                        <div style="float:left; cursor: pointer;" class="delete " title="delete"  same_quantity_id="<?php echo $voucher["unique_id"]; ?>" order_pro_id="<?php echo $voucher["unique_id"]; ?>" ><img alt="remove icon" src="<?php echo BASEURL_OIMG . "remove.png" ?>" /></div>                      
                                    </div>
                                </div>

                            </div>
                            <?php
                        } else if ($voucher['is_voucher'] == 0 && $voucher['is_flower'] == 1) {

                            $sum += $voucher["flower_price"];
                            if ($voucher["flower_image"] != '')
                                $img = BASEURL_PRODUCT_IMG . $voucher["flower_image"];
                            else {
                                $img = BASEURL_PRODUCT_IMG . "unnamed.png";
                            }
                            ?>
                            <div class=" clearfix" style="border-bottom: 1px solid #bdbdbd;padding: 5px; ">
                                <div class="col-1-4">
                                    <div style="text-align:center">
                                        <div style="border:1px solid #cdcdcd;border-radius: 5px;padding: 8px">
                                            <img src="<?php echo $img; ?>" />
                                        </div>
                                        <br/>
                                        <span class="sendinfo"><?php echo $voucher["receiver_name"]; ?></span><br/>
                                        <span class="sendinfo"><?php echo $voucher["receiver_email"]; ?></span><br/>
                                        <span class="sendinfo"><?php echo $voucher["receiver_phone"]; ?></span>

                                    </div>

                                </div>
                                <div class="col-1-4">
                                    <div style="text-align:center;">
                                        <div style="margin:auto;text-align: center;font-weight: bold">Total amount</div>                                    
                                        <div class="fontClass" style="padding: 5px;"> Rs.<span ><?php echo ($voucher["flower_price"] * $voucher["quantity"]); ?></span></div>
                                    </div>
                                </div>
                                <div class="col-1-4">
                                    <div style="margin:auto;text-align: center;"><div style="margin:auto;text-align: center;font-weight: bold">Quantity</div>
                                        <div  class="myquantity">
                                            <div same_quantity_id="<?php echo $voucher["unique_id"]; ?>" order_pro_id="<?php echo $voucher["unique_id"]; ?>"  class="incedec minus_flower" field="quantity" ><img alt="minus" src="<?php echo BASEURL_OIMG . "myminus.png" ?>"/></div>
                                            <div style="float: left;padding: 3px; " ><div style="border: 1px solid rgb(214, 214, 214);text-align: center;width: 30px;height: 20px;"  class="qvalue<?php echo $voucher["unique_id"]; ?>" order_pro_id="<?php echo $voucher["unique_id"]; ?>"><?php echo $voucher["quantity"]; ?> </div> 
                                                <span style="font-size:12px;cursor: pointer;display: none;" class="savequantity" >Save</span>

                                            </div>
                                            <div same_quantity_id="<?php echo $voucher["unique_id"]; ?>" class="incedec plus_flower"  order_pro_id="<?php echo $voucher["unique_id"]; ?>" field="quantity" ><img src="<?php echo BASEURL_OIMG . "myplus.png" ?>" /></div>  
                                            <br/>                    
                                        </div>

                                    </div>
                                </div>
                                <div class="col-1-4">
                                    <div style="margin:auto;text-align: center;font-weight: bold">Action</div>                                    

                                    <div class="myaction">
                                        <div style="float:left; cursor: pointer;" class="editcart_flower" title="edit" order_pro_id="<?php echo $voucher["flower_id"]; ?>" same_quantity_id="<?php echo $voucher["unique_id"]; ?>"><img alt="edit image" src="<?php echo BASEURL_OIMG . "edit.png" ?>" /></div>
                                        <div style="float:left; cursor: pointer;" class="delete1 " title="delete" same_quantity_id="<?php echo $voucher["unique_id"]; ?>" order_pro_id="<?php echo $voucher["flower_id"]; ?>" ><img alt="remove icon" src="<?php echo BASEURL_OIMG . "remove.png" ?>" /></div>                      
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    <?php } ?>

                </div>

                <div class=" wrapper mb20 mt20 text-center" style="font-family: gotham-book;"  onload="submitPayuForm()">
                    <form action="<?php echo $action; ?>" method="post" name="payuForm" >
                        <?php
                        $asum = 0;
                        $sum_new = 0;
                        ?>
                        <?php
                        foreach ($vouchers as $voucher) {
                            $sum_new += $voucher["selected_amount_new"];
                            ?>
                        <?php } ?>
                        <?php
                        $processing_fee = round(($sum_new * 0.035), 2);
                        $total_all = ($sum_new + $processing_fee);
                        $asum = $total_all;
                        ?>
                        <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY; ?>" />
                        <input type="hidden" name="hash" value="<?php echo $hash; ?>"/>
                        <input type="hidden" name="txnid" value="<?php echo $txnid; ?>" />
                        <input name="amount" id="vamount" type="hidden" value="<?php echo $asum; ?>" />
                        <input name="firstname" type="hidden" id="firstname" value="<?php echo $user_first_name; ?>" />
                        <input type="hidden" name="productinfo" value="<?php echo $product_info; ?>">
                        <input type="hidden" name="email" value="<?php echo $user_email; ?>">
                        <input type="hidden" name="phone" value="<?php echo $uphone; ?>">

                        <input name="surl" type="hidden" value="<?php echo BASEURL . "success"; ?>" size="64" />
                        <input name="furl" type="hidden" value="<?php echo BASEURL . "failure"; ?>" size="64" />
                        <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
                        <input type="hidden" name="udf2" value="<?php echo $udf2; ?>" size="64" />
                        <input type="hidden" name="udf1" value="<?php echo $processing_fee; ?>" size="64" />



                        <table style="float:right;font-size: 22px;" class="total-table">
                            <tr style="padding:2px">
                                <td style="text-align:right;color:#000;font-weight: bold;">Sub Total </td>
                                <td style="color:#000;">:</td>
                                <td>Rs.</td>
                                <td style="text-align:right;"><?php echo $sum_new; ?></td>
                            </tr>
                            <tr style="padding:2px">
                                <td style="text-align:right;color:#000;font-weight: bold;">Processing Fee 3.5%</td>
                                <td style="color:#000;">:</td>
                                <td>Rs.</td>
                                <td style="text-align:right;"><?php echo $processing_fee; ?></td>
                            </tr>
                            <tr style="padding:2px" >
                                <td style="text-align:right;color:#000;font-weight: bold;">Total</td>
                                <td style="color:#000;">:</td>
                                <td>Rs.</td>
                                <td style="text-align:right;"><?php echo $total_all; ?></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <?php if (!$hash) { ?>
                                        <div class="arrowbtn" id="confirm_n_pay" style="float:right;margin-top: 20px;">Checkout</div>
                                    <?php } ?>
                                </td>

                            </tr>
                        </table>


                        <div class="alertPopupDiv3 alertPopupDiv">
                            <div class="alertPopupMessage3 alertPopupMessage">
                                <div class="closepopup" id="close_popup"> <img alt="closer icon" src="<?php echo BASEURL_OIMG; ?>closer.png" /> </div>
                                <br/>
                                <br/>
                                <div style="text-align: center; margin: auto auto; width: 100%; height:120px;">
                                    <h3 class="titlemessage">YOU ARE REDIRECTING TO PAYMENT GATEWAY PAGE.</h3>
                                    <br/>
                                    <img alt="birthdayowl" src="<?php echo BASEURL_OIMG . "1.gif" ?>"  />
                                </div>
                                <div class="alertcloses" id="alertclose" style="color:black;font-weight: bold;"></div>
                                <div id="paymentok" style="cursor: pointer;text-align: center; margin: 10px auto auto; color: white; border-radius: 3px; width: 24%; height: 25px; line-height: 26px; background: rgb(92, 173, 1) none repeat scroll 0% 0%;">OK</div>
                            </div>
                        </div>
                        <div class="alertBox"></div>
                    </form>
                </div>
                <div class="">
                    <div style="font-size: 14px; margin: 0 10px;font-family: gotham-book;font-weight: 600;"><b>NOTE:</b> "Birthday Owl will charge a processing fee of 3.5% which will be added to your purchase amount"
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="cd-popup cd-popup-ck" role="alert">
        <div class="cd-popup-container" style="color: black;">
            <p>You've just made a great choice with this <span id="buy_text" style="font-weight: bold;"></span>. Your recipient is going to love it!
                <br/>
                <br/>
                Would you like to add some <span id="not_buy_text" style="font-weight: bold;"></span> to make your gift more special?</p>
            <ul class="cd-buttons">
                <li><a id="yes" style="cursor:pointer;" >Yes</a></li>
                <li><a id="no" style="cursor:pointer;" >No</a></li>
            </ul>
            <!--<a href="#0" class="cd-popup-close img-replace">Close</a>-->
        </div> <!-- cd-popup-container -->
    </div> <!-- cd-popup -->
</div>

<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript">
<?php
$flower_data = count($_SESSION['userdata']['flower_data']);
$voucher_data = count($_SESSION['userdata']['voucher_data']);
?>
    var flower_count = "<?php echo $flower_data; ?>";
    var voucher_count = "<?php echo $voucher_data; ?>";

    $('#yes').click(function () {
        $("body").css("overflow", "auto");
        if (flower_count > 0) {
            window.location = "<?php echo WEB_EGIFTCARD_VOUCHERS; ?>";
        } else if (voucher_count > 0) {
            window.location = "<?php echo WEB_FLOWERS_AND_CAKES; ?>";
        }
    });



    $('#no').click(function (event) {
        $("body").css("overflow", "auto");
        event.preventDefault();
        $('.cd-popup').removeClass('is-visible');

        $(".savequantity").hide();

        $(".alertBox").css("display", "block");
        $(".alertPopupDiv3").css("display", "block");
        $('.alertPopupMessage3').animate({top: '34%'}, 500);
        ShowCurrentTime();
    });

    $(document).ready(function () {
        if (typeof (Storage) != "undefined") {
            localStorage.setItem("add_another_message", "0");
        }

        $(".editcart").click(function () {
            $(".savequantity").hide();
            var order_pro_id = $(this).attr("order_pro_id");
            window.location = "<?php echo BASEURL . "edit_voucher/" ?>" + order_pro_id;

        });

        $(".editcart_flower").click(function () {
            $(".savequantity").hide();
            var order_pro_id = $(this).attr("order_pro_id");
            var same_quantity_id = $(this).attr("same_quantity_id");
            window.location = "<?php echo BASEURL . "flowers_detail/" ?>" + order_pro_id + "/" + same_quantity_id;

        });

//        $('.plus').click(function (e) {
//            e.preventDefault();
//            var order_pro_id = $(this).attr("order_pro_id");
////            $("#savequantity" + order_pro_id).show();
//            var currentVal = parseInt($('#qvalue' + order_pro_id).html());
//            if (currentVal == 10) {
//                $('#qvalue' + order_pro_id).html(currentVal);
//            } else if (!isNaN(currentVal)) {
//                $('#qvalue' + order_pro_id).html(currentVal + 1);
//            } else {
//                $('#qvalue' + order_pro_id).html(1);
//            }
//
//            var same_quantity_id = $(this).attr("same_quantity_id");
//            var quantity = $("#qvalue" + order_pro_id).html();
//
//
//            var data = {
//                order_pro_id: order_pro_id,
//                same_quantity_id: same_quantity_id,
//                quantity: quantity
//            }
//            $.ajax({
//                type: "POST",
//                url: "<?php // echo BASEURL;                                                                     ?>Home_web/save_quantity",
//                data: data,
//                dataType: "json",
//                beforeSend: function () {
//                    $(".showloader").addClass("show_overlay").show();
//                },
//                success: function (r) {
//                    $(".showloader").removeClass("show_overlay").hide();
//
//                    if (r.success.toString() === "false") {
//                        alert(r.message.toString());
//                        window.location = "<?php // echo BASEURL;                                                                     ?>";
//                        return false;
//                    } else {
//                        window.location = "<?php // echo BASEURL;                                                                     ?>cart";
//
//                    }
//                }
//            });
//        });

        $('.plus_flower').click(function (e) {
            e.preventDefault();
            var order_pro_id = $(this).attr("order_pro_id");
//            $("#savequantity" + order_pro_id).show();
            var currentVal = parseInt($('.qvalue' + order_pro_id).html());
            if (currentVal == 10) {
                $('.qvalue' + order_pro_id).html(currentVal);
            } else if (!isNaN(currentVal)) {
                $('.qvalue' + order_pro_id).html(currentVal + 1);
            } else {
                $('.qvalue' + order_pro_id).html(1);
            }

            var same_quantity_id = $(this).attr("same_quantity_id");
            var quantity = $(".qvalue" + order_pro_id).html();


            var data = {
                same_quantity_id: same_quantity_id,
                quantity: quantity
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>flower-qty",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    $(".showloader").removeClass("show_overlay").hide();
                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        window.location = "<?php echo WEB_HOME; ?>";
                        return false;
                    } else {
                        window.location = "<?php echo WEB_SHOW_CART; ?>";

                    }
                }
            });
        });

        $('.plus_voucher').click(function (e) {
            e.preventDefault();
            var order_pro_id = $(this).attr("order_pro_id");
//            $("#savequantity" + order_pro_id).show();
            var currentVal = parseInt($('.qvalue' + order_pro_id).html());
            if (currentVal == 10) {
                $('.qvalue' + order_pro_id).html(currentVal);
            } else if (!isNaN(currentVal)) {
                $('.qvalue' + order_pro_id).html(currentVal + 1);
            } else {
                $('.qvalue' + order_pro_id).html(1);
            }

            var same_quantity_id = $(this).attr("same_quantity_id");
            var quantity = $(".qvalue" + order_pro_id).html();

            var data = {
                same_quantity_id: same_quantity_id,
                quantity: quantity
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>voucher-qty",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    $(".showloader").removeClass("show_overlay").hide();
                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        window.location = "<?php echo WEB_HOME; ?>";
                        return false;
                    } else {
                        window.location = "<?php echo WEB_SHOW_CART; ?>";

                    }
                }
            });
        });
        // This button will decrement the value till 0
//        $(".minus").click(function (e) {
//            var order_pro_id = $(this).attr("order_pro_id");
//            e.preventDefault();
//            var currentVal = parseInt($('#qvalue' + order_pro_id).html());
//            if (!isNaN(currentVal) && currentVal > 1) {
////                $("#savequantity" + order_pro_id).show();
//
//                $('#qvalue' + order_pro_id).html(currentVal - 1);
//                var same_quantity_id = $(this).attr("same_quantity_id");
//                var quantity = $("#qvalue" + order_pro_id).html();
//                var data = {
//                    order_pro_id: order_pro_id,
//                    same_quantity_id: same_quantity_id,
//                    quantity: quantity
//                }
//                $.ajax({
//                    type: "POST",
//                    url: "<?php // echo BASEURL;                                                                        ?>Home_web/save_quantity",
//                    data: data,
//                    dataType: "json",
//                    beforeSend: function () {
//                        $(".showloader").addClass("show_overlay").show();
//                    },
//                    success: function (r) {
//                        $(".showloader").removeClass("show_overlay").hide();
//
//                        if (r.success.toString() === "false") {
//                            alert(r.message.toString());
//                            window.location = "<?php // echo BASEURL;                                                                        ?>";
//                            return false;
//                        } else {
//                            window.location = "<?php // echo BASEURL;                                                                        ?>cart";
//
//                        }
//                    }
//                });
//
//            } else {
//                $('#qvalue' + order_pro_id).html(1);
//            }
//        });

        // This button will decrement the value till 0
        $(".minus_voucher").click(function (e) {
            var order_pro_id = $(this).attr("order_pro_id");
            e.preventDefault();
            var currentVal = parseInt($('.qvalue' + order_pro_id).html());
            if (!isNaN(currentVal) && currentVal > 1) {
//                $("#savequantity" + order_pro_id).show();

                $('.qvalue' + order_pro_id).html(currentVal - 1);
                var same_quantity_id = $(this).attr("same_quantity_id");
                var quantity = $(".qvalue" + order_pro_id).html();
                var data = {
                    order_pro_id: order_pro_id,
                    same_quantity_id: same_quantity_id,
                    quantity: quantity
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>voucher-qty",
                    data: data,
                    dataType: "json",
                    beforeSend: function () {
                        $(".showloader").addClass("show_overlay").show();
                    },
                    success: function (r) {
                        $(".showloader").removeClass("show_overlay").hide();

                        if (r.success.toString() === "false") {
                            alert(r.message.toString());
                            window.location = "<?php echo WEB_HOME; ?>";
                            return false;
                        } else {
                            window.location = "<?php echo WEB_SHOW_CART; ?>";

                        }
                    }
                });

            } else {
                $('#qvalue' + order_pro_id).html(1);
            }
        });

        $(".minus_flower").click(function (e) {
            var order_pro_id = $(this).attr("order_pro_id");
            e.preventDefault();
            var currentVal = parseInt($('.qvalue' + order_pro_id).html());
            if (!isNaN(currentVal) && currentVal > 1) {
//                $("#savequantity" + order_pro_id).show();

                $('.qvalue' + order_pro_id).html(currentVal - 1);
                var same_quantity_id = $(this).attr("same_quantity_id");
                var quantity = $(".qvalue" + order_pro_id).html();
                var data = {
                    same_quantity_id: same_quantity_id,
                    quantity: quantity
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo BASEURL; ?>flower-qty",
                    data: data,
                    dataType: "json",
                    beforeSend: function () {
                        $(".showloader").addClass("show_overlay").show();
                    },
                    success: function (r) {
                        $(".showloader").removeClass("show_overlay").hide();

                        if (r.success.toString() === "false") {
                            alert(r.message.toString());
                            window.location = "<?php echo WEB_HOME; ?>";
                            return false;
                        } else {
                            window.location = "<?php echo WEB_SHOW_CART; ?>";

                        }
                    }
                });

            } else {
                $('.qvalue' + order_pro_id).html(1);
            }
        });
    });

//    $(document).on("click", ".savequantity", function () {
//        var order_pro_id = $(this).attr("order_pro_id");
//        var same_quantity_id = $(this).attr("same_quantity_id");
//        var quantity = $("#qvalue" + order_pro_id).html();
//
//
//        var data = {
//            order_pro_id: order_pro_id,
//            same_quantity_id: same_quantity_id,
//            quantity: quantity
//        }
//        $.ajax({
//            type: "POST",
//            url: "<?php // echo BASEURL;                                                                 ?>Home_web/save_quantity",
//            data: data,
//            dataType: "json",
//            beforeSend: function () {
//                $(".showloader").addClass("show_overlay").show();
//            },
//            success: function (r) {
//                $(".showloader").removeClass("show_overlay").hide();
//
//                if (r.success.toString() === "false") {
//                    alert(r.message.toString());
//                    window.location = "<?php // echo BASEURL;                                                                 ?>";
//                    return false;
//                } else {
//                    window.location = "<?php // echo BASEURL;                                                                 ?>cart";
//
//                }
//            }
//        });
//    });
    var total = parseInt($("#sum").attr("sum"));
    $("#total").text(total);
    $(document).on("click", "#confirm_n_pay", function () {
        $("body").css("overflow", "hidden");
        if ((flower_count > 0) && (voucher_count > 0)) {
            $(".savequantity").hide();

            $(".alertBox").css("display", "block");
            $(".alertPopupDiv3").css("display", "block");
            $('.alertPopupMessage3').animate({top: '34%'}, 500);
            ShowCurrentTime();
        } else {
            $('.cd-popup').addClass('is-visible');
            $('.cd-popup-un').hide();
            $('.cd-popup-add').hide();
//        setTimeout(function () {
//            $('.cd-popup').addClass('is-visible');
//        }, 5000);
            if (flower_count > 0) {
                $('#buy_text').html('Flowers & Cakes');
                $('#not_buy_text').html('e-Gift Cards');
            } else if (voucher_count > 0) {
                $('#buy_text').html('e-Gift Cards');
                $('#not_buy_text').html('Flowers & Cakes');
            }
        }
    });
    $("#close_popup").click(function () {
        $('.alertPopupMessage3').animate({top: '0'}, 500);
        $(".alertBox").css("display", "none");
        $(".alertPopupDiv3").css("display", "none");
        window.location = "<?php echo WEB_SHOW_CART; ?>";
    });

//                    $(document).on("click", "#alertclose", function () {
//                        var payuForm = document.forms.payuForm;
//                        payuForm.submit();
//                    });

    var hash = '<?php echo $hash ?>';
    submitPayuForm();
    function submitPayuForm() {
        if (hash == '') {
            return;
        }
        var payuForm = document.forms.payuForm;
        payuForm.submit();
    }

    $("#paymentok").click(function () {
        var payuForm = document.forms.payuForm;
        payuForm.submit();
    });

    var i = 0;
    function ShowCurrentTime() {
        var dt = new Date();
        $(".alertcloses").html(5 - i + " seconds");
        i++;
        if (i == 5) {
            var payuForm = document.forms.payuForm;
            payuForm.submit();
        }
        window.setTimeout("ShowCurrentTime()", 1000);
    }


    var count = "<?php echo $gift_count; ?>";
    if (count == "1") {

        $('.right_v ul').bxSlider({
            nextText: '',
            prevText: '',
            pager: true,
            auto: false,
            speed: 1000,
            mode: 'fade'
        });
        $(".delete").click(function () {
            var same_quantity_id = $(this).attr("same_quantity_id");

            var data = {
                same_quantity_id: same_quantity_id,
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>voucher-delete",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    $(".showloader").removeClass("show_overlay").hide();

                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        return false;
                    } else {
                        alert(r.message.toString());
                        window.location = "<?php echo WEB_HOME; ?>";

                    }
                }
            });
        });
        $(".delete1").click(function () {
            var same_quantity_id = $(this).attr("same_quantity_id");

            var data = {
                same_quantity_id: same_quantity_id,
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>flower-delete",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    $(".showloader").removeClass("show_overlay").hide();

                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        return false;
                    } else {
                        alert(r.message.toString());
                        window.location = "<?php echo WEB_HOME; ?>";

                    }
                }
            });
        });
    } else {
        $('.right_v ul').bxSlider({
            nextText: '',
            prevText: '',
            pager: true,
            auto: true,
            speed: 1000,
            mode: 'fade'
        });
        $(".delete").click(function () {
            $(".savequantity").hide();

            var same_quantity_id = $(this).attr("same_quantity_id");

            var data = {
                same_quantity_id: same_quantity_id,
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>voucher-delete",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    $(".showloader").removeClass("show_overlay").hide();

                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        return false;
                    } else {
                        alert(r.message.toString());
                        window.location = "<?php echo WEB_SHOW_CART; ?>";



                    }
                }
            });
        });
        $(".delete1").click(function () {
            $(".savequantity").hide();
            var same_quantity_id = $(this).attr("same_quantity_id");

            var data = {
                same_quantity_id: same_quantity_id,
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>flower-delete",
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $(".showloader").addClass("show_overlay").show();
                },
                success: function (r) {
                    $(".showloader").removeClass("show_overlay").hide();
                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        return false;
                    } else {
                        alert(r.message.toString());
                        window.location = "<?php echo WEB_SHOW_CART; ?>";
                    }
                }
            });
        });
    }

</script> 
<?php // $this->load->view("new_templates/freegreeting_footer");  ?>

