<?php $this->load->view("new_templates/header"); ?>    

<style>
    .greeting_full
    {
        margin-top: 0px !important;
    }
    .dashbord_nav1 li:hover:before, .dashbord_nav1 li.active:before
    {
        content: none !important;
    }
    .dashbord_nav1 li a
    {
        font-size: 1.0em !important;
    }
    .esub{
        position: relative;
        top:-148px;
        text-align:center;
    }
    .esubemail{
        width: 68% !important;
        margin-top: 32px; height: 26px;
    }


    .msgarea{
        word-wrap: break-word;
        height: 230px;
        width: 100%;
    }
    .display_msg{
        width: 100%;
    }
    .photoupload{
        height: 184px;
        margin-top:159px;
        width: 100%;
    }
    .backcorners{
        width: 100%;
        height: 219px;
    }
    .photo_img{
        margin-top: -186px;
    }
    .sampleimg{
        height: 182px;
        margin-top: -19px;
        width: 66%;
    }

    .video_container{

        height: 181px;
        position: absolute;
        width: 100%;
    }
    .stopVideo{
        color: white;
        left: 74%;
        position: absolute;
        top: 14px;
        z-index: 30;

    }

    .display_video{
        background: #000 none repeat scroll 0 0;
        bottom: 0;
        height: 106%;
        left: -6%;
        overflow: hidden;
        position: relative;
        top: -11%;
        width: 79%;
    }
    .bredcumb li { display:inline-block;  }
    .bredcumb li:after { content:'/'; margin:0 10px}
    .bredcumb li:last-child:after { display:none;}
    .bredcumb li a { color:#575757;}
    .bredcumb li a.active { color:#D30003;}
    .display_audio{
        bottom: 0;
        height: 106%;
        left: -10%;
        overflow: hidden;
        position: relative;
        top: -80%;
        width: 67%;
    }
    .freegreeting_links1{
        margin-top: -48px; 
        padding:5px;
    }
    .glink{
        width: 98%;
    }
    .glink li a {
        color:black;
    }
    .brown_color{
        background-color: beige;
    }
    .outer{
        width:75%;
    }
	
	.leftside span {
		left: 0 !important;
		right: 0;
		margin: auto;
		display: block;
		text-align: center;
		margin-bottom: 15px;
	}
	
	.leftside br{
		display: none;
	}

    @media only screen and (max-width:768px)
    {
        .r_con{
            height: 424px;
            margin-left: 22px;

        }

        .msgarea{
            height: 186px;
        }

        .backcorners{
            height: 188px;
        }
        .sampleimg{
            width: 72%;
            height: 148px;
            margin-top:17px;
        }
        .photoupload{
            margin-top: 176px;
        }
        .display_video{
            height: 90%;
            left: -8%;
            top: 5%;
            width: 77%;
        }
        .newletter_owl{
            position:static;
        }

        .esubemail{
            width: 34% !important;
            margin-top:17px;
        }
    }   
    @media only screen and (max-width:600px)
    {
        .display_video{

            left: -10%;

            width: 74%;
        }

        .esubemail{
            width: 44% !important;
            margin-top:17px;
        }


    }
    @media only screen and (max-width:480px)
    {
        .display_video{

            left: -11%;

            width: 71%;
        }
        .esubemail{
            width: 58% !important;

        }

    }
    @media only screen and (max-width:350px)
    {
        .display_video{
            left: -17%;
            width: 59%;
        }
    }
	
	@media only screen and (max-width: 991px){
		
		.greeting_full .g_left img {
			width: 50%;
			max-width: 100%;
			margin: auto;
			padding-bottom: 10px;
			display: block;
		}
		
		.subscriber-cont {
			border: 0px solid #000;
			max-width: 315px;
			margin: auto;
		}
		
		.greeting_full .r_con{
			height: 300px;
			margin-left: 0;
		}
		
		.msgarea{
			font-size: 18px;
		}
		
		a.pdfdownload {
			padding: 0 !important;
		}

		a.pdfdownload img {
			top: 0 !important;
			margin-left: 10px;
		}
		
	}
	
	@media only screen and (max-width: 767px){
		
		
		
		.subscriber-img {
			width: 100%;
			padding: 0 15px;
		}
		
		.greeting_full .r_con{
			height: 200px;
			margin-left: 0;
		}
		
		.msgarea{
			font-size: 15px;
			margin-top: 0 !important;
		}
		
		a.pdfdownload {
			padding: 0 !important;
		}

		a.pdfdownload img {
			top: 0 !important;
			margin-left: 10px;
		}
		
	}
	
</style>

<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            It`s Your <span>Birthday Greeting Card &amp; Voucher</span>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <div class="inner_midd">
            <div class="in_f_l">
                <div class="subscriber-cont">
                    <img src="<?php echo BASEURL_OIMG . "sub2.png" ?>"  class="subscriber-img"/>
                    <div class='esub'>
                        <input type='email' class='esubemail' id="nsemail" name="email_id" style='border-radius: 9px;'/>
                        <div class="error_n3" style="color: red; width: 105px; margin: auto;"></div>
                        <button id='esubscribe' class="esubscribe" style="right:0%"></button>
                        <div style="font-size:12px;color: white">OR</div>
                        <div style="font-size:12px;color: white">like and follow us on <a  style="font-family:FontAwesome;color: white" href="https://www.facebook.com/birthdayowl/" target="_blank">&#xf09a;</a></div>
                    </div>
                </div> 
            </div>

            <div class="in_f_r">
                <div class="wrapper">
                    <div class="greeting_full">
                        <div class="g_left">
                            <img src="<?php echo BASEURL_CROPPED_GREET . $gift_data['front_page_image']; ?>" />
                        </div>
                        <div class="g_right">
                            <div class="r_con scroll_issue">
                                <div class="msgarea">
                                    <div class="display_msg">
                                        <?php echo nl2br(trim(strip_tags($gift_data['greeting_message'], '<br/>'))); ?>
                                    </div>
                                </div>
                                <?php if ($gift_data['type'] == 1 || $gift_data['type'] == 2 || $gift_data['type'] == 3) { ?>
                                    <div class="photo_img">
                                        <?php if ($gift_data['type'] == 1) { ?>
                                            <div class="photoupload" >
                                                <img src="<?php echo BASEURL_OIMG ?>imgframe.png" class="backcorners"/>
                                                <div class="photo_img">
                                                    <img src="<?php echo BASEURL_BVMG . $gift_data['uploded_img_video'] ?>" id="defaultpic" class="sampleimg" >
                                                </div>
                                            </div>
                                        <?php } else if ($gift_data['type'] == 2) { ?>
                                            <div class="photoupload" >
                                                <img src="<?php echo BASEURL_OIMG ?>imgframe.png" class="backcorners"/>
                                                <div class="photo_img">
                                                    <div class="video_container">
                                                        <video src="<?php echo BASEURL_BVMG . $gift_data['uploded_img_video'] ?>" controls id="videof" class="display_video" autoplay="">
                                                        </video>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } else if ($gift_data['type'] == 3) { ?>
                                            <div class="photoupload"  style="margin-top:0px;">
                                                <img src="<?php echo BASEURL_OIMG ?>imgframe.png" class="backcorners"/>
                                                <div class="photo_img">

                                                    <div class="video_container">
                                                        <audio src="<?php echo BASEURL_BVMG . $gift_data['uploded_img_video'] ?>" controls id="videof" class="display_audio" autoplay="">
                                                            Your browser does not support the audio element.
                                                        </audio> 

                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>

                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div style="color: #000; margin-top: 40px; font-size: 18px; margin-left: 5px;">
                        From: <span> <?php echo $gift_data['uname']; ?>(<?php echo $gift_data['uemail']; ?>)</span>
                    </div>
                    <div style="margin-top: 20px; font-size: 18px; margin-left: 5px;">
                        <a href="<?php echo BASEURL . "Home_web/download_pdf/" . $order_pro_id . "/2" ?>" class="pdfdownload" style="color: #000;">Download e-card as a pdf
                            <img src='<?php echo BASEURL_OIMG; ?>pdf.png' style="top: 20px; position: relative;"/></a> 
                    </div>
                </div>
            </div>
            <div class="in_f_l" >
                <div class="leftside" style="height:383px;" >
                    <div style="text-align: center;height:30px;line-height: 30px;font-weight: 500;">Your Voucher</div>
                    <div style="text-align: center; height:132px;"><img src="<?php echo $gift_data["voucher_img"]; ?>" /></div>
                    <div style="text-align: center;height: 30px;font-family: cursive;"><?php echo $gift_data["voucher_pro_name"]; ?></div>
                    <div style="text-align: center;font-weight: bold;font-size: 28px;height: 37px;">Rs. &nbsp;<?php echo $gift_data["selected_amount"] ?></div>
                    <?php if ($gift_data["expiry_date"] != "") { ?>
                        <?php $expiry_date = date("d F, Y", strtotime($gift_data["expiry_date"])); ?>
                        <div style="text-align: center;height: 37px;">valid until <?php echo $expiry_date; ?></div>

                    <?php } ?>
                    <div style="text-align: center;height: 37px;">
                        <img src="<?php echo BASEURL_OIMG; ?>egift.png" />
                    </div>
                    <span  style="position: relative; left: 58px; top: 14px;font-size:12px;"><?php echo $gift_data["voucher_code"]; ?></span><br/><br/>
                    <?php if ($gift_data["pin"] != '') { ?>
                        <span  style="position: relative; left: 58px; top: 14px;font-size:12px;"><b>PIN:</b><?php echo $gift_data["pin"]; ?></span>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script type="text/javascript">
    $(document).ready(function () {
        send_gift_open_mail();
//        send_thankyouemail();
    });

<?php if ($gift_data['type'] != 1 && $gift_data['type'] != 2) { ?>
        $(".msgarea").css("margin-top", "95px");
<?php } ?>

    $("#esubscribe").click(function () {
        var semail = $("#nsemail").val();
        if (semail == '') {
            $(".error_n3").html("Enter Email");
        } else if ((semail != '') && (validateEmail(semail) == false)) {
            $(".error_n3").show();
            alert("Not a Valid Email Address");
        } else if ((semail != '') && (validateEmail(semail) == true)) {
            var data = {
                email_id: semail
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>newsletter",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        $("#message").html(r.message.toString());
                        return false;
                    } else {
                        $("#semail").val('');
                        alert(r.message.toString());


                    }
                }
            });
        }
    });

    function send_gift_open_mail() {
    }

    function send_thankyouemail() {
        var data = {
            order_pro_id: '<?php echo $order_pro_id; ?>'
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>Home_web/sendegiftThankMail",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() == "false") {
                    return false;
                } else {

                    $("#nsemail").val('');
                }
            }
        });


    }

    $("#nsemail").keyup(function () {
        $(".error_n3").html("");
    })
</script>

