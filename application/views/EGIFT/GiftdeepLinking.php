<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
         <script type="text/javascript" src="<?php echo BASEURL_OJS ?>redirect.js"></script>
        <title></title>
    </head>
    <body>
        <p>Redirecting...</p>
        <script>
         
                var qs = AppRedirect.queryString;

                AppRedirect.redirect({
                    iosApp: "birthdayowl://EgiftReceived?<?php echo $order_pro_id; ?>/<?php echo $user_id; ?>",
                    iosAppStore: "<?php echo WEB_DASHBOARD; ?>",
                    // For this, your app need to have category filter: android.intent.category.BROWSABLE
                    android: {
                        'host': "EgiftReceived/<?php echo $order_pro_id; ?>/<?php echo $user_id; ?>",
                        'scheme': 'birthdayowl', // Scheme part in a custom scheme URL
                        'package': 'com.tech.birthdayowl', // Package name in Play store
                        'fallback': 'https://play.google.com/store/apps/details?id=com.tech.birthdayowl&hl=en'
                    }

                });
          
        </script>
    </body>
</body>
</html>

