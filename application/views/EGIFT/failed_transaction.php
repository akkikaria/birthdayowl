<?php $this->load->view("new_templates/header"); ?>
<?php
//echo '<pre>';
//print_r($orderdata);
//exit;
?>
<style>
    .pro_img{
        left: 37%;
        position: absolute;
        top: 19%;
    }
    .recname{
        color: white;font-size: 14px;  text-align: left;     margin-left: 26px;  width: 100%;    height: 13px;
    }

    .recemail{
        color: white; width: 100%;   text-align: left;    margin-left: 26px; height: 13px;font-size: 14px;
    }
    .recschedule{
        text-align: left;    margin-left: 26px;  width: 100%;    height: 13px; color: white;font-size: 14px;  
    }

    .scart{
        background-color: gray;height:229px; width:100%;   border-radius: 12px;
    }
    .namelist{
        padding-left: 58px;
        padding-top: 5px;

    }
    .namelist li{
        list-style: disc;
        height: 30px;

    }
    @media only screen and (max-width: 768px) {
        .scart{
            margin: auto;
            width: 50%;
        }

    }

    @media only screen and (max-width:480px)
    {


        .scart{

            width: 100%;
        }

    }




</style>


<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            Payment <span>Failed! </span>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <div class="inner_midd">
            <div class="in_f_l">
                <ul class="leftside leftside_nav">
                    <li class="psuccess"><a href="#pick_amount">Pick Amount</a></li>
                    <li class="psuccess"><a href="#make_it_personal">Make It Personal</a></li>
                    <li class="psuccess"><a href="#choose_delivery">Choose Delivery</a></li>
                    <li class="psuccess"><a href="#checkout">CHECKOUT</a></li>
                </ul>
            </div>
            <div class="in_f_r">
                <div class="wrapper ">

                    <div style="width: 100%;  height: 150px;">
                        <?php
                        $tamount = 0;
                        $receiver_names = array();

                        for ($i = 0; $i < count($orderdata); $i++) {

                            $tamount += $orderdata[$i]["selected_amount"];
                            $receiver_names[$i] = $orderdata[$i]["fname"];
                        }
                        $names = implode(",", $receiver_names);
                        ?>
                        <div style="font-size:22px;height: 50px;line-height: 44px;">We're Sorry!</div>

                        <div style="border-bottom: 1px solid #bdbdbd;text-align: center;"></div>

                        <ul>

                            <li style="font-size:18px;height: 30px;padding-top: 10px;"><img src="<?php echo BASEURL_OIMG; ?>checkmark.png"  style="width:20px;"/><span style="height:20px;padding-left: 10px">Seems like something didn't work out and your payment did not get through to us.</span></li>
                            <li style="font-size:18px;height: 30px;padding-top: 10px;"><img src="<?php echo BASEURL_OIMG; ?>checkmark.png" style="width:20px;"/><span style="height:20px;padding-left: 10px">Please try again or try an alternate payment method.</span></li>

                        </ul>



                    </div>
                </div>
            </div>
            <div class="in_f_l">
                <div class="leftside">
                    <div class="right_v">
                        <h4>
                            YOUR ORDER
                        </h4>
                        <ul style="height: 95px; display: block; background-color:gray;border-radius:12px; width: 100%;">
                            <div style="color: white; padding-bottom: 90px;">
                                <h2>Ooops Sorry...!!!</h2>
                            </div>
                            <?php
                            $sum = 0;
                            foreach ($orderdata as $voucher) {
                                $sum += $voucher["selected_amount"];
                                ?>

                                <li style="width:100%;">
                                    <abbr><?php echo $voucher["voucher_pro_name"]; ?></abbr>
                                    <div class='scart'>

                                        <div style="overflow: hidden; transform: rotate(-8deg); padding-top: 18px; margin-left: 24px; width: 39%;" >
                                            <img src="<?php echo BASEURL_CROPPED_GREET . $voucher["front_page_image"]; ?>" style=" height:128px;"/> 
                                        </div>

                                        <div class="pro_img">
                                            <?php
                                            if ($voucher["voucher_img"] != '')
                                                $img = $voucher["voucher_img"];
                                            else {
                                                $img = BASEURL_OIMG . "unnamed.png";
                                            }
                                            if (count($orderdata) == 0) {
                                                $img = BASEURL_OIMG . "unnamed.png";
                                            }
                                            ?>
                                            <img src="<?php echo $img; ?>"  style="width: 93%; height: 68px;" />
                                        </div>

                                        <div >
                                            <?php if ($voucher['femail'] != NULL) { ?>
                                                <p style="text-align: center; font-weight: bold; font-size: 11px;color: white;">Delivery By Email</p> 
                                            <?php } else { ?>
                                                <p style="text-align: center; font-weight: bold; font-size: 11px;color: white;">Delivery By SMS</p> 
                                            <?php } ?>
                                            <div class='recname' style="background: transparent url('../public/assets/images/user.png') no-repeat scroll 0px 0px / 13px 14px; "><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["fname"]; ?></span></div>
                                            <?php if ($voucher["femail"] == "") { ?>
                                                <div class='recemail'style=" background: transparent url('../public/assets/images/smm_greyb.png') no-repeat scroll 0px 0px  / 13px 14px;"><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["fphone"]; ?></span></div>
                                            <?php } else { ?>
                                                <div class='recemail'style=" background: transparent url('../public/assets/images/email.png') no-repeat scroll 0px 0px  / 13px 14px;"><span style=" height: 13px; line-height: 10px;  margin-left: 27px;  "><?php echo $voucher["femail"]; ?></span></div>
                                            <?php } ?>

                                            <?php
                                            $schedule = $voucher["delievery_schedule"];
                                            $status = "";
                                            if ($schedule == "0") {
                                                $status = "Immediately";
                                            } else if ($schedule == "1") {
                                                $status = "Today";
                                            } else if ($schedule == "2") {
                                                $status = "Tomorrow";
                                            } else if ($schedule == "3") {
                                                $status = "Day after tomorrow";
                                            } else if ($schedule == "4") {
                                                $status = date("M d,Y", strtotime($voucher["delivery_date_time"]));
                                            }
                                            ?>
                                            <div  class='recschedule' style=" background: transparent url('../public/assets/images/clk.png') no-repeat scroll 0px 0px / 13px 14px;"><span style=" height: 13px; line-height: 10px; margin-left: 27px;   "><?php echo $status; ?></span></div>
                                        </div>
                                    </div>

                                </li>
                            <?php } ?>

                        </ul>

                    </div>
                </div>
                <div class="leftside" style="margin-top: 10px; padding: 10px; line-height: 25px;display: none;">
                    <div class="daysalign1">
                        <span>Delivery by Email</span>
                        <br />
                        <span>abdul</span>
                        <br />
                        <span>abd@gmail.com</span>
                        <br />
                        <span>Feb 24, 2016 3:44 PM</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<link href="<?php echo BASEURL_OCSS; ?>jquery.loader.css" rel="stylesheet" />  <!--page loading -->
<script src="<?php echo BASEURL_BJS; ?>my.loader.js"></script>
<script>
    $(document).ready(function () {
        if (typeof (Storage) != "undefined") {
            localStorage.setItem("add_another_message", "0");
        }
    });

    var count = "<?php echo $gift_count; ?>";

    if (count == "1") {

        $('.right_v ul').bxSlider({
            nextText: '',
            prevText: '',
            pager: true,
            auto: false,
            speed: 1000,
            mode: 'fade'
        });
        $(".delete").click(function () {
            var order_pro_id = $(this).attr("order_pro_id");
            $.loader({
                className: "myloader",
                content: ''
            });
            var data = {
                order_pro_id: order_pro_id,
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>voucher-delete",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        return false;
                    } else {
                        var seconds = 5;
                        setInterval(function () {
                            seconds--;
                            if (seconds == 0) {
                                $.loader('close');
                                alert(r.message.toString());
                                window.location = "<?php echo WEB_HOME; ?>";
                            }
                        }, 1000);

                    }
                }
            });
        });
    } else {
        $('.right_v ul').bxSlider({
            nextText: '',
            prevText: '',
            pager: true,
            auto: true,
            speed: 1000,
            mode: 'fade'
        });
        $(".delete").click(function () {
            var order_pro_id = $(this).attr("order_pro_id");
            $.loader({
                className: "myloader",
                content: ''
            });
            var data = {
                order_pro_id: order_pro_id,
            }
            $.ajax({
                type: "POST",
                url: "<?php echo BASEURL; ?>voucher-delete",
                data: data,
                dataType: "json",
                success: function (r) {
                    if (r.success.toString() === "false") {
                        alert(r.message.toString());
                        return false;
                    } else {
                        var seconds = 5;
                        setInterval(function () {
                            seconds--;
                            if (seconds == 0) {
                                $.loader('close');

                                alert(r.message.toString());

                                window.location = "<?php echo WEB_SHOW_CART; ?>";

                            }
                        }, 1000);


                    }
                }
            });
        });
    }

</script> 

