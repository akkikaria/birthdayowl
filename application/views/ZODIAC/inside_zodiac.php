<?php $this->load->view("new_templates/header"); ?>

<link href="<?PHP echo BASEURL_OCSS ?>inside_zodiac_css.css" rel="stylesheet" type="text/css" media="all" />

<style type="text/css">

    @media only screen and (max-width:480px)
    {
        .container_12 .grid_2  {
            width: 27%
        }
        select{
            font-size: 12px;
        }
    }

</style>
<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            <span><?php echo $zodiac_sign; ?></span>
        </div>
    </div>
</div>

<div class="wrapper">
    <div class="container_12">
        <div class="inner_midd dashbord">
            <div class="in_f_l zodiac_tab">
                <ul style="font-family: gotham-book;">
                    <li class="Aries " style="cursor: pointer" ><a zodiac_id="1" id="zodiac_id1"  ><span>Aries</span></a></li>
                    <li class="Taurus" style="cursor: pointer"><a zodiac_id="2" id="zodiac_id2"><span>Taurus</span></a></li>
                    <li class="Gemini" style="cursor: pointer"><a zodiac_id="3" id="zodiac_id3"><span>Gemini</span></a></li>
                    <li class="Cancer" style="cursor: pointer"><a zodiac_id="4" id="zodiac_id4"><span>Cancer</span></a></li>
                    <li class="Leo" style="cursor: pointer"><a zodiac_id="5" id="zodiac_id5"><span>Leo</span></a></li>
                    <li class="Virgo" style="cursor: pointer"><a zodiac_id="6" id="zodiac_id6"><span>Virgo</span></a></li>
                    <li class="Libra" style="cursor: pointer"><a zodiac_id="7" id="zodiac_id7"><span>Libra</span></a></li>
                    <li class="Scorpio" style="cursor: pointer"><a zodiac_id="8" id="zodiac_id8"><span>Scorpio</span></a></li>
                    <li class="Sagittarius" style="cursor: pointer"><a zodiac_id="9" id="zodiac_id9"><span>Sagittarius</span></a></li>
                    <li class="Capricorn" style="cursor: pointer"><a zodiac_id="10" id="zodiac_id10"><span>Capricorn</span></a></li>
                    <li class="Aquarius" style="cursor: pointer"><a zodiac_id="11" id="zodiac_id11"><span>Aquarius</span></a></li>
                    <li class="Pisces" style="cursor: pointer"><a zodiac_id="12" id="zodiac_id12"><span>Pisces</span></a></li>
                </ul>
            </div>
            <div class="in_f_r">

                <?php if ($zodiac_id == 1) { ?>
                    <script>
                        var name = "Aries";
                        $(".Aries").addClass('active');
                        $("#zname").text(name);
                    </script>
                    <div class="horoscope_image">
                        <img src="<?php echo BASEURL_IMG; ?>zodiac/Aries.png" />
                        <abbr style="font-family: gotham-book;">Aries <span>(March 21 - April 19)</span></abbr>

                    </div>
                <?php } else if ($zodiac_id == 2) { ?>
                    <script>
                        var name = "Taurus";
                        $(".Taurus").addClass('active');
                        $("#zname").text(name);
                    </script>

                    <div class="horoscope_image">
                        <img src="<?php echo BASEURL_IMG; ?>zodiac/taurus.png" />
                        <abbr style="font-family: gotham-book;">Taurus <span>(April 20 - May 20)</span></abbr>
                    </div>


                <?php } else if ($zodiac_id == 3) { ?>
                    <script>
                        var name = "Gemini";
                        $(".Gemini").addClass('active');
                        $("#zname").text(name);
                    </script>
                    <div class="horoscope_image">
                        <img src="<?php echo BASEURL_IMG; ?>zodiac/gemini.png" />
                        <abbr style="font-family: gotham-book;">Gemini <span>(May 21 - June 20)</span></abbr>
                    </div>

                <?php } else if ($zodiac_id == 4) { ?>
                    <script>
                        var name = "Cancer";
                        $(".Cancer").addClass('active');
                        $("#zname").text(name);
                    </script>

                    <div class="horoscope_image">
                        <img src="<?php echo BASEURL_IMG; ?>zodiac/cancer.png" />
                        <abbr style="font-family: gotham-book;">Cancer <span>(June 21 - July 22)</span></abbr>
                    </div>
                <?php } else if ($zodiac_id == 5) { ?>
                    <script>
                        var name = "Leo";
                        $(".Leo").addClass('active');
                        $("#zname").text(name);
                    </script>
                    <div class="horoscope_image">
                        <img src="<?php echo BASEURL_IMG; ?>zodiac/leo.png" />
                        <abbr style="font-family: gotham-book;">Leo <span>(July 23 - August 22)</span></abbr>
                    </div>

                <?php } else if ($zodiac_id == 6) { ?>
                    <script>
                        var name = "Virgo";
                        $(".Virgo").addClass('active');
                        $("#zname").text(name);
                    </script>
                    <div class="horoscope_image">
                        <img src="<?php echo BASEURL_IMG; ?>zodiac/virgo.png" />
                        <abbr style="font-family: gotham-book;">Virgo <span>(August 23 - September 22)</span></abbr>
                    </div>

                <?php } else if ($zodiac_id == 7) { ?>
                    <script>
                        var name = "Libra";
                        $(".Libra").addClass('active');
                        $("#zname").text(name);
                    </script>
                    <div class="horoscope_image">
                        <img src="<?php echo BASEURL_IMG; ?>zodiac/libra.png" />
                        <abbr style="font-family: gotham-book;">Libra <span>(September 23 - October 22)</span></abbr>
                    </div>
                <?php } else if ($zodiac_id == 8) { ?>
                    <script>
                        var name = "Scorpio";
                        $(".Scorpio").addClass('active');
                        $("#zname").text(name);
                    </script>
                    <div class="horoscope_image">
                        <img src="<?php echo BASEURL_IMG; ?>zodiac/scorpio.png" />
                        <abbr style="font-family: gotham-book;">Scorpio <span>(October 23 - November 21)</span></abbr>
                    </div>

                <?php } else if ($zodiac_id == 9) { ?>
                    <script>
                        var name = "Sagittarius";
                        $(".Sagittarius").addClass('active');
                        $("#zname").text(name);
                    </script>

                    <div class="horoscope_image">
                        <img src="<?php echo BASEURL_IMG; ?>zodiac/sagittarius.png" />
                        <abbr style="font-family: gotham-book;">Sagittarius <span>(November 22 - December 21)</span></abbr>
                    </div>


                <?php } else if ($zodiac_id == 10) { ?>
                    <script>
                        var name = "Capricorn";
                        $(".Capricorn").addClass('active');
                        $("#zname").text(name);
                    </script>

                    <div class="horoscope_image">
                        <img src="<?php echo BASEURL_IMG; ?>zodiac/capricorn.png" />
                        <abbr style="font-family: gotham-book;">Capricorn <span>(December 22 - January 19)</span></abbr>
                    </div>


                <?php } else if ($zodiac_id == 11) { ?> 
                    <script>
                        var name = "Aquarius";
                        $(".Aquarius").addClass('active');
                        $("#zname").text(name);</script>
                    <div class="horoscope_image">
                        <img src="<?php echo BASEURL_IMG; ?>zodiac/aquarius.png" />
                        <abbr style="font-family: gotham-book;">Aquarius <span>(January 20 - February 18)</span></abbr>
                    </div>

                <?php } else if ($zodiac_id == 12) { ?>
                    <script>
                        var name = "Pisces";
                        $(".Pisces").addClass('active');
                        $("#zname").text(name);</script>
                    <div class="horoscope_image">
                        <img src="<?php echo BASEURL_IMG; ?>zodiac/pisces.png" />
                        <abbr style="font-family: gotham-book;">Pisces <span>(February 19 - March 20)</span></abbr>
                    </div>

                <?php } ?>

                <?php
                $year = date("y");
                $number = cal_days_in_month(CAL_GREGORIAN, 5, $year);
                ?>


                <?php if (($fmonth == 3) && ($tmonth == 4)) { ?>
                    <div  class="inputwrapper" style="margin-left: 32px;">
                        <div class="grid_2 optnno" id="march">
                            <select id="date1">
                                <option selected="selected" value="<?php echo $fdate; ?>">21</option>
                                <?php
                                $year = date("y");
                                $number1 = cal_days_in_month(CAL_GREGORIAN, 3, $year);
                                for ($i = $fdate + 1; $i <= $number1; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2 optnno" id="April" style="display: none">
                            <select id="date2">
                                <option selected="selected" value="1">1</option>
                                <?php
                                $year = date("y");
                                $number2 = cal_days_in_month(CAL_GREGORIAN, 4, $year);
                                for ($i = 2; $i <= $tdate; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2">
                            <select id="select_month">
                                <option selected="selected" value="3">March</option>
                                <option value="4">April</option>
                            </select>
                        </div>
                        <div class="grid_2" >
                            <input type="button" id="submit_zodiac" date="<?php echo $fdate; ?>" month="<?php echo $fmonth; ?>" value=" Read!"  class="szodiac">
                        </div>


                    </div>
                <?php } else if (($fmonth == 4) && ($tmonth == 5)) { ?>
                    <div class="inputwrapper" style="margin-left: 32px;">
                        <div class="grid_2 optnno" id="April1">
                            <select id="date3">
                                <option selected="selected" value="<?php echo $fdate; ?>"><?php echo $fdate; ?></option>
                                <?php
                                $year1 = date("y");
                                $number1 = cal_days_in_month(CAL_GREGORIAN, 4, $year1);
                                for ($i = $fdate + 1; $i <= $number1; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2 optnno" id="may" style="display: none">
                            <select id="date4">
                                <option selected="selected" value="1">1</option>
                                <?php
                                $year2 = date("y");
                                $number2 = cal_days_in_month(CAL_GREGORIAN, 5, $year2);
                                for ($i = 2; $i <= $tdate; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2">
                            <select id="select_month1">
                                <option selected="selected" value="4">April</option>
                                <option value="5">May</option>
                            </select>
                        </div>
                        <div class="grid_2" >
                            <input type="button" id="submit_zodiac" date="<?php echo $fdate; ?>" month="<?php echo $fmonth; ?>" value=" Read!" class="szodiac">
                        </div>

                    </div>
                <?php } else if (($fmonth == 5) && ($tmonth == 6)) { ?>

                    <div class="inputwrapper" style="margin-left: 32px;">
                        <div class="grid_2 optnno" id="may1">
                            <select id="date5">
                                <option selected="selected" value="<?php echo $fdate; ?>"><?php echo $fdate; ?></option>
                                <?php
                                $year1 = date("y");
                                $number1 = cal_days_in_month(CAL_GREGORIAN, 5, $year1);
                                for ($i = $fdate + 1; $i <= $number1; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2 optnno" id="june" style="display: none">
                            <select id="date6">
                                <option selected="selected" value="1">1</option>
                                <?php
                                $year2 = date("y");
                                $number2 = cal_days_in_month(CAL_GREGORIAN, 6, $year2);
                                for ($i = 2; $i <= $tdate; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2">
                            <select id="select_month2">
                                <option selected="selected" value="5">May</option>
                                <option value="6">June</option>
                            </select>
                        </div>
                        <div class="grid_2" >
                            <input type="button" id="submit_zodiac" date="<?php echo $fdate; ?>" month="<?php echo $fmonth; ?>" value=" Read!" class="szodiac">
                        </div>

                    </div>
                <?php } else if (($fmonth == 6) && ($tmonth == 7)) { ?>

                    <div class="inputwrapper" style="margin-left: 32px;">
                        <div class="grid_2 optnno" id="june1">
                            <select id="date7">
                                <option selected="selected" value="<?php echo $fdate; ?>"><?php echo $fdate; ?></option>
                                <?php
                                $year1 = date("y");
                                $number1 = cal_days_in_month(CAL_GREGORIAN, 6, $year1);
                                for ($i = $fdate + 1; $i <= $number1; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2 optnno" id="july" style="display: none">
                            <select id="date8">
                                <option selected="selected" value="1">1</option>
                                <?php
                                $year2 = date("y");
                                $number2 = cal_days_in_month(CAL_GREGORIAN, 7, $year2);
                                for ($i = 2; $i <= $tdate; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2">
                            <select id="select_month3">
                                <option selected="selected" value="6">June</option>
                                <option value="7">July</option>
                            </select>
                        </div>
                        <div class="grid_2" >
                            <input type="button" id="submit_zodiac" date="<?php echo $fdate; ?>" month="<?php echo $fmonth; ?>" value=" Read!" class="szodiac">
                        </div>
                    </div>
                <?php } else if (($fmonth == 7) && ($tmonth == 8)) { ?>

                    <div class="inputwrapper" style="margin-left: 32px;">
                        <div class="grid_2 optnno" id="july1">
                            <select id="date9">
                                <option selected="selected" value="<?php echo $fdate; ?>"><?php echo $fdate; ?></option>
                                <?php
                                $year1 = date("y");
                                $number1 = cal_days_in_month(CAL_GREGORIAN, 7, $year1);
                                for ($i = $fdate + 1; $i <= $number1; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2 optnno" id="august" style="display: none">
                            <select id="date10">
                                <option selected="selected" value="1">1</option>
                                <?php
                                $year2 = date("y");
                                $number2 = cal_days_in_month(CAL_GREGORIAN, 8, $year2);
                                for ($i = 2; $i <= $tdate; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2">
                            <select id="select_month4">
                                <option selected="selected" value="7">July</option>
                                <option value="8">August</option>
                            </select>
                        </div>
                        <div class="grid_2" >
                            <input type="button" id="submit_zodiac" date="<?php echo $fdate; ?>" month="<?php echo $fmonth; ?>" value=" Read!" class="szodiac">
                        </div>
                    </div>
                <?php } else if (($fmonth == 8) && ($tmonth == 9)) { ?>

                    <div class="inputwrapper" style="margin-left: 32px;">
                        <div class="grid_2 optnno" id="august1">
                            <select id="date11">
                                <option selected="selected" value="<?php echo $fdate; ?>"><?php echo $fdate; ?></option>
                                <?php
                                $year1 = date("y");
                                $number1 = cal_days_in_month(CAL_GREGORIAN, 8, $year1);
                                for ($i = $fdate + 1; $i <= $number1; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2 optnno" id="sept" style="display: none">
                            <select id="date12">
                                <option selected="selected" value="1">1</option>
                                <?php
                                $year2 = date("y");
                                $number2 = cal_days_in_month(CAL_GREGORIAN, 9, $year2);
                                for ($i = 2; $i <= $tdate; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2">
                            <select id="select_month5">
                                <option selected="selected" value="8">August</option>
                                <option value="9">September</option>
                            </select>
                        </div>
                        <div class="grid_2" >
                            <input type="button" id="submit_zodiac" date="<?php echo $fdate; ?>" month="<?php echo $fmonth; ?>" value=" Read!" class="szodiac">
                        </div>

                    </div>

                <?php } else if (($fmonth == 9) && ($tmonth == 10)) { ?>

                    <div class="inputwrapper" style="margin-left: 32px;">
                        <div class="grid_2 optnno" id="sept1">
                            <select id="date13">
                                <option selected="selected" value="<?php echo $fdate; ?>"><?php echo $fdate; ?></option>
                                <?php
                                $year1 = date("y");
                                $number1 = cal_days_in_month(CAL_GREGORIAN, 9, $year1);
                                for ($i = $fdate + 1; $i <= $number1; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2 optnno" id="oct" style="display: none">
                            <select id="date14">
                                <option selected="selected" value="1">1</option>
                                <?php
                                $year2 = date("y");
                                $number2 = cal_days_in_month(CAL_GREGORIAN, 10, $year2);
                                for ($i = 2; $i <= $tdate; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2">
                            <select id="select_month6">
                                <option selected="selected" value="9">September</option>
                                <option value="10">October</option>
                            </select>
                        </div>
                        <div class="grid_2" >
                            <input type="button" id="submit_zodiac" date="<?php echo $fdate; ?>" month="<?php echo $fmonth; ?>" value=" Read!" class="szodiac">
                        </div>
                    </div>
                <?php } else if (($fmonth == 10) && ($tmonth == 11)) { ?>

                    <div class="inputwrapper" style="margin-left: 32px;">
                        <div class="grid_2 optnno" id="oct1">
                            <select id="date15">
                                <option selected="selected" value="<?php echo $fdate; ?>"><?php echo $fdate; ?></option>
                                <?php
                                $year1 = date("y");
                                $number1 = cal_days_in_month(CAL_GREGORIAN, 10, $year1);
                                for ($i = $fdate + 1; $i <= $number1; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2 optnno" id="nov" style="display: none">
                            <select id="date16">
                                <option selected="selected" value="1">1</option>
                                <?php
                                $year2 = date("y");
                                $number2 = cal_days_in_month(CAL_GREGORIAN, 11, $year2);
                                for ($i = 2; $i <= $tdate; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2">
                            <select id="select_month7">
                                <option selected="selected" value="10">October</option>
                                <option value="11">November</option>
                            </select>
                        </div>
                        <div class="grid_2" >
                            <input type="button" id="submit_zodiac" date="<?php echo $fdate; ?>" month="<?php echo $fmonth; ?>" value=" Read!" class="szodiac">
                        </div>
                    </div>
                <?php } else if (($fmonth == 11) && ($tmonth == 12)) { ?>

                    <div class="inputwrapper" style="margin-left: 32px;">
                        <div class="grid_2 optnno" id="nov1">
                            <select id="date17">
                                <option selected="selected" value="<?php echo $fdate; ?>"><?php echo $fdate; ?></option>
                                <?php
                                $year1 = date("y");
                                $number1 = cal_days_in_month(CAL_GREGORIAN, 11, $year1);
                                for ($i = $fdate + 1; $i <= $number1; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2 optnno" id="dec" style="display: none">
                            <select id="date18">
                                <option selected="selected" value="1">1</option>
                                <?php
                                $year2 = date("y");
                                $number2 = cal_days_in_month(CAL_GREGORIAN, 12, $year2);
                                for ($i = 2; $i <= $tdate; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2">
                            <select id="select_month8">
                                <option selected="selected" value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                        <div class="grid_2" >
                            <input type="button" id="submit_zodiac" date="<?php echo $fdate; ?>" month="<?php echo $fmonth; ?>" value=" Read!" class="szodiac">
                        </div>
                    </div>
                <?php } else if (($fmonth == 12) && ($tmonth == 1)) { ?>

                    <div class="inputwrapper" style="margin-left: 32px;">
                        <div class="grid_2 optnno" id="dec1">
                            <select id="date19">
                                <option selected="selected" value="<?php echo $fdate; ?>"><?php echo $fdate; ?></option>
                                <?php
                                $year1 = date("y");
                                $number1 = cal_days_in_month(CAL_GREGORIAN, 12, $year1);
                                for ($i = $fdate + 1; $i <= $number1; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2 optnno" id="jan" style="display: none">
                            <select id="date20">
                                <option selected="selected" value="1">1</option>
                                <?php
                                $year2 = date("y");
                                $number2 = cal_days_in_month(CAL_GREGORIAN, 1, $year2);
                                for ($i = 2; $i <= $tdate; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2">
                            <select id="select_month9">
                                <option selected="selected" value="12">December</option>
                                <option value="1">January</option>
                            </select>
                        </div>
                        <div class="grid_2" >
                            <input type="button" id="submit_zodiac" date="<?php echo $fdate; ?>" month="<?php echo $fmonth; ?>" value=" Read!" class="szodiac">
                        </div>
                    </div>
                <?php } else if (($fmonth == 1) && ($tmonth == 2)) { ?>

                    <div class="inputwrapper" style="margin-left: 32px;">
                        <div class="grid_2 optnno" id="jan1">
                            <select id="date21">
                                <option selected="selected" value="<?php echo $fdate; ?>"><?php echo $fdate; ?></option>
                                <?php
                                $year1 = date("y");
                                $number1 = cal_days_in_month(CAL_GREGORIAN, 1, $year1);
                                for ($i = $fdate + 1; $i <= $number1; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2 optnno" id="feb" style="display: none">
                            <select id="date22">
                                <option selected="selected" value="1">1</option>
                                <?php
                                $year2 = date("y");
                                $number2 = cal_days_in_month(CAL_GREGORIAN, 2, $year2);
                                for ($i = 2; $i <= $tdate; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2">
                            <select id="select_month10">
                                <option selected="selected" value="1">January</option>
                                <option value="2">February</option>
                            </select>
                        </div>
                        <div class="grid_2" >
                            <input type="button" id="submit_zodiac" date="<?php echo $fdate; ?>" month="<?php echo $fmonth; ?>" value=" Read!" class="szodiac">
                        </div>
                    </div>
                <?php } else if (($fmonth == 2) && ($tmonth == 3)) { ?>

                    <div class="inputwrapper" style="margin-left: 32px;">
                        <div class="grid_2 optnno" id="feb1">
                            <select id="date23">
                                <option selected="selected" value="<?php echo $fdate; ?>"><?php echo $fdate; ?></option>
                                <?php
                                $year1 = date("y");
                                $number1 = cal_days_in_month(CAL_GREGORIAN, 2, $year1);
                                for ($i = $fdate + 1; $i <= $number1; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2 optnno" id="march1" style="display: none">
                            <select id="date24">
                                <option selected="selected" value="1">1</option>
                                <?php
                                $year2 = date("y");
                                $number2 = cal_days_in_month(CAL_GREGORIAN, 3, $year2);
                                for ($i = 2; $i <= $tdate; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="grid_2">
                            <select id="select_month11">
                                <option selected="selected" value="2">February</option>
                                <option value="3">March</option>
                            </select>
                        </div>
                        <div class="grid_2" >
                            <input type="button" id="submit_zodiac" date="<?php echo $fdate; ?>" month="<?php echo $fmonth; ?>"  class="szodiac" value="Read!" >
                        </div>
                    </div>
                <?php } ?>



                <div class="wrapper res100_768" style="margin-top: 40px;" >
                    <div class="grid_8">
                        <div class="horoscope_content" id="zodiac_content" style="font-size: 15px;font-family: gotham-book;">

                        </div>
                    </div>
                    <div class="grid_4 horoscope_owl" id="owlimg" >

                    </div>
                </div>

                <div class="horoscope_content wrapper" id="second_zodiac_section" style="font-size: 15px;font-family:gotham-book;">

                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script>
    $(".showloader").addClass("show_overlay").show();

    $(document).ready(function () {

        //var zodiac_id = "<?php //echo $zodiac_id             ?>";

        var date = $("#submit_zodiac").attr("date");
        var month = $("#submit_zodiac").attr("month");

        var data = {
            sign_day: date,
            sign_month: month
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>zodiac-info",
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.success.toString() === "false") {
                    $("#TxtSign").val(r.message.toString());
                    return false;
                } else {
                    var sign_info = r.sign_info;
                    var first_part_info = r.first_part;
                    var second_part_info = r.second_part;



                    $("#zodiac_content").html(first_part_info);
                    $("#second_zodiac_section").html(second_part_info);
                    var oimg = "<?php echo BASEURL_OIMG . 'horoscope_owl.png' ?>";
                    var data = '';
                    data = '<img src="' + oimg + '"> ';
                    $("#owlimg").html(data);

                }
            }
        });
    });

    $(window).load(function () {

        $(".showloader").removeClass("show_overlay").hide();
    });
    $("#select_month").change(function () {
        var month = $(this).val();
        if (month == "4") {
            var date = $("#date2").val();
            $("#march").hide();
            $("#April").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);
        } else {
            var date = $("#date1").val();
            $("#April").hide();
            $("#march").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);
        }
    });

    $("#date1").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date2").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date3").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date4").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date5").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date6").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date7").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date8").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date9").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date10").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date11").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date12").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date13").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date14").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date15").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date16").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date17").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date18").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date19").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date20").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date21").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date22").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date23").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date24").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#select_month1").change(function () {
        var month = $(this).val();
        if (month == "5") {
            var date = $("#date4").val();
            $("#April1").hide();
            $("#may").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);
        } else {
            var date = $("#date3").val();
            $("#may").hide();
            $("#April1").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);

        }
    });
    $("#date2").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#date3").change(function () {
        var date = $(this).val();
        $("#submit_zodiac").attr("date", date);

    });
    $("#select_month2").change(function () {
        var month = $(this).val();
        if (month == "6") {
            var date = $("#date6").val();
            $("#may1").hide();
            $("#june").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);
        } else {
            var date = $("#date5").val();
            $("#june").hide();
            $("#may1").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);

        }
    });
    $("#select_month3").change(function () {
        var month = $(this).val();
        if (month == "7") {
            var date = $("#date8").val();
            $("#june1").hide();
            $("#july").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);
        } else {
            var date = $("#date7").val();
            $("#july").hide();
            $("#june1").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);

        }
    });

    $("#select_month4").change(function () {
        var month = $(this).val();
        if (month == "8") {
            var date = $("#date10").val();
            $("#july1").hide();
            $("#august").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);
        } else {
            var date = $("#date9").val();
            $("#august").hide();
            $("#july1").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);

        }
    });
    $("#select_month5").change(function () {
        var month = $(this).val();
        if (month == "9") {
            var date = $("#date12").val();
            $("#august1").hide();
            $("#sept").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);
        } else {
            var date = $("#date11").val();
            $("#sept").hide();
            $("#august1").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);

        }
    });
    $("#select_month6").change(function () {
        var month = $(this).val();
        if (month == "10") {
            var date = $("#date14").val();
            $("#sept1").hide();
            $("#oct").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);
        } else {
            var date = $("#date13").val();
            $("#oct").hide();
            $("#sept1").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);

        }
    });

    $("#select_month7").change(function () {
        var month = $(this).val();
        if (month == "11") {
            var date = $("#date16").val();
            $("#oct1").hide();
            $("#nov").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);
        } else {
            var date = $("#date15").val();
            $("#nov").hide();
            $("#oct1").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);

        }
    });

    $("#select_month8").change(function () {
        var month = $(this).val();
        if (month == "12") {
            var date = $("#date18").val();
            $("#nov1").hide();
            $("#dec").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);
        } else {
            var date = $("#date17").val();
            $("#dec").hide();
            $("#nov1").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);

        }
    });
    $("#select_month9").change(function () {
        var month = $(this).val();
        if (month == "1") {
            var date = $("#date20").val();
            $("#dec1").hide();
            $("#jan").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);
        } else {
            var date = $("#date19").val();
            $("#jan").hide();
            $("#dec1").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);

        }
    });

    $("#select_month10").change(function () {
        var month = $(this).val();
        if (month == "2") {
            var date = $("#date22").val();
            $("#jan1").hide();
            $("#feb").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);
        } else {
            var date = $("#date21").val();
            $("#feb").hide();
            $("#jan1").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);

        }
    });

    $("#select_month11").change(function () {
        var month = $(this).val();
        if (month == "3") {
            var date = $("#date24").val();
            $("#feb1").hide();
            $("#march1").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);
        } else {
            var date = $("#date23").val();
            $("#march1").hide();
            $("#feb1").show();
            $("#submit_zodiac").attr("date", date);
            $("#submit_zodiac").attr("month", month);

        }
    });
    $("#submit_zodiac").click(function () {
        var date = $(this).attr("date");
        var month = $(this).attr("month");
        var data = {
            sign_day: date,
            sign_month: month
        }
        $.ajax({
            type: "POST",
            url: "<?php echo BASEURL; ?>zodiac-info",
            data: data,
            dataType: "json",
            beforeSend: function () {
                $(".showloader").addClass("show_overlay").show();


            },
            success: function (r) {
                $(".showloader").removeClass("show_overlay").hide();

                if (r.success.toString() === "false") {
                    $("#TxtSign").val(r.message.toString());
                    return false;
                } else {
                    var sign_info = r.sign_info;
                    var first_part_info = r.first_part;
                    var second_part_info = r.second_part;
                    $("#zodiac_content").html(first_part_info);
                    $("#second_zodiac_section").html(second_part_info);
                    var oimg = "<?php echo BASEURL_OIMG . 'horoscope_owl.png' ?>";
                    var data = '';
                    data = '<img src="' + oimg + '"> ';
                    $("#owlimg").html(data);
                }
            }
        });
    });
    $("#zodiac_id1").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id2").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id3").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id4").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id5").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id6").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id7").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id8").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id9").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id10").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id11").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id12").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });


</script>



