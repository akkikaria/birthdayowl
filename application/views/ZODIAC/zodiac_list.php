<?php $this->load->view("new_templates/header"); ?>
<style type="text/css">
  .sortinfo{
        padding: 60px;
        text-align: justify;
        font-family:"gotham-book";
        font-size:15px;
    }
    @media only screen and (max-width:768px)
    {
        #blah {
            top: 0px;
            width: 47%;
        }
         .sortinfo{
        padding: 10px;
        text-align: justify;
        font-family:"gotham-book";
        font-size:15px;
    }

    }

</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 col-xs-12 no-padd inner-top-slider">
            <img src="<?php echo BASEURL_ONEWIMG ?>zodiacbanner.jpg" class="img-responsive owl-image-heading" alt="zodiac banner">
        </div>
    </div>
</div>
<!--Banner -->
<div class="wrapper">
    <div class="container_12 text-center">
        <div class="heading_2">
            Zodiac <span>Signs</span>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container_12">
        <div class="inner_midd dashbord">
            <div class="in_f_l" style="text-align: center;">
                <img src="<?PHP echo BASEURL_OIMG ?>Zodiac_Signs.png" id="blah" style="padding-left: 30px;" alt="Free Zodiac Signs" />
            </div>
            <div class="in_f_r">
                <ul class="zodiac">
                    <li  zodiac_id="1" id="zodiac_id1" style="cursor: pointer"><span>Aries</span>
                        <abbr>March 21 - April 19</abbr>
                        <a href="#">Read more about the Aries Zodiac Sign</a></li>
                    <li class="Taurus"  zodiac_id="2" id="zodiac_id2"  style="cursor: pointer"><span>Taurus</span>
                        <abbr>April 20 - May 20</abbr>
                        <a href="#">Read more about the Taurus Zodiac Sign</a></li>
                    <li class="Gemini" zodiac_id="3" id="zodiac_id3"  style="cursor: pointer"><span>Gemini</span>
                        <abbr>May 21 - June 20</abbr>
                        <a href="#">Read more about the Gemini Zodiac Sign</a></li>
                    <li class="Cancer" zodiac_id="4" id="zodiac_id4"  style="cursor: pointer"><span>Cancer</span>
                        <abbr>June 21 - July 22</abbr>
                        <a href="#">Read more about the Cancer Zodiac Sign</a></li>
                    <li class="Leo" zodiac_id="5" id="zodiac_id5"  style="cursor: pointer"><span>Leo</span>
                        <abbr>July 23 - August 22</abbr>
                        <a href="#">Read more about the Leo Zodiac Sign</a></li>
                    <li class="Virgo" zodiac_id="6" id="zodiac_id6"  style="cursor: pointer"><span>Virgo</span>
                        <abbr>August 23 - Sept. 22</abbr>
                        <a href="#">Read more about the Virgo Zodiac Sign</a></li>
                    <li class="Libra" zodiac_id="7" id="zodiac_id7"  style="cursor: pointer"><span>Libra</span>
                        <abbr>Sept. 23 - October 22</abbr>
                        <a href="#">Read more about the Libra Zodiac Sign</a></li>
                    <li class="Scorpio" zodiac_id="8" id="zodiac_id8"  style="cursor: pointer"><span>Scorpio</span>
                        <abbr>October 23 - Nov. 21</abbr>
                        <a href="#">Read more about the Scorpio Zodiac Sign</a></li>
                    <li class="Sagittarius"  zodiac_id="9" id="zodiac_id9"  style="cursor: pointer"><span>Sagittarius</span>
                        <abbr>Nov. 22 - Dec. 21</abbr>
                        <a href="#">Read more about the Sagittarius Zodiac Sign</a></li>
                    <li class="Capricorn" zodiac_id="10" id="zodiac_id10"  style="cursor: pointer ;"><span>Capricorn</span>
                        <abbr>Dec. 22 - Jan. 19</abbr>
                        <a href="#">Read more about the Capricorn Zodiac Sign</a></li>
                    <li class="Aquarius" zodiac_id="11" id="zodiac_id11"  style="cursor: pointer"><span>Aquarius</span>
                        <abbr>Jan. 20 - Feb. 18</abbr>
                        <a href="#">Read more about the Aquarius Zodiac Sign</a></li>
                    <li class="Pisces" zodiac_id="12" id="zodiac_id12"  style="cursor: pointer"><span>Pisces</span>
                        <abbr>Feb. 19 - March 20</abbr>
                        <a href="#">Read more about the Pisces Zodiac Sign</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container-fluid sortinfo">
    <h1 style="font-size: 22px;text-align: center;font-weight: 600;" >Zodiac Signs</h1>
       <h2 style="font-size: 22px;">Free Zodiac Signs</h2> 
    <p>Do you believe in zodiac signs? We certainly do, and you would be amazed what you can learn about a person's personality just by their date of birth.You can now learn all this and more at our <a href="https://www.birthdayowl.com/zodiac-signs"><b>Zodiac Signs</b></a> page.</p>

    <p>Firstly, we can remind you of all your birthdays once you input them. Then we provide you with zodiac profile readings of each and every birthday so that you can get an instant insight into their personalities. Try to learn about someone by simply reading about their zodiac date reading and this would make it even easier for you to decide on an appropriate gift or plan for their birthday.</p>

    <p>We have free zodiac sign information on each and every birth date of the year, so we have you covered. All this for free - only to enable you to make right choices on your loved ones' birthdays.</p>

    

</div>
<div class="clearfix"></div>
<link href="<?php echo BASEURL_OCSS; ?>jquery.loader.css" rel="stylesheet" />  <!--page loading -->
<?php $this->load->view("new_templates/vouchers_footer"); ?>
<script src="<?php echo BASEURL_BJS; ?>my.loader.js"></script>
<script type="text/javascript">
    $("#zodiac_id1").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id2").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id3").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id4").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id5").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id6").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id7").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id8").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id9").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id10").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id11").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });
    $("#zodiac_id12").click(function () {
        var zodiac_id = $(this).attr("zodiac_id");
        window.location = "<?php echo BASEURL; ?>select_zodiac/" + zodiac_id;
    });

    if ($(window).width() > 768) {
        $(function () {

            var $blah = $("#blah"),
                    $window = $(window),
                    offset = $blah.offset();

            $window.scroll(function () {
                if ($window.scrollTop() > offset.top) {
                    $blah.stop().animate({
                        top: 450
                    });
                } else {
                    $blah.stop().animate({
                        top: 0
                    });
                }
            });
        });
    }
</script>
