<?php

//checks if a user is registered as a birthday owl user

class CheckAsBirthdayowl {

    private $CI;
    private $email;
    private $userId;
    private $mobileNo;
    private $result;

    public function __construct() {
        $this->CI = & get_instance();
    }

    function getUserId() {
        return $this->userId;
    }

    function getResult() {
        return $this->result;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }

    function setResult($result) {
        $this->result = $result;
    }

    function getEmail() {
        return $this->email;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function getMobileNo() {
        return $this->mobileNo;
    }

    function setMobileNo($mobileNo) {
        $this->mobileNo = $mobileNo;
    }

    public function runner() {
        $result = $this->checkAvailableBirthdayOwlUser();
        if (!empty($result)) {
            $this->result = $result[0];
            $this->userId = $this->result['user_id'];
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function checkAvailableBirthdayOwlUser() {
        if (!is_null($this->mobileNo)) {
            $where = "(email='$this->email' or mobile_no = '$this->mobileNo') and is_guest=0 and mobile_verify = 1";  // the user exist as a birthday owl user
        } else {
            $where = "email='$this->email' and is_guest = 0 and mobile_verify = 1";
        }
        $check_user = $this->CI->Birthday->getSelectData("*", "users", $where)->result_array();
        return $check_user;
    }

}
