<?php
/**
 * OAuth support for PHP
 * @author Stefan Thoolen <stefan@ibizz.nl>
 * @package OAuth
 * @link http://oauth.bitbucket.org/
 * @copyright 2014 STUDiO iBiZZ
 * @license CC-BY-SA-3.0
 * @license http://creativecommons.org/licenses/by-sa/3.0/ Creative Commons Attribution-ShareAlike 3.0 Unported
 * @version 0.5.0
 */
namespace StudioIbizz\OAuth;

/**
 * Abstract OAuth Class
 */
abstract class OAuth {
	/**
	 * The OAuth version
	 * @access protected
	 * @var integer
	 */
	protected static $oauth_version = null;
	
	/**
	 * Additional cURL options
	 * @access protected
	 * @var array
	 */
	protected $curl_options = array();
	
	/**
	 * Performs a signed OAuth webrequest
	 * @access public
	 * @param string $url The URL to be requested
	 * @param string $request_method So far it may be GET, POST
	 * @param array $data POST or GET values
	 * @param array $headers Additional HTTP headers
	 * @return OAuthResponse 
	 * @throws OAuthException If something went wrong, OAuthException->getCode() will validate with any OAuthException::ERROR_ constant.
	 */	
	public abstract function OAuthRequest($url, $request_method = 'GET', array $data = array(), array $headers = array());
	
	/**
	 * Sets a cURL option (see also: curl_setopt)
	 * Keep in mind, some cURL options (url, headers, etc.) are overwritten by the OAuth library.
	 * @param int $option
	 * @param mixed $value
	 */
	public function setCurlOption($option, $value) {
		$this->curl_options[$option] = $value;
	}
			
	/**
	 * Sets multiple cURL options (see also: curl_setopt_array)
	 * Keep in mind, some cURL options (url, headers, etc.) are overwritten by the OAuth library.
	 * @param array $options
	 */
	public function setCurlOptions(array $options) {
		foreach ($options as $option => $value) {
			$this->setCurlOption($option, $value);
		}
	}
			
	/**
	 * Executes a cURL request
	 * @access protected
	 * @param string $url The URL to be requested
	 * @param string $request_method So far it may be GET, POST
	 * @param array $data POST or GET values
	 * @param array $headers Additional HTTP headers
	 * @param string $payload Additional payload to be posted
	 * @throws OAuthException If something went wrong, OAuthException->getCode() will validate with any OAuthException::ERROR_ constant.
	 * @return OAuthResponse
	 */	
	protected function CurlRequest($url, $request_method = 'GET', array $data = array(), array $headers = array(), $payload = null) {
		// Add User-Agent, since some OAuth providers require a user-agent header
		$user_agent = 'OAuth'.self::$oauth_version.' client (http://oauth.bitbucket.org/)';
	
		// Default options
		$CurlOptions = $this->curl_options;
		$CurlOptions[CURLOPT_URL] = $url;
		$CurlOptions[CURLOPT_RETURNTRANSFER] = true;
		$CurlOptions[CURLOPT_HEADER] = true;
		$CurlOptions[CURLOPT_HTTPHEADER] = $headers;
		$CurlOptions[CURLOPT_USERAGENT] = $user_agent;
		
		/**
		 * Handling the various request methods below
		 */
		
		// PATCH-requests can do the same as POST requests, with one difference, the request method is custom
		if (strtoupper($request_method) == 'PATCH') {
			$CurlOptions[CURLOPT_CUSTOMREQUEST] = 'PATCH';
			$request_method = 'POST';
		}
		
		// PUT-requests
		if (strtoupper($request_method) == 'PUT' && !$payload) {
			// PUT requests require a payload
			throw new OAuthException('Payload-parameter not used or empty. Payload is required when executing a PUT request.', OAuthException::ERROR_PUT_REQUIRES_PAYLOAD);
		}
		else if (strtoupper($request_method) == 'PUT') {
			// If there are values in the user data, they will be appended to the URL as GET-request.
			$mem = fopen('php://memory', 'rw');
			fwrite($mem, $payload);
			rewind($mem);
			$CurlOptions[CURLOPT_URL] = self::ArrayToRequestURI($data, $url);
			$CurlOptions[CURLOPT_PUT] = true;
			$CurlOptions[CURLOPT_INFILESIZE] = strlen($payload);
			$CurlOptions[CURLOPT_INFILE] = $mem;
		}
		
		// POST-requests
		else if (strtoupper($request_method) == 'POST' && $payload !== null) {
			// When doing a POST with a payload, the payload will be in the POST fields.
			// If there are values in the user data, they will be appended to the URL as GET-request.
			$CurlOptions[CURLOPT_POST] = true;
			$CurlOptions[CURLOPT_URL] = self::ArrayToRequestURI($data, $url);
			$CurlOptions[CURLOPT_POSTFIELDS] = $payload;
		}
		else if (strtoupper($request_method) == 'POST' && self::hasFileData($data)) {
			// When doing a POST with file uploads, CURLOPT_POSTFIELDS is provided as array. The content-type will be 'multipart/form-data'.
			$CurlOptions[CURLOPT_POSTFIELDS] = $data;
			$CurlOptions[CURLOPT_POST] = true;
		}
		else if (strtoupper($request_method) == 'POST') {
			// When doing a regular POST request, CURLOPT_POSTFIELDS is provided as string, the content-type will be 'application/x-www-form-urlencoded'.
			$CurlOptions[CURLOPT_POSTFIELDS] = self::ArrayToRequestURI($data);
			$CurlOptions[CURLOPT_POST] = true;
		}
		
		// Request methods below here can't handle a payload
		else if ($payload !== null) {
			// Payloads are only valid in combination with POST requests
			throw new OAuthException('Payload not possible with '.strtoupper($request_method).'-requests', OAuthException::ERROR_WRONG_REQUEST_FOR_PAYLOAD);
		}
		
		// GET-requests
		else if (strtoupper($request_method) == 'GET') {
			// When doing a regular GET request, user data will be appended to the URL.
			$CurlOptions[CURLOPT_HTTPGET] = true;
			$CurlOptions[CURLOPT_URL] = self::ArrayToRequestURI($data, $url);
		}
		
		// DELETE-requests
		else if (strtoupper($request_method) == 'DELETE') {
			// A DELETE request is custom for now
			$CurlOptions[CURLOPT_CUSTOMREQUEST] = 'DELETE';
			$CurlOptions[CURLOPT_POSTFIELDS] = self::ArrayToRequestURI($data);
		}
		
		// PATCH-requests can do the same as POST requests, with one difference, the request method is custom
		if (isset($CurlOptions[CURLOPT_CUSTOMREQUEST]) && $CurlOptions[CURLOPT_CUSTOMREQUEST] == 'PATCH') {
			unset($CurlOptions[CURLOPT_POST]);
		}
		
		// Executes the request
		$ch = curl_init();
		curl_setopt_array($ch, $CurlOptions);
		$data = curl_exec($ch);
		
		// When something went wrong, throw an exception
		if ($data === false) {
			throw new OAuthException('Unexpected cURL error #'.curl_errno($ch).': '.curl_error($ch), OAuthException::ERROR_CURL + curl_errno($ch));
		}
		
		$info = curl_getinfo($ch);
		curl_close($ch);
		
		// Closes any possible open memory stream
		if (isset($mem)) {
			fclose($mem);
		}
		
		// Returns all data as array
		return new OAuthResponse($CurlOptions, $info, $data);
	}
	
	/**
	 * Checks if the user data contains file uploads
	 * @param array $data
	 * @return boolean
	 * @access private
	 */
	private static function hasFileData(array $data) {
		foreach ($data as $value) {
			if (substr($value, 0, 1) == '@' && file_exists($value, 1)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Converts an array to a request URI
	 * @access protected
	 * @param array $data Example: array('id' => 34, 'action' => 'edit');
	 * @param string $url Optional, if specified, the values will be appended to the URL	 
	 * @return string Example: id=34&action=edit
	 */
	protected static function ArrayToRequestURI(array $data, $url = null) {
		$return = '';
		
		// Joins the array with & and = signs, including url encoding 
		foreach ($data as $key => $value) {
			$return .= '&'.rawurlencode($key).'='.rawurlencode($value);
		}
		$return = substr($return, 1);
		
		// Prepends the URL if needed
		if ($url !== null && $return == '') {
			$return = $url;
		} else if ($url !== null) {
			$return = $url.(strpos($url, '?') !== false ? '&' : '?').$return;
		}
		
		return $return;
	}
	
	/**
	 * Generates a random string, uniquely for each request.
	 * @access protected
	 * @return string
	 */
	protected static function getUnique() {
		return md5(time().__FILE__.rand(1000,9999));
	}
	
	/**
	 * Returns the current URL
	 * @access protected
	 * @return string
	 */
	protected static function getCurrentURL() {
		$return  = isset($_SERVER['HTTPS']) ? 'https' : 'http';
		$return .= '://'.$_SERVER['SERVER_NAME'];
		if (
			(isset($_SERVER['HTTPS']) && $_SERVER['SERVER_PORT'] != 443)
		||
			(!isset($_SERVER['HTTPS']) && $_SERVER['SERVER_PORT'] != 80)
		) {
			$return .= ':'.$_SERVER['SERVER_PORT'];
		}
		$return .= $_SERVER['REQUEST_URI'];
		return $return;
	}
}


