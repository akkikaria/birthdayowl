<?php


class TotalPurchase {

    private $CI;
    private $totalValueOfProductPayU;   //this parameter will be set by payumoney response
    private $totalAmountCalculated;   //this instace variable will store the final value for matching with the payumoney;
    private $voucherFinalValue;    //this variable will stoere the final value of the voucher final amount after ForLoop addVoucherAmount;
    private $flowerFinalValue;     //this variable will store the final value of the flower after the ForLoop of addFlowerAmount;
    private $totalAmount;
    private $taxAmount;

    public function __construct() {
        $this->CI = & get_instance();
        $this->flowerFinalValue = 0;
        $this->voucherFinalValue = 0;
    }
    
    function getTotalValueOfProductPayU() {
        return $this->totalValueOfProductPayU;
    }

    function setTotalValueOfProductPayU($totalValueOfProductPayU) {
        $this->totalValueOfProductPayU = (int)$totalValueOfProductPayU;
    }
    
    public function addVoucherAmount() {
        $voucher_array = $this->CI->session->userdata['userdata']['voucher_data'];
        $countVoucherArray = count($voucher_array);
        if (!empty($voucher_array)) {
            for ($i = 0; $i < $countVoucherArray; $i++) {
                $currentvoucheramount = $voucher_array[$i]['selected_amount_new'];
                $this->voucherFinalValue = $this->voucherFinalValue + $currentvoucheramount;
            }
        }
    }

    public function addFlowerAmount() {
        $flower_array = $this->CI->session->userdata['userdata']['flower_data'];
        $countFlowerArray = count($flower_array);
        if (!empty($flower_array)) {
            for ($j = 0; $j < $countFlowerArray; $j++) {
                $currentfloweramount = $flower_array[$j]['selected_amount_new'];
                $this->flowerFinalValue = $this->flowerFinalValue + $currentfloweramount;
            }
        }
    }

    public function addTotal() {
        $this->totalAmount = $this->voucherFinalValue + $this->flowerFinalValue; //adding the total value 
    }

    public function addTax() {
        $this->taxAmount = 3.5 * $this->totalAmount / 100;    // 3.5 is the tax
    }

    public function totalAmountCalculated() {
        $this->totalAmountCalculated = (int)($this->totalAmount + $this->taxAmount); //adding tax value to the orginal value of product
    }

    public function validateFinalAmount() {
        if ($this->totalAmountCalculated <= $this->totalValueOfProductPayU) {   //here we added the condition of greater than or equal to as there can be other taxes of payumoney
            return true;
        } else {
            return false;
        }
    }

    public function runner() {
        $this->addVoucherAmount();
        $this->addFlowerAmount();
        $this->addTotal();
        $this->addTax();
        $this->totalAmountCalculated();
        
        $result = $this->validateFinalAmount();
        return $result;
    }

}
