<?php

class CommonHelperLibrary {

    private $CI;

    public function __construct() {
        $this->CI = &get_instance();
    }

    public function isMobileDevice() {
        $this->CI->load->library('user_agent');
        $isMobile = $this->CI->agent->is_mobile();

        if ($isMobile) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
