<?php

//this library will only be used to the Delete Invalid Guest where mobile_verify = 0
class DeleteInvalidGuest {

    private $CI;
    private $email;     // essential to set after object creation
    private $userId;
    private $phoneNumber;
    private $stringUserIds;

    public function __construct() {
        $this->CI = & get_instance();
    }

    function getStringUserIds() {
        return $this->stringUserIds;
    }

    function setStringUserIds($stringUserIds) {
        $this->stringUserIds = $stringUserIds;
    }

    function getEmail() {
        return $this->email;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function getUserId() {
        return $this->userId;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }

    function getPhoneNumber() {
        return $this->phoneNumber;
    }

    function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }

    public function runner() {
        $this->checkInValidGuest();
    }

    public function checkInValidGuest() {

//        $where_delete = "(email='$this->email' or mobile_no = '$this->phoneNumber') and is_guest = 1 and mobile_verify = 0 and is_google_login = 0";
        $where_delete = "email='$this->email' and is_guest = 1 and mobile_verify = 0 and is_google_login = 0";
        $check_user = $this->CI->Birthday->getSelectData("user_id", "users", $where_delete)->result_array();
        if (!empty($check_user)) {
            $userid_array = array();
            foreach ($check_user as $key => $value) {
                foreach ($value as $new_key => $new_value) {
                    $userid_array[] = $new_value;
                }
            }
            $user_id_string = implode(',', $userid_array);
            $this->stringUserIds = $user_id_string;
            $this->deleteInvalidGuest();
        }
    }

    private function deleteInvalidGuest() {
        $where = "DELETE FROM users WHERE user_id IN (" . "$this->stringUserIds" . ")";
        $this->CI->Birthday->simple_query($where);
    }

    //    public function checkInValidGuest() {
//        $where_delete = "email='$this->email' and is_guest = 1 and mobile_verify = 0";
////        $where_delete = "mobile_no = '$this->phoneNumber' and is_guest = 1 and mobile_verify = 0";
//        $check_user = $this->CI->Birthday->getSelectData("user_id", "users", $where_delete)->row_array();
//        if (!empty($check_user)) {
//            $this->userId = $check_user['user_id'];
//            $this->deleteInvalidGuest();
//        }
//    }
//    private function deleteInvalidGuest() {
//        $id = $this->userId;
//        //if id is found then delete the entry for the particular id and continue
//        $where = "user_id = '$id'";
//        $this->CI->Birthday->delete_reminder('users', $where);
//    }
}
