<?php

class DeleteInvalidBirthdayOwlUser {

    private $CI;
    private $email;     // essential to set after object creation
    private $userId;
    private $phoneNumber;
    private $stringUserIds;

    public function __construct() {
        $this->CI = & get_instance();
    }

    function getStringUserIds() {
        return $this->stringUserIds;
    }

    function setStringUserIds($stringUserIds) {
        $this->stringUserIds = $stringUserIds;
    }

    function getEmail() {
        return $this->email;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function getUserId() {
        return $this->userId;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }

    function getPhoneNumber() {
        return $this->phoneNumber;
    }

    function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }

    public function runner() {
        $this->checkInValidBirthdayOwlUser();
    }

    public function checkInValidBirthdayOwlUser() {

        if(!is_null($this->phoneNumber)){
            $where_delete = "(email='$this->email' or mobile_no = '$this->phoneNumber') and is_guest = 0 and mobile_verify = 0";
        }
        else{
            $where_delete = "email='$this->email' and is_guest = 0 and mobile_verify = 0";
        }
            
//        $where_delete = "(email='$this->email' or mobile_no = '$this->phoneNumber') and is_guest = 0 and mobile_verify = 0";
        $check_user = $this->CI->Birthday->getSelectData("user_id", "users", $where_delete)->result_array();
        $userid_array = array();
        if (!empty($check_user)) {
            foreach ($check_user as $key => $value) {
                foreach ($value as $new_key => $new_value) {
                    $userid_array[] = $new_value;
                }
            }
            $user_id_string = implode(',', $userid_array);
            $this->stringUserIds = $user_id_string;
            $this->deleteInvalidGuest();
        }
    }

//        public function checkInValidGuest() {
//        $where_delete = "email='$this->email' and is_guest = 0 and mobile_verify = 0";
//        $check_user = $this->CI->Birthday->getSelectData("user_id", "users", $where_delete)->row_array();
//        if (!empty($check_user)) {
//            $this->userId = $check_user['user_id'];
//            $this->deleteInvalidGuest();
//        }
//    }
//    private function deleteInvalidGuest2() {
//        $id = $this->userId;
////if id is found then delete the entry for the particular id and continue
//        $where = "user_id = '$id'";
//        $this->CI->Birthday->delete_reminder('users', $where);
//    }

    private function deleteInvalidGuest() {
        $where = "DELETE FROM users WHERE user_id IN (" . "$this->stringUserIds" . ")";
        $this->CI->Birthday->simple_query($where);
    }

}
