<?php

//here we only check the valid

class CheckAsGoogle {

    private $googleEmailId;
    private $userId;
    private $result;
    private $country_code;
    private $CI;

    public function __construct() {
        $this->CI = & get_instance();
    }

    function getGoogleEmailId() {
        return $this->googleEmailId;
    }

    function setGoogleEmailId($googleEmailId) {
        $this->googleEmailId = $googleEmailId;
    }

    function getResult() {
        return $this->result;
    }

    function setResult($result) {
        $this->result = $result;
    }

    function getCountry_code() {
        return $this->country_code;
    }

    function setCountry_code($country_code) {
        $this->country_code = $country_code;
    }

    function getUserId() {
        return $this->userId;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }

    private function checkavailibilityAsGoogle() {
        $query = "select * from users where email = '$this->googleEmailId' and is_google_login = '1'";
//        $query = "select * from users where email = '$this->guest_email_id' and is_guest != 1 and is_google_login = 1)";
        $result = $this->CI->Birthday->simple_query($query)->result_array();
//        var_dump($result);
//        exit;
        return $result;
    }

    public function runner() {
        $result = $this->checkavailibilityAsGoogle();
        if (!empty($result)) {
            $this->result = $result[0];
            $this->userId = $this->result['user_id'];
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function update($userInputArray) {
        $update_array["first_name"] = $userInputArray["first_name"];
        $update_array["fb_id"] = $userInputArray["fb_id"];
        $update_array["g_id"] = $userInputArray["g_id"];
        $update_array["username"] = $userInputArray["username"];
        $update_array["login_with"] = $userInputArray["login_with"];
        $update_array["user_profile_pic"] = $userInputArray["user_profile_pic"];
        $update_array["reset_time"] = $userInputArray["reset_time"];
        $update_array["reset_link"] = $userInputArray["reset_link"];
        $update_array["reset_activate"] = $userInputArray["reset_activate"];
        $update_array["membership_status"] = $userInputArray["membership_status"];
        $update_array["password"] = $userInputArray["password"];
        $update_array["country_id"] = $userInputArray["country_id"];
        $update_array["gender"] = $userInputArray["gender"];
        $update_array["bmonth"] = $userInputArray["bmonth"];
        $update_array["bdate"] = $userInputArray["bdate"];
        $update_array["byear"] = $userInputArray["byear"];
        $update_array["mobile_no"] = $userInputArray["mobile_no"];
        $update_array["zodiac_id"] = $userInputArray["zodiac_id"];
        $update_array["birth_date"] = $userInputArray["birth_date"];
        $update_array["created_date"] = $userInputArray["created_date"];
        $update_array["u_last_login"] = $userInputArray["u_last_login"];

//        $update_array = $userInputArray["is_google_login"];
//        $update_array = $userInputArray["is_guest"];
//        $update_array = $userInputArray["sent_greeting_counter"];
//        $update_array = $userInputArray["user_id"];
//        $update_array = $userInputArray["mobile_verify"];
//        $update_array = $userInputArray["email"];
//        $update_array = $userInputArray["updated_date"];
        $this->CI->Birthday->update("users", $update_array, "user_id = $this->userId");
    }

    public function CheckCountryCode() {
        if ($this->country_code != '102') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function reset_Google_Guest() {
        $update['mobile_verify'] = 1;
        $update['is_guest'] = 0;
        $update['is_google_login'] = 0;
        $this->CI->Birthday->update("users", $update, "user_id = $this->userId");
    }

    public function getUserInfo() {
        $where = "user_id = '$this->userId'";
        $user_info = $this->CI->Birthday->getSelectData("*", "users", $where)->row_array();
        return $user_info;
    }

}
