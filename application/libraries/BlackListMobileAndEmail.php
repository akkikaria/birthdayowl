<?php

class BlackListMobileAndEmail {

    private static $blackListMobile = array('7860923907', '7080405060', '8050406020', '8707880839', '8299057152', '7860923908', '8795154001' , '8050406020');
    private static $blackListEmail = array('sdfc200@gmail.com', 'bunticc1@gmail.com' , 'anubhav98002@gmail.com');
    private $is_black_list;

    public function __construct() {
        $this->is_black_list = FALSE;
    }

    public function checkBlaclistMobile($phone) {
        if (in_array($phone, self::$blackListMobile)) {
            $this->is_black_list = TRUE;
        }
    }

    public function checkBlacklistEmail($email) {
        if (in_array($email, self::$blackListEmail)) {
            $this->is_black_list = TRUE;
        }
    }
    
    public function isBlackList() {
        return $this->is_black_list;
    }

}
