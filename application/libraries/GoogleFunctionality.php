<?php

class GoogleFunctionality {

    private $user_email;
    private $user_name;
    private $current_date;
    private $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->library('session');
    }

    function getUser_name() {
        return $this->user_name;
    }

    function setUser_name($user_name) {
        $this->user_name = $user_name;

        if ($this->user_name == '' || $this->user_name == NULL) {
            $name = explode('@', $this->user_email);
            $this->user_name = $name[0];
        }
    }

    function getUser_email() {
        return $this->user_email;
    }

    function setUser_email($user_email) {
        $this->user_email = $user_email;
    }

    public function checkAvailability() {
//        $query = "select * from users where email = '$this->user_email' and (mobile_verify = '1' or login_with = '3')";
        $query = "select * from users where email = '$this->user_email' and (mobile_verify = '1' or is_google_login = '1')";
        $result = $this->CI->Birthday->simple_query($query)->result_array();
        return $result;
    }

    public function insertGoogleLogin() {

//        user_id first_name username password email mobile_no zodiac_id gender 0->male 1->female birth_date bdate bmonth byear fb_id g_id Descending 1 login_with user_profile_pic created_date updated_date u_last_login reset_time reset_link reset_activate sent_greeting_counter membership_status 0-not active 1-active, 2-expired country_id is_guest 0-normal user 1- guest mobile_verify

        $date = date('Y-m-d H:i:s');
        //
        $data["first_name"] = $this->user_name;
        $data["password"] = '';
        $data["email"] = $this->user_email;
        $data["bdate"] = '';
        $data["mobile_no"] = '';
        $data["zodiac_id"] = '';
        $data["gender"] = '';
        $data["birth_date"] = '';
        $data["username"] = '';
        $data["bmonth"] = '';
        $data["byear"] = '';
        $data["fb_id"] = '';
        $data["g_id"] = '';
        $data["login_with"] = 3;
        $data["user_profile_pic"] = '';
        $data["created_date"] = '';
        $data["u_last_login"] = $date;
        $data["reset_time"] = '';
        $data["reset_link"] = '';
        $data["reset_activate"] = '';
        $data["sent_greeting_counter"] = 0;
        $data["membership_status"] = '';
        $data['is_google_login'] = 1;
        $data["is_guest"] = 0;
        $data["mobile_verify"] = 0;

        $return_user_id = $this->CI->Birthday->addData('users', $data);

        $google_data['user_id_from_user'] = $return_user_id;
        $google_data['gmail_user_email'] = $this->user_email;
        $google_data['gmail_user_name'] = $this->user_name;
        $google_data['created_date'] = $date;

        $return_google_id = $this->CI->Birthday->addData('google_user', $google_data);

        $query = "select * from users where user_id = $return_user_id";
        $result = $this->CI->Birthday->simple_query($query)->result_array();
        return $result;
    }

    public function updateAvailability($user_id) {  //other login checks if the user is login wiht guest or facebook
//        $if_other_login = $other_login;
//        switch ($if_other_login) {
//            case 1://guest
//                $update_guest = 0;
//                break;
//            case 2://facebook
//                break;
//            case 3://
//                break;
//            default :
//                $update_guest = 0;
//        }
        $date = date('Y-m-d H:i:s');
        //
//        $data["first_name"] = $this->user_name;
//        $data["password"] = '';
//        $data["email"] = '';
//        $data["bdate"] = '';
//        $data["mobile_no"] = '';
//        $data["zodiac_id"] = '';
//        $data["gender"] = '';
//        $data["birth_date"] = '';
//        $data["bdate"] = '';
//        $data["bmonth"] = '';
//        $data["byear"] = '';
//        $data["fb_id"] = '';
//        $data["g_id"] = '';
        $data["login_with"] = 3;
//        $data["user_profile_pic"] = '';
//        $data["created_date"] = '';
        $data["u_last_login"] = $date;
//        $data["reset_time"] = '';
//        $data["reset_link"] = '';
//        $data["reset_activate"] = '';
//        $data["sent_greeting_counter"] = '';
//        $data["membership_status"] = '';
        $data['is_google_login'] = 1;
//        $data["is_guest"] = $update_guest;
//        $data["mobile_verify"] = '';

        $this->CI->Birthday->update("users", $data, "user_id = '$user_id'");
    }

    public function runner() {

        $row_data = $this->checkAvailability();
        $output = array();
        if (!empty($row_data)) {
//            check if he is already a google user or a birth day owl user
            $user_details = $row_data[0];
            if ($user_details['is_google_login'] == 1) { //that means the user is has logged in as google previously and is not an birthday owl user yet'
                $output["userdata"] = $user_details;
                $output["userdata"]['flower_data'] = array();
                $output["userdata"]['voucher_data'] = array();
                $output["success"] = true;
                $output["login_type"] = 3;
                $output["is_user_exist"] = 3; //User 3 means google user_exit
                $output["is_user_google_login"] = 1;
//                $this->session->set_userdata($output
            } else if ($user_details['is_guest'] == 1) { // that means the user which is logged as goole is already a guest in birhday owl
                $this->updateAvailability($user_details['user_id']);
                $output["userdata"] = $user_details;
                $output["userdata"]['flower_data'] = array();
                $output["userdata"]['voucher_data'] = array();
                $output["success"] = true;
                $output["login_type"] = 3;
                $output["is_user_exist"] = 3;
                $output["is_user_google_login"] = 1;
            } else { // he is birthday owl user who is logged in as a google user
                $output["userdata"] = $user_details;
                $output["userdata"]['flower_data'] = array();
                $output["userdata"]['voucher_data'] = array();
                $output["success"] = true;
                $output["login_type"] = 1;
                $output["is_user_exist"] = 1; //User 3 means google user_exit
//                $this->session->set_userdata($output);
            }
        } else {
//            // insert a new entry of goolge login
            $this->CI->load->library("DeleteInvalidGuest");
            $deleteInvalidGuest = new DeleteInvalidGuest();
            $deleteInvalidGuest->setEmail($this->user_email);
            $deleteInvalidGuest->setPhoneNumber(''); // here we are passing empty mobile number since we do not have mobile number while google login
            $deleteInvalidGuest->runner();
            $this->CI->load->library("DeleteInvalidBirthdayOwlUser");
            $deleteInvalidBirthdayowlUser = new DeleteInvalidBirthdayOwlUser();
            $deleteInvalidBirthdayowlUser->setEmail($this->user_email);
            $deleteInvalidBirthdayowlUser->setPhoneNumber(''); // here we are passing empty mobile number since we do not have mobile number while google login
            $deleteInvalidBirthdayowlUser->runner();
            $inserted_array = $this->insertGoogleLogin();
            $output["userdata"] = $inserted_array[0];
            $output["userdata"]['flower_data'] = array();
            $output["userdata"]['voucher_data'] = array();
            $output["success"] = true;
            $output["login_type"] = 3;
            $output["is_user_exist"] = 3; //User 3 means google user_exit
            $output["is_user_google_login"] = 1;
//            $this->session->set_userdata($output);
        }

        return $output;
    }

}
