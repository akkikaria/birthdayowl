<?php

class CheckAsGuest {

    private $CI;
    private $guestEmail;
    private $userId;
    private $mobileNo;
    private $result;
    private $country_code;

    public function __construct() {
        $this->CI = & get_instance();
    }

    function getGuestEmail() {
        return $this->guestEmail;
    }

    function setGuestEmail($guestEmail) {
        $this->guestEmail = $guestEmail;
    }

    function getResult() {
        return $this->result;
    }

    function setResult($result) {
        $this->result = $result;
    }

    function getUserId() {
        return $this->userId;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }

    function getCountry_code() {
        return $this->country_code;
    }

    function setCountry_code($country_code) {
        $this->country_code = $country_code;
    }

    function getMobileNo() {
        return $this->mobileNo;
    }

    function setMobileNo($mobileNo) {
        $this->mobileNo = $mobileNo;
    }

    public function checkAvailabilityAsGuest() {
        $where = "email = '$this->guestEmail' and is_guest = '1' and mobile_verify = '1'";
        $result = $this->CI->Birthday->getSelectData("*", "users", $where)->result_array();
//        $result = $this->CI->Birthday->simple_query($query)->result_array();
        return $result;
    }

    public function runner() {
        $result = $this->checkAvailabilityAsGuest();
        if (!empty($result)) {
            $this->result = $result[0];
            $this->userId = $this->result['user_id'];
//            $this->mobileNo = $this->result['mobile_no'];
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function update($userInputArray) {

        $update_array['first_name'] = $userInputArray["first_name"];
        $update_array["fb_id"] = $userInputArray["fb_id"];
        $update_array["g_id"] = $userInputArray["g_id"];
        $update_array["username"] = $userInputArray["username"];
        $update_array["login_with"] = $userInputArray["login_with"];
        $update_array["user_profile_pic"] = $userInputArray["user_profile_pic"];
        $update_array["reset_time"] = $userInputArray["reset_time"];
        $update_array["reset_link"] = $userInputArray["reset_link"];
        $update_array["reset_activate"] = $userInputArray["reset_activate"];
        $update_array["membership_status"] = $userInputArray["membership_status"];
        $update_array["password"] = $userInputArray["password"];
        $update_array["country_id"] = $userInputArray["country_id"];
        $update_array["gender"] = $userInputArray["gender"];
        $update_array["bmonth"] = $userInputArray["bmonth"];
        $update_array["bdate"] = $userInputArray["bdate"];
        $update_array["byear"] = $userInputArray["byear"];
//        $update_array["mobile_no"] = $userInputArray["mobile_no"];
        $update_array["zodiac_id"] = $userInputArray["zodiac_id"];
        $update_array["birth_date"] = $userInputArray["birth_date"];
        $update_array["created_date"] = $userInputArray["created_date"];
        $update_array["u_last_login"] = $userInputArray["u_last_login"];
//        $update_array["mobile_no"] = $userInputArray["mobile_no"];


//        $this->mobileNo = $userInputArray["mobile_no"];
//        $update_array = $userInputArray["is_google_login"];
//        $update_array = $userInputArray["is_guest"];
//        $update_array = $userInputArray["sent_greeting_counter"];
//        $update_array = $userInputArray["user_id"];
//        $update_array = $userInputArray["mobile_verify"];
//        $update_array = $userInputArray["email"];
//        $update_array = $userInputArray["updated_date"];
        $this->CI->Birthday->update("users", $update_array, "user_id = $this->userId");
    }

    public function checkMobileAreSame() {
        if ($this->result['mobile_no'] == $this->mobileNo) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function CheckCountryCode() {
        if ($this->country_code != '102') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function resetGoogleGuest() {
        $update['mobile_verify'] = 1;
        $update['is_guest'] = 0;
        $update['is_google_login'] = 0;
        $this->CI->Birthday->update("users", $update, "user_id = $this->userId");
    }

    public function updateForLogin($update_array) {
        $this->CI->Birthday->update("users", $update_array, "user_id = $this->userId");
    }

    public function getUserInfo() {
        $where = "user_id = '$this->userId'";
        $user_info = $this->CI->Birthday->getSelectData("*", "users", $where)->row_array();
        return $user_info;
    }

}
