<?php

class ThanksForPurchaseAuditCreator {

    private $CI;

    public function __construct() {
        $this->CI = &get_instance();
    }

    public function createAuditArray($payment_array, $user_id, $product_info, $items_in_cart) {// for minizing of oode in thanks for purchaase api I will take parameter
        $stringofItemsofvoucher = $this->checksessionValueofVoucher($items_in_cart);
        $audit_array = array(
            'payment_mode' => $payment_array['payment_mode'],
            'transaction_status' => $payment_array['transaction_status'],
            'bill_to_fname' => $payment_array['bill_to_fname'],
            'total_amount' => $payment_array['total_amount'],
            'transaction_id' => $payment_array['transaction_id'],
            'bill_to_email' => $payment_array['bill_to_email'],
            'purchased_date' => $payment_array['purchased_date'],
            'pauId' => $payment_array['payuId'],
            'user_id' => $user_id,
            'product_info' => $product_info,
            'items_in_cart' => $stringofItemsofvoucher
        );
        
        $this->CI->db->insert('thanks_for_purchase_voucher_audit', $audit_array);
    }

    private function checksessionValueofVoucher($voucher_items_in_cart) {
        $voucher_string = '';
        if (empty($voucher_items_in_cart)) {
            return $voucher_string;
        } else {
            foreach ($voucher_items_in_cart as $key => $value) {
                $quantity = $value["quantity"];
                $vouchername = $value["voucher_pro_name"];
                $voucher_string = $voucher_string . '_' . $vouchername . '-' . $quantity;
            }
            return $voucher_string;
        }
    }

}
