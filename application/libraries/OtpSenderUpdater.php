<?php

class OtpSenderUpdater {

    private $CI;
    private $mobileNumber;
    private $OtpCode;
    private $countryCode;
    private $hashKey;

    public function __construct() {
        $this->CI = & get_instance();
    }

    function getMobieNumber() {
        return $this->mobileNumber;
    }

    function getOtpCode() {
        return $this->OtpCode;
    }

    function setMobileNumber($mobieNumber) {
        $this->mobileNumber = $mobieNumber;
    }

    function setOtpCode($OtpCode) {
        $this->OtpCode = $OtpCode;
    }

    function getCountryCode() {
        return $this->countryCode;
    }

    function setCountryCode($countryCode) {
        $this->countryCode = $countryCode;
    }

    function getHashKey() {
        return $this->hashKey;
    }

    function setHashKey($hashKey) {
        $this->hashKey = $hashKey;
    }

    public function runner() {

        $this->code = $data['code'] = $this->generateRandomString(5);
        $hashkey = $this->hashKey;
        $data['mobile_no'] = $this->mobileNumber;
        $otp_data = $this->CI->Birthday->getSelectData("mobile_no", "otp", "mobile_no=$this->mobileNumber")->result_array();
        $count = count($otp_data);
        if ($count > 0) {
            $update["code"] = $this->code;
            $update["sent_time"] = date("Y-m-d H:i:s");
            $this->CI->Birthday->update("otp", $update, "mobile_no='$this->mobileNumber'");
        } else {
            $data["created_date"] = date("Y-m-d H:i:s");
            $data["sent_time"] = date("Y-m-d H:i:s");
            $this->CI->Birthday->addData("otp", $data);
        }
        $show_message = "<#> Enter the following code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone. $this->code $hashkey";
//        $show_message = "Enter $this->code as your verification code to login to your BirthdayOwl account. Valid for 5 minutes. Do not share it with anyone.";
        try {
            $resp = $this->CI->Birthday->send_mobile_otp($this->mobileNumber, $show_message);
//            $output["success"] = true;
            $output = true;
//                $output["message"] = "OTP sent successfully";
        } catch (Exception $e) {
//            $output["success"] = false;
            $output = false;
//                $output["message"] = "Failed.Please try again";
        }

        return $output;
    }

    private function generateRandomString($length) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
